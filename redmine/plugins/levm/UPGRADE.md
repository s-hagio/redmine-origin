Lychee EVM plugin 2.0
====================================================================

Uninstall levm 1.x
--------------------------------------------------------------------

1. cron等にスナップショット保存タスクを登録済であれば、登録を解除
1. (任意) schema_migrations, project_snapshots, version_snapshotsテーブルのバックアップを取る
1. schema_migrations テーブルから、levm関連のレコードを削除


	```sql
	select * from schema_migrations where version like '%levm';
	delete from schema_migrations where version like '%levm';
	```

1. project_snapshots, version_snapshotsテーブルを削除


	```sql
	drop table project_snapshots;
	drop table version_snapshots;
	```

1. levmディレクトリを削除


	```sh
	cd redmine_dir
	rm -r plugins/levm
	```


Install levm 2.0
--------------------------------------------------------------------


1. redmineのpluginsディレクトリにプラグインを展開


	```sh
	cd redmine_dir
	cp -r levm_dir plugins/levm
	```

1. bundle install実行


	```sh
	bundle install
	```

1. migration実行


	```sh
	bundle exec rake redmine:plugins:migrate RAILS_ENV=production 
	```


1. redmineインスタンスを再起動
1. EVMを利用するプロジェクトのモジュール設定でLychee evmを有効化
	>levm 1.xで設定済であれば不要です。

1. (任意) crontab等に以下のスナップショット保存タスクを1日毎に定期実行するように登録
	>過去の日付のEVMを当時の状態で描画するには、データベースにスナップショットが必要です。  
	>データベースに該当のスナップショットが存在しない場合は、即時計算した結果を描画します。
	>以下のタスクは、Lychee evmを有効にしたプロジェクトに対して、実行時の前日のスナップショットを保存します。


	```sh
	bundle exec rake redmine:plugins:levm:snapshot:fixed RAILS_ENV=production
	```
