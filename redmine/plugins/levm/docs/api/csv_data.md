# API

## Line chart
`GET /levm/api/projects/:project_id/csv_data/line_chart.json`

Parameters

| name | type | descriptions |
| --- | --- | --- |
| evm_targets | `[number, number \| null][]` | 対象バージョン. `[projectId, versionId]`の配列. Allの`versionId`には`null`を指定する
| evm_date2 | `string` | 基準日. YYYY-MM-DD |
| evm_unit | `'daily' \| 'weekly' \| 'monthly'` | 単位 |
| evm_y_axis_unit | `'hours' \| 'days' \| 'months'` | 工数単位
| eac_method | `'optimistic' \| 'intermediate' \| 'pessimistic'` | EACの算出方法

Response

```
{
    csv_data: {
        date: string,
        planned_value: number,
        actual_cost: number,
        earned_value: number,
        bac: number,
        spi: number | null,
        cpi: number | null,
    }[]
}
```

## Comparison chart 
`GET /levm/api/projects/:project_id/csv_data/comparison_chart.json`

Parameters

| name | type | descriptions |
| --- | --- | --- |
| evm_targets | `[number, number \| null][]` | 対象バージョン. `[projectId, versionId]`の配列. Allの`versionId`には`null`を指定する
| evm_date1 | `string` | 必須. 基準日前. YYYY-MM-DD |
| evm_date2 | `string` | 必須. 基準日後. YYYY-MM-DD |
| evm_y_axis_unit | `'hours' \| 'days' \| 'months'` | 工数単位
| eac_method | `'optimistic' \| 'intermediate' \| 'pessimistic'` | EACの算出方法

Response

```
{
    csv_data: {
        date: string,
        bac(former): number,
        bac: number,
        planned_value(former): number,
        planned_value: number,
        actual_cost: number,
        earned_value: number,
    }[]
}
```