namespace :redmine do
  namespace :plugins do
    namespace :levm do
      namespace :snapshot do
        desc "take yesterday's fixed snapshots by lychee evm plugin"
        task fixed: :environment do
          take_snapshots_on(1.day.ago.to_date)
        end

        desc 'take specified date snapshots by lychee evm plugin'
        task :on, %i[snapped_on project] => :environment do |_task, args|
          begin
            snapped_on = Date.parse(args.snapped_on)
            projects = ([Project.find(args.project)] if args.project.present?)
          rescue StandardError => e
            Rails.logger.error e
            Rails.logger.error e.backtrace.join("\n")
            next
          end

          take_snapshots_on(snapped_on, projects)
        end

        # TODO: Levm::Snapshotモデルに移動
        def take_snapshots_on(snapped_on, projects = nil)
          (projects || default_target_projects).each do |project|
            take_snapshots_for(project, snapped_on)
          end
        end

        def default_target_projects
          Project.active.joins(:enabled_modules).where(enabled_modules: { name: 'lychee_evm' })
        end

        def take_snapshots_for(project, snapped_on)
          ActiveRecord::Base.transaction do
            take_snapshot(project.id, nil, snapped_on)
            project.shared_versions.find_each do |version|
              take_snapshot(project.id, version.id, snapped_on)
            end
          end
        end

        def take_snapshot(project_id, version_id, snapped_on)
          return if Levm::Snapshot.where(project_id: project_id, version_id: version_id, snapped_on: snapped_on).present?
          begin
            Levm::Evm.new(project_id, version_id, snapped_on, 'daily').save_as_snapshot!
          rescue StandardError => e
            message = "#{Time.now} Taking Snapshot Failed. project_id: #{project_id}, version_id: #{version_id}, snapped_on: #{snapped_on}"
            Rails.logger.error message
            Rails.logger.error e
            Rails.logger.error e.backtrace.join("\n")

            if Redmine::Plugin.installed?(:lychee_message_box)
              LycheeMessageBox::SystemErrorImporter.call(
                error: e,
                raised_at: Time.current,
                subject: %i[ja en].to_h { |locale|
                  [
                    locale,
                    I18n.t(:raised_errors, locale: locale)
                  ]
                },
                description: %i[ja en].to_h { |locale|
                  [locale, I18n.t(:raised_errors_from_task, task: 'redmine:plugins:levm:snapshot:*', locale: locale)]
                }
              )
            end
          end
        end
      end
    end
  end
end
