namespace :redmine do
  namespace :plugins do
    namespace :levm do
      desc 'Create lemv development data.'
      task create_dev_data: :environment do
        env_seed_file = Rails.root.join('plugins', 'levm', 'db', 'seeds', "#{Rails.env}.rb")
        load(env_seed_file) if File.exist?(env_seed_file)

        lychee_cost_env_seed_file = Rails.root.join('plugins', 'levm', 'db', 'seeds', "lychee_cost_#{Rails.env}.rb")
        if File.exist?(lychee_cost_env_seed_file) && Redmine::Plugin.installed?(:lychee_cost)
          load(lychee_cost_env_seed_file)
        end
      end

      namespace :db do
        desc 'seeding test data'
        task seed: :environment do
          load Rails.root.join('plugins', 'levm', 'db', 'seed.rb')
        end
      end
    end
  end
end
