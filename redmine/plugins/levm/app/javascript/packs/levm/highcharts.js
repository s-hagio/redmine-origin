import Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting';

Exporting(Highcharts);
window.Highcharts = Highcharts;

Highcharts.setOptions({
  global: {
    useUTC: false
  }
});

if (document.documentElement.lang === 'ja') {
  Highcharts.setOptions({
    lang: {
      printChart: "印刷",
      downloadJPEG: "JPEGでダウンロード",
      downloadPDF: "PDFでダウンロード",
      downloadPNG: "PNGでダウンロード",
      downloadSVG: "SVGでダウンロード",
      months: [
        "1月",
        "2月",
        "3月",
        "4月",
        "5月",
        "6月",
        "7月",
        "8月",
        "9月",
        "10月",
        "11月",
        "12月",
      ],
      shortMonths: [
        "1月",
        "2月",
        "3月",
        "4月",
        "5月",
        "6月",
        "7月",
        "8月",
        "9月",
        "10月",
        "11月",
        "12月",
      ]
    },
  });
}
