import 'select2';
import 'select2/select2.css';

document.addEventListener('DOMContentLoaded', () => {
  $('.levm_select2').select2({
    dropdownAutoWidth: true
  });
});
