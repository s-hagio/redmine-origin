document.addEventListener('DOMContentLoaded', () => {
  function eacTitle(value){
    var eac_title = 'EAC = AC + (BAC - EV)';
    switch(value) {
      case 'optimistic':
        return eac_title;
      case 'intermediate':
        return eac_title + ' / CPI';
      case 'pessimistic':
        return eac_title + ' / (CPI * SPI)';
      default:
        return eac_title + ' / CPI';
    }
  }

  setTimeout(() => {
    $('tspan:contains(EAC)').parents('.highcharts-legend-item').attr('title', eacTitle($('#eac_method').val()));
    $('tspan:contains(SPI)').parents('.highcharts-legend-item').attr('title', 'SPI = EV / PV');
    $('tspan:contains(CPI)').parents('.highcharts-legend-item').attr('title', 'CPI = EV / AC');

    $('.highcharts-legend-item').tooltip();
  }, 1000);
});
