document.addEventListener('DOMContentLoaded', () => {
  $('#query_id').on('change', function(){
    $(this).siblings('a').hide();

    var query_id = $.trim($(this).val());
    location.search = 'query_id=' + query_id;
  });

  $("input[name='levm_query[visibility]']").change(function(){
    var roles_checked = $('#levm_query_visibility_1').is(':checked');
    var private_checked = $('#levm_query_visibility_0').is(':checked');

    $("input[name='query[role_ids][]'][type=checkbox]").attr('disabled', !roles_checked);

    if (!private_checked){
      $("input.disable-unless-private").attr('checked', false);
    }
    $("input.disable-unless-private").attr('disabled', !private_checked);
  }).change();

  $('#levm_query_evm_type').on('change', function(){
    if ($(this).val() === 'line') {
      $('#former-date, #latter-date').hide();
    }else{
      $('#former-date, #latter-date').show();
    }
  }).change();

  $('#levm_query_query_is_for_all').on('change', function(){
    if($(this).prop('checked')){
      $('#levm-target, #levm-milestones').hide();
    }else{
      $('#levm-target, #levm-milestones').show();
    }
  }).change();
});
