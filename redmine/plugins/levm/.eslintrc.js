module.exports = {
  extends: 'airbnb-base',
  plugins: ['import'],
  env: {
    browser: true,
    jquery: true,
  },
  rules: {
    'space-before-function-paren': ['error', 'never'],
    'func-names': ['error', 'never'],
    'prefer-arrow-callback': 0,
  },
};
