function hideDoneRatioForm() {
  $('p#done-ratio-form')
    .html('')
    .hide();
}

function renderDoneRatioForm() {
  const issueId = $('#time_entry_issue_id').val();
  if (issueId === '') {
    hideDoneRatioForm();
    return;
  }

  const doneRatioForm = $('p#done-ratio-form');
  $.ajax({
    url: doneRatioForm.data('url'),
    type: 'GET',
    data: {
      issue_id: issueId,
    },
  })
    .done(function(data) {
      doneRatioForm.html(data).show();
    })
    .fail(hideDoneRatioForm);
}

$(function() {
  $('#time_entry_hours')
    .parent()
    .after($('p#done-ratio-form'));
  renderDoneRatioForm();

  $('#time_entry_issue_id').on('change', renderDoneRatioForm);
});
