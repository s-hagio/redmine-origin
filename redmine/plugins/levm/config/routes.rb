# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

get '/projects/:project_id/project_evms', to: 'project_evms#index', as: 'project_evms'
get '/projects/:project_id/line_chart', to: 'project_evms#line_chart', as: 'line_chart_project_evms'
get '/projects/:project_id/comparison_chart', to: 'project_evms#comparison_chart', as: 'comparison_chart_project_evms'

get '/projects/:project_id/evm_targets', to: 'project_evms#evm_targets', defaults: { format: :json }
get '/projects/:project_id/milestones', to: 'project_evms#milestones', defaults: { format: :json }

namespace :levm do
  resource :done_ratio_form, only: :show
end

resources :projects, only: [] do
  resources :levm_queries, except: %i[index show]
end

namespace :levm do
  namespace :api do
    resources :projects, only: [] do
      resources :csv_data, only: [], defaults: { format: :json } do
        collection do
          get :line_chart
          get :comparison_chart
        end
      end
    end
  end
end
