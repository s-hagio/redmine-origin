const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: './app/javascript/packs/levm.js',
  output: {
    path: './assets',
    filename: '/javascripts/levm.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015']
      }
    }, {
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        fallbackLoader: 'style-loader',
        loader: 'css-loader'
      })
    }, {
      test: /\.(png|gif)$/, use: 'url-loader'
    }]
  },
  plugins: [
    new ExtractTextPlugin('./stylesheets/levm.css')
  ]
};
