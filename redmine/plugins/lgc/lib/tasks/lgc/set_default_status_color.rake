namespace :redmine do
  namespace :plugins do
    namespace :lgc do
      desc "Set default colors to existing issue_statuses for Lychee Gantt Chart"
      task set_default_status_colors: :environment do
        ActiveRecord::Base.transaction do
          IssueStatus.all.zip(Lgc::STATUS_COLOR_OPTIONS.map(&:last).reject{|code| code == '#ffffff'}.cycle).each do |status, color_code|
            status.update!(lgc_color: color_code)
          end
        end
      end
    end
  end
end
