namespace :redmine do
  namespace :plugins do
    namespace :lgc do
      desc "Fix Setting.plugin_lgc type"
      task fix_setting_type: :environment do
        value = Setting.where(name: 'plugin_lgc').first_or_initialize.value

        if value.is_a?(ActionController::Parameters)
          Setting['plugin_lgc'] = value.to_unsafe_hash.with_indifferent_access
        end
      end
    end
  end
end
