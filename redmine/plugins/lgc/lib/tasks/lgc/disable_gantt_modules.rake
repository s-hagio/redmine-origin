namespace :redmine do
  namespace :plugins do
    namespace :lgc do
      desc "remove 'gantt' from default_projects_modules and destroy enabled_modules named 'gantt'"
      task disable_gantt_modules: :environment do
        Setting.set_from_params 'default_projects_modules', (Setting.default_projects_modules - ['gantt'])
        EnabledModule.destroy_all(name: 'gantt')
      end
    end
  end
end
