namespace :redmine do
  namespace :plugins do
    namespace :lgc do
      desc "upgrade enbaled_modules from 'gantt' to 'lgc'"
      task upgrade_enabled_modules: :environment do
        Lgc::Upgrader::V2_0_0.upgrade_enabled_modules
      end
    end
  end
end
