namespace :redmine do
  namespace :plugins do
    namespace :lgc do
      desc "Add label columns to saved queries and setting"
      task add_label_columns_to_saved_queries_and_setting: :environment do
        Lgc::IssueQuery.where(type: 'Lgc::IssueQuery').find_each do |query|
          next if query.has_default_columns?
          query.column_names = __prepend_label_columns(query.column_names)
          query.save
        end

        setting = Setting.plugin_lgc
        if setting
          setting['column_names'] = __prepend_label_columns(setting['column_names'])
          Setting.plugin_lgc = setting
        end
      end

      private

      def __prepend_label_columns(column_names)
        column_names.map(&:to_sym).tap do |names|
          # ラベル用の項目が既に入っている場合は変更済みなのでスキップ
          next if names.any? { |name| Lgc::IssueQuery::LABEL_COLUMNS.include?(name.to_sym) }

          # 固定の項目（題名）とセットで先頭に挿入
          names.unshift(*(Lgc::IssueQuery::LABEL_COLUMNS + Lgc::IssueQuery::FIXED_COLUMNS))
          names.uniq!
        end
      end
    end
  end
end
