namespace :redmine do
  namespace :plugins do
    namespace :lgc do
      task upgrade: :environment do
        Lgc::Upgrader.run
        Rake::Task["db:schema:dump"].invoke
      end
    end
  end
end
