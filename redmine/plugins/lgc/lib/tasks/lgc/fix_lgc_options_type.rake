namespace :redmine do
  namespace :plugins do
    namespace :lgc do
      desc "Fix lgc_options column type"
      task fix_lgc_options_type: :environment do
        module MigrateFixLgcOptionsType
          class CustomField < ActiveRecord::Base
            self.table_name = "#{table_name_prefix}custom_fields#{table_name_suffix}"
            self.inheritance_column = '__type__disabled__' # disable STI
            default_scope -> { where(type: 'IssueCustomField') }
            serialize :lgc_options
          end
          class IssueQuery < ActiveRecord::Base
            self.table_name = "#{table_name_prefix}queries#{table_name_suffix}"
            self.inheritance_column = '__type__disabled__' # disable STI
            default_scope -> { where(type: 'Lgc::IssueQuery') }
            serialize :lgc_options
          end
          class Project < ActiveRecord::Base
            self.table_name = "#{table_name_prefix}projects#{table_name_suffix}"
            serialize :lgc_options
          end
        end

        puts 'Migrate IssueCustomField#lgc_options'
        MigrateFixLgcOptionsType::CustomField.all.find_each do |issue_custom_field|
          if issue_custom_field.lgc_options.is_a?(ActionController::Parameters)
            issue_custom_field.update_columns lgc_options: issue_custom_field.lgc_options.to_unsafe_hash.with_indifferent_access
          end
        end
        puts 'Migrate Lgc::IssueQuery#lgc_options'
        MigrateFixLgcOptionsType::IssueQuery.all.find_each do |issue_query|
          if issue_query.lgc_options.is_a?(ActionController::Parameters)
            issue_query.update_columns lgc_options: issue_query.lgc_options.to_unsafe_hash.with_indifferent_access
          end
        end
        puts 'Migrate Project#lgc_options'
        MigrateFixLgcOptionsType::Project.all.find_each do |project|
          if project.lgc_options.is_a?(ActionController::Parameters)
            project.update_columns lgc_options: project.lgc_options.to_unsafe_hash.with_indifferent_access
          end
        end
      end
    end
  end
end
