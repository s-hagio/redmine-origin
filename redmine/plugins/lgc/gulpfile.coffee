path = require('path')
gulp = require('gulp')
$ = do require('gulp-load-plugins')
uglifySaveLicense = require('uglify-save-license')
isProduction = process.env.NODE_ENV == 'production'

# Config
config =
  js:
    src: './src/js/lgc'
    dest: './assets/javascripts/lgc.js'
    order: [
      'extensions/**/*.coffee'
      'lgc.coffee'
      'initializers/**/*.coffee'
      'utils/*.coffee'
      'utils/**/*.coffee'
      'mixins/**/*.coffee'
      'models/model.coffee'
      'models/abstracts/*.coffee'
      'models/issue.coffee'
      'models/new_issue.coffee'
      'models/row.coffee'
      'models/*.coffee'
      'models/**/*.coffee'
      'validations/validation.coffee'
      'validations/*.coffee'
      'collections/collection.coffee'
      'collections/*.coffee'
      'collections/**/*.coffee'
      'decorators/decorator.coffee'
      'decorators/abstracts/*.coffee'
      'decorators/**/*.coffee'
      'behaviors/behavior.coffee'
      'behaviors/**/*.coffee'
      'views/abstracts/layout_view.coffee'
      'views/abstracts/collection_view.coffee'
      'views/abstracts/composite_view.coffee'
      'views/abstracts/item_view.coffee'
      'views/abstracts/*.coffee'
      'views/**/row_view.coffee'
      'views/issue_detail/item_view.coffee'
      'views/**/*.coffee'
      'controllers/controller.coffee'
      'controllers/*.coffee'
      'controllers/**/*.coffee'
    ]

  pro_js:
    src: './src/js/lgc_pro'
    dest: './assets/javascripts/lgc_pro.js'
    order: [
      'lgc_pro.coffee'
      'initializers/**/*.coffee'
      'models/abstracts/*.coffee'
      'models/*.coffee'
      'models/**/*.coffee'
      'collections/*.coffee'
      'collections/**/*.coffee'
      'behaviors/*.coffee'
      'behaviors/**/*.coffee'
      'views/*.coffee'
      'views/**/*.coffee'
      'controllers/*.coffee'
      'controllers/**/*.coffee'
      'activator/**/*.coffee'
    ]

  pdf_js:
    src: './src/js/pdf'
    dest: './assets/javascripts/pdf.js'
    order: [
      'pdf.coffee'
      'app_context.coffee'
      'object.coffee'
      'views/view.coffee'
      'views/row.coffee'
      'views/svg.coffee'
      'views/svg/view.coffee'
      'builders/builder.coffee'
      'components/component.coffee'
      'builders/progress_line_builder.coffee'
      'views/**/*'
    ]

  css:
    src: './src/css'
    dest: './assets/stylesheets'

  vendor_js:
    src: './src/js/lib'
    dest: './assets/javascripts/vendor.js'
    order: ['jquery.js', 'underscore.js', 'backbone.js', 'jquery/**/*', 'backbone/**/*']

  test:
    dir: './test/js'
    target: './test/js/**/*_spec.coffee'

# 本番用ビルドではファイル名に.minをつける
if isProduction
  Object.keys(config).forEach (key) =>
    destPath = config[key].dest
    return unless destPath
    index = destPath.lastIndexOf('.')
    ext = destPath.slice(index)
    return if ext != '.js'
    config[key].dest = destPath.slice(0, index) + '.min' + ext

# JS
gulp.task 'js', ->
  gulp.src "#{config.js.src}/**/*.coffee"
    .pipe $.plumber()
    .pipe $.order(config.js.order)
    .pipe $.cache($.coffee(bare: true))
    .pipe $.concat(path.basename(config.js.dest))
    .pipe $.iife(useStrict: false)
    .pipe $.if isProduction, $.uglify()
    .pipe gulp.dest(path.dirname(config.js.dest))

gulp.task 'pro_js', ->
  gulp.src "#{config.pro_js.src}/**/*.coffee"
    .pipe $.plumber()
    .pipe $.order(config.pro_js.order)
    .pipe $.cache($.coffee(bare: true))
    .pipe $.concat(path.basename(config.pro_js.dest))
    .pipe $.iife(useStrict: false)
    .pipe $.if isProduction, $.uglify()
    .pipe gulp.dest(path.dirname(config.pro_js.dest))

gulp.task 'pdf_js', ->
  gulp.src "#{config.pdf_js.src}/**/*.coffee"
    .pipe $.plumber()
    .pipe $.order(config.pdf_js.order)
    .pipe $.cache($.coffee(bare: true))
    .pipe $.concat(path.basename(config.pdf_js.dest))
    .pipe $.if isProduction, $.uglify()
    .pipe gulp.dest(path.dirname(config.pdf_js.dest))

gulp.task 'js:test', ['js', 'pro_js'], ->
  gulp.start ['test']

# Vendor JS
gulp.task 'vendor-js', ->
  gulp.src "#{config.vendor_js.src}/**/*.js"
    .pipe $.plumber()
    .pipe $.order(config.vendor_js.order)
    .pipe $.concat(path.basename(config.vendor_js.dest))
    .pipe $.if isProduction, $.uglify(output: {comments: uglifySaveLicense})
    .pipe gulp.dest(path.dirname(config.vendor_js.dest))

# CSS
gulp.task 'css', ->
  gulp.src "#{config.css.src}/**/*.{sass,scss}"
    .pipe $.plumber()
    .pipe $.sass(indentedSyntax: true, outputStyle: 'compressed')
    .pipe $.if isProduction, $.rename(suffix: '.min')
    .pipe gulp.dest(config.css.dest)

# Build
gulp.task 'build', ['js', 'pro_js', 'pdf_js', 'vendor-js', 'css']

# Watch
gulp.task 'watch', ['build'], ->
  # CSS
  gulp.watch "#{config.css.src}/**/*", ['css']
  # JS
  gulp.watch "#{config.js.src}/**/*", ['js']
  gulp.watch "#{config.pro_js.src}/**/*", ['pro_js']
  gulp.watch "#{config.pdf_js.src}/**/*", ['pdf_js']

# Default
gulp.task 'default', ['build']
