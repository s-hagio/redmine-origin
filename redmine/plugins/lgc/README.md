# LGC2

- [次期LGC](https://github.com/agileware-jp/lychee_gantt_chart)
- [リリースノート](https://lychee-redmine.jp/release_detailn/schedule/gantt-chart/)

## 開発

### 準備

```console
$ nodenv install --skip-existing
$ pyenv install --skip-existing
```

### ビルド

```console
$ npx yarn build
```

### 動作

```console
$ npx yarn watch
```
