get 'lgc', to: 'lgc#index'
post 'lgc/export', to: 'lgc/export#create', defaults: { format: :pdf }

resources :projects do
  get 'lgc', to: 'lgc#index'
  post 'lgc/export', to: 'lgc/export#create', defaults: { format: :pdf }

  namespace :lgc do
    post 'issues/new' # 編集モードで使用
    resources :issues, shallow: true do
      post :edit # 編集モードで使用
      resources :time_entries, controller: :timelog, only: %w( create )
    end
    resources :queries, only: %w( new create )
    resources :journals, only: %w( index )
    put :query_params, to: 'query_params#update'
  end
end

namespace :lgc do
  resources :queries, except: %w( show )
  resources :relations, controller: :issue_relations, only: %w( create destroy )
  resources :issues, only: %w( index destroy )
  resources :projects, only: %w( index update )
  resources :versions, only: %w( index update )
  resources :journals, only: %w( index update destroy )
  resources :annotations, only: :index
  put :query_params, to: 'query_params#update'

  match 'context_menu/:action', controller: 'context_menus', via: %w( get post ), as: :context_menu
  get 'projects/:id/field_rule', to: 'projects#field_rule', as: :project_field_rule

  delete 'issues', to: 'issues#destroy'
  post 'issues/bulk_edit', to: 'issues#bulk_edit'
  post 'issues/bulk_update', to: 'issues#bulk_update'
  post 'issues/assume'
  post :custom_sort_orders, to: 'custom_sort_orders#save'
  post 'undo', to: 'undo#undo'
  post 'redo', to: 'undo#redo'

  get 'projects/:project_id/workers', to: 'workers#index', as: :project_workers, defaults: { format: :json }
  get 'workers', to: 'workers#index', defaults: { format: :json }
end

namespace :api do
  namespace :v1 do
    resources :projects, only: [] do
      resource :gantt, path: 'lgc', only: :show, format: :json
    end
    resource :gantt, path: 'lgc', only: :show, format: :json do
      resource :sync, only: :show
    end
  end
end
