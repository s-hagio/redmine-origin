#!/bin/bash -x

echo '[Redmineチェックアウト]'
git clone --depth=1 --branch=${REDMINE_VERSION:-3.4-stable} https://github.com/redmine/redmine.git
echo '[Redmine database.yml]'
cp project/.circleci/database.yml redmine/config/
echo '[他のプラグイン用のスクリプトがあればコピー]'
if [ -e project/.circleci/setup_plugins.rb ]; then
  cp project/.circleci/setup_plugins.rb redmine/
fi
if [ -e project/.circleci/seeds.rb ]; then
  cp project/.circleci/seeds.rb redmine/db/
fi

echo '[プロジェクトをplugins配下に移動]'
mkdir -p redmine/plugins/${CIRCLE_PROJECT_REPONAME}
shopt -s dotglob # .circleciをmvするのに必要
mv project/* redmine/plugins/${CIRCLE_PROJECT_REPONAME}/

echo '[redmineをproject配下に移動し]'
mv redmine/* project/
cd project/

echo '[必要であれば他のプラグインをチェックアウト・セットアップ]'
if [ -e setup_plugins.rb ]; then
  ruby setup_plugins.rb
fi

echo '[Redmineをセットアップ]'
bundle install
RAILS_ENV=test bundle exec rake db:create db:migrate redmine:plugins:migrate --trace
RAILS_ENV=test REDMINE_LANG=ja rake redmine:load_default_data --trace
RAILS_ENV=test rake redmine:plugins:assets --trace
RAILS_ENV=test rake db:seed --trace

exit 0 # 強制的に成功とし以降のコマンドでセットアップ成功かを判断する（シェルの実行が失敗ステータスになる要因がよくわからない）
