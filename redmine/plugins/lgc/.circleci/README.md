# circle ci for LGC

## CircleCI上でのテストの実行

config.ymlのbuild部分に相当

setup.sh概要

- redmine取得
- redmineのデータベース(このフォルダ内のdatabase.yml)
- redmineとこのプロジェクトの配置(このプロジェクトをredmineのplugins配下に移動)
- setup_plugins.rbが存在すれば、その内容にしたがってプラグインを配置、セットアップ
- redmineのデータベースのマイグレーション
- seeds.rbが存在すれば、そのSEEDを読み込む
- このフォルダ内のGemfileを使ってテストを実行(redmineのGemfileやGemfile.localなどを必要に応じて読み込んでいる)

### ruby, redmineのバージョンを切り替える方法

rubyは以下から適当なイメージを選択し、config.ymlに指定
https://hub.docker.com/r/circleci/ruby/tags/

redmineは環境変数 REDMINE_VERSION か、setup.shのデフォルト値を変更
環境変数はconfig.ymlのenvironmentなどで指定することも可能

## 手元のマシーンでテストを実行

手元のマシーンで同様のテストを実行できる
環境構築が正しくできれば、CirckeCI上でのテスト結果と同様になる
基本的にCircleCI上で失敗しているテストを修正する場合は、この手元で動かす方法で修正するべき

- ローカルにredmine, pluginsなどを含んだ環境を構築する必要がある
    - setup_plugins.rb, seeds.rbはローカルでも使用可能
    - setup.shに倣ってデータベースをリセットする
- 使われるのはローカルのselenium+Chrome+chromedriver
- 実行が早い
- byebug, pryなどのデバッガ、Chromeのデベロッパツールが使える

例.lgcの場合

```
# redmineセットアップ後
cd plugins/lgc
BUNDLE_GEMFILE=.circleci/Gemfile bundle install
BUNDLE_GEMFILE=.circleci/Gemfile bundle exec rspec spec/features/ --format=documentation
```

※ 言語・タイムゾーンなどの設定、datepickerの設定、Linux, MacOS間の違いなどで差異ができる場合がある

## 手元のマシーンから共有環境に対してテストを実行

手元にredmineを構築しなくても動作する
browserstackで失敗しているテストを修正する時に使う
コマンドライン->browserstack->共有環境のテスト実行(環境変数DRIVER=remoteとする)

例.lgcの場合

```
BUNDLE_GEMFILE=.circleci/Gemfile bundle install
DRIVER=remote BUNDLE_GEMFILE=.circleci/Gemfile bundle exec rspec spec/features/lgc00_baseline_spec.rb --format=documentation

# lgc00_baseline_spec.rb
# Starting standalone mode on [https://newrelic.cloudmine.jp/redmine]
```

もし、様々な環境の共有環境を構築できれば、それらの環境に対してテストを実行することも可能

例. https://any.sharedredmine.com というredmineの共有環境があれば

```
BASE_URL=https://any.sharedredmine.com BUNDLE_GEMFILE=.circleci/Gemfile bundle exec rspec spec/features/lgc00_baseline_spec.rb --format=documentation
# browserstackがhttps://any.sharedredmine.comに対してテストを実行
```

また、ローカルでredmineを起動していれば、chromedriverを指定して

```
BASE_URL=http://localhost:3000 BUNDLE_GEMFILE=.circleci/Gemfile bundle exec rspec spec/features/lgc00_baseline_spec.rb --format=documentation
```

## browserstackでブラウザを切り替えて共有環境に対してビルドする

異なるブラウザやOSに対して選択的にビルドを起動できる
Refs: [Nightly Builds](https://circleci.com/docs/1.0/nightly-builds/)

### trigger_build.rbを使ってコマンドラインから実行

手動、cronなどで実行可能

#### 準備

CircleCIの [API Token](https://circleci.com/account/api) を取得して
_.circleci/.circle_token_ に置く

#### 使い方

```
cd .circleci
./trigger_build.rb -h
```

例

```
# ブランチを変更
./trigger_build.rb agileware-jp/lgc new_branch

# ブラウザーを変更
./trigger_build.rb agileware-jp/lgc -b IE --browser-version=11

# OSを変更
./trigger_build.rb agileware-jp/lgc -o 'OS X' --os-version='High Sierra'
```

### 環境変数を指定して実行

config.yml内でenvironmentなどを設定すれば、異なるブラウザのパターンを指定できる
それらをCircleCIのworkflowでそれぞれ定期実行すれば良い

基本的に環境変数でbrowserstackの設定の切り替えが行われる
BS_BROWSER, BS_BROWSER_VERSION, BS_OS, BS_OS_VERSIONなど

## テスターのローカル環境でのredmine構築,テスト実行手順(メモ)
```
mkdir superdirectory
cd superdirectory
git clone https://github.com/agileware-jp/lgc.git
git clone --depth=1 --branch=${REDMINE_VERSION:-3.4-stable} https://github.com/redmine/redmine.git
cp lgc/.circleci/database.yml redmine/config
cp lgc/.circleci/setup_plugins.rb redmine
cp lgc/.circleci/seeds.rb redmine/db
mkdir -p redmine/plugins/lgc
shopt -s dotglob
mv lgc/* redmine/plugins/lgc/
mv redmine/* lgc/
cd lgc
sudo npm install -g gulp
ruby setup_plugins.rb
`sed -i -e 's/Factory/#Factory/g' plugins/lpt/init.rb`
`sed -i -e 's/Factory/#Factory/g' plugins/lychee_issue_board/init.rb`
<!-- testデータベースを設定する(database.yml) -->
bundle install
RAILS_ENV=test bundle exec rake db:drop db:create db:migrate redmine:plugins:migrate --trace
RAILS_ENV=test REDMINE_LANG=ja rake redmine:load_default_data --trace
RAILS_ENV=test rake redmine:plugins:assets --trace
RAILS_ENV=test rake db:seed --trace
cd plugins/lgc
BUNDLE_GEMFILE=.circleci/Gemfile bundle install
BUNDLE_GEMFILE=.circleci/Gemfile bundle exec rspec spec --format=documentation
```
