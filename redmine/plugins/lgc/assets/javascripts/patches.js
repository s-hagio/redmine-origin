!function($) {
  'use strict'

  function isLabelColumn($option) {
    return QUERY_LABEL_COLUMNS.indexOf($option.val()) !== -1
  }

  function isFixedColumn($option) {
    return QUERY_FIXED_COLUMNS.indexOf($option.val()) !== -1
  }

  function findFixedColumns($select) {
    return $select.find(QUERY_FIXED_COLUMNS.map(function(val) {
      return 'option[value="' + val + '"]'
    }).join(','))
  }

  var finderMethodsByType = { up: 'prev', down: 'next' }
  var moverMethodsByType = { up: 'before', down: 'after' }

  function moveOptionToUpOrDown(selectElement, upOrDown) {
    var selectedOptions = $(':selected', selectElement).toArray()
    var finderMethod = finderMethodsByType[upOrDown]
    var moverMethod = moverMethodsByType[upOrDown]

    selectedOptions.forEach(function(el) {
      var $selectedOption = $(el)
      var $targetOption = $selectedOption[finderMethod]()

      while (selectedOptions.indexOf($targetOption[0]) !== -1) {
        $targetOption = $targetOption[finderMethod]()

        if (!$targetOption || !$targetOption.length) {
          return
        }
      }

      if (isLabelColumn($selectedOption) || !isFixedColumn($targetOption)) {
        $targetOption[moverMethod]($selectedOption)
      }
    })
  }

  window.moveOptions = function(selectFromElement, selectToElement) {
    var $from = $(selectFromElement)
    var $to = $(selectToElement)
    var $option = $from.find(':selected').prop('selected', false)

    if (isLabelColumn($option)) {
      $to.prepend($option)
    } else {
      $to.append($option)
    }
  }

  window.moveOptionUp = function(selectElement) {
    moveOptionToUpOrDown(selectElement, 'up')
  }

  window.moveOptionDown = function(selectElement) {
    moveOptionToUpOrDown(selectElement, 'down')
  }

  window.moveOptionTop = function(selectElement) {
    var $select = $(selectElement)
    var selectedOptions = $(':selected', selectElement).toArray()
    var prependableOptions = []
    var unprependableOptions = []

    selectedOptions.forEach(function(el) {
      var $selectedOption = $(el)

      if (isLabelColumn($selectedOption)) {
        prependableOptions.push(el)
      } else {
        unprependableOptions.push(el)
      }
    })

    if (prependableOptions.length) {
      $select.prepend(prependableOptions)
    }
    if (unprependableOptions.length) {
      findFixedColumns($select).last().after(unprependableOptions)
    }
  }

  window.selectAllOptions = function(id) {
    $('#' + id).children().prop('selected', true)
  }
}(jQuery)

