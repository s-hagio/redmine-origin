!function($) {
  var KEY = 'lgc:fullscreen:state';
  var state = sessionStorage.getItem(KEY) || 'exit';

  function toggleState() {
    state = ['exit', 'start'][+(state === 'exit')];
  }

  function applyState() {
    $('html').toggleClass('lgc-is-fullscreen', state === 'start');
    window.Backbone && Backbone.$(window).trigger('resize');
    sessionStorage.setItem(KEY, state);
  }

  function updateTriggerElements() {
    $('[data-fullscreen]').each(function() {
      var $el = $(this);
      $el.text($el.attr('data-fullscreen-label-' + state));
      $el.toggleClass('lgc-icon-fullscreen__exit', state === 'start');
      $el.toggleClass('lgc-icon-fullscreen__start', state === 'exit');
    });
  }

  $(document).on('click', '[data-fullscreen]', function() {
    toggleState();
    applyState();
    updateTriggerElements();
  });

  $(updateTriggerElements);
  applyState();
}(jQuery);
