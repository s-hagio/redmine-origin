api.array :issues do
  @bulk_edit.issues.each do |issue|
    api.issue do
      api.link link_to_issue(issue)
    end
  end
end

disabled_core_fields = @bulk_edit.disabled_core_fields
disabled_core_fields << 'parent_issue_id' if @bulk_edit.issue_projects.size != 1
disabled_core_fields.concat Lgc::ACTUAL_DATE_COLUMNS unless Lgc.actual_date?

api.disabled_core_fields disabled_core_fields
api.safe_attribute_names @bulk_edit.safe_attribute_names
api.is_closed @bulk_edit.closed?, true
api.is_all_default_status @bulk_edit.all_default_status?, 1

if Redmine::VERSION::MAJOR >= 4
  api.preview_url preview_text_path
end

%w[ project tracker status priority assigned_to category fixed_version ].each do |assoc_name|
  key = "#{assoc_name}_id"

  if @bulk_edit.safe_attribute?(key)
    api.__send__(@bulk_edit.collection_name_of(key), @bulk_edit.select_options_for(key))
  end
end

%w[ is_private done_ratio ].each do |attr_name|
  if @bulk_edit.safe_attribute?(attr_name)
    api.__send__(@bulk_edit.collection_name_of(attr_name), @bulk_edit.select_options_for(attr_name))
  end
end

api.array :custom_fields do
  @bulk_edit.editable_custom_field_values.each do |value|
    custom_field = value.custom_field

    api.custom_field do
      api.id custom_field.id
      api.label custom_field.name
      api.format custom_field.field_format
      api.validation_rules(
        custom_field.slice(:regexp, :min_length, :max_length).delete_if do |k, v|
          v.blank? ||
            (k == 'min_length' && v.to_i == 0) ||
            (k == 'max_length' && v.to_i == 0)
        end
      )

      api.field_id custom_field_tag_id(:issue, custom_field)
      api.field custom_field_tag_for_bulk_edit(:issue, custom_field, @bulk_edit.issues, @bulk_edit.cf_param_of(custom_field.id))
    end
  end
end
