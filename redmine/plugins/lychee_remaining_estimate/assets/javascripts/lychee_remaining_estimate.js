$(function(){
  $remaining_estimate_area = $('#remaining_estimate_area');
  issue_estimated_hours = $('#issue_estimated_hours');
  issue_done_ratio = $('#issue_done_ratio');
  if(issue_estimated_hours.length){
    issue_estimated_hours.parent().after($remaining_estimate_area)
  } else if(issue_done_ratio.length){
    issue_done_ratio.parent().before($remaining_estimate_area);
  } else {
    $('#issue-form .splitcontentright').eq(0).prepend($remaining_estimate_area);
  }
});
