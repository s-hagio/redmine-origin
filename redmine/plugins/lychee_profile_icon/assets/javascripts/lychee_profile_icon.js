var LycheeProfileIcon = (function($) {
  "use strict";

  var LycheeProfileIcon = {};

  function setPreviewAvatar(input, previewAvatar) {
    previewAvatar.addClass('only-avatar').removeClass('only-default-avatar');

    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        previewAvatar.find('img.gravatar').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  function resetPreviewAvatar(input, previewAvatar){
    previewAvatar.addClass('only-default-avatar').removeClass('only-avatar');
  }

  LycheeProfileIcon.addInputFiles = function(inputEl){
    if($(inputEl).val() == '') { return; }

    var attachmentsForm = $(inputEl).closest('.attachments_form');
    var attachmentsFields = attachmentsForm.find('.attachments_fields');
    var addAttachment = attachmentsForm.find('.add_attachment');
    var clearedFileInput = $(inputEl).clone().val('');

    var maxFileSize = $(inputEl).data('max-file-size');
    var maxFileSizeExceeded = $(inputEl).data('max-file-size-message');
    var sizeExceeded = false;

    $.each(inputEl.files, function() {
      if (this.size && maxFileSize != null && this.size > parseInt(maxFileSize)) { sizeExceeded = true; }
    });
    if (sizeExceeded) {
      $(inputEl).val(null);
      window.alert(maxFileSizeExceeded);
      return;
    }

    attachmentsFields.find('.icon-del').click(); // Delete avatar before save new avatar
    window.addInputFiles(inputEl);
    setPreviewAvatar(inputEl, attachmentsForm.siblings('.preview-avatar'));

    attachmentsFields.find('input.description').remove();
    $('#attachments_fields').siblings('.file_selector').remove();
    addAttachment.show(); // addAttachment always should be shown

    if(!addAttachment.find('.file_selector')){
      clearedFileInput.prependTo(addAttachment);
    }

    attachmentsFields.on('click', '.icon-del', function(){
      LycheeProfileIcon.deleteInputFiles(this);
    })
  }

  LycheeProfileIcon.deleteInputFiles = function(inputEl){
    var attachmentsForm = $(inputEl).closest('.attachments_form');

    resetPreviewAvatar(inputEl, attachmentsForm.siblings('.preview-avatar'));
    attachmentsForm.find('.add_attachment').show();
    $(inputEl).parent().remove();
  }

  return(LycheeProfileIcon);
}($));