var methods = {
  expand: function(){
    this.trigger('expand');
    this.treeAction('showChildren');

    this.removeClass('tree-collapsed').addClass('tree-expanded');
    this.find('.tree-expander').removeClass('tree-expander-collapsed').addClass('tree-expander-expanded');
  },

  collapse: function(){
    this.trigger('collapse');
    this.treeAction('hideChildren');

    this.removeClass('tree-expanded').addClass('tree-collapsed');
    this.find('.tree-expander').removeClass('tree-expander-expanded').addClass('tree-expander-collapsed');
  },

  showChildren: function(){
    var children = this.siblings("[data-parent-id='" + this.data('project-id') + "']");
    children.each(function(){
      if($(this).treeAction('isExpanded')){
        $(this).treeAction('showChildren');
      }
    })
    children.show();
  },

  hideChildren: function(){
    var children = this.siblings("[data-parent-id='" + this.data('project-id') + "']");
    children.each(function(){
      $(this).treeAction('hideChildren');
    });
    children.hide();
  },

  toggle: function(){
    if (this.treeAction('isCollapsed')) {
      this.treeAction('expand');
    } else if (this.treeAction('isExpanded')) {
      this.treeAction('collapse');
    } else {
      return this;
    }
  },

  isCollapsed: function(){
    return this.hasClass("tree-collapsed");
  },

  isExpanded: function(){
    return this.hasClass("tree-expanded");
  }
}

module.exports = methods;
