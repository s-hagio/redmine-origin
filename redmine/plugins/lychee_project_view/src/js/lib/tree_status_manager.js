var Cookies = require('js-cookie');
const TREE_SESSION_KEY = 'lychee_project_view/collapsed_trees/';

class TreeStatusManager {
  constructor(treeName) {
    this.keyName = TREE_SESSION_KEY + treeName;
  }

  onExpand(projectId) {
    var ids = Cookies.getJSON(this.keyName) || [];
    var index = ids.indexOf(projectId);
    if (index > -1) {
      ids.splice(index, 1);
    }
    Cookies.set(this.keyName, JSON.stringify(ids), {expires: 7});
  }

  onCollapse(projectId) {
    var ids = Cookies.getJSON(this.keyName) || [];
    if (ids.indexOf(projectId) === -1) {
      ids.push(projectId);
    }
    Cookies.set(this.keyName, JSON.stringify(ids), {expires: 7});
  }
}

module.exports = TreeStatusManager;
