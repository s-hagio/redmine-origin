function onFavoriteButtonClick(event) {
  event.preventDefault();
  var $target = $(event.target);
  var projectId = $target.data('projectId');
  var url = $target.hasClass('favorited') ? $target.data('removeProjectFavoritesPath') : $target.data('addProjectFavoritesPath');
  return $.ajax({
    method: 'POST',
    url: url,
    data: { project_id: projectId }
  }).done(function() {
    $(`.favorite-project-button[data-project-id="${projectId}"]`).toggleClass('favorited');
  }).fail(function() {
    alert('更新に失敗しました。');
  });
}

module.exports = onFavoriteButtonClick;
