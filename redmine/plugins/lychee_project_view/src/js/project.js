var onFavoriteButtonClick = require('./lib/favorite_button');

$(function() {
  $('.lpv-contextual-favorite-button-container')
    .appendTo('.contextual')
    .show()
    .find('.favorite-project-button')
    .on('click', onFavoriteButtonClick);
});
