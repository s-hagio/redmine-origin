import onFavoriteButtonClick from './lib/favorite_button';
import TreeStatusManager from './lib/tree_status_manager';
import methods from './lib/tree_actions';

$(function() {
  $.fn.treeAction = function(method) {
    if (methods[method]) {
      return methods[method].apply(this);
    } else {
      $.error(`Method with name ${method} does not exists for treeAction`);
    }
  };

  $('label[for="closed"]').parents('form').hide();
  $('.icon-fav.my-project').removeClass('icon-fav').addClass('icon-user');

  $('h3 .tree-expander').on('click', function() {
    var $this = $(this);
    if($this.hasClass('tree-expander-expanded')){
      $this.parent().next().hide();
      $this.removeClass('tree-expander-expanded').addClass('tree-expander-collapsed');
    }else{
      $this.parent().next().show();
      $this.removeClass('tree-expander-collapsed').addClass('tree-expander-expanded');
    }
  });

  $('.favorite-projects, .list.projects').each(function() {
    initPreparedTree($(this));
  });

  $('.icon-quick-link').tooltip();

  function initPreparedTree($tree) {
    $tree.find('.tree-expander.tree-expander-collapsed').each(function(){
      $(this).parents('tr').treeAction('hideChildren');
    });

    $tree.on('click', '.tree-expander', function() {
      $(this)
        .parents('tr')
        .treeAction('toggle');
    });

    $tree.on('click', '.favorite-project-button', function(event) {
      onFavoriteButtonClick(event).done(function() {
        const $favorites = $('.favorite-project-container');
        const url = $favorites.data('projectFavoritesPath');
        // お気に入りプロジェクト取得後にツリーを初期化する
        $favorites.load(url + ' .favorite-projects', function() {
          initPreparedTree($(this).find('.favorite-projects'));
        });
      });
    });

    const treeStatusManager = new TreeStatusManager($tree.data('treeName'));

    $tree.on('expand', function(event) {
      treeStatusManager.onExpand($(event.target).data('projectId'));
    });

    $tree.on('collapse', function(event) {
      treeStatusManager.onCollapse($(event.target).data('projectId'));
    });

    $tree.on('click', 'tr.group', function(){
      var $this = $(this);
      var $expander = $this.find('span.expander');

      $this.toggleClass('open');
      $expander.toggleClass('icon-expended icon-collapsed');

      $this.siblings("tr[data-group-value='" + $this.data('group-value') + "']").toggle();
    })
  }
});
