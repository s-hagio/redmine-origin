# lychee_project_view

## 開発

### 準備

必要なバージョンのNodeJSとnpmパッケージをインストール

```console
$ nodenv install --skip-existing $(cat .node-version) && npm install
```

### Webpackの起動

```console
$ npm start
```
