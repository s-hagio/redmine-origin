namespace :redmine do
  namespace :plugins do
    namespace :lychee_project_view do
      desc 'Generate sample data for Lychee Project View.'
      task seed: :environment do
        load Rails.root.join('plugins', 'lychee_project_view', 'db', 'seeds.rb')
      end

      desc 'Enable all native and installed plugins quick links'
      task enable_all_quick_links: :environment do
        LycheeProjectView::QuickLinks.enable_all
      end

      desc 'Enable a quick link for project setting'
      task enable_setting_quick_link: :environment do
        lpv_setting = Setting.plugin_lychee_project_view
        Setting.plugin_lychee_project_view = lpv_setting.merge(
          'quick_links' => lpv_setting['quick_links'] | ['settings']
        )
      end

      desc 'Fix old kanban setting'
      task fix_old_kanban_settings: :environment do
        lpv_setting = Setting.plugin_lychee_project_view
        is_rejected = lpv_setting['quick_links'].reject! { |link| link.in?(%w[ranked_kanban assigned_kanban]) }
        lpv_setting['quick_links'] << 'kanban' if is_rejected
        Setting.plugin_lychee_project_view = lpv_setting
      end

      desc 'Update Lychee Issue Board settings for Lychee Kanban'
      task update_from_lychee_issue_board_to_lychee_kanban: :environment do
        lpv_setting = Setting.plugin_lychee_project_view
        %w[quick_links position].each do |setting|
          next if lpv_setting[setting].blank?

          lpv_setting[setting].map! { |name|
            name == 'lychee_issue_board' ? 'lychee_kanban' : name
          }
        end
        Setting.plugin_lychee_project_view = lpv_setting
      end

      desc 'Conversion bookmark project'
      task conversion_bookmark_project: :environment do
        unless Redmine::VERSION::STRING >= '4.1'
          next
        end

        lpv = LpvProjectFavorite.includes(:user, :project)
        ActiveRecord::Base.transaction do
          lpv.group_by(&:user).each do |user, project_favorites|
            project_ids = project_favorites.map(&:project_id)
            new_ids = (user.bookmarked_project_ids + project_ids).uniq.join(',')
            user.pref.others[:bookmarked_project_ids] = new_ids
            user.pref.save!
          end
        end
      end

      desc 'Enable a quick link for lychee ccpm and project_report'
      task enable_lychee_ccpm_and_project_report_quick_link: :environment do
        lpv_setting = Setting.plugin_lychee_project_view
        lpv_setting['quick_links'] ||= []
        Setting.plugin_lychee_project_view = lpv_setting.merge(
          'quick_links' => lpv_setting['quick_links'] | ['lychee_ccpm', 'lychee_project_report']
        )
      end

      desc 'Enable a quick link for project_dashbord'
      task enable_lychee_project_dashboard_quick_link: :environment do
        lpv_setting = Setting.plugin_lychee_project_view
        lpv_setting['quick_links'] ||= []
        Setting.plugin_lychee_project_view = lpv_setting.merge(
          'quick_links' => lpv_setting['quick_links'] | ['lychee_project_dashboard']
        )
      end
    end
  end
end
