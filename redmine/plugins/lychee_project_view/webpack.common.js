const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
  entry: ['./src/js/project.js', './src/js/projects.js'],
  plugins: [
    new CleanWebpackPlugin(),
    new WebpackNotifierPlugin({
      title: 'Webpack [Lychee Project View]',
      skipFirstNotification: true,
    }),
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'assets/javascripts'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/preset-env',
                {
                  targets: [
                    'last 1 chrome version',
                    'last 1 firefox version',
                    'ie 11',
                  ],
                },
              ],
            ],
          },
        },
      },
    ],
  },
};
