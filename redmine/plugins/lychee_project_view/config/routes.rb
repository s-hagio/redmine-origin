resources :project_favorites, only: %i[index] do
  collection do
    post :add
    post :remove
  end
end

put 'settings/plugin/lychee_project_view', to: 'lychee_project_view/quick_links#reorder'
