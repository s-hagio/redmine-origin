Feature: ActualDate
  Manage ActualDate

  @wip
  Scenario: Create a IssueSet
    Given logged in as actual_date admin
    And   goto "OnlineStore" New Issue page

    When  I fill in the following:
        |Subject           |actual date test |
        |Actual Start Date |2012-12-25       |
        |Actual Due Date   |2012-12-27       |
    And   press "Create" exact match

    Then  I should see "Actual Start Date"
    And     should see "12/25/2012"

    When  I follow "Issues"
    Then  I should see "Issue on project 2"

    Then the issues list should be:
      |Tracker |Status |Priority |Subject            |
      |Bug     |New    |Normal   |actual date test   |
      |Bug     |New    |Low      |Issue on project 2 |

  @wip
  Scenario: Create a IssueSet validate. not a valid date
    Given logged in as actual_date admin
    And   goto "OnlineStore" New Issue page

    When  I fill in the following:
        |Subject           |not a valid date   |
        |Actual Start Date |1   |
        |Actual Due Date   |2   |
    And   press "Create" exact match

    Then  I should see "Actual Start Date is not a valid date"
    And     should see "Actual Due Date is not a valid date"

  @wip
  Scenario: Create a IssueSet validate. actual due date greater than actual start date.
    Given logged in as actual_date admin
    And   goto "OnlineStore" New Issue page

    When  I fill in the following:
        |Subject           |greater than test |
        |Actual Start Date |2012-12-27        |
        |Actual Due Date   |2012-12-25        |
    And   press "Create" exact match

    Then  I should see "Actual Due Date must be greater than Actual Start Date"

  @wip
  Scenario: Create a IssueSet validate. Fields permissions
    Given logged in as actual_date admin
    And   goto Fields permissions page
    And   select "Developer" from "Role:"
    And   select "Bug" from "Tracker:"
    And   press "Edit"
    And   select Required from permissions new "actual_start_date"
    And   select Required from permissions new "actual_due_date"
    And   press "Save"
    And   I follow "Sign out"

    #Developer user
    And   I logged in as "jsmith" with password "jsmith"
    And   goto "OnlineStore" New Issue page

    When  I fill in the following:
        |Subject           |actual date required |
    And   press "Create" exact match

    Then  I should see "Actual Start Date can't be blank"
    And     should see "Actual Due Date can't be blank"
