namespace :redmine do
  namespace :plugins do
    namespace :lad do
      desc 'Correct tracker fields for description (Redmine-3.4.x)'
      task correct_fields_bits: :environment do
        next unless Tracker::CORE_FIELDS.include?('description')
        Tracker.all.each do |tracker|
          next unless tracker.fields_bits
          # insert description bit - xx0xxxxxxxx
          tracker.fields_bits = (((tracker.fields_bits & 0b1100000000) << 1) + (tracker.fields_bits & 0b0011111111))
          tracker.save!
        end
      end
    end
  end
end
