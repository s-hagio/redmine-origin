$(function(){
  $actual_dates_area = $('#actual_dates_area');
  $target = $($('#issue_due_date')[0] || $('#issue_start_date')[0] || $('#issue_parent_issue_id')[0]);
  if ($target[0]) {
    $target.parent().after($actual_dates_area);
  } else {
    $('#issue-form .splitcontentright').eq(0).prepend($actual_dates_area);
  }
});

$(document).ajaxComplete(function() {
  if ($('#issue_actual_start_date').val() !== '' &&
      $('#actual_start_date_was').data('date') === '') {
    $('#issue_actual_start_date').addClass('lad_value_entered_autofill');
  }

  if ($('#issue_actual_due_date').val() !== '' &&
      $('#actual_due_date_was').data('date') === '') {
    $('#issue_actual_due_date').addClass('lad_value_entered_autofill');
  }
});
