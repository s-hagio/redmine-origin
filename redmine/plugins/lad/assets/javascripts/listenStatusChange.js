$(function() {
  $('body.controller-issues.action-bulk_edit #issue_status_id').on(
    'change',
    function() {
      updateBulkEditFrom($('#bulk-edit-url').data('url'));
    }
  );
});
