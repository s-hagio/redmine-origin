$(function() {
  var addLadFields = function() {
    $.ajax({
      type: "GET",
      url: $("#imports-lad").data("url")
    });
  };

  addLadFields();

  $(document).ajaxComplete(function(e, xhr, settings) {
    var endpoint = settings.url.split('/').pop().replace(/\?.*$/, "");
    if (endpoint === "mapping.js") {
      addLadFields();
    }
  });
});
