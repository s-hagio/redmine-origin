namespace :redmine do
  namespace :plugins do
    namespace :lychee_workdays do
      desc 'migrate rest_days table from redmine_work_days'
      task migrate_from_redmine_work_days: :environment do
        rest_days = RestDay.all
        LycheeRestDay.transaction do
          rest_days.each { |rest_day| LycheeRestDay.create!(rest_day.attributes) }
        end
      end
    end
  end
end
