# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

resources :rest_days do
  post :import, :on => :collection
  post :api, on: :collection
end
