var LycheeWorkdays = (function($) {
  'use strict';

  function addRestDayClasses($table, rest_days) {
    $table
      .find('td.even')
      .filter(function() {
        return (
          $.inArray(
            $(this)
              .find('.day-num')
              .text(),
            rest_days
          ) >= 0
        );
      })
      .addClass('rest_day');
  }

  function addNonWorkingWeekDayClasses($table, non_working_week_days) {
    $.each(non_working_week_days, function(_index, non_working_week_day) {
      var non_working_day_index = $table
        .find("th:contains('" + non_working_week_day['day_name'] + "')")
        .index();
      var columns = $table.find(
        'td:nth-child(' + (non_working_day_index + 1) + ')'
      );

      columns.addClass('non_working_week_day');
    });
  }

  function addSaturdayClasses($table, saturday) {
    var saturdayIndex = $table.find("th:contains('" + saturday + "')").index();
    $table
      .find('td:nth-child(' + (saturdayIndex + 1) + ')')
      .addClass('saturday');
  }

  var LycheeWorkdays = {};

  LycheeWorkdays.addClasses = function(
    $table,
    non_working_week_days,
    rest_days,
    saturday
  ) {
    addRestDayClasses($table, rest_days);
    addNonWorkingWeekDayClasses($table, non_working_week_days);
    addSaturdayClasses($table, saturday);
  };

  return LycheeWorkdays;
})(jQuery);
