var LycheeWorkdaysGantts = (function($) {
  'use strict';
  var LycheeWorkdaysGantts = {};

  LycheeWorkdaysGantts.setRestDay = function(gantt_days_size, lychee_rest_days_index){
    var gantt_hdr_size = $('.gantt_hdr').length;
    if(gantt_days_size > gantt_hdr_size) { return }

    for (var i = 0, item; (item = lychee_rest_days_index[i]); i++) {
      var lychee_rest_day_index = gantt_hdr_size - gantt_days_size + item;

      $(".gantt_hdr").eq(lychee_rest_day_index).addClass('nwday');
      if(gantt_hdr_size > gantt_days_size * 2){
        $(".gantt_hdr").eq(lychee_rest_day_index - gantt_days_size).addClass('nwday');
      }
    }
  }
  return LycheeWorkdaysGantts;
})(jQuery);
