resources :lychee_issue_sets do
  resources :lychee_issue_templates, except: :destroy do
    resources :relations, controller: 'lychee_issue_template_relations', only: [:create, :destroy]
    put 'update_position'
    get 'context_menu', on: :collection
  end
  match 'update_template_titles', via: [:post, :patch]

  get 'create_issue_set', on: :collection
  get 'export', on: :collection
  collection do
    get 'imports/new', to: 'lychee_issue_sets_imports#new'
    post 'imports', to: 'lychee_issue_sets_imports#create'
  end
  delete '/lychee_issue_templates', to: 'lychee_issue_templates#destroy'
end

namespace :lychee_issue_templates do
  resources :auto_completes, only: :index
end

resources :projects, only: [] do
  resource :issue_set_setting, only: :update
  resources :issue_sets, controller: 'issue_forms', only: [:edit, :update] do
    get 'context_menu', on: :member
    defaults format: :js do
      member do
        get 'bulk_edit'
        get 'bulk_exclude'
        get 'bulk_include'
      end
    end
  end
end
