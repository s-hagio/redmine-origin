namespace :redmine do
  namespace :plugins do
    namespace :lychee_issue_set do
      desc 'Set default positions for Lychee Issue Set.'
      task set_default_positions: :environment do
        Lychee::IssueSet.find_each do |issue_set|
          position = 0
          issue_set.issue_templates.order(:root_id, :lft).each do |issue_template|
            issue_template.update_columns(position: position += 1)
          end
        end
      end
    end
  end
end
