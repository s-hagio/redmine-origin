$(function() {
  $(document).on("change", "#issue_set_option", function() {
    var val = $('#issue_set_option').val();

    if (val === 'head' || val === 'tail') {
      $('#option_insert_text').show();
      $('#option_replace_text').hide();
      $('#replace_text_form').hide();
    } else {
      $('#option_insert_text').hide();
      $('#option_replace_text').show();
      $('#replace_text_form').show();
    }
  });
}());
