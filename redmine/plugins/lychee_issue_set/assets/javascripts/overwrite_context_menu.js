// prevent cursor loss in description textarea
function contextMenuClearDocumentSelection() { }

function findExcludeValueInput(element) {
  return element.find('input[type="hidden"]').filter(function () {
    return /lychee_issue_set_issue_templates_attributes_(\d+)_exclude_in_issue_set_edit/.test($(this).attr('id'));
  }).first()
}

// exclude_in_issue_set_editがtrueの行は、全体チェック処理で除外する
function toggleIssuesSelection(el) {
  var checked = $(this).prop('checked');
  var boxes = $(this).parents('table').find('input[name=ids\\[\\]]');

  boxes.each(function (_, box) {
    const isExclude = findExcludeValueInput($(box).parents('.hascontextmenu')).val() === 'true';
    if (isExclude) { return; }

    $(box).prop('checked', checked).parents('.hascontextmenu').toggleClass('context-menu-selection', checked);
  });
}

// 除外済みのテンプレートは、クリックしても選択させないようにする
const oldContextMenuAddSelection = contextMenuAddSelection;
var contextMenuAddSelection = (tr) => {
  const isExclude = findExcludeValueInput($(tr)).val() === 'true';
  if (isExclude) { return; }
  oldContextMenuAddSelection(tr);
}

function getIssueTemplateExclusionValues(issueTemplateIds) {
  return issueTemplateIds.map(function () {
    const id = $(this).val();
    const value = findExcludeValueInput($(`tr.hascontextmenu[data-issue-template-id="${id}"]`)).val();
    return { id, value };
  });
}

function getIssueTemplateExclusionValuesAsQueryParam(issueTemplateIds) {
  const values = getIssueTemplateExclusionValues(issueTemplateIds);
  return values.map(function (_, v) {
    return `exclusion_values[${v.id}]=${v.value}`;
  }).get().join('&');
}

// The context menu uses a GET action on the form.
// Sending a large quantity of string parameters to a GET action will result in
// "HTTP element QUERY_STRING (1024 * 10) longer than allowed length".
$(function() {
  var oldAjax = $.ajax;

  $.ajax = function() {
    // Pass only id, not the contents of input data.
    if (arguments[0].url.match(/context_menu/)) {
      const issueTemplateIds = $(event.target).parents('form').find('[id=ids_]')
      arguments[0].data = issueTemplateIds.serialize() +
        '&' +
        getIssueTemplateExclusionValuesAsQueryParam(issueTemplateIds);
    }
    return oldAjax.apply($, arguments);
  }
}());

// For input and textarea, override contextMenuRightClick to display the default context menu.
var oldContextMenuRightClick = contextMenuRightClick;
var contextMenuRightClick = function(event) {
  // ajaxリクエスト時のパラメータを作るため、除外しているissue templateを右クリックした場合、他のチェックは全部外してチェックだけ入れる
  const isExclude = findExcludeValueInput($(event.target).parents('tr')).val() === 'true';
  const checkbox = $(event.target).parents('.hascontextmenu');
  if (isExclude) {
    contextMenuUnselectAll();
    oldContextMenuAddSelection(checkbox);
    console.log(checkbox);
  }

  if ($(event.target).is('input, textarea')) { return; }
  oldContextMenuRightClick(event);
}

// Fix where click on anything other than input area would uncheck the issue list.
// This issue is resolved in Redmine >= 3.3.
var oldContextMenuClick = contextMenuClick;
var contextMenuClick = function(event) {
  if ($('#ajax-modal').is(':visible')) { return; }
  oldContextMenuClick(event);
}

// Custom field checkbox are not checked for row selection.
function contextMenuCheckSelectionBox(tr, checked) {
  tr.find('input[type=checkbox]').not('[name*="custom_field_values"]').prop('checked', checked);
}

var oldContextMenuInit = contextMenuInit;
// Event is attached to the document, before overriding a method.
// This issue is resolved in Redmine >= 3.4.
var contextMenuInit = function () {
  oldContextMenuInit.apply(this, arguments);

  var event = $._data(document, 'events').click.find(
    function(ev, _, _) {
      return ev.handler.name === 'contextMenuClick'
    }
  );
  if (event) {
    event.handler = contextMenuClick;
  }

  $(document).unbind('contextmenu').contextmenu(contextMenuRightClick);
}
