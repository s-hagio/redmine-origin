$(function() {
  $('#issue-template-bulk-edit-submit').click(function() {
    var editedIssueTemplateIds = $('#issue-template-bulk-edit-submit').data('editedIssueTemplateIds');
    $('#ajax-modal select, #ajax-modal input[type="date"], #ajax-modal input[type="text"], #ajax-modal textarea').each(function() {
      var modalSourceId = $(this).attr('id').match(/(issue_.+)/)[1].replace('_id', '');
      modalSourceId = $(this).attr('multiple') ? modalSourceId.slice(0, -1) : modalSourceId;
      var selectedTargetsSelector = editedIssueTemplateIds.map(function (id) {
        return '.hascontextmenu[data-issue-template-id=' + id + '] .' + modalSourceId
      }).join(', ');
      var $selectedTargets = $(selectedTargetsSelector);
      var tagName = $(this).prop('tagName');

      if ($(this).val()) {
        if (isCheckBoxGroup($selectedTargets)) {
          checkOnCheckBoxGroup($selectedTargets, $(this).val());
        } else if (isCheckBox($selectedTargets)) {
          $selectedTargets.find('input[type="checkbox"]').prop('checked', parseInt($(this).val()));
        } else {
          $selectedTargets.find(tagName).val($(this).val());
        }
      }
      if (isValueClearCheckBoxChecked(modalSourceId)) {
        $selectedTargets.find(tagName).val('');
      }
    });
    hideModal(this);
  });

  function isValueClearCheckBoxChecked(target) {
    return $('[data-disables="#' + target + '"]').is(':checked');
  }

  function isCheckBoxGroup($target) {
    return $target.find('.check_box_group input')[0];
  }

  function isCheckBox($target) {
    return $target.find('input[type="checkbox"]')[0];
  }

  function boolNormalization(val) {
    return val === '__none__' ? '' : val;
  }

  function checkOnCheckBoxGroup($checkBoxGroup, selectedValue) {
    $checkBoxGroup.find('input').prop('checked', false);

    if ($.type(selectedValue) === 'array') {
      $.each(selectedValue, function(_, val) {
        $checkBoxGroup.find('input[value="' + val + '"]').prop('checked', true);
      });
    } else {
      $checkBoxGroup.find('input[value="' + boolNormalization(selectedValue) + '"]').prop('checked', true);
    }
  }
}());
