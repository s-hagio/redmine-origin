$(function() {
  $(document).on("change", "input[type='date'][id^=lychee_issue_set_issue_templates_attributes_]", function() {
    var tokens = $(this).attr('id').split('_').reverse();
    if (tokens[0] === 'date' && tokens[1] === 'start') {
      set_issue_set_due_date(tokens[2], $(this).val());
    }
  });

  function set_issue_set_due_date(id, value) {
    var due_date_element = $("input#lychee_issue_set_issue_templates_attributes_" + id + "_due_date");
    if (due_date_element.val() === '') due_date_element.val(value);
  }
}());
