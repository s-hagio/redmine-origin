api.issue do
  api.id @issue.id
  api.projectId @issue.project_id unless @issue.project.nil?
  api.trackerId @issue.tracker_id unless @issue.tracker.nil?
  api.statusId @issue.status_id unless @issue.status.nil?
  api.priorityId @issue.priority_id unless @issue.priority.nil?
  api.author @issue.author_id unless @issue.author.nil?
  api.assignedToId @issue.assigned_to_id unless @issue.assigned_to.nil?
  api.categoryId @issue.category_id unless @issue.category.nil?
  api.fixedVersionId @issue.fixed_version_id unless @issue.fixed_version.nil?
  api.parentIssueId @issue.parent_id unless @issue.parent.nil?
  api.actualStartDate @issue.actual_start_date unless @issue.actual_start_date.nil?
  api.actualDueDate @issue.actual_due_date unless @issue.actual_due_date.nil?

  api.subject @issue.subject
  api.description @issue.description
  api.startDate @issue.start_date
  api.dueDate @issue.due_date
  api.doneRatio @issue.done_ratio
  api.isPrivate @issue.is_private
  api.estimatedHours @issue.estimated_hours
  api.remainingEstimate @issue.remaining_estimate
  api.totalEstimatedHours @issue.total_estimated_hours
  if User.current.allowed_to?(:view_time_entries, @project)
    api.spentHours(@issue.spent_hours)
    api.totalSpentHours(@issue.total_spent_hours)
  end

  if Redmine::Plugin.installed?(:lychee_kanban)
    api.lychee_issue_board_issue_option_attributes_blocking !!@issue.lychee_issue_board_issue_option&.blocking
    api.lychee_issue_board_issue_option_attributes_point @issue.lychee_issue_board_issue_option&.point
  end

  api.array :customFieldValues do
    @issue.visible_custom_field_values.each do |custom_value|
      attrs = {:id => custom_value.custom_field_id, :name => custom_value.custom_field.name}
      attrs[:multiple] = true if custom_value.custom_field.multiple?
      api.customFieldValues attrs do
        if custom_value.value.is_a?(Array)
          api.array :value do
            custom_value.value.each do |value|
              api.value value unless value.blank?
            end
          end
        else
          if custom_value.custom_field.field_format === 'attachment'
            _value = {
              type: 'attachment',
              filename: Attachment.find_by_id(custom_value.value)&.filename,
              value: custom_value&.value,
              token: Attachment.find_by_id(custom_value.value)&.token,
            }
            api.value _value
          else
            api.value custom_value.value
          end
        end
      end
    end
  end unless @issue.visible_custom_field_values.empty?

  api.createdOn @issue.created_on
  api.updatedOn @issue.updated_on
  api.closedOn @issue.closed_on

  render_api_issue_children(@issue, api) if include_in_api_response?('children')

  api.array :attachments do
    @issue.attachments.each do |attachment|
      render_api_attachment(attachment, api)
    end
  end # if include_in_api_response?('attachments')

  api.array :relations do
    @relations.each do |relation|
      api.relation(:id => relation.id, :issue_id => relation.issue_from_id, :issue_to_id => relation.issue_to_id, :relation_type => relation.relation_type, :delay => relation.delay)
    end
  end if include_in_api_response?('relations') && @relations.present?

  api.array :changesets do
    @changesets.each do |changeset|
      api.changeset :revision => changeset.revision do
        api.user(:id => changeset.user_id, :name => changeset.user.name) unless changeset.user.nil?
        api.comments changeset.comments
        api.committedOn changeset.committed_on
      end
    end
  end if include_in_api_response?('changesets')

  api.array :journals do
    @journals.each do |journal|
      api.journal :id => journal.id do
        api.user(:id => journal.user_id, :name => journal.user.name) unless journal.user.nil?
        api.notes journal.notes
        api.createdOn journal.created_on
        api.privateNotes journal.private_notes
        api.array :details do
          journal.visible_details.each do |detail|
            api.detail :property => detail.property, :name => detail.prop_key do
              api.oldValue detail.old_value
              api.newValue detail.value
            end
          end
        end
      end
    end
  end if include_in_api_response?('journals')

  api.array :watchers do
    @issue.watcher_users.each do |user|
      api.user :id => user.id, :name => user.name
    end
  end if include_in_api_response?('watchers') && User.current.allowed_to?(:view_issue_watchers, @issue.project)

  api.array :allowed_statuses do
    @allowed_statuses.each do |status|
      api.statusId status.id
    end
  end if include_in_api_response?('allowed_statuses')
end
