# frozen_string_literal: true

namespace :redmine do
  namespace :plugins do
    namespace :lychee_issue_form do
      desc 'LIFで更新した未送信のJournalをメール送信する'
      task send_notification_for_journals: :environment do
        Journal.send_notification_for_lychee_issue_form_journals
      end
    end
  end
end
