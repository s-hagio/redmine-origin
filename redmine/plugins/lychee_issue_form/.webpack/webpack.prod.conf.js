const { merge } = require('webpack-merge')
const path = require('path')
const baseWebpackConfig = require('./webpack.base.conf')

process.env.BABEL_ENV = 'production'
process.env.NODE_ENV = 'production'

module.exports = merge(baseWebpackConfig, {
  entry: [path.resolve(__dirname, '../src/main.jsx')],
  output: {
    path: path.resolve(__dirname, '../assets/javascripts'),
    filename: 'bundle.js',
  },
  mode: 'production',
})
