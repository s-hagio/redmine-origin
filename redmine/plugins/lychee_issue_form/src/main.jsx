import { Application } from '@hotwired/stimulus'

import IssueController from './controllers/issue_controller'
import PopupController from './controllers/popup_controller'
import ResizeController from './controllers/resize_controller'
import TabController from './controllers/tab_controller'
import IssueItemController from './controllers/issue_item_controller'
import AttachmentController from './controllers/attachment_controller'
import CommentController from './controllers/comment_controller'
import ModeController from './controllers/mode_controller'
import TimeEntryController from './controllers/time_entry_controller'
import IssueOptionController from './controllers/issue_option_controller'

window.Stimulus = Application.start()
Stimulus.register('issue', IssueController)
Stimulus.register('popup', PopupController)
Stimulus.register('resize', ResizeController)
Stimulus.register('tab', TabController)
Stimulus.register('issue_item', IssueItemController)
Stimulus.register('attachment', AttachmentController)
Stimulus.register('comment', CommentController)
Stimulus.register('mode', ModeController)
Stimulus.register('time_entry', TimeEntryController)
Stimulus.register('issue_option', IssueOptionController)
