import { Controller } from '@hotwired/stimulus'
/*
 *  Lychee Issue Form
 *  ポップアップ表示コントロール
 */
export default class extends Controller {
  static targets = ['help', 'ticketContext', 'journalContext', 'menu']

  connect() {
    // インスタンス変数
    this.currentJournalContextValue = null // 現在どのコメントのコンテキストメニューを開いているか
  }

  getCompatibleIssuesUrl() {
    return document.getElementById('request-url').dataset.fixedIssuesUrl
  }

  // NOTE: 子/関連チケットのヘルプ切り替え
  toggleHelpContent(event) {
    const target = event.currentTarget
    // 他popupを非表示
    this.ticketContextTargets.forEach(ticketContextTarget => {
      ticketContextTarget.classList.add('hideView')
    })
    this.menuTarget.classList.add('hideView')

    this.helpTargets.forEach(helpTarget => {
      if (helpTarget.id === `${target.id}-content`) {
        helpTarget.classList.toggle('hideView')
      } else {
        helpTarget.classList.add('hideView')
      }
    })
  }

  // NOTE: 子/関連チケットのコンテキストメニュー切り替え
  toggleTicketContextContent(event) {
    const target = event.currentTarget
    // 他popupを非表示
    this.helpTargets.forEach(helpTarget => {
      helpTarget.classList.add('hideView')
    })
    this.menuTarget.classList.add('hideView')

    this.ticketContextTargets.forEach(ticketContextTarget => {
      if (ticketContextTarget.id === `${target.id}-content`) {
        ticketContextTarget.classList.toggle('hideView')
      } else {
        ticketContextTarget.classList.add('hideView')
      }
    })
  }

  // NOTE: コメント/履歴のコンテキストメニュー切り替え
  toggleJournalContextContent(event) {
    const target = event.currentTarget
    // 他popupを非表示
    this.helpTargets.forEach(helpTarget => {
      helpTarget.classList.add('hideView')
    })
    this.menuTarget.classList.add('hideView')

    this.journalContextTargets.forEach(journalContextTarget => {
      if (journalContextTarget.id === `${target.id}-content`) {
        journalContextTarget.classList.toggle('hideView')
      } else {
        journalContextTarget.classList.add('hideView')
      }
    })

    // 現在開いているコンテキストメニューを記憶
    this.currentJournalContextValue = target.id
  }

  // NOTE: コメント/履歴のコンテキストメニューを非表示にする
  hideOtherJournalContextContent(event) {
    const target = event.target
      .closest('.journal-item')
      .querySelector('.journal-context')
    // 現在コンテキストメニューを表示している領域からフォーカスが外れたら非表示にする
    if (this.currentJournalContextValue != target.id) {
      this.journalContextTargets.forEach(journalContextTarget => {
        journalContextTarget.classList.add('hideView')
      })
      this.currentJournalContextValue = null
    }
  }

  // NOTE: フッタのメニュー切り替え
  toggleMenuContent(event) {
    // 他popupを非表示
    this.helpTargets.forEach(helpTarget => {
      helpTarget.classList.add('hideView')
    })
    this.ticketContextTargets.forEach(ticketContextTarget => {
      ticketContextTarget.classList.add('hideView')
    })

    this.menuTarget.classList.toggle('hideView')
  }
}
