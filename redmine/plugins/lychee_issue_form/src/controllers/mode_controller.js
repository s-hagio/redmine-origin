import { Controller } from '@hotwired/stimulus'
import { get } from '@rails/request.js'

/*
 *  Lychee Issue Form
 *  チケット項目コントロール: 項目外クリック時に編集モードリセット
 */
export default class extends Controller {
  connect() {
    this.element[this.identifier] = this
  }

  getCompatibleIssuesUrl() {
    return document.getElementById('request-url').dataset.fixedIssuesUrl
  }

  // ViewMode(Full/Half) を取得
  getViewMode() {
    if (!document.getElementById('viewFull')) return 'viewHalf'

    return !Array.from(document.getElementById('viewFull').classList).includes(
      'hideView',
    )
      ? 'viewFull'
      : 'viewHalf'
  }

  // 選択中のタブIDを取得する
  getSelectTabId() {
    return Array.from(
      document.querySelectorAll(`#${this.getViewMode()} #tab-buttons button`),
    ).find(el => el.tabIndex === 0).id
  }

  // 引数を元に表示モードを設定する //TODO: コメントタブ内のジャーナル等の表示モードも判定必要
  setViewMode(viewMode, selectTabId, journalMode) {
    const selectTabContentId = selectTabId.replace('btn', 'tab')

    if (viewMode === 'viewFull') {
      document.getElementById('viewFull').classList.remove('hideView')
      document.getElementById('viewHalf').classList.add('hideView')
      document.querySelectorAll('#viewFull #tab-buttons button').forEach(el => {
        el.id === selectTabId ? (el.tabIndex = 0) : (el.tabIndex = -1)
      })
      document.querySelectorAll('#viewFull .lif-tab-content').forEach(el => {
        el.id === selectTabContentId
          ? el.classList.remove('hideView')
          : el.classList.add('hideView')
      })
      document.querySelectorAll('#viewHalf #tab-buttons button').forEach(el => {
        el.tabIndex = -1
      })
      document.querySelectorAll('#viewHalf .lif-tab-content').forEach(el => {
        el.classList.add('hideView')
      })
      document.getElementById('dialog-footer').classList.add('__fullsize')
      document.getElementById('dialog-footer').classList.remove('__halfsize')
    } else if (viewMode === 'viewHalf') {
      document.getElementById('viewFull').classList.add('hideView')
      document.getElementById('viewHalf').classList.remove('hideView')
      document.querySelectorAll('#viewFull #tab-buttons button').forEach(el => {
        el.tabIndex = -1
      })
      document.querySelectorAll('#viewFull .lif-tab-content').forEach(el => {
        el.classList.add('hideView')
      })
      document.querySelectorAll('#viewHalf #tab-buttons button').forEach(el => {
        el.id === selectTabId ? (el.tabIndex = 0) : (el.tabIndex = -1)
      })
      document.querySelectorAll('#viewHalf .lif-tab-content').forEach(el => {
        el.id === selectTabContentId
          ? el.classList.remove('hideView')
          : el.classList.add('hideView')
      })
      document.getElementById('dialog-footer').classList.remove('__fullsize')
      document.getElementById('dialog-footer').classList.add('__halfsize')
    } else {
      document.getElementById('dialog-footer').classList.add('__fullsize')
      document.getElementById('dialog-footer').classList.remove('__halfsize')
      document.getElementById('viewHalf').classList.remove('hideView')
      document.querySelectorAll('#viewHalf #tab-buttons button').tabIndex = 0
      document
        .querySelector('#viewHalf .lif-tab-content')
        .classList.remove('hideView')
      document.getElementById('dialog-footer').classList.remove('__fullsize')
      document.getElementById('dialog-footer').classList.add('__halfsize')
      return
    }

    this.setJournalMode(journalMode)
  }

  // ジャーナルの表示モード（all/comments/journals）の取得
  getJournalMode() {
    const el = document.querySelector(`#${this.getViewMode()} .journals-filter`)
    return el.value
  }

  hideCommentInputWhenJournalsMode(mode) {
    if (mode === 'journals') {
      document.querySelectorAll('#comment-input').forEach(el => {
        el.querySelector('.edit textarea').value = null
        el.querySelector('.edit').classList.add('hideView')
        el.querySelector('.notify').classList.remove('hideView')
        el.classList.add('hideView')
      })
    } else {
      document.querySelectorAll('#comment-input').forEach(el => {
        el.classList.remove('hideView')
      })
    }
  }

  // ジャーナルの表示モード（all/comments/journals）の設定
  setJournalMode(journalMode) {
    if (!journalMode) return

    const mode = journalMode
    document.querySelectorAll('.filter-sections').forEach(filterSection => {
      if (filterSection.id === `filter-${mode}`) {
        filterSection.classList.remove('hideView')
        document.querySelectorAll(`.journals-filter`).forEach(el => {
          el.value = mode
        })

        this.hideCommentInputWhenJournalsMode(mode)
      } else {
        filterSection.classList.add('hideView')
      }
    })
  }

  // select コントロールの change イベントによりコメント/履歴タブの表示モード（all/comments/journals）を変更する
  changeJournalMode(event) {
    const mode = event.target.value

    document.querySelectorAll('.filter-sections').forEach(filterSection => {
      if (filterSection.id !== `filter-${mode}`) {
        filterSection.classList.add('hideView')

        this.hideCommentInputWhenJournalsMode(mode)
      } else {
        filterSection.classList.remove('hideView')
        filterSection.parentElement.querySelector('.journals-filter').value =
          mode
      }
    })

    // 編集中のため隠れているコンテキストメニューはすべて一度表示させる
    const contextMenus = document.querySelectorAll('.context-menus')
    contextMenus.forEach(contextMenu =>
      contextMenu.classList.remove('hidden-context-menus'),
    )
  }

  async fetchFormData(selectTabId) {
    const viewMode = this.getViewMode()
    const issueId = document.getElementById('issue-id').innerText
    const journalMode = this.getJournalMode()

    const response = await get(`${this.getCompatibleIssuesUrl()}/${issueId}`)

    if (response.ok) {
      const dialogContent = document.getElementById('dialog-content')

      dialogContent.innerHTML = await response.text
      this.setViewMode(viewMode, selectTabId, journalMode)
    } else {
      // TODO: 失敗時の処理
    }
  }
}
