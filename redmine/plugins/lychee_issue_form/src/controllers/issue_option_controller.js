import { Controller } from "@hotwired/stimulus"
import { get, patch } from "@rails/request.js"
/*
 *  Lychee Issue Form
 *  ポップアップ表示コントロール
 */

// 差分比較用のフォーム初期値
let timeLogFormInitData = {}

export default class extends Controller {
  initialize() {
    this.storeTimeLogFormData(timeLogFormInitData)
  }

  appendFormDataValues(obj, formData) {
    formData.forEach((value, key) => {
      // もし同じキーが既に存在する場合、値を配列に追加（selectタグのmultiple対応）
      if (obj[key]) {
        if (!Array.isArray(obj[key])) {
          obj[key] = [obj[key]]; // 既存の値を配列に変換
        }
        obj[key].push(value);
      } else {
        obj[key] = value;
      }
    })
  }

  storeFormData(obj, form) {
    if (Object.keys(obj).length || !form) return

    const formData = new FormData(form)
    this.appendFormDataValues(obj, formData)

    // formがnestされている場合の対応（CFのFileの必須の場合）
    const nestedForms = form.querySelectorAll('form')
    nestedForms.forEach((nestedForm) => {
      const nestedFormData = new FormData(nestedForm)
      this.appendFormDataValues(obj, nestedFormData)
    })
  }

  storeTimeLogFormData(obj) {
    const viewMode = this.getViewMode()
    const form = document.querySelector(`#${viewMode} [name="timelogForm"]`);

    this.storeFormData(obj, form)
  }

  confirmIfFormChanged() {
    if (this.hasFormChanged()) {
      return this.showConfirmCloseMessage()
    }

    return true
  }

  showConfirmCloseMessage() {
    const message = document.getElementById('issue-id').dataset.closeConfirmMessage
    return confirm(message)
  }

  hasFormChanged() {
    // [バリデーションエラー]
    const validationError = document.querySelector('.lif_validation_error_message')
    if (validationError) return true

    // [編集時]
    const viewMode = this.getViewMode()

    // プロパティタブ（現在のモードの要素で編集中の添付ファイルがあるか, update前の添付ファイルがあるかを検証）
    const fileElements = Array.from(document.querySelectorAll(`#${viewMode} #tab-content-files .edit.file-info`))
    const hasChangedFiles = !!fileElements.find(el => !el.hasAttribute('hidden'))

    const addingFileElements = document.querySelector(`#${viewMode} #add-files-name`);
    const hasUnupdatedFile = !!addingFileElements?.children.length;

    // コメント/履歴タブ（現在のモードの要素で編集中があるかを検証）
    const noteElements = Array.from(document.querySelectorAll(`#${viewMode} #tab-comments_and_journals .edit`));
    const hasChangedNotes = !!noteElements.find(el => !el.classList.contains('hideView'));

    // 作業時間タブ（FormDataの初期値と現在値の差分があるかを検証）
    let timelogFormCurrentData = {}
    this.storeTimeLogFormData(timelogFormCurrentData)

    const hasChangedTimeLog = this.hasChanged(timeLogFormInitData, timelogFormCurrentData)
    return hasChangedFiles || hasUnupdatedFile || hasChangedNotes || hasChangedTimeLog
  }

  // 2つのobjを比較し、差分があるかを取得
  hasChanged (a, b) {
    if (a === b) return false;

    const aKeys = Object.keys(a).sort();
    const bKeys = Object.keys(b).sort();

    if (aKeys.toString() !== bKeys.toString()) return true;

    // aとbがすべて一致するときは-1
    const wrongIndex = aKeys.findIndex((key) => {
      // NOTE: 配列のとき（複数選択可）は、文字列化して比較
      if (Array.isArray(a[key]) && Array.isArray(b[key])) {
        if (JSON.stringify(a[key]) !== JSON.stringify(b[key])) return true;
      } else if (key === 'resume' || key.includes('file')) {
        // NOTE: FileオブジェクトのlastModifiedはオブジェクトごとに変わるので、添付ファイルのみnameとsizeで比較
        return (
          a[key]['name'] !== b[key]['name'] && a[key]['size'] !== b[key]['size']
        );
      } else if (a[key] !== b[key]) {
        return true;
      }
      return false;
    });

    return wrongIndex !== -1;
  };

  getCompatibleIssuesUrl() {
    return document.getElementById('request-url').dataset.fixedIssuesUrl
  }

  async handleChangePrivate(event) {
    if (!this.confirmIfFormChanged()) return

    const viewMode = this.getViewMode()
    const selectTabId = this.element['mode'].getSelectTabId()
    const journalMode = this.element['mode'].getJournalMode()
    const isPrivate = this.element.getAttribute("data-is-private") === 'true'

    const issueId = document.getElementById('issue-id').innerText
    const response = await patch(`${this.getCompatibleIssuesUrl()}/${issueId}`, { body: {
      issue: {
        is_private: !isPrivate
      }
    }})

    if (response.ok) {

      // データ再取得
      this.fetchFormData(viewMode, selectTabId, journalMode)
    }
  }

  getViewMode() {
    if (!document.getElementById('viewFull')) return 'viewHalf'

    return !Array.from(document.getElementById('viewFull').classList).includes('hideView') ? 'viewFull' : 'viewHalf'
  }

  // 自身のチケットデータを取得する
  async fetchFormData(viewMode, selectTabId, journalMode) {
    const issueId = document.getElementById('issue-id').innerText
    const response = await get(`${this.getCompatibleIssuesUrl()}/${issueId}`)

    if (response.ok) {
      const dialogContent = document.getElementById('dialog-content')

      dialogContent.innerHTML = await response.text
      this.element['mode'].setViewMode(viewMode, selectTabId, journalMode)
      this.dispatch('setRadioControlChecked')
    }
  }
}
