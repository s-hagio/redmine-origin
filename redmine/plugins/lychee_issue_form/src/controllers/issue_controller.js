import fetch from 'isomorphic-fetch'
import { Controller } from '@hotwired/stimulus'
import { get, post, patch } from '@rails/request.js'

const CURRENT_PATH = location.pathname
const FIXED_PATH = '/lychee_issue_form/issues'
const ONLY_HALF_VIEW_MODE = true
const ENABLE_FORM_WIDTH_RESIZE = false
const SEQUENTIAL_ISSUES_CREATION_TIMEOUT_MILLISECONDS = 1000

// NOTE: カスタムイベント
const CE_LIF_BEFORE_SUBMIT = 'lifBeforeSubmit'
const CE_LIF_SUCCESS_SAVE = 'lifSuccessSave'

// NOTE: Issue Form を開くイベントを割り当てた識別のためのクラス
const CLICK_EVENT_CLASS = 'issue-form-click-event-attached'

// NOTE: URL 一覧
const LIF_COMPATIBLE_PATHS = {
  ORIGIN_ROADMAP_PATH: /\/projects\/(.*)\/roadmap$/,
  ORIGIN_ISSUES_PATH: /\/projects\/(.*)\/issues$|\/issues$/,
  ORIGIN_LGC_PATH: /\/projects\/(.*)\/lgc$/,
  ORIGIN_LYCHEE_GANTT_PATH: /\/projects\/(.*)\/lychee_gantt$|\/lychee_gantt$/,
  ORIGIN_KANBAN_PATH: /\/projects\/(.*)\/kanban(.*)$/,
  ORIGIN_BACKLOG_PATH: /\/projects\/(.*)\/backlog$/,
  ORIGIN_PROJECT_REPORT_PATH: /\/projects\/(.*)\/project_report$/,
  ORIGIN_TIME_MANAGEMENT_PATH: /\/time_management$/,
}

function replacePath(regex, replacement) {
  return CURRENT_PATH.replace(regex, replacement)
}

function getCompatibleUrl() {
  const originPath = Object.keys(LIF_COMPATIBLE_PATHS).find(key =>
    LIF_COMPATIBLE_PATHS[key].test(CURRENT_PATH),
  )
  return originPath
    ? replacePath(LIF_COMPATIBLE_PATHS[originPath], FIXED_PATH)
    : null
}

// NOTE: 「チケット編集モード」で開く際の対応するページとそのセレクタの組み合わせ
const EVENT_ATTACH_TARGETS_EDIT = [
  // ロードマップページの「題名」列と「#{id}」と箇所
  // { jsBased: false, targetPage: ORIGIN_ROADMAP_PATH, selectors: ['table.related-issues td.subject'] },
  // チケットページの「題名」列と「#{id}」と箇所
  // { jsBased: false, targetPage: ORIGIN_ISSUES_PATH, selectors: ['table.issues td.subject', 'table.issues td.id'] },
  // Lychee gantt chart（現行版）のツリーのチケット詳細リンク
  // { jsBased: true, targetPage: ORIGIN_LGC_PATH, selectors: ['div.lgc-gantt a.lgc-tree-label'] },
  // Lychee gantt chart（式年遷宮版）のツリーのチケット詳細リンク
  // { jsBased: true, targetPage: ORIGIN_LYCHEE_GANTT_PATH, selectors: ['span[class*="lgc-Cell-"] a'] },
  // { jsBased: true, targetPage: ORIGIN_LYCHEE_GANTT_PATH, selectors: [] }, // 式年遷宮版は global object 経由で LIF を開く
  // 現行カンバンの優先順の「#{id}」箇所
  // { jsBased: true, targetPage: ORIGIN_KANBAN_PATH, selectors: ['div.subject a'] },
  // 現行カンバンの担当者別の「#{id}」箇所
  // { jsBased: true, targetPage: ORIGIN_KANBAN_PATH, selectors: ['div.subject a'] },
  // バックログの「#{id}」箇所
  // { jsBased: true, targetPage: ORIGIN_BACKLOG_PATH, selectors: ['div#backlog-version-list div.subject a', 'div.subject a'] },
  // プロジェクトレポートのチケットパネルの「題名」リンク
  // { jsBased: true, targetPage: ORIGIN_PROJECT_REPORT_PATH, selectors: ['div.td span a'] },
  // タイムマネジメント（カレンダー形式）の「題名」リンク
  // TODO:（作業実績）の「#」リンク ※固定のクラス名ではない
  // { jsBased: true, targetPage: ORIGIN_TIME_MANAGEMENT_PATH, selectors: ['div.rbc-event-content a', 'div.sc-Rmtcm.cnGujc a'] },
]

// NOTE: 「チケット編集モード」で開く際の対応するページとそのセレクタの組み合わせ
const EVENT_ATTACH_TARGETS_NEW = [
  // チケット一覧ページの右上「新しいチケット」リンクの箇所
  // { jsBased: false, targetPage: ORIGIN_ISSUES_PATH, selectors: ['a.new-issue'], mode: 'new' },
]

// 差分比較用のフォーム初期値
let newFormInitData = {}
let timeLogFormInitData = {}

let onClose = null

/*
 *  Lychee Issue Form
 *  フォーム表示コントロール（フォーム表示タイミングにおける制御）
 */
export default class extends Controller {
  static targets = [
    'hideable',
    'issueFormContainer',
    'viewFull',
    'viewHalf',
    'footer',
    'item',
    'comment',
  ]
  static values = {
    lifParam: Object,
    lifExistIssueId: Number,
  }

  initialize() {
    // カンバン・バックログで一時的にLIFを無効化する
    if(this.isKanbanOrBacklogPage() && this.isUsingKanbanDialog()) return

    this.storeNewFormData(newFormInitData)
    this.storeTimeLogFormData(timeLogFormInitData)

    this.showLycheeKanbanOptions()
    // 1st リリース対象でない箇所を非表示
    this.hideTargetsAfter2ndRelease()

    // Global object
    if (window.lif == null) {
      const parent = this
      window.lif = {
        open: async function (param, error) {
          // チケット編集中の場合は、確認ダイアログを表示
          if (!parent.confirmIfFormChanged(param.issue)) return

          parent.lifParamValue = param
          parent.lifExistIssueId = param.id
          parent.extractOnClose(param)
          const compatibleUrl = getCompatibleUrl()

          if (!!param.id) {
            // id がある場合は既存チケット
            if (!!param.issue) {
              // issue: {...optionalParams} を含む場合は全編集モードで開く
              const issue = await parent.fetchIssueData(param.id, compatibleUrl)
              await parent.fetchNewIssueHtml(compatibleUrl, {
                ...param.issue,
                issueId: param.id,
              })
              document.getElementById('open-mode-meta').innerText = 'edit-all'
              parent.switchEditAllOrNot(false)
              const formData = { issue: issue, optionalParam: param.issue }
              await parent.setFormDataAndSaveIssue(formData, compatibleUrl)
            } else {
              // optionalParams を含まない場合は閲覧モードで開く
              await parent.fetchIssueHtml(`issues/${param.id}`, compatibleUrl)
              parent.showTarget(false)
            }
          } else {
            const formData = convertKeysToCamelCase(param)

            await parent.fetchNewIssueHtml(compatibleUrl, formData)
            parent.switchEditAllOrNot(true)

            if (!error) {
              // 新規フォームを表示
              parent.showForm(true, formData)
            } else {
              // エラーリカバリーフォームを表示
              parent.showRecoveryForm(formData)
            }
          }
          document.documentElement.style.overflowX = 'auto'
        },
        onBeforeSubmit: function (callback) {
          if (!parent.isEventListenerExists(CE_LIF_BEFORE_SUBMIT)) {
            document.addEventListener(CE_LIF_BEFORE_SUBMIT, e => callback(e))
            parent.setEventListenerExists(CE_LIF_BEFORE_SUBMIT)
          }
        },
        onSuccessSave: function (callback) {
          if (!parent.isEventListenerExists(CE_LIF_SUCCESS_SAVE)) {
            document.addEventListener(CE_LIF_SUCCESS_SAVE, e => callback(e))
            parent.setEventListenerExists(CE_LIF_SUCCESS_SAVE)
          }
        },
      }
    }
  }

  isKanbanOrBacklogPage() {
    return (
      LIF_COMPATIBLE_PATHS.ORIGIN_KANBAN_PATH.test(CURRENT_PATH) ||
      LIF_COMPATIBLE_PATHS.ORIGIN_BACKLOG_PATH.test(CURRENT_PATH)
    )
  }

  isUsingKanbanDialog() {
    return document.getElementById('is_using_kanban_dialog').innerText === 'true'
  }

  setEventListenerExists(eventName) {
    document.body.setAttribute(`customevent-${eventName.toLowerCase()}`, true)
  }

  isEventListenerExists(eventName) {
    return document.body.getAttribute(`customevent-${eventName.toLowerCase()}`)
  }

  connect() {
    this.replaceProcesses(EVENT_ATTACH_TARGETS_EDIT, false)
    this.replaceProcesses(EVENT_ATTACH_TARGETS_NEW, true)
    window.addEventListener('beforeunload', this.handleBeforeUnload.bind(this))
  }

  disconnect() {
    this.undoProcesses(EVENT_ATTACH_TARGETS_EDIT)
    this.undoProcesses(EVENT_ATTACH_TARGETS_NEW)
    window.removeEventListener(
      'beforeunload',
      this.handleBeforeUnload.bind(this),
    )
  }

  handleBeforeUnload(event) {
    if (!this.isFormVisible()) return

    if (this.hasFormChanged()) {
      event.preventDefault()
      // NOTE: returnValueをダイアログに表示できないが、文字列を入れておかないとchromeではエラーになる可能性あり
      event.returnValue = 'このページを離れますか？'
    }
  }

  appendFormDataValues(obj, formData) {
    formData.forEach((value, key) => {
      // もし同じキーが既に存在する場合、値を配列に追加（selectタグのmultiple対応）
      if (obj[key]) {
        if (!Array.isArray(obj[key])) {
          obj[key] = [obj[key]] // 既存の値を配列に変換
        }
        obj[key].push(value)
      } else {
        obj[key] = value
      }
    })
  }

  storeFormData(obj, form) {
    if (Object.keys(obj).length || !form) return

    const formData = new FormData(form)
    this.appendFormDataValues(obj, formData)

    // formがnestされている場合の対応（CFのFileの必須の場合）
    const nestedForms = form.querySelectorAll('form')
    nestedForms.forEach(nestedForm => {
      const nestedFormData = new FormData(nestedForm)
      this.appendFormDataValues(obj, nestedFormData)
    })

    // 新規フォームのプライベートチェックのデータを追加
    if (this.isNewOpenMode()) {
      const isPrivate = document.getElementById('issue_is_private')?.checked
      obj['issue[is_private]'] = isPrivate
    }
  }

  storeNewFormData(obj) {
    const form = document.getElementById('new')

    this.storeFormData(obj, form)
  }

  storeTimeLogFormData(obj) {
    const viewMode = this.getViewMode()
    const form = document.querySelector(`#${viewMode} [name="timelogForm"]`)

    this.storeFormData(obj, form)
  }

  // TODO: 2nd リリース以降で必要により非表示から復活させる
  hideTargetsAfter2ndRelease() {
    // 「子/関連チケット」タブの切り替えボタンを非表示
    document
      .querySelectorAll('#btn-descendants')
      ?.forEach(el => el.setAttribute('style', 'display:none'))
    // フッターエリアの「ウォッチ」ボタンを非表示
    document.getElementById('watch')?.setAttribute('style', 'display:none')
    // フッターエリアの三点リーダー内メニューのプライベートチケット切り替え以外を非表示
    Array.from(document.querySelectorAll('#popup li'))
      .filter(li => !li.className.includes('toggle-private'))
      .forEach(li => li.setAttribute('style', 'display:none'))
  }

  showLycheeKanbanOptions() {
    const pathIdentifier = Object.keys(LIF_COMPATIBLE_PATHS).find(key =>
      LIF_COMPATIBLE_PATHS[key].test(CURRENT_PATH),
    )
    if (!pathIdentifier) return

    const elementsBlocking = document.querySelectorAll(
      '.kanban-option-blocking',
    )
    const elementsPoint = document.querySelectorAll('.kanban-option-point')

    if (pathIdentifier.includes('KANBAN') || pathIdentifier.includes('BACKLOG'))
      elementsBlocking.forEach(el => el.classList.remove('hideView'))
    if (pathIdentifier.includes('BACKLOG') && this.isBacklogPointEnabled())
      elementsPoint.forEach(el => el.classList.remove('hideView'))
  }

  isBacklogPointEnabled() {
    const redmineState =
      document.getElementById('target-content')?.dataset?.redmineState
    if (!redmineState) return false

    const backlogPointEnabled = JSON.parse(redmineState).backlog_point_enabled
    return !!backlogPointEnabled
  }

  replaceProcesses(targets, isNewIssue) {
    // NOTE: MutationObserver で動的に追加される DOM を捕捉しイベントを実行する
    const eventAttachTarget = this.isExistOtherJSBasedPlugin(targets)
    if (!!eventAttachTarget) {
      // NOTE: 新カンバンなど React などで作られたプラグインの場合
      const targetNode = document.getElementById('wrapper')
      const observer = new MutationObserver((mutationList, observer) => {
        for (const mutation of mutationList) {
          if (mutation.type === 'childList') {
            // NOTE: 追加された node の配下のみ対象
            this.dispatchClickEvent(
              eventAttachTarget,
              isNewIssue,
              mutation.target,
            )
          }
        }
      })
      const config = { childList: true, subtree: true }
      observer.observe(targetNode, config)
    } else {
      // NOTE: Redmine 標準（≒ HTML View ベース）の場合
      // NOTE: 指定したセレクタのクリックイベントを乗っ取る
      targets.forEach(eventAttachTarget => {
        this.dispatchClickEvent(eventAttachTarget, isNewIssue)
      })
    }
  }

  undoProcesses(targets) {
    // NOTE: 画面遷移時にイベントは破棄されるが作法として remove する
    targets.forEach(eventAttachTarget => {
      eventAttachTarget.selectors.forEach(selector => {
        document.querySelectorAll(selector).forEach(element => {
          const clickEvent = async event => {
            event.preventDefault()
            event.stopPropagation()
            await this.ticketClickEvent(
              event.target.href,
              eventAttachTarget.targetPage,
            )
          }
          element.removeEventListener('click', clickEvent, true)
        })
      })
    })
  }

  dispatchClickEvent(
    eventAttachTarget,
    isNewIssue = false,
    baseElement = null,
  ) {
    const element = baseElement || document

    eventAttachTarget.selectors.forEach(selector => {
      const clickEvent = async event => {
        if (
          !baseElement &&
          event.srcElement.closest(['#issue_tree', '#relations'])
        )
          return

        event.preventDefault()
        event.stopPropagation()
        const eventTargetHref = event?.target?.href
          ? event.target.href
          : this.parentElementHref(event.target)
        await this.ticketClickEvent(
          eventTargetHref,
          eventAttachTarget.targetPage,
          isNewIssue,
        )
      }

      element.querySelectorAll(selector).forEach(element => {
        // NOTE: 重複してアタッチしないようにする
        if (!element.classList.contains(CLICK_EVENT_CLASS)) {
          element.classList.add(CLICK_EVENT_CLASS)
          element.addEventListener('click', clickEvent, true)
        }
      })
    })
  }

  switchEditAllOrNot(hideEditAll) {
    if (hideEditAll) {
      document.querySelectorAll('.edit-all').forEach(e => (e.hidden = true))
      document
        .querySelectorAll('.non-edit-all')
        .forEach(e => (e.hidden = false))
    } else {
      document.querySelectorAll('.edit-all').forEach(e => (e.hidden = false))
      document.querySelectorAll('.non-edit-all').forEach(e => (e.hidden = true))
    }
  }

  parentElementHref(element) {
    // NOTE: 親要素の href を取得する
    return element.offsetParent ? element.offsetParent.href : null
  }

  isExistOtherJSBasedPlugin(targets) {
    // NOTE: 動的に追加される DOM を捕捉しイベントを実行する対象のプラグインかどうかを判定
    let result = targets.find(target => {
      return target.targetPage === FIXED_PATH
    })

    return result
  }

  // 表示状態における新規チケット作成かどうかの判定
  isNewOpenMode() {
    return document.getElementById('open-mode')?.innerText === 'new'
  }

  isAttributesEditable() {
    return JSON.parse(
      document.getElementById('is-attributes-editable').innerText,
    )
  }

  async ticketClickEvent(
    eventTargetHref,
    targetPage,
    modeCreateIssue = false,
    options = {},
  ) {
    // NOTE: 編集中があれば、確認ダイアログを表示
    if (eventTargetHref !== 'create' && !this.canCloseOrChangeForm()) return

    // 全編集モード時の編集中のissueId
    const editIssueId = document.getElementById('edit-issue-id')?.innerText
    if (editIssueId) options.issueId = editIssueId

    // NOTE: セレクタクリック時のイベント
    if (eventTargetHref) {
      // TODO: ローディングインジケータ的なものを挟むのであればここ
      const result = modeCreateIssue
        ? await this.fetchNewIssueHtml(targetPage, options)
        : await this.fetchIssueHtml(eventTargetHref, targetPage)

      if (result) {
        this.showForm(modeCreateIssue)
      }

      // 全編集モード時、トラッカー等を切り替えたとき、「作成」「連続作成」ボタンを表示されないように
      this.switchEditAllOrNot(!editIssueId)
    }
  }

  async eventOpenModal() {
    const viewMode = this.getViewMode()
    const element = document.querySelector(
      `#${viewMode} #lif_statuses_description`,
    )
    element.hidden = false
  }

  async eventCloseModal() {
    const viewMode = this.getViewMode()
    const element = document.querySelector(
      `#${viewMode} #lif_statuses_description`,
    )
    element.hidden = true
  }

  async eventSelectChange(event) {
    if (!this.isNewOpenMode()) {
      this.assignTodayToActualDates(event)
      return
    }

    let options = { form_update: `${event.target.id}_id` }
    options['repost_new'] = true

    if (event.target.id === 'issue_tracker')
      options['tracker_id'] = event.target.value
    else {
      const selectTracker = document.querySelector('#issue_tracker')
      options['tracker_id'] = selectTracker.selectedOptions[0].value
    }
    if (event.target.id === 'issue_status')
      options['status_id'] = event.target.value
    if (event.target.id === 'issue_project')
      options['project_id'] = event.target.value

    await this.ticketClickEvent('create', getCompatibleUrl(), true, options)

    this.assignTodayToActualDates(event)
  }

  populateFormData() {
    let formData = new FormData()

    formData.append(
      'issue[is_private]',
      document.querySelector('#issue_is_private')?.checked ? 1 : 0,
    )

    this.itemTargets.forEach(itemTarget => {
      const targetInputParent = itemTarget.querySelector('.edit')
      const fieldName = targetInputParent.getAttribute(
        'data-issue_item-field-name-value',
      )
      const targetInputChilds = targetInputParent.querySelectorAll(
        'select, input, textarea',
      )
      const fieldValue =
        targetInputChilds.length > 0
          ? this.getFieldValue(targetInputChilds)
          : ''
      // 複数選択セレクトボックス
      if (Array.isArray(fieldValue) && fieldValue.length > 0) {
        fieldValue.forEach(fv => {
          if (typeof fv === 'object') {
            // ファイルアップロード
            formData.append(`${fieldName}[1][token]`, fv.token)
          } else {
            formData.append(`${fieldName}[]`, fv)
          }
        })
      } else if (Array.isArray(fieldValue) && fieldValue.length === 0) {
        // 空配列の場合は "" をセットしないとバリデーションエラーにならない
        formData.append(fieldName, '')
      } else {
        formData.append(fieldName, fieldValue)
      }
    })

    // set default status per tracker
    const defaultStatus = document.querySelector('#issue-default-status').value
    const statusId = formData.get('issue[status_id]')
    if (statusId === defaultStatus) {
      formData.delete('issue[status_id]')
      formData.append('was_default_status', statusId)
    }

    return formData
  }

  async fetchIssueHtml(eventTargetHref, targetPage) {
    // NOTE: HTML を取ってくる
    let issueId
    if (typeof eventTargetHref === 'string') {
      const parsedUrl = eventTargetHref.split('/')
      issueId = parsedUrl[parsedUrl.length - 1]
    } else {
      issueId = document.getElementById('issue-id').innerText
    }
    // NOTE: View の送出元は app/controllers/lychee_issue_form/issues_controller.rb#show です
    const url = `${targetPage}/${issueId}.html`
    await fetch(url, {
      method: 'get',
      credentials: 'include',
      headers: this.makeHeaders({ 'Content-Type': 'text/html' }),
    })
      .then(response => response.text()) // NOTE: Promise {<pending>}
      .then(response => {
        const dialogContent = document.getElementById('dialog-content')
        // NOTE: app/views/lychee_issue_form/issues/show.html.erb を描画している
        dialogContent.innerHTML = response

        this.setHasValidationErrors('false')
      })

    return true
  }

  async fetchIssueData(issueId, targetPage) {
    // NOTE: JSON を取ってくる
    const response = await get(`${targetPage}/${issueId}.json`)
    const message = await response.text
    return JSON.parse(message)
  }

  async fetchNewIssueHtml(targetPage, options = {}) {
    const url = `${targetPage}/new`
    const formData = new FormData()
    if (options['form_update']) {
      // on select box change event
      formData.delete('form_update_triggered_by')
      formData.append('form_update_triggered_by', options['form_update'])
      for (const [key, value] of this.populateFormData()) {
        formData.append(key, value)
      }
    }

    const projectIdFromUrl = location.pathname.match(
      'projects/(.*)/lychee_gantt',
    )
    const projectIdentifier = projectIdFromUrl ? projectIdFromUrl[1] : ''
    formData.append('project_identifier', projectIdentifier)

    const projectId = options?.projectId
    if (projectId) formData.append('project_id', projectId)
    const trackerId = options?.trackerId
    if (trackerId) formData.append('tracker_id', trackerId)
    const statusId = options?.statusId
    if (statusId) formData.append('status_id', statusId)
    const issueId = options?.issueId
    if (issueId) formData.append('id', issueId)

    if (options['project_id']) {
      formData.append('issue[project_id]', options['project_id'])
    }
    if (options['tracker_id']) {
      formData.append('issue[tracker_id]', options['tracker_id'])
    }
    if (options['status_id']) {
      formData.append('issue[status_id]', options['status_id'])
    }
    if (options['customFieldValues']) {
      const customFieldValues = options['customFieldValues']
      customFieldValues.forEach(({ id, value }) => {
        formData.append(`issue[custom_field_values][${id}]`, value)
      })
    }

    if (this.isChecklistLocalMode()) {
      formData.append(
        'checklist_items',
        document.getElementById('__lychee_checklist_data').value,
      )
    }

    // ファイルが添付の要素の clone を取得
    const viewMode = this.getViewMode()
    const elAttachFiles = document.querySelector(`#${viewMode} .attachment_files`)
    const elementCloneAttachFiles = elAttachFiles?.cloneNode(true)

    const response = await fetch(url, {
      method: 'post',
      body: formData,
      credentials: 'include',
      headers: this.makeHeaders(),
    })

    const responseText = await response.text()
    if (response.ok) {
      const dialogContent = document.getElementById('dialog-content')
      dialogContent.innerHTML = responseText
      this.element['mode'].setViewMode('viewNew', 'btn-property', '')

      // 新規フォーム再取得時にファイル添付要素を clone 要素で置き換える
      if (options.repost_new && elementCloneAttachFiles) {
        const elAttachFiles = document.querySelector('.attachment_files')
        elAttachFiles.parentElement.insertBefore(
          elementCloneAttachFiles,
          elAttachFiles.nextElementSibling,
        )
        document.querySelectorAll('.attachment_files .btn-area button').forEach(el => el.hidden = true)
        elAttachFiles.remove()
      }

      return true
    } else if (response.status === 403) {
      this.showNotification(JSON.parse(responseText).errors[0])
      return false
    } else {
      JSON.parse(responseText).errors.forEach(e => {
        console.log(e)
      })
      return false
    }
  }

  makeHeaders(options = {}) {
    const domCsrfToken = document.getElementsByName('csrf-token')[0]
    const csrfToken = domCsrfToken && domCsrfToken.getAttribute('content')

    return {
      Accept: 'text/html',
      'X-CSRF-Token': csrfToken,
      ...options,
    }
  }

  setRadioControlChecked(isCreateIssue) {
    if (isCreateIssue) return

    const viewMode = Array.from(
      document.getElementById('viewFull').classList,
    ).includes('hideView')
      ? 'viewHalf'
      : 'viewFull'
    const otherViewMode = viewMode === 'viewFull' ? 'viewHalf' : 'viewFull'
    document
      .querySelectorAll(`#${otherViewMode} input[type="radio"]`)
      .forEach(el => el.setAttribute('disabled', 'disabled'))
    document
      .querySelectorAll(`#${viewMode} input[type="radio"]`)
      .forEach(el => el.setAttribute('enabled', 'enabled'))
    document
      .querySelectorAll(
        'input[type="radio"][checked="checked"][enabled="enabled"]',
      )
      .forEach(el => (el.checked = true))
  }

  showNotification(message) {
    const notification = document.querySelector('#notification')
    notification.classList.remove('fade-out')
    notification.querySelector('#notification-content').innerText = message
    // NOTE: classList.remove() の直後の classList.add() だと、連続で呼び出された際に
    //       2回目以降は classList は反映されるが CSSアニメーションに反映されないため、10[ms] 待機
    setTimeout(() => {
      notification.classList.add('fade-out')
    }, 10)
  }

  showFlashNotice(message) {
    const element = document.getElementById('flash-notice')
    element.classList.remove('fade-out-inline')
    document.getElementById('flash-notice-content').innerText = message
    setTimeout(() => {
      element.classList.add('fade-out-inline')
    }, 10)
  }

  showTarget(isCreateIssue) {
    // NOTE: Issue Form を表示する

    // TODO: 「通常表示モード」のみ利用対応（「分割表示モード」対応時に false 判定の箇所のみにする
    const issueFormSize = ONLY_HALF_VIEW_MODE
      ? 'half'
      : isCreateIssue
      ? 'half'
      : document.body.id || 'full'
    if (ONLY_HALF_VIEW_MODE)
      document.querySelector('.dialog-toggle-view').hidden = true
    if (!ENABLE_FORM_WIDTH_RESIZE)
      document.querySelector('.handle-outside').hidden = true

    document.body.classList.add(`lychee-issue-form__body_${issueFormSize}`)
    this.issueFormContainerTargets.forEach(target =>
      target.classList.add(`lychee-issue-form__container_${issueFormSize}`),
    )

    // TODO: Refactor
    if (issueFormSize === 'full') {
      this.viewHalfTargets.forEach(target => target.classList.add('hideView'))
      this.viewFullTargets.forEach(target =>
        target.classList.remove('hideView'),
      )
      this.footerTargets.forEach(target =>
        target.classList.remove('__halfsize'),
      )
      this.footerTargets.forEach(target => target.classList.add('__fullsize'))
      this.setRadioControlChecked(isCreateIssue)
    } else {
      this.viewHalfTargets.forEach(target =>
        target.classList.remove('hideView'),
      )
      this.viewFullTargets.forEach(target => target.classList.add('hideView'))
      this.footerTargets.forEach(target =>
        target.classList.remove('__fullsize'),
      )
      this.footerTargets.forEach(target => target.classList.add('__halfsize'))
      this.setRadioControlChecked(isCreateIssue)
    }
    this.hideableTargets.forEach(el => (el.hidden = false))
    document.querySelector(
      `.lychee-issue-form__container_${issueFormSize}`,
    ).hidden = false
    document.querySelector(
      `.lychee-issue-form__container_${issueFormSize} .content`,
    ).hidden = false

    // NOTE: 分割表示時に再考慮
    // if (this.issueFormContainerTargets[0] && this.issueFormContainerTargets[0].style.width.length != 0) {
    //  // NOTE: ノブで横幅が移動されているときはそれを引き継ぐ
    //  const offsetWidth = this.issueFormContainerTargets[0].offsetWidth
    //  this.footerTargets[0].style.width = offsetWidth + 'px'
    //  document.body.style.paddingRight = offsetWidth + 'px'
    // }

    if (document.body.style.paddingRight === '0px')
      // NOTE: paddingRight: 0px はスクロールを非表示にしてしまうため明示しない
      document.body.style.paddingRight = null

    // 新規モードの際のみに表示/非表示する項目の管理
    this.setNewOpenViewMode(isCreateIssue)
  }

  canCloseOrChangeForm(event) {
    /*
      NOTE: event['isConfirmation'] は issue_item_controller.js の clickOutside における確認ダイアログの結果を格納。
            clickOutside → hideTarget という順に処理され、ダイアログが2回表示されないようにするため
    */
    const isConfirmation = event['isConfirmation']
    // isConfirmationが undefinedの場合は、通常のバツボタンの動作になる
    if (isConfirmation !== undefined) return isConfirmation

    return this.confirmIfFormChanged()
  }

  confirmIfFormChanged(options = {}) {
    if (this.hasFormChanged(options)) {
      return this.showConfirmCloseMessage()
    }

    return true
  }

  showConfirmCloseMessage() {
    const message =
      document.getElementById('issue-id').dataset.closeConfirmMessage
    return confirm(message)
  }

  hideTarget(forceReset) {
    /*
      NOTE: forceReset は 新規作成時の作成ボタン押下時 → true
                          新規作成時・編集時のバツボタン押下時 → PointerEvent
    */
    if (
      forceReset instanceof PointerEvent &&
      !this.canCloseOrChangeForm(forceReset)
    )
      return

    // NOTE: Issue Form を非表示にする
    this.hideableTargets.forEach(el => (el.hidden = true))
    document.body.classList.remove(`lychee-issue-form__body_full`)
    document.body.classList.remove(`lychee-issue-form__body_half`)
    document.documentElement.style.removeProperty('overflow-x')
    if (this.isChecklistLocalMode()) {
      document.getElementById('__lychee_checklist_data').value = '[]'
    }
    if (forceReset) document.body.style.paddingRight = '0'
    this.setHasValidationErrors('false')
    this.executeOnClose()
  }

  isFormVisible() {
    const issueFormContainer = this.issueFormContainerTargets[0]
    return issueFormContainer ? !issueFormContainer?.hidden : false
  }

  hasFormChanged(options = {}) {
    if (!this.isFormVisible()) return

    // [バリデーションエラー]
    const validationError = document.querySelector(
      '.lif_validation_error_message',
    )
    if (validationError) return true

    // [新規作成時]
    if (this.isNewOpenMode()) {
      let newFormCurrentData = {}
      this.storeNewFormData(newFormCurrentData)

      return this.hasChanged(newFormInitData, newFormCurrentData)
    }
    // [編集時]
    const viewMode = this.getViewMode()

    // プロパティタブ（現在のモードの要素で編集中の添付ファイルがあるか, update前の添付ファイルがあるかを検証）
    const fileElements = Array.from(
      document.querySelectorAll(
        `#${viewMode} #tab-content-files .edit.file-info`,
      ),
    )
    const hasChangedFiles = !!fileElements.find(
      el => !el.hasAttribute('hidden'),
    )

    const addingFileElements = document.querySelector(
      `#${viewMode} #add-files-name`,
    )
    const hasUnupdatedFile = !!addingFileElements?.children.length && !options.repost_new

    // コメント/履歴タブ（現在のモードの要素で編集中があるかを検証）
    const noteElements = Array.from(
      document.querySelectorAll(
        `#${viewMode} #tab-comments_and_journals .edit`,
      ),
    )
    const hasChangedNotes = !!noteElements.find(
      el => !el.classList.contains('hideView'),
    )

    // 作業時間タブ（FormDataの初期値と現在値の差分があるかを検証）
    let timelogFormCurrentData = {}
    this.storeTimeLogFormData(timelogFormCurrentData)

    const hasChangedTimeLog = this.hasChanged(
      timeLogFormInitData,
      timelogFormCurrentData,
    )

    return (
      hasChangedFiles ||
      hasUnupdatedFile ||
      hasChangedNotes ||
      hasChangedTimeLog
    )
  }

  // ViewMode(Full/Half) を取得
  getViewMode() {
    if (!document.getElementById('viewFull')) return 'viewHalf'

    return !Array.from(document.getElementById('viewFull').classList).includes(
      'hideView',
    )
      ? 'viewFull'
      : 'viewHalf'
  }

  // 2つのobjを比較し、差分があるかを取得
  hasChanged(a, b) {
    if (a === b) return false

    const aKeys = Object.keys(a).sort()
    const bKeys = Object.keys(b).sort()

    if (aKeys.toString() !== bKeys.toString()) return true

    // aとbがすべて一致するときは-1
    const wrongIndex = aKeys.findIndex(key => {
      // NOTE: 配列のとき（複数選択可）は、文字列化して比較
      if (Array.isArray(a[key]) && Array.isArray(b[key])) {
        if (JSON.stringify(a[key]) !== JSON.stringify(b[key])) return true
      } else if (key === 'resume' || key.includes('file')) {
        // NOTE: FileオブジェクトのlastModifiedはオブジェクトごとに変わるので、添付ファイルのみnameとsizeで比較
        return (
          a[key]['name'] !== b[key]['name'] && a[key]['size'] !== b[key]['size']
        )
      } else if (a[key] !== b[key]) {
        return true
      }
      return false
    })

    return wrongIndex !== -1
  }

  toggleTarget() {
    this.hideableTargets.forEach(el => (el.hidden = !el.hidden))
  }

  resetEditMode() {
    // 新規作成モードは編集モードをリセットしない
    if (this.isNewOpenMode()) return

    this.itemTargets.forEach(el => {
      if (el.querySelector('.show')) el.querySelector('.show').hidden = false
      if (el.querySelector('.edit')) el.querySelector('.edit').hidden = true
      if (el.querySelector('.unit')) el.querySelector('.unit').hidden = false
    })
  }

  resetPrivateCommentCheckbox(selector) {
    const elements = document.querySelectorAll(`.${selector}`)

    elements.forEach(el => {
      if (el.querySelector('.journal_private_notes'))
        el.querySelector('.journal_private_notes').checked = false
    })
  }

  hideShow(event) {
    // 編集モードとならないケース
    // リンクの場合
    if (event.target.href) return

    // チケット編集権限がない場合
    if (!this.isAttributesEditable()) return

    // 編集不可の場合
    if (
      Array.from(event.target.closest('.issue-item').classList).includes(
        'not-editable',
      )
    )
      return

    // 編集モードでバリデーションエラーがある場合
    if (this.hasValidationErrors()) return

    this.resetEditMode()

    let el = event.currentTarget
    el.querySelector('.show').hidden = true
    el.querySelector('.edit').hidden = false
    if (el.querySelector('.unit')) el.querySelector('.unit').hidden = true
  }

  hasValidationErrors() {
    return (
      document
        .getElementById('dialog-content')
        .getAttribute('data-has-validation-errors') === 'true'
    )
  }

  setHasValidationErrors(value) {
    return document
      .getElementById('dialog-content')
      .setAttribute('data-has-validation-errors', value)
  }

  getProjectId() {
    return document.getElementById('project-identifier').innerHTML
  }

  // 新規チケット作成表示モード: 通常モードのみ表示でプロパティ項目以外のタブも非表示に制御
  setNewOpenViewMode(isCreateIssue) {
    document
      .querySelectorAll('span.tracker')
      .forEach(el => (el.hidden = isCreateIssue))
    document.querySelector('#tab-buttons button[role="half-tab"]').hidden =
      isCreateIssue
    document.querySelector('.dialog-toggle-view').hidden = isCreateIssue
    document
      .querySelectorAll('.issue-item .show')
      .forEach(el => (el.hidden = isCreateIssue))
    document
      .querySelectorAll('.issue-item .unit')
      .forEach(el => (el.hidden = isCreateIssue))
    document
      .querySelectorAll('.issue-item .edit')
      .forEach(el => (el.hidden = !isCreateIssue))
    // TODO: 「通常表示モード」のみ利用対応（「分割表示モード」対応時に false 判定の箇所のみにする
    ONLY_HALF_VIEW_MODE
      ? (document.querySelector('.dialog-toggle-view').hidden = true)
      : (document.querySelector('.dialog-toggle-view').hidden = isCreateIssue)
  }

  allResetCommentEditMode() {
    this.commentTargets.forEach(el => {
      el.querySelector('.show').hidden = false
      el.querySelector('.show').classList.remove('hideView')
      el.querySelector('.edit').hidden = true
      el.querySelector('.edit').classList.add('hideView')
      if (el.querySelector('.unit')) el.querySelector('.unit').hidden = false
    })
  }

  allToggleCommentContext() {
    const contextMenus = document.querySelectorAll(
      '.comment-input-edit .context-menus',
    )
    contextMenus.forEach(contextMenu =>
      contextMenu.classList.toggle('hidden-context-menus'),
    )
  }

  resetEditModeWithQuerySelector(event) {
    // NOTE: comment での show / edit 制御
    const elements = document.querySelectorAll(`.${event.params.selector}`)

    this.resetEditMode()
    this.allResetCommentEditMode()
    this.allToggleCommentContext()
    this.resetPrivateCommentCheckbox(event.params.selector)

    elements.forEach(el => {
      el.querySelector('.show').hidden = false
      el.querySelector('.show').classList.remove('hideView')
      el.querySelector('.edit').hidden = true
      el.querySelector('.edit').classList.add('hideView')

      if (el.querySelector('.unit')) el.querySelector('.unit').hidden = false
    })
  }

  hideShowWithQuerySelector(event) {
    // NOTE: comment での show / edit 制御
    const elements = document.querySelectorAll(`.${event.params.selector}`)

    // 他のコメント編集が有効である場合、そちらを閉じる
    if (this.haveNewCommentEditingNow()) {
      const message =
        document.getElementById('issue-id').dataset.closeConfirmMessage
      if (!confirm(message)) return

      this.closeOtherEditingComment()
    }

    this.resetEditMode()
    this.allResetCommentEditMode()
    this.allToggleCommentContext()

    elements.forEach(el => {
      el.querySelector('.show').hidden = true
      el.querySelector('.show').classList.add('hideView')
      el.querySelector('.edit').hidden = false
      el.querySelector('.edit').classList.remove('hideView')

      el.querySelector('.edit').querySelector('textarea').value =
        event.params.oldValue
      if (el.querySelector('.unit')) el.querySelector('.unit').hidden = true
    })
  }

  // NOTE: コメント投稿エリアが編集中かどうか
  haveNewCommentEditingNow() {
    const viewMode = this.getViewMode()
    const commentInput = document.querySelector(`#${viewMode} .comment-add`)
    const editSection = commentInput.querySelector('.edit')
    return !editSection.classList.contains('hideView')
  }

  // NOTE: コメント投稿エリアの編集状態を解除
  closeOtherEditingComment() {
    document
      .querySelectorAll('.comment-add-notify')
      .forEach(target => target.classList.remove('hideView'))
    document
      .querySelectorAll('.comment-add-edit')
      .forEach(target => target.classList.add('hideView'))
    document
      .querySelectorAll('.comment-input .edit textarea')
      .forEach(target => (target.value = null))
    document
      .querySelectorAll('.issue_add_private_notes')
      .forEach(target => (target.checked = false))
  }

  // 入力コンポーネントから value を取り出す
  getFieldValue(targets) {
    let target = null
    if (targets.length === 1) target = targets[0]

    if (!target && targets) {
      // CFのリスト、チェックボックス、ラジオボックスなど
      if (targets[0].nodeName === 'SELECT' && targets[1].nodeName === 'INPUT') {
        // 複数リスト
        const options = targets[0].options
        let fieldValue = []
        for (let i = 0, l = options.length; i < l; i++) {
          const opt = options[i]
          if (opt.selected) fieldValue.push(opt.value)
        }
        return fieldValue // object
      } else if (targets[0].closest('.check_box_group')) {
        let fieldValue = []
        Array.from(targets).forEach(target => {
          if (target.checked) {
            const beforeValue = target
              .closest('.edit')
              .getAttribute('data-issue_item-before-value')
            const valueToAdd = target.value || beforeValue

            if (valueToAdd) {
              fieldValue.push(valueToAdd)
            }
          }
        })
        return fieldValue.length === 1 ? fieldValue[0] : fieldValue // string or object
      } else if (
        (targets[0].nodeName === 'INPUT' && targets[0].type === 'hidden') ||
        (targets[1].nodeName === 'INPUT' && targets[1].type === 'hidden')
      ) {
        // ファイルアップロード
        if (targets[0].parentNode) {
          const token =
            targets[0].parentNode.querySelector('input.token')?.value ||
            targets[0].parentNode.querySelector(
              '.attachments_fields input[type="hidden"]',
            )?.value
          const filename =
            targets[0].parentNode.querySelector('input.filename')?.value
          const id = targets[0].parentNode.querySelector(
            'input.attachment_id',
          )?.value
          if (token && filename && id) {
            return [{ filename: filename, id: id }] // object
          } else if (!token && filename && id) {
            return [{ filename: filename, id: id }] // object
          } else if (token) {
            return [{ token: token }] // object
          }
        }
      }
    } else {
      if (target.nodeName === 'SELECT' && target.multiple) {
        const options = target.options
        let fieldValue = []
        for (let i = 0, l = options.length; i < l; i++) {
          const opt = options[i]
          if (opt.selected) fieldValue.push(opt.value)
        }
        return fieldValue // object
      } else {
        // projectを変更したとき、選択中のtrackerが変更後のprojectで無効のとき、trackerのselectタグんのvalueが空になるが、
        // controller側で、@issue.tracker_id をセットしていてもうまく表示されない。
        //  ↳ app/views/lychee_issue_form/issues/parts/_header.html.erb:29の:selectedで対応できない。
        if (target.id === 'issue_tracker' && !target.value) {
          target.value = target.parentElement.dataset.issue_itemBeforeValue
        }

        // checkedがついてないcheckboxは空で送信する。
        // そうしないとバリデーションエラーが発生しない
        if (target.type === 'radio' && !target.checked) {
          return ''
        }

        // CF連携絞り込み対応
        // 絞込先の値を入力済みの状態で、選択元の値を変更したとき、
        // 絞込先の元の値が選択肢からなくなった場合、空の値を返すとエラーリカバリーフォームが閉じてしまうので、
        // 絞込先の元の値を返す
        if (
          target.nodeName === 'SELECT' &&
          !target.value &&
          target.name.includes('custom_field_values')
        ) {
          return (
            target.parentNode.getAttribute('data-issue_item-before-value') ||
            target.value
          )
        }

        return target.value // string
      }
    }
  }

  async setFormDataAndSaveIssue(params, url) {
    this.showTarget(true)

    const originCFvalues = params.issue.issue?.customFieldValues || [] // DB上のCFの値
    const enterCFvalues = params.optionalParam?.customFieldValues || [] // ユーザーが入力したCFの値

    // DBのCFの値をキープしつつ、ユーザーが入力した値があれば上書きする
    const customFieldValues = originCFvalues.map(originCF => {
      const matchingEnterCF = enterCFvalues.find(
        enterCF => enterCF.id === originCF.id,
      )
      return matchingEnterCF
        ? { id: originCF.id, value: matchingEnterCF.value }
        : originCF
    })

    const issue = {
      ...params.issue.issue,
      ...params.optionalParam,
      customFieldValues,
    }
    if (issue.trackerId) issue.tracker = issue.trackerId

    Object.keys(issue).forEach(key => {
      const keyName = camelToSnake(this.convertLGCKeyToLIFKey(key))
      const keyValue = issue[key]

      if ('custom_field_values' === keyName) {
        Object.keys(keyValue).forEach(cfKey => {
          const cfId = keyValue[cfKey].id
          const cfValue = keyValue[cfKey].value
          const cfPass = `cf_${cfId}`
          const cfControl = document.getElementsByClassName(cfPass)[0]

          if (cfControl) {
            const cfControlChilds = cfControl.querySelectorAll(
              'select, input, textarea',
            )
            if (cfControlChilds.length === 1) {
              const cfControlChild = cfControlChilds[0]
              if (cfControlChild) {
                if (cfControlChild.nodeName === 'INPUT') {
                  if (cfControlChild.type === 'radio') {
                    if (cfControlChild.value === cfValue)
                      cfControlChild.checked = true
                  } else {
                    cfControlChild.value = cfValue
                  }
                } else if (cfControlChild.nodeName === 'TEXTAREA') {
                  cfControlChild.textContent = cfValue
                } else if (cfControlChild.nodeName === 'SELECT') {
                  const cfValues =
                    typeof cfValue === 'object' ? cfValue : [`${cfValue}`]
                  const options = cfControlChild.options
                  for (let i = 0, l = options.length; i < l; i++) {
                    options[i].selected = cfValues?.includes(
                      `${options[i].value}`,
                    )
                  }
                }
              }
            } else {
              if (
                cfControlChilds[0].nodeName === 'INPUT' &&
                cfControlChilds[0].type === 'hidden'
              ) {
                // ファイル
                if (cfValue.type === 'attachment') {
                  this.setCfFileValueOnLifOpen(cfId, cfValue, cfControlChilds)
                }
              } else if (
                cfControlChilds[0].nodeName === 'SELECT' &&
                cfControlChilds[1].nodeName === 'INPUT'
              ) {
                // 複数リスト
                const options = cfControlChilds[0].options
                for (let i = 0, l = options.length; i < l; i++) {
                  options[i].selected = cfValue.includes(`${options[i].value}`)
                }
              } else {
                Array.from(cfControlChilds).forEach(child => {
                  if (cfValue?.includes(child.value)) {
                    child.checked = true
                  } else {
                    child.checked = false
                  }
                })
                const cfControlEditElm = cfControl.querySelector('.edit')
                if (
                  !cfControlEditElm.getAttribute(
                    'data-issue_item-before-value',
                  ) &&
                  cfValue
                ) {
                  cfControlEditElm.setAttribute(
                    'data-issue_item-before-value',
                    cfValue,
                  )
                }
              }
            }
          }
        })
      } else {
        const pass = `issue_${keyName}`
        const viewMode = this.getViewMode()

        const field = document.getElementById(pass)
        if (field) {
          // トラッカー変更の際に設定中のステータスが利用不可の場合は、コントローラ側でセットされたステータスに従う
          if (
            pass === 'issue_status' &&
            !Array.from(
              document.querySelectorAll(`#${viewMode} #issue_status option`),
            )
              .map(el => el.value)
              .includes(keyValue)
          )
            return
          field.type === 'checkbox'
            ? (field.checked = keyValue)
            : (field.value = keyValue)
        }
      }
    })

    document.getElementById('create-issue').click()
  }

  setCfFileValueOnLifOpen(cfId, cfValue, cfControlChilds) {
    const id = cfValue.value
    const filename = cfValue.filename
    const token = cfValue.token
    const uploadUrl =
      document.getElementById('request-url').dataset.fileUploadUrl
    const maxSizeValue = parseInt(
      document.getElementById('attachment-max-size').innerText || 0,
    )
    const maxHumanSizeValue =
      document.getElementById('attachment-max-human-size').innerText || '0'
    const tooBigMessageValue = document.getElementById(
      'message-attachment-too-big',
    ).innerText

    if (!filename) return

    let elBlank = document.createElement('input')
    elBlank.type = 'hidden'
    elBlank.name = `issue[custom_field_values][${cfId}][blank]`
    elBlank.id = `issue[custom_field_values_${cfId}_blank`
    elBlank.value = id
    let elFilename = document.createElement('input')
    elFilename.type = 'text'
    elFilename.name = `issue[custom_field_values][${cfId}][1][filename]`
    elFilename.id = `issue_custom_field_values_${cfId}_1_filename`
    elFilename.value = filename
    elFilename.className = 'icon icon-attachment filename readonly'
    elFilename.readOnly = true
    let elFilenameLink = document.createElement('a')
    elFilenameLink.setAttribute(
      'onclick',
      "$(this).closest('.attachments_form').find('.add_attachment').show(); $(this).parent().remove(); return false;",
    )
    elFilenameLink.className = 'icon-only icon-del remove-upload'
    elFilenameLink.style = 'display: inline-block'
    elFilenameLink.setAttribute('data-remote', 'true')
    elFilenameLink.setAttribute('data-method', 'delete')
    elFilenameLink.href = '#'
    let elHiddenId = document.createElement('input')
    elHiddenId.type = 'hidden'
    elHiddenId.name = `issue[custom_field_values][${cfId}][1][id]`
    elHiddenId.id = `issue[custom_field_values_${cfId}_1_id`
    elHiddenId.className = 'attachment_id'
    elHiddenId.value = id
    let elHiddenToken = document.createElement('input')
    elHiddenToken.type = 'hidden'
    elHiddenToken.name = `issue[custom_field_values][${cfId}][1][token]`
    elHiddenToken.id = `issue_custom_field_values_${cfId}_1_token`
    elHiddenToken.value = token
    elHiddenToken.className = 'token'
    let elDummy = document.createElement('input')
    elDummy.type = 'file'
    elDummy.name = `issue[custom_field_values][${cfId}][dummy][file]`
    elDummy.className = 'file_selector custom-field-filedrop'
    elDummy.setAttribute('onchange', 'addInputFiles(this);')
    elDummy.setAttribute('data-max-number-of-files-message', 'a')
    elDummy.setAttribute('data-max-file-size', `${maxSizeValue}`)
    elDummy.setAttribute('data-max-file-size-message', `${tooBigMessageValue}`)
    elDummy.setAttribute('data-max-concurrent-uploads', '2')
    elDummy.setAttribute('data-upload-path', uploadUrl)
    elDummy.setAttribute('data-param', `issue[custom_field_values][${cfId}]`)
    elDummy.setAttribute('data-description', 'false')
    elDummy.setAttribute('data-description-placeholder', '説明（任意）')
    elDummy.innerText = `(サイズの上限: ${maxHumanSizeValue})`
    const topNode = cfControlChilds[0].closest('.issue-item')
    let elSpanForm = document.createElement('span')
    elSpanForm.className = 'attachments_form'
    let elSpanFields = document.createElement('span')
    elSpanFields.className = 'attachments_fields'
    let elSpanFieldsP0 = document.createElement('span')
    elSpanFieldsP0.id = 'attachments_1'
    elSpanFieldsP0.appendChild(elFilename)
    elSpanFieldsP0.appendChild(elHiddenId)
    elSpanFieldsP0.appendChild(elHiddenToken)
    elSpanFieldsP0.appendChild(elFilenameLink)
    elSpanFields.appendChild(elSpanFieldsP0)
    elSpanForm.appendChild(elSpanFields)
    let elSpanAddAttachment = document.createElement('span')
    elSpanAddAttachment.className = 'add_attachment'
    elSpanAddAttachment.style = 'display:none;'
    elSpanAddAttachment.appendChild(elDummy)
    elSpanForm.appendChild(elSpanAddAttachment)
    let elForm = document.createElement('form')
    elForm.setAttribute('data-issue-target', 'item')
    elForm.appendChild(elBlank)
    elForm.appendChild(elSpanForm)
    if (topNode.querySelector('.edit .attachments_form'))
      topNode
        .querySelector('.edit')
        .removeChild(topNode.querySelector('.edit .attachments_form'))
    topNode.querySelector('.edit').appendChild(elForm)
  }

  setFormData(params) {
    // フォームデータ組み立て
    let formData = new FormData()
    formData.append('form_update_triggered_by', '')
    formData.append('project_identifier', this.getProjectId())
    formData.append('issue[subject]', params.subject)
    formData.append('issue[project]', params.projectId)
    formData.append('issue[tracker]', params.trackerId)
    formData.append('issue[status]', params.statusId)
    formData.append('issue[fixed_version]', params.fixedVersionId)
    formData.append('issue[start_date]', params.startDate)
    formData.append('issue[due_date]', params.dueDate)
    formData.append('issue[assigned_to]', params.assignedToId)
    formData.append('issue[parent_issue_id]', params.parentIssueId)

    // コントロールに値をセット
    this.setElementValue('issue_subject', params.subject)
    this.setElementValue('issue_id', params.id)
    this.setElementValue('issue_project', params.projectId)
    this.setElementValue('issue_tracker', params.trackerId)
    this.setElementValue('issue_status', params.statusId)
    this.setElementValue('issue_fixed_version', params.fixedVersionId)
    this.setElementValue('issue_start_date', params.startDate)
    this.setElementValue('issue_due_date', params.dueDate)
    this.setElementValue('issue_assigned_to', params.assignedToId)
    this.setElementValue('issue_parent_issue_id', params.parentIssueId)
    this.setElementValue('bypass', true)
  }

  setElementValue(elementId, value) {
    const element = document.getElementById(elementId)
    if (element && value !== undefined) {
      // 0が入ってくる可能性もあるので、undefinedで比較
      element.value = value
    }
  }

  // 新規・編集フォームを表示
  showForm(isCreateIssue, formData = undefined) {
    this.showTarget(isCreateIssue)
    if (formData) {
      this.setFormData(formData)
    }
    this.focusSubject()
  }

  // エラーリカバリーフォームを表示
  showRecoveryForm(formData) {
    this.showTarget(true)
    this.setFormData(formData)
    this.createIssue()
  }

  focusSubject() {
    document.getElementById('issue_subject').focus()
  }

  createIssue() {
    document.getElementById('create-issue').click()
  }

  // NOTE: チケットの新規作成
  async create(event) {
    // ファイルが添付されているのであれば、まずファイルアップロードを実施する。
    const attachFiles = document.querySelector(`#file-input`).files
    const uploadResponses = await Promise.all(
      Array.from(attachFiles).map(async (file, idx) => {
        const response = await this.element['attachment'].postFile(file, idx)
        return response
      }),
    )

    // ファイル添付がない時　または　ファイル添付でアップロード成功した時
    if (!attachFiles.length || uploadResponses.find(response => response.ok)) {
      const sequential = event.params.sequential
      const isPrivate = document.querySelector('#issue_is_private')?.checked
        ? 1
        : 0
      const defaultStatus = document.querySelector(
        '#issue-default-status',
      ).value
      let formData = new FormData()
      formData.append('form_update_triggered_by', '')
      formData.append('project_identifier', this.getProjectId())

      // フォームデータを列挙する
      let hasFormDataError = false
      this.itemTargets.forEach(itemTarget => {
        const targetInputParent = itemTarget.querySelector('.edit')
        const targetInputChilds = targetInputParent.querySelectorAll(
          'select, input, textarea',
        )
        const fieldName = targetInputParent.getAttribute(
          'data-issue_item-field-name-value',
        )
        const fieldValue =
          targetInputChilds.length > 0
            ? this.getFieldValue(targetInputChilds)
            : ''

        // 複数選択セレクトボックス
        if (Array.isArray(fieldValue) && fieldValue.length > 0) {
          fieldValue.forEach(fv => {
            if (typeof fv === 'object') {
              // ファイルアップロード
              if (fv.token) {
                // トークンがある（新規アップロードの場合）
                formData.append(`${fieldName}[1][token]`, fv.token)
              } else if (!fv.token && fv.filename && fv.id) {
                // ID とファイル名がある（既存アップロード済みの場合）
                formData.append(`${fieldName}[blank]`, '')
                formData.append(`${fieldName}[p0][filename]`, fv.filename)
                formData.append(`${fieldName}[p0][id]`, fv.id)
              }
            } else {
              formData.append(`${fieldName}[]`, fv)
            }
          })
        } else if (Array.isArray(fieldValue) && fieldValue.length === 0) {
          // 空配列の場合は "" をセットしないとバリデーションエラーにならない
          formData.append(fieldName, '')
        } else {
          formData.append(fieldName, fieldValue)
        }

        // 日付フィールドは validity を発火させてエラーがあるかどうかチェック
        if (
          targetInputChilds.length > 0 &&
          targetInputChilds[0].type === 'date'
        ) {
          targetInputChilds[0].validity
          if (!!targetInputChilds[0].validationMessage) {
            hasFormDataError = true
            targetInputChilds[0].reportValidity() // jquery のエラー表示（Redmine標準）を呼び出す
          }
        }
      })
      formData.append('issue[is_private]', isPrivate)
      if (hasFormDataError) return

      if (this.isChecklistLocalMode()) {
        formData.append(
          '__lychee_checklist_data',
          document.getElementById('__lychee_checklist_data').value,
        )
      }

      // ファイル添付があるとき
      if (attachFiles.length > 0) {
        Array.from(attachFiles).forEach((file, idx) => {
          const descriptionValue = document.querySelectorAll(
            `#add-files-name input.added-file-description`,
          )[idx].value
          const fileDescription = descriptionValue
            ? descriptionValue.substring(0, 255)
            : ''
          const tokenValue = document
            .querySelectorAll(`#add-files-name .added-file`)
            [idx].querySelector('input.file-token').value

          formData.append(`attachments[${idx}][filename]`, file.name)
          formData.append(`attachments[${idx}][description]`, fileDescription)
          formData.append(`attachments[${idx}][token]`, tokenValue)
        })
      }

      // NOTE: グローバルオブジェクト連携
      if (!document.getElementById('bypass').value) {
        const detail = {
          ...this.parseRailsFormDataToJSON(formData),
          originalParam: this.lifParamValue,
        }
        const customEvent = new CustomEvent(CE_LIF_BEFORE_SUBMIT, {
          cancelable: true,
          detail: detail,
        })
        if (!document.dispatchEvent(customEvent)) {
          // NOTE: 呼び出し側で event.preventDefault() が呼ばれた場合は処理を中断
          return
        }
      }

      const compatibleUrl = getCompatibleUrl()
      let response
      this.lifExistIssueId =
        this.lifExistIssueId ||
        document.getElementById('edit-issue-id')?.innerText ||
        null
      if (this.lifExistIssueId) {
        response = await patch(`${compatibleUrl}/${this.lifExistIssueId}`, {
          body: formData,
        })
      } else {
        response = await post(`${compatibleUrl}/`, { body: formData })
      }
      const message = await response.text
      const messageJson = JSON.parse(message)
      document.getElementById('bypass').value = null

      if (response.ok) {
        this.setHasValidationErrors('false')

        // NOTE: グローバルオブジェクト連携
        if (messageJson) {
          const issueId = this.extractIssueId(messageJson.location)
          const detail = { issueId: issueId, originalParam: this.lifParamValue }
          const customEvent = new CustomEvent(CE_LIF_SUCCESS_SAVE, {
            cancelable: true,
            detail: detail,
          })
          if (!document.dispatchEvent(customEvent)) {
            // NOTE: 呼び出し側で event.preventDefault() が呼ばれた場合は処理を中断
            return
          }

          this.showFlashNotice(messageJson.message)
        }

        if (this.lifExistIssueId) {
          // NOTE: 更新の場合
          // グローバルオブジェクト連携
          const issueId = this.lifExistIssueId
          const detail = { issueId: issueId, originalParam: this.lifParamValue }
          const customEvent = new CustomEvent(CE_LIF_SUCCESS_SAVE, {
            cancelable: true,
            detail: detail,
          })
          if (!document.dispatchEvent(customEvent)) {
            // NOTE: 呼び出し側で event.preventDefault() が呼ばれた場合は処理を中断
            return
          }
        } else {
          // NOTE: 新規追加の場合
          // グローバルオブジェクト連携
          const issueId = this.extractIssueId(messageJson.location)
          const detail = { issueId: issueId, originalParam: this.lifParamValue }
          const customEvent = new CustomEvent(CE_LIF_SUCCESS_SAVE, {
            cancelable: true,
            detail: detail,
          })
          if (!document.dispatchEvent(customEvent)) {
            // NOTE: 呼び出し側で event.preventDefault() が呼ばれた場合は処理を中断
            return
          }
        }

        if (sequential) {
          // 連続作成
          const trackerIndex =
            document.querySelector('#issue_tracker').selectedIndex
          // メッセージ表示時間として少し待機した後、フォームを再描画させる
          setTimeout(async () => {
            await this.fetchNewIssueHtml(compatibleUrl) // 先にロードさせておく
            document.querySelector('#issue_tracker').options[
              trackerIndex
            ].selected = true // 引き継いだトラッカーを使う
            this.hideTarget(true)
            this.showTarget(true)
          }, SEQUENTIAL_ISSUES_CREATION_TIMEOUT_MILLISECONDS)
        } else {
          this.hideTarget(true)
        }

        this.lifExistIssueId = null
        this.lifParamValue = {}
      } else {
        if (response.statusCode === 403) {
          // チケット作成権限なし
          this.showNotification(messageJson.messages[0])
        } else if (response.statusCode === 422) {
          // バリデーション
          let errors = messageJson.errors
          if (messageJson.errors.base && messageJson.errors.base.length > 0) {
            let newErrors = []
            // errors.base パスに CF エラーが集約されている場合
            errors.base.forEach(entry => {
              const key = entry.split(' ')[0]
              const message = entry.split(' ').slice(1).join(' ')
              newErrors[key] = [message]
            })
            // errors.base
            Object.keys(errors).forEach(key => {
              if (key !== 'base') newErrors[key] = errors[key]
            })
            errors = newErrors
          }
          this.showValidationErrors(errors)
        }
      }
    } else {
      // 赤枠とエラーメッセージを初期化
      document
        .querySelectorAll('.lif_validation_error')
        .forEach(t => t.classList.remove('lif_validation_error'))
      document
        .querySelectorAll('.lif_validation_error_message')
        .forEach(t => t.remove())

      // ファイル添付があるケースで全てのファイルのアップロードが失敗したとき
      const fileInfoInputs = document.querySelector('#tab-content-files')
      uploadResponses.forEach(async response => {
        const message = await response.text
        const messageJson = JSON.parse(message)

        if (messageJson.full_messages) {
          messageJson.full_messages.forEach(message => {
            const addElement = document.createElement('div')
            addElement.className = 'lif_validation_error_message'
            addElement.textContent = message
            fileInfoInputs.appendChild(addElement)
          })
        }
      })
    }
  }

  // NOTE: バリデーションエラーを表示する
  showValidationErrors(errors) {
    // 赤枠とエラーメッセージを初期化
    document
      .querySelectorAll('.lif_validation_error')
      .forEach(t => t.classList.remove('lif_validation_error'))
    document
      .querySelectorAll('.lif_validation_error_message')
      .forEach(t => t.remove())

    Object.keys(errors).forEach(key => {
      this.setHasValidationErrors('true')
      this.itemTargets.forEach(itemTarget => {
        const editSection = itemTarget.querySelector('.edit')
        const label = editSection.getAttribute('data-label')
        const child = editSection.querySelector('select, input, textarea')
        const attrName = editSection.getAttribute('data-attribute-name')

        if (attrName === key) {
          if (this.isCheckBoxGroup(editSection)) {
            // CF のチェックボックスグループ
            const checkBoxGroup = editSection.querySelector('.check_box_group')
            checkBoxGroup.classList.add('lif_validation_error')
          } else {
            child.classList.add('lif_validation_error') // 赤枠
          }
          errors[key].forEach(error => {
            let errorMessage = document.createElement('p')
            errorMessage.classList.add('lif_validation_error_message') // エラーメッセージ
            errorMessage.innerText = `${label} ${error}`
            editSection.append(errorMessage)
          })
        }
      })
    })
    // 最初の要素にフォーカスする
    if (
      document.querySelectorAll('.lif_validation_error') &&
      document.querySelectorAll('.lif_validation_error')[0]
    ) {
      const firstErrorElement = document.querySelectorAll(
        '.lif_validation_error',
      )[0]
      if (
        firstErrorElement.type === 'hidden' ||
        firstErrorElement.classList.contains('check_box_group')
      ) {
        firstErrorElement.closest('.edit').focus() // ファイルアップロードCFや複数チェックボックスなど直接フォーカスできないもの
      } else {
        firstErrorElement.focus()
      }
    }
  }

  isCheckBoxGroup(target) {
    return !!target.querySelector('.check_box_group')
  }

  // NOTE: Rails 形式の formData を JSON に変換する
  parseRailsFormDataToJSON(formData) {
    let json = {}
    const mergedFormData = this.mergeFormData(formData) // issue[cf][0][] など複数あるエントリをまとめる
    let objectFormData = Object.fromEntries(formData.entries())
    objectFormData = Object.assign(objectFormData, mergedFormData)
    Object.keys(objectFormData).forEach(key => {
      const payload = objectFormData[key]
      const jsonValue = this.arrayToNestedJSON(
        this.extractBracketsToArray(key),
        payload,
      )
      json = this.mergeDeeply(json, jsonValue)
    })
    return json
  }

  // NOTE: Rails 形式の formData キー文字列を Array 型に変換する
  //       例 "issue[subject]" => ["issue", "subject"]
  //       キーがスネークケースの場合はキャメルケースにする
  extractBracketsToArray(inputString) {
    const bracketContents = inputString.match(/\[(.*?)\]/g)
    if (!bracketContents) return [snakeToCamel(inputString)] // 中括弧が見つからない場合はそのままの文字列を含む配列を返す

    // 中括弧内の文字列を抽出し、括弧を削除して結果を返す
    const resultArray = bracketContents.map(content =>
      snakeToCamel(content.slice(1, -1)),
    )
    resultArray.unshift(snakeToCamel(inputString.split('[')[0])) // "issue" の部分を先頭に追加
    return resultArray
  }

  // NOTE: Array 型のデータを JSON パスに変換し、値をセットする
  //       例 (["issue", "subject"], "hoge") => { issue: { subject: "hoge" } }
  arrayToNestedJSON(array, value) {
    if (array.length === 0) return null // 空の配列の場合 null を返す

    const obj = {}
    let lastKey = array.pop() // 最後の要素を取得
    if (lastKey === '') {
      // キー配列の最後が空の場合は []（要素追加） なので value を Array 型にする
      value = [value]
      lastKey = array.pop()
    }
    let currentObj = obj
    for (const key of array) {
      currentObj[key] = {}
      currentObj = currentObj[key]
    }
    currentObj[lastKey] = value
    return obj
  }

  // NOTE: formData に重複するキーが含まれる場合、Array 型にまとめる
  mergeFormData(formData) {
    const arrayEntries = Array.from(formData.entries())
    const keys = arrayEntries.map(entry => entry[0])
    let duplicatedKeys = keys.filter(
      (value, i, array) => !(array.indexOf(value) === i),
    )
    duplicatedKeys = Array.from(new Set(duplicatedKeys))

    let mergedEntries = {}
    arrayEntries.forEach(entry => {
      const key = entry[0]
      const value = entry[1]
      if (duplicatedKeys.includes(key)) {
        if (typeof mergedEntries[key] != 'object') mergedEntries[key] = []
        mergedEntries[key].push(value)
      }
    })
    return mergedEntries
  }

  // NOTE: JSON の deep merge を行う
  mergeDeeply(target, source, opts) {
    const isObject = obj =>
      obj && typeof obj === 'object' && !Array.isArray(obj)
    const isConcatArray = opts && opts.concatArray
    let result = Object.assign({}, target)
    if (isObject(target) && isObject(source)) {
      for (const [sourceKey, sourceValue] of Object.entries(source)) {
        const targetValue = target[sourceKey]
        if (
          isConcatArray &&
          Array.isArray(sourceValue) &&
          Array.isArray(targetValue)
        ) {
          result[sourceKey] = targetValue.concat(...sourceValue)
        } else if (isObject(sourceValue) && target.hasOwnProperty(sourceKey)) {
          result[sourceKey] = this.mergeDeeply(targetValue, sourceValue, opts) // recursive
        } else {
          Object.assign(result, { [sourceKey]: sourceValue })
        }
      }
    }
    return result
  }

  extractIssueId(location) {
    if (!location || location.length === 0) return null

    return parseInt(location.split('/').slice(-1)[0])
  }

  convertLGCKeyToLIFKey(lgcKey) {
    if (lgcKey === 'parentIssueId') return lgcKey
    return lgcKey.replace(/(_id|Id)$/, '')
  }

  getIssueStatusClosedById() {
    return JSON.parse(
      document.getElementById('issue-status-closed').dataset.issueStatusClosed,
    )
  }

  getTrackerDefaultStatusId() {
    return JSON.parse(
      document.getElementById('tracker-default-status-id').dataset
        .trackerDefaultStatusId,
    )
  }

  getSelectedTrackerId() {
    const viewMode = this.getViewMode()
    return document.querySelector(`#${viewMode} #issue_tracker`)[0]?.value
  }

  isClosedStatus(statusId) {
    const issueStatuses = this.getIssueStatusClosedById()
    return issueStatuses[statusId].isClosed
  }

  isChecklistLocalMode() {
    if (document.getElementById('checklist-local-mode') == null) return false

    return JSON.parse(document.getElementById('checklist-local-mode').innerText)
  }

  getActualDates(isNewMode) {
    const viewMode = this.getViewMode()

    const actualStartDateSelectorId = isNewMode
      ? 'issue_actual_start_date'
      : 'actual_start_date'
    const actualDueDateSelectorId = isNewMode
      ? 'issue_actual_due_date'
      : 'actual_due_date'
    const actualStartDate = document.querySelector(
      `#${viewMode} #${actualStartDateSelectorId}`,
    )
    const actualDueDate = document.querySelector(
      `#${viewMode} #${actualDueDateSelectorId}`,
    )

    return { actualStartDate, actualDueDate }
  }

  setActualDatesByEditMode({
    statusId,
    prevStatusId,
    defaultStatusId,
    actualStartDate,
    actualDueDate,
  }) {
    const today = getToday()

    if (
      actualStartDate &&
      defaultStatusId === Number(prevStatusId) &&
      defaultStatusId !== Number(statusId)
    ) {
      if (!actualStartDate.value) actualStartDate.value = today
    }
    if (
      actualDueDate &&
      !this.isClosedStatus(prevStatusId) &&
      this.isClosedStatus(statusId)
    ) {
      if (!actualDueDate.value) actualDueDate.value = today
    }
  }

  setActualDatesByShowMode({
    statusId,
    prevStatusId,
    defaultStatusId,
    actualStartDate,
    actualDueDate,
  }) {
    const today = getToday()

    if (
      actualStartDate &&
      defaultStatusId === Number(prevStatusId) &&
      defaultStatusId !== Number(statusId)
    ) {
      if (!isValidDateFormat(actualStartDate.textContent.trim()))
        actualStartDate.textContent = today
    }

    if (
      actualDueDate &&
      !this.isClosedStatus(prevStatusId) &&
      this.isClosedStatus(statusId)
    ) {
      if (!isValidDateFormat(actualDueDate.textContent.trim()))
        actualDueDate.textContent = today
    }
  }

  assignTodayToActualDates(event) {
    if (event.target.name !== 'issue[status]' || !this.isInstalledActualDate())
      return

    const isNewMode = this.isNewOpenMode()
    const statusId = event.target.value
    const attributeNameBeforeValue = 'data-issue_item-before-value'
    const prevStatusId = event.target.parentElement.getAttribute(
      attributeNameBeforeValue,
    )
    const defaultStatusId = this.getTrackerDefaultStatusId()
    const { actualStartDate, actualDueDate } = this.getActualDates(isNewMode)

    const args = {
      statusId,
      prevStatusId,
      defaultStatusId,
      actualStartDate,
      actualDueDate,
    }

    isNewMode
      ? this.setActualDatesByEditMode(args)
      : this.setActualDatesByShowMode(args)
  }

  isInstalledActualDate() {
    return (
      document.getElementById('actual-date').dataset.actualDateInstalled ===
      'true'
    )
  }

  extractOnClose(param) {
    if (onClose || !param.onClose) return

    onClose = param.onClose
  }

  executeOnClose() {
    if (onClose) {
      onClose()
    }
  }
}

const getToday = () => {
  const today = new Date()

  const year = today.getFullYear()
  const month = (today.getMonth() + 1).toString().padStart(2, '0')
  const day = today.getDate().toString().padStart(2, '0')

  return `${year}-${month}-${day}`
}

const isValidDateFormat = dateString => {
  const datePattern = /^\d{4}-\d{2}-\d{2}$/
  return datePattern.test(dateString)
}

const camelToSnake = camelCase => {
  return camelCase.replace(/([A-Z])/g, group => `_${group.toLowerCase()}`)
}

const snakeToCamel = snakeCase => {
  return snakeCase
    .toLowerCase()
    .replace(/([-_][a-z])/g, group =>
      group.toUpperCase().replace('-', '').replace('_', ''),
    )
}

const convertKeysToCamelCase = obj => {
  const camelCasedObj = {}
  Object.keys(obj).forEach(key => {
    const camelKey = snakeToCamel(key)
    camelCasedObj[camelKey] = obj[key]
  })
  return camelCasedObj
}
