import { Controller } from '@hotwired/stimulus'
import { get, put, post } from '@rails/request.js'

// 差分比較用のフォーム初期値
let timeLogFormInitData = {}
// NOTE: カスタムイベント
const CE_LIF_BEFORE_SUBMIT = 'lifBeforeSubmit'
const CE_LIF_SUCCESS_SAVE = 'lifSuccessSave'

/*
 *  Lychee Issue Form
 *  コメントコントロール
 */
export default class extends Controller {
  static targets = ['notify', 'edit', 'filterSections']
  static values = {
    journalId: String,
    journalIndice: String,
  }

  initialize() {
    this.element[this.identifier] = this
    this.showCommentAddNotifier()
    this.storeTimeLogFormData(timeLogFormInitData)
  }

  getCompatibleIssuesUrl() {
    return document.getElementById('request-url').dataset.fixedIssuesUrl
  }

  getCompatibleJournalsUrl() {
    return document.getElementById('request-url').dataset.fixedJournalsUrl
  }

  // 要素特定に向け ViewMode(Full/Half) を取得
  getViewMode() {
    return this.element['mode']?.getViewMode()
  }

  appendFormDataValues(obj, formData) {
    formData.forEach((value, key) => {
      // もし同じキーが既に存在する場合、値を配列に追加（selectタグのmultiple対応）
      if (obj[key]) {
        if (!Array.isArray(obj[key])) {
          obj[key] = [obj[key]] // 既存の値を配列に変換
        }
        obj[key].push(value)
      } else {
        obj[key] = value
      }
    })
  }

  storeFormData(obj, form) {
    if (Object.keys(obj).length || !form) return

    const formData = new FormData(form)
    this.appendFormDataValues(obj, formData)

    // formがnestされている場合の対応（CFのFileの必須の場合）
    const nestedForms = form.querySelectorAll('form')
    nestedForms.forEach(nestedForm => {
      const nestedFormData = new FormData(nestedForm)
      this.appendFormDataValues(obj, nestedFormData)
    })
  }

  storeTimeLogFormData(obj) {
    const viewMode = this.getViewMode() || 'viewHalf'
    const form = document.querySelector(`#${viewMode} [name="timelogForm"]`)

    this.storeFormData(obj, form)
  }

  canCloseOrChangeForm() {
    if (this.hasFormChanged()) {
      const message =
        document.getElementById('issue-id').dataset.closeConfirmMessage
      return confirm(message)
    }
    return true
  }

  hasFormChanged() {
    // [バリデーションエラー]
    const validationError = document.querySelector(
      '.lif_validation_error_message',
    )
    if (validationError) return true

    // [編集時]
    const viewMode = this.getViewMode()

    // プロパティタブ（現在のモードの要素で編集中の添付ファイルがあるか, update前の添付ファイルがあるかを検証）
    const fileElements = Array.from(
      document.querySelectorAll(
        `#${viewMode} #tab-content-files .edit.file-info`,
      ),
    )
    const hasChangedFiles = !!fileElements.find(
      el => !el.hasAttribute('hidden'),
    )

    const addingFileElements = document.querySelector(
      `#${viewMode} #add-files-name`,
    )
    const hasUnupdatedFile = !!addingFileElements?.children.length

    // 作業時間タブ（FormDataの初期値と現在値の差分があるかを検証）
    let timelogFormCurrentData = {}
    this.storeTimeLogFormData(timelogFormCurrentData)

    const hasChangedTimeLog = this.hasChanged(
      timeLogFormInitData,
      timelogFormCurrentData,
    )

    return hasChangedFiles || hasUnupdatedFile || hasChangedTimeLog
  }

  // 2つのobjを比較し、差分があるかを取得
  hasChanged(a, b) {
    if (a === b) return false

    const aKeys = Object.keys(a).sort()
    const bKeys = Object.keys(b).sort()

    if (aKeys.toString() !== bKeys.toString()) return true

    // aとbがすべて一致するときは-1
    const wrongIndex = aKeys.findIndex(key => {
      // NOTE: 配列のとき（複数選択可）は、文字列化して比較
      if (Array.isArray(a[key]) && Array.isArray(b[key])) {
        if (JSON.stringify(a[key]) !== JSON.stringify(b[key])) return true
      } else if (key === 'resume' || key.includes('file')) {
        // NOTE: FileオブジェクトのlastModifiedはオブジェクトごとに変わるので、添付ファイルのみnameとsizeで比較
        return (
          a[key]['name'] !== b[key]['name'] && a[key]['size'] !== b[key]['size']
        )
      } else if (a[key] !== b[key]) {
        return true
      }
      return false
    })

    return wrongIndex !== -1
  }

  getInputCommentElement() {
    return this.element.querySelector(
      `#${this.getViewMode()} .comment-input .edit textarea`,
    )
  }

  isPostPrivateComment() {
    return this.element.querySelector(
      `#${this.getViewMode()} .comment-input .edit input[name="private_comment"]`,
    )?.checked
  }

  showCommentAddNotifier() {
    this.notifyTargets.forEach(el => {
      el.classList.remove('hideView')
    })
    this.editTargets.forEach(el => {
      el.classList.add('hideView')
    })
    document
      .querySelectorAll('.comment-input .edit textarea')
      .forEach(target => (target.value = null))
    document
      .querySelectorAll('.issue_add_private_notes')
      .forEach(target => (target.checked = false))
  }

  // NOTE: 投稿済みのコメントのいずれかが編集中かどうか
  haveOtherCommentsEditingNow() {
    const commentInputs = document.querySelectorAll('.comment-input-edit')
    return Array.from(commentInputs).some(commentInput => {
      let editSection = commentInput.querySelector('.edit')
      if (!editSection.classList.contains('hideView')) return true
    })
  }

  // NOTE: 投稿済みのコメントの編集状態を解除
  closeOtherEditingComment() {
    const comments = document.querySelectorAll('.comment-input-edit')
    comments.forEach(el => {
      el.querySelector('.show').hidden = false
      el.querySelector('.show').classList.remove('hideView')
      el.querySelector('.edit').hidden = true
      el.querySelector('.edit').classList.add('hideView')
    })
    const contextMenus = document.querySelectorAll('.context-menus')
    contextMenus.forEach(contextMenu =>
      contextMenu.classList.remove('hidden-context-menus'),
    )
  }

  showCommentEditor() {
    // 他のコメント編集が有効である場合、そちらを閉じる
    if (this.haveOtherCommentsEditingNow()) {
      const message = document.getElementById('issue-id').dataset.closeConfirmMessage
      if (!confirm(message)) return

      this.closeOtherEditingComment()
    }

    this.notifyTargets.forEach(el => {
      el.classList.add('hideView')
    })
    this.editTargets.forEach(el => {
      el.classList.remove('hideView')
    })
  }

  // 引用コメントセット用に viewMode によって id を振り直す
  resetIssueNotesId() {
    const el = this.element
    const issue_notes = document.getElementById('issue_notes')
    if (issue_notes) issue_notes.id = ''
    el.closest('.view-mode').querySelector('.issue_notes').id = 'issue_notes'
  }

  postQuoteParameter() {
    const journalId = this.journalIdValue
    const journalIndice = this.journalIndiceValue

    return journalId && journalIndice
      ? `?journal_id=${journalId}&journal_indice=${journalIndice}`
      : ''
  }

  switchJournalsTab() {
    const viewMode = this.getViewMode()
    const journalMode = this.element['mode'].getJournalMode()
    this.element['mode'].setViewMode(
      viewMode,
      'btn-comments_and_journals',
      journalMode,
    )
  }

  // NOTE: コンテキストメニューの「リンクをコピー」押下で、クリップボードにリンクを格納
  copyObjectUrlLink(event) {
    const objectUrl = event.params.objectUrl
    if (navigator.clipboard) navigator.clipboard.writeText(objectUrl)

    // 「リンクをコピー」押下時にコンテキストメニューを閉じる
    const target = event.target.closest('.popup-journal-context')
    target.classList.add('hideView')
  }

  // Redmine 標準の機能を利用して、引用は /issues/:id/quoted に post する
  async quoteComments() {
    const el = this.element

    // 説明欄で引用符が非表示の場合は引用を行わない
    if (el.closest('.lif-description') && !el.querySelector('svg')) return

    // 他のコメント編集が有効である場合、そちらを閉じる
    if (this.haveOtherCommentsEditingNow()) {
      const message = document.getElementById('issue-id').dataset.closeConfirmMessage
      if (!confirm(message)) return

      this.closeOtherEditingComment()
    }

    // コメント/履歴タブが「プロパティ更新」の時に説明欄が引用された場合は、強制的に「すべて」に変えて説明欄の内容をセット
    if (this.element['mode'].getJournalMode() == 'journals')
      this.element['mode'].setJournalMode('all')

    el.closest('.view-mode')
      .querySelector('#comment-input .notify')
      .classList.add('hideView')
    el.closest('.view-mode')
      .querySelector('#comment-input .edit')
      .classList.remove('hideView')

    this.resetIssueNotesId()
    this.switchJournalsTab()

    const issueId = document.getElementById('issue-id').innerText
    const postPath =
      `${this.getCompatibleIssuesUrl()}/${issueId}/quoted` +
      this.postQuoteParameter()
    const response = await post(postPath, {
      contentType: 'application/javascript',
      headers: { accept: 'application/javascript' },
    })

    if (response.ok) {
      const result = await response.text
      eval(result)
    }
  }

  async fetchFormData(viewMode, selectTabId) {
    const issueId = document.getElementById('issue-id').innerText
    const journalMode = this.element['mode'].getJournalMode()

    const response = await get(`${this.getCompatibleIssuesUrl()}/${issueId}`)

    if (response.ok) {
      const dialogContent = document.getElementById('dialog-content')

      dialogContent.innerHTML = await response.text
      this.element['mode'].setViewMode(viewMode, selectTabId, journalMode)
    } else {
      // TODO: 失敗時の処理
    }
  }

  showNotification(message) {
    const notification = document.querySelector('#notification')
    notification.classList.remove('fade-out')
    notification.querySelector('#notification-content').innerText = message
    notification.classList.add('fade-out')
  }

  // コメントの新規登録
  async postComment() {
    if (!this.canCloseOrChangeForm()) return

    const formData = new FormData()
    const viewMode = this.getViewMode()
    const selectTabId = this.element['mode'].getSelectTabId()

    const updateComment = this.getInputCommentElement().value
    if (!updateComment) return

    formData.append('issue[notes]', updateComment)
    if (this.isPostPrivateComment()) {
      formData.append('issue[private_notes]', 1)
    }

    const issueId = document.getElementById('issue-id').innerText
    const response = await put(`${this.getCompatibleIssuesUrl()}/${issueId}`, {
      body: formData,
    })

    if (response.ok) {
      // データ再取得処理
      await this.fetchFormData(viewMode, selectTabId)
      this.showCommentAddNotifier()

      // グローバルオブジェクト連携
      const detail = { issueId: issueId, originalParam: { id: issueId } }
      const customEvent = new CustomEvent(CE_LIF_SUCCESS_SAVE, {
        cancelable: true,
        detail: detail,
      })
      if (!document.dispatchEvent(customEvent)) {
        // NOTE: 呼び出し側で event.preventDefault() が呼ばれた場合は処理を中断
        return
      }
    } else {
      // エラー表示
      const dialogContent = document.getElementById('dialog-content')
      dialogContent.innerHTML = await response.text
    }
  }

  // コメントの更新
  async updateComment(event) {
    if (!this.canCloseOrChangeForm()) return

    const viewMode = this.getViewMode()
    const selectTabId = this.element['mode'].getSelectTabId()
    const issueId = document.getElementById('issue-id').innerText
    const journalId = event.params.journalId
    const updateComment = this.getInputCommentElement().value

    const formData = new FormData()
    formData.append('journal[notes]', updateComment)
    formData.append(
      'journal[private_notes]',
      this.isPostPrivateComment() ? 1 : 0,
    ) // プライベートコメントの ON/OFF に対応

    const response = await put(
      `${this.getCompatibleJournalsUrl()}/${journalId}`,
      { body: formData },
    )

    if (response.ok) {
      // データ再取得処理
      await this.fetchFormData(viewMode, selectTabId)

      // グローバルオブジェクト連携
      const detail = { issueId: issueId, originalParam: { id: issueId } }
      const customEvent = new CustomEvent(CE_LIF_SUCCESS_SAVE, {
        cancelable: true,
        detail: detail,
      })
      if (!document.dispatchEvent(customEvent)) {
        // NOTE: 呼び出し側で event.preventDefault() が呼ばれた場合は処理を中断
        return
      }
    } else {
      // エラー表示（暫定）
      const message = await response.text
      const messageJson = JSON.parse(message)
      await this.fetchFormData(viewMode, selectTabId)
      this.showNotification(messageJson.messages[0])
    }
  }

  async delete() {
    const message =
      document.getElementById('issue-id').dataset.destroyConfirmMessage
    if (!confirm(message)) {
      return
    }

    const viewMode = this.getViewMode()
    const selectTabId = this.element['mode'].getSelectTabId()

    const issueId = document.getElementById('issue-id').innerText
    const journalId = this.element.id.match(/journal-delete-(\d+)/)[1]
    const formData = new FormData()
    formData.append('journal[notes]', '')
    const response = await put(
      `${this.getCompatibleJournalsUrl()}/${journalId}`,
      { body: formData },
    )

    if (response.ok) {
      // データ再取得処理
      await this.fetchFormData(viewMode, selectTabId)

      // グローバルオブジェクト連携
      const detail = { issueId: issueId, originalParam: { id: issueId } }
      const customEvent = new CustomEvent(CE_LIF_SUCCESS_SAVE, {
        cancelable: true,
        detail: detail,
      })
      if (!document.dispatchEvent(customEvent)) {
        // NOTE: 呼び出し側で event.preventDefault() が呼ばれた場合は処理を中断
        return
      }
    } else {
      // エラー表示（暫定）
      const message = await response.text
      const messageJson = JSON.parse(message)
      await this.fetchFormData(viewMode, selectTabId)
      this.showNotification(messageJson.messages[0])
    }
  }
}
