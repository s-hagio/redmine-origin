const CE_LIF_SUCCESS_SAVE = 'lifSuccessSave'

export const dispatchCustomEvent = (customDetail = {}) => {
  const issueId = document.getElementById('issue-id').innerText
  const detail = {
    issueId: issueId,
    originalParam: { id: issueId },
  }
  const customEvent = new CustomEvent(CE_LIF_SUCCESS_SAVE, {
    cancelable: true,
    detail: Object.keys(customDetail).length > 0 ? customDetail : detail,
  })

  if (!document.dispatchEvent(customEvent)) {
    // NOTE: 呼び出し側で event.preventDefault() が呼ばれた場合は、
    // falseを返し、後続処理を実行しない
    return false
  }

  return true
}
