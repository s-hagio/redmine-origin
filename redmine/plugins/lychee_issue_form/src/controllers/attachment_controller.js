import { Controller } from '@hotwired/stimulus'
import { post, patch, destroy } from '@rails/request.js'
import { dispatchCustomEvent } from './common'

// ドラッグ&ドロップイベントを割り当てた識別のためのクラス
const ADDED_EVENT_LISTENER_CLASS = 'added-event-listener'
// 一度にアップロード可能なファイル数の最大値 / Redmine本体も固定値: 10 でセット
const MAX_ATTACH_FILES_COUNT_AT_ONCE = 10
let inputFileList = []

// 差分比較用のフォーム初期値
let timeLogFormInitData = {}

/*
 *  Lychee Issue Form
 *  ファイル追加コントロール
 */
export default class extends Controller {
  static values = {
    fileId: String,
    deleteConfirmMessage: String,
    beforeFileName: String,
    beforeFileDescription: String,
  }

  initialize() {
    this.storeTimeLogFormData(timeLogFormInitData)
  }

  connect() {
    this.element[this.identifier] = this
    this.drag_and_drop()
  }

  getCompatibleUrl() {
    return document.getElementById('request-url').dataset.fixedUrl
  }

  getCompatibleIssuesUrl() {
    return document.getElementById('request-url').dataset.fixedIssuesUrl
  }

  // ViewMode(Full/Half) を取得
  getViewMode() {
    if (!document.getElementById('viewFull')) return 'viewHalf'

    return !Array.from(document.getElementById('viewFull').classList).includes(
      'hideView',
    )
      ? 'viewFull'
      : 'viewHalf'
  }

  appendFormDataValues(obj, formData) {
    formData.forEach((value, key) => {
      // もし同じキーが既に存在する場合、値を配列に追加（selectタグのmultiple対応）
      if (obj[key]) {
        if (!Array.isArray(obj[key])) {
          obj[key] = [obj[key]] // 既存の値を配列に変換
        }
        obj[key].push(value)
      } else {
        obj[key] = value
      }
    })
  }

  storeFormData(obj, form) {
    if (Object.keys(obj).length || !form) return

    const formData = new FormData(form)
    this.appendFormDataValues(obj, formData)

    // formがnestされている場合の対応（CFのFileの必須の場合）
    const nestedForms = form.querySelectorAll('form')
    nestedForms.forEach(nestedForm => {
      const nestedFormData = new FormData(nestedForm)
      this.appendFormDataValues(obj, nestedFormData)
    })

    // 新規フォームのプライベートチェックのデータを追加
    if (this.isNewOpenMode()) {
      const isPrivate = document.getElementById('issue_is_private')?.checked
      obj['issue[is_private]'] = isPrivate
    }
  }

  storeTimeLogFormData(obj) {
    const viewMode = this.getViewMode()
    const form = document.querySelector(`#${viewMode} [name="timelogForm"]`)

    this.storeFormData(obj, form)
  }

  canCloseOrChangeForm() {
    if (this.hasFormChanged()) {
      const message =
        document.getElementById('issue-id').dataset.closeConfirmMessage
      return confirm(message)
    }
    return true
  }

  hasFormChanged() {
    // [バリデーションエラー]
    const validationError = document.querySelector(
      '.lif_validation_error_message',
    )
    if (validationError) return true

    // [編集時]
    const viewMode = this.getViewMode()

    // コメント/履歴タブ（現在のモードの要素で編集中があるかを検証）
    const noteElements = Array.from(
      document.querySelectorAll(
        `#${viewMode} #tab-comments_and_journals .edit`,
      ),
    )
    const hasChangedNotes = !!noteElements.find(
      el => !el.classList.contains('hideView'),
    )

    // 作業時間タブ（FormDataの初期値と現在値の差分があるかを検証）
    let timelogFormCurrentData = {}
    this.storeTimeLogFormData(timelogFormCurrentData)

    const hasChangedTimeLog = this.hasChanged(
      timeLogFormInitData,
      timelogFormCurrentData,
    )

    return hasChangedNotes || hasChangedTimeLog
  }

  // 2つのobjを比較し、差分があるかを取得
  hasChanged(a, b) {
    if (a === b) return false

    const aKeys = Object.keys(a).sort()
    const bKeys = Object.keys(b).sort()

    if (aKeys.toString() !== bKeys.toString()) return true

    // aとbがすべて一致するときは-1
    const wrongIndex = aKeys.findIndex(key => {
      // NOTE: 配列のとき（複数選択可）は、文字列化して比較
      if (Array.isArray(a[key]) && Array.isArray(b[key])) {
        if (JSON.stringify(a[key]) !== JSON.stringify(b[key])) return true
      } else if (key === 'resume' || key.includes('file')) {
        // NOTE: FileオブジェクトのlastModifiedはオブジェクトごとに変わるので、添付ファイルのみnameとsizeで比較
        return (
          a[key]['name'] !== b[key]['name'] && a[key]['size'] !== b[key]['size']
        )
      } else if (a[key] !== b[key]) {
        return true
      }
      return false
    })

    return wrongIndex !== -1
  }

  drag_and_drop() {
    Array.from(['viewFull', 'viewHalf']).forEach(view => {
      const fileArea = document.querySelector(
        `#${view} div.file-input-drop-area`,
      )
      if (!fileArea) return

      const fileInput = fileArea.querySelector('input.file-input')
      if (fileArea.classList.contains(ADDED_EVENT_LISTENER_CLASS)) return

      inputFileList = []
      fileArea.classList.add(ADDED_EVENT_LISTENER_CLASS)
      fileArea.addEventListener('dragover', function (e) {
        e.preventDefault()
        fileArea.classList.add('dragover')
      })

      fileArea.addEventListener('dragleave', function (e) {
        e.preventDefault()
        fileArea.classList.remove('dragover')
      })

      fileArea.addEventListener('drop', function (e) {
        e.preventDefault()
        fileArea.classList.remove('dragover')

        // ドロップしたファイルの取得
        var files = e.dataTransfer.files

        // 取得したファイルをinput[type=file]へ
        fileInput.files = files

        if (typeof files[0] !== 'undefined') {
          const files = e.dataTransfer.files
          if (files.length === 0) return

          // ファイル容量チェック
          const maxSizeValue = parseInt(
            document.getElementById('attachment-max-size').innerText || 0,
          )
          const tooBigMessageValue = document.getElementById(
            'message-attachment-too-big',
          ).innerText
          const result = Array.from(files).filter(
            file => file.size > maxSizeValue,
          )
          if (result.length > 0) {
            const filesInput = document.querySelector(
              `#${view} div.attachment_files #file-input`,
            )
            filesInput.value = null

            alert(tooBigMessageValue)
            return
          }

          const dt = new DataTransfer()
          inputFileList.forEach(file => {
            dt.items.add(file)
          })

          // 1度にアップロードできる最大値を超える場合はメッセージを表示
          const messageMaxAttachFilesCount = document.getElementById(
            'message-validation-max-attach-files-count-at-once',
          ).innerText
          const messageValidationFileExtension = document.getElementById(
            'message-validation-file-extension',
          ).innerText
          if (
            inputFileList.length + files.length >
            MAX_ATTACH_FILES_COUNT_AT_ONCE
          )
            alert(messageMaxAttachFilesCount)

          // ファイルをセット
          Array.from(files).forEach((file, idx) => {
            const addedFileCount = document.querySelectorAll(
              `#${view} .added-file`,
            ).length
            const extension = file.name.split('.').pop()
            if (addedFileCount >= MAX_ATTACH_FILES_COUNT_AT_ONCE) return
            const extensionAllowedList = document.getElementById(
              'attachment-allowed-extension',
            ).innerText
            const extensionDeniedList = document.getElementById(
              'attachment-denied-extension',
            ).innerText

            if (extensionAllowedList) {
              const arrAllowedExtensions = extensionAllowedList
                .split(',')
                .map(extension => extension.trim())
              if (!arrAllowedExtensions.includes(extension)) {
                alert(
                  messageValidationFileExtension.replace(
                    '%{extension}',
                    extension,
                  ),
                )
                return
              }
            } else if (extensionDeniedList) {
              const arrDeniedExtensions = extensionDeniedList
                .split(',')
                .map(extension => extension.trim())
              if (arrDeniedExtensions.includes(extension)) {
                alert(
                  messageValidationFileExtension.replace(
                    '%{extension}',
                    extension,
                  ),
                )
                return
              }
            }

            // ファイル追加時はアップロード済みファイルの編集をできなくする
            const alreadDisabledEditUploadFiles = Array.from(
              document.querySelectorAll('td.attach-right button'),
            ).find(el => el.disabled)
            if (alreadDisabledEditUploadFiles) return
            document
              .querySelectorAll('td.attach-right button')
              .forEach(el => el.setAttribute('disabled', 'disabled'))
            document
              .querySelectorAll('td.attach-right button a')
              .forEach(el => {
                const parent = el.parentElement
                parent.appendChild(el.querySelector('svg').cloneNode(true))
                el.classList.add('hideView')
              })
            document
              .querySelectorAll('td.attach-right button > svg')
              .forEach(svg => svg.setAttribute('opacity', '20%'))

            inputFileList.push(file)
            dt.items.add(file)

            const templateElement = document.querySelector(
              `#${view} div.attachment_files .add-files-template`,
            )
            const parent = document.querySelector(`#${view} #add-files-name`)
            parent.appendChild(templateElement.cloneNode(true))

            const targets = document.querySelectorAll(
              `#${view} div.attachment_files .add-files-template`,
            )
            const targetElement = targets[targets.length - 1]
            targetElement.querySelector('.added-file-name').textContent =
              file.name
            targetElement.classList.remove('add-files-template')
            targetElement.classList.remove('hideView')
            targetElement.classList.add(`added-file`)

            if (idx === 0) {
              targetElement.querySelector('.added-file-description').focus()
            }

            const isNewOpenMode =
              document.getElementById('open-mode')?.innerText === 'new'
            if (!isNewOpenMode) {
              const btnUpload = document.querySelector(`#${view} #btn-upload`)
              btnUpload.classList.remove('hideView')
              const btnCancel = document.querySelector(
                `#${view} #btn-upload-cancel`,
              )
              btnCancel.classList.remove('hideView')
            }
          })

          const inputFiles = document.querySelector(
            `#${view} div.attachment_files #file-input`,
          )
          inputFiles.files = dt.files
        }
      })
    })
  }

  // 新規チケット作成モードかどうかの判定
  isNewOpenMode() {
    return document.getElementById('open-mode').innerText === 'new'
  }

  getFileExtension(filename) {
    return filename.indexOf('.') < 0 ? '' : filename.split('.').pop()
  }

  isValidExtension(filename) {
    const extensionAllowedList = document.getElementById(
      'attachment-allowed-extension',
    ).innerText
    const extensionDeniedList = document.getElementById(
      'attachment-denied-extension',
    ).innerText
    const extension = this.getFileExtension(filename)

    if (extensionAllowedList) {
      // allowed があるとき: 設定されているものに含まれない場合は false
      const arrAllowedExtensions = extensionAllowedList
        .split(',')
        .map(extension => extension.trim())
      if (!arrAllowedExtensions.includes(extension)) return false
    } else if (extensionDeniedList) {
      // allowed がなくて、denied があるとき: 該当と一致する場合は false
      const arrDeniedExtensions = extensionDeniedList
        .split(',')
        .map(extension => extension.trim())
      if (arrDeniedExtensions.includes(extension)) return false
    }

    return true
  }

  setDisabledAddFiles() {
    document
      .querySelector(`#viewHalf input.file-input`)
      .setAttribute('disabled', 'disabled')
  }

  setEnabledAddFiles() {
    document
      .querySelector(`#viewHalf input.file-input`)
      .removeAttribute('disabled')
  }

  setDisabledEditUploadFiles() {
    const alreadDisabledEditUploadFiles = Array.from(
      document.querySelectorAll('td.attach-right button'),
    ).find(el => el.disabled)
    if (alreadDisabledEditUploadFiles) return

    document
      .querySelectorAll('td.attach-right button')
      .forEach(el => el.setAttribute('disabled', 'disabled'))
    document.querySelectorAll('td.attach-right button a').forEach(el => {
      const parent = el.parentElement
      parent.appendChild(el.querySelector('svg').cloneNode(true))
      el.classList.add('hideView')
    })
    document
      .querySelectorAll('td.attach-right button > svg')
      .forEach(svg => svg.setAttribute('opacity', '20%'))
  }

  setEnabledEditUploadFiles() {
    document
      .querySelectorAll('td.attach-right button')
      .forEach(el => el.removeAttribute('disabled'))
    document.querySelectorAll('td.attach-right button a').forEach(el => {
      el.classList.remove('hideView')
      el.parentElement.querySelector('svg[opacity="20%"]').remove()
    })
    document
      .querySelectorAll('td.attach-right button svg')
      .forEach(svg => svg.setAttribute('opacity', '100%'))
  }

  addFiles(event) {
    event.preventDefault()
    event.stopPropagation()

    const files = event.target.files
    if (files.length === 0) return

    const maxSizeValue = parseInt(
      document.getElementById('attachment-max-size').innerText || 0,
    )
    const tooBigMessageValue = document.getElementById(
      'message-attachment-too-big',
    ).innerText
    const result = Array.from(files).filter(file => file.size > maxSizeValue)
    if (result.length > 0) {
      const filesInput = this.element
        .closest('div.attachment_files')
        .querySelector('#file-input')
      filesInput.value = null
      alert(tooBigMessageValue)
      return
    }

    const dt = new DataTransfer()
    inputFileList.forEach(file => {
      dt.items.add(file)
    })
    const messageMaxAttachFilesCount = document.getElementById(
      'message-validation-max-attach-files-count-at-once',
    ).innerText
    const messageValidationFileExtension = document.getElementById(
      'message-validation-file-extension',
    ).innerText
    if (inputFileList.length + files.length > MAX_ATTACH_FILES_COUNT_AT_ONCE)
      alert(messageMaxAttachFilesCount)

    Array.from(files).forEach((file, idx) => {
      const addedFileCount = document.querySelectorAll(
        `#${this.element['mode'].getViewMode()} .added-file`,
      ).length
      if (addedFileCount >= MAX_ATTACH_FILES_COUNT_AT_ONCE) return
      if (!this.isValidExtension(file.name)) {
        const extension = this.getFileExtension(file.name)
        alert(messageValidationFileExtension.replace('%{extension}', extension))
        return
      }

      this.setDisabledEditUploadFiles()
      inputFileList.push(file)
      dt.items.add(file)
      this.addFilesInfo(file.name, idx === 0)
    })
    const inputFiles = this.element
      .closest('div.attachment_files')
      .querySelector('#file-input')
    inputFiles.files = dt.files
  }

  // 追加ファイルの情報入力コントロールを初期化
  clearAddFilesInfo() {
    this.setEnabledEditUploadFiles()
    const el = document.querySelector(
      `#${this.element['mode'].getViewMode()} #add-files-name`,
    )
    el.innerHTML = ''
    inputFileList = []

    const btnUpload = document.querySelector(
      `#${this.element['mode'].getViewMode()} #btn-upload`,
    )
    btnUpload.classList.add('hideView')
    const btnCancel = document.querySelector(
      `#${this.element['mode'].getViewMode()} #btn-upload-cancel`,
    )
    btnCancel.classList.add('hideView')
  }

  // 追加ファイルの情報入力コントロールを表示
  addFilesInfo(filename, isFocus = false) {
    const templateElement = document.querySelector(
      `#${this.element[
        'mode'
      ].getViewMode()} div.attachment_files .add-files-template`,
    )
    const parent = document.querySelector(
      `#${this.element['mode'].getViewMode()} #add-files-name`,
    )
    parent.appendChild(templateElement.cloneNode(true))

    const targets = document.querySelectorAll(
      `#${this.element[
        'mode'
      ].getViewMode()} div.attachment_files .add-files-template`,
    )
    const targetElement = targets[targets.length - 1]
    targetElement.querySelector('.added-file-name').textContent = filename
    targetElement.classList.remove('add-files-template')
    targetElement.classList.remove('hideView')
    targetElement.classList.add(`added-file`)

    if (isFocus) {
      targetElement.querySelector('.added-file-description').focus()
    }

    // 新規作成モードではファイルアップロード/キャンセルボタンは非表示
    if (!this.isNewOpenMode()) {
      const btnUpload = document.querySelector(
        `#${this.element['mode'].getViewMode()} #btn-upload`,
      )
      btnUpload.classList.remove('hideView')
      const btnCancel = document.querySelector(
        `#${this.element['mode'].getViewMode()} #btn-upload-cancel`,
      )
      btnCancel.classList.remove('hideView')
    }
  }

  cancelAddFiles() {
    const filesInput = this.element
      .closest('div.attachment_files')
      .querySelector('#file-input')
    filesInput.value = null
    this.clearAddFilesInfo()
  }

  async uploadFiles(event) {
    if (!this.canCloseOrChangeForm()) return // 他タブに編集中のものがある場合、確認ダイアログを表示

    event.preventDefault()
    event.stopPropagation()

    // ファイルアップロード
    const elFiles = document.querySelector(
      `#${this.element['mode'].getViewMode()} #file-input`,
    )
    const uploadResponses = await Promise.all(
      Array.from(elFiles.files).map(async (file, idx) => {
        const response = await this.postFile(file, idx)
        return response
      }),
    )

    // アップロードしたファイル情報（説明欄）を更新
    if (uploadResponses.find(response => response.ok)) {
      const updateDescriptionResponses = await this.updateDescription(
        elFiles.files,
      )

      if (updateDescriptionResponses.ok) {
        if (!dispatchCustomEvent()) return

        this.clearAddFilesInfo()
        this.element['mode'].fetchFormData('btn-property')
      } else {
        // ファイルアップロードには成功、しかしチケット更新に失敗した場合は
        // 他項目のバリデーションエラー発生とみなし、エラーリカバリーモードへ移行
        const message = await updateDescriptionResponses.text
        const errorMessage = JSON.parse(message)
        console.log(errorMessage)

        const issue_id = document.getElementById('issue-id').innerText
        const tracker_id = document.querySelector(`#${this.getViewMode()} #issue_tracker`).value
        const project_id = document.querySelector(`#${this.getViewMode()} #issue_project`).value
        const status_id = document.querySelector(`#${this.getViewMode()} #issue_status`).value
        const issue = { tracker_id, project_id, status_id, repost_new: true }
        const params = { id: issue_id, issue }

        lif.open(params, errorMessage)
      }
    }
  }

  deleteAddFile() {
    const dt = new DataTransfer()
    const elementsAddedFile = document.querySelectorAll(
      `#${this.element['mode'].getViewMode()} .added-file`,
    )
    const elementTargetDelete = this.element.closest('.added-file')
    const index = [].slice.call(elementsAddedFile).indexOf(elementTargetDelete)

    inputFileList.splice(index, 1)
    inputFileList.forEach(file => {
      dt.items.add(file)
    })
    const inputFiles = this.element
      .closest('div.attachment_files')
      .querySelector('#file-input')
    inputFiles.files = dt.files
    elementTargetDelete.remove()

    if (
      document.querySelectorAll(
        `#${this.element['mode'].getViewMode()} .added-file`,
      ).length < 1
    )
      this.clearAddFilesInfo()
  }

  async deleteFile() {
    if (!confirm(this.deleteConfirmMessageValue)) return

    const response = await destroy(
      `${this.getCompatibleUrl()}/attachments/${this.fileIdValue}`,
    )

    if (response.ok) {
      if (!dispatchCustomEvent()) return

      this.element['mode'].fetchFormData('btn-property')
    }
  }

  async postFile(file, idx) {
    const filename = encodeURIComponent(file.name)
    const contentType = encodeURIComponent(file.type)
    const response_upload = await post(
      `${this.getCompatibleUrl()}/uploads?attachment_id=null&filename=${filename}&content_type=${contentType}`,
      {
        contentType: 'application/octet-stream',
        credentials: 'include',
        headers: this.makeHeaders({
          'Content-Type': 'application/octet-stream',
        }),
        body: file,
      },
    )

    const responseUploadJson = JSON.parse(await response_upload.text)
    const addedFile = document.querySelectorAll(
      `#${this.element['mode'].getViewMode()} #add-files-name .added-file`,
    )[idx]

    const addElement = document.createElement('input')
    addElement.className = 'hideView'
    addElement.classList.add('file-token')
    addElement.value = responseUploadJson.token
    addedFile.appendChild(addElement)

    return response_upload
  }

  async updateDescription(files) {
    const issueId = document.getElementById('issue-id').innerText
    const formData = new FormData()

    Array.from(files).forEach((file, idx) => {
      const descriptionValue = document.querySelectorAll(
        `#${this.element[
          'mode'
        ].getViewMode()} #add-files-name input.added-file-description`,
      )[idx].value
      const fileDescription = descriptionValue
        ? descriptionValue.substring(0, 255)
        : ''
      const tokenValue = document
        .querySelectorAll(
          `#${this.element['mode'].getViewMode()} #add-files-name .added-file`,
        )
        [idx].querySelector('input.file-token').value

      formData.append(`attachments[${idx}][filename]`, file.name)
      formData.append(`attachments[${idx}][description]`, fileDescription)
      formData.append(`attachments[${idx}][token]`, tokenValue)
    })

    const response = await patch(
      `${this.getCompatibleIssuesUrl()}/${issueId}`,
      { body: formData },
    )
    return response
  }

  editFileInfo() {
    this.setDisabledAddFiles()
    this.element
      .closest('tr')
      .querySelectorAll('td.attach-left .edit')
      .forEach(el => {
        el.hidden = false
      })
    this.element
      .closest('tr')
      .querySelectorAll('td.attach-left .show')
      .forEach(el => {
        el.hidden = true
      })
    document
      .querySelectorAll(
        `#${this.element[
          'mode'
        ].getViewMode()} table.attach_files tr td.attach-right`,
      )
      .forEach(el => {
        el.hidden = true
      })
    this.element
      .closest('tr')
      .querySelector('td.attach-left .edit input.filename')
      .focus()
  }

  resetFileInfo() {
    this.setEnabledAddFiles()
    // 入力中の内容をクリア
    const fileInfoInputs = this.element
      .closest('tr')
      .querySelector('td.attach-left div.file-info')
    this.clearValidationErrors()
    fileInfoInputs.querySelector('input.filename').value =
      this.beforeFileNameValue
    fileInfoInputs.querySelector('input.description').value =
      this.beforeFileDescriptionValue

    // 要素の表示/非表示をリセット
    this.element
      .closest('tr')
      .querySelectorAll('td.attach-left .edit')
      .forEach(el => {
        el.hidden = true
      })
    this.element
      .closest('tr')
      .querySelectorAll('td.attach-left .show')
      .forEach(el => {
        el.hidden = false
      })
    document
      .querySelectorAll(
        `#${this.element[
          'mode'
        ].getViewMode()} table.attach_files tr td.attach-right`,
      )
      .forEach(el => {
        el.hidden = false
      })
  }

  clearValidationErrors() {
    document.querySelectorAll('.validation_error').forEach(el => {
      el.classList.remove('validation_error')
    })
    document.querySelectorAll('.validation_error_message').forEach(el => {
      el.remove()
    })
  }

  // 添付ファイルの情報更新
  async updateFileInfo() {
    if (!this.canCloseOrChangeForm()) return // 他タブに編集中のものがある場合、確認ダイアログを表示

    const issueId = document.getElementById('issue-id').innerText
    const fileInfoInputs = this.element
      .closest('tr')
      .querySelector('td.attach-left div.file-info')
    const filename = fileInfoInputs.querySelector('input.filename').value
    const description = fileInfoInputs.querySelector('input.description').value
    this.clearValidationErrors()

    // ファイル名が未入力の時
    if (!filename) {
      const addElement = document.createElement('div')
      addElement.className = 'validation_error_message'
      addElement.textContent = document.getElementById(
        'message-validation-file-name',
      ).innerText

      const targetElement = fileInfoInputs.querySelector('input.filename')
      targetElement.classList.add('validation_error')
      fileInfoInputs.appendChild(addElement)
      return
    }

    const formData = new FormData()
    formData.append(`attachments[${this.fileIdValue}][filename]`, filename)
    formData.append(
      `attachments[${this.fileIdValue}][description]`,
      description,
    )
    formData.append('object_type', 'issues')
    formData.append('object_id', issueId)
    const response = await patch(
      `${this.getCompatibleUrl()}/attachments/${this.fileIdValue}`,
      { body: formData },
    )

    if (response.ok) {
      if (!dispatchCustomEvent()) return

      this.resetFileInfo
      this.element['mode'].fetchFormData('btn-property')
    } else {
      const message = await response.text
      const errorMessage = JSON.parse(message)

      if (errorMessage.full_messages) {
        errorMessage.full_messages.forEach(message => {
          const addElement = document.createElement('div')
          addElement.className = 'validation_error_message'
          addElement.textContent = message
          const target = errorMessage.errors
            ? Object.keys(errorMessage.errors)
            : null
          const targetElement =
            target && target[0] !== 'base'
              ? fileInfoInputs.querySelector(`.${target[0]}`)
              : fileInfoInputs
          targetElement.classList.add('validation_error')
          fileInfoInputs.appendChild(addElement)
        })
      } else {
        const addElement = document.createElement('div')
        addElement.className = 'validation_error_message'
        addElement.textContent = errorMessage.messages
        const target = errorMessage.errors
          ? Object.keys(errorMessage.errors)
          : null
        const targetElement =
          target && target[0] !== 'base'
            ? fileInfoInputs.querySelector(`.${target[0]}`)
            : fileInfoInputs
        targetElement.classList.add('validation_error')
        fileInfoInputs.appendChild(addElement)
      }
    }
  }

  makeHeaders(options = {}) {
    const domCsrfToken = document.getElementsByName('csrf-token')[0]
    const csrfToken = domCsrfToken && domCsrfToken.getAttribute('content')

    return {
      Accept: 'application/json',
      'Content-Type': 'application/octet-stream',
      'X-CSRF-Token': csrfToken,
      ...options,
    }
  }

  handleError(response) {
    if (!response.ok) {
      const error = new Error(response.statusText)
      error.response = response
      throw error
    }
    if (response.status === 204) {
      return response
    }
    return response.json()
  }
}
