import { Controller } from '@hotwired/stimulus'
import { useClickOutside } from 'stimulus-use'
import { get, patch } from '@rails/request.js'

const ADDED_EVENT_LISTENER_CLASS = 'added-event-listener'
let isCompositioning = false

// 差分比較用のフォーム初期値
let timeLogFormInitData = {}
// NOTE: カスタムイベント
const CE_LIF_BEFORE_SUBMIT = 'lifBeforeSubmit'
const CE_LIF_SUCCESS_SAVE = 'lifSuccessSave'

/*
 *  Lychee Issue Form
 *  チケット項目コントロール: 項目外クリック時に編集モードリセット
 */
export default class extends Controller {
  static values = {
    forceDisableOutsideClick: Boolean,
    fieldName: String,
    before: String,
    lcfRequired: Array,
    lcfAll: Object,
    lcfVisible: Object,
  }

  initialize() {
    this.storeTimeLogFormData(timeLogFormInitData)
    document.querySelectorAll('.type_attachment.not-editable')?.forEach(el => {
      el.querySelector('input[type="file"]').disabled = true
    })
  }

  connect() {
    // NOTE: data-controller に issue_item が含まれると
    //       明示的に data-action を指定していなくても useClickOutside 自体が発火するようになってしまうため
    //       パラメータで data-issue_item-force-disable-outside-click-value="true" が
    //       渡っている場合は useClickOutside を行わないようにする
    //       （意図しない clickOutside の発火を抑制する）
    //
    //       例: issue_item において clickOutside は使わないが他のアクションは使いたいという場合は
    //       data-issue_item-force-disable-outside-click-value="true" を指定する
    if (!this.forceDisableOutsideClickValue) useClickOutside(this)

    // 1行テキストボックスで変換中の判定用イベントリスナーの設定
    this.compositionOnTextInputs()
  }

  getCompatibleIssuesUrl() {
    return document.getElementById('request-url').dataset.fixedIssuesUrl
  }

  appendFormDataValues(obj, formData) {
    formData.forEach((value, key) => {
      // もし同じキーが既に存在する場合、値を配列に追加（selectタグのmultiple対応）
      if (obj[key]) {
        if (!Array.isArray(obj[key])) {
          obj[key] = [obj[key]] // 既存の値を配列に変換
        }
        obj[key].push(value)
      } else {
        obj[key] = value
      }
    })
  }

  storeFormData(obj, form) {
    if (Object.keys(obj).length || !form) return

    const formData = new FormData(form)
    this.appendFormDataValues(obj, formData)

    // formがnestされている場合の対応（CFのFileの必須の場合）
    const nestedForms = form.querySelectorAll('form')
    nestedForms.forEach(nestedForm => {
      const nestedFormData = new FormData(nestedForm)
      this.appendFormDataValues(obj, nestedFormData)
    })
  }

  storeTimeLogFormData(obj) {
    const viewMode = this.getViewMode() || 'viewHalf'
    const form = document.querySelector(`#${viewMode} [name="timelogForm"]`)

    this.storeFormData(obj, form)
  }

  canCloseOrChangeForm(event) {
    if (this.hasFormChanged()) {
      const message =
        document.getElementById('issue-id').dataset.closeConfirmMessage
      const result = confirm(message)
      event['isConfirmation'] = result

      return result
    }
    return true
  }

  hasFormChanged() {
    // [バリデーションエラー] プロパティタブのバリデーションエラーは除外する
    const validationErrors = Array.from(
      document.querySelectorAll('.lif_validation_error_message'),
    ).filter(element => !element.closest('#tab-property'))
    if (validationErrors.length) return true

    // [編集時]
    const viewMode = this.getViewMode()

    // プロパティタブ（現在のモードの要素で編集中の添付ファイルがあるか, update前の添付ファイルがあるかを検証）
    const fileElements = Array.from(
      document.querySelectorAll(
        `#${viewMode} #tab-content-files .edit.file-info`,
      ),
    )
    const hasChangedFiles = !!fileElements.find(
      el => !el.hasAttribute('hidden'),
    )

    const addingFileElements = document.querySelector(
      `#${viewMode} #add-files-name`,
    )
    const hasUnupdatedFile = !!addingFileElements?.children.length

    // コメント/履歴タブ（現在のモードの要素で編集中があるかを検証）
    const noteElements = Array.from(
      document.querySelectorAll(
        `#${viewMode} #tab-comments_and_journals .edit`,
      ),
    )
    const hasChangedNotes = !!noteElements.find(
      el => !el.classList.contains('hideView'),
    )

    // 作業時間タブ（FormDataの初期値と現在値の差分があるかを検証）
    let timelogFormCurrentData = {}
    this.storeTimeLogFormData(timelogFormCurrentData)

    const hasChangedTimeLog = this.hasChanged(
      timeLogFormInitData,
      timelogFormCurrentData,
    )

    return (
      hasChangedFiles ||
      hasUnupdatedFile ||
      hasChangedNotes ||
      hasChangedTimeLog
    )
  }

  // 2つのobjを比較し、差分があるかを取得
  hasChanged(a, b) {
    if (a === b) return false

    const aKeys = Object.keys(a).sort()
    const bKeys = Object.keys(b).sort()

    if (aKeys.toString() !== bKeys.toString()) return true

    // aとbがすべて一致するときは-1
    const wrongIndex = aKeys.findIndex(key => {
      // NOTE: 配列のとき（複数選択可）は、文字列化して比較
      if (Array.isArray(a[key]) && Array.isArray(b[key])) {
        if (JSON.stringify(a[key]) !== JSON.stringify(b[key])) return true
      } else if (key === 'resume' || key.includes('file')) {
        // NOTE: FileオブジェクトのlastModifiedはオブジェクトごとに変わるので、添付ファイルのみnameとsizeで比較
        return (
          a[key]['name'] !== b[key]['name'] && a[key]['size'] !== b[key]['size']
        )
      } else if (a[key] !== b[key]) {
        return true
      }
      return false
    })

    return wrongIndex !== -1
  }

  clickOutside(event) {
    const openMode = document.getElementById('open-mode').innerText

    if (openMode === 'new' || openMode === 'edit-all') return

    // 1行テキストボックスで変換中の場合は発火させない
    if (isCompositioning) return

    this.clearErrorMessages()
    // input 要素のバリデーションエラー時は更新せずエラー表示
    if (
      this.element.querySelector('input') &&
      !this.element.querySelector('input').validity.valid
    ) {
      this.setErrorMessages(
        this.element.querySelector('input').validationMessage,
      )
      return
    }

    // NOTE: connect() に記載した内容に共通した判定
    //       （意図しない clickOutside の発火を抑制する）
    if (!this.forceDisableOutsideClickValue) {
      // 更新処理
      event.preventDefault()
      const updateValue = this.getUpdateValue()
      if (this.isChanged(updateValue)) {
        if (!this.canCloseOrChangeForm(event)) return // 他タブに編集中のものがある場合、確認ダイアログを表示
        this.update(updateValue)
      } else {
        // NOTE: 値に変化がない場合はバリデーション状態も解除する
        this.deleteAllValidationErrors()
        this.setHasValidationErrors('false')
        this.resetItem()
      }
      this.setShowItem()
    }
  }

  isNewOpenMode() {
    return document.getElementById('open-mode').innerText === 'new'
  }

  noneLocalized() {
    const dialogContent = document.getElementById('issue-id')
    return dialogContent.getAttribute('data-destroy-confirm-message') ===
      'よろしいですか？'
      ? ' (なし)'
      : ' (none)'
  }

  eventSelectChange(event) {
    if (!this.isNewOpenMode()) return

    this.redrawRequiredMark(event.target)
    // 'all' is the same for all options so get the first one
    const allEnums = this.lcfAllValue[Object.keys(this.lcfAllValue)[0]]
    if (!allEnums) return // no LCF action attached

    let enums = allEnums
    if (event.target.value !== '') {
      enums = this.lcfVisibleValue[event.target.value]
      // if no visible enums add [] for every element
      if (!enums) {
        enums = {}
        Object.keys(allEnums).forEach(key => (enums[key] = []))
      }
    }

    this.redrawElements(enums)
  }

  redrawElements(enums) {
    if (!enums) return

    Object.keys(enums).forEach(key => {
      const element =
        document.querySelector(`span.cf_${key}`) ||
        document.querySelector(`select.cf_${key}`)
      if (element) {
        const isCheckBox = element.className.includes('check_box_group')
        const selectedValues = this.getOptionsValues(element).map(i =>
          parseInt(i),
        )
        element.innerHTML = ''
        isCheckBox ? this.addRadio(element, key, true) : this.addOption(element)
        enums[key].forEach(item => {
          const selected = selectedValues.includes(parseInt(item.value))
          isCheckBox
            ? this.addRadio(element, key, selected, item.text, item.value)
            : this.addOption(element, selected, item.text, item.value)
        })
      }
    })
  }

  redrawRequiredMark(element) {
    const values = this.getOptionsValues(element).map(i => parseInt(i))
    this.lcfRequiredValue.forEach(cf => {
      const cfNameDt = document.querySelector(`dl.cf_${cf.cf_id} dt`)
      if (cfNameDt) {
        const marked = cfNameDt.querySelector("span[class='required']")
        const need_marked = cf.cf_enumeration_ids.find(i => values.includes(i))
        if (!marked && need_marked) {
          this.addRequiredMark(cfNameDt)
        } else if (marked && !need_marked) {
          marked.remove()
        }
      }
    })
  }

  getOptionsValues(element) {
    let targets = null
    if (element.selectedOptions) {
      // <select>
      targets = element.selectedOptions
    } else {
      // <input type="checkbox">
      targets = element.parentNode.parentNode.querySelectorAll('input:checked')
    }
    return Array.from(targets).map(i => i.value)
  }

  addOption(element, selected = false, text = '', value = '') {
    const choice = document.createElement('option')
    choice.text = text
    choice.value = value
    if (selected) choice.selected = 'selected'
    element.appendChild(choice)
  }

  addRadio(element, key, checked = false, text = '', value = '') {
    const choice = document.createElement('input')
    choice.type = 'radio'
    choice.name = `issue[custom_field_values][${key}]`
    if (checked) {
      choice.checked = 'checked'
      choice.defaultChecked = true
    }
    choice.value = value
    const label = document.createElement('label')
    label.appendChild(choice)
    label.innerHTML += text === '' ? this.noneLocalized() : text
    element.appendChild(label)
  }

  addRequiredMark(element) {
    const span = document.createElement('span')
    span.className = 'required'
    span.textContent = '*'
    element.appendChild(span)
  }

  eventKeydownEnter(event) {
    if (event.target.closest('.issue-item').querySelector('.edit textarea'))
      return

    this.clickOutside(event)
  }

  compositionOnTextInputs() {
    const headerTextnputs = document.querySelectorAll(
      '#dialog-content-header input[type="text"]',
    )
    const viewFullTextInputs = document.querySelectorAll(
      '#dialog-content-column-left input[type="text"]',
    )
    const viewHalfTextInputs = document.querySelectorAll(
      '#tab-property input[type="text"]',
    )
    const propertyTextInputs = Object.setPrototypeOf(
      [...headerTextnputs, ...viewFullTextInputs, ...viewHalfTextInputs],
      NodeList.prototype,
    )
    propertyTextInputs.forEach(element => {
      if (element.classList.contains(ADDED_EVENT_LISTENER_CLASS)) return

      element.classList.add(ADDED_EVENT_LISTENER_CLASS)
      element.addEventListener('compositionstart', function (e) {
        e.preventDefault()
        isCompositioning = true
      })
      element.addEventListener('compositionend', function (e) {
        e.preventDefault()
        isCompositioning = false
      })
    })
  }

  // 編集対象要素が Multiple の attribute を持っているか判定
  isMultiple() {
    return this.getUpdateElement().getAttribute('multiple')
  }

  isRadioControl() {
    return this.getUpdateElement().querySelector('input[type="radio"]')
  }

  // 編集対象要素がチェックボックスか判定
  isCheckControl() {
    return this.getUpdateElement().className.includes('check_box_group')
  }

  // 編集対象要素が添付ファイルか判定
  isAttachmentControl() {
    return this.getUpdateElement().className.includes('attachments_fields')
  }

  // 表示用要素(show) に表示用テキストとともに value を更新する対象かどうか判定
  isUpdateStringAndValueField() {
    return (
      this.element.closest('.type_project') ||
      this.element.closest('.type_user') ||
      this.element.closest('.type_version') ||
      this.element.closest('.type_issue')
    )
  }

  // 複数値を持つ場合の判定用に値を整形
  toStringMultipleValues(values) {
    const emptyValue = this.isRadioControl() ? '' : '[""]'
    return values.length === 0 || !values[0]
      ? emptyValue
      : '["' + values.reverse().join('", "') + '"]'
  }

  // 編集要素(edit)で値が変更かれたか判定
  isChanged(updateValue) {
    const beforeValue = this.beforeValue
    let updValue = ''
    if (this.isAttachmentControl() && updateValue.length > 1) {
      updValue = updateValue[1].split('.')[0]
    } else if (this.isMultiple() || this.isCheckControl()) {
      updValue = this.toStringMultipleValues(updateValue)
    } else {
      updValue = updateValue[0] || ''
    }

    return beforeValue !== updValue
  }

  // 要素特定に向け ViewMode(Full/Half) を取得
  getViewMode() {
    return this.element['mode']?.getViewMode()
  }

  // 自身のチケット項目要素を特定するためのセレクタ（response 取得後でも対応可能とするため）
  getSelfAttributeSelector() {
    return `.edit[data-issue_item-field-name-value="${this.fieldNameValue}"]`
  }

  // 自身の要素を取得
  getAttributeElement() {
    return document
      .querySelector(
        `#${this.getViewMode()} ${this.getSelfAttributeSelector()}`,
      )
      .closest('.issue-item')
  }

  // 自身の編集用(edit)要素の取得
  getUpdateElement() {
    return (
      this.getAttributeElement().querySelector('.edit .check_box_group') ||
      this.getAttributeElement().querySelector('.edit select') ||
      this.getAttributeElement().querySelector('.edit textarea') ||
      this.getAttributeElement().querySelector('.edit .attachments_fields') ||
      this.getAttributeElement().querySelector('.edit input')
    )
  }

  // (チェックボックス形式のinputでの)値を取得
  getCheckControlValue(elCheck) {
    return Array.from(elCheck.querySelectorAll('input'))
      .filter(el => el.checked)
      .map(inp => inp.value)
  }

  // 表示用(show) 要素に設定されている値を取得
  getShowText() {
    return this.element.parentElement.querySelector('.show').innerText.trim()
  }

  // 編集用(edit)要素に入力された値を取得
  getUpdateValue() {
    const el = this.getUpdateElement()
    let value = []

    if (this.isMultiple()) {
      Array.from(el).forEach(option => {
        if (option.selected == true) value.push(option.value)
      })
    } else if (this.isAttachmentControl()) {
      if (el.querySelector('input.token')) {
        value.push(el.querySelector('input.filename').value)
        value.push(el.querySelector('input.token').value)
      } else if (el.querySelector('input[type="hidden"]')) {
        value.push(el.querySelector('input[type="hidden"]').value)
      }
    } else if (this.isCheckControl()) {
      Array.from(this.getCheckControlValue(el)).forEach(val => {
        value.push(val)
      })
    } else {
      value.push(el.value)
    }

    return value
  }

  clearErrorMessages() {
    const errorMessageEl = document.querySelector('#errorExplanation')
    if (errorMessageEl) {
      const contentColumns = document.querySelectorAll(
        '#dialog-content-columns',
      )
      contentColumns.forEach(el =>
        el.setAttribute('style', 'height: calc(100vh - 178px'),
      )
      const tabContents = document.querySelectorAll('.lif-tab-content')
      tabContents.forEach(el =>
        el.setAttribute('style', 'height: calc(100vh - 230px'),
      )
    }

    document.querySelectorAll('#errorExplanation').forEach(el => el.remove())
  }

  setErrorMessages(errorMessage) {
    const targetElements = document.querySelectorAll('#dialog-content-columns')
    targetElements.forEach(el => {
      let insertElement = document.createElement('div')
      insertElement.setAttribute('id', 'errorExplanation')
      insertElement.innerText = errorMessage

      el.before(insertElement)
    })

    this.setErrorHeight()
  }

  setErrorHeight() {
    const errorMessageEl = document.querySelector(
      `#${this.getViewMode()} #errorExplanation`,
    )
    if (!errorMessageEl) return

    const targetHeight = errorMessageEl.clientHeight
    const contentColumns = document.querySelectorAll('#dialog-content-columns')
    contentColumns.forEach(el =>
      el.setAttribute(
        'style',
        `height: calc(100vh - 178px - ${targetHeight}px`,
      ),
    )
    const tabContents = document.querySelectorAll('.lif-tab-content')
    tabContents.forEach(el =>
      el.setAttribute(
        'style',
        `height: calc(100vh - 230px - ${targetHeight}px`,
      ),
    )
  }

  setShowItem() {
    const el = this.element.closest('.attribute')
    this.element.hidden = true
    el.querySelector('.show').hidden = false
    if (el.querySelector('.unit')) el.querySelector('.unit').hidden = false
  }

  setShowItemWithQuerySelector(el) {
    el = el.closest('.attribute')
    el.querySelector('.edit').hidden = true
    el.querySelector('.show').hidden = false
    if (el.querySelector('.unit')) el.querySelector('.unit').hidden = false
  }

  // 表示用(show)要素に指定の値をセットさせる
  setShowText(update_info) {
    const attributeElement = this.getAttributeElement()

    if (this.element.closest('.done-ratio')) {
      // done_ratio の場合
      const text = update_info[0].string // not-multi
      let el_bar = document.createElement('div')
      el_bar.classList.add('bar')
      el_bar.style.width = parseInt(text) + '%'
      el_bar.innerHTML = '&nbsp;'
      let el_label = document.createElement('span')
      el_label.classList.add('bar-label')
      el_label.innerText = text.replaceAll(/\s+/g, '')
      let el_barholder = document.createElement('div')
      el_barholder.classList.add('bar-holder')
      el_barholder.appendChild(el_bar)
      let el_parent = document.createElement('div')
      el_parent.classList.add('flex')
      el_parent.appendChild(el_barholder)
      el_parent.appendChild(el_label)
      attributeElement.querySelector('.show').innerText = ''
      attributeElement.querySelector('.show').appendChild(el_parent)
    } else if (this.element.closest('.type_project') && update_info[0].value) {
      // プロジェクトタイプで値が存在する場合（プロジェクトへのリンク付き）
      const text = update_info[0].string // not-multi
      const value = update_info[0].value
      const projectPath = document.getElementById('project-path').innerText
      let el_target = document.createElement('a')
      el_target.textContent = text
      el_target.target = '_blank'
      el_target.href = projectPath + value.toString()
      attributeElement.querySelector('.show').innerText = ''
      attributeElement.querySelector('.show').appendChild(el_target)
    } else if (this.element.closest('.type_user') && update_info[0].value) {
      // ユーザータイプで値が存在する場合（ユーザーへのリンク付き）
      const userPath = document.getElementById('user-path').innerText
      attributeElement.querySelector('.show').innerText = ''
      update_info.forEach((info, idx) => {
        if (idx > 0) attributeElement.querySelector('.show').append(', ')
        let el_target = document.createElement('a')
        el_target.textContent = info.string
        el_target.target = '_blank'
        el_target.href = userPath + info.value.toString()
        attributeElement.querySelector('.show').appendChild(el_target)
      })
    } else if (this.element.closest('.type_version') && update_info[0].value) {
      // バージョンで値が存在する場合（バージョンへのリンク付き）
      const versionPath = document.getElementById('version-path').innerText
      attributeElement.querySelector('.show').innerText = ''
      update_info.forEach((info, idx) => {
        if (idx > 0) attributeElement.querySelector('.show').append(', ')
        let el_target = document.createElement('a')
        el_target.textContent = info.string
        el_target.target = '_blank'
        el_target.href = versionPath + info.value.toString()
        attributeElement.querySelector('.show').appendChild(el_target)
      })
    } else if (this.element.closest('.type_issue') && update_info[0].value) {
      // チケット番号入力で値が存在する場合（チケットへのリンク付き）
      const text = update_info[0].string // not-multi
      const issuePath = document.getElementById('issue-path').innerText
      let el_target = document.createElement('a')
      el_target.textContent = `#${text}`
      el_target.target = '_blank'
      el_target.href = issuePath + update_info[0].value.toString()
      attributeElement.querySelector('.show').innerText = ''
      attributeElement.querySelector('.show').appendChild(el_target)
    } else if (this.element.closest('.type_link')) {
      // cf_link の場合
      const text = update_info[0].string // not-multi
      let el_target = document.createElement('a')
      el_target.textContent = text
      el_target.href = text
      attributeElement.querySelector('.show').innerText = ''
      attributeElement.querySelector('.show').appendChild(el_target)
    } else if (this.element.closest('.type_attachment')) {
      // cf_attachment の場合
      const text = update_info[0].string // not-multi
      if (
        this.element.querySelector('.attachments_fields input[type="hidden"]')
      ) {
        const attachment_id = parseInt(
          this.element.querySelector('.attachments_fields input[type="hidden"]')
            .value,
        )
        let el_link = document.createElement('a')
        el_link.href = '/attachments/' + attachment_id
        el_link.innerText = text
        let el_download = document.createElement('a')
        el_download.classList.add('icon-only')
        el_download.classList.add('icon-download')
        el_download.title = 'ダウンロード'
        el_download.href = '/attachments/download/' + attachment_id + '/' + text
        el_download.innerText = text
        let el_span = document.createElement('span')
        el_span.appendChild(el_link)
        el_span.appendChild(el_download)
        attributeElement.querySelector('.show').innerText = ''
        attributeElement.querySelector('.show').appendChild(el_span)
      } else {
        attributeElement.querySelector('.show').innerText = ''
      }
    } else if (this.element.querySelector('input[type="date"]')) {
      // 日付入力タイプの場合
      const text = update_info[0].string // not-multi
      attributeElement.querySelector('.show').innerText = text
      attributeElement.querySelector('.edit input[type="date"]').value = text
    } else {
      const text = update_info[0].string // not-multi
      attributeElement.querySelector('.show').innerText = text
    }
  }

  isSelfUserItemSelectCtrl(value) {
    return (
      this.element.querySelectorAll(`select option[value="${value}"]`).length >
      1
    )
  }

  selectedSelfUserValueSelectCtrl() {
    // select コントロールで<<自分>>を選択中の場合は値を返す TODO: checkbox タイプ
    const selfItem = Array.from(
      this.element.querySelectorAll('select option'),
    ).filter(el => el.selected && this.isSelfUserItemSelectCtrl(el.value))

    return selfItem[0] ? selfItem[0].value : null
  }

  transferSelfUserSelectCtrl() {
    if (this.selectedSelfUserValueSelectCtrl()) {
      this.element.querySelectorAll(
        `select option[value="${this.selectedSelfUserValueSelectCtrl()}"]`,
      )[1].selected = true
      this.element.querySelectorAll(
        `select option[value="${this.selectedSelfUserValueSelectCtrl()}"]`,
      )[0].selected = false
    }
  }

  isSelfUserItemRadioCtrl(value) {
    return this.element.querySelectorAll(`input[value="${value}"]`).length > 1
  }

  checkedSelfUserValueRadioCtrl() {
    // select コントロールで<<自分>>を選択中の場合は値を返す TODO: checkbox タイプ
    const selfItem = Array.from(this.element.querySelectorAll('input')).filter(
      el => el.checked && this.isSelfUserItemRadioCtrl(el.value),
    )

    return selfItem[0] ? selfItem[0].value : null
  }

  // NOTE: コントロール固有のバリデーションを発火させ、エラーメッセージを表示する
  controlTypicalValidation(event) {
    if (
      this.element.querySelector('input') &&
      !this.element.querySelector('input').validity.valid
    ) {
      const errors = {}
      const attributeName = this.element.getAttribute('data-attribute-name')
      errors[attributeName] = [
        this.element.querySelector('input').validationMessage,
      ]
      this.showValidationErrorToTarget(errors, attributeName)
      return
    } else {
      this.deleteTargetValidationErrors(this.element)
    }
  }

  transferSelfUserRadioCtrl() {
    if (this.checkedSelfUserValueRadioCtrl()) {
      this.element.querySelectorAll(
        `input[value="${this.checkedSelfUserValueRadioCtrl()}"]`,
      )[1].checked = true
      this.element.querySelectorAll(
        `input[value="${this.checkedSelfUserValueRadioCtrl()}"]`,
      )[0].checked = false
    }
  }

  // 編集モード中に入力内容キャンセルし表示モードに戻す
  resetItem() {
    const attributeNameBeforeValue = 'data-issue_item-before-value'
    const openMode = document.getElementById('open-mode').innerText
    if (openMode === 'new' || openMode === 'edit-all') return

    const prevValue = this.element.getAttribute(attributeNameBeforeValue)
    // 修正前の値に戻す
    const editElement = this.getUpdateElement()
    editElement.value = prevValue

    this.getAttributeElement().querySelector('div.show').hidden = false
    this.getAttributeElement().querySelector('div.edit').hidden = true
    if (this.getAttributeElement().querySelector('.unit'))
      this.getAttributeElement().querySelector('.unit').hidden = false

    this.setHasValidationErrors('false')
    this.deleteAllValidationErrors()

    if (!this.isInstalledActualDate()) return

    if (editElement.id === 'issue_status') {
      const {
        actualStartDate: editActualStartDate,
        actualDueDate: editActualDueDate,
      } = this.getActualDatesSelector(true)
      const {
        actualStartDate: showActualStartDate,
        actualDueDate: showActualDueDate,
      } = this.getActualDatesSelector(false)

      const actualStartDateBeforeValue =
        editActualStartDate.parentElement.getAttribute(attributeNameBeforeValue)
      const actualDueDateBeforeValue =
        editActualDueDate.parentElement.getAttribute(attributeNameBeforeValue)

      showActualStartDate.textContent = actualStartDateBeforeValue || '-'
      showActualDueDate.textContent = actualDueDateBeforeValue || '-'
    }
  }

  isInstalledActualDate() {
    return (
      document.getElementById('actual-date').dataset.actualDateInstalled ===
      'true'
    )
  }

  getActualDatesSelector(isEditMode) {
    const viewMode = this.getViewMode()

    const actualStartDateSelectorId = isEditMode
      ? 'issue_actual_start_date'
      : 'actual_start_date'
    const actualDueDateSelectorId = isEditMode
      ? 'issue_actual_due_date'
      : 'actual_due_date'

    const actualStartDate = document.querySelector(
      `#${viewMode} #${actualStartDateSelectorId}`,
    )
    const actualDueDate = document.querySelector(
      `#${viewMode} #${actualDueDateSelectorId}`,
    )

    return { actualStartDate, actualDueDate }
  }

  // 編集用(edit)要素に入力された値をもとに表示用(show)要素に値をセットする
  updateShowTextFromEdit() {
    let updateForShow = []

    if (this.isUpdateStringAndValueField()) {
      if (this.element.closest('.type_issue')) {
        updateForShow.push({
          string: this.element.querySelector('input').value,
          value: this.element.querySelector('input').value,
        })
      } else if (this.element.querySelector('select')) {
        if (this.element.closest('.type_user'))
          this.transferSelfUserSelectCtrl()
        updateForShow = Array.from(
          this.element.querySelector('select').selectedOptions,
        ).map(option => {
          return {
            string: option.innerText.trim(),
            value: parseInt(option.value),
          }
        })
      } else {
        if (this.element.closest('.type_user')) this.transferSelfUserRadioCtrl()
        updateForShow = Array.from(this.element.querySelectorAll('input'))
          .filter(el => el.checked)
          .map(el => {
            return {
              string: el.parentElement.innerText.trim(),
              value: parseInt(el.value),
            }
          })

        if (!updateForShow[0].value) updateForShow[0].string = ''
      }
    } else if (this.element.querySelector('select')) {
      // selectBox
      updateForShow.push({
        string: Array.from(this.element.querySelector('select').selectedOptions)
          .map(option => option.innerText.trim())
          .join(', '),
      })
    } else if (this.isCheckControl()) {
      // radioButton / checkBox
      updateForShow.push({
        string: Array.from(this.element.querySelectorAll('input'))
          .filter(el => el.checked)
          .map(el => el.parentElement.innerText.trim())
          .join(', '),
      })
      if (
        !Array.from(this.element.querySelectorAll('input'))
          .filter(el => el.checked)
          .map(el => el.value)[0]
      )
        updateForShow[0].string = ''
    } else if (this.element.querySelector('textarea')) {
      updateForShow.push({
        string: this.element.querySelector('textarea').value,
      })
    } else if (this.element.querySelector('.attachments_form')) {
      updateForShow.push({
        string: this.element.querySelector('.attachments_form input').value,
      })
    } else {
      // テキストボックス
      updateForShow.push({ string: this.element.querySelector('input').value })
    }

    this.setShowText(updateForShow)
  }

  setHasValidationErrors(value) {
    return document
      .getElementById('dialog-content')
      .setAttribute('data-has-validation-errors', value)
  }

  // 更新エラー時にいったん置き換えた表示値を元に戻す
  setPrevValueForShow(prevText, prevValue) {
    let updateForShow = []

    if (this.isMultiple() || this.isCheckControl()) {
      let texts = prevText.split(', ')
      let values = prevValue
        .replace(/\[/, '')
        .replace(/\]/, '')
        .replace(/\"/g, '')
        .split(', ')

      texts.forEach((text, idx) => {
        updateForShow.push({ string: text, value: values[idx] })
      })
    } else {
      const updateValue = this.isUpdateStringAndValueField()
        ? parseInt(prevValue)
        : null
      updateForShow.push({ string: prevText, value: updateValue })
    }

    return updateForShow
  }

  // 自身のチケットデータを取得する
  async fetchFormData(viewMode, selectTabId, journalMode) {
    const issueId = document.getElementById('issue-id').innerText
    const response = await get(`${this.getCompatibleIssuesUrl()}/${issueId}`)

    if (response.ok) {
      const dialogContent = document.getElementById('dialog-content')

      // （更新後などの）再表示タイミングで別のチケットに切り替えられていた場合は再描画しない
      const displayedIssueId = document.getElementById('issue-id').innerText
      if (displayedIssueId !== issueId) return

      dialogContent.innerHTML = await response.text
      this.element['mode'].setViewMode(viewMode, selectTabId, journalMode)
      this.dispatch('setRadioControlChecked')
    }
  }

  // 更新処理時に実開始/実終了日をreq bodyにセット
  setActualDate(formData) {
    if (!formData.get('issue[status_id]')) return

    const viewMode = this.getViewMode()

    const actualStartDate = document.querySelector(
      `#${viewMode} #actual_start_date`,
    )
    const actualDueDate = document.querySelector(
      `#${viewMode} #actual_due_date`,
    )

    const actualStartDateValue = actualStartDate?.textContent.trim()
    const actualDueDateValue = actualDueDate?.textContent.trim()

    if (isValidDateFormat(actualStartDateValue))
      formData.append('issue[actual_start_date]', actualStartDateValue)
    if (isValidDateFormat(actualDueDateValue))
      formData.append('issue[actual_due_date]', actualDueDateValue)
  }

  // 更新処理を実施
  async update(updateValue) {
    const formData = new FormData()
    const prevText = this.getShowText()
    const viewMode = this.getViewMode()
    const selectTabId = this.element['mode'].getSelectTabId()
    const prevValue = this.element.getAttribute('data-issue_item-before-value')
    const journalMode = this.element['mode'].getJournalMode()

    // show モードの表示を変更した edit モードの内容で上書き
    this.updateShowTextFromEdit()

    this.deleteAllValidationErrors()

    if (this.isAttachmentControl()) {
      if (updateValue.length > 1) {
        formData.append(this.fieldNameValue + '[filename]', updateValue[0])
        formData.append(this.fieldNameValue + '[token]', updateValue[1])
      } else {
        formData.append(this.fieldNameValue, updateValue)
      }
    } else if (
      (this.isMultiple() && updateValue.length) ||
      updateValue.length > 1
    ) {
      Array.from(updateValue).forEach(value => {
        formData.append(this.fieldNameValue + '[]', value)
      })
    } else if (
      this.fieldNameValue.includes(
        'issue[lychee_issue_board_issue_option_attributes]',
      )
    ) {
      const update_value_blocking = document.querySelector(
        `#${this.getViewMode()} dl.blocking select`,
      )?.value
      const update_value_point = document.querySelector(
        `#${this.getViewMode()} dl.point input`,
      )?.value
      formData.append(
        'issue[lychee_issue_board_issue_option_attributes][blocking]',
        update_value_blocking,
      )
      if (update_value_point)
        formData.append(
          'issue[lychee_issue_board_issue_option_attributes][point]',
          update_value_point,
        )
    } else {
      formData.append(this.fieldNameValue, updateValue[0] || '')
    }

    this.setActualDate(formData)

    const issueId = document.getElementById('issue-id').innerText
    const response = await patch(
      `${this.getCompatibleIssuesUrl()}/${issueId}`,
      { body: formData },
    )
    const message = await response.text
    const messageJson = JSON.parse(message)

    if (response.ok) {
      this.setHasValidationErrors('false')

      // NOTE: 更新の場合
      // グローバルオブジェクト連携
      const detail = { issueId: issueId, originalParam: { id: issueId } }
      const customEvent = new CustomEvent(CE_LIF_SUCCESS_SAVE, {
        cancelable: true,
        detail: detail,
      })
      if (!document.dispatchEvent(customEvent)) {
        // NOTE: 呼び出し側で event.preventDefault() が呼ばれた場合は処理を中断
        return
      }

      // データ再取得
      this.fetchFormData(viewMode, selectTabId, journalMode)
    } else {
      this.deleteAllValidationErrors()

      if (response.statusCode === 422) {
        // 修正前の状態を戻す
        this.element['mode'].setViewMode(viewMode, selectTabId, journalMode)
        this.setShowText(this.setPrevValueForShow(prevText, prevValue))
        this.getAttributeElement().querySelector('div.show').hidden = true
        const unitSection = this.getAttributeElement().querySelector('div.unit')
        if (unitSection) unitSection.hidden = true
        this.getAttributeElement().querySelector('div.edit').hidden = false

        // バリデーションエラーの表示
        let errors = messageJson.errors
        if (messageJson.errors.base && messageJson.errors.base.length > 0) {
          let newErrors = []
          // errors.base パスに CF エラーが集約されている場合
          errors.base.forEach(entry => {
            const key = entry.split(' ')[0]
            const message = entry.split(' ').slice(1).join(' ')
            newErrors[key] = [message]
          })
          // errors.base
          Object.keys(errors).forEach(key => {
            if (key != 'base') newErrors[key] = errors[key]
          })
          errors = newErrors
        }
        this.showValidationErrorToTarget(
          errors,
          this.fieldNameValue,
          updateValue,
        )
        this.getUpdateElement().focus()
      }
    }
  }

  // NOTE: バリデーションエラー表示（赤枠とエラーメッセージ）を初期化
  deleteAllValidationErrors() {
    document
      .querySelectorAll('.lif_validation_error')
      .forEach(t => t.classList.remove('lif_validation_error'))
    document
      .querySelectorAll('.lif_validation_error_message')
      .forEach(t => t.remove())
  }

  // NOTE: 対象のバリデーションエラー表示（赤枠とエラーメッセージ）を初期化
  deleteTargetValidationErrors(target) {
    const errors = target.querySelector('.lif_validation_error')
    if (errors) errors.classList.remove('lif_validation_error')
    const errorMessages = target.querySelector('.lif_validation_error_message')
    if (errorMessages) errorMessages.remove()
  }

  // NOTE: バリデーションエラーを表示する
  showValidationErrorToTarget(error, fieldName, updateValue = null) {
    this.deleteAllValidationErrors()

    const cfAttrName = this.getAttributeElement()
      .querySelector('.edit')
      .getAttribute('data-attribute-name')
    // 変更を加えた自身のフィールドに係るエラー
    const errorMessages = error[cfAttrName]
    const section = this.getAttributeElement()
    const editSection = section.querySelector('.edit')
    const label = editSection.getAttribute('data-label')
    const child = editSection.querySelector('select, input, textarea')
    // 赤枠をつける
    child.classList.add('lif_validation_error')

    // 変更を加えた自身以外のフィールドに係るエラー
    const otherErrors = Object.keys(error).find(key => key !== cfAttrName)
    if (!otherErrors) {
      // 自身の単独エラーの場合のみエラーメッセージを足す
      errorMessages?.forEach(errorMessage => {
        this.setHasValidationErrors('true')
        let errorMessageSection = document.createElement('p')
        errorMessageSection.classList.add('lif_validation_error_message')
        errorMessageSection.innerText = `${label} ${errorMessage}`
        editSection.append(errorMessageSection)
      })

      return
    }

    const issueId = document.getElementById('issue-id').innerText
    const trackerId =
      cfAttrName === 'tracker_id'
        ? updateValue[0]
        : document.querySelector(`#${this.getViewMode()} #issue_tracker`).value
    const projectId =
      cfAttrName === 'project_id'
        ? updateValue[0]
        : document.querySelector(`#${this.getViewMode()} #issue_project`).value
    const statusId =
      cfAttrName === 'status_id'
        ? updateValue[0]
        : document.querySelector(`#${this.getViewMode()} #issue_status`).value

    const customFieldValues = this.createCustomFieldValues(
      fieldName,
      cfAttrName,
      updateValue,
    )

    const issue = { trackerId, projectId, statusId }
    if (customFieldValues.length) issue['customFieldValues'] = customFieldValues
    if (!this.isCustomField(fieldName)) issue[cfAttrName] = updateValue[0]

    const params = { id: issueId, issue }
    // 変更を加えた自身以外でエラーがでている場合は、LIFの全編集モードを開く
    lif.open(params, error)
  }

  parentIdAutoComplete(event) {
    const fieldId = event.params.fieldId
    const url = event.params.url
    const options = event.params.options || {}

    $(document).ready(function () {
      $('.' + fieldId).autocomplete(
        $.extend(
          {
            source: url,
            minLength: 2,
            position: { collision: 'flipfit' },
            search: function () {
              $('.' + fieldId).addClass('ajax-loading')
            },
            response: function () {
              $('.' + fieldId).removeClass('ajax-loading')
            },
          },
          options,
        ),
      )
      $('.' + fieldId).addClass('autocomplete')
    })
  }

  createCustomFieldValues(fieldName, cfAttrName, updateValue) {
    const customFieldValues = this.getCustomFieldValues()

    if (!this.isCustomField(fieldName)) return customFieldValues

    const newCustomFieldValue = {
      id: Number(this.extractCustomFieldId(fieldName)),
      name: cfAttrName,
      value: updateValue.length > 1 ? updateValue : updateValue[0],
    }

    const index = customFieldValues.findIndex(
      item => item.id === newCustomFieldValue.id,
    )
    // 同じidがある場合は、変更後の値で上書きする
    if (index !== -1) {
      customFieldValues[index] = newCustomFieldValue
    } else {
      customFieldValues.push(newCustomFieldValue)
    }

    return customFieldValues
  }

  getFieldName(attributeName) {
    return document
      .querySelector(`[data-attribute-name=${attributeName}]`)
      ?.getAttribute('data-issue_item-field-name-value')
  }

  isCustomField(fieldName) {
    return fieldName.includes('issue[custom_field_values]')
  }

  extractCustomFieldId(fieldName) {
    const match = fieldName.match(/\[([0-9]+)\]/)
    return match ? match[1] : null
  }

  getCustomFieldValues() {
    const view = document.getElementById(this.getViewMode())
    const customFieldElements = view.querySelectorAll(
      "input[id^='issue_custom_field_values_'], select[id^='issue_custom_field_values_']",
    )

    let customFieldValues = []
    customFieldElements.forEach(element => {
      const elClosetAttachment = element.closest('.attachments_form')
      if (elClosetAttachment) return

      const elChildAttachment = element.querySelector('.attachments_form')
      const paramValue = elChildAttachment ? element.querySelector('input[type="hidden"]').value : element.value
      if (!paramValue) return

      customFieldValues.push({
        id: Number(this.extractCustomFieldId(element.name)),
        name: element
          .closest('.issue-item')
          .querySelector('.edit')
          .getAttribute('data-attribute-name'),
        value: paramValue,
      })
    })

    return customFieldValues
  }
}

const isValidDateFormat = dateString => {
  const datePattern = /^\d{4}-\d{2}-\d{2}$/
  return datePattern.test(dateString)
}
