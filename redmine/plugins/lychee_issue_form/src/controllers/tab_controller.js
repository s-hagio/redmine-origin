import { Controller } from '@hotwired/stimulus'

/*
 *  Lychee Issue Form
 *  タブ表示コントロール
 */
export default class extends Controller {
  static targets = [
    'tabViewHalf',
    'tabViewFull',
    'viewHalf',
    'viewFull',
    'tabSectionHalf',
    'tabSectionFull',
    'sectionHalf',
    'sectionFull',
    'issueFormContainer',
    'footer',
  ]

  changeView(event) {
    // NOTE: 通常表示<->分割表示タブクリック時に画面を切り替える
    this.toggleView(event.target.id)
    this.resetSectionAndTab(event.target.id)
  }

  setRadioControlChecked() {
    const viewMode = Array.from(
      document.getElementById('viewFull').classList,
    ).includes('hideView')
      ? 'viewHalf'
      : 'viewFull'
    const otherViewMode = viewMode === 'viewFull' ? 'viewHalf' : 'viewFull'
    document
      .querySelectorAll(`#${otherViewMode} input[type="radio"]`)
      .forEach(el => el.setAttribute('disabled', 'disabled'))
    document
      .querySelectorAll(`#${otherViewMode}  input[type="radio"]`)
      .forEach(el => el.removeAttribute('enabled'))
    document
      .querySelectorAll(`#${viewMode} input[type="radio"]`)
      .forEach(el => el.setAttribute('enabled', 'enabled'))
    document
      .querySelectorAll(`#${viewMode}  input[type="radio"]`)
      .forEach(el => el.removeAttribute('disabled'))
    document
      .querySelectorAll(
        'input[type="radio"][checked="checked"][enabled="enabled"]',
      )
      .forEach(el => (el.checked = true))
  }

  toggleView(newViewMode) {
    // NOTE: 通常表示<->分割表示タブ、チケットの内容、フォーム全体、フッタ、BODY タグなどの切り替え
    if (newViewMode === 'tab-half') {
      this.tabViewHalfTarget.classList.add('active')
      this.tabViewHalfTarget.classList.remove('deactive')
      this.tabViewFullTarget.classList.remove('active')
      this.tabViewFullTarget.classList.add('deactive')
      this.viewHalfTarget.classList.remove('hideView')
      this.viewFullTarget.classList.add('hideView')
      this.issueFormContainerTargets.forEach(target =>
        target.classList.add('lychee-issue-form__container_half'),
      )
      this.issueFormContainerTargets.forEach(target =>
        target.classList.remove('lychee-issue-form__container_full'),
      )
      this.footerTargets.forEach(footer =>
        footer.classList.remove('__fullsize'),
      )
      this.footerTargets.forEach(footer => footer.classList.add('__halfsize'))
      document.body.classList.remove('lychee-issue-form__body_full')
      document.body.classList.add('lychee-issue-form__body_half')
      document.body.setAttribute('id', 'half')
      this.setRadioControlChecked()
    } else if (newViewMode === 'tab-full') {
      this.tabViewHalfTarget.classList.remove('active')
      this.tabViewHalfTarget.classList.add('deactive')
      this.tabViewFullTarget.classList.add('active')
      this.tabViewFullTarget.classList.remove('deactive')
      this.viewHalfTarget.classList.add('hideView')
      this.viewFullTarget.classList.remove('hideView')
      this.issueFormContainerTargets.forEach(target =>
        target.classList.remove('lychee-issue-form__container_half'),
      )
      this.issueFormContainerTargets.forEach(target =>
        target.classList.add('lychee-issue-form__container_full'),
      )
      this.footerTargets.forEach(footer =>
        footer.classList.remove('__halfsize'),
      )
      this.footerTargets.forEach(footer => footer.classList.add('__fullsize'))
      document.body.classList.remove('lychee-issue-form__body_half')
      document.body.classList.add('lychee-issue-form__body_full')
      document.body.setAttribute('id', 'full')
      this.setRadioControlChecked()
    }
  }

  resetSectionAndTab(newViewMode) {
    // NOTE: タブとセクションの初期化
    this.closeAllSectionsAndTabs(
      this.sectionHalfTargets,
      this.tabSectionHalfTargets,
    )
    this.closeAllSectionsAndTabs(
      this.sectionFullTargets,
      this.tabSectionFullTargets,
    )
    // NOTE: 一度全部非表示にしてから開くものだけ有効にする
    if (newViewMode === 'tab-half') {
      this.tabSectionHalfTargets[0].setAttribute('aria-selected', 'true')
      this.tabSectionHalfTargets[0].setAttribute('tabindex', '0')
      this.sectionHalfTargets[0].classList.remove('hideView')
    } else if (newViewMode === 'tab-full') {
      this.tabSectionFullTargets[0].setAttribute('aria-selected', 'true')
      this.tabSectionFullTargets[0].setAttribute('tabindex', '0')
      this.sectionFullTargets[0].classList.remove('hideView')
    }
  }

  changeSection(event) {
    // NOTE: クリックされたタブによってセクションを切り替える
    const targetSection = event.currentTarget
    if (targetSection.getAttribute('role') === 'half-tab') {
      this.switchSection(
        targetSection,
        this.sectionHalfTargets,
        this.tabSectionHalfTargets,
      )
    } else if (targetSection.getAttribute('role') === 'full-tab') {
      this.switchSection(
        targetSection,
        this.sectionFullTargets,
        this.tabSectionFullTargets,
      )
    }
  }

  switchSection(targetSection, sections, tabs) {
    // NOTE: タブとセクションの切り替え
    const currentIndex = tabs.indexOf(targetSection)

    this.closeAllSectionsAndTabs(sections, tabs) // 一度全部非表示にしてから開くものだけ有効にする
    targetSection.setAttribute('aria-selected', 'true')
    targetSection.setAttribute('tabindex', '0')
    sections[currentIndex].classList.remove('hideView')
  }

  closeAllSectionsAndTabs(sections, tabs) {
    // NOTE: すべてのセクションとタブを閉じる
    sections.forEach(section => {
      section.classList.add('hideView')
    })
    tabs.forEach(tab => {
      tab.setAttribute('aria-selected', 'false')
      tab.setAttribute('tabindex', '-1')
    })
  }
}
