import { Controller } from '@hotwired/stimulus'

/*
 *  Lychee Issue Form
 *  ノブによる横幅リサイズ
 *  ※独自実装です
 */
export default class extends Controller {
  static targets = [
    'outsideKnob',
    'outside',
    'insideKnob',
    'inside',
    'otherside',
  ]
  static values = {
    outsideHold: Boolean,
    outsideXaxis: Number,
    insideHold: Boolean,
    insideXaxis: Number,
    othersideXaxis: Number,
  }

  startOutsideDrag(event) {
    // NOTE: 外側ノブのクリック時
    this.outsideKnobTargets.forEach(
      outside => (outside.style.cursor = 'grabbing'),
    )
    this.outsideHoldValue = true
    this.outsideXaxisValue = event.x
  }

  whileOutsideDrag(event) {
    // NOTE: 外側ノブのドラッグ時
    if (this.outsideHoldValue) {
      this.outsideKnobTargets.forEach(
        outside => (outside.style.cursor = 'grabbing'),
      )

      const diff = this.outsideXaxisValue - event.x
      this.outsideXaxisValue = event.x
      this.outsideTargets.forEach(outside => {
        outside.style.width = outside.offsetWidth + diff + 'px'
      })

      // NOTE: フォームの横幅に従って BODY の右側パディング（横スクロールの余白）もリサイズする
      document.body.style.paddingRight =
        this.outsideTargets[0].offsetWidth + 'px'
    }
  }

  endOutsideDrag(event) {
    // NOTE: 外側ノブのドラッグ終了時
    this.outsideKnobTargets.forEach(outside => (outside.style.cursor = 'grab'))
    this.outsideHoldValue = false
  }

  startInsideDrag(event) {
    // NOTE: 内側ノブのクリック時
    this.insideKnobTargets.forEach(inside => (inside.style.cursor = 'grabbing'))
    this.insideHoldValue = true
    this.insideXaxisValue = event.x
    this.othersideXaxisValue = this.othersideTargets[0].offsetWidth
  }

  whileInsideDrag(event) {
    // NOTE: 内側ノブのドラッグ時
    if (this.insideHoldValue) {
      this.insideKnobTargets.forEach(
        inside => (inside.style.cursor = 'grabbing'),
      )
      this.insideTargets.forEach(inside => {
        const insideDiff = this.insideXaxisValue - event.x
        this.insideXaxisValue = event.x
        const othersideDiff = +insideDiff // 左側もリサイズが必要
        this.othersideXaxisValue = this.othersideXaxisValue - othersideDiff
        inside.style.width = inside.offsetWidth + insideDiff + 'px'
        this.othersideTargets[0].style.width = this.othersideXaxisValue + 'px'
      })
    }
  }

  endInsideDrag(event) {
    // NOTE: 内側ノブのドラッグ終了時
    this.insideKnobTargets.forEach(inside => (inside.style.cursor = 'grab'))
    this.insideHoldValue = false
  }
}
