import { Controller } from '@hotwired/stimulus'
import { get, post, destroy } from '@rails/request.js'

// NOTE: カスタムイベント
const CE_LIF_BEFORE_SUBMIT = 'lifBeforeSubmit'
const CE_LIF_SUCCESS_SAVE = 'lifSuccessSave'

/*
 *  Lychee Issue Form
 *  作業時間コントロール
 */
export default class extends Controller {
  getCompatibleIssuesUrl() {
    return document.getElementById('request-url').dataset.fixedIssuesUrl
  }

  getCompatibleTimeEntriesUrl() {
    return document.getElementById('request-url').dataset.fixedTimeEntriesUrl
  }

  // 要素特定に向け ViewMode(Full/Half) を取得
  getViewMode() {
    return this.element['mode']?.getViewMode()
  }

  canCloseOrChangeForm() {
    if (this.hasFormChanged()) {
      const message =
        document.getElementById('issue-id').dataset.closeConfirmMessage
      return confirm(message)
    }
    return true
  }

  hasFormChanged() {
    // [バリデーションエラー] 作業時間タブのバリデーションエラーは除外する
    const validationErrors = Array.from(
      document.querySelectorAll('.lif_validation_error_message'),
    ).filter(element => !element.closest('#tab-content-time_entries'))
    if (validationErrors.length) return true

    // [編集時]
    const viewMode = this.getViewMode()

    // プロパティタブ（現在のモードの要素で編集中の添付ファイルがあるか, update前の添付ファイルがあるかを検証）
    const fileElements = Array.from(
      document.querySelectorAll(
        `#${viewMode} #tab-content-files .edit.file-info`,
      ),
    )
    const hasChangedFiles = !!fileElements.find(
      el => !el.hasAttribute('hidden'),
    )

    const addingFileElements = document.querySelector(
      `#${viewMode} #add-files-name`,
    )
    const hasUnupdatedFile = !!addingFileElements?.children.length

    // コメント/履歴タブ（現在のモードの要素で編集中があるかを検証）
    const noteElements = Array.from(
      document.querySelectorAll(
        `#${viewMode} #tab-comments_and_journals .edit`,
      ),
    )
    const hasChangedNotes = !!noteElements.find(
      el => !el.classList.contains('hideView'),
    )

    return hasChangedFiles || hasUnupdatedFile || hasChangedNotes
  }

  async fetchFormData(viewMode, selectTabId) {
    const issueId = document.getElementById('issue-id').innerText
    const response = await get(`${this.getCompatibleIssuesUrl()}/${issueId}`)

    if (response.ok) {
      const dialogContent = document.getElementById('dialog-content')

      dialogContent.innerHTML = await response.text
      this.element['mode'].setViewMode(viewMode, selectTabId, null)
    } else {
      // TODO: 失敗時の処理
    }
  }

  async delete() {
    const message =
      document.getElementById('issue-id').dataset.destroyConfirmMessage
    if (!confirm(message)) {
      return
    }

    const viewMode = this.getViewMode()
    const selectTabId = this.element['mode'].getSelectTabId()

    const issueId = document.getElementById('issue-id').innerText
    const timeEntryId = this.element.id.match(/timelog-delete-(\d+)/)[1]
    const response = await destroy(
      `${this.getCompatibleTimeEntriesUrl()}/${timeEntryId}`,
    )

    if (response.ok) {
      // データ再取得処理
      await this.fetchFormData(viewMode, selectTabId)

      // グローバルオブジェクト連携
      const detail = { issueId: issueId, originalParam: { id: issueId } }
      const customEvent = new CustomEvent(CE_LIF_SUCCESS_SAVE, {
        cancelable: true,
        detail: detail,
      })
      if (!document.dispatchEvent(customEvent)) {
        // NOTE: 呼び出し側で event.preventDefault() が呼ばれた場合は処理を中断
        return
      }
    } else {
      // エラー表示（暫定）
      const message = await response.text
      const messageJson = JSON.parse(message)
      await this.fetchFormData(viewMode, selectTabId)
      this.showNotification(messageJson.messages[0])
    }
  }

  async record(event) {
    if (!event.currentTarget.form.checkValidity()) return

    if (!this.canCloseOrChangeForm()) return // 他タブに編集中のものがある場合、確認ダイアログを表示

    const viewMode = this.getViewMode()
    const issueId = document.getElementById('issue-id').innerText

    const formData = new FormData()
    formData.append('back_url', `/issues/${issueId}/time_entries/new`)
    formData.append('issue_id', issueId)
    formData.append('time_entry[issue_id]', issueId)

    const formElements = document.querySelector(
      `#${viewMode} form[name="timelogForm"]`,
    ).elements
    for (const element of formElements) {
      if (element.name) {
        if (element.type === 'submit') continue
        if (element.type === 'radio' && !element.checked) continue
        if (element.type === 'checkbox' && !element.checked) continue
        if (element.type === 'file' && !element.value) continue
        if (element.type === 'select-multiple') {
          Array.from(element.selectedOptions).forEach(opt => {
            formData.append(element.name, opt.value)
          })
          continue
        }
        formData.append(element.name, element.value)
      }
    }

    const selectTabId = this.element['mode'].getSelectTabId()
    const response = await post(this.getCompatibleTimeEntriesUrl(), {
      body: formData,
    })

    if (response.ok) {
      // データ再取得処理
      await this.fetchFormData(viewMode, selectTabId)

      // グローバルオブジェクト連携
      const detail = { issueId: issueId, originalParam: { id: issueId } }
      const customEvent = new CustomEvent(CE_LIF_SUCCESS_SAVE, {
        cancelable: true,
        detail: detail,
      })
      if (!document.dispatchEvent(customEvent)) {
        // NOTE: 呼び出し側で event.preventDefault() が呼ばれた場合は処理を中断
        return
      }
    } else {
      const message = await response.text
      const messageJson = JSON.parse(message)

      // バリデーション
      if (response.statusCode === 422) {
        this.showValidationErrors(messageJson.errors)
      } else if (response.statusCode === 403) {
        this.showValidationErrors(messageJson)
      }

      const errorInputs = document.querySelectorAll('.lif_validation_error')
      const errorMessages = document.querySelectorAll(
        '.lif_validation_error_message',
      )
      if (errorInputs.length > 0) errorInputs[0].focus()
      else errorMessages[0].focus()
    }
  }

  // NOTE: バリデーションエラーを表示する
  showValidationErrors(errors) {
    // 赤枠とエラーメッセージを初期化
    document
      .querySelectorAll('.lif_validation_error')
      .forEach(t => t.classList.remove('lif_validation_error'))
    document
      .querySelectorAll('.lif_validation_error_message')
      .forEach(t => t.remove())

    const viewMode = this.getViewMode()
    const attributes = document.querySelectorAll(
      `#${viewMode} form[name=timelogForm] dl.attribute`,
    )
    Object.keys(errors).forEach(key => {
      if (key === 'base') {
        errors.base.forEach(entry => {
          const key = entry.split(' ')[0]
          const targetElement = document.querySelector(
            `#${viewMode} .tab-content-time_entries-form dl[data-attribute-key="${key}"]`,
          )

          if (targetElement) {
            const targetInput = targetElement
              .querySelector('dd')
              .querySelector('span, input, select, textarea')
            targetInput.classList.add('lif_validation_error')

            const newElement = document.createElement('div')
            newElement.setAttribute('tabindex', '0')
            newElement.classList.add('lif_validation_error_message')
            const newContent = document.createTextNode(entry)
            newElement.appendChild(newContent)
            targetElement.querySelector('dd').appendChild(newElement)
          } else {
            const newElement = document.createElement('div')
            newElement.setAttribute('tabindex', '0')
            newElement.classList.add('lif_validation_error_message')
            const newContent = document.createTextNode(entry)
            newElement.appendChild(newContent)
            document
              .querySelector(`#${viewMode} #tab-content-time_entries .messages`)
              .appendChild(newElement)
          }
        })

        return
      }

      if (key === 'messages') {
        const newElement = document.createElement('div')
        newElement.classList.add('lif_validation_error_message')
        const newContent = document.createTextNode(`${errors.messages[0]}`)
        newElement.appendChild(newContent)
        document
          .querySelector(`#${viewMode} #tab-content-time_entries .messages`)
          .appendChild(newElement)

        return
      }

      attributes.forEach(attr => {
        const attrKey = attr.getAttribute('data-attribute-key')
        const attrName = attr.getAttribute('data-attribute-name')

        if (key == attrKey) {
          const el = attr
            .querySelector('dd')
            .querySelector('span, input, select, textarea')
          el.classList.add('lif_validation_error')

          const newElement = document.createElement('div')
          newElement.classList.add('lif_validation_error_message')
          errors[key].forEach(error => {
            const newContent = document.createTextNode(`${attrName} ${error}`)
            newElement.appendChild(newContent)
            attr.querySelector('dd').appendChild(newElement)
          })
        }
      })
    })
  }
}
