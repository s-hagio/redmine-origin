# lychee_issue_form

System-wide issue view/edit form.

# Architectures

- Stimulus for frontend

# Getting started (via docker-compose)

## node
```
$ curl https://deb.nodesource.com/setup_18.x | bash
$ curl https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
$ echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
$ apt-get update && apt-get install -y build-essential nodejs yarn
* Change your own project path:
$ cd /usr/src/redmine/plugins/lychee_issue_form
$ yarn install
```

## Development

```
$ yarn dev
```

## Deployment

```
$ yarn build && rails redmine:plugins:migrate
* Restart Puma (if needed)
$ pumactl -P /usr/src/redmine/tmp/pids/server.pid restart
```

## 本番環境の設定

### LIFでチケット編集した未送信のJournalを5分ごとにメール送信する

Redmine のルートディレクトリで下記を実行すると cron 設定ファイルに書き込まれる。

```console
$ bundle exec whenever -f plugins/lychee_issue_form/config/schedule.rb --update-crontab
```
