# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

namespace :lychee_issue_form do
  post '/issues/new', :to => 'issues#new'
  resources :issues
  resources :journals, only: [:update]
  resources :time_entries, controller: 'timelog', only: [:create, :destroy]
  match '/issues/:id/quoted', to: 'journals#new', id: /\d+/, via: :post, as: 'quoted_issue'
  post :uploads, to: 'attachments#upload'
  resources :attachments, :only => [:destroy, :update]
  get 'settings', to: 'settings#index'
  post 'settings', to: 'settings#update'
end
