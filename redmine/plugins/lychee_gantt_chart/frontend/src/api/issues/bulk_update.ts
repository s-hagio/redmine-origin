import { BatchIssueError, BatchIssueParams, IssueRelatedResources } from '@/models/issue';

export type Methods = {
  put: {
    reqFormat: string;
    reqBody: {
      issues: BatchIssueParams[];
    };
    resBody: IssueRelatedResources & {
      errors: BatchIssueError[];
    };
  };
};
