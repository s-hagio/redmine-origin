import { Issue, IssueParams, IssueWithRelatedResources } from '@/models/issue';

export type Methods = {
  get: {
    query: {
      ids: string;
    };

    resBody: Issue[];
  };

  post: {
    reqFormat: string;
    reqBody: {
      issue: Partial<IssueParams>;
    };
    resBody: IssueWithRelatedResources;
  };
};
