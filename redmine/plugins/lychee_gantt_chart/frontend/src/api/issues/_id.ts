import { IssueParams, IssueWithRelatedResources } from '@/models/issue';

export type Methods = {
  get: {
    resBody: IssueWithRelatedResources;
  };

  put: {
    reqFormat: string;
    reqBody: {
      issue: Partial<IssueParams>;
    };
    resBody: IssueWithRelatedResources;
  };
};
