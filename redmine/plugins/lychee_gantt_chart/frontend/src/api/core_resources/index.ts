import { CoreResourceCollection } from '@/models/coreResource';
import { IssueQueryParams } from '@/models/query';

export type Methods = {
  get: {
    query?: IssueQueryParams;

    resBody: CoreResourceCollection;
  };
};
