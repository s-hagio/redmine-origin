import { AxiosError } from 'axios';

export type ClientConfig = {
  baseURL: string;
  formAuthenticityToken: string;
};

export type ErrorResponse =
  | {
      errors: string[] | string;
      error?: null;
    }
  | {
      errors?: null;
      error: string;
    };

type BaseAPIError = AxiosError<ErrorResponse>;

export type APIError = Omit<BaseAPIError, 'response'> & Required<Pick<BaseAPIError, 'response'>>;
