import { IssueQueryParams } from '@/models/query';
import {
  IssueRelationParams,
  IssueRelationWithRelatedResources,
  StoredIssueRelation,
} from '@/models/issueRelation';

export type Methods = {
  get: {
    query: IssueQueryParams;
    resBody: StoredIssueRelation[];
  };

  post: {
    reqFormat: string;
    reqBody: {
      issueRelation: IssueRelationParams;
    };
    resBody: IssueRelationWithRelatedResources;
  };
};
