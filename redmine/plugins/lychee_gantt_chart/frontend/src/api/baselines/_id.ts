import { Baseline, BaselineSummary, BaselineParams } from '@/models/baseline';

export type Methods = {
  get: {
    resBody: Baseline;
  };

  put: {
    reqFormat: string;
    reqBody: {
      baseline: Partial<BaselineParams>;
    };
    resBody: BaselineSummary;
  };

  delete: {
    resBody: null;
  };
};
