import { ProjectID } from '@/models/project';
import { BaselineSummary, BaselineParams } from '@/models/baseline';

export type Methods = {
  get: {
    query: {
      projectId: ProjectID;
    };

    resBody: BaselineSummary[];
  };

  post: {
    reqFormat: string;
    reqBody: {
      baseline: Partial<BaselineParams>;
    };
    resBody: BaselineSummary;
  };
};
