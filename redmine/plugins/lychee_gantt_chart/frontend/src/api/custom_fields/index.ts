import { CustomField } from '@/models/customField';

export type Methods = {
  get: {
    resBody: CustomField[];
  };
};
