import { AssociatedResourceCollection } from '@/models/associatedResource';

export type Methods = {
  get: {
    resBody: AssociatedResourceCollection;
  };
};
