import { isAxiosError } from 'axios';
import { APIError } from './types';

export const isAPIError = (error: unknown): error is APIError => {
  return isAxiosError(error);
};
