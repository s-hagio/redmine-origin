import { Project } from '@/models/project';

export type Methods = {
  get: {
    query: {
      ids: string;
    };

    resBody: Project[];
  };
};
