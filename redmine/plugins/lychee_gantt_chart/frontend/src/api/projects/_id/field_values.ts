import { FieldValueCollection } from '@/models/fieldValue';

export type Methods = {
  get: {
    resBody: FieldValueCollection;
  };
};
