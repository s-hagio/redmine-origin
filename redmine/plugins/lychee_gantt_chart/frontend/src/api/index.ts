import { stringify } from 'qs';
import axios from 'axios';
import aspida from '@aspida/axios';
import applyCaseMiddleware from 'axios-case-converter';
import api, { ApiInstance } from '@/api/$api';
import { ClientConfig } from './types';

export * from './types';
export * from './helpers';

const PATH_PREFIX = '/lychee_gantt/api';
const client = applyCaseMiddleware(axios.create(), {
  caseOptions: {
    stripRegexp: /[^A-Z0-9[\].]+/gi, // project.cf_1のようにドットが含まれる場合にドットが消えるのを防ぐ
  },
});
const aspidaClient = aspida(client, {
  paramsSerializer: {
    serialize: (params) => stringify(params, { arrayFormat: 'brackets' }),
  },
});

export const configureClient = (config: ClientConfig): void => {
  aspidaClient.baseURL = `${config.baseURL}${PATH_PREFIX}`;

  ['post', 'put', 'delete'].forEach((method) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    client.defaults.headers[method]['X-CSRF-Token'] = config.formAuthenticityToken;
  });
};

export const createClient = (): ApiInstance => {
  return api(aspidaClient) as ApiInstance;
};
