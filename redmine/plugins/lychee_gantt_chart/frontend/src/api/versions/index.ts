import { Version } from '@/models/version';

export type Methods = {
  get: {
    query: {
      ids: string;
    };

    resBody: Version[];
  };
};
