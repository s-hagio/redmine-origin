import { QueryFilterOption, QueryFilterValuesParams } from '@/models/queryFilter';

export type Methods = {
  get: {
    query?: QueryFilterValuesParams;
    resBody: QueryFilterOption[];
  };
};
