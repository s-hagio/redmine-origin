import { WorkflowRulesCollection } from '@/models/workflowRule';

export type Methods = {
  get: {
    resBody: WorkflowRulesCollection;
  };
};
