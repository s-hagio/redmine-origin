import * as React from 'react';
import { createRoot } from 'react-dom/client';
import { AppProvider } from '@/providers';
import { ClientConfig, configureClient } from '@/api';
import { Settings } from '@/models/setting';
import { CurrentUser } from '@/models/currentUser';
import { App } from '@/features/app';

const startLycheeGanttChart = (
  container: Element,
  currentUser: CurrentUser,
  clientConfig: ClientConfig,
  settings: Settings
) => {
  configureClient(clientConfig);

  const root = createRoot(container);

  root.render(
    <React.StrictMode>
      <AppProvider currentUser={currentUser} settings={settings}>
        <App />
      </AppProvider>
    </React.StrictMode>
  );
};

declare global {
  interface Window {
    startLycheeGanttChart: typeof startLycheeGanttChart;
  }
}

window.startLycheeGanttChart = startLycheeGanttChart;
