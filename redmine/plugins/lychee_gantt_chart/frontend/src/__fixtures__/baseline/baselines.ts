import { Baseline } from '@/models/baseline';
import { baselineSummary1 } from './baselineSummaries';

export const baseline1: Baseline = {
  ...baselineSummary1,
  projects: [{ id: 1, startDate: '2021-01-01', dueDate: '2021-01-30' }],
  versions: [{ id: 1, startDate: '2021-01-03', dueDate: '2021-01-20', completedPercent: 25 }],
  issues: [
    { id: 1, startDate: '2021-01-05', dueDate: '2021-01-09', doneRatio: 0, status: 'open' },
    { id: 2, startDate: '2021-01-08', dueDate: '2021-01-12', doneRatio: 10, status: 'open' },
  ],
};
