import { BaselineSummary } from '@/models/baseline';

let idCounter = 0;

const build = (attrs?: Partial<BaselineSummary>): BaselineSummary => ({
  id: ++idCounter,
  name: '',
  snappedAt: '2021-01-01T00:00:00Z',
  ...attrs,
});

export const baselineSummary1 = build({ name: 'Baseline 1' });
export const baselineSummary2 = build({ name: 'Baseline 2' });
export const baselineSummary3 = build({ name: 'Baseline 3' });
