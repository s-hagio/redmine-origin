import { Project } from '@/models/project';

export const rootProject: Readonly<Project> = {
  id: 1,
  identifier: 'root',
  name: 'Root project',
  rootId: 1,
  lft: 1,
  rgt: 12,
};

export const parentProject: Readonly<Project> = {
  id: 2,
  identifier: 'parent',
  name: 'Parent project',
  rootId: rootProject.id,
  lft: 2,
  rgt: 11,
};

export const mainProject: Readonly<Project> = {
  id: 3,
  identifier: 'main',
  name: 'Main project',
  rootId: rootProject.id,
  lft: 3,
  rgt: 10,
};

export const childProject1: Readonly<Project> = {
  id: 4,
  identifier: 'child1',
  name: 'Child project 1',
  rootId: rootProject.id,
  lft: 4,
  rgt: 7,
};

export const grandchildProject: Readonly<Project> = {
  id: 5,
  identifier: 'grandchild',
  name: 'Grandchild project',
  rootId: rootProject.id,
  lft: 5,
  rgt: 6,
};

export const childProject2: Readonly<Project> = {
  id: 6,
  identifier: 'child2',
  name: 'Child project 2',
  rootId: rootProject.id,
  lft: 8,
  rgt: 9,
};

export const otherTreeProject: Readonly<Project> = {
  id: 101,
  identifier: 'other_tree',
  name: 'Other tree project',
  rootId: 101,
  lft: 101,
  rgt: 102,
};
