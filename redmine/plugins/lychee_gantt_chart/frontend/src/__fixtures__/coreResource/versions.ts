import { pick } from '@/utils/object';
import { Version, VersionSharing, VersionStatus } from '@/models/version';
import { Project } from '@/models/project';
import { mainProject, grandchildProject } from './projects';

let idCounter = 0;

const buildVersion = (
  project: Project,
  sharing: VersionSharing,
  status: VersionStatus = VersionStatus.Open
): Readonly<Version> => ({
  id: ++idCounter,
  projectId: project.id,
  name: `${project.name} version shared by ${sharing}`,
  status,
  sharing,
  projectTreeValue: extractTreeValue(project),
  isClosed: false,
});

const extractTreeValue = (project: Project) => pick(project, ['rootId', 'lft', 'rgt']);

export const versionSharedByNone = buildVersion(mainProject, VersionSharing.None);
export const versionSharedByDescendants = buildVersion(mainProject, VersionSharing.Descendants);
export const versionSharedByHierarchy = buildVersion(mainProject, VersionSharing.Hierarchy);
export const versionSharedByTree = buildVersion(mainProject, VersionSharing.Tree);
export const versionSharedBySystem = buildVersion(mainProject, VersionSharing.System);

export const grandchildProjectVersionSharedByHierarchy = buildVersion(
  grandchildProject,
  VersionSharing.Hierarchy
);
export const grandchildProjectVersionSharedByTree = buildVersion(grandchildProject, VersionSharing.Tree);
