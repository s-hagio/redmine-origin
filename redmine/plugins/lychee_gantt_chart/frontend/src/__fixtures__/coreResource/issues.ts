import { Issue } from '@/models/issue';
import { grandchildProject, mainProject } from './projects';
import { versionSharedByHierarchy } from './versions';

const DEFAULTS = {
  fixedVersionId: null,
  trackerId: 1,
  statusId: 1,
  categoryId: null,
  priorityId: 3,
  assignedToId: null,
  authorId: 1,
  parentId: null,
  startDate: null,
  dueDate: null,
  doneRatio: null,
  estimatedHours: null,
  isClosed: false,
  isPrivate: false,
  customFieldValues: {},
};

export const rootIssue: Readonly<Issue> = {
  ...DEFAULTS,
  id: 1,
  projectId: mainProject.id,
  subject: 'root',
  rootId: 1,
  lft: 1,
  rgt: 10,
};

export const parentIssue: Readonly<Issue> = {
  ...DEFAULTS,
  id: 2,
  projectId: mainProject.id,
  subject: 'parent',
  parentId: rootIssue.id,
  rootId: rootIssue.id,
  lft: 2,
  rgt: 9,
};

export const childIssue: Readonly<Issue> = {
  ...DEFAULTS,
  id: 3,
  projectId: mainProject.id,
  subject: 'child',
  parentId: parentIssue.id,
  rootId: rootIssue.id,
  lft: 3,
  rgt: 8,
};

export const grandchildIssue: Readonly<Issue> = {
  ...DEFAULTS,
  id: 4,
  projectId: mainProject.id,
  subject: 'grandchild 1',
  parentId: childIssue.id,
  rootId: rootIssue.id,
  lft: 4,
  rgt: 5,
};

export const leafIssue = grandchildIssue;

export const grandchildIssue2: Readonly<Issue> = {
  ...DEFAULTS,
  id: 5,
  projectId: mainProject.id,
  subject: 'grandchild 2',
  parentId: childIssue.id,
  rootId: rootIssue.id,
  lft: 6,
  rgt: 7,
};

export const otherTreeIssue: Readonly<Issue> = {
  ...DEFAULTS,
  id: 101,
  projectId: mainProject.id,
  subject: 'other tree',
  rootId: 101,
  lft: 101,
  rgt: 102,
};

export const versionIssue: Readonly<Issue> = {
  ...DEFAULTS,
  id: 111,
  projectId: mainProject.id,
  fixedVersionId: versionSharedByHierarchy.id,
  subject: 'main project version issue',
  rootId: 111,
  lft: 111,
  rgt: 112,
};

export const grandchildProjectIssue: Readonly<Issue> = {
  ...DEFAULTS,
  id: 201,
  projectId: grandchildProject.id,
  subject: 'grandchild project #101',
  rootId: 201,
  lft: 201,
  rgt: 202,
};
