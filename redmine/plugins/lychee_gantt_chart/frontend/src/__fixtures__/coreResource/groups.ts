import { getGroupId, Group } from '@/models/group';
import { childIssue, parentIssue, rootIssue } from '../coreResource/';

export const newStatusGroup: Group = {
  id: getGroupId('status', 1),
  value: 1,
  content: 1,
  issueIds: [rootIssue.id, parentIssue.id, childIssue.id],
};

export const inProgressStatusGroup: Group = {
  id: getGroupId('status', 2),
  value: 2,
  content: 2,
  issueIds: [],
};

export const resolvedIssueStatusGroup: Group = {
  id: getGroupId('status', 3),
  value: 3,
  content: 3,
  issueIds: [],
};

export const closedIssueStatusGroup: Group = {
  id: getGroupId('status', 4),
  value: 4,
  content: 4,
  issueIds: [],
};
