import { AssociatedPrincipal } from '@/models/associatedResource';

export const user1: AssociatedPrincipal = {
  id: 1,
  name: 'User #1',
  isGroup: false,
  isLocked: false,
  position: 1,
};

export const user2: AssociatedPrincipal = {
  id: 2,
  name: 'User #2',
  isGroup: false,
  isLocked: false,
  position: 2,
};

export const user3: AssociatedPrincipal = {
  id: 3,
  name: 'User #3',
  isGroup: false,
  isLocked: false,
  position: 3,
};

export const group1: AssociatedPrincipal = {
  id: 11,
  name: 'Group #1',
  isGroup: true,
  isLocked: false,
  position: 104,
};

export const group2: AssociatedPrincipal = {
  id: 12,
  name: 'Group #2',
  isGroup: true,
  isLocked: false,
  position: 105,
};

export const group3: AssociatedPrincipal = {
  id: 13,
  name: 'Group #4',
  isGroup: true,
  isLocked: false,
  position: 106,
};

export const userWithAvatar: AssociatedPrincipal = {
  id: 101,
  name: 'User with avatar',
  isGroup: false,
  avatar: '<img alt="" title="User with avatar" src="https://example.com/avatar_url">',
  isLocked: false,
  position: 11,
};

export const lockedUser: AssociatedPrincipal = {
  id: 102,
  name: 'Locked user',
  isGroup: false,
  isLocked: true,
  position: 12,
};
