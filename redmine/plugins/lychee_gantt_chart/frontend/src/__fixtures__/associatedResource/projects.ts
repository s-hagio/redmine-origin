import { pick } from '@/utils/object';
import { AssociatedProject } from '@/models/associatedResource';
import { ProjectCore } from '@/models/project';
import { childProject1, childProject2, mainProject, rootProject } from '../coreResource';

const build = (baseProject: ProjectCore): AssociatedProject => {
  return {
    ...pick(baseProject, ['id', 'name', 'rootId', 'lft', 'rgt']),
    trackerIds: [1, 2, 3],
  };
};

export const associatedRootProject = build(rootProject);
export const associatedMainProject = build(mainProject);
export const associatedChildProject1 = build(childProject1);
export const associatedChildProject2 = build(childProject2);
