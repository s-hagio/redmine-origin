import { indexBy } from '@/utils/array';
import {
  AssociatedResourceCollection,
  IndexedAssociatedResourceCollection,
} from '@/models/associatedResource';
import { associatedMainProject, associatedRootProject } from './projects';
import { associatedVersionSharedByNone, associatedVersionSharedByTree } from './versions';
import { user1, user2, user3, group1, group2, group3 } from './principals';
import { bugTracker, featureTracker, supportTracker } from './trackers';
import {
  newIssueStatus,
  inProgressIssueStatus,
  resolvedIssueStatus,
  closedIssueStatus,
} from './issueStatuses';
import { highIssuePriority, normalIssuePriority, lowIssuePriority } from './issuePriorities';
import { issueCategory1, issueCategory2, issueCategory3 } from './issueCategories';

export const associatedResourceCollection: Readonly<AssociatedResourceCollection> = {
  projects: [associatedRootProject, associatedMainProject],
  versions: [associatedVersionSharedByNone, associatedVersionSharedByTree],
  trackers: [bugTracker, featureTracker, supportTracker],
  principals: [user1, user2, user3, group1, group2, group3],
  issueStatuses: [newIssueStatus, inProgressIssueStatus, resolvedIssueStatus, closedIssueStatus],
  issuePriorities: [highIssuePriority, normalIssuePriority, lowIssuePriority],
  issueCategories: [issueCategory1, issueCategory2, issueCategory3],
};

export const indexedAssociatedResourceCollection: IndexedAssociatedResourceCollection = {
  projectsById: indexBy(associatedResourceCollection.projects, 'id'),
  versionsById: indexBy(associatedResourceCollection.versions, 'id'),
  trackersById: indexBy(associatedResourceCollection.trackers, 'id'),
  principalsById: indexBy(associatedResourceCollection.principals, 'id'),
  issueStatusesById: indexBy(associatedResourceCollection.issueStatuses, 'id'),
  issuePrioritiesById: indexBy(associatedResourceCollection.issuePriorities, 'id'),
  issueCategoriesById: indexBy(associatedResourceCollection.issueCategories, 'id'),
};
