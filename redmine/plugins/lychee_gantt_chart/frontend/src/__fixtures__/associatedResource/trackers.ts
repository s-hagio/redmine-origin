import { AssociatedTracker } from '@/models/associatedResource';

export const bugTracker: AssociatedTracker = {
  id: 1,
  name: 'Bug',
  defaultStatusId: 1,
  disabledCoreFields: [],
};

export const featureTracker: AssociatedTracker = {
  id: 2,
  name: 'Feature',
  defaultStatusId: 1,
  disabledCoreFields: [],
};

export const supportTracker: AssociatedTracker = {
  id: 3,
  name: 'Support',
  defaultStatusId: 2,
  disabledCoreFields: [],
};
