import { pick } from '@/utils/object';
import { AssociatedVersion } from '@/models/associatedResource';
import { Version } from '@/models/version';
import { versionSharedByNone, versionSharedByTree } from '@/__fixtures__';

const associatedVersionPropKeys: (keyof Version)[] = [
  'id',
  'name',
  'sharing',
  'isClosed',
  'projectTreeValue',
];

export const associatedVersionSharedByNone: AssociatedVersion = {
  ...pick(versionSharedByNone, associatedVersionPropKeys),
  isLocked: false,
};

export const associatedVersionSharedByTree: AssociatedVersion = {
  ...pick(versionSharedByTree, associatedVersionPropKeys),
  isLocked: false,
};
