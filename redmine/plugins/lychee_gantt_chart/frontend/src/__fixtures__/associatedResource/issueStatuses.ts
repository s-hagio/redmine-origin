import { AssociatedIssueStatus } from '@/models/associatedResource';

export const newIssueStatus: AssociatedIssueStatus = {
  id: 1,
  name: 'New',
  isClosed: false,
  color: '#ff1',
};

export const inProgressIssueStatus: AssociatedIssueStatus = {
  id: 2,
  name: 'Doing',
  isClosed: false,
  color: '#ff2',
};

export const resolvedIssueStatus: AssociatedIssueStatus = {
  id: 3,
  name: 'Resolve',
  isClosed: false,
  color: '#ff3',
};

export const closedIssueStatus: AssociatedIssueStatus = {
  id: 4,
  name: 'Closed',
  isClosed: true,
  color: '#ff4',
};
