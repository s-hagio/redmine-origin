import { AssociatedIssuePriority } from '@/models/associatedResource';

export const highIssuePriority: AssociatedIssuePriority = {
  id: 1,
  name: 'High',
  isActive: true,
  isDefault: false,
};

export const normalIssuePriority: AssociatedIssuePriority = {
  id: 2,
  name: 'Normal',
  isActive: true,
  isDefault: true,
};

export const lowIssuePriority: AssociatedIssuePriority = {
  id: 3,
  name: 'Low',
  isActive: true,
  isDefault: false,
};

export const inactiveIssuePriority: AssociatedIssuePriority = {
  id: 4,
  name: 'Inactive',
  isActive: false,
  isDefault: false,
};

export const activeIssuePriorities = [highIssuePriority, normalIssuePriority, lowIssuePriority];
export const inactiveIssuePriorities = [inactiveIssuePriority];
