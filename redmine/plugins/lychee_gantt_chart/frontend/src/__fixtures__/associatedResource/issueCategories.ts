import { AssociatedIssueCategory } from '@/models/associatedResource';
import { associatedMainProject, associatedRootProject } from './projects';

export const issueCategory1: AssociatedIssueCategory = {
  id: 1,
  name: 'Category #1',
  projectId: associatedMainProject.id,
};

export const issueCategory2: AssociatedIssueCategory = {
  id: 2,
  name: 'Category #2',
  projectId: associatedMainProject.id,
};

export const issueCategory3: AssociatedIssueCategory = {
  id: 3,
  name: 'Category #3',
  projectId: associatedRootProject.id,
};
