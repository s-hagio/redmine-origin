import { CustomField, CustomFieldFormat, CustomFieldID } from '@/models/customField';
import { bugTracker, featureTracker, supportTracker } from '../associatedResource';

let idCounter = 0;

const build = (format: CustomFieldFormat, attrs: Partial<CustomField>): Readonly<CustomField> => {
  const id = ++idCounter;

  return {
    id,
    name: `cf_${id}`,
    label: `カスタムフィールド #${id}`,
    format,
    minLength: null,
    maxLength: null,
    isFullTextFormatting: false,
    isMultiple: false,
    isBulkEditSupported: true,
    groupable: false,
    sortable: false,
    userRoleIds: [],
    isForAll: true,
    projectIds: [],
    trackerIds: [bugTracker.id, featureTracker.id, supportTracker.id],
    ...attrs,
  };
};

export const enumerationCustomField = build(CustomFieldFormat.Enumeration, {
  label: 'キー・バリューリストCF',
});

export const stringCustomField = build(CustomFieldFormat.String, {
  label: 'テキストCF',
});

export const formattableStringCustomField = build(CustomFieldFormat.String, {
  label: '書式適用のテキストCF',
  isFullTextFormatting: true,
});

export const versionCustomField = build(CustomFieldFormat.Version, {
  label: 'バージョンCF',
});

export const attachmentCustomField = build(CustomFieldFormat.Attachment, {
  label: 'ファイルCF',
});

export const userCustomField = build(CustomFieldFormat.User, {
  label: 'ユーザーCF',
});

export const listCustomField = build(CustomFieldFormat.List, {
  label: 'リストCF',
});

export const linkCustomField = build(CustomFieldFormat.Link, {
  label: 'リンクCF',
});

export const floatCustomField = build(CustomFieldFormat.Float, {
  label: '小数CF',
});

export const intCustomField = build(CustomFieldFormat.Int, {
  label: '整数CF',
});

export const dateCustomField = build(CustomFieldFormat.Date, {
  label: '日付CF',
});

export const boolCustomField = build(CustomFieldFormat.Bool, {
  label: '真偽値CF',
});

export const textCustomField = build(CustomFieldFormat.Text, {
  label: '長いテキストCF',
});

export const formattableTextCustomField = build(CustomFieldFormat.Text, {
  label: '書式適用の長いテキストCF',
  isFullTextFormatting: true,
});

export const customFields: ReadonlyMap<CustomFieldID, CustomField> = new Map(
  [
    enumerationCustomField,
    stringCustomField,
    versionCustomField,
    attachmentCustomField,
    userCustomField,
    listCustomField,
    linkCustomField,
    floatCustomField,
    intCustomField,
    dateCustomField,
    boolCustomField,
    textCustomField,
  ].map((cf) => [cf.id, cf])
);
