import { CustomFieldDefinition } from '@/models/fieldDefinition';
import { CustomField } from '@/models/customField';
import {
  attachmentCustomField,
  boolCustomField,
  dateCustomField,
  enumerationCustomField,
  floatCustomField,
  formattableStringCustomField,
  formattableTextCustomField,
  intCustomField,
  linkCustomField,
  listCustomField,
  stringCustomField,
  textCustomField,
  userCustomField,
  versionCustomField,
} from '../customField';

const build = (customField: CustomField): CustomFieldDefinition => ({
  name: `cf_${customField.id}`,
  label: customField.label,
  valuePath: `customFieldValues.${customField.id}`,
  contentPath: `formattedCustomFieldValues.${customField.id}`,
  customFieldId: customField.id,
  format: customField.format,
  maxLength: customField.maxLength,
  minLength: customField.minLength,
  isFullTextFormatting: customField.isFullTextFormatting,
  isMultiple: customField.isMultiple,
  isBulkEditSupported: customField.isBulkEditSupported,
  sortable: customField.sortable,
  groupable: customField.groupable,
});

export const enumerationCustomFieldDefinition = build(enumerationCustomField);
export const stringCustomFieldDefinition = build(stringCustomField);
export const versionCustomFieldDefinition = build(versionCustomField);
export const attachmentCustomFieldDefinition = build(attachmentCustomField);
export const userCustomFieldDefinition = build(userCustomField);
export const listCustomFieldDefinition = build(listCustomField);
export const linkCustomFieldDefinition = build(linkCustomField);
export const floatCustomFieldDefinition = build(floatCustomField);
export const intCustomFieldDefinition = build(intCustomField);
export const dateCustomFieldDefinition = build(dateCustomField);
export const boolCustomFieldDefinition = build(boolCustomField);
export const textCustomFieldDefinition = build(textCustomField);
export const formattedStringCustomFieldDefinition = build(formattableStringCustomField);
export const formattedTextCustomFieldDefinition = build(formattableTextCustomField);
