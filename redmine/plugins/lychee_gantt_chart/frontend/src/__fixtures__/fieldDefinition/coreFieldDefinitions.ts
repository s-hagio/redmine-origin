import { CoreFieldDefinition, FieldFormat } from '@/models/fieldDefinition';

export const idFieldDefinition: CoreFieldDefinition = {
  name: 'id',
  label: '#',
  valuePath: 'id',
  format: FieldFormat.Id,
};

export const projectFieldDefinition: CoreFieldDefinition = {
  name: 'project',
  label: 'Project',
  valuePath: 'projectId',
  format: FieldFormat.Project,
  groupable: true,
};

export const versionFieldDefinition: CoreFieldDefinition = {
  name: 'fixed_version',
  label: 'Version',
  valuePath: 'fixedVersionId',
  format: FieldFormat.Version,
  groupable: true,
};

export const trackerFieldDefinition: CoreFieldDefinition = {
  name: 'tracker',
  label: 'Tracker',
  valuePath: 'trackerId',
  format: FieldFormat.Tracker,
  groupable: true,
};

export const statusFieldDefinition: CoreFieldDefinition = {
  name: 'status',
  label: 'Status',
  valuePath: 'statusId',
  format: FieldFormat.IssueStatus,
  groupable: true,
};

export const priorityFieldDefinition: CoreFieldDefinition = {
  name: 'priority',
  label: 'Priority',
  valuePath: 'priorityId',
  format: FieldFormat.IssuePriority,
  groupable: true,
};

export const categoryFieldDefinition: CoreFieldDefinition = {
  name: 'category',
  label: 'Category',
  valuePath: 'categoryId',
  format: FieldFormat.IssueCategory,
  groupable: true,
};

export const issueSubjectFieldDefinition: CoreFieldDefinition = {
  name: 'subject',
  label: 'Subject',
  valuePath: 'subject',
  format: FieldFormat.String,
};

export const parentIssueFieldDefinition: CoreFieldDefinition = {
  name: 'parent',
  label: 'Parent issue',
  valuePath: 'parentId',
  format: FieldFormat.ParentIssueId,
};

export const parentSubjectFieldDefinition: CoreFieldDefinition = {
  name: 'parent.subject',
  label: 'Parent task subject',
  valuePath: 'parentIssueSubject',
  format: FieldFormat.String,
};

export const startDateFieldDefinition: CoreFieldDefinition = {
  name: 'start_date',
  label: 'Start date',
  valuePath: 'startDate',
  format: FieldFormat.Date,
  groupable: true,
};

export const assignedToFieldDefinition: CoreFieldDefinition = {
  name: 'assigned_to',
  label: 'Assigned to',
  valuePath: 'assignedToId',
  format: FieldFormat.Principal,
  groupable: true,
};

export const createdOnFieldDefinition: CoreFieldDefinition = {
  name: 'created_on',
  label: 'Created on',
  valuePath: 'createdOn',
  format: FieldFormat.DateTime,
  groupable: true,
  readonly: true,
};

export const descriptionFieldDefinition: CoreFieldDefinition = {
  name: 'description',
  label: 'Description',
  valuePath: 'description',
  contentPath: 'formattedDescription',
  format: FieldFormat.FormattableText,
};

export const estimatedHoursFieldDefinition: CoreFieldDefinition = {
  name: 'estimated_hours',
  label: 'Estimated hours',
  valuePath: 'estimatedHours',
  format: FieldFormat.Float,
};

export const doneRatioFieldDefinition: CoreFieldDefinition = {
  name: 'done_ratio',
  label: 'Done ratio',
  valuePath: 'doneRatio',
  format: FieldFormat.DoneRatio,
};

export const isPrivateFieldDefinition: CoreFieldDefinition = {
  name: 'is_private',
  label: 'Is private',
  valuePath: 'isPrivate',
  format: FieldFormat.Bool,
};
