import { Issue } from '@/models/issue';
import { IssueRow, RowResourceType } from '@/features/row';
import {
  rootIssue,
  parentIssue,
  childIssue,
  grandchildIssue,
  grandchildIssue2,
  otherTreeIssue,
  versionIssue,
  grandchildProjectIssue,
} from '../coreResource/issues';

const build = (issue: Issue, partialAttributes?: Partial<IssueRow>): Readonly<IssueRow> => ({
  id: `issue-${issue.id}`,
  resourceId: issue.id,
  resourceType: RowResourceType.Issue,
  depth: -1,
  isCollapsible: true,
  subtreeRootIds: [],
  ...partialAttributes,
});

export const rootIssueRow = build(rootIssue);
export const parentIssueRow = build(parentIssue);
export const childIssueRow = build(childIssue);
export const grandchildIssueRow = build(grandchildIssue, { isCollapsible: false });
export const grandchildIssue2Row = build(grandchildIssue2, { isCollapsible: false });
export const otherTreeIssueRow = build(otherTreeIssue, { isCollapsible: false });
export const versionIssueRow = build(versionIssue, { isCollapsible: false });
export const grandchildProjectIssueRow = build(grandchildProjectIssue, { isCollapsible: false });
