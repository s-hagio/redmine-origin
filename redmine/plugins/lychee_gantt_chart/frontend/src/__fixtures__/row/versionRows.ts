import { Project } from '@/models/project';
import { Version } from '@/models/version';
import { RowResourceType, VersionRow } from '@/features/row';
import {
  rootProject,
  parentProject,
  mainProject,
  childProject1,
  grandchildProject,
  childProject2,
} from '../coreResource/projects';
import { versionSharedByHierarchy } from '../coreResource/versions';

const build = (
  project: Project,
  version: Version,
  partialAttributes?: Partial<VersionRow>
): Readonly<VersionRow> => ({
  id: `version-${project.id}-${version.id}`,
  resourceId: version.id,
  resourceType: RowResourceType.Version,
  depth: -1,
  isCollapsible: true,
  subtreeRootIds: [],
  ...partialAttributes,
});

const version = versionSharedByHierarchy;

export { version as rowVersion };

export const rootProjectVersionRow = build(rootProject, version, { isCollapsible: false });
export const parentProjectVersionRow = build(parentProject, version, { isCollapsible: false });
export const mainProjectVersionRow = build(mainProject, version);
export const childProject1VersionRow = build(childProject1, version, { isCollapsible: false });
export const grandchildProjectVersionRow = build(grandchildProject, version, { isCollapsible: false });
export const childProject2VersionRow = build(childProject2, version, { isCollapsible: false });
