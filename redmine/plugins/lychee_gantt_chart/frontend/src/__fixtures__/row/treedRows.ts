import { Row } from '@/features/row';
import { rootProjectRow, mainProjectRow, parentProjectRow } from './projectRows';
import {
  childIssueRow,
  grandchildIssue2Row,
  grandchildIssueRow,
  parentIssueRow,
  rootIssueRow,
} from './issueRows';

export const [
  treedRootProjectRow,
  treedParentProjectRow,
  treedMainProjectRow,
  treedRootIssueRow,
  treedParentIssueRow,
  treedChildIssueRow,
] = [rootProjectRow, parentProjectRow, mainProjectRow, rootIssueRow, parentIssueRow, childIssueRow].reduce<
  Row[]
>((rows, row) => {
  const previous = rows.at(-1);
  const subtreeRootIds = previous ? [...previous.subtreeRootIds, previous.id] : [];

  return [...rows, { ...row, subtreeRootIds }];
}, []);

export const [treedGrandchildIssueRow, treedGrandchildIssue2Row] = [
  grandchildIssueRow,
  grandchildIssue2Row,
].map((row) => ({
  ...row,
  subtreeRootIds: [...treedChildIssueRow.subtreeRootIds, treedChildIssueRow.id],
}));

export const treedRows = [
  treedRootProjectRow,
  treedParentProjectRow,
  treedMainProjectRow,
  treedRootIssueRow,
  treedParentIssueRow,
  treedChildIssueRow,
  treedGrandchildIssueRow,
  treedGrandchildIssue2Row,
];
