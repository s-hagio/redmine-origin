import { Group } from '@/models/group';
import { GroupRow, RowResourceType } from '@/features/row';
import {
  closedIssueStatusGroup,
  inProgressStatusGroup,
  newStatusGroup,
  resolvedIssueStatusGroup,
} from '../coreResource';

const build = (group: Group): GroupRow => {
  return {
    id: `${RowResourceType.Group}-${group.id}`,
    resourceType: RowResourceType.Group,
    resourceId: group.id,
    depth: -1,
    isCollapsible: true,
    subtreeRootIds: [],
  };
};

export const newStatusGroupRow = build(newStatusGroup);
export const inProgressStatusGroupRow = build(inProgressStatusGroup);
export const resolvedIssueStatusGroupRow = build(resolvedIssueStatusGroup);
export const closedIssueStatusGroupRow = build(closedIssueStatusGroup);
