import { PlaceholderRow, RowResourceType } from '@/features/row';

export * from './projectRows';
export * from './versionRows';
export * from './issueRows';
export * from './groupRows';
export * from './treedRows';

export const placeholderRow: PlaceholderRow = {
  id: 'placeholder',
  resourceType: RowResourceType.Placeholder,
  depth: -1,
  isCollapsible: false,
  subtreeRootIds: [],
};
