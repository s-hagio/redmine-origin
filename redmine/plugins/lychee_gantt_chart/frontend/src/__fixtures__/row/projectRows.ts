import { Project } from '@/models/project';
import { ProjectRow, RowResourceType } from '@/features/row';
import {
  rootProject,
  parentProject,
  mainProject,
  childProject1,
  grandchildProject,
  childProject2,
  otherTreeProject,
} from '../coreResource/projects';

const build = (project: Project, partialAttributes?: Partial<ProjectRow>): Readonly<ProjectRow> => ({
  id: `${RowResourceType.Project}-${project.id}`,
  resourceId: project.id,
  resourceType: RowResourceType.Project,
  depth: -1,
  isCollapsible: true,
  subtreeRootIds: [],
  ...partialAttributes,
});

export const rootProjectRow = build(rootProject);
export const parentProjectRow = build(parentProject);
export const mainProjectRow = build(mainProject);
export const childProject1Row = build(childProject1);
export const grandchildProjectRow = build(grandchildProject, { isCollapsible: false });
export const childProject2Row = build(childProject2, { isCollapsible: false });

export const otherTreeProjectRow = build(otherTreeProject);
