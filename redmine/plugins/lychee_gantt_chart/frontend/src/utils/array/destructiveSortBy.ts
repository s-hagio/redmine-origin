export type SortableValue = string | number | boolean | Date | null | undefined;

export default function destructiveSortBy<T>(list: T[], resolver: string | ((obj: T) => SortableValue)): T[] {
  const getValue = typeof resolver === 'string' ? (obj: T) => obj[resolver as keyof T] : resolver;

  return list.sort((a, b) => {
    const aValue = getValue(a);
    const bValue = getValue(b);

    if (aValue === bValue) return 0;
    if (aValue == null) return 1;
    if (bValue == null) return -1;
    if (aValue < bValue) return -1;
    if (aValue > bValue) return 1;

    return 0;
  });
}
