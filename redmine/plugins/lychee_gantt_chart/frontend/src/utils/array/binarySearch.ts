const binarySearch = <T>(sortedList: T[], compare: (node: T) => number): T | null => {
  let left = 0;
  let right = sortedList.length - 1;

  while (left <= right) {
    const mid = Math.floor((left + right) / 2);
    const node = sortedList[mid];

    if (!node) {
      return null;
    }

    const compareResult = compare(node);

    if (compareResult === 0) {
      return node;
    }

    if (compareResult > 0) {
      right = mid - 1;
    } else if (compareResult < 0) {
      left = mid + 1;
    }
  }

  return null;
};

export default binarySearch;
