import { binarySearch } from '@/utils/array';

const numbers = [0, 1, 2, 3, 4, 5];

test('比較関数で0を返したノードを返す', () => {
  expect(binarySearch(numbers, (node) => node - 3)).toBe(3);
});

test('マッチするノードがなければnullを返す', () => {
  expect(binarySearch(numbers, (node) => node - 6)).toBe(null);
});
