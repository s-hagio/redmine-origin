import destructiveSortBy from '../destructiveSortBy';

type Item = { value: string | number | boolean | Date | null };

const testCases = (list: Item[], expected: Item[]) => {
  test('by property key', () => {
    const result = destructiveSortBy(list, 'value');

    expect(result).toStrictEqual(expected);
    expect(result).toBe(list);
  });

  test('by function', () => {
    const result = destructiveSortBy(list, (item: Item) => item.value);

    expect(result).toStrictEqual(expected);
    expect(result).toBe(list);
  });
};

describe('value: String', () => {
  testCases(
    [{ value: 'b' }, { value: null }, { value: 'c' }, { value: 'a' }],
    [{ value: 'a' }, { value: 'b' }, { value: 'c' }, { value: null }]
  );
});

describe('value: Number', () => {
  testCases(
    [{ value: 2 }, { value: 3 }, { value: 1 }, { value: null }],
    [{ value: 1 }, { value: 2 }, { value: 3 }, { value: null }]
  );
});

describe('value: Boolean', () => {
  testCases(
    [{ value: false }, { value: true }, { value: false }, { value: true }],
    [{ value: false }, { value: false }, { value: true }, { value: true }]
  );
});

describe('value: Date', () => {
  const now = Date.now();
  const list: Item[] = [
    { value: new Date(now + 2) },
    { value: null },
    { value: new Date(now + 3) },
    { value: new Date(now + 1) },
  ];

  testCases(list, [list[3], list[0], list[2], list[1]]);
});
