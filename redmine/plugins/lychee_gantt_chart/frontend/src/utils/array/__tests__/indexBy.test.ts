import indexBy from '../indexBy';

type Item = {
  id: number;
};

const list: Item[] = [{ id: 1 }, { id: 2 }];
const expected = { 1: list[0], 2: list[1] };

test('by property key', () => {
  expect(indexBy(list, 'id')).toEqual(expected);
});

test('by function', () => {
  expect(indexBy(list, (o: Item) => o.id)).toEqual(expected);
});
