import groupBy from '../groupBy';

type Item = {
  id: number;
  category: string | null;
};

const list: Item[] = [
  { id: 1, category: 'foo' },
  { id: 2, category: 'bar' },
  { id: 3, category: 'bar' },
  { id: 4, category: null },
];

const expected = {
  foo: [list[0]],
  bar: [list[1], list[2]],
  null: [list[3]],
};

test('by property key', () => {
  expect(groupBy(list, 'category')).toEqual(expected);
});

test('by function', () => {
  expect(groupBy(list, (item) => item.category)).toEqual(expected);
});
