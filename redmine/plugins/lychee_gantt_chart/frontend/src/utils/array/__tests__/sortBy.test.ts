import sortBy from '../sortBy';

test('非破壊的ソート', () => {
  const array = [1, 2, 3];
  const result = sortBy(array, (value) => -value);

  expect(result).toStrictEqual([3, 2, 1]);
  expect(result).not.toBe(array);
});
