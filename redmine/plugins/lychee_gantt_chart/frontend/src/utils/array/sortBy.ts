import destructiveSortBy, { SortableValue } from './destructiveSortBy';

export default function sortBy<T>(list: readonly T[], resolver: string | ((obj: T) => SortableValue)): T[] {
  return destructiveSortBy([...list], resolver);
}
