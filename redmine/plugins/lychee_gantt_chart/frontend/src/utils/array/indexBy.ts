/* eslint-disable @typescript-eslint/no-explicit-any */

export default function indexBy<T>(
  list: readonly T[],
  resolver: string | ((item: T) => any)
): Record<keyof any, T> {
  const getKey = typeof resolver === 'string' ? (item: T): any => item[resolver as keyof T] : resolver;

  return list.reduce((memo: Record<keyof any, T>, item) => {
    memo[getKey(item)] = item;
    return memo;
  }, {});
}
