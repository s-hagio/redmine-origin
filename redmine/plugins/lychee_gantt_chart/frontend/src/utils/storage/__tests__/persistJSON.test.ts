import persistJSON from '../persistJSON';

test('objectをlocalStorageにJSON文字列で保存', () => {
  vi.spyOn(localStorage, 'setItem');

  const name = 'test-key';
  const object = { a: 123 };

  persistJSON(name, object);

  expect(localStorage.setItem).toBeCalledWith(name, JSON.stringify(object));
});
