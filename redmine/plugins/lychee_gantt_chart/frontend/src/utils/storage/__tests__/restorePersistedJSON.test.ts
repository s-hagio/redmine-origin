import restorePersistedJSON from '../restorePersistedJSON';

const name = 'test-key';

test('localStorageに保存されているJSON文字列からObjectを復元', () => {
  const json = { a: 1, b: true } as const;

  vi.spyOn(localStorage, 'getItem').mockReturnValue(JSON.stringify(json));

  expect(restorePersistedJSON(name)).toEqual(json);
});

describe('localStorageにデータがない場合', () => {
  beforeEach(() => {
    vi.spyOn(localStorage, 'getItem').mockReturnValue(null);
  });

  test('fallbackで指定したオブジェクトを返す', () => {
    const fallback = { a: 123 } as const;

    expect(restorePersistedJSON(name, fallback)).toEqual(fallback);
  });

  test('fallbackが未指定ならnullを返す', () => {
    expect(restorePersistedJSON(name)).toEqual(null);
  });
});
