type RestorePersistedJSON = {
  <T>(key: string, fallback: T): T;
  <T>(key: string): T | null;
};

const restorePersistedJSON: RestorePersistedJSON = <T>(key: string, fallback?: T) => {
  const storedValue = localStorage.getItem(key);

  if (storedValue != null) {
    return JSON.parse(storedValue);
  }

  return fallback ?? null;
};

export default restorePersistedJSON;
