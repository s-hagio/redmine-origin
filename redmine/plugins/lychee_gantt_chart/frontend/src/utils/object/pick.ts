export default function pick<T, K extends keyof T>(object: T, keys: K[]): Pick<T, K> {
  const result = {} as Pick<T, K>;

  keys.forEach((key: K) => {
    result[key] = object[key];
  });

  return result;
}
