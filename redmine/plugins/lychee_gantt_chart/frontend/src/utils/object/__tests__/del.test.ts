import del from '../del';

test('ネストしたオブジェクトの値をパスで指定してdelete', () => {
  const obj = {
    a: {
      b: {
        c: 1,
      },
    },
    d: null,
  };

  del(obj, 'a.b');
  del(obj, 'd');

  expect(obj).toStrictEqual({ a: {} });
});
