import create from '../create';

test('pathで指定したオブジェクトを作成する', () => {
  expect(create('a.b.c', 1)).toEqual({ a: { b: { c: 1 } } });
});
