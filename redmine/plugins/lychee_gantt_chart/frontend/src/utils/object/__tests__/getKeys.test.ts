import getKeys from '../getKeys';

test('objectのkeyを型付けされた配列で取得', () => {
  const obj = { a: 1, b: 'foo', c: true };

  expect<('a' | 'b' | 'c')[]>(getKeys(obj)).toStrictEqual(['a', 'b', 'c']);
});
