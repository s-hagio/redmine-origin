import set from '../set';

test('ネストしたオブジェクトの値をパスで指定して変更', () => {
  const obj = {
    a: {
      b: {
        c: 1,
      },
    },
  };

  set(obj, 'a.b', 999);
  set(obj, 'd', 123);

  expect(obj).toStrictEqual({
    a: {
      b: 999,
    },
    d: 123,
  });
});
