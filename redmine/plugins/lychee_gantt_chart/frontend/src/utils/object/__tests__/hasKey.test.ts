import hasKey from '../hasKey';

test('オブジェクトがキーを持っている: true', () => {
  expect(hasKey({ a: 1 }, 'a')).toBe(true);
});

test('オブジェクトがキーを持っていない: false', () => {
  expect(hasKey({ a: 1 }, 'b')).toBe(false);
});
