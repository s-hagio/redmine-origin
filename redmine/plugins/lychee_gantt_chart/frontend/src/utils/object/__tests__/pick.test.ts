import pick from '../pick';

const object = { a: 1, b: 2, c: 3 };

it('指定したキーのみを持つオブジェクトのコピーを返す', () => {
  expect(pick(object, ['a', 'c'])).toEqual({ a: 1, c: 3 });
});
