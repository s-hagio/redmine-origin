import omit from '../omit';

const object = { a: 1, b: 2, c: 3 };

it('指定したキーを取り除いたオブジェクトのコピーを返す', () => {
  expect(omit(object, ['a', 'b'])).toEqual({ c: 3 });
});
