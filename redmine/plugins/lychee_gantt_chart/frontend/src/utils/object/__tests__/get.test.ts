import get from '../get';

const obj = {
  a: {
    b: {
      c: 1,
    },
  },
};

test('ネストしたオブジェクトの値を取得', () => {
  expect(get(obj, 'a.b')).toStrictEqual({ c: 1 });
  expect(get(obj, 'a.b.c')).toBe(1);
});

test('値が存在しない場合はnullを返す', () => {
  expect(get(obj, 'a.c')).toBe(null);
});

test('object以外の場合はnullを返す', () => {
  expect(get(String(), 'length')).toBe(null);
});
