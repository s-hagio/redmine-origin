import isPlainObject from '../isPlainObject';

test('Objectオブジェクトならtrue', () => {
  expect(isPlainObject({})).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isPlainObject(null)).toBe(false);
  expect(isPlainObject(undefined)).toBe(false);
  expect(isPlainObject(false)).toBe(false);
  expect(isPlainObject('')).toBe(false);
  expect(isPlainObject(1)).toBe(false);
  expect(isPlainObject(new Date())).toBe(false);
});
