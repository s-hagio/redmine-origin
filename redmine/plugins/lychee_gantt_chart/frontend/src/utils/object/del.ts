import { PATH_SPLITTER } from './constants';
import get from './get';
import isPlainObject from './isPlainObject';

const del = <T>(obj: unknown, pathString: string): void => {
  const nestedKeys = pathString.split(PATH_SPLITTER);

  if (!nestedKeys.length) {
    return;
  }

  const lastKey = nestedKeys.pop();
  const targetObject = nestedKeys.length ? get(obj, nestedKeys.join(PATH_SPLITTER)) : obj;

  if (!isPlainObject(targetObject)) {
    return;
  }

  delete (targetObject as Record<string, T>)[lastKey as keyof typeof targetObject];
};

export default del;
