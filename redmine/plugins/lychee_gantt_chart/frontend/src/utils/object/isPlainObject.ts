const isPlainObject = (value: unknown): value is object => {
  return value != null && String.prototype.slice.call(value) === '[object Object]';
};

export default isPlainObject;
