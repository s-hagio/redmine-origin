import { PATH_SPLITTER } from './constants';
import get from './get';
import isPlainObject from './isPlainObject';

const set = <T>(obj: unknown, pathString: string, value: T): void => {
  const nestedKeys = pathString.split(PATH_SPLITTER);

  if (!nestedKeys.length) {
    return;
  }

  const lastKey = nestedKeys.pop();
  const targetObject = nestedKeys.length ? get(obj, nestedKeys.join(PATH_SPLITTER)) : obj;

  if (!isPlainObject(targetObject)) {
    return;
  }

  (targetObject as Record<string, T>)[lastKey as keyof typeof targetObject] = value;
};

export default set;
