import { PATH_SPLITTER } from './constants';
import isPlainObject from './isPlainObject';
import hasKey from './hasKey';

const get = <T>(obj: unknown, pathString: string): T | null => {
  return pathString.split(PATH_SPLITTER).reduce((current, key) => {
    if (isPlainObject(current) && hasKey(current, key)) {
      return current[key] ?? null;
    }

    return null;
  }, obj) as T;
};

export default get;
