import { PATH_SPLITTER } from './constants';

const create = <T>(path: string, value: T): object => {
  const obj = {};

  const nestedKeys = path.split(PATH_SPLITTER);
  const lastKey = nestedKeys.pop();

  if (lastKey == null) {
    return obj;
  }

  const targetObject = nestedKeys.reduce<Record<string, object | T>>((memo, key) => {
    return (memo[key] = {});
  }, obj);

  targetObject[lastKey] = value;

  return obj;
};

export default create;
