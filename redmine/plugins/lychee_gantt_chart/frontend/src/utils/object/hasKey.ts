const hasKey = <T>(obj: T, key: PropertyKey): key is keyof typeof obj => {
  return Object.prototype.hasOwnProperty.call(obj, key);
};

export default hasKey;
