const getKeys = <T extends object, K extends keyof T>(obj: T, typeCaster?: (key: string) => K): K[] => {
  return Object.keys(obj).map(typeCaster ?? ((key: string) => key as K));
};

export default getKeys;
