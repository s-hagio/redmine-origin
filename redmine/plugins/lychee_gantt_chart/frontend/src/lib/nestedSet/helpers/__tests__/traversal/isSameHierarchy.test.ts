import { isSameHierarchy } from '../../traversal';
import {
  rootNode,
  parentNode,
  contextNode,
  childNode,
  grandchildNode,
  otherNodeInSameTree,
  otherTreeRootNode,
} from '../__fixtures__';

test('同一ノードはtrue', () => {
  expect(isSameHierarchy(contextNode, contextNode)).toBe(true);
});

test('先祖ノードはtrue', () => {
  expect(isSameHierarchy(contextNode, rootNode)).toBe(true);
  expect(isSameHierarchy(contextNode, parentNode)).toBe(true);
});

test('子孫ノードはtrue', () => {
  expect(isSameHierarchy(contextNode, childNode)).toBe(true);
  expect(isSameHierarchy(contextNode, grandchildNode)).toBe(true);
});

test('同一ツリー内の親戚ノードはfalse', () => {
  expect(isSameHierarchy(contextNode, otherNodeInSameTree)).toBe(false);
});

test('別ツリーのノードはfalse', () => {
  expect(isSameHierarchy(contextNode, otherTreeRootNode)).toBe(false);
});
