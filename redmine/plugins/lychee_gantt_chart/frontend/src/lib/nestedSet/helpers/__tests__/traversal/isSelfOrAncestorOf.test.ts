import { isSelfOrAncestorOf } from '../../traversal';
import {
  contextNode,
  rootNode,
  parentNode,
  childNode,
  grandchildNode,
  otherNodeInSameTree,
  otherTreeRootNode,
} from '../__fixtures__';

test('対象ノード自身または先祖ならtrue', () => {
  expect(isSelfOrAncestorOf(contextNode, { ...contextNode })).toBe(true);
  expect(isSelfOrAncestorOf(contextNode, childNode)).toBe(true);
  expect(isSelfOrAncestorOf(contextNode, grandchildNode)).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isSelfOrAncestorOf(contextNode, rootNode)).toBe(false);
  expect(isSelfOrAncestorOf(contextNode, parentNode)).toBe(false);
  expect(isSelfOrAncestorOf(contextNode, otherNodeInSameTree)).toBe(false);
  expect(isSelfOrAncestorOf(contextNode, otherTreeRootNode)).toBe(false);
});
