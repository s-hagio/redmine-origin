import { isSameTree } from '../../traversal';
import {
  rootNode,
  parentNode,
  contextNode,
  childNode,
  grandchildNode,
  otherNodeInSameTree,
  otherTreeRootNode,
} from '../__fixtures__';

test('ルートノードが同一ならtrue', () => {
  expect(isSameTree(contextNode, rootNode)).toBe(true);
  expect(isSameTree(contextNode, parentNode)).toBe(true);
  expect(isSameTree(contextNode, contextNode)).toBe(true);
  expect(isSameTree(contextNode, childNode)).toBe(true);
  expect(isSameTree(contextNode, grandchildNode)).toBe(true);
  expect(isSameTree(contextNode, otherNodeInSameTree)).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isSameTree(contextNode, otherTreeRootNode)).toBe(false);
});
