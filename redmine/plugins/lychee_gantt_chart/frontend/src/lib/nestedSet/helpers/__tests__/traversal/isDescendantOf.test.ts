import { isDescendantOf } from '../../traversal';
import {
  contextNode,
  rootNode,
  parentNode,
  childNode,
  grandchildNode,
  otherNodeInSameTree,
  otherTreeRootNode,
} from '../__fixtures__';

test('自身が対象ノードの子孫ならtrue', () => {
  expect(isDescendantOf(contextNode, rootNode)).toBe(true);
  expect(isDescendantOf(contextNode, parentNode)).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isDescendantOf(contextNode, childNode)).toBe(false);
  expect(isDescendantOf(contextNode, grandchildNode)).toBe(false);
  expect(isDescendantOf(contextNode, contextNode)).toBe(false);
  expect(isDescendantOf(contextNode, otherNodeInSameTree)).toBe(false);
  expect(isDescendantOf(contextNode, otherTreeRootNode)).toBe(false);
});
