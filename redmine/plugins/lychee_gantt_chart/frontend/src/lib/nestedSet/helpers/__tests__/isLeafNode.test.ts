import { isLeafNode } from '@/lib/nestedSet';
import {
  childNode,
  contextNode,
  grandchildNode,
  otherNodeInSameTree,
  otherTreeRootNode,
  parentNode,
  rootNode,
} from './__fixtures__';

test('末端ノードならtrue', () => {
  expect(isLeafNode(grandchildNode)).toBe(true);
  expect(isLeafNode(otherNodeInSameTree)).toBe(true);
  expect(isLeafNode(otherTreeRootNode)).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isLeafNode(rootNode)).toBe(false);
  expect(isLeafNode(parentNode)).toBe(false);
  expect(isLeafNode(contextNode)).toBe(false);
  expect(isLeafNode(childNode)).toBe(false);
});
