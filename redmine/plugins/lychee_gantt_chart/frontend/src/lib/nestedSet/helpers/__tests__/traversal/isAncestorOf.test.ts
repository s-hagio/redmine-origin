import { isAncestorOf } from '../../traversal';
import {
  contextNode,
  rootNode,
  parentNode,
  childNode,
  grandchildNode,
  otherNodeInSameTree,
  otherTreeRootNode,
} from '../__fixtures__';

test('自身が対象ノードの先祖ならtrue', () => {
  expect(isAncestorOf(contextNode, childNode)).toBe(true);
  expect(isAncestorOf(contextNode, grandchildNode)).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isAncestorOf(contextNode, rootNode)).toBe(false);
  expect(isAncestorOf(contextNode, parentNode)).toBe(false);
  expect(isAncestorOf(contextNode, contextNode)).toBe(false);
  expect(isAncestorOf(contextNode, otherNodeInSameTree)).toBe(false);
  expect(isAncestorOf(contextNode, otherTreeRootNode)).toBe(false);
});
