import { Node } from '@/lib/nestedSet';

export const isLeafNode = (node: Node) => {
  return node.rgt - node.lft === 1;
};
