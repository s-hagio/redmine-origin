export const rootNode = {
  rootId: 1,
  lft: 1,
  rgt: 10,
};

export const parentNode = {
  rootId: rootNode.rootId,
  lft: 2,
  rgt: 9,
};

export const contextNode = {
  rootId: rootNode.rootId,
  lft: 3,
  rgt: 8,
};

export const childNode = {
  rootId: rootNode.rootId,
  lft: 4,
  rgt: 7,
};

export const grandchildNode = {
  rootId: rootNode.rootId,
  lft: 5,
  rgt: 6,
};

export const otherNodeInSameTree = {
  rootId: rootNode.rootId,
  lft: 11,
  rgt: 12,
};

export const otherTreeRootNode = {
  rootId: 101,
  lft: 101,
  rgt: 102,
};
