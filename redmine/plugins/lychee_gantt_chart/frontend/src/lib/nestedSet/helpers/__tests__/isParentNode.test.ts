import { isParentNode } from '@/lib/nestedSet';
import {
  childNode,
  contextNode,
  grandchildNode,
  otherNodeInSameTree,
  otherTreeRootNode,
  parentNode,
  rootNode,
} from './__fixtures__';

test('親ノードならtrue', () => {
  expect(isParentNode(rootNode)).toBe(true);
  expect(isParentNode(parentNode)).toBe(true);
  expect(isParentNode(contextNode)).toBe(true);
  expect(isParentNode(childNode)).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isParentNode(grandchildNode)).toBe(false);
  expect(isParentNode(otherNodeInSameTree)).toBe(false);
  expect(isParentNode(otherTreeRootNode)).toBe(false);
});
