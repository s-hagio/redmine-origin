import { Node } from '@/lib/nestedSet';
import { isLeafNode } from './isLeafNode';

export const isParentNode = (node: Node) => {
  return !isLeafNode(node);
};
