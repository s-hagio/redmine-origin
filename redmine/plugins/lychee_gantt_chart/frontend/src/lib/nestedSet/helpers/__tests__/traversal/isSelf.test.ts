import { isSelf } from '../../traversal';
import { contextNode } from '../__fixtures__';

test('ノードの値が同じならtrue', () => {
  expect(isSelf(contextNode, { ...contextNode })).toBe(true);
});

test('ノードの値が違うならfalse', () => {
  expect(isSelf(contextNode, { ...contextNode, lft: contextNode.lft + 1 })).toBe(false);
});
