import { Node } from '../types';

export const isSameTree = (node: Node, otherNode: Node): boolean => {
  return node.rootId === otherNode.rootId;
};

export const isSameHierarchy = (node: Node, otherNode: Node): boolean => {
  return isSelf(node, otherNode) || isAncestorOf(node, otherNode) || isDescendantOf(node, otherNode);
};

export const isSelf = (node: Node, otherNode: Node): boolean => {
  return isSameTree(node, otherNode) && node.lft === otherNode.lft && node.rgt === otherNode.rgt;
};

export const isAncestorOf = (node: Node, otherNode: Node): boolean => {
  return isSameTree(node, otherNode) && node.lft < otherNode.lft && node.rgt > otherNode.rgt;
};

export const isSelfOrAncestorOf = (node: Node, otherNode: Node): boolean => {
  return isSelf(node, otherNode) || isAncestorOf(node, otherNode);
};

export const isDescendantOf = (node: Node, otherNode: Node): boolean => {
  return isSameTree(node, otherNode) && node.lft > otherNode.lft && node.rgt < otherNode.rgt;
};
