export type Node = {
  rootId: number | string;
  lft: number;
  rgt: number;
};
