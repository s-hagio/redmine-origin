import { renderHook } from '@testing-library/react';
import { useAnimationFrame } from '../useAnimationFrame';

vi.useFakeTimers();

beforeEach(() => {
  vi.spyOn(window, 'requestAnimationFrame').mockImplementation(setTimeout);
  vi.spyOn(window, 'cancelAnimationFrame').mockImplementation(clearTimeout);
});

test('Request', () => {
  const { result } = renderHook(() => useAnimationFrame());
  const [request] = result.current;
  const callback = vi.fn();

  request(callback);

  expect(callback).not.toBeCalled();

  vi.advanceTimersToNextTimer();

  expect(callback).toBeCalled();
});

test('Cancel', () => {
  const { result } = renderHook(() => useAnimationFrame());
  const [request, cancel] = result.current;
  const callback = vi.fn();

  request(callback);

  expect(callback).not.toBeCalled();

  cancel();

  vi.advanceTimersToNextTimer();

  expect(callback).not.toBeCalled();
});
