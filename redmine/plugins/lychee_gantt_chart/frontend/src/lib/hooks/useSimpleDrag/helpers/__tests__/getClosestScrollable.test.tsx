import * as React from 'react';
import { render } from '@testing-library/react';
import { getClosestScrollable } from '../getClosestScrollable';

const createElementTree = () => {
  const { getByTestId } = render(
    <div data-testid="root" style={{ overflow: 'scroll' }}>
      <div data-testid="parent" style={{ overflow: 'auto' }}>
        <div data-testid="leaf" />
      </div>
    </div>
  );

  const elements = {
    root: getByTestId('root'),
    parent: getByTestId('parent'),
    leaf: getByTestId('leaf'),
  };

  Object.values<HTMLElement>(elements).forEach((el) => {
    Object.defineProperty(el, 'scrollWidth', { value: 0, configurable: true });
    Object.defineProperty(el, 'scrollHeight', { value: 0, configurable: true });
  });

  return elements;
};

test('Horizontal scrollable', () => {
  const { root, leaf } = createElementTree();

  vi.spyOn(root, 'clientWidth', 'get').mockReturnValue(100);
  vi.spyOn(root, 'scrollWidth', 'get').mockReturnValue(500);

  expect(getClosestScrollable(leaf)).toBe(root);
});

test('Vertical scrollable', () => {
  const { parent, leaf } = createElementTree();

  vi.spyOn(parent, 'clientHeight', 'get').mockReturnValue(100);
  vi.spyOn(parent, 'scrollHeight', 'get').mockReturnValue(500);

  expect(getClosestScrollable(leaf)).toBe(parent);
});
