import { RefObject, useCallback, useEffect, useMemo, useRef } from 'react';
import { AutoScroller, AutoScrollOptions, Position, ResetScroll, StartScroll, StopScroll } from './types';
import { getClosestScrollable } from './helpers';

export const DEFAULT_OPTIONS = {
  edgeResistance: 30,
  scrollSize: 5,
};

type AdjustmentPositionRef = RefObject<Position>;

export const useAutoScroll = <DraggableElement extends HTMLElement>(
  enabled: boolean,
  draggableRef: RefObject<DraggableElement>,
  options: AutoScrollOptions = {}
): [AutoScroller, AdjustmentPositionRef] => {
  const opts = useMemo(() => ({ ...DEFAULT_OPTIONS, ...options }), [options]);

  const scrollableElementRef = useRef<HTMLElement | null>(null);
  const scrollingRequestIdRef = useRef<number | null>(null);
  const adjustmentPositionRef = useRef<Position>({ x: 0, y: 0 });

  const stop: StopScroll = useCallback(() => {
    scrollingRequestIdRef.current && clearTimeout(scrollingRequestIdRef.current);
    scrollingRequestIdRef.current = null;
  }, []);

  const start = useCallback<StartScroll>(
    (draggerPosition, onScroll) => {
      if (!scrollableElementRef.current) {
        return;
      }

      stop();

      const scrollable = scrollableElementRef.current as HTMLElement;
      const rect = scrollable.getBoundingClientRect();

      const delta = {
        left: draggerPosition.x - rect.left,
        right: rect.right - draggerPosition.x,
        top: draggerPosition.y - rect.top,
        bottom: rect.bottom - draggerPosition.y,
      };

      type DeltaType = keyof typeof delta;

      const setScrollToOptions = (type: DeltaType) => {
        const deltaRatio = Math.abs((delta[type] - opts.edgeResistance) / opts.edgeResistance) + 0.1;
        const scrollSize = Math.min(Math.round(opts.scrollSize * deltaRatio), opts.scrollSize);

        switch (type) {
          case 'left':
            if (scrollable.scrollLeft !== 0) {
              scrollable.scrollLeft = Math.max(scrollable.scrollLeft - scrollSize, 0);
              adjustmentPositionRef.current.x += scrollSize;
            }
            break;
          case 'right':
            if (scrollable.scrollLeft < scrollable.scrollWidth - scrollable.clientWidth) {
              scrollable.scrollLeft = Math.min(scrollable.scrollLeft + scrollSize, scrollable.scrollWidth);
              adjustmentPositionRef.current.x -= scrollSize;
            }
            break;
          case 'top':
            if (scrollable.scrollTop !== 0) {
              scrollable.scrollTop = Math.max(scrollable.scrollTop - scrollSize, 0);
              adjustmentPositionRef.current.y += scrollSize;
            }
            break;
          case 'bottom':
            if (scrollable.scrollTop < scrollable.scrollHeight - scrollable.clientHeight) {
              scrollable.scrollTop = Math.min(scrollable.scrollTop + scrollSize, scrollable.scrollHeight);
              adjustmentPositionRef.current.y -= scrollSize;
            }
            break;
        }
      };

      let hasChanged = false;

      (Object.keys(delta) as DeltaType[]).forEach((type) => {
        if (delta[type] <= opts.edgeResistance) {
          setScrollToOptions(type);
          hasChanged = true;
        }
      });

      if (!hasChanged) {
        return;
      }

      if (onScroll) {
        onScroll(adjustmentPositionRef.current);
      }

      scrollingRequestIdRef.current = window.setTimeout(() => {
        start(draggerPosition, onScroll);
      }, 0);
    },
    [opts, stop]
  );

  const reset: ResetScroll = useCallback(() => {
    adjustmentPositionRef.current = { x: 0, y: 0 };
  }, []);

  useEffect(() => {
    if (enabled && draggableRef.current) {
      scrollableElementRef.current = getClosestScrollable(draggableRef.current);
    }
  }, [draggableRef, enabled]);

  return [{ start, stop, reset }, adjustmentPositionRef];
};
