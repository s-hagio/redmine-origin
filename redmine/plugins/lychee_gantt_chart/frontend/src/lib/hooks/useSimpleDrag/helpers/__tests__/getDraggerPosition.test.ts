import { createEvent } from '@testing-library/react';
import { getDraggerPosition } from '../getDraggerPosition';

test('Get dragger position', () => {
  const event = createEvent.mouseMove(document, { clientX: 50, clientY: 100 }) as MouseEvent;

  expect(getDraggerPosition(event)).toStrictEqual({ x: 50, y: 100 });
});
