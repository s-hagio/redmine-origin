import { useRef } from 'react';
import { renderHook, RenderHookResult } from '@testing-library/react';
import { getClosestScrollable } from '../helpers';
import { DEFAULT_OPTIONS, useAutoScroll } from '../useAutoScroll';

vi.mock('../helpers', () => ({
  getClosestScrollable: vi.fn(),
}));

const { edgeResistance, scrollSize } = DEFAULT_OPTIONS;

const scrollableRect = {
  width: 500,
  right: 550,
  left: 25,
  height: 600,
  top: 50,
  bottom: 650,
} as const as DOMRect;

const initialScrollLeft = 1000;
const initialScrollTop = 2000;
const scrollWidth = 5000;
const scrollHeight = 6000;

const createScrollableMock = () => {
  const scrollable = document.createElement('div');

  vi.spyOn(scrollable, 'getBoundingClientRect').mockReturnValue(scrollableRect);

  Object.defineProperty(scrollable, 'scrollWidth', { value: scrollWidth });
  Object.defineProperty(scrollable, 'scrollHeight', { value: scrollHeight });

  scrollable.scrollTo(initialScrollLeft, initialScrollTop);

  vi.mocked(getClosestScrollable).mockReturnValue(scrollable);

  return scrollable;
};

const useMockElementRef = () => useRef(document.createElement('div'));

const renderHookWithScrollable = <Result, Props>(
  ...args: Parameters<typeof renderHook<Result, Props>>
): RenderHookResult<Result, Props> & { scrollable: HTMLDivElement } => {
  const scrollable = createScrollableMock();
  const result = renderHook<Result, Props>(...args);

  return { ...result, scrollable };
};

beforeEach(() => {
  vi.useFakeTimers();
});

test('Basic auto scroll', () => {
  const { result, scrollable } = renderHookWithScrollable(() => useAutoScroll(true, useMockElementRef()));
  const [autoScroller, adjustmentPositionRef] = result.current;

  const onScroll = vi.fn();

  autoScroller.start(
    {
      x: scrollableRect.left + edgeResistance + 1,
      y: scrollableRect.top + edgeResistance + 1,
    },
    onScroll
  );

  expect(scrollable.scrollLeft).toBe(initialScrollLeft);
  expect(scrollable.scrollTop).toBe(initialScrollTop);
  expect(adjustmentPositionRef.current).toStrictEqual({ x: 0, y: 0 });
  expect(onScroll).not.toHaveBeenCalled();

  autoScroller.start(
    {
      x: scrollableRect.left,
      y: scrollableRect.top,
    },
    onScroll
  );

  expect(scrollable.scrollLeft).toBe(initialScrollLeft - scrollSize);
  expect(scrollable.scrollTop).toBe(initialScrollTop - scrollSize);
  expect(adjustmentPositionRef.current).toStrictEqual({ x: scrollSize, y: scrollSize });
  expect(onScroll).toHaveBeenCalled();
});

test('Repeat auto scrolling while cursor is on edge', () => {
  const { result, scrollable } = renderHookWithScrollable(() => useAutoScroll(true, useMockElementRef()));
  const [autoScroller, adjustmentPositionRef] = result.current;

  autoScroller.start({ x: scrollableRect.left, y: scrollableRect.top });

  expect.assertions(5 * 3);

  for (let i = 1; i <= 5; i++) {
    const size = scrollSize * i;

    expect(scrollable.scrollLeft).toBe(initialScrollLeft - size);
    expect(scrollable.scrollTop).toBe(initialScrollTop - size);
    expect(adjustmentPositionRef.current).toStrictEqual({ x: size, y: size });

    vi.advanceTimersToNextTimer();
  }
});

test('Right edge and bottom edge', () => {
  const { result, scrollable } = renderHookWithScrollable(() => useAutoScroll(true, useMockElementRef()));
  const [autoScroller, adjustmentPositionRef] = result.current;

  autoScroller.start({ x: scrollableRect.right, y: scrollableRect.bottom });

  expect(scrollable.scrollLeft).toBe(initialScrollLeft + scrollSize);
  expect(scrollable.scrollTop).toBe(initialScrollTop + scrollSize);
  expect(adjustmentPositionRef.current).toStrictEqual({ x: -scrollSize, y: -scrollSize });
});

test('Reset adjustment position', () => {
  const { result } = renderHookWithScrollable(() => useAutoScroll(true, useMockElementRef()));
  const [autoScroller, adjustmentPositionRef] = result.current;

  autoScroller.start({ x: scrollableRect.left, y: scrollableRect.top });
  expect(adjustmentPositionRef.current).toStrictEqual({ x: scrollSize, y: scrollSize });

  autoScroller.reset();
  expect(adjustmentPositionRef.current).toStrictEqual({ x: 0, y: 0 });
});

test('Disable auto scroll', () => {
  const { result } = renderHookWithScrollable(() => useAutoScroll(false, useMockElementRef()));
  const [autoScroller, adjustmentPositionRef] = result.current;

  autoScroller.start({ x: scrollableRect.left, y: scrollableRect.top });
  expect(adjustmentPositionRef.current).toStrictEqual({ x: 0, y: 0 });
});

test('Stop scrolling', () => {
  const { result, scrollable } = renderHookWithScrollable(() => useAutoScroll(true, useMockElementRef()));
  const [autoScroller, adjustmentPositionRef] = result.current;

  autoScroller.start({ x: scrollableRect.left, y: scrollableRect.top });

  expect(scrollable.scrollLeft).toBe(initialScrollLeft - scrollSize);
  expect(scrollable.scrollTop).toBe(initialScrollTop - scrollSize);
  expect(adjustmentPositionRef.current).toStrictEqual({ x: scrollSize, y: scrollSize });

  autoScroller.stop();

  vi.advanceTimersToNextTimer();

  expect(scrollable.scrollLeft).toBe(initialScrollLeft - scrollSize);
  expect(scrollable.scrollTop).toBe(initialScrollTop - scrollSize);
  expect(adjustmentPositionRef.current).toStrictEqual({ x: scrollSize, y: scrollSize });
});

test('Stop scroll scheduler on start scroll', () => {
  const { result } = renderHookWithScrollable(() => useAutoScroll(true, useMockElementRef()));
  const [autoScroller] = result.current;

  const onScroll = vi.fn();

  autoScroller.start({ x: scrollableRect.left, y: scrollableRect.top }, onScroll);
  expect(onScroll).toHaveBeenCalledTimes(1);

  autoScroller.start({ x: scrollableRect.left, y: scrollableRect.top }, onScroll);
  expect(onScroll).toHaveBeenCalledTimes(2);

  vi.advanceTimersToNextTimer();
  expect(onScroll).toHaveBeenCalledTimes(3);
});

test('Adjust scroll size with distance from edge', () => {
  const edgeResistance = 10;
  const scrollSize = 5;
  const y = scrollableRect.top + edgeResistance + 1;

  const { result, scrollable } = renderHookWithScrollable(() =>
    useAutoScroll(true, useMockElementRef(), { edgeResistance, scrollSize })
  );
  const [autoScroller] = result.current;

  expect.assertions(12);

  [
    { delta: 10, scrollSize: 1 },
    { delta: 9, scrollSize: 1 },
    { delta: 8, scrollSize: 2 },
    { delta: 7, scrollSize: 2 },
    { delta: 6, scrollSize: 3 },
    { delta: 5, scrollSize: 3 },
    { delta: 4, scrollSize: 4 },
    { delta: 3, scrollSize: 4 },
    { delta: 2, scrollSize: 5 },
    { delta: 1, scrollSize: 5 },
    { delta: 0, scrollSize: 5 },
    { delta: -100, scrollSize: 5 },
  ].forEach(({ delta, scrollSize }) => {
    scrollable.scrollLeft = initialScrollLeft;
    autoScroller.start({ x: scrollableRect.left + delta, y });

    expect(scrollable.scrollLeft, `delta: ${delta}, scrollSize: ${scrollSize}`).toBe(
      initialScrollLeft - scrollSize
    );
  });
});
