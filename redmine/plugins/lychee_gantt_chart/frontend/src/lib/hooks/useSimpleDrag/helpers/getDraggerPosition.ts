import { DraggerPosition } from '../types';

export const getDraggerPosition = (event: MouseEvent): DraggerPosition => {
  return {
    x: event.clientX,
    y: event.clientY,
  };
};
