import * as React from 'react';
import { createEvent, fireEvent, render } from '@testing-library/react';
import { useSimpleDrag } from '../useSimpleDrag';

type DraggableComponentProps = {
  options: Parameters<typeof useSimpleDrag>[0];
};

const DRAGGABLE_COMPONENT_ID = 'draggable';

const DraggableComponent: React.FC<DraggableComponentProps> = ({ options }) => {
  const [ref, isDragging] = useSimpleDrag(options);

  return <div ref={ref} data-testid={DRAGGABLE_COMPONENT_ID} data-dragging={isDragging} />;
};

const onDragStart = vi.fn();
const onDrag = vi.fn();
const onDragEnd = vi.fn();
const options = { onDragStart, onDrag, onDragEnd };

const clearAll = () => {
  onDragStart.mockClear();
  onDrag.mockClear();
  onDragEnd.mockClear();
};

afterEach(clearAll);

test('Basic drag and dragging state', () => {
  const { getByTestId } = render(<DraggableComponent options={options} />);
  const el = getByTestId(DRAGGABLE_COMPONENT_ID);

  const pointerDownEvent = createEvent.pointerDown(el, { clientX: 100, clientY: 100 });
  fireEvent(el, pointerDownEvent);

  expect(onDragStart).toHaveBeenCalledWith({ x: 0, y: 0 }, pointerDownEvent);
  expect(onDrag).not.toHaveBeenCalled();
  expect(onDragEnd).not.toHaveBeenCalled();
  expect(el.getAttribute('data-dragging')).toBe('false');

  clearAll();

  const pointerMoveEvent = createEvent.pointerMove(document, { clientX: 88, clientY: 120 });
  fireEvent(document, pointerMoveEvent);

  expect(onDragStart).not.toHaveBeenCalled();
  expect(onDrag).toHaveBeenCalledWith({ x: -12, y: 20 }, pointerMoveEvent);
  expect(onDragEnd).not.toHaveBeenCalled();
  expect(el.getAttribute('data-dragging')).toBe('true');

  clearAll();

  const pointerUpEvent = createEvent.pointerUp(document, { clientX: 85, clientY: 122 });
  fireEvent(document, pointerUpEvent);

  expect(onDragStart).not.toHaveBeenCalled();
  expect(onDrag).not.toHaveBeenCalled();
  expect(onDragEnd).toHaveBeenCalledWith({ x: -15, y: 22 }, pointerUpEvent);
  expect(el.getAttribute('data-dragging')).toBe('false');
});

test('"onDragEnd" is only called after drag', () => {
  const { getByTestId } = render(<DraggableComponent options={options} />);
  const el = getByTestId(DRAGGABLE_COMPONENT_ID);

  const pointerDownEvent = createEvent.pointerDown(el, { clientX: 100, clientY: 100 });

  fireEvent(el, pointerDownEvent);
  fireEvent.pointerUp(document, { clientX: 100, clientY: 100 });

  expect(onDragStart).toHaveBeenCalledWith({ x: 0, y: 0 }, pointerDownEvent);
  expect(onDrag).not.toHaveBeenCalled();
  expect(onDragEnd).not.toHaveBeenCalled();
});

test('Drag starts only when the main button pressed', () => {
  const { getByTestId } = render(<DraggableComponent options={options} />);
  const el = getByTestId(DRAGGABLE_COMPONENT_ID);

  expect.assertions(4);

  for (let n = 1; n <= 4; n++) {
    fireEvent.pointerDown(el, { button: n });
    expect(onDragStart).not.toHaveBeenCalled();
  }
});

test('Remove pointerdown event listener on unmount', () => {
  const { getByTestId, unmount } = render(<DraggableComponent options={options} />);
  const el = getByTestId(DRAGGABLE_COMPONENT_ID);

  unmount();
  fireEvent.pointerDown(el);

  expect(onDragStart).not.toHaveBeenCalled();
});

test('Remove event listener for document on pointer up', () => {
  const { getByTestId } = render(<DraggableComponent options={options} />);
  const el = getByTestId(DRAGGABLE_COMPONENT_ID);

  fireEvent.pointerDown(el);
  fireEvent.pointerUp(document);
  clearAll();
  fireEvent.pointerMove(document);
  fireEvent.pointerUp(document);

  expect(onDragStart).not.toHaveBeenCalled();
  expect(onDrag).not.toHaveBeenCalled();
  expect(onDragEnd).not.toHaveBeenCalled();
});

test('Collect values', () => {
  const { getByTestId } = render(
    <DraggableComponent
      options={{
        ...options,
        collect: ({ x, y, draggerY }) => ({
          left: x,
          baseDraggerY: draggerY - y,
        }),
      }}
    />
  );
  const el = getByTestId(DRAGGABLE_COMPONENT_ID);

  fireEvent.pointerDown(el, { clientX: 100, clientY: 200, isPrimary: true });

  const pointerMoveEvent = createEvent.pointerMove(document, { clientX: 85, clientY: 220 });
  fireEvent(document, pointerMoveEvent);

  expect(onDrag).toHaveBeenCalledWith(
    {
      left: -15,
      baseDraggerY: 200,
    },
    pointerMoveEvent
  );
});
