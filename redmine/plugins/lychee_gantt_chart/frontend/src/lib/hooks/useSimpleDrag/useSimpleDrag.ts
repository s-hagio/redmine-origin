import { useRef, useCallback, useEffect, useState, RefObject } from 'react';
import { MovedPosition, DraggerPosition, AdjustmentPosition, Options } from './types';
import { useAutoScroll } from './useAutoScroll';
import { getDraggerPosition } from './helpers';

type IsDragging = boolean;

const styleElement = document.createElement('style');
styleElement.textContent = `
  body {
    -moz-user-select: none;
    -webkit-user-select: none;
    user-select: none;
  }
`;

export const useSimpleDrag = <
  DraggableElement extends HTMLElement = HTMLDivElement,
  CollectedPosition extends object = MovedPosition,
>(
  options: Options<CollectedPosition> = {}
): [RefObject<DraggableElement>, IsDragging] => {
  const draggableElementRef = useRef<DraggableElement>(null);
  const baseDraggerPositionRef = useRef<DraggerPosition>({ x: 0, y: 0 });

  const draggingRef = useRef<boolean>(false);
  const [isDragging, setIsDragging] = useState<IsDragging>(false);

  const [autoScroller, adjustmentPositionRef] = useAutoScroll(
    options.autoScroll !== false,
    draggableElementRef,
    options.autoScroll || {}
  );

  const computeCollectedValues = useCallback(
    (draggerPosition: DraggerPosition): CollectedPosition => {
      const adjustmentPosition = adjustmentPositionRef.current as AdjustmentPosition;

      const position: MovedPosition = {
        x: draggerPosition.x - baseDraggerPositionRef.current.x - adjustmentPosition.x,
        y: draggerPosition.y - baseDraggerPositionRef.current.y - adjustmentPosition.y,
      };

      const collectedPosition =
        options.collect?.({
          ...position,
          draggerX: draggerPosition.x,
          draggerY: draggerPosition.y,
          adjustmentX: adjustmentPosition.x,
          adjustmentY: adjustmentPosition.y,
        }) ?? position;

      return collectedPosition as CollectedPosition;
    },
    [adjustmentPositionRef, options]
  );

  const handleDrag = useCallback(
    (event: PointerEvent) => {
      if (!draggingRef.current) {
        draggingRef.current = true;
        setIsDragging(true);
      }

      const draggerPosition = getDraggerPosition(event);

      if (options.onDrag) {
        options.onDrag(computeCollectedValues(draggerPosition), event);
      }

      autoScroller.start(draggerPosition, () => {
        options.onDrag && options.onDrag(computeCollectedValues(draggerPosition), event);
      });
    },
    [autoScroller, computeCollectedValues, options]
  );

  const handleDragEnd = useCallback(
    (event: PointerEvent) => {
      document.body.removeChild(styleElement);

      document.removeEventListener('pointermove', handleDrag);
      document.removeEventListener('pointerup', handleDragEnd);

      autoScroller.stop();

      setIsDragging(false);

      if (draggingRef.current && options.onDragEnd) {
        draggingRef.current = false;
        options.onDragEnd(computeCollectedValues(getDraggerPosition(event)), event);
      }
    },
    [handleDrag, autoScroller, options, computeCollectedValues]
  );

  const handleDragStart = useCallback(
    (event: PointerEvent) => {
      // 0: 主ボタン（だいたい左クリック）
      if (event.button !== 0) {
        return;
      }

      document.body.appendChild(styleElement);

      const draggerPosition = getDraggerPosition(event);

      baseDraggerPositionRef.current.x = draggerPosition.x;
      baseDraggerPositionRef.current.y = draggerPosition.y;

      if (options.onDragStart) {
        options.onDragStart(computeCollectedValues(draggerPosition), event);
      }

      autoScroller.reset();

      document.addEventListener('pointermove', handleDrag);
      document.addEventListener('pointerup', handleDragEnd);
    },
    [autoScroller, computeCollectedValues, handleDrag, handleDragEnd, options]
  );

  useEffect(() => {
    const draggableElement = draggableElementRef.current;

    if (!draggableElement || options.draggable === false) {
      return;
    }

    draggableElement.addEventListener('pointerdown', handleDragStart, { passive: true });

    return () => {
      draggableElement.removeEventListener('pointerdown', handleDragStart);
    };
  }, [handleDragStart, options.draggable]);

  return [draggableElementRef, isDragging];
};
