const scrollableOverflowStyleValues: string[] = ['scroll', 'auto'];

export const getClosestScrollable = (el: HTMLElement): HTMLElement | null => {
  const parent = el.parentElement;

  if (!parent) {
    return null;
  }

  const xScrollable = parent.scrollWidth > parent.clientWidth;
  const yScrollable = parent.scrollHeight > parent.clientHeight;

  if (!xScrollable && !yScrollable) {
    return getClosestScrollable(parent);
  }

  const grandparentStyle = window.getComputedStyle(parent);

  if (
    scrollableOverflowStyleValues.includes(grandparentStyle.overflow) ||
    (xScrollable && scrollableOverflowStyleValues.includes(grandparentStyle.overflowX)) ||
    (yScrollable && scrollableOverflowStyleValues.includes(grandparentStyle.overflowY))
  ) {
    return parent;
  }

  return getClosestScrollable(parent);
};
