export type Position = {
  x: number;
  y: number;
};

export type MovedPosition = Position;
export type DraggerPosition = Position;
export type AdjustmentPosition = Position;

export type CollectableValues = MovedPosition & {
  draggerX: number;
  draggerY: number;
  adjustmentX: number;
  adjustmentY: number;
};

export type DragEventHandler<CollectedValues> = (collectedValues: CollectedValues, event: MouseEvent) => void;
export type ScrollEventHandler = (adjustmentPosition: AdjustmentPosition) => void;

export type AutoScrollOptions = {
  edgeResistance?: number;
  scrollSize?: number;
};

export type Options<CollectedValues> = {
  draggable?: boolean;
  autoScroll?: false | AutoScrollOptions;
  collect?: (collectableValues: CollectableValues) => CollectedValues;
  onDragStart?: DragEventHandler<CollectedValues>;
  onDrag?: DragEventHandler<CollectedValues>;
  onDragEnd?: DragEventHandler<CollectedValues>;
};

export type StartScroll = (draggerPosition: DraggerPosition, onScroll?: ScrollEventHandler) => void;
export type StopScroll = () => void;
export type ResetScroll = () => void;

export type AutoScroller = {
  start: StartScroll;
  stop: StopScroll;
  reset: ResetScroll;
};
