import { useRef, useCallback } from 'react';

type Request = (callback: CallableFunction) => void;
type Cancel = () => void;

export const useAnimationFrame = (): [Request, Cancel] => {
  const handleRef = useRef<number | null>(null);

  const cancel: Cancel = useCallback(() => {
    if (handleRef.current !== null) {
      window.cancelAnimationFrame(handleRef.current);
      handleRef.current = null;
    }
  }, []);

  const request: Request = useCallback(
    (callback: CallableFunction) => {
      cancel();

      handleRef.current = window.requestAnimationFrame(() => {
        callback();
      });

      return handleRef.current;
    },
    [cancel]
  );

  return [request, cancel];
};
