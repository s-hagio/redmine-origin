import { constSelector, RecoilLoadable } from 'recoil';
import { useLoadRecoilValue } from '../useLoadRecoilValue';

const useRecoilValueLoadableMock = vi.hoisted(() => vi.fn());

vi.mock('recoil', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useRecoilValueLoadable: useRecoilValueLoadableMock,
}));

test('loadable.stateがloadingの時にPromiseを投げる', () => {
  useRecoilValueLoadableMock.mockReturnValue(RecoilLoadable.loading());

  expect(() => {
    useLoadRecoilValue(constSelector(123));
  }).toThrow(Promise);
});

test('loading以外ならそのままloadableを返す', () => {
  const loadable = RecoilLoadable.of(123);
  useRecoilValueLoadableMock.mockReturnValue(loadable);

  expect(useLoadRecoilValue(constSelector(123))).toBe(loadable);
});
