import { Loadable, RecoilValue, useRecoilValueLoadable } from 'recoil';

export const useLoadRecoilValue = <T>(recoilValue: RecoilValue<T>): Loadable<T> => {
  const loadable = useRecoilValueLoadable(recoilValue);

  if (loadable.state === 'loading') {
    throw new Promise((resolve) => {
      loadable.contents.finally(() => resolve(null));
    });
  }

  return loadable;
};
