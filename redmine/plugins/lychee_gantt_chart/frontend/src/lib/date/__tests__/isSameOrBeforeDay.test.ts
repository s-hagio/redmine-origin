import isSameOrBeforeDay from '../isSameOrBeforeDay';

test('自身が対象日よりも前ならtrueを返す', () => {
  expect(isSameOrBeforeDay('2022-03-01', new Date(2022, 2, 2))).toBe(true);
  expect(isSameOrBeforeDay(new Date(2022, 2, 2), '2022-03-01')).toBe(false);
  expect(isSameOrBeforeDay('2022-03-01', '2022-03-01')).toBe(true);
});
