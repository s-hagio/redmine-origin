import formatISODate from '../formatISODate';

test('ISO日付文字列にフォーマットする', () => {
  expect(formatISODate(new Date(2022, 2, 1, 12, 34, 56))).toBe('2022-03-01');
  expect(formatISODate('2022-03-01')).toBe('2022-03-01');
});
