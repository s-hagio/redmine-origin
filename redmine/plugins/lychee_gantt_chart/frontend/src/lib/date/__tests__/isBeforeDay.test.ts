import isBeforeDay from '../isBeforeDay';

test('自身が対象日よりも前ならtrueを返す', () => {
  expect(isBeforeDay('2022-03-01', new Date(2022, 2, 2))).toBe(true);
  expect(isBeforeDay(new Date(2022, 2, 2), '2022-03-01')).toBe(false);
  expect(isBeforeDay('2022-03-01', '2022-03-01')).toBe(false);
});
