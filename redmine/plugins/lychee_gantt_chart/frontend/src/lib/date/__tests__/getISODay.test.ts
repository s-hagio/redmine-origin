import getISODay from '../getISODay';

test('ISO曜日番号を返す', () => {
  expect(getISODay(new Date(2022, 1, 28))).toBe(1);

  expect(getISODay('2022-03-07')).toBe(1);
  expect(getISODay('2022-03-08')).toBe(2);
  expect(getISODay('2022-03-09')).toBe(3);
  expect(getISODay('2022-03-10')).toBe(4);
  expect(getISODay('2022-03-11')).toBe(5);
  expect(getISODay('2022-03-12')).toBe(6);
  expect(getISODay('2022-03-13')).toBe(7);
});
