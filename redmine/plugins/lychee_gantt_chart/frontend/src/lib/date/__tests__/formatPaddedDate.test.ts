import formatPaddedDate from '../formatPaddedDate';

test('ゼロ埋めした「日」にフォーマットする', () => {
  expect(formatPaddedDate(new Date(2022, 2, 1))).toBe('01');
  expect(formatPaddedDate('2022-03-01')).toBe('01');
});
