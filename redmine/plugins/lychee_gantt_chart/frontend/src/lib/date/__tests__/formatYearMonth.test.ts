import formatYearMonth from '../formatYearMonth';

test('年月文字列にフォーマットする', () => {
  expect(formatYearMonth(new Date(2022, 2, 1))).toBe('2022-3');
  expect(formatYearMonth('2022-03-01')).toBe('2022-3');
});

test('オプションpadがtrueなら月をゼロ埋め', () => {
  expect(formatYearMonth(new Date(2022, 2, 1), { pad: true })).toBe('2022-03');
});
