import getYear from '../getYear';

test('「年」を返す', () => {
  expect(getYear(new Date(2022, 2, 1))).toBe(2022);
  expect(getYear('2022-03-01')).toBe(2022);
});
