import { getISOWeek as dateFnsGetISOWeek } from 'date-fns';
import getISOWeek from '../getISOWeek';

vi.mock('date-fns', async () => ({
  ...(await vi.importActual<object>('date-fns')),
  getISOWeek: vi.fn().mockReturnValue(123456),
}));

test('ISO週番号を返す', () => {
  const date = new Date(2022, 2, 1, 0, 0, 0, 0);

  expect(getISOWeek(date)).toBe(123456);
  expect(dateFnsGetISOWeek).toBeCalledWith(date);
});

test('日付文字列での指定も可', () => {
  const date = new Date(2022, 2, 1, 0, 0, 0, 0);
  const dateString = '2022-03-01';

  expect(getISOWeek(dateString)).toBe(123456);
  expect(dateFnsGetISOWeek).toBeCalledWith(date);
});
