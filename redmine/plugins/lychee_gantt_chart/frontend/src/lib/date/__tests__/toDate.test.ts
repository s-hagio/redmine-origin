import toDate from '../toDate';

test('時間をリセット', () => {
  const date = toDate('2023-11-23T12:34:56.789');

  expect(date.getHours()).toBe(0);
  expect(date.getMinutes()).toBe(0);
  expect(date.getSeconds()).toBe(0);
});

test('preserveTime: trueを指定した場合は時間をリセットしない', () => {
  const date = toDate('2023-11-23T12:34:56.789', { preserveTime: true });

  expect(date.getHours()).toBe(12);
  expect(date.getMinutes()).toBe(34);
  expect(date.getSeconds()).toBe(56);
});
