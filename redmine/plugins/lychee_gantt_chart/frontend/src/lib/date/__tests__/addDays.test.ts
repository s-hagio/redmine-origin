import formatISODate from '../formatISODate';
import addDays from '../addDays';

test('指定日数を追加した新しいDateオブジェクトを返す', () => {
  const originalDate = new Date(2022, 2, 1, 0, 0, 0); // 2022-03-01
  const actualDate = addDays(originalDate, 10);

  expect(formatISODate(actualDate)).toBe('2022-03-11');
  expect(actualDate).not.toBe(originalDate);
});

test('日付文字列での指定も可', () => {
  expect(formatISODate(addDays('2022-03-01', 2))).toBe('2022-03-03');
});
