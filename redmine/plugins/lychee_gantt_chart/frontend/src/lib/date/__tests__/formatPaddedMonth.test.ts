import formatPaddedMonth from '../formatPaddedMonth';

test('ゼロ埋めした「月」にフォーマットする', () => {
  expect(formatPaddedMonth(new Date(2022, 2, 1))).toBe('03');
  expect(formatPaddedMonth('2022-03-01')).toBe('03');
});
