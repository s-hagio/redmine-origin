export type ISODateString = string;
export type DateArgument = Date | ISODateString;
