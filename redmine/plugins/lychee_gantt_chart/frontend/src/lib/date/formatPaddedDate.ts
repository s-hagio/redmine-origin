import { DateArgument } from './types';
import toDate from './toDate';

const formatPaddedDate = (arg: DateArgument): string => {
  const date = toDate(arg);

  return (date.getDate() + '').padStart(2, '0');
};

export default formatPaddedDate;
