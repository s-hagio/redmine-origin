import { getISODay as __getISODay } from 'date-fns';
import { DateArgument } from './types';
import toDate from './toDate';

const getISODay = (arg: DateArgument): number => {
  const date = toDate(arg);

  return __getISODay(date);
};

export default getISODay;
