import { DateArgument } from './types';
import toDate from './toDate';

const getYear = (arg: DateArgument) => {
  const date = toDate(arg);

  return date.getFullYear();
};

export default getYear;
