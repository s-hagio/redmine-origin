import { DateArgument } from './types';
import toDate from './toDate';

const addDays = (arg: DateArgument, amount: number): Date => {
  const date = toDate(arg);

  date.setDate(date.getDate() + amount);

  return date;
};

export default addDays;
