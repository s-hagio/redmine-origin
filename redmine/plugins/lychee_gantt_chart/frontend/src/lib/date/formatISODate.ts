import { DateArgument, ISODateString } from './types';
import toDate from './toDate';
import formatPaddedDate from './formatPaddedDate';
import formatPaddedMonth from './formatPaddedMonth';

const formatISODate = (arg: DateArgument): ISODateString => {
  const date = toDate(arg);

  return `${date.getFullYear()}-${formatPaddedMonth(date)}-${formatPaddedDate(date)}`;
};

export default formatISODate;
