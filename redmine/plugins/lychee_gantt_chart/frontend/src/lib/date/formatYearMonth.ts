import { DateArgument } from './types';
import toDate from './toDate';
import formatPaddedMonth from './formatPaddedMonth';

type Options = {
  pad?: boolean;
};

const formatYearMonth = (arg: DateArgument, options: Options = {}): string => {
  const date = toDate(arg);
  const month = options.pad ? formatPaddedMonth(date) : date.getMonth() + 1;

  return `${date.getFullYear()}-${month}`;
};

export default formatYearMonth;
