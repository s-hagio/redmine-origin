import { getISOWeek as __getISOWeek } from 'date-fns';
import toDate from './toDate';
import { DateArgument } from './types';

const getISOWeek = (arg: DateArgument) => {
  const date = toDate(arg);

  return __getISOWeek(date);
};

export default getISOWeek;
