import { DateArgument } from './types';
import toDate from './toDate';

const isBeforeDay = (contextDateArg: DateArgument, targetDateArg: DateArgument) => {
  const contextDate = toDate(contextDateArg);
  const targetDate = toDate(targetDateArg);

  return contextDate < targetDate;
};

export default isBeforeDay;
