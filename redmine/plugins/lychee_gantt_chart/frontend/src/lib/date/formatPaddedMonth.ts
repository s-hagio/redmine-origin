import { DateArgument } from './types';
import toDate from './toDate';

const formatPaddedMonth = (arg: DateArgument): string => {
  const date = toDate(arg);

  return `${date.getMonth() + 1}`.padStart(2, '0');
};

export default formatPaddedMonth;
