import { parseISO, isDate } from 'date-fns';
import { isString } from '@/utils/string';
import { DateArgument } from './types';

type Options = {
  preserveTime?: boolean;
};

const toDate = (arg: DateArgument, options: Options = {}) => {
  let date;

  if (isString(arg)) {
    date = parseISO(arg);
  } else if (isDate(arg)) {
    date = new Date(arg.getTime());
  }

  if (!date) {
    throw new Error(`Invalid Argument.`);
  }

  if (!options.preserveTime) {
    date.setHours(0, 0, 0, 0);
  }

  return date;
};

export default toDate;
