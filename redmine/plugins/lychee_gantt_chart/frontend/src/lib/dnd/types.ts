import {
  Data as DataBase,
  DataRef,
  Active as ActiveBase,
  Over as OverBase,
  DragStartEvent as DragStartEventBase,
  DragMoveEvent as DragMoveEventBase,
  DragOverEvent as DragOverEventBase,
  DragEndEvent as DragEndEventBase,
  DragCancelEvent as DragCancelEventBase,
} from '@dnd-kit/core';

export type Data = DataBase & {
  type: string;
};

export type Active<T extends Data> = Omit<ActiveBase, 'data'> & {
  data: DataRef<T>;
};

export type Over<T extends Data> = Omit<OverBase, 'data'> & {
  data: DataRef<T>;
};

export type WithTypedDataProps<T extends Data> = {
  active: Active<T>;
  over: Over<T>;
};

export type ObjectWithTypedDataProps<Base, T extends Data> = Omit<Base, keyof WithTypedDataProps<T>> &
  WithTypedDataProps<T>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type DndHookResult<F extends (...args: any) => any, T extends Data> = ObjectWithTypedDataProps<
  ReturnType<F>,
  T
>;

export type DragStartEvent<T extends Data = Data> = ObjectWithTypedDataProps<DragStartEventBase, T>;
export type DragStartEventHandler<T extends Data = Data> = (event: DragStartEvent<T>) => void;

export type DragMoveEvent<T extends Data = Data> = ObjectWithTypedDataProps<DragMoveEventBase, T>;
export type DragMoveEventHandler<T extends Data = Data> = (event: DragMoveEvent<T>) => void;

export type DragOverEvent<T extends Data = Data> = ObjectWithTypedDataProps<DragOverEventBase, T>;
export type DragOverEventHandler<T extends Data = Data> = (event: DragOverEvent<T>) => void;

export type DragEndEvent<T extends Data = Data> = ObjectWithTypedDataProps<DragEndEventBase, T>;
export type DragEndEventHandler<T extends Data = Data> = (event: DragEndEvent<T>) => void;

export type DragCancelEvent<T extends Data = Data> = ObjectWithTypedDataProps<DragCancelEventBase, T>;
export type DragCancelEventHandler<T extends Data = Data> = (event: DragCancelEvent<T>) => void;

export type DragEvent<T extends Data = Data> =
  | DragStartEvent<T>
  | DragMoveEvent<T>
  | DragOverEvent<T>
  | DragEndEvent<T>
  | DragCancelEvent<T>;

export type DragEventHandlerMap<T extends Data = Data> = {
  onDragStart?: DragStartEventHandler<T>;
  onDragMove?: DragMoveEventHandler<T>;
  onDragOver?: DragOverEventHandler<T>;
  onDragEnd?: DragEndEventHandler<T>;
  onDragCancel?: DragCancelEventHandler<T>;
};
