import * as React from 'react';
import { useDndMonitor } from '@dnd-kit/core';

const DndMonitor: React.FC = () => {
  useDndMonitor({
    onDragStart(event) {
      console.debug('onDragStart', event);
    },
    onDragMove(event) {
      console.debug('onDragMove', event);
    },
    onDragOver(event) {
      console.debug('onDragOver', event);
    },
    onDragEnd(event) {
      console.debug('onDragEnd', event);
    },
    onDragCancel(event) {
      console.debug('onDragCancel', event);
    },
  });

  return null;
};

export default DndMonitor;
