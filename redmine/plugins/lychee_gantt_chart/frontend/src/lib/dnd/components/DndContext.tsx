import * as React from 'react';
import {
  DndContext as DndContextBase,
  useSensor,
  useSensors,
  PointerSensor,
  TouchSensor,
  DndContextProps,
  pointerWithin,
} from '@dnd-kit/core';
import DndMonitor from './DndMonitor';

type Props = DndContextProps;

const DndContext: React.FC<Props> = ({ sensors, collisionDetection, children, ...props }) => {
  // ドラッグ要素内でクリックが効かなくなるので { distance: 1 } で対応
  const defaultSensors = useSensors(
    useSensor(PointerSensor, { activationConstraint: { distance: 1 } }),
    useSensor(TouchSensor, { activationConstraint: { distance: 1 } })
  );

  return (
    <DndContextBase
      sensors={sensors ?? defaultSensors}
      collisionDetection={collisionDetection ?? pointerWithin}
      {...props}
    >
      {children}

      {import.meta.env.DEV && !import.meta.env.VITEST && <DndMonitor />}
    </DndContextBase>
  );
};

export default DndContext;
