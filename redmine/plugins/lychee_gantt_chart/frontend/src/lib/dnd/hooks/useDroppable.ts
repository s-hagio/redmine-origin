import {
  useDroppable as useDroppableBase,
  UseDroppableArguments as UseDroppableArgumentsBase,
} from '@dnd-kit/core';
import { Data, DndHookResult } from '../types';

export type UseDroppableArguments<T extends Data> = Omit<UseDroppableArgumentsBase, 'data'> & {
  data?: T;
};

export type UseDroppableResult<T extends Data> = DndHookResult<typeof useDroppableBase, T>;

export const useDroppable = <T extends Data>(args: UseDroppableArguments<T>): UseDroppableResult<T> => {
  return useDroppableBase(args) as UseDroppableResult<T>;
};
