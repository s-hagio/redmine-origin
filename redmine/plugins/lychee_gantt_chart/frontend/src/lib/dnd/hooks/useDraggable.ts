import {
  useDraggable as useDraggableBase,
  UseDraggableArguments as UseDraggableArgumentsBase,
} from '@dnd-kit/core';
import { Data, DndHookResult } from '../types';

export type UseDraggableArguments<T extends Data> = Omit<UseDraggableArgumentsBase, 'data'> & {
  data?: T;
};

export type UseDraggableResult<T extends Data> = DndHookResult<typeof useDraggableBase, T>;

export const useDraggable = <T extends Data>(args: UseDraggableArguments<T>): UseDraggableResult<T> => {
  return useDraggableBase(args) as UseDraggableResult<T>;
};
