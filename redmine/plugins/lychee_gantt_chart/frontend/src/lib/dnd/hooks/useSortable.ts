import {
  useSortable as useSortableBase,
  UseSortableArguments as UseSortableArgumentsBase,
} from '@dnd-kit/sortable';
import { Data, DndHookResult } from '../types';

export type UseSortableArguments<T extends Data> = Omit<UseSortableArgumentsBase, 'data'> & {
  data?: T;
};

export type UseSortableResult<T extends Data> = DndHookResult<typeof useSortableBase, T>;

export const useSortable = <T extends Data>(args: UseSortableArguments<T>): UseSortableResult<T> => {
  // { disabled: true }と指定すると、ドラッグはできないがドロップ対象にはなってしまう問題:
  //
  // dnd kitの後方互換性維持によってdroppable: { disabled: boolean }が常にfalseになってしまうのが原因。
  // draggableとdroppableを個別で指定することで回避できる。
  // https://github.com/clauderic/dnd-kit/pull/754
  if (typeof args.disabled === 'boolean') {
    args.disabled = {
      draggable: args.disabled,
      droppable: args.disabled,
    };
  }

  return useSortableBase(args) as UseSortableResult<T>;
};
