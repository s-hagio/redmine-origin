import * as React from 'react';
import { render } from '@testing-library/react';
import { TranslationProvider } from '@/providers/translationProvider';

type RenderParams = Parameters<typeof render>;

type Options = RenderParams[1] & {
  lang?: string;
};

export const renderWithTranslation = (ui: RenderParams[0], options?: Options) => {
  return render(ui, {
    ...options,
    wrapper: ({ children }) => (
      <TranslationProvider lang={options?.lang ?? 'ja'}>{children}</TranslationProvider>
    ),
  });
};
