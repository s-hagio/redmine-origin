import * as React from 'react';
import { renderHook, RenderHookOptions } from '@testing-library/react';
import { TranslationProvider } from '@/providers/translationProvider';

type Options<TProps> = RenderHookOptions<TProps> & {
  lang?: string;
};

export const renderHookWithTranslation = <TProps, TResult>(
  callback: (props: TProps) => TResult,
  options?: Options<TProps>
) => {
  return renderHook(callback, {
    ...options,
    wrapper: ({ children }) => (
      <TranslationProvider lang={options?.lang ?? 'ja'}>{children}</TranslationProvider>
    ),
  });
};
