import * as React from 'react';
import { MutableSnapshot, RecoilRoot } from 'recoil';
import { render, RenderHookOptions, RenderResult } from '@testing-library/react';

type RenderParams = Parameters<typeof render>;

type OptionalRecoilRootProps = {
  initializeState?: (mutableSnapshot: MutableSnapshot) => void;
  override?: boolean;
};

type RenderWithRecoilHookResult<Result> = RenderResult & {
  result: {
    current: Result;
  };
};

export const renderWithRecoilHook = <Result, Props>(
  ui: RenderParams[0],
  renderCallback: () => Result,
  recoilRootProps?: OptionalRecoilRootProps,
  options?: RenderHookOptions<Props>
): RenderWithRecoilHookResult<Result> => {
  const hookResult = React.createRef() as React.MutableRefObject<Result>;

  const TestComponent: React.FC = () => {
    const pendingResult = renderCallback();

    React.useEffect(() => {
      hookResult.current = pendingResult;
    }, [pendingResult]);

    return null;
  };

  const renderResult = render(ui, {
    ...options,
    wrapper: ({ children }) => (
      <RecoilRoot {...recoilRootProps}>
        <TestComponent />
        {children}
      </RecoilRoot>
    ),
  });

  return {
    ...renderResult,
    result: hookResult,
  };
};
