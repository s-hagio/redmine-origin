import * as React from 'react';
import { MutableSnapshot, RecoilRoot } from 'recoil';
import { renderHook, RenderHookOptions } from '@testing-library/react';

type OptionalRecoilRootProps = {
  initializeState?: (mutableSnapshot: MutableSnapshot) => void;
  override?: boolean;
};

export const renderRecoilHook = <P, R>(
  callback: (props: P) => R,
  recoilRootProps?: OptionalRecoilRootProps,
  options?: RenderHookOptions<P>
) => {
  return renderHook(callback, {
    ...options,
    wrapper: ({ children }) => (
      <RecoilRoot {...recoilRootProps}>
        {options?.wrapper ? <options.wrapper>{children}</options.wrapper> : children}
      </RecoilRoot>
    ),
  });
};
