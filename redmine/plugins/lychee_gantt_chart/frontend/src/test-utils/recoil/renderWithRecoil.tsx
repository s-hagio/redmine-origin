import * as React from 'react';
import { MutableSnapshot, RecoilRoot } from 'recoil';
import { render, RenderResult } from '@testing-library/react';

type RenderParams = Parameters<typeof render>;

type OptionalRecoilRootProps = {
  initializeState?: (mutableSnapshot: MutableSnapshot) => void;
  override?: boolean;
};

export const renderWithRecoil = (
  ui: RenderParams[0],
  recoilRootProps?: OptionalRecoilRootProps,
  options?: RenderParams[1]
): RenderResult => {
  return render(ui, {
    ...options,
    wrapper: ({ children }) => <RecoilRoot {...recoilRootProps}>{children}</RecoilRoot>,
  });
};
