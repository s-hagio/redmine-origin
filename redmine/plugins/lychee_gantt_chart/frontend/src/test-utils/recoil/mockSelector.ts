import { RecoilValueReadOnly, selector } from 'recoil';

let keyCounter = 0;

export const mockSelector = <T>(value: T): RecoilValueReadOnly<T> => {
  return selector({
    key: `mockSelector__${++keyCounter}`,
    get: () => value,
    cachePolicy_UNSTABLE: {
      eviction: 'most-recent',
    },
  });
};
