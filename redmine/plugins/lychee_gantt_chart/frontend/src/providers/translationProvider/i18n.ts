import { default as i18n } from 'i18next';
import { initReactI18next } from 'react-i18next';
import * as resources from '@/locales';

i18n.use(initReactI18next).init({
  debug: import.meta.env.MODE === 'development',
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false,
  },
  resources: resources,
});

export default i18n;
