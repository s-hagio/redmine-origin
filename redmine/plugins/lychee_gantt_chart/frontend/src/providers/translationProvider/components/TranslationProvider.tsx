import * as React from 'react';
import { setDefaultOptions } from 'date-fns';
import { I18nextProvider } from 'react-i18next';
import { ja } from '../dateLocale';
import i18n from '../i18n';

type Props = {
  lang: string;
  children: React.ReactNode;
};

const TranslationProvider: React.FC<Props> = ({ lang, children }) => {
  i18n.changeLanguage(lang);

  if (lang === 'ja') {
    setDefaultOptions({ locale: ja });
  }

  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};

export default TranslationProvider;
