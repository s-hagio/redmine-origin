import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { renderHook } from '@testing-library/react';
import TranslationProvider from '../TranslationProvider';

test('i18nextが初期化されて翻訳が正常に行える', () => {
  const { result } = renderHook(() => useTranslation(), {
    wrapper: ({ children }) => <TranslationProvider lang="ja">{children}</TranslationProvider>,
  });

  expect(result.current.t('field.subject')).toBe('題名');
});
