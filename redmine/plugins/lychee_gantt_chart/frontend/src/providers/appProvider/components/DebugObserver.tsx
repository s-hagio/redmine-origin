import * as React from 'react';
import { useRecoilTransactionObserver_UNSTABLE } from 'recoil';

const ignoredKeys: Set<string> = new Set(['userInteraction/currentMousePosition']);

export const DebugObserver: React.FC = () => {
  useRecoilTransactionObserver_UNSTABLE(({ previousSnapshot, snapshot }) => {
    const nodes = [...snapshot.getNodes_UNSTABLE({ isModified: true })].filter(
      (node) => !ignoredKeys.has(node.key)
    );

    if (!nodes.length) {
      return;
    }

    for (const node of nodes) {
      const prevValueLoadable = previousSnapshot.getLoadable(node);
      const currentValueLoadable = snapshot.getLoadable(node);

      if (prevValueLoadable.state !== 'hasValue' || currentValueLoadable.state !== 'hasValue') {
        continue;
      }

      const prev = prevValueLoadable.contents;
      const current = currentValueLoadable.contents;

      console.debug(
        `%c${node.key}`,
        'color: #fff; background: #7e3d7f; border-radius: 3px; padding: 3px;',
        prev,
        current
      );
    }
  });

  return null;
};
