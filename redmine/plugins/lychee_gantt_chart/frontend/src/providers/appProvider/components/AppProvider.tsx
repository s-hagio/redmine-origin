import * as React from 'react';
import { RecoilRoot } from 'recoil';
import { createStateInitializer } from '@/models';
import { CurrentUser } from '@/models/currentUser';
import { Settings } from '@/models/setting';
import { TranslationProvider } from '../../translationProvider';
import { DebugObserver } from './DebugObserver';

import 'react-datepicker/dist/react-datepicker.css';

type Props = {
  children: React.ReactNode;
  currentUser: CurrentUser;
  settings: Settings;
};

const AppProvider: React.FC<Props> = ({ children, currentUser, settings }) => {
  return (
    <RecoilRoot initializeState={createStateInitializer(currentUser, settings)}>
      {import.meta.env.DEV && <DebugObserver />}

      <TranslationProvider lang={settings.lang}>{children}</TranslationProvider>
    </RecoilRoot>
  );
};

export default AppProvider;
