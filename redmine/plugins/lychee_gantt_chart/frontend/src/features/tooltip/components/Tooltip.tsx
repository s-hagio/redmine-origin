import * as React from 'react';
import { css } from '@linaria/core';
import {
  arrow,
  autoUpdate,
  flip,
  FloatingArrow,
  FloatingPortal,
  offset,
  ReferenceType,
  shift,
  useFloating,
  useHover,
  useInteractions,
} from '@floating-ui/react';
import { getZIndex } from '@/styles';

type Props = React.PropsWithChildren<{
  renderReference: (
    setReference: (node: ReferenceType | null) => void,
    props: Record<string, unknown>
  ) => React.ReactNode;
}>;

const ARROW_HEIGHT = 5;

const Tooltip: React.FC<Props> = ({ renderReference, children }) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const arrowRef = React.useRef(null);

  const { refs, floatingStyles, context } = useFloating({
    open: isOpen,
    onOpenChange: setIsOpen,
    placement: 'bottom',
    middleware: [offset(ARROW_HEIGHT), flip(), shift(), arrow({ element: arrowRef, padding: 4 })],
    whileElementsMounted: autoUpdate,
  });

  const hover = useHover(context, { move: false });
  const { getReferenceProps, getFloatingProps } = useInteractions([hover]);

  return (
    <>
      {renderReference(refs.setReference, getReferenceProps())}

      <FloatingPortal>
        {isOpen && (
          <div ref={refs.setFloating} className={tooltipStyle} style={floatingStyles} {...getFloatingProps()}>
            {children}

            <FloatingArrow
              ref={arrowRef}
              context={context}
              width={10}
              height={ARROW_HEIGHT}
              tipRadius={1}
              className={arrowStyle}
            />
          </div>
        )}
      </FloatingPortal>
    </>
  );
};

export default Tooltip;

const tooltipStyle = css`
  padding: 0.4rem 0.6rem;
  border-radius: 4px;
  background: rgba(0, 0, 0, 0.75);
  color: #fff;
  z-index: ${getZIndex('tooltip/Tooltip')};
`;

const arrowStyle = css`
  fill: rgba(0, 0, 0, 0.75);
`;
