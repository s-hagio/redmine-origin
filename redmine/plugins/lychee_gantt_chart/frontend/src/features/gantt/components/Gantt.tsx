import * as React from 'react';
import {
  RowBoundingRectStyles,
  RowSelectKeyboardObserver,
  RowDeselector,
  RowStateRefresher,
} from '@/features/row';
import { RowResourceLoader } from '@/features/resourceLoader';
import Header from './Header';
import Body from './Body';

const Gantt: React.FC = () => {
  return (
    <>
      <RowStateRefresher />
      <RowSelectKeyboardObserver />
      <RowDeselector />
      <RowBoundingRectStyles />
      <RowResourceLoader />

      <Header />
      <Body />
    </>
  );
};

export default React.memo(Gantt);
