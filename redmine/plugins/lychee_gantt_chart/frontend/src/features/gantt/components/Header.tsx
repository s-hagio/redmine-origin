import * as React from 'react';
import { css, cx } from '@linaria/core';
import { Toolbar } from '@/features/toolbar';
import { DataTableHeader } from '@/features/dataTable';
import { TimelineHeader } from '@/features/timeline';
import { getZIndex, borderColor } from '@/styles';

const GanttHeader: React.FC = () => {
  const elementRef = React.useRef<HTMLDivElement>(null);
  const fixableContentsElementRef = React.useRef<HTMLDivElement>(null);

  const offsetTopRef = React.useRef<number>(0);

  const [width, setWidth] = React.useState<number>(-1);
  const [height, setHeight] = React.useState<number | string>('');
  const [isFixed, setFixed] = React.useState<boolean>(false);

  const handleScroll = React.useCallback(() => {
    setFixed(offsetTopRef.current < window.scrollY);
  }, []);

  const reflectFixableContentsHeight = () => {
    setHeight(fixableContentsElementRef.current?.clientHeight ?? 0);
  };

  React.useEffect(() => {
    if (!elementRef.current || !fixableContentsElementRef.current) {
      return;
    }

    offsetTopRef.current = elementRef.current.offsetTop;

    reflectFixableContentsHeight();

    const mutationObserver = new MutationObserver(reflectFixableContentsHeight);
    mutationObserver.observe(fixableContentsElementRef.current, { childList: true, subtree: true });

    const resizeObserver = new ResizeObserver(([entry]) => setWidth(entry.contentRect.width));
    resizeObserver.observe(elementRef.current);

    handleScroll();
    window.addEventListener('scroll', handleScroll, { passive: true });

    return () => {
      mutationObserver.disconnect();
      resizeObserver.disconnect();
      window.removeEventListener('scroll', handleScroll);
    };
  }, [handleScroll]);

  return (
    <div ref={elementRef} className={style} style={{ height }}>
      <div
        ref={fixableContentsElementRef}
        className={cx(fixableContentsStyle, isFixed && fixedContentsStyle)}
        style={{ width }}
      >
        <Toolbar />

        <div className={contentHeadersStyle}>
          <DataTableHeader containerWidth={width} />
          <TimelineHeader />
        </div>
      </div>
    </div>
  );
};

export default React.memo(GanttHeader);

const style = css`
  position: relative;
`;

const fixableContentsStyle = css`
  position: absolute;
  top: 0;
  left: 0;
  background: #fff;
  z-index: ${getZIndex('gantt/Header')};
`;

const fixedContentsStyle = css`
  position: fixed;
  top: 0;
  left: auto;
`;

const contentHeadersStyle = css`
  display: grid;
  grid-template-columns: auto minmax(0, 1fr);
  margin-top: 1px;
  border: 1px solid ${borderColor};
  border-bottom: 0;
`;
