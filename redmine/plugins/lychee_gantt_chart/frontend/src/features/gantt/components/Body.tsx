import * as React from 'react';
import { css } from '@linaria/core';
import { DataTable } from '@/features/dataTable';
import { Timeline } from '@/features/timeline';
import { RowVisualizer, useHighlightRowByOffsetTop } from '@/features/row';
import { IssueCreationContainer } from '@/features/issueCreation';
import { borderColor } from '@/styles';

const GanttBody: React.FC = () => {
  const highlightRowByOffsetTop = useHighlightRowByOffsetTop();

  const handleMouseMove = React.useCallback(
    (event: React.MouseEvent<HTMLDivElement>) => {
      highlightRowByOffsetTop(window.scrollY + (event.clientY - event.currentTarget.offsetTop));
    },
    [highlightRowByOffsetTop]
  );

  const handleMouseLeave = React.useCallback(() => {
    highlightRowByOffsetTop(-1);
  }, [highlightRowByOffsetTop]);

  return (
    <div className={style} onMouseMove={handleMouseMove} onMouseLeave={handleMouseLeave}>
      <DataTable />
      <Timeline />
      <RowVisualizer />
      <IssueCreationContainer />
    </div>
  );
};

export default React.memo(GanttBody);

const style = css`
  position: relative;
  display: flex;
  border: solid 1px ${borderColor};
  border-top: none;

  > :nth-child(2) {
    flex: 1;
  }
`;
