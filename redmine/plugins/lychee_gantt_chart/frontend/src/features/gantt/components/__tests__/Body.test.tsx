import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import GanttBody from '../Body';

vi.mock('@/features/dataTable', () => ({
  DataTable: vi.fn((props) => `DataTable: ${JSON.stringify(props)}`),
}));

vi.mock('@/features/timeline', () => ({
  Timeline: vi.fn((props) => `Timeline: ${JSON.stringify(props)}`),
}));

const highlightRowByOffsetTopMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/row', () => ({
  RowVisualizer: vi.fn((props) => `RowVisualizer: ${JSON.stringify(props)}`),
  useHighlightRowByOffsetTop: vi.fn().mockReturnValue(highlightRowByOffsetTopMock),
}));

vi.mock('@/features/issueCreation', () => ({
  IssueCreationContainer: vi.fn((props) => `IssueCreationContainer: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const result = render(<GanttBody />);

  expect(result.container).toMatchSnapshot();
});

test('mousemove時に行のハイライト処理を実行', () => {
  const { container } = render(<GanttBody />);
  const el = container.firstChild as HTMLElement;

  expect(highlightRowByOffsetTopMock).not.toHaveBeenCalled();

  vi.spyOn(window, 'scrollY', 'get').mockReturnValue(50);
  vi.spyOn(el, 'offsetTop', 'get').mockReturnValue(3);

  fireEvent.mouseMove(el, { clientY: 100 });
  expect(highlightRowByOffsetTopMock).toHaveBeenCalledWith(147); // 50 - 3 + 100

  fireEvent.mouseMove(el, { clientY: 120 });
  expect(highlightRowByOffsetTopMock).toHaveBeenCalledWith(167); // 50 - 3 + 120

  fireEvent.mouseLeave(el);
  expect(highlightRowByOffsetTopMock).toHaveBeenCalledWith(-1);
});
