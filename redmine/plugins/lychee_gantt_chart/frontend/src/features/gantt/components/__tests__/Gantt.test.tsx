import * as React from 'react';
import { render } from '@testing-library/react';
import Gantt from '../Gantt';

vi.mock('@/features/row', async () => ({
  ...(await vi.importActual<object>('@/features/row')),
  RowStateRefresher: vi.fn((props) => `RowStateRefresher: ${JSON.stringify(props)}`),
  RowBoundingRectStyles: vi.fn((props) => `RowBoundingRectStyles: ${JSON.stringify(props)}`),
  RowSelectKeyboardObserver: vi.fn((props) => `RowSelectKeyboardObserver: ${JSON.stringify(props)}`),
  RowDeselector: vi.fn((props) => `RowDeselector: ${JSON.stringify(props)}`),
}));

vi.mock('@/features/resourceLoader', () => ({
  RowResourceLoader: vi.fn((props) => `RowResourceLoader: ${JSON.stringify(props)}`),
}));

vi.mock('../Header', () => ({
  default: vi.fn((props) => `GanttHeader: ${JSON.stringify(props)}`),
}));

vi.mock('../Body', () => ({
  default: vi.fn((props) => `GanttBody: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const result = render(<Gantt />);

  expect(result.container).toMatchSnapshot();
});
