import * as React from 'react';
import { act, render } from '@testing-library/react';
import GanttHeader from '../Header';

vi.mock('@/features/toolbar', () => ({
  Toolbar: vi.fn((props) => `Toolbar: ${JSON.stringify(props)}`),
}));

vi.mock('@/features/dataTable', () => ({
  DataTableHeader: vi.fn((props) => `DataTableHeader: ${JSON.stringify(props)}`),
}));

vi.mock('@/features/timeline', () => ({
  TimelineHeader: vi.fn((props) => `TimelineHeader: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const resizeObserverSpy = vi.spyOn(window, 'ResizeObserver').mockImplementation(() => ({
    observe: vi.fn(),
    unobserve: vi.fn(),
    disconnect: vi.fn(),
  }));

  const result = render(<GanttHeader />);

  // TODO: もうちょっといい感じに
  act(() => {
    const cb = resizeObserverSpy.mock.calls[0][0];
    cb([{ contentRect: { width: 123 } } as ResizeObserverEntry], new ResizeObserver(cb));
  });

  expect(result.container).toMatchSnapshot();
});

test.todo('width/heightの反映');
test.todo('MutationObserverの処理');
test.todo('ResizeObserverの処理');
