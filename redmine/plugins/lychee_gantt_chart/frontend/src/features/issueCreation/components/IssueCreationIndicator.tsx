import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { getFrontZIndex } from '@/styles';
import { useIsIssueCreationActive } from '../models/issueCreation';

const IssueCreationIndicator: React.FC = () => {
  const { t } = useTranslation();
  const isActive = useIsIssueCreationActive();

  if (!isActive) {
    return null;
  }

  return <div className={style}>{t('label.issueCreationMode')}</div>;
};

export default React.memo(IssueCreationIndicator);

const style = css`
  position: fixed;
  top: 0;
  left: 50%;
  display: flex;
  align-items: center;
  gap: 0.25rem;
  transform: translateX(-50%);
  padding: 0.5rem 0.75rem;
  border-radius: 0 0 4px 4px;
  background-color: #5e8f32;
  color: #fff;
  z-index: ${getFrontZIndex('issueCreation/IssueCreationPreparer')};
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);

  select {
    max-width: 200px;
  }
`;
