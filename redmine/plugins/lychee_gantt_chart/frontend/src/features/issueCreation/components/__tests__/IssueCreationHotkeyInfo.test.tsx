import * as React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithTranslation } from '@/test-utils';
import IssueCreationHotkeyInfo from '../IssueCreationHotkeyInfo';

vi.mock('@floating-ui/react', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  FloatingPortal: vi.fn((props) => React.createElement('floating-portal', props)),
  FloatingArrow: React.forwardRef<HTMLDivElement>(function FloatingArrow() {
    return React.createElement('floating-arrow');
  }),
}));

test('Render', () => {
  const { container } = renderWithTranslation(<IssueCreationHotkeyInfo />);

  expect(container).toMatchSnapshot();
});

test('マウスオーバーでメッセージ表示', () => {
  const { container } = renderWithTranslation(<IssueCreationHotkeyInfo />);
  const el = container.firstChild as HTMLElement;

  fireEvent.mouseEnter(el);

  expect(container).toMatchSnapshot();
});
