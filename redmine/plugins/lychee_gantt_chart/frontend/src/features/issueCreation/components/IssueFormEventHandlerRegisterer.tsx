import * as React from 'react';
import { IssueFormEventType, useRegisterIssueFormEventHandler } from '@/features/issueForm';
import { FEATURE_NAME } from '../constants';
import { useHandleIssueFormSaveSuccess } from '../hooks';

const IssueFormEventHandlerRegisterer: React.FC = () => {
  const registerIssueFormEventHandler = useRegisterIssueFormEventHandler();
  const handleIssueFormSaveSuccess = useHandleIssueFormSaveSuccess();

  React.useEffect(() => {
    registerIssueFormEventHandler(IssueFormEventType.SuccessSave, FEATURE_NAME, handleIssueFormSaveSuccess);
  }, [handleIssueFormSaveSuccess, registerIssueFormEventHandler]);

  return null;
};

export default React.memo(IssueFormEventHandlerRegisterer);
