import * as React from 'react';
import { renderWithRecoil } from '@/test-utils';
import IssueCreationContainer from '../IssueCreationContainer';

const useIsIssueCreationActiveMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/issueCreation', () => ({
  useIsIssueCreationActive: useIsIssueCreationActiveMock,
}));

vi.mock('../IssueFormEventHandlerRegisterer', () => ({
  default: vi.fn((props) => `IssueFormEventHandlerRegisterer: ${JSON.stringify(props)}`),
}));

vi.mock('../IssueCreationHotkeyObserver', () => ({
  default: vi.fn((props) => `IssueCreationHotkeyObserver: ${JSON.stringify(props)}`),
}));

vi.mock('../IssueCreationActivator', () => ({
  default: vi.fn((props) => `IssueCreationActivator: ${JSON.stringify(props)}`),
}));

vi.mock('../IssueCreationIndicator', () => ({
  default: vi.fn((props) => `IssueCreationIndicator: ${JSON.stringify(props)}`),
}));

vi.mock('../IssueCreationPreparer', () => ({
  default: vi.fn((props) => `IssueCreationPreparer: ${JSON.stringify(props)}`),
}));

test('Render: inactive', () => {
  useIsIssueCreationActiveMock.mockReturnValue(false);

  const { container } = renderWithRecoil(<IssueCreationContainer />);

  expect(container).toMatchSnapshot();
});

test('Render: active', () => {
  useIsIssueCreationActiveMock.mockReturnValue(true);

  const { container } = renderWithRecoil(<IssueCreationContainer />);

  expect(container).toMatchSnapshot();
});
