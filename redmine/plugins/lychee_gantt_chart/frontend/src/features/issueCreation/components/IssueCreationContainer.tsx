import * as React from 'react';
import { useIsIssueCreationActive } from '../models/issueCreation';
import IssueFormEventHandlerRegisterer from './IssueFormEventHandlerRegisterer';
import IssueCreationHotkeyObserver from './IssueCreationHotkeyObserver';
import IssueCreationActivator from './IssueCreationActivator';
import IssueCreationIndicator from './IssueCreationIndicator';
import IssueCreationPreparer from './IssueCreationPreparer';

const IssueCreationContainer: React.FC = () => {
  const isActive = useIsIssueCreationActive();

  return (
    <React.Suspense fallback={null}>
      <IssueFormEventHandlerRegisterer />
      <IssueCreationHotkeyObserver />
      <IssueCreationActivator />

      {isActive && (
        <>
          <IssueCreationIndicator />
          <IssueCreationPreparer />
        </>
      )}
    </React.Suspense>
  );
};

export default React.memo(IssueCreationContainer);
