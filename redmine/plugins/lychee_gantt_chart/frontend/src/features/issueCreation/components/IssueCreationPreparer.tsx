import * as React from 'react';
import { css } from '@linaria/core';
import { useCurrentMousePosition } from '@/models/userInteraction';
import { getZIndex } from '@/styles';
import { usePrepareIssueCreationByTop } from '../hooks';
import { useIsHotkeyPressedState } from '../models/issueCreation';

const IssueCreationPreparer: React.FC = () => {
  const ref = React.useRef<HTMLDivElement>(null);

  const [isHotkeyPressed] = useIsHotkeyPressedState();

  const mousePosition = useCurrentMousePosition();
  const prepareIssueCreationByTop = usePrepareIssueCreationByTop();

  const prepareByTop = React.useCallback(
    (absoluteTop: number) => {
      const el = ref.current;

      if (!el || !isHotkeyPressed) {
        return;
      }

      const bounds = el.getBoundingClientRect();
      const top = Math.round(absoluteTop - bounds.top);

      prepareIssueCreationByTop(top);
    },
    [isHotkeyPressed, prepareIssueCreationByTop]
  );

  const handleMouseMove = React.useCallback(
    (event: React.MouseEvent) => {
      prepareByTop(event.clientY);
    },
    [prepareByTop]
  );

  const isHotkeyPressedRef = React.useRef<boolean>(false);

  React.useEffect(() => {
    if (isHotkeyPressed !== isHotkeyPressedRef.current) {
      prepareByTop(mousePosition.y);
    }

    isHotkeyPressedRef.current = isHotkeyPressed;
  }, [prepareByTop, mousePosition.y, isHotkeyPressed]);

  return <div ref={ref} className={style} onMouseMove={handleMouseMove} />;
};

export default IssueCreationPreparer;

const style = css`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: ${getZIndex('issueCreation/IssueCreationPreparer')};
`;
