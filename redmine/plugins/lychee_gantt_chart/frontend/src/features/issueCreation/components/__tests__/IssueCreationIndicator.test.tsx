import * as React from 'react';
import { renderWithTranslation } from '@/test-utils';
import IssueCreationIndicator from '../IssueCreationIndicator';

const useIsIssueCreationActiveMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/issueCreation', () => ({
  useIsIssueCreationActive: useIsIssueCreationActiveMock,
}));

test.each([false, true])('Render: active == %s', (isActive) => {
  useIsIssueCreationActiveMock.mockReturnValue(isActive);

  const { container } = renderWithTranslation(<IssueCreationIndicator />);

  expect(container).toMatchSnapshot();
});
