import * as React from 'react';
import {
  useIsIssueCreationActive,
  useRefreshIssueCreationRowOptions,
  useDestroyIssueCreation,
} from '../models/issueCreation';

const IssueCreationActivator: React.FC = () => {
  const refreshRowOptions = useRefreshIssueCreationRowOptions();

  const isActive = useIsIssueCreationActive();
  const destroyIssueCreation = useDestroyIssueCreation();

  React.useEffect(refreshRowOptions, [refreshRowOptions]);

  React.useEffect(() => {
    if (!isActive) {
      destroyIssueCreation();
    }
  }, [isActive, destroyIssueCreation]);

  return null;
};

export default React.memo(IssueCreationActivator);
