import * as React from 'react';
import { useIsHotkeyPressedState } from '../models/issueCreation';

const isActivationKeyPressed = (event: KeyboardEvent) => {
  return event.shiftKey && (event.altKey || event.metaKey);
};

const IssueCreationHotkeyObserver: React.FC = () => {
  const [, setHotkeyPressed] = useIsHotkeyPressedState();

  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      if (isActivationKeyPressed(event)) {
        event.preventDefault();
        setHotkeyPressed(true);
      }
    };

    const handleKeyUp = (event: KeyboardEvent) => {
      setHotkeyPressed(isActivationKeyPressed(event));
    };

    document.addEventListener('keydown', handleKeyDown);
    document.addEventListener('keyup', handleKeyUp, { passive: true });

    return () => {
      document.removeEventListener('keydown', handleKeyDown);
      document.removeEventListener('keyup', handleKeyUp);
    };
  }, [setHotkeyPressed]);

  return null;
};

export default React.memo(IssueCreationHotkeyObserver);
