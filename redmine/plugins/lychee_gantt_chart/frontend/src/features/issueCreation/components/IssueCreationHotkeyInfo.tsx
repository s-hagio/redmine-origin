import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { Tooltip } from '@/features/tooltip';
import { Icon } from '@/components/Icon';

const hotkey = navigator.userAgent.includes('Mac OS')
  ? '<kbd><kbd>shift ⇧</kbd> + <kbd>command ⌘</kbd></kbd>'
  : '<kbd><kbd>Shift</kbd> + <kbd>Alt</kbd></kbd>';

const IssueCreationHotkeyInfo: React.FC = () => {
  const { t } = useTranslation();

  return (
    <Tooltip
      renderReference={(setReference, props) => (
        <span ref={setReference} className={style} {...props}>
          <Icon name="info" size={14} />
        </span>
      )}
    >
      <span
        className={helpMessageStyle}
        dangerouslySetInnerHTML={{
          __html: t('message.issueCreationHotkeyInfo', { hotkey }),
        }}
      />
    </Tooltip>
  );
};

export default IssueCreationHotkeyInfo;

const style = css`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 1.5em;
  height: 1.5em;
  margin-left: 0.25em;
  cursor: default;
`;

const helpMessageStyle = css`
  line-height: 1.8;
  white-space: pre-wrap;

  kbd kbd {
    padding: 0.05em 0.35em;
    border: 1px solid #eee;
    border-radius: 3px;
    font-weight: 700;
    font-family: monospace;
    line-height: 1;
    white-space: nowrap;
  }
`;
