import * as React from 'react';
import { useRecoilValue } from 'recoil';
import { fireEvent } from '@testing-library/react';
import { renderWithRecoilHook } from '@/test-utils';
import { isHotkeyPressedState } from '../../models/issueCreation';
import IssueCreationHotkeyObserver from '../IssueCreationHotkeyObserver';

test('ホットキーが押されたらisHotkeyPressedStateをtrueにする', () => {
  const { result } = renderWithRecoilHook(<IssueCreationHotkeyObserver />, () =>
    useRecoilValue(isHotkeyPressedState)
  );

  expect(result.current).toBe(false);

  fireEvent.keyDown(document, { shiftKey: true, altKey: true });
  expect(result.current).toBe(true);

  fireEvent.keyUp(document);
  expect(result.current).toBe(false);
});

test('altKeyの代わりにmetaKeyでも可', () => {
  const { result } = renderWithRecoilHook(<IssueCreationHotkeyObserver />, () =>
    useRecoilValue(isHotkeyPressedState)
  );

  expect(result.current).toBe(false);

  fireEvent.keyDown(document, { shiftKey: true, metaKey: true });
  expect(result.current).toBe(true);

  fireEvent.keyUp(document);
  expect(result.current).toBe(false);
});
