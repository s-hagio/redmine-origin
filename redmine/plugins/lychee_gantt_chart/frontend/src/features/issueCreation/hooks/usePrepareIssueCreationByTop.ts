import { useCallback } from 'react';
import { useSettings } from '@/models/setting';
import { getProjectResourceIdentifier } from '@/models/coreResource';
import { useRowBoundingRects, isIssueRow, useAvailableRows, isSubtreeRootRow } from '@/features/row';
import {
  PLACEHOLDER_ROW_ID,
  useInsertPlaceholderRow,
  useRemovePlaceholderRow,
} from '../models/placeholderRow';
import { useIsValidSubtreeRootRow, usePrepareIssueCreation } from '../models/issueCreation';

type InsertPlaceholderRow = (top: number) => void;

export const usePrepareIssueCreationByTop = (): InsertPlaceholderRow => {
  const { mainProject } = useSettings();

  const rows = useAvailableRows();
  const rowBoundingRects = useRowBoundingRects();

  const isValidSubtreeRootRow = useIsValidSubtreeRootRow();
  const insertPlaceholderRow = useInsertPlaceholderRow();
  const removePlaceholderRow = useRemovePlaceholderRow();

  const prepareIssueCreation = usePrepareIssueCreation();

  return useCallback(
    (top) => {
      const lastRow = rowBoundingRects.at(-1) ?? { top: 0, height: 0 };
      const isBelowLastRow = lastRow.top + lastRow.height < top;

      const selectedRowIndex = isBelowLastRow
        ? Math.max(rows.length, 1) - 1
        : rowBoundingRects.findIndex((bounds) => bounds.top <= top && bounds.top + bounds.height >= top);

      const selectedRow = rows[selectedRowIndex];

      if (!selectedRow) {
        if (rows.length === 0 && mainProject) {
          prepareIssueCreation([getProjectResourceIdentifier(mainProject.id)]);
          insertPlaceholderRow([], { index: selectedRowIndex });
        }
        return;
      }

      if (selectedRow.id === PLACEHOLDER_ROW_ID) {
        return;
      }

      const isMovingUpward =
        rows.length > 1 &&
        ((isIssueRow(selectedRow) && rows[selectedRowIndex + 1]?.id === PLACEHOLDER_ROW_ID) ||
          !rows.some(({ id }) => id === PLACEHOLDER_ROW_ID));

      const targetRow = isMovingUpward ? rows[selectedRowIndex - 1] : selectedRow;

      if (!targetRow) {
        return;
      }

      const closestSubtreeRootRow = isSubtreeRootRow(targetRow)
        ? targetRow
        : [...rows].reverse().find((row) => {
            return targetRow.subtreeRootIds.includes(row.id);
          });

      if (!closestSubtreeRootRow || !isValidSubtreeRootRow(closestSubtreeRootRow)) {
        removePlaceholderRow();
        return;
      }

      const completedSubtreeRootIds = [...closestSubtreeRootRow.subtreeRootIds, closestSubtreeRootRow.id];

      const previousPeerRow = (() => {
        if (!isMovingUpward) {
          return selectedRow;
        }

        const previousRow = rows[selectedRowIndex - 1];

        if (previousRow?.id !== PLACEHOLDER_ROW_ID) {
          return previousRow;
        }

        return rows[selectedRowIndex - 2];
      })();

      prepareIssueCreation(completedSubtreeRootIds, {
        previousPeerIssueRowId: isIssueRow(previousPeerRow) ? previousPeerRow?.id : void 0,
      });

      insertPlaceholderRow(completedSubtreeRootIds, {
        [isMovingUpward ? 'before' : 'after']: selectedRow,
      });
    },
    [
      mainProject,
      rowBoundingRects,
      rows,
      isValidSubtreeRootRow,
      prepareIssueCreation,
      insertPlaceholderRow,
      removePlaceholderRow,
    ]
  );
};
