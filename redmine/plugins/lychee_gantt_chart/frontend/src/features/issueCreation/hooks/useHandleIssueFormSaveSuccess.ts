import { useCallback } from 'react';
import { isPlainObject } from '@/utils/object';
import { isNewIssueId } from '@/models/issue';
import { useAddIssue } from '@/models/coreResource';
import { IssueFormEventHandler } from '@/features/issueForm';
import { useRefreshIssue } from '@/features/resourceController';
import { IssueCreation, useReflectIssueCreation } from '../models/issueCreation';

type IssueCreationMeta = {
  issueCreation: IssueCreation;
};

const isIssueCreationMeta = (meta: unknown): meta is IssueCreationMeta => {
  return isPlainObject(meta) && 'issueCreation' in meta;
};

export const useHandleIssueFormSaveSuccess = (): IssueFormEventHandler => {
  const refreshIssue = useRefreshIssue();
  const reflectIssueCreation = useReflectIssueCreation();
  const addIssue = useAddIssue();

  return useCallback(
    async (event) => {
      if (!isNewIssueId(event.detail.originalParam.id)) {
        return;
      }

      const { issue } = await refreshIssue(event.detail.issueId);
      const meta = event.detail.originalParam._meta;

      if (isIssueCreationMeta(meta)) {
        reflectIssueCreation(issue, meta.issueCreation);
      } else {
        addIssue(issue);
      }
    },
    [refreshIssue, reflectIssueCreation, addIssue]
  );
};
