import { atom } from 'recoil';

export const isHotkeyPressedState = atom<boolean>({
  key: 'issueCreation/isHotkeyPressed',
  default: false,
});
