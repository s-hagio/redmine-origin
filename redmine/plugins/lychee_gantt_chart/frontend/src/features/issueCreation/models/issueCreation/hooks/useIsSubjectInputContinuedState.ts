import { useRecoilState } from 'recoil';
import { isSubjectInputContinuedState } from '../states';

export const useIsSubjectInputContinuedState = () => useRecoilState(isSubjectInputContinuedState);
