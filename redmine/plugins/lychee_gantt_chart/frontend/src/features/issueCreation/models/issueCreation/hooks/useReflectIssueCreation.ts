import { useRecoilCallback } from 'recoil';
import {
  coreResourceCollectionState,
  getIssueResourceIdentifier,
  getResourceIdFromIdentifier,
} from '@/models/coreResource';
import { Issue } from '@/models/issue';
import { rowsState, useAddIssueRow } from '@/features/row';
import { issueCreationState } from '../states';
import { IssueCreation } from '../types';

type ReflectIssueCreation = (issue: Issue, specifiedIssueCreation?: IssueCreation | null) => void;

export const useReflectIssueCreation = (): ReflectIssueCreation => {
  const addIssueRow = useAddIssueRow();

  return useRecoilCallback(
    ({ snapshot, transact_UNSTABLE }) => {
      return (issue, specifiedIssueCreation) => {
        const currentIssueCreation = snapshot.getLoadable(issueCreationState).getValue();
        const issueCreation = specifiedIssueCreation ?? currentIssueCreation;

        if (!issueCreation) {
          return;
        }

        const rows = snapshot.getLoadable(rowsState).getValue();
        const closestSubtreeRootId = issueCreation.subtreeRootIds.at(-1);
        const closestSubtreeRootRow = rows.find((row) => row.id === closestSubtreeRootId);

        if (!closestSubtreeRootRow) {
          throw new Error('Closest subtree root row is not found');
        }

        const indexToInsert = issueCreation.previousPeerIssueRowId
          ? rows.findIndex(({ id }) => id === issueCreation.previousPeerIssueRowId)
          : rows.indexOf(closestSubtreeRootRow);

        addIssueRow(issue, {
          subtreeRootIds: issueCreation.subtreeRootIds,
          index: indexToInsert + 1,
        });

        transact_UNSTABLE(({ set }) => {
          if (currentIssueCreation?.id === issueCreation.id) {
            set(issueCreationState, {
              ...issueCreation,
              previousPeerIssueRowId: getIssueResourceIdentifier(issue.id),
            });
          }

          set(coreResourceCollectionState, (collection) => {
            const newIssues = [...collection.issues];

            const previousPeerIssueId = issueCreation.previousPeerIssueRowId
              ? getResourceIdFromIdentifier(issueCreation.previousPeerIssueRowId)
              : null;

            const index = previousPeerIssueId
              ? newIssues.findIndex((issue) => issue.id === previousPeerIssueId) + 1
              : 0;

            newIssues.splice(index, 0, issue);

            return { ...collection, issues: newIssues };
          });
        });
      };
    },
    [addIssueRow]
  );
};
