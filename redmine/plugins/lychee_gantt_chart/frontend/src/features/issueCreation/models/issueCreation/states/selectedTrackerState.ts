import { selector } from 'recoil';
import { indexedAssociatedResourceCollectionState } from '@/models/associatedResource';
import { issueCreationState } from './issueCreationState';

export const selectedTrackerState = selector({
  key: 'issueCreation/selectedTracker',
  get: ({ get }) => {
    const issueCreation = get(issueCreationState);

    if (!issueCreation) {
      return null;
    }

    const { trackersById } = get(indexedAssociatedResourceCollectionState);

    return trackersById[issueCreation.params.trackerId];
  },
});
