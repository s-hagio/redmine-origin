import { useCallback } from 'react';
import { InsertRowParams, RowID, RowResourceType, useInsertRow, useRowBuildOptions } from '@/features/row';
import { PLACEHOLDER_ROW_ID } from '../constants';

type InsertPlaceholderRow = (subtreeRootIds: RowID[], params: InsertRowParams) => void;

export const useInsertPlaceholderRow = (): InsertPlaceholderRow => {
  const { rootDepth } = useRowBuildOptions();
  const insertRow = useInsertRow();

  return useCallback(
    (subtreeRootIds, params) => {
      insertRow(
        {
          id: PLACEHOLDER_ROW_ID,
          resourceType: RowResourceType.Placeholder,
          depth: rootDepth + subtreeRootIds.length,
          isCollapsible: false,
          subtreeRootIds: [...subtreeRootIds, PLACEHOLDER_ROW_ID],
        },
        params
      );
    },
    [rootDepth, insertRow]
  );
};
