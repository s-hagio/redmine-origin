import { useRecoilCallback } from 'recoil';
import { useTranslation } from 'react-i18next';
import {
  getResourceIdFromIdentifier,
  isProjectResourceIdentifier,
  isVersionResourceIdentifier,
  isIssueResourceIdentifier,
} from '@/models/coreResource';
import { ProjectID, useMainProject } from '@/models/project';
import { AssociatedTracker, useIndexedAssociatedCollection } from '@/models/associatedResource';
import { IssueRow, RowID } from '@/features/row';
import { IssueCreation } from '../types';
import { issueCreationState } from '../states';
import { isValidIssueCreationParams } from '../helpers';

type Options = {
  previousPeerIssueRowId?: IssueRow['id'];
};

type PrepareIssueCreation = (subtreeRootIds: RowID[], options?: Options) => void;

let idCounter = 0;

export const usePrepareIssueCreation = (): PrepareIssueCreation => {
  const { t } = useTranslation();
  const mainProject = useMainProject();

  const { projectsById, trackersById } = useIndexedAssociatedCollection();

  const getTracker = (projectId: ProjectID): AssociatedTracker => {
    const { trackerIds } = projectsById[projectId];

    return trackerIds.map((id) => trackersById[id])[0];
  };

  return useRecoilCallback(({ set }) => {
    return (subtreeRootIds, options = {}) => {
      const params = [...subtreeRootIds].reverse().reduce<Partial<IssueCreation['params']>>(
        (memo, rowId) => {
          if (isProjectResourceIdentifier(rowId)) {
            memo.projectId ??= getResourceIdFromIdentifier(rowId);
          } else if (isVersionResourceIdentifier(rowId)) {
            memo.fixedVersionId ??= getResourceIdFromIdentifier(rowId);
          } else if (isIssueResourceIdentifier(rowId)) {
            memo.parentId ??= getResourceIdFromIdentifier(rowId);
          }

          return memo;
        },
        {
          subject: `${t('label.untitled')}`,
        }
      );

      params.projectId ??= mainProject?.id;

      if (!params.projectId) {
        throw new Error('Project ID is not specified');
      }

      const tracker = getTracker(params.projectId);

      params.trackerId = tracker.id;
      params.statusId = tracker.defaultStatusId;

      if (!isValidIssueCreationParams(params)) {
        throw new Error('Invalid issue creation params');
      }

      set(issueCreationState, {
        id: ++idCounter,
        subtreeRootIds,
        params,
        ...options,
      });
    };
  });
};
