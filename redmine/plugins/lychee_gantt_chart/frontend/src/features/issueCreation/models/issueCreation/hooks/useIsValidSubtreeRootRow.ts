import { useRecoilValue } from 'recoil';
import { isValidSubtreeRootRowState } from '../functionalStates';

export const useIsValidSubtreeRootRow = () => useRecoilValue(isValidSubtreeRootRowState);
