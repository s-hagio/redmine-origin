import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { useDestroyIssueCreation } from '../useDestroyIssueCreation';
import { issueCreationState } from '../../states';
import { IssueCreation } from '../../types';

const removePlaceholderRowMock = vi.hoisted(() => vi.fn());

vi.mock('../../../placeholderRow', () => ({
  useRemovePlaceholderRow: vi.fn().mockReturnValue(removePlaceholderRowMock),
}));

const renderForTesting = () => {
  return renderRecoilHook(
    () => ({
      destroy: useDestroyIssueCreation(),
      issueCreation: useRecoilValue(issueCreationState),
    }),
    {
      initializeState: ({ set }) => {
        set(issueCreationState, { id: 123 } as IssueCreation);
      },
    }
  );
};

test('PlaceholderRowを削除', () => {
  const { result } = renderForTesting();

  act(() => result.current.destroy());

  expect(removePlaceholderRowMock).toBeCalled();
});

test('IssueCreationを削除', () => {
  const { result } = renderForTesting();

  expect(result.current.issueCreation).not.toBeNull();

  act(() => result.current.destroy());

  expect(result.current.issueCreation).toBeNull();
});
