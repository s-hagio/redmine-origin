import { useRecoilState } from 'recoil';
import { isHotkeyPressedState } from '../states';

export const useIsHotkeyPressedState = () => useRecoilState(isHotkeyPressedState);
