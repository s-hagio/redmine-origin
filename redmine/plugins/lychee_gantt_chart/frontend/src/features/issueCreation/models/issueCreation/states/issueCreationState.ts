import { atom } from 'recoil';
import { IssueCreation } from '../types';

export const issueCreationState = atom<IssueCreation | null>({
  key: 'issueCreation/issueCreation',
  default: null,
});
