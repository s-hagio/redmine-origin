import { selector } from 'recoil';
import { RowOptions } from '@/features/row';
import { isIssueCreationActiveState } from './isIssueCreationActiveState';
import { issueCreationState } from './issueCreationState';

export const partialRowOptionsState = selector<Partial<RowOptions> | null>({
  key: 'issueCreation/partialRowOptions',
  get: ({ get }) => {
    const isActive = get(isIssueCreationActiveState);
    const issueCreation = get(issueCreationState);

    return isActive
      ? {
          includeMainAndAncestorProjects: true,
          includeEmptyProjects: true,
          includeEmptyVersions: true,
          skipRefresh: !!issueCreation,
        }
      : null;
  },
});
