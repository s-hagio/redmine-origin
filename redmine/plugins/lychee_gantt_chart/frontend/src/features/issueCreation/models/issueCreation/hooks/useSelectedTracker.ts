import { useRecoilValue } from 'recoil';
import { selectedTrackerState } from '../states';

export const useSelectedTracker = () => useRecoilValue(selectedTrackerState);
