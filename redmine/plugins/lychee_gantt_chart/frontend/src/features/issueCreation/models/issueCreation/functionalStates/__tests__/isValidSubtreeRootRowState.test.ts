import { constSelector, MutableSnapshot, selector, useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { IsAllowedTo } from '@/models/currentUser';
import { versionState } from '@/models/version';
import { issueEntityState } from '@/models/issue';
import {
  associatedChildProject1,
  associatedChildProject2,
  associatedMainProject,
  associatedRootProject,
  childProject1Row,
  childProject1VersionRow,
  childProject2Row,
  grandchildIssue,
  grandchildIssueRow,
  indexedAssociatedResourceCollection,
  mainProject,
  mainProjectRow,
  parentIssue,
  parentIssueRow,
  rootProjectRow,
  rootProjectVersionRow,
  rowVersion,
} from '@/__fixtures__';
import { isValidSubtreeRootRowState } from '../isValidSubtreeRootRowState';

const indexedAssociatedResourceCollectionStateMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/associatedResource', async (importOriginal) =>
  Object.defineProperties(await importOriginal(), {
    indexedAssociatedResourceCollectionState: { get: indexedAssociatedResourceCollectionStateMock },
  })
);

indexedAssociatedResourceCollectionStateMock.mockReturnValue(
  constSelector({
    ...indexedAssociatedResourceCollection,
    projectsById: {
      [associatedRootProject.id]: associatedRootProject,
      [associatedMainProject.id]: associatedMainProject,
      [associatedChildProject1.id]: associatedChildProject1,
      [associatedChildProject2.id]: { ...associatedChildProject2, trackerIds: [] },
    },
  })
);

const isAllowedToStateMock = vi.hoisted(() => vi.fn());
const isAllowedToMock = vi.fn().mockReturnValue(false);

vi.mock('@/models/currentUser', async (importOriginal) =>
  Object.defineProperties(await importOriginal(), {
    isAllowedToState: { get: isAllowedToStateMock },
  })
);

isAllowedToStateMock.mockReturnValue(
  selector<IsAllowedTo>({
    key: 'isAllowedTo/mock',
    get: () => isAllowedToMock,
  })
);

const renderForTesting = (initializeState?: (mutableSnapshot: MutableSnapshot) => void) => {
  return renderRecoilHook(() => useRecoilValue(isValidSubtreeRootRowState), {
    initializeState: (mutableSnapshot) => {
      const { set } = mutableSnapshot;

      set(versionState(rowVersion.id), rowVersion);
      [parentIssue, grandchildIssue].forEach((issue) => set(issueEntityState(issue.id), issue));

      initializeState?.(mutableSnapshot);
    },
  });
};

describe('with main project', () => {
  const renderWithMainProject = () => {
    return renderForTesting(({ set }) => {
      set(settingsState, (current) => ({ ...current, mainProject }));
    });
  };

  describe('add_issues権限あり', () => {
    beforeEach(() => {
      isAllowedToMock.mockImplementation((permission) => permission === 'add_issues');
    });

    test('メインプロジェクト以下でトラッカーを持っているプロジェクト行は有効', () => {
      const { result } = renderWithMainProject();

      expect(result.current(rootProjectRow)).toBe(false);
      expect(result.current(mainProjectRow)).toBe(true);
      expect(result.current(childProject1Row)).toBe(true);
      expect(result.current(childProject2Row), 'トラッカーが無い').toBe(false);
    });

    test('有効プロジェクトのバージョン行は有効', () => {
      const { result } = renderWithMainProject();

      expect(result.current(rootProjectVersionRow)).toBe(false);
      expect(result.current(childProject1VersionRow)).toBe(true);
    });

    test('有効プロジェクトの親チケット行は有効', () => {
      const { result } = renderWithMainProject();

      expect(result.current(parentIssueRow)).toBe(true);
      expect(result.current(grandchildIssueRow)).toBe(false);
    });
  });

  describe('add_issues権限なし', () => {
    beforeEach(() => {
      isAllowedToMock.mockImplementation((permission) => permission !== 'add_issues');
    });

    test('権限がないプロジェクトとそれに属しているリソースの行では無効', () => {
      const { result } = renderWithMainProject();

      expect(result.current(rootProjectRow)).toBe(false);
      expect(result.current(mainProjectRow)).toBe(false);
      expect(result.current(childProject1Row)).toBe(false);
      expect(result.current(childProject2Row)).toBe(false);
      expect(result.current(rootProjectVersionRow)).toBe(false);
      expect(result.current(childProject1VersionRow)).toBe(false);
      expect(result.current(parentIssueRow)).toBe(false);
      expect(result.current(grandchildIssueRow)).toBe(false);
    });
  });
});

describe('without main project', () => {
  const renderWithoutMainProject = () => {
    return renderForTesting(({ set }) => {
      set(settingsState, (current) => ({ ...current, mainProject: undefined }));
    });
  };

  beforeEach(() => {
    isAllowedToMock.mockImplementation((permission, { projectId }) => {
      return (
        permission === 'add_issues' &&
        (projectId === rootProjectRow.resourceId || projectId === mainProjectRow.resourceId)
      );
    });
  });

  test('有効条件を満たす行は有効', () => {
    const { result } = renderWithoutMainProject();

    expect(result.current(rootProjectRow)).toBe(true);
    expect(result.current(mainProjectRow)).toBe(true);
    expect(result.current(rootProjectVersionRow)).toBe(true);
    expect(result.current(parentIssueRow)).toBe(true);
    expect(result.current(grandchildIssueRow)).toBe(false);
  });

  test('権限がないプロジェクトとそれに属しているリソースの行では無効', () => {
    const { result } = renderWithoutMainProject();

    expect(result.current(childProject1Row)).toBe(false);
    expect(result.current(childProject1VersionRow)).toBe(false);
  });

  test('トラッカーが無いプロジェクトの行は無効', () => {
    const { result } = renderWithoutMainProject();

    expect(result.current(childProject2Row)).toBe(false);
  });
});
