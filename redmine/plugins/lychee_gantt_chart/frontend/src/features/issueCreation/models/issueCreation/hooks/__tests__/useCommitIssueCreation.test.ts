import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { beforeEach } from 'vitest';
import { AxiosError } from 'axios';
import { renderRecoilHook } from '@/test-utils';
import { childIssue, mainProjectRow, rootIssueRow } from '@/__fixtures__';
import { isSubjectInputContinuedState, issueCreationState } from '../../states';
import { IssueCreation, IssueCreationParams } from '../../types';
import { useCommitIssueCreation } from '../useCommitIssueCreation';

const saveIssueMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/resourceController', () => ({
  useSaveIssue: vi.fn().mockReturnValue(saveIssueMock),
}));

const removePlaceholderRowMock = vi.hoisted(() => vi.fn());

vi.mock('../../../placeholderRow', () => ({
  useRemovePlaceholderRow: vi.fn().mockReturnValue(removePlaceholderRowMock),
}));

const reflectIssueCreationMock = vi.hoisted(() => vi.fn());

vi.mock('../useReflectIssueCreation', () => ({
  useReflectIssueCreation: vi.fn().mockReturnValue(reflectIssueCreationMock),
}));

beforeEach(() => {
  saveIssueMock.mockResolvedValue({ issue: childIssue });
});

const issueCreationParams: IssueCreationParams = {
  projectId: 1,
  trackerId: 2,
  statusId: 3,
  fixedVersionId: null,
  parentId: null,
  subject: 'Untitled',
};

const issueCreation: IssueCreation = {
  id: 111,
  params: issueCreationParams,
  subtreeRootIds: [mainProjectRow.id, rootIssueRow.id],
  previousPeerIssueRowId: rootIssueRow.id,
};

const renderForTesting = () => {
  return renderRecoilHook(
    () => ({
      commit: useCommitIssueCreation(),
      issueCreation: useRecoilValue(issueCreationState),
      inputContinued: useRecoilValue(isSubjectInputContinuedState),
    }),
    {
      initializeState: ({ set }) => {
        set(issueCreationState, issueCreation);
      },
    }
  );
};

test.todo('IssueCreationをコミットする', async () => {
  const { result } = renderForTesting();
  const params = { subject: 'foo!' };

  expect(saveIssueMock).not.toBeCalled();
  expect(reflectIssueCreationMock).not.toBeCalled();

  await act(() => result.current.commit(params));

  expect(saveIssueMock).toBeCalledWith({ ...issueCreationParams, ...params }, { meta: issueCreation });
  expect(reflectIssueCreationMock).toBeCalledWith(childIssue);
});

describe('エラー発生時', () => {
  beforeEach(() => {
    saveIssueMock.mockRejectedValue(new AxiosError('error'));
  });

  test('IssueFormでの入力準備を整える', async () => {
    const { result } = renderForTesting();

    await act(async () => {
      try {
        await result.current.commit({});
      } catch (e) {
        // noop
      }
    });

    expect(result.current.issueCreation?.missingError).toBe(true);
    expect(result.current.inputContinued).toBe(false);
    expect(removePlaceholderRowMock).toBeCalled();
  });
});
