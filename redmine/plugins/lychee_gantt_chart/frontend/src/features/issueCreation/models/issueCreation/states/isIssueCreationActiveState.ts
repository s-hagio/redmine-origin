import { selector } from 'recoil';
import { issueQueryState } from '@/models/query';
import { isHotkeyPressedState } from './isHotkeyPressedState';
import { isSubjectInputContinuedState } from './isSubjectInputContinuedState';

export const isIssueCreationActiveState = selector<boolean>({
  key: 'issueCreation/isIssueCreationActive',
  get: ({ get }) => {
    const { projectId, groupBy } = get(issueQueryState);
    const isHotkeyPressed = get(isHotkeyPressedState);
    const isSubjectInputContinued = get(isSubjectInputContinuedState);

    return (!!projectId || !groupBy) && (isHotkeyPressed || isSubjectInputContinued);
  },
});
