import { useCallback } from 'react';
import { useRecoilValue } from 'recoil';
import { useOverwriteRowOptions } from '@/features/row';
import { partialRowOptionsState } from '../states/partialRowOptionsState';
import { ROW_OPTIONS_ID } from '../constants';

export const useRefreshIssueCreationRowOptions = () => {
  const rowOptions = useRecoilValue(partialRowOptionsState);
  const overwriteRowOptions = useOverwriteRowOptions(ROW_OPTIONS_ID);

  return useCallback(() => {
    overwriteRowOptions(rowOptions);
  }, [overwriteRowOptions, rowOptions]);
};
