import { atom } from 'recoil';

export const isSubjectInputContinuedState = atom<boolean>({
  key: 'issueCreation/isSubjectInputContinued',
  default: false,
});
