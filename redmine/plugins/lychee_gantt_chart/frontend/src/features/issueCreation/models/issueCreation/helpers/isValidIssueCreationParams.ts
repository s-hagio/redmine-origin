import { hasKey, isPlainObject } from '@/utils/object';
import { IssueCreationParams } from '../types';

export const isValidIssueCreationParams = (arg: unknown): arg is IssueCreationParams => {
  return (
    isPlainObject(arg) &&
    hasKey(arg, 'projectId') &&
    hasKey(arg, 'trackerId') &&
    hasKey(arg, 'statusId') &&
    hasKey(arg, 'subject')
  );
};
