import { useRecoilCallback } from 'recoil';
import { IssueParams } from '@/models/issue';
import { useSaveIssue } from '@/features/resourceController';
import { useRemovePlaceholderRow } from '../../placeholderRow';
import { issueCreationState } from '../states';
import { useIsSubjectInputContinuedState } from './useIsSubjectInputContinuedState';
import { useReflectIssueCreation } from './useReflectIssueCreation';

type CommitIssueCreation = (params: IssueParams) => Promise<void>;

export const useCommitIssueCreation = (): CommitIssueCreation => {
  const saveIssue = useSaveIssue();

  const removePlaceholderRow = useRemovePlaceholderRow();
  const [, setIsInputContinued] = useIsSubjectInputContinuedState();
  const reflectIssueCreation = useReflectIssueCreation();

  return useRecoilCallback(
    ({ set, snapshot }) => {
      return async (params) => {
        const issueCreation = snapshot.getLoadable(issueCreationState).getValue();

        if (!issueCreation) {
          throw new Error('Issue creation is not initialized');
        }

        const newIssueParams = { ...issueCreation.params, ...params };

        try {
          const { issue } = await saveIssue(newIssueParams, {
            meta: { issueCreation },
            shouldOpenFormOnError: true,
          });

          reflectIssueCreation(issue);
        } catch (error) {
          set(issueCreationState, (current) => {
            if (current?.id === issueCreation.id) {
              return { ...current, missingError: true };
            }
            return current;
          });
          setIsInputContinued(false);
          removePlaceholderRow();

          throw error;
        }
      };
    },
    [saveIssue, reflectIssueCreation, setIsInputContinued, removePlaceholderRow]
  );
};
