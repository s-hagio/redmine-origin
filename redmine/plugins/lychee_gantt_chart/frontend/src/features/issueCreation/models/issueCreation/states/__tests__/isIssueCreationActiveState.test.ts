import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { issueQueryState } from '@/models/query';
import { ProjectID } from '@/models/project';
import { isHotkeyPressedState } from '../isHotkeyPressedState';
import { isSubjectInputContinuedState } from '../isSubjectInputContinuedState';
import { isIssueCreationActiveState } from '../isIssueCreationActiveState';

const renderForTesting = (projectId?: ProjectID) => {
  const hookResult = renderRecoilHook(
    () => ({
      isActive: useRecoilValue(isIssueCreationActiveState),
      setIsHotkeyPressed: useSetRecoilState(isHotkeyPressedState),
      setIsSubjectInputContinued: useSetRecoilState(isSubjectInputContinuedState),
      setIssueQuery: useSetRecoilState(issueQueryState),
    }),
    {
      initializeState: ({ set }) => {
        set(isHotkeyPressedState, false);
        set(isSubjectInputContinuedState, false);
      },
    }
  );

  act(() => {
    hookResult.result.current.setIssueQuery((current) => ({ ...current, projectId }));
  });

  return hookResult;
};

describe('プロジェクト個別画面の場合', () => {
  const renderForIndividualProject = () => renderForTesting(1);

  test('ホットキーが押されていればtrueを返す', () => {
    const { result } = renderForIndividualProject();

    expect(result.current.isActive).toBe(false);
    act(() => result.current.setIsHotkeyPressed(true));
    expect(result.current.isActive).toBe(true);
  });

  test('題名入力が続いていればtrueを返す', () => {
    const { result } = renderForIndividualProject();

    expect(result.current.isActive).toBe(false);
    act(() => result.current.setIsSubjectInputContinued(true));
    expect(result.current.isActive).toBe(true);
  });

  test('グループ条件が指定されている場合も通常通り判定', () => {
    const { result } = renderForIndividualProject();

    act(() => result.current.setIssueQuery((current) => ({ ...current, groupBy: 'fixed_version' })));

    expect(result.current.isActive).toBe(false);
    act(() => result.current.setIsHotkeyPressed(true));
    expect(result.current.isActive).toBe(true);
  });
});

describe('全プロジェクト横断画面の場合', () => {
  const renderForAllProjects = () => renderForTesting(undefined);

  test('グループ条件が未指定なら通常の条件で判定', () => {
    const { result } = renderForAllProjects();

    act(() => result.current.setIssueQuery((current) => ({ ...current, groupBy: null })));

    expect(result.current.isActive).toBe(false);
    act(() => result.current.setIsHotkeyPressed(true));
    expect(result.current.isActive).toBe(true);
  });

  test('グループ条件が指定されている場合は常にfalse', () => {
    const { result } = renderForAllProjects();

    act(() => result.current.setIssueQuery((current) => ({ ...current, groupBy: 'fixed_version' })));

    expect(result.current.isActive).toBe(false);
    act(() => result.current.setIsHotkeyPressed(true));
    expect(result.current.isActive).toBe(false);
  });
});
