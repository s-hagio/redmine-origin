import { useRecoilValue } from 'recoil';
import { issueCreationState } from '../states';

export const useIssueCreationOrThrow = () => {
  const issueCreation = useRecoilValue(issueCreationState);

  if (!issueCreation) {
    throw new Error('Issue creation is not initialized');
  }

  return issueCreation;
};
