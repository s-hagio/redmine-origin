import { IssueCore } from '@/models/issue';
import { IssueRow, RowID } from '@/features/row';

export type IssueCreationParams = Pick<
  IssueCore,
  'projectId' | 'fixedVersionId' | 'trackerId' | 'statusId' | 'parentId' | 'subject'
>;

export type IssueCreation = {
  id: number;
  params: IssueCreationParams;
  subtreeRootIds: RowID[];
  previousPeerIssueRowId?: IssueRow['id'];
  missingError?: boolean;
};
