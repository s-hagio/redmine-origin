import { useCallback } from 'react';
import { useAvailableRows, useRemoveRow } from '@/features/row';
import { PLACEHOLDER_ROW_ID } from '../constants';

export const useRemovePlaceholderRow = () => {
  const rows = useAvailableRows();
  const removeRow = useRemoveRow();

  return useCallback(() => {
    const row = rows.find((row) => row.id === PLACEHOLDER_ROW_ID);

    row && removeRow(row);
  }, [rows, removeRow]);
};
