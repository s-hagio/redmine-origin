import { useRecoilValue } from 'recoil';
import { isIssueCreationActiveState } from '../states';

export const useIsIssueCreationActive = () => useRecoilValue(isIssueCreationActiveState);
