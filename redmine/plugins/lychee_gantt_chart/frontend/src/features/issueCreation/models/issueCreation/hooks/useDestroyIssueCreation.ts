import { useRecoilCallback } from 'recoil';
import { useRemovePlaceholderRow } from '../../placeholderRow';
import { issueCreationState } from '../states';

type DestroyIssueCreation = () => void;

export const useDestroyIssueCreation = (): DestroyIssueCreation => {
  const removePlaceholderRow = useRemovePlaceholderRow();

  return useRecoilCallback(
    ({ set }) => {
      return () => {
        removePlaceholderRow();
        set(issueCreationState, null);
      };
    },
    [removePlaceholderRow]
  );
};
