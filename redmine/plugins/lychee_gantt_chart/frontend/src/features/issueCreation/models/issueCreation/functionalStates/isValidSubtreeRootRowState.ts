import { selector } from 'recoil';
import { isDescendantOf } from '@/lib/nestedSet';
import { isAllowedToState } from '@/models/currentUser';
import { settingsState } from '@/models/setting';
import { indexedAssociatedResourceCollectionState } from '@/models/associatedResource';
import { ProjectID } from '@/models/project';
import { issueState } from '@/models/issue';
import { versionState, VersionStatus } from '@/models/version';
import { ProjectRow, VersionRow, IssueRow, Row, RowResourceType, isRowCollapsedState } from '@/features/row';

export const isValidSubtreeRootRowState = selector({
  key: 'issueCreation/isValidSubtreeRootRow',
  get: ({ get, getCallback }) => {
    const { projectsById } = get(indexedAssociatedResourceCollectionState);

    const settings = get(settingsState);
    const mainProject = settings.mainProject ? projectsById[settings.mainProject.id] : null;

    const isAllowedTo = get(isAllowedToState);

    return getCallback(({ snapshot }) => {
      const isValidProjectId = (projectId: ProjectID) => {
        const project = projectsById[projectId];

        if (mainProject && isDescendantOf(mainProject, project)) {
          return false;
        }

        return project.trackerIds.length > 0 && isAllowedTo('add_issues', { projectId });
      };

      const isValidProjectRow = (projectRow: ProjectRow): boolean => {
        return isValidProjectId(projectRow.resourceId);
      };

      const isValidVersionRow = (versionRow: VersionRow): boolean => {
        const [, projectId, versionId] = versionRow.id.split('-');

        if (!isValidProjectId(+projectId)) {
          return false;
        }

        const version = snapshot.getLoadable(versionState(+versionId)).getValue();

        return version?.status === VersionStatus.Open;
      };

      const isValidIssueRow = (issueRow: IssueRow): boolean => {
        const issue = snapshot.getLoadable(issueState(issueRow.resourceId)).getValue();

        return (
          issueRow.isCollapsible &&
          !!issue &&
          isValidProjectId(issue.projectId) &&
          !snapshot.getLoadable(isRowCollapsedState(issueRow.id)).getValue()
        );
      };

      return (row: Row) => {
        switch (row.resourceType) {
          case RowResourceType.Project:
            return isValidProjectRow(row);
          case RowResourceType.Version:
            return isValidVersionRow(row);
          case RowResourceType.Issue:
            return isValidIssueRow(row);
          case RowResourceType.Group:
            return !!mainProject && isValidProjectId(mainProject.id);
          default:
            break;
        }
      };
    });
  },
});
