import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { expect } from 'vitest';
import { renderRecoilHook } from '@/test-utils';
import { CoreResourceCollection, coreResourceCollectionState } from '@/models/coreResource';
import { rowsState } from '@/features/row';
import {
  childIssue,
  grandchildIssue,
  grandchildIssue2,
  otherTreeIssue,
  otherTreeIssueRow,
  parentIssue,
  rootIssue,
  treedGrandchildIssueRow,
  treedMainProjectRow,
  treedRows,
} from '@/__fixtures__';
import { IssueCreation } from '../../types';
import { issueCreationState } from '../../states';
import { useReflectIssueCreation } from '../useReflectIssueCreation';

const addIssueRowMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useAddIssueRow: vi.fn().mockReturnValue(addIssueRowMock),
}));

const issues = [rootIssue, parentIssue, childIssue, grandchildIssue, grandchildIssue2];

const baseIssueCreation: IssueCreation = {
  id: 1,
  params: { projectId: 1, trackerId: 2, statusId: 3, fixedVersionId: null, parentId: null, subject: 'test' },
  subtreeRootIds: [],
};

test('作成したチケットをIssueCreationの状態に応じて反映する', () => {
  const issueCreation = {
    ...baseIssueCreation,
    subtreeRootIds: [...treedMainProjectRow.subtreeRootIds, treedMainProjectRow.id],
  };

  const { result } = renderRecoilHook(
    () => ({
      reflect: useReflectIssueCreation(),
      coreResourceCollection: useRecoilValue(coreResourceCollectionState),
      issueCreation: useRecoilValue(issueCreationState),
    }),
    {
      initializeState: ({ set }) => {
        set(rowsState, treedRows);
        set(coreResourceCollectionState, { issues } as CoreResourceCollection);
        set(issueCreationState, issueCreation);
      },
    }
  );

  act(() => result.current.reflect(otherTreeIssue));

  expect(addIssueRowMock).toBeCalledWith(otherTreeIssue, {
    subtreeRootIds: issueCreation.subtreeRootIds,
    index: treedRows.indexOf(treedMainProjectRow) + 1,
  });

  expect(result.current.coreResourceCollection.issues).toEqual([otherTreeIssue, ...issues]);
  expect(result.current.issueCreation).toEqual({
    ...issueCreation,
    previousPeerIssueRowId: otherTreeIssueRow.id,
  });
});

test('previousPeerIssueRowIdが指定されている場合は対象行の直後に挿入する', () => {
  const issueCreation = {
    ...baseIssueCreation,
    subtreeRootIds: treedGrandchildIssueRow.subtreeRootIds,
    previousPeerIssueRowId: treedGrandchildIssueRow.id,
  };

  const { result } = renderRecoilHook(
    () => ({
      reflect: useReflectIssueCreation(),
      coreResourceCollection: useRecoilValue(coreResourceCollectionState),
      issueCreation: useRecoilValue(issueCreationState),
    }),
    {
      initializeState: ({ set }) => {
        set(rowsState, treedRows);
        set(coreResourceCollectionState, { issues } as CoreResourceCollection);
        set(issueCreationState, issueCreation);
      },
    }
  );

  act(() => result.current.reflect(otherTreeIssue));

  expect(addIssueRowMock).toBeCalledWith(otherTreeIssue, {
    subtreeRootIds: issueCreation.subtreeRootIds,
    index: treedRows.indexOf(treedGrandchildIssueRow) + 1,
  });

  expect(result.current.coreResourceCollection.issues).toEqual([
    rootIssue,
    parentIssue,
    childIssue,
    grandchildIssue,
    otherTreeIssue,
    grandchildIssue2,
  ]);

  expect(result.current.issueCreation).toEqual({
    ...issueCreation,
    previousPeerIssueRowId: otherTreeIssueRow.id,
  });
});

describe('issueCreationを指定した場合', () => {
  const currentIssueCreation = { ...baseIssueCreation, id: 101 };
  const specifiedIssueCreation = {
    ...baseIssueCreation,
    id: 102,
    subtreeRootIds: [...treedMainProjectRow.subtreeRootIds, treedMainProjectRow.id],
  };

  test('指定したissueCreationを使って反映を実行する', () => {
    const { result } = renderRecoilHook(
      () => ({
        reflect: useReflectIssueCreation(),
        issueCreation: useRecoilValue(issueCreationState),
      }),
      {
        initializeState: ({ set }) => {
          set(rowsState, treedRows);
          set(issueCreationState, currentIssueCreation);
        },
      }
    );

    act(() => result.current.reflect(otherTreeIssue, specifiedIssueCreation));

    expect(addIssueRowMock).toBeCalledWith(otherTreeIssue, {
      subtreeRootIds: specifiedIssueCreation.subtreeRootIds,
      index: treedRows.indexOf(treedMainProjectRow) + 1,
    });

    expect(result.current.issueCreation, '現在のissueCreationを更新しない').toEqual(currentIssueCreation);
  });
});
