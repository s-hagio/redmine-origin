import { CustomFieldDefinition } from '@/models/fieldDefinition';
import { FieldFormat } from '@/features/field';
import {
  attachmentCustomFieldDefinition,
  boolCustomFieldDefinition,
  dateCustomFieldDefinition,
  descriptionFieldDefinition,
  enumerationCustomFieldDefinition,
  floatCustomFieldDefinition,
  formattedStringCustomFieldDefinition,
  formattedTextCustomFieldDefinition,
  idFieldDefinition,
  intCustomFieldDefinition,
  issueSubjectFieldDefinition,
  linkCustomFieldDefinition,
  listCustomFieldDefinition,
  parentSubjectFieldDefinition,
  startDateFieldDefinition,
  stringCustomFieldDefinition,
  textCustomFieldDefinition,
  trackerFieldDefinition,
  userCustomFieldDefinition,
  versionCustomFieldDefinition,
} from '@/__fixtures__';
import { DataColumn, SubjectDataColumn } from '../models/column';

export const idColumn: DataColumn = {
  name: idFieldDefinition.name,
  field: idFieldDefinition,
};

export const trackerColumn: DataColumn = {
  name: trackerFieldDefinition.name,
  field: trackerFieldDefinition,
};

export const issueSubjectColumn: DataColumn = {
  name: issueSubjectFieldDefinition.name,
  field: issueSubjectFieldDefinition,
};

export const parentSubjectColumn: DataColumn = {
  name: parentSubjectFieldDefinition.name,
  field: parentSubjectFieldDefinition,
};

export const startDateColumn: DataColumn = {
  name: startDateFieldDefinition.name,
  field: startDateFieldDefinition,
};

export const descriptionColumn: DataColumn = {
  name: descriptionFieldDefinition.name,
  field: descriptionFieldDefinition,
  format: FieldFormat.PlainText,
};

export const subjectColumn: SubjectDataColumn = {
  ...issueSubjectColumn,
  partColumns: [trackerColumn, idColumn],
};

const buildCustomFieldColumn = (customFieldDefinition: CustomFieldDefinition): DataColumn => ({
  name: customFieldDefinition.name,
  field: customFieldDefinition,
});

export const enumerationCustomFieldColumn = buildCustomFieldColumn(enumerationCustomFieldDefinition);
export const stringCustomFieldColumn = buildCustomFieldColumn(stringCustomFieldDefinition);
export const versionCustomFieldColumn = buildCustomFieldColumn(versionCustomFieldDefinition);
export const attachmentCustomFieldColumn = buildCustomFieldColumn(attachmentCustomFieldDefinition);
export const userCustomFieldColumn = buildCustomFieldColumn(userCustomFieldDefinition);
export const listCustomFieldColumn = buildCustomFieldColumn(listCustomFieldDefinition);
export const linkCustomFieldColumn = buildCustomFieldColumn(linkCustomFieldDefinition);
export const floatCustomFieldColumn = buildCustomFieldColumn(floatCustomFieldDefinition);
export const intCustomFieldColumn = buildCustomFieldColumn(intCustomFieldDefinition);
export const dateCustomFieldColumn = buildCustomFieldColumn(dateCustomFieldDefinition);
export const boolCustomFieldColumn = buildCustomFieldColumn(boolCustomFieldDefinition);
export const textCustomFieldColumn = buildCustomFieldColumn(textCustomFieldDefinition);
export const formattableStringCustomFieldColumn = buildCustomFieldColumn(
  formattedStringCustomFieldDefinition
);
export const formattableTextCustomFieldColumn = buildCustomFieldColumn(formattedTextCustomFieldDefinition);
