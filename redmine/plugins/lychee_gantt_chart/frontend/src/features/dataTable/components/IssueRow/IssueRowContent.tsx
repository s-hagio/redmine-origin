import * as React from 'react';
import { Issue } from '@/models/issue';
import { IssueRow } from '@/features/row';
import { RowProps } from '../types';
import IssueSubjectColumn from './IssueSubjectColumn';
import IssueColumn from './IssueColumn';

type Props = RowProps<IssueRow> & {
  issue: Issue;
};

const IssueRowContent: React.FC<Props> = ({ id, issue, isCollapsible, depth, columns }) => {
  return (
    <>
      <IssueSubjectColumn rowId={id} issue={issue} isCollapsible={isCollapsible} depth={depth} />

      {columns.map((column) => (
        <IssueColumn key={column.name} rowId={id} issue={issue} column={column} />
      ))}
    </>
  );
};

export default IssueRowContent;
