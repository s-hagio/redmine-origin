import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { SortableContext } from '@dnd-kit/sortable';
import { DndContext, DragOverEvent } from '@/lib/dnd';
import Item from '../Item';
import { descriptionColumn, issueSubjectColumn, startDateColumn } from '../../../../__fixtures__';
import { useToggleSelectedColumn } from '../../../../models/column';

vi.mock('../../../../models/column', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useToggleSelectedColumn: vi.fn(),
  useMoveSelectedColumn: vi.fn(),
}));

test('Render', () => {
  const { container } = render(
    <>
      <Item column={issueSubjectColumn} />
      <Item column={descriptionColumn} />
    </>
  );

  expect(container).toMatchSnapshot();
});

test('Buttonクリックで表示項目の選択を解除する', async () => {
  const toggleSelectedColumnMock = vi.fn();
  vi.mocked(useToggleSelectedColumn).mockReturnValue(toggleSelectedColumnMock);

  const { getByRole } = render(<Item column={descriptionColumn} />);
  const button = getByRole('button');

  expect(toggleSelectedColumnMock).not.toBeCalled();

  fireEvent.click(button);

  expect(toggleSelectedColumnMock).toBeCalledWith(descriptionColumn.name);
});

describe('with Sortable', () => {
  const columns = [descriptionColumn, startDateColumn] as const;
  const onDragOver = vi.fn();

  const renderWithSortable = () => {
    return render(
      <DndContext onDragOver={onDragOver}>
        <SortableContext items={columns.map(({ name }) => name)}>
          {columns.map((column) => (
            <Item key={column.name} column={column} />
          ))}
        </SortableContext>
      </DndContext>
    );
  };

  test('ドラッグで並び替え可能', () => {
    const { getByText } = renderWithSortable();
    const columnElement = getByText(startDateColumn.field.label);

    expect(onDragOver).not.toBeCalled();

    fireEvent.pointerDown(columnElement, { clientX: 0, clientY: 0, isPrimary: true });
    fireEvent.pointerMove(columnElement, { clientX: 0, clientY: 10 });

    expect(onDragOver).toBeCalled();

    const event: DragOverEvent = onDragOver.mock.calls[0][0];

    expect(event.active.id).toBe(startDateColumn.name);
    // ClientRectが全て同じ値になるため常に最初のItem（Column）がoverにセットされる
    expect(event.over.id).toBe(descriptionColumn.name);
  });
});
