import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { FieldProps } from './types';
import ListFieldInput, { OptionItem } from './ListFieldInput';

type Props = FieldProps & {
  asNumericString?: boolean;
};

const BoolFieldInput: React.FC<Props> = ({
  value,
  isRequired,
  asNumericString,
  onChange,
  onCommit,
  ...props
}) => {
  const { t } = useTranslation();

  const optionItems: OptionItem[] = React.useMemo(() => {
    const items: OptionItem[] = [
      { id: 'yes', value: true, label: t('label.yes') },
      { id: 'no', value: false, label: t('label.no') },
    ];

    if (asNumericString) {
      items[0].value = '1';
      items[1].value = '0';
    }

    return items;
  }, [asNumericString, t]);

  return (
    <ListFieldInput
      {...props}
      options={optionItems}
      value={value}
      isRequired={isRequired}
      onChange={onChange}
      onCommit={onCommit}
    />
  );
};

export default BoolFieldInput;
