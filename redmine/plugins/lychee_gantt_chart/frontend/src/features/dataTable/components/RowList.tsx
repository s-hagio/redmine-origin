import * as React from 'react';
import { Row, RowResourceType, useVisibleRows } from '@/features/row';
import { useOptionalColumns } from '../models/column';
import { ProjectRow } from './ProjectRow';
import { VersionRow } from './VersionRow';
import { IssueRow } from './IssueRow';
import { GroupRow } from './GroupRow';
import { PlaceholderRow } from './PlaceholderRow';

const RowList: React.FC = () => {
  const rows = useVisibleRows();
  const columns = useOptionalColumns();

  return (
    <>
      {rows.map((row: Row) => {
        switch (row.resourceType) {
          case RowResourceType.Project:
            return <ProjectRow key={row.id} {...row} columns={columns} />;
          case RowResourceType.Version:
            return <VersionRow key={row.id} {...row} columns={columns} />;
          case RowResourceType.Issue:
            return <IssueRow key={row.id} {...row} columns={columns} />;
          case RowResourceType.Group:
            return <GroupRow key={row.id} {...row} columns={columns} />;
          case RowResourceType.Placeholder:
            return <PlaceholderRow key={row.id} {...row} columns={columns} />;
        }
      })}
    </>
  );
};

export default React.memo(RowList);
