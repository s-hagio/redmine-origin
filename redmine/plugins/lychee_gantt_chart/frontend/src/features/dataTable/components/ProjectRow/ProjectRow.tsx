import * as React from 'react';
import { useProject } from '@/models/project';
import Row from '../Row';
import ProjectRowContent from './ProjectRowContent';

type Props = Omit<React.ComponentProps<typeof ProjectRowContent>, 'project'>;

const ProjectRow: React.FC<Props> = ({ id, resourceId, ...props }) => {
  const project = useProject(resourceId);

  if (!project) {
    return null;
  }

  return (
    <Row id={id}>
      <ProjectRowContent id={id} resourceId={resourceId} project={project} {...props} />
    </Row>
  );
};

export default React.memo(ProjectRow);
