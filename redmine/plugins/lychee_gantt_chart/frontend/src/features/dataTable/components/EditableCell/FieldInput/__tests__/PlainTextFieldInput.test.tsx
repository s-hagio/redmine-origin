import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { descriptionFieldDefinition } from '@/__fixtures__';
import PlainTextFieldInput from '../PlainTextFieldInput';
import { basicProps } from '../__fixtures__';

const props = {
  ...basicProps,
  field: descriptionFieldDefinition,
  value: 'TEXT!TEXT\nTEXTXXXXX\nTEXT!!!!!!',
};

test('textareaを表示する', () => {
  const { container } = render(<PlainTextFieldInput {...props} />);

  expect(container).toMatchSnapshot();
});

test('高さを入力内容に合わせて自動調節', () => {
  const { container } = render(<PlainTextFieldInput {...props} />);
  const el = container.querySelector('textarea') as HTMLTextAreaElement;
  Object.defineProperty(el, 'scrollHeight', { value: 123 });

  expect(el.style.height).toBe('');
  fireEvent.change(el, { target: { value: 'TEXT!!!' } });
  expect(el.style.height).toBe('123px');
});

test('Shift+Enterで入力内容を確定する', () => {
  vi.spyOn(props, 'onCommit');

  const { container } = render(<PlainTextFieldInput {...props} />);
  const el = container.querySelector('textarea') as HTMLTextAreaElement;

  const value = 'NEW TEXT!!!!!!!';
  fireEvent.change(el, { target: { value } });

  expect(props.onCommit).not.toBeCalled();
  fireEvent.keyDown(document, { shiftKey: true, key: 'Enter' });
  expect(props.onCommit).toBeCalledWith(value);
});
