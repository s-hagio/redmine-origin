import * as React from 'react';
import { css, cx } from '@linaria/core';

type Props = JSX.IntrinsicElements['span'];

const LabelText: React.FC<Props> = ({ className, children, ...props }) => {
  const classNames = [style, className];

  return (
    <span className={cx(...classNames)} {...props}>
      {children}
    </span>
  );
};

export default LabelText;

export const style = css`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;
