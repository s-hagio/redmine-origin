import * as React from 'react';
import { css, cx } from '@linaria/core';
import { FieldValue } from '@/features/field';
import { FieldProps } from './types';

type Props = FieldProps &
  Pick<React.ComponentProps<'input'>, 'className' | 'type'> & {
    onChangeRaw?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  };

const normalizeValue = (value?: FieldValue): string => (value ? value + '' : '');
const PADDING = 9;

const CellBasicInput = React.forwardRef<HTMLInputElement, Props>(function CellBasicInput(
  { className, type, value, isRequired, onChange, onChangeRaw, onCommit },
  ref
) {
  const elementRef = React.useRef<HTMLInputElement>(null);
  React.useImperativeHandle<HTMLInputElement | null, HTMLInputElement | null>(ref, () => elementRef.current);

  const widthComputerRef = React.useRef<HTMLSpanElement>(null);

  const handleChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      onChangeRaw && onChangeRaw(event);
      onChange && onChange(event.currentTarget.value);
    },
    [onChange, onChangeRaw]
  );

  const handleSubmit = React.useCallback(
    (event: React.FormEvent) => {
      event.preventDefault();
      event.stopPropagation();

      if (elementRef.current && onCommit) {
        onCommit(elementRef.current.value);
      }
    },
    [onCommit]
  );

  React.useEffect(() => {
    const input = elementRef.current;
    const widthComputer = widthComputerRef.current;

    if (!input || !widthComputer) {
      return;
    }

    const adjustWidth = () => {
      widthComputer.textContent = input.value;
      input.style.width = widthComputer.offsetWidth + PADDING * 2 + 'px';
    };

    adjustWidth();
    input.addEventListener('input', adjustWidth);

    return () => {
      input.removeEventListener('input', adjustWidth);
    };
  });

  return (
    <form onSubmit={handleSubmit}>
      <input
        ref={elementRef}
        className={cx(style, className)}
        type={type ?? 'text'}
        defaultValue={normalizeValue(value)}
        onChange={handleChange}
        required={isRequired}
        autoFocus
      />
      <span ref={widthComputerRef} className={widthComputerStyle} />
    </form>
  );
});

export default CellBasicInput;

const style = css`
  min-width: 100%;
  height: 100%;
  margin: 0;
  padding: 0 ${PADDING}px;
  background: #fff;
  font: inherit;

  &,
  &:focus {
    border: none !important;
    outline: none !important;
  }
`;

const widthComputerStyle = css`
  position: absolute;
  top: -9999px;
  left: -9999px;
  visibility: hidden;
`;
