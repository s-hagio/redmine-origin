import * as React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithTranslation } from '@/test-utils';
import { IssueQuery, useDefaultIssueQuery } from '@/models/query';
import { useSelectedColumnNamesState } from '../../../../models/column';
import DefaultColumnsSelectButton from '../DefaultColumnsSelectButton';
import { idColumn, startDateColumn } from '../../../../__fixtures__';

vi.mock('@/models/query', async () => ({
  ...(await vi.importActual<object>('@/models/query')),
  useDefaultIssueQuery: vi.fn().mockReturnValue({}),
}));

vi.mock('../../../../models/column', () => ({
  useSelectedColumnNamesState: vi.fn().mockReturnValue([[], vi.fn()]),
}));

test('Render', () => {
  const { container } = renderWithTranslation(<DefaultColumnsSelectButton />);

  expect(container).toMatchSnapshot();
});

test('クリックで表示項目をデフォルト表示項目に置き換える', () => {
  const defaultSelectedColumnNames = [idColumn.name, startDateColumn.name];
  vi.mocked(useDefaultIssueQuery).mockReturnValue({ c: defaultSelectedColumnNames } as IssueQuery);

  const setSelectedColumnNamesMock = vi.fn();
  vi.mocked(useSelectedColumnNamesState).mockReturnValue([[], setSelectedColumnNamesMock]);

  const { container } = renderWithTranslation(<DefaultColumnsSelectButton />);
  const button = container.querySelector('button') as HTMLButtonElement;

  expect(setSelectedColumnNamesMock).not.toBeCalled();

  fireEvent.click(button);

  expect(setSelectedColumnNamesMock).toBeCalledWith(defaultSelectedColumnNames);
});
