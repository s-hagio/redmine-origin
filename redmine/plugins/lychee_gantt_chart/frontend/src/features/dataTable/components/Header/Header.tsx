import * as React from 'react';
import { css, cx } from '@linaria/core';
import { lightBorderColor } from '@/styles';
import { useIsDataTableVisible } from '../../models/column';
import { useTotalColumnWidth } from '../../models/width';
import ConfigDialog from '../ConfigDialog';
import ButtonList from './ButtonList';
import ColumnList from './ColumnList';

type Props = {
  containerWidth: number;
};

const DataTableHeader: React.FC<Props> = ({ containerWidth }) => {
  const totalColumnWidth = useTotalColumnWidth();
  const isDataTableVisible = useIsDataTableVisible();

  return (
    <div
      className={cx(style, !isDataTableVisible && invisibleStyle)}
      style={{ width: Math.min(containerWidth - 3, totalColumnWidth) }}
    >
      <ButtonList />

      {isDataTableVisible && (
        <div className={columnListContainerStyle}>
          <div style={{ width: totalColumnWidth }}>
            <ColumnList />
          </div>
        </div>
      )}

      <ConfigDialog />
    </div>
  );
};

export default React.memo(DataTableHeader);

const style = css`
  position: relative;
  width: fit-content;
  height: inherit;
`;

const invisibleStyle = css`
  border-bottom: 1px solid ${lightBorderColor};
`;

const columnListContainerStyle = css`
  overflow: hidden;
`;
