import * as React from 'react';
import { render } from '@testing-library/react';
import { versionFieldDefinition, versionSharedByHierarchy, versionSharedByNone } from '@/__fixtures__';
import VersionFieldInput from '../VersionFieldInput';
import { basicProps } from '../__fixtures__';

const useAssignableVersionsMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldValue', () => ({
  useAssignableVersions: useAssignableVersionsMock,
}));

const AssociatedResourceFieldInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../AssociatedResourceFieldInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    AssociatedResourceFieldInputSpy(props);
    return React.createElement('associated-resource-field-input', null, children);
  }),
}));

const versions = [versionSharedByHierarchy, versionSharedByNone];
const props = {
  ...basicProps,
  field: versionFieldDefinition,
  value: versionSharedByHierarchy.id,
};

beforeEach(() => {
  useAssignableVersionsMock.mockReturnValue(versions);
});

test('割り当て可能な値の配列を渡してAssociatedResourceFieldInputを表示', () => {
  const { container } = render(<VersionFieldInput {...props} />);

  expect(useAssignableVersionsMock).toBeCalledWith(props.issue.projectId);
  expect(AssociatedResourceFieldInputSpy).toBeCalledWith({
    ...props,
    resources: versions,
  });
  expect(container).toMatchSnapshot();
});
