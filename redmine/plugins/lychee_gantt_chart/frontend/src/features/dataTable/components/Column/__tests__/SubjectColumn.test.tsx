import * as React from 'react';
import { render } from '@testing-library/react';
import SubjectColumn from '../SubjectColumn';

vi.mock('@/features/row', async () => ({
  ...(await vi.importActual<object>('@/features/row')),
  ToggleRowCollapseButton: (props: unknown) => `toggle-button: ${JSON.stringify(props)}`,
}));

vi.mock('../Column', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn(({ children, ...props }) => React.createElement('column-mock', props, children)),
}));

test('Render with collapsible', () => {
  const result = render(
    <SubjectColumn rowId="issue-1" isCollapsible={true} depth={10}>
      Content!!
    </SubjectColumn>
  );

  expect(result.container).toMatchSnapshot();
});

test('Render with non-collapsible', () => {
  const result = render(
    <SubjectColumn rowId="issue-2" isCollapsible={false} depth={20}>
      Content!
    </SubjectColumn>
  );

  expect(result.container).toMatchSnapshot();
});
