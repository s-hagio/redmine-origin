import * as React from 'react';
import { render } from '@testing-library/react';
import { assignedToFieldDefinition, group1, user1, user2 } from '@/__fixtures__';
import AssignedToFieldInput from '../AssignedToFieldInput';
import { basicProps } from '../__fixtures__';

const useAssignableIssueUsersMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldValue', () => ({
  useAssignableIssueUsers: useAssignableIssueUsersMock,
}));

const AssociatedResourceFieldInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../AssociatedResourceFieldInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    AssociatedResourceFieldInputSpy(props);
    return React.createElement('associated-resource-field-input', null, children);
  }),
}));

const users = [user1, user2, group1];
const props = {
  ...basicProps,
  field: assignedToFieldDefinition,
  value: user2.id,
};

beforeEach(() => {
  useAssignableIssueUsersMock.mockReturnValue(users);
});

test('割り当て可能な値の配列を渡してAssociatedResourceFieldInputを表示', () => {
  const { container } = render(<AssignedToFieldInput {...props} />);

  expect(useAssignableIssueUsersMock).toBeCalledWith(props.issue);
  expect(AssociatedResourceFieldInputSpy).toBeCalledWith({
    ...props,
    resources: users,
  });
  expect(container).toMatchSnapshot();
});
