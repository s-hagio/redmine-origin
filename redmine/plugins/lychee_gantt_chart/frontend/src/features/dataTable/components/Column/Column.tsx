import * as React from 'react';
import { css } from '@linaria/core';
import { RowID } from '@/features/row';
import { selectedBackgroundColor, selectedBorderColor } from '@/styles';
import { ColumnName } from '../../models/column';
import { MIN_WIDTH } from '../../models/width';
import { useCellSelectedStatusState, useSelectCell, useToggleSelectedCellEdit } from '../../models/cell';
import { SelectableCellAutoScroller } from '.././SelectableCell';

type Props = React.PropsWithChildren<{
  rowId?: RowID;
  name: ColumnName;
}>;

const Column: React.FC<Props> = ({ rowId, name, children }) => {
  const ref = React.useRef<HTMLDivElement>(null);

  const status = useCellSelectedStatusState(rowId ?? null, name);
  const selectCell = useSelectCell();
  const toggleEdit = useToggleSelectedCellEdit();

  const handleClick = React.useCallback(() => {
    if (!status && rowId) {
      setTimeout(() => {
        selectCell(rowId, name);
      }, 5);
    }
  }, [status, selectCell, rowId, name]);

  const handleDblClick = React.useCallback(() => {
    if (status?.isEditable) {
      toggleEdit(true);
      document.getSelection()?.removeAllRanges();
    }
  }, [status?.isEditable, toggleEdit]);

  return (
    <div
      ref={ref}
      className={style}
      data-column={name}
      data-is-selected={status ? '' : null}
      data-is-editable={status?.isEditable ? '' : null}
      data-is-editing={status?.isEditing ? '' : null}
      onClick={handleClick}
      onDoubleClick={handleDblClick}
    >
      {children}

      {status && <SelectableCellAutoScroller elementRef={ref} />}
    </div>
  );
};

export default Column;

const style = css`
  position: relative;
  display: flex;
  align-items: center;
  min-width: ${MIN_WIDTH}px;
  min-height: 100%;
  padding: 1px 10px 0;
  border: 1px solid transparent;

  &[data-is-selected] {
    border-color: #999;
    background: #f9f9f9;
  }

  &[data-is-editable] {
    border-color: ${selectedBorderColor};
    background: ${selectedBackgroundColor};
  }

  &[data-is-editing] {
    border-radius: 3px;
    border-color: ${selectedBorderColor};
    background: #fff;
  }
`;
