import * as React from 'react';
import { CustomFieldFormat } from '@/models/customField';
import { CustomFieldProps } from '../types';
import StringFieldInput from '../StringFieldInput';
import NumberFieldInput from '../NumberFieldInput';
import DateFieldInput from '../DateFieldInput';
import PlainTextFieldInput from '../PlainTextFieldInput';
import BoolFieldInput from '../BoolFieldInput';
import CustomFieldListInput from './CustomFieldListInput';

const CustomFieldInput: React.FC<CustomFieldProps> = (fieldProps) => {
  switch (fieldProps.field.format) {
    case CustomFieldFormat.String:
    case CustomFieldFormat.Link:
      return <StringFieldInput {...fieldProps} />;

    case CustomFieldFormat.Int:
    case CustomFieldFormat.Float:
      return <NumberFieldInput {...fieldProps} />;

    case CustomFieldFormat.Date:
      return <DateFieldInput {...fieldProps} />;

    case CustomFieldFormat.Text:
      return <PlainTextFieldInput {...fieldProps} />;

    case CustomFieldFormat.Bool:
      return <BoolFieldInput {...fieldProps} asNumericString />;

    case CustomFieldFormat.Version:
    case CustomFieldFormat.User:
    case CustomFieldFormat.List:
    case CustomFieldFormat.Enumeration:
      return <CustomFieldListInput {...fieldProps} />;
  }

  return null;
};

export default CustomFieldInput;
