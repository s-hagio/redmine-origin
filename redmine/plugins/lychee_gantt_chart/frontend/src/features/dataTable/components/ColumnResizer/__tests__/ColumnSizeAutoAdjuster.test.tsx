import * as React from 'react';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderWithRecoilHook } from '@/test-utils';
import ColumnSizeAutoAdjuster from '../ColumnSizeAutoAdjuster';
import { columnToAdjustWidthState, storedColumnWidthsState } from '../../../models/width';
import { descriptionColumn } from '../../../__fixtures__';

// DOMのモックが面倒臭すぎるのですべてのHTMLElementのclientWidthを固定する
const longestColumnWidth = 555;

const originalOffsetWidth = Object.getOwnPropertyDescriptor(HTMLElement.prototype, 'clientWidth');

beforeAll(() => {
  Object.defineProperty(HTMLElement.prototype, 'clientWidth', {
    get: () => longestColumnWidth,
    set: () => void 0,
  });
});

afterAll(() => {
  if (originalOffsetWidth) {
    Object.defineProperty(HTMLElement.prototype, 'clientWidth', originalOffsetWidth);
  }
});

test('実際の描画幅を元に列幅を自動調整', () => {
  const { result } = renderWithRecoilHook(<ColumnSizeAutoAdjuster />, () => ({
    setColumnToAdjustWidth: useSetRecoilState(columnToAdjustWidthState),
    storedColumnWidths: useRecoilValue(storedColumnWidthsState),
  }));

  expect(result.current.storedColumnWidths.description).toBeUndefined();

  act(() => {
    result.current.setColumnToAdjustWidth(descriptionColumn);
  });

  expect(result.current.storedColumnWidths.description).toBe(longestColumnWidth + 1);
});
