import * as React from 'react';
import { MIN_WIDTH, useDisplayColumnWidths } from '../../models/width';

const ColumnStyles: React.FC = () => {
  const columnWidths = useDisplayColumnWidths();

  const styles = React.useMemo(
    () =>
      [...columnWidths]
        .map(([name, width]) => {
          return `[data-column="${name}"]{width:${Math.max(width, MIN_WIDTH)}px}`;
        })
        .join(''),
    [columnWidths]
  );

  return <style>{styles}</style>;
};

export default React.memo(ColumnStyles);
