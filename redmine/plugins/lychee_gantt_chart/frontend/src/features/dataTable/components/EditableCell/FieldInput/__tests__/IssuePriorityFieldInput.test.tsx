import * as React from 'react';
import { render } from '@testing-library/react';
import { highIssuePriority, lowIssuePriority, priorityFieldDefinition } from '@/__fixtures__';
import IssuePriorityFieldInput from '../IssuePriorityFieldInput';
import { basicProps } from '../__fixtures__';

const useAssignableIssuePrioritiesMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldValue', () => ({
  useAssignableIssuePriorities: useAssignableIssuePrioritiesMock,
}));

const AssociatedResourceFieldInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../AssociatedResourceFieldInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    AssociatedResourceFieldInputSpy(props);
    return React.createElement('associated-resource-field-input', null, children);
  }),
}));

const priorities = [highIssuePriority, lowIssuePriority];
const props = {
  ...basicProps,
  field: priorityFieldDefinition,
  value: lowIssuePriority.id,
};

beforeEach(() => {
  useAssignableIssuePrioritiesMock.mockReturnValue(priorities);
});

test('割り当て可能な値の配列を渡してAssociatedResourceFieldInputを表示', () => {
  const { container } = render(<IssuePriorityFieldInput {...props} />);

  expect(AssociatedResourceFieldInputSpy).toBeCalledWith({
    ...props,
    resources: priorities,
  });
  expect(container).toMatchSnapshot();
});
