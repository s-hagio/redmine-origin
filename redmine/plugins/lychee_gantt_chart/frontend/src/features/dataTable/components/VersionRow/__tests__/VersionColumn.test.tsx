import * as React from 'react';
import { render } from '@testing-library/react';
import { mainProjectVersionRow, versionSharedByNone } from '@/__fixtures__';
import { startDateColumn } from '../../../__fixtures__';
import VersionColumn from '../VersionColumn';

vi.mock('../../Column', async () => ({
  ...(await vi.importActual<object>('../../Column')),
  default: vi.fn(({ children, ...props }) => React.createElement('column-mock', props, children)),
  Cell: vi.fn(({ children, ...props }) => React.createElement('cell-mock', props, children)),
  ColumnContent: vi.fn(({ column, ...props }) => `ColumnContent: ${column.name} ${JSON.stringify(props)}`),
}));

const version = {
  ...versionSharedByNone,
  startDate: '2022-01-01',
};

test('Render', () => {
  const result = render(
    <VersionColumn rowId={mainProjectVersionRow.id} column={startDateColumn} version={version} />
  );

  expect(result.container).toMatchSnapshot();
});
