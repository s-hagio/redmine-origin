import { RefObject } from 'react';
import { Issue } from '@/models/issue';
import { CustomFieldDefinition } from '@/models/fieldDefinition';
import { FieldDefinition, FieldValue } from '@/features/field';

export type FieldProps = {
  cellRef: RefObject<HTMLDivElement>;
  issue: Issue;
  field: FieldDefinition;
  value: FieldValue;
  isRequired: boolean;
  onChange: (value: FieldValue) => void;
  onCommit: (value: FieldValue) => void;
};

export type CustomFieldProps = Omit<FieldProps, 'field'> & {
  field: CustomFieldDefinition;
};
