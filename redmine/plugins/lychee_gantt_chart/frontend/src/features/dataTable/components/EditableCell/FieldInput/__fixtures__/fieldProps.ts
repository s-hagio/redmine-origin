import { createRef } from 'react';
import { childIssue } from '@/__fixtures__';
import { FieldProps } from '../types';

export const basicProps: Omit<FieldProps, 'value' | 'field'> = {
  cellRef: createRef<HTMLDivElement>(),
  issue: childIssue,
  onChange: () => {},
  onCommit: () => {},
  isRequired: false,
};
