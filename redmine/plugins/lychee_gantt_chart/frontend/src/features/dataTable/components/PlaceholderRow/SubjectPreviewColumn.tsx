import * as React from 'react';
import { css, cx } from '@linaria/core';
import { useIssueCreationOrThrow, useSelectedTracker } from '@/features/issueCreation';
import { FieldContent } from '@/features/field';
import { Icon, RedmineIcon } from '@/components/Icon';
import { ColumnName, useSubjectColumn } from '../../models/column';

type Props = {
  toggleContinuousInput: () => void;
};

const SubjectPreviewColumn: React.FC<Props> = ({ toggleContinuousInput }) => {
  const subjectColumn = useSubjectColumn();
  const selectedTracker = useSelectedTracker();

  const { params, missingError } = useIssueCreationOrThrow();

  const handleMouseUp = React.useCallback(() => {
    if (!missingError) {
      toggleContinuousInput();
    }
  }, [missingError, toggleContinuousInput]);

  const partColumnContentMap = React.useMemo<ReadonlyMap<ColumnName, string>>(() => {
    const map = new Map<ColumnName, string>();
    const columnNames = subjectColumn.partColumns.map(({ name }) => name);

    if (columnNames.includes('tracker')) {
      map.set('tracker', selectedTracker?.name ?? '');
    }

    if (columnNames.includes('id')) {
      map.set('id', '#new');
    }

    return map;
  }, [selectedTracker?.name, subjectColumn.partColumns]);

  return (
    <div className={cx(style, missingError && missingErrorStyle)} onMouseUp={handleMouseUp}>
      <RedmineIcon className={ticketIconStyle} name="issue" width={16} />

      {partColumnContentMap.size > 0 && (
        <span className={metaStyle}>
          {subjectColumn.partColumns.map((column) => (
            <span key={column.name}>
              <FieldContent format="string" value={partColumnContentMap.get(column.name) ?? ''} />
            </span>
          ))}
        </span>
      )}

      <span className={textStyle}>{params.subject}</span>
      <Icon className={editIconStyle} name="edit" size={14} />
    </div>
  );
};

export default SubjectPreviewColumn;

const editIconStyle = css``;
const missingErrorStyle = css``;

const style = css`
  display: flex;
  align-items: center;
  width: 100%;

  .${editIconStyle} {
    visibility: hidden;
  }
  :not(.${missingErrorStyle}) {
    cursor: pointer;

    &:hover .${editIconStyle} {
      visibility: visible;
    }
  }
`;

const metaStyle = css`
  ::after {
    content: ':';
    margin-right: 0.15rem;
  }

  span:not(:first-of-type) {
    margin-left: 0.25rem;
  }
`;

const ticketIconStyle = css`
  height: 17px;
  margin-right: 2px;
`;

const textStyle = css`
  margin: 0 7px 0 2px;
`;
