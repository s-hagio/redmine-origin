import * as React from 'react';
import { Version } from '@/models/version';
import { VersionRow } from '@/features/row';
import { RowProps } from '../types';
import VersionSubjectColumn from './VersionSubjectColumn';
import VersionColumn from './VersionColumn';

type Props = RowProps<VersionRow> & {
  version: Version;
};

const VersionRowContent: React.FC<Props> = ({ id, version, isCollapsible, depth, columns }) => {
  return (
    <>
      <VersionSubjectColumn rowId={id} isCollapsible={isCollapsible} depth={depth} version={version} />

      {columns.map((column) => (
        <VersionColumn key={column.name} rowId={id} version={version} column={column} />
      ))}
    </>
  );
};

export default React.memo(VersionRowContent);
