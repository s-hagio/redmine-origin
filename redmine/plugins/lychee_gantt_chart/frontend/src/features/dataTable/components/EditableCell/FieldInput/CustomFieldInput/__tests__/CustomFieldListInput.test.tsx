import * as React from 'react';
import { render } from '@testing-library/react';
import { enumerationCustomFieldDefinition } from '@/__fixtures__';
import { basicProps } from '../../__fixtures__';
import CustomFieldListInput from '../CustomFieldListInput';

const useAssignableCustomFieldValuesMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldValue', () => ({
  useAssignableCustomFieldValues: useAssignableCustomFieldValuesMock,
}));

const ListFieldInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../../ListFieldInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    ListFieldInputSpy(props);
    return React.createElement('list-field-input', null, children);
  }),
}));

const props = {
  ...basicProps,
  field: enumerationCustomFieldDefinition,
  value: '1',
};

test('CustomFieldValueの配列からListItem配列を作成してListFieldInputを表示する', () => {
  useAssignableCustomFieldValuesMock.mockReturnValue([
    { value: '1', label: 'name1' },
    { value: '2', label: 'name2' },
  ]);

  const { container } = render(<CustomFieldListInput {...props} />);

  expect(ListFieldInputSpy).toBeCalledWith({
    ...props,
    isMultiple: props.field.isMultiple,
    options: [
      { id: 'name1-1', label: 'name1', value: '1' },
      { id: 'name2-2', label: 'name2', value: '2' },
    ],
  });
  expect(container).toMatchSnapshot();
});

test('valueのみのObjectの場合はvalueをlabelにする', () => {
  useAssignableCustomFieldValuesMock.mockReturnValue([{ value: '1' }, { value: '2' }]);

  render(<CustomFieldListInput {...props} />);

  expect(ListFieldInputSpy).toBeCalledWith({
    ...props,
    isMultiple: props.field.isMultiple,
    options: [
      { id: '1-1', label: '1', value: '1' },
      { id: '2-2', label: '2', value: '2' },
    ],
  });
});
