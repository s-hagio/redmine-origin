import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import MultipleItem from '../MultipleItem';
import { OptionItem } from '../types';

const ItemSpy = vi.hoisted(() => vi.fn());

vi.mock('../Item', () => ({
  default: vi.fn(({ children, className, onClick, ...props }) => {
    ItemSpy(props);
    return React.createElement('item-mock', { className, onClick, ...props }, children);
  }),
}));

const item: OptionItem = { id: 'foo-bar', label: 'foo', value: 'bar' };
const toggleSpy = vi.fn();

test('Render', () => {
  const { container } = render(<MultipleItem item={item} isChecked={false} toggle={toggleSpy} />);

  expect(container).toMatchSnapshot();
  expect(ItemSpy).toBeCalledWith({ item });
});

test('クリックでtoggleをコール', () => {
  const { getByText } = render(<MultipleItem item={item} isChecked={false} toggle={toggleSpy} />);

  expect(toggleSpy).not.toBeCalled();
  fireEvent.click(getByText('foo'));
  expect(toggleSpy).toBeCalledWith(item.value);
});
