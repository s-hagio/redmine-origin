import * as React from 'react';
import { render } from '@testing-library/react';
import { expect } from 'vitest';
import { parentIssue, parentIssueRow } from '@/__fixtures__';
import { startDateColumn } from '../../../__fixtures__';
import IssueRow from '../IssueRow';

const useIssueMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/issue', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIssue: useIssueMock,
}));

vi.mock('../../Row', () => ({
  default: vi.fn(({ children, ...props }) => {
    return React.createElement('row-mock', props, children);
  }),
}));

const IssueRowContentSpy = vi.hoisted(() => vi.fn());

vi.mock('../IssueRowContent', () => ({
  default: vi.fn((props) => {
    IssueRowContentSpy(props);
    return 'IssueRowContent';
  }),
}));

const issue = parentIssue;
const rowProps = { ...parentIssueRow, columns: [startDateColumn] };

beforeEach(() => {
  useIssueMock.mockReturnValue(issue);
});

test('Render', () => {
  const { container } = render(<IssueRow {...rowProps} />);

  expect(container).toMatchSnapshot();
  expect(IssueRowContentSpy).toBeCalledWith({ ...rowProps, issue });
});

describe('詳細データがない場合', () => {
  beforeEach(() => {
    useIssueMock.mockReturnValueOnce(null);
  });

  test('レンダリングをスキップ', () => {
    const { container } = render(<IssueRow {...rowProps} />);

    expect(container.innerHTML).toBe('');
  });
});
