import * as React from 'react';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import { DataColumn } from '../../models/column';
import HeaderColumn from './HeaderColumn';

type Props = {
  column: DataColumn;
};

const OptionalColumn: React.FC<Props> = ({ column }) => {
  const { setNodeRef, listeners, transform } = useSortable({
    id: column.name,
  });

  const cssProps = {
    transform: CSS.Transform.toString(transform),
  };

  return <HeaderColumn ref={setNodeRef} column={column} style={cssProps} {...listeners} />;
};

export default React.memo(OptionalColumn);
