import * as React from 'react';
import { css, cx } from '@linaria/core';
import { Version, useVersionURL } from '@/models/version';
import { RedmineIcon } from '@/components/Icon';
import { SubjectColumnProps } from '../types';
import { SubjectColumn } from '../Column';

type Props = SubjectColumnProps & {
  version: Version;
};

const VersionSubjectColumn: React.FC<Props> = ({ rowId, isCollapsible, depth, version }) => {
  const url = useVersionURL(version.id);

  return (
    <SubjectColumn rowId={rowId} isCollapsible={isCollapsible} depth={depth}>
      <a
        className={cx(linkStyle, version.isClosed && closedStyle)}
        href={url}
        target="_blank"
        rel="noreferrer"
      >
        <RedmineIcon name="version" width={16} />
        {version.name}
      </a>
    </SubjectColumn>
  );
};

export default React.memo(VersionSubjectColumn);

const linkStyle = css`
  display: flex;
  align-items: center;

  img {
    margin-right: 3px;
  }
`;

const closedStyle = css`
  // application.cssのスタイルに詳細度で負けるので!importantを使う
  color: #999 !important;
  text-decoration: line-through !important;
`;
