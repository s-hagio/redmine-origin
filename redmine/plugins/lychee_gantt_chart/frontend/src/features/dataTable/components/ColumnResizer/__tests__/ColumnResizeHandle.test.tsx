import * as React from 'react';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act, fireEvent } from '@testing-library/react';
import { expect } from 'vitest';
import { renderWithRecoilHook } from '@/test-utils';
import { columnToAdjustWidthState, storedColumnWidthsState } from '../../../models/width';
import { descriptionColumn } from '../../../__fixtures__';
import ColumnResizeHandle from '../ColumnResizeHandle';

test('ドラッグで列幅を変更', () => {
  const { result, container } = renderWithRecoilHook(
    <ColumnResizeHandle column={descriptionColumn} />,
    () => ({
      setWidths: useSetRecoilState(storedColumnWidthsState),
      widths: useRecoilValue(storedColumnWidthsState),
    })
  );

  act(() => {
    result.current.setWidths({ description: 200 });
  });

  const el = container.querySelector('div') as HTMLDivElement;

  fireEvent.pointerDown(el, { clientX: 0, clientY: 0 });
  expect(result.current.widths.description).toBe(200);

  fireEvent.pointerMove(document, { clientX: 10, clientY: 0 });
  expect(result.current.widths.description).toBe(210);

  fireEvent.pointerMove(document, { clientX: -30, clientY: 0 });
  expect(result.current.widths.description).toBe(170);
});

test('ダブルクリックで列幅を自動調整', () => {
  const { result, container } = renderWithRecoilHook(
    <ColumnResizeHandle column={descriptionColumn} />,
    () => ({
      columnToAdjustWidth: useRecoilValue(columnToAdjustWidthState),
    })
  );

  expect(result.current.columnToAdjustWidth).toBe(null);

  const el = container.querySelector('div') as HTMLDivElement;
  fireEvent.doubleClick(el);

  expect(result.current.columnToAdjustWidth).toBe(descriptionColumn);
});
