import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { useDraggable } from '@/lib/dnd';
import { useSelectedColumns } from '../../../../models/column';
import { idColumn, issueSubjectColumn, startDateColumn, trackerColumn } from '../../../../__fixtures__';
import SelectedColumnList from '../SelectedColumnList';

vi.mock('@dnd-kit/core', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  DragOverlay: vi.fn(({ children }) => <div data-testid="dragOverlay">{children}</div>),
}));

vi.mock('../../../../models/column', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useSelectedColumns: vi.fn(),
}));

vi.mock('../Item', () => ({
  default: vi.fn(({ column }) => {
    const { setNodeRef, listeners } = useDraggable({ id: column.name });

    return (
      <div ref={setNodeRef} data-testid={column.name} {...listeners}>
        {column.field.label}
      </div>
    );
  }),
}));

beforeEach(() => {
  vi.mocked(useSelectedColumns).mockReturnValue([
    idColumn,
    trackerColumn,
    issueSubjectColumn,
    startDateColumn,
  ]);
});

test('Render', () => {
  const { container } = render(<SelectedColumnList />);

  expect(container).toMatchSnapshot();
});

test('DragOverlayの中にドラッグ中の列を表示する', () => {
  const { getByTestId } = render(<SelectedColumnList />);

  const overlayElement = getByTestId('dragOverlay');
  const columnElement = getByTestId(startDateColumn.name);

  expect(overlayElement.textContent).not.toContain(startDateColumn.field.label);

  fireEvent.pointerDown(columnElement, { isPrimary: true });
  fireEvent.pointerMove(columnElement, { clientX: 100, clientY: 100 });

  expect(overlayElement.textContent).toContain(startDateColumn.field.label);
});
