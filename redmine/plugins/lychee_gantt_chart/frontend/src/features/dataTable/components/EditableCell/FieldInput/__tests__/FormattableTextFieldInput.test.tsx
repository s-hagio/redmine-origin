import * as React from 'react';
import { render } from '@testing-library/react';
import { stringCustomFieldDefinition } from '@/__fixtures__';
import FormattableTextFieldInput from '../FormattableTextFieldInput';
import { basicProps } from '../__fixtures__';
import { FieldProps } from '../types';

const PlainTextFieldInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../PlainTextFieldInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    PlainTextFieldInputSpy(props);
    return React.createElement('plain-text-field-input', null, children);
  }),
}));

const props: FieldProps = {
  ...basicProps,
  value: 'string!',
  field: stringCustomFieldDefinition,
  isRequired: true,
};

test('CellBasicInputを表示', () => {
  const { container } = render(<FormattableTextFieldInput {...props} />);

  expect(PlainTextFieldInputSpy).toBeCalledWith(props);
  expect(container).toMatchSnapshot();
});
