import * as React from 'react';
import { useAssignableCustomFieldValues } from '@/models/fieldValue';
import { CustomFieldProps } from '../types';
import ListFieldInput, { OptionItem } from '../ListFieldInput';

const CustomFieldListInput: React.FC<CustomFieldProps> = ({ issue, field, ...props }) => {
  const values = useAssignableCustomFieldValues(issue.projectId, field.customFieldId, issue.trackerId);

  const options = React.useMemo<OptionItem[]>(() => {
    return values.map(({ label, value }) => ({
      id: `${label ?? value}-${value}`,
      label: (label ?? value) + '',
      value,
    }));
  }, [values]);

  return (
    <ListFieldInput {...props} issue={issue} field={field} options={options} isMultiple={field.isMultiple} />
  );
};

export default CustomFieldListInput;
