import * as React from 'react';
import { css } from '@linaria/core';
import { FieldProps } from './types';

const PlainTextFieldInput: React.FC<FieldProps> = ({ value, onChange, onCommit, isRequired }) => {
  const ref = React.useRef<HTMLTextAreaElement>(null);
  const [currentValue, setCurrentValue] = React.useState<string>(value ? value + '' : '');

  const handleChange = React.useCallback(
    (event: React.ChangeEvent<HTMLTextAreaElement>) => {
      onChange(event.target.value);
      setCurrentValue(event.target.value);

      const el = ref.current;

      if (el) {
        el.style.height = 'auto';
        el.style.height = el.scrollHeight + 'px';
      }
    },
    [onChange]
  );

  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      if (event.shiftKey && event.key === 'Enter') {
        onCommit(currentValue);
      }
    };

    document.addEventListener('keydown', handleKeyDown);

    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [currentValue, onCommit]);

  return (
    <textarea
      ref={ref}
      className={style}
      value={currentValue}
      required={isRequired}
      autoFocus
      onChange={handleChange}
    />
  );
};

export default PlainTextFieldInput;

const style = css`
  resize: none;
  position: absolute;
  top: -1px;
  left: -1px;
  min-width: 10em;
  min-height: 10em;
  margin: 0;
  padding: 0.5em;
  border: 1px solid #0d679f;
  border-radius: 0;
  background: #fff;
  font: inherit;
  outline: none;
  overflow: hidden;
`;
