import * as React from 'react';
import { render } from '@testing-library/react';
import { Issue } from '@/models/issue';
import { parentIssue, parentIssueRow } from '@/__fixtures__';
import IssueRowContent from '../IssueRowContent';
import { DataColumn } from '../../../models/column';

vi.mock('../IssueSubjectColumn', () => ({
  default: vi.fn((props) => `IssueSubjectColumn: ${JSON.stringify(props)}`),
}));

vi.mock('../IssueColumn', () => ({
  default: vi.fn((props) => `IssueColumn: ${JSON.stringify(props)}`),
}));

const issue = { id: parentIssue.id, subject: parentIssue.subject } as Issue;
const columns = [{ name: 'startDate' }, { name: 'description' }] as DataColumn[];

test('Render', () => {
  const { container } = render(<IssueRowContent {...parentIssueRow} columns={columns} issue={issue} />);

  expect(container).toMatchSnapshot();
});
