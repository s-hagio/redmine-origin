import * as React from 'react';
import { render } from '@testing-library/react';
import {
  childProject2,
  grandchildProject,
  mainProject,
  parentProject,
  projectFieldDefinition,
  rootProject,
} from '@/__fixtures__';
import ProjectFieldInput from '../ProjectFieldInput';
import { basicProps } from '../__fixtures__';

const useAssignableProjectsMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldValue', () => ({
  useAssignableProjects: useAssignableProjectsMock,
}));

const AssociatedResourceFieldInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../AssociatedResourceFieldInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    AssociatedResourceFieldInputSpy(props);
    return React.createElement('associated-resource-field-input', null, children);
  }),
}));

const projects = [
  { ...rootProject, depth: 0 },
  { ...parentProject, depth: 1 },
  { ...mainProject, depth: 2 },
  { ...grandchildProject, depth: 3 },
  { ...childProject2, depth: 3 },
];

const props = {
  ...basicProps,
  field: projectFieldDefinition,
  value: mainProject.id,
};

beforeEach(() => {
  useAssignableProjectsMock.mockReturnValue(projects);
});

test('割り当て可能な値の配列を渡してAssociatedResourceFieldInputを表示', () => {
  const { container } = render(<ProjectFieldInput {...props} />);

  expect(useAssignableProjectsMock).toBeCalledWith();
  expect(AssociatedResourceFieldInputSpy).toBeCalledWith({
    ...props,
    resources: [
      { id: rootProject.id, name: rootProject.name },
      { id: parentProject.id, name: `  » ${parentProject.name}` },
      { id: mainProject.id, name: `    » ${mainProject.name}` },
      { id: grandchildProject.id, name: `      » ${grandchildProject.name}` },
      { id: childProject2.id, name: `      » ${childProject2.name}` },
    ],
  });
  expect(container).toMatchSnapshot();
});
