import * as React from 'react';
import { useIssue } from '@/models/issue';
import Row from '../Row';
import IssueRowContent from './IssueRowContent';

type Props = Omit<React.ComponentProps<typeof IssueRowContent>, 'issue'>;

const IssueRow: React.FC<Props> = ({ id, resourceId, ...props }) => {
  const issue = useIssue(resourceId);

  if (!issue) {
    return null;
  }

  return (
    <Row id={id}>
      <IssueRowContent id={id} resourceId={resourceId} issue={issue} {...props} />
    </Row>
  );
};

export default React.memo(IssueRow);
