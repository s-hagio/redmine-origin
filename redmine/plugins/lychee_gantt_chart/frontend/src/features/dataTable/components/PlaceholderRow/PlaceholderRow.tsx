import * as React from 'react';
import { css } from '@linaria/core';
import { placeholderRowBackgroundColor, useIsSubjectInputContinuedState } from '@/features/issueCreation';
import { PlaceholderRow as PlaceholderRowObject } from '@/features/row';
import { getFrontZIndex } from '@/styles';
import { useRowNumberColumnWidth, useTotalColumnWidth } from '../../models/width';
import { RowProps } from '../types';
import Row from '../Row';
import { INDENT_WIDTH } from '../Column';
import SubjectPreviewColumn from './SubjectPreviewColumn';
import SubjectInputForm from './SubjectInputForm';

type Props = RowProps<PlaceholderRowObject>;

const PlaceholderRow: React.FC<Props> = ({ id, depth }) => {
  const totalWidth = useTotalColumnWidth();
  const rowNumberWidth = useRowNumberColumnWidth();

  const indentWidth = INDENT_WIDTH * depth;
  const width = totalWidth - rowNumberWidth - indentWidth;

  const [isInputContinued, setIsInputContinued] = useIsSubjectInputContinuedState();

  const toggleContinuousInput = React.useCallback(() => {
    setIsInputContinued((current) => !current);
  }, [setIsInputContinued]);

  return (
    <Row id={id} className={style}>
      <div className={mainColumnStyle} style={{ width, marginLeft: indentWidth }}>
        {!isInputContinued && <SubjectPreviewColumn toggleContinuousInput={toggleContinuousInput} />}
        {isInputContinued && <SubjectInputForm toggleContinuousInput={toggleContinuousInput} />}
      </div>
    </Row>
  );
};

export default React.memo(PlaceholderRow);

const style = css`
  background: ${placeholderRowBackgroundColor};
  z-index: ${getFrontZIndex('issueCreation/IssueCreationPreparer')};
`;

const mainColumnStyle = css`
  display: flex;
  width: 100%;
  padding: 0 10px;
  align-items: center;
`;
