import * as React from 'react';
import { render } from '@testing-library/react';
import LabelText from '../LabelText';

test('Render', () => {
  const { container } = render(<LabelText>TEXT!</LabelText>);

  expect(container).toMatchSnapshot();
});
