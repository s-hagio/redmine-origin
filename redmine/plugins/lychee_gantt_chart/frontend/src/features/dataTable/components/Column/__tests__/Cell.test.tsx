import * as React from 'react';
import { render } from '@testing-library/react';
import Cell from '../Cell';

test('Render', () => {
  const result = render(<Cell>セルの内容！</Cell>);

  expect(result.container.textContent).toBe('セルの内容！');
});

test.todo('リンクを別ウィンドウで開く');
