import * as React from 'react';
import { css, cx } from '@linaria/core';
import { useSortStatus, useSelectSortColumn, SortDirectionIndicator } from '@/features/sort';
import { ganttHeaderRowHeight, borderColor } from '@/styles';
import Column, { Cell } from '../Column';
import { DataColumn } from '../../models/column';
import { ColumnResizeHandle } from '../ColumnResizer';

type Props = React.JSX.IntrinsicElements['div'] & {
  column: DataColumn;
};

const HeaderColumn = React.forwardRef<HTMLDivElement, Props>(function HeaderColumn(
  { column, ...props },
  ref
) {
  const resizeHandleRef = React.useRef<HTMLDivElement>(null);

  const sortStatus = useSortStatus(column.name);
  const selectSortColumn = useSelectSortColumn();

  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLDivElement>) => {
      if (
        !sortStatus.isSortable ||
        (resizeHandleRef.current && event.nativeEvent.composedPath().includes(resizeHandleRef.current))
      ) {
        return;
      }

      selectSortColumn(column.name);
    },
    [column.name, selectSortColumn, sortStatus]
  );

  return (
    <div
      ref={ref}
      className={cx(style, sortStatus.isSortable && sortableStyle)}
      onClick={handleClick}
      {...props}
    >
      <Column name={column.name}>
        <Cell>{column.field.label}</Cell>

        {sortStatus.isSelected && (
          <span className={sortDirectionIndicatorStyle}>
            <SortDirectionIndicator name={column.name} />
          </span>
        )}

        <div ref={resizeHandleRef}>
          <ColumnResizeHandle column={column} />
        </div>
      </Column>
    </div>
  );
});

export default React.memo(HeaderColumn);

const style = css`
  position: relative;
  height: ${ganttHeaderRowHeight}px;

  :not(:first-of-type)::before {
    content: '';
    position: absolute;
    top: 2px;
    left: 0;
    border-left: 1px solid ${borderColor};
    height: ${ganttHeaderRowHeight - 4}px;
    z-index: 0;
  }
`;

export const sortableStyle = css`
  cursor: pointer;
`;

const sortDirectionIndicatorStyle = css`
  margin: 0 -7px 0 auto;
`;
