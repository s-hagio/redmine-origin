import * as React from 'react';
import { render } from '@testing-library/react';
import { useVersionURL } from '@/models/version';
import { versionSharedByHierarchy, mainProjectVersionRow } from '@/__fixtures__';
import VersionSubjectColumn from '../VersionSubjectColumn';

vi.mock('@/models/version', async () => ({
  ...(await vi.importActual<object>('@/models/version')),
  useVersionURL: vi.fn(),
}));

vi.mock('@/components/Icon', () => ({
  RedmineIcon: vi.fn((props) => `RedmineIcon: ${JSON.stringify(props)}`),
}));

vi.mock('../../Column', () => ({
  SubjectColumn: vi.fn(({ children, ...props }) => React.createElement('subject-column', props, children)),
}));

beforeEach(() => {
  vi.mocked(useVersionURL).mockReturnValue(`/versions/${versionSharedByHierarchy.id}`);
});

test('Render', () => {
  const result = render(
    <VersionSubjectColumn
      rowId={mainProjectVersionRow.id}
      version={versionSharedByHierarchy}
      depth={3}
      isCollapsible={false}
    />
  );

  expect(result.container).toMatchSnapshot();
  expect(useVersionURL).toHaveBeenCalledWith(versionSharedByHierarchy.id);
});
