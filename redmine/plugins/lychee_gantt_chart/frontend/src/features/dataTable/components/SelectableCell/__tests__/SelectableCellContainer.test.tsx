import * as React from 'react';
import { render, renderHook } from '@testing-library/react';
import SelectableCellContainer from '../SelectableCellContainer';

vi.mock('../SelectableCellKeyboardNavigator', () => ({
  default: vi.fn((props) => `SelectableCellKeyboardNavigator: ${JSON.stringify(props)}`),
}));

vi.mock('../SelectableCellDeselector', () => ({
  default: vi.fn((props) => `SelectableCellDeselector: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { result } = renderHook(() => React.useRef(null));
  const { container } = render(<SelectableCellContainer containerRef={result.current} />);

  expect(container).toMatchSnapshot();
});
