import * as React from 'react';
import { render } from '@testing-library/react';
import { bugTracker, featureTracker, trackerFieldDefinition } from '@/__fixtures__';
import TrackerFieldInput from '../TrackerFieldInput';
import { basicProps } from '../__fixtures__';

const useAssignableTrackersMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldValue', () => ({
  useAssignableTrackers: useAssignableTrackersMock,
}));

const AssociatedResourceFieldInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../AssociatedResourceFieldInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    AssociatedResourceFieldInputSpy(props);
    return React.createElement('associated-resource-field-input', null, children);
  }),
}));

const trackers = [bugTracker, featureTracker];
const props = {
  ...basicProps,
  field: trackerFieldDefinition,
  value: bugTracker.id,
};

beforeEach(() => {
  useAssignableTrackersMock.mockReturnValue(trackers);
});

test('割り当て可能な値の配列を渡してAssociatedResourceFieldInputを表示', () => {
  const { container } = render(<TrackerFieldInput {...props} />);

  expect(useAssignableTrackersMock).toBeCalledWith(props.issue.projectId);
  expect(AssociatedResourceFieldInputSpy).toBeCalledWith({
    ...props,
    resources: trackers,
  });
  expect(container).toMatchSnapshot();
});
