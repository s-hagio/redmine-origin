import * as React from 'react';
import { render } from '@testing-library/react';
import ConfigDialog from '../ConfigDialog';

vi.mock('@/features/dialog', () => ({
  Dialog: vi.fn(({ children, ...props }) => (
    <>
      {`Dialog: ${JSON.stringify(props)}`}
      <div>{children}</div>
    </>
  )),
}));

vi.mock('../Toolbar/Toolbar', () => ({
  default: vi.fn((props) => `Toolbar: ${JSON.stringify(props)}`),
}));

vi.mock('../ColumnListContainer', () => ({
  default: vi.fn((props) => `ColumnListContainer: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<ConfigDialog />);

  expect(container).toMatchSnapshot();
});
