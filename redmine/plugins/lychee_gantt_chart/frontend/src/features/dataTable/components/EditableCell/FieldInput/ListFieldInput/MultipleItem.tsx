import * as React from 'react';
import { css, cx } from '@linaria/core';
import Item, { Props as ItemProps } from './Item';
import { OptionItem } from './types';

type Props = ItemProps & {
  isChecked: boolean;
  toggle: (value: OptionItem['value']) => void;
};

const MultipleItem: React.FC<Props> = ({ item, className, isChecked, toggle, ...props }) => {
  const handleChange = React.useCallback(() => {
    toggle(item.value);
  }, [toggle, item]);

  return (
    <Item className={cx(style, className)} item={item} {...props} onClick={handleChange}>
      <input type="checkbox" checked={isChecked} onChange={handleChange} />

      {item.label}
    </Item>
  );
};

export default React.memo(MultipleItem);

const style = css`
  padding-left: 0.5rem;
  gap: 0.5rem;

  &[data-focused] {
    background: #e3e3e3;
  }
`;
