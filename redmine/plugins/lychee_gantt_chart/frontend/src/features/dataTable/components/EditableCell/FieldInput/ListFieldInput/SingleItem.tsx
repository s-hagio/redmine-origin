import * as React from 'react';
import { css } from '@linaria/core';
import { primaryColor } from '@/styles';
import Item, { Props as ItemProps } from './Item';

type Props = ItemProps;

const SingleItem: React.FC<Props> = ({ item, ...props }) => {
  return (
    <Item className={style} item={item} {...props}>
      {item.label}
    </Item>
  );
};

export default React.memo(SingleItem);

const style = css`
  &[data-focused] {
    background: ${primaryColor};
    color: #fff;
  }
`;
