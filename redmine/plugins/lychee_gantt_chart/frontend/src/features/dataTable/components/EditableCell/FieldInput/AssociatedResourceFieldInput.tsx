import * as React from 'react';
import { AssociatedResource } from '@/models/associatedResource';
import { FieldProps } from './types';
import ListFieldInput, { OptionItem } from './ListFieldInput';

type Props = FieldProps & {
  resources: Pick<AssociatedResource, 'id' | 'name'>[];
};

const AssociatedResourceFieldInput: React.FC<Props> = ({ resources, ...props }) => {
  const options = React.useMemo<OptionItem[]>(() => {
    return resources.map(({ name, id }) => ({
      id: `${name}-${id}`,
      label: name,
      value: id,
    }));
  }, [resources]);

  return <ListFieldInput {...props} options={options} />;
};

export default AssociatedResourceFieldInput;
