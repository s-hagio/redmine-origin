import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { useCloseConfigDialog } from '../../../../hooks';
import Toolbar from '../Toolbar';

vi.mock('../../../../hooks', () => ({
  useCloseConfigDialog: vi.fn(),
}));

vi.mock('../DefaultColumnsSelectButton', () => ({
  default: vi.fn((props) => `DefaultColumnsSelectButton: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<Toolbar />);

  expect(container).toMatchSnapshot();
});

test('閉じるボタンをクリックでダイアログを閉じる', () => {
  const closeConfigDialogMock = vi.fn();
  vi.mocked(useCloseConfigDialog).mockReturnValue(closeConfigDialogMock);

  const { container } = render(<Toolbar />);
  const closeButton = container.querySelector('button') as HTMLButtonElement;

  expect(closeConfigDialogMock).not.toHaveBeenCalled();

  fireEvent.click(closeButton);

  expect(closeConfigDialogMock).toHaveBeenCalled();
});
