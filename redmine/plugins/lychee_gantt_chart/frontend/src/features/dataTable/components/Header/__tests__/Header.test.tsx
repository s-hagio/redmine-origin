import * as React from 'react';
import { render } from '@testing-library/react';
import DataTableHeader from '../Header';
import { useIsDataTableVisible } from '../../../models/column';

vi.mock('../../../models/width', () => ({
  useTotalColumnWidth: vi.fn().mockReturnValue(123),
}));

vi.mock('../../../models/column', () => ({
  useIsDataTableVisible: vi.fn().mockReturnValue(true),
}));

vi.mock('../../ConfigDialog', () => ({
  default: vi.fn((props) => `ConfigDialog: ${JSON.stringify(props)}`),
}));

vi.mock('../ButtonList', () => ({
  default: vi.fn((props) => `ButtonList: ${JSON.stringify(props)}`),
}));

vi.mock('../ColumnList', () => ({
  default: vi.fn((props) => `ColumnList: ${JSON.stringify(props)}`),
}));

test('Render when visible', () => {
  vi.mocked(useIsDataTableVisible).mockReturnValue(true);

  const result = render(<DataTableHeader containerWidth={1230} />);

  expect(result.container).toMatchSnapshot();
});

test('Render when invisible', () => {
  vi.mocked(useIsDataTableVisible).mockReturnValue(false);

  const result = render(<DataTableHeader containerWidth={999} />);

  expect(result.container).toMatchSnapshot();
});
