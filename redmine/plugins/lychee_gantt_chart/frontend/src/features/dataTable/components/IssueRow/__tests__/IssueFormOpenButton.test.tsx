import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import IssueFormOpenButton from '../IssueFormOpenButton';

const openIssueFormMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/issueForm', () => ({
  useOpenIssueForm: vi.fn().mockReturnValue(openIssueFormMock),
}));

test('クリックでIssueFormを開く', () => {
  const { getByRole } = render(<IssueFormOpenButton issueId={123} />);
  const button = getByRole('button');

  expect(openIssueFormMock).not.toBeCalled();

  fireEvent.click(button);

  expect(openIssueFormMock).toBeCalledWith({ id: 123 });
});
