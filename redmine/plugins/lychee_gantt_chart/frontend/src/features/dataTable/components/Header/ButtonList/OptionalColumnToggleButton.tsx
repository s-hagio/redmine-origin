import * as React from 'react';
import { Icon } from '@/components/Icon';
import { NakedButton } from '@/components/Button';
import { ColumnListType, useColumnListVisibilityState } from '../../../models/column';

const OptionalColumnToggleButton: React.FC = () => {
  const [isVisible, setVisible] = useColumnListVisibilityState(ColumnListType.Optional);

  const handleClick = React.useCallback(() => {
    setVisible(!isVisible);
  }, [isVisible, setVisible]);

  return (
    <NakedButton type="button" onClick={handleClick}>
      <Icon name={isVisible ? 'doubleArrowLeft' : 'doubleArrowRight'} size={20} />
    </NakedButton>
  );
};

export default OptionalColumnToggleButton;
