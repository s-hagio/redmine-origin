import * as React from 'react';
import { useAssignableProjects } from '@/models/fieldValue';
import { FieldProps } from './types';
import AssociatedResourceFieldInput from './AssociatedResourceFieldInput';

const ProjectFieldInput: React.FC<FieldProps> = ({ ...props }) => {
  const projects = useAssignableProjects();

  const formattedProjects = React.useMemo(() => {
    return projects.map(({ id, name, depth }) => ({
      id,
      name: '  '.repeat(depth) + (depth > 0 ? '» ' : '') + name,
    }));
  }, [projects]);

  return <AssociatedResourceFieldInput resources={formattedProjects} {...props} />;
};

export default ProjectFieldInput;
