import * as React from 'react';
import { render } from '@testing-library/react';
import { categoryFieldDefinition, issueCategory1, issueCategory2, issueCategory3 } from '@/__fixtures__';
import { basicProps } from '../__fixtures__';
import IssueCategoryFieldInput from '../IssueCategoryFieldInput';

const useAssignableIssueCategoriesMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldValue', () => ({
  useAssignableIssueCategories: useAssignableIssueCategoriesMock,
}));

const AssociatedResourceFieldInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../AssociatedResourceFieldInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    AssociatedResourceFieldInputSpy(props);
    return React.createElement('associated-resource-field-input', null, children);
  }),
}));

const categories = [issueCategory1, issueCategory2, issueCategory3];
const props = {
  ...basicProps,
  field: categoryFieldDefinition,
  value: issueCategory2.id,
};

beforeEach(() => {
  useAssignableIssueCategoriesMock.mockReturnValue(categories);
});

test('割り当て可能な値の配列を渡してAssociatedResourceFieldInputを表示', () => {
  const { container } = render(<IssueCategoryFieldInput {...props} />);

  expect(useAssignableIssueCategoriesMock).toBeCalledWith(props.issue.projectId);
  expect(AssociatedResourceFieldInputSpy).toBeCalledWith({
    ...props,
    resources: categories,
  });
  expect(container).toMatchSnapshot();
});
