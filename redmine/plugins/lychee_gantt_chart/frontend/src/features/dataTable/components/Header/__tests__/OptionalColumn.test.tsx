import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { SortableContext } from '@dnd-kit/sortable';
import { DndContext, DragOverEvent } from '@/lib/dnd';
import OptionalColumn from '../OptionalColumn';
import { descriptionColumn, startDateColumn } from '../../../__fixtures__';
import { DataColumn } from '../../../models/column';

vi.mock('../../../models/column', () => ({
  useMoveSelectedColumn: vi.fn(),
}));

type Props = {
  column: DataColumn;
};

vi.mock('../HeaderColumn', () => ({
  default: React.forwardRef<HTMLDivElement, Props>(function HeaderColumn({ column, ...props }, ref) {
    return (
      <div ref={ref} data-testid={`column-${column.name}`} {...props}>
        {column.field.label}
      </div>
    );
  }),
}));

test('Render', () => {
  const { container } = render(<OptionalColumn column={startDateColumn} />);

  expect(container).toMatchSnapshot();
});

describe('with SortableContext', () => {
  const columns = [startDateColumn, descriptionColumn] as const;
  const onDragOver = vi.fn();

  const renderWithSortable = () => {
    return render(
      <DndContext onDragOver={onDragOver}>
        <SortableContext items={columns.map(({ name }) => name)}>
          {columns.map((column) => (
            <OptionalColumn key={column.name} column={column} />
          ))}
        </SortableContext>
      </DndContext>
    );
  };

  test('ドラッグで表示列の順番を入れ替える', () => {
    const { getByTestId } = renderWithSortable();
    const columnElement = getByTestId(`column-${descriptionColumn.name}`);

    expect(onDragOver).not.toBeCalled();

    fireEvent.pointerDown(columnElement, { clientX: 0, clientY: 0, isPrimary: true });
    fireEvent.pointerMove(columnElement, { clientX: 0, clientY: 10 });

    expect(onDragOver).toBeCalled();

    const event: DragOverEvent = onDragOver.mock.calls[0][0];

    expect(event.active.id).toBe(descriptionColumn.name);
    expect(event.over.id).toBe(startDateColumn.name);
  });
});
