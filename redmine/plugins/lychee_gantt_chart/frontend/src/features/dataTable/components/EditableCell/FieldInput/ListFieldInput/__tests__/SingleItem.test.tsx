import * as React from 'react';
import { render } from '@testing-library/react';
import SingleItem from '../SingleItem';

const ItemSpy = vi.hoisted(() => vi.fn());

vi.mock('../Item', () => ({
  default: vi.fn(({ children, className, ...props }) => {
    ItemSpy(props);
    return React.createElement('item-mock', { className, ...props }, children);
  }),
}));

const item = { id: 'item-1', label: 'foo', value: 'bar' };

test('Render', () => {
  const { container } = render(<SingleItem item={item} />);

  expect(container).toMatchSnapshot();
  expect(ItemSpy).toBeCalledWith({ item });
});
