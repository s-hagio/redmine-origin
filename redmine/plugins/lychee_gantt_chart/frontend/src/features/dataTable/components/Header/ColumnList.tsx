import * as React from 'react';
import { css } from '@linaria/core';
import { verticalListSortingStrategy } from '@dnd-kit/sortable';
import { borderColor } from '@/styles';
import { useOptionalColumns, useSubjectColumn, ROW_NUMBER_COLUMN_NAME } from '../../models/column';
import Column, { ColumnSortableContext } from '../Column';
import HeaderColumn from './HeaderColumn';
import OptionalColumn from './OptionalColumn';

const ColumnList: React.FC = () => {
  const subjectColumn = useSubjectColumn();
  const columns = useOptionalColumns();

  return (
    <div className={style}>
      <Column name={ROW_NUMBER_COLUMN_NAME} />
      <HeaderColumn column={subjectColumn} />

      <ColumnSortableContext columns={columns} strategy={verticalListSortingStrategy}>
        {columns.map((column) => (
          <OptionalColumn key={column.name} column={column} />
        ))}
      </ColumnSortableContext>
    </div>
  );
};

export default React.memo(ColumnList);

const style = css`
  display: flex;
  align-items: center;
  width: fit-content;
  border-top: 1px solid ${borderColor};
  border-bottom: 1px solid ${borderColor};
`;
