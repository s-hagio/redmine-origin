import * as React from 'react';

type Props = {
  elementRef: React.RefObject<HTMLElement>;
};

const SelectableCellAutoScroller: React.FC<Props> = ({ elementRef }) => {
  const timerIdRef = React.useRef<number | null>(null);

  React.useEffect(() => {
    if (!elementRef.current) {
      return;
    }

    const bounds = elementRef.current.getBoundingClientRect();

    const scrollOptions: ScrollToOptions = {
      behavior: 'smooth',
      top: -1,
    };

    if (bounds.bottom > window.innerHeight) {
      scrollOptions.top = window.scrollY + (bounds.bottom - window.innerHeight);
    } else {
      scrollOptions.top = ((): number => {
        const overlapElements = document.elementsFromPoint(bounds.left, bounds.top);
        let bottom = -1;

        for (const overlapElement of overlapElements) {
          if (overlapElement === elementRef.current) {
            break;
          }

          const overlapElementBounds = overlapElement.getBoundingClientRect();

          if (overlapElementBounds.bottom > bottom) {
            bottom = overlapElementBounds.bottom;
          }
        }

        if (bottom === -1) {
          return -1;
        }

        return window.scrollY - (bottom - bounds.top);
      })();
    }

    if (scrollOptions.top === -1) {
      return;
    }

    timerIdRef.current && window.cancelAnimationFrame(timerIdRef.current);
    timerIdRef.current = requestAnimationFrame(() => window.scrollTo(scrollOptions));

    return () => {
      if (timerIdRef.current) {
        window.cancelAnimationFrame(timerIdRef.current);
      }
    };
  }, [elementRef]);

  return null;
};

export default SelectableCellAutoScroller;
