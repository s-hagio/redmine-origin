import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import Item from '../Item';
import { descriptionColumn, issueSubjectColumn } from '../../../../__fixtures__';
import { useToggleSelectedColumn } from '../../../../models/column';

vi.mock('../../../../models/column', async () => ({
  ...(await vi.importActual<object>('../../../../models/column')),
  useToggleSelectedColumn: vi.fn(),
}));

test('Render', () => {
  const { container } = render(
    <>
      <Item column={issueSubjectColumn} checked={true} />
      <Item column={descriptionColumn} checked={false} />
    </>
  );

  expect(container).toMatchSnapshot();
});

test('チェックボックスのchangeイベントで表示項目の選択をトグルする', async () => {
  const toggleSelectedColumnMock = vi.fn();
  vi.mocked(useToggleSelectedColumn).mockReturnValue(toggleSelectedColumnMock);

  const { container } = render(<Item column={descriptionColumn} checked={false} />);
  const checkbox = container.querySelector('input[type="checkbox"]') as HTMLInputElement;

  expect(toggleSelectedColumnMock).not.toHaveBeenCalled();

  fireEvent.click(checkbox);
  expect(toggleSelectedColumnMock).toHaveBeenCalledWith(descriptionColumn.name);

  toggleSelectedColumnMock.mockClear();

  fireEvent.click(checkbox);
  expect(toggleSelectedColumnMock).toHaveBeenCalledWith(descriptionColumn.name);
});
