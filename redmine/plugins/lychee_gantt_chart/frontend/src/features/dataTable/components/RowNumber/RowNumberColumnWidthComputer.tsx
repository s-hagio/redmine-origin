import * as React from 'react';
import { css } from '@linaria/core';
import { useMaxRowNumber } from '@/features/row';
import { useSetRowNumberColumnWidth } from '../../models/width';
import RowNumberColumnContent from './RowNumberColumnContent';

const MIN_WIDTH = 20;

const RowNumberColumnWidthComputer: React.FC = () => {
  const maxRowNumber = useMaxRowNumber();
  const setWidth = useSetRowNumberColumnWidth();

  const elementRef = React.useRef<HTMLDivElement>(null);
  const [isComputing, setComputing] = React.useState<boolean>(false);

  React.useEffect(() => {
    setComputing(maxRowNumber > 0);
  }, [maxRowNumber]);

  React.useEffect(() => {
    if (!elementRef.current) {
      return;
    }

    setWidth(Math.max(elementRef.current.clientWidth, MIN_WIDTH));
    setComputing(false);
  }, [setWidth, isComputing]);

  if (!isComputing) {
    return null;
  }

  return (
    <div ref={elementRef} className={style}>
      <RowNumberColumnContent number={maxRowNumber} />
    </div>
  );
};

export default React.memo(RowNumberColumnWidthComputer);

const style = css`
  position: fixed;
  height: 0;
  overflow: hidden;
  visibility: hidden;
`;
