import * as React from 'react';
import { render } from '@testing-library/react';
import { versionFieldDefinition } from '@/__fixtures__';
import AssociatedResourceFieldInput from '../AssociatedResourceFieldInput';
import { basicProps } from '../__fixtures__';

const ListFieldInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../ListFieldInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    ListFieldInputSpy(props);
    return React.createElement('list-field-input', null, children);
  }),
}));

const resources = [
  { id: 1, name: 'name1' },
  { id: 2, name: 'name2' },
];
const props = {
  ...basicProps,
  field: versionFieldDefinition,
  value: 1,
};

test('{id, name}を持つリソースの配列からListItem配列を作成してListFieldInputを表示する', () => {
  const { container } = render(<AssociatedResourceFieldInput {...props} resources={resources} />);

  expect(ListFieldInputSpy).toBeCalledWith({
    ...props,
    options: [
      { id: 'name1-1', label: 'name1', value: 1 },
      { id: 'name2-2', label: 'name2', value: 2 },
    ],
  });
  expect(container).toMatchSnapshot();
});
