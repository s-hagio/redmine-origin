import * as React from 'react';
import { render } from '@testing-library/react';
import { parentIssueFieldDefinition } from '@/__fixtures__';
import { basicProps } from '../__fixtures__';
import { FieldProps } from '../types';
import ParentIssueIdFieldInput from '../ParentIssueIdFieldInput';

const CellBasicInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../CellBasicInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    CellBasicInputSpy(props);
    return React.createElement('cell-basic-input', null, children);
  }),
}));

const props: FieldProps = {
  ...basicProps,
  value: 123,
  field: parentIssueFieldDefinition,
  isRequired: true,
};

test('CellBasicInputを表示', () => {
  const { container } = render(<ParentIssueIdFieldInput {...props} />);

  expect(CellBasicInputSpy).toBeCalledWith(props);
  expect(container).toMatchSnapshot();
});
