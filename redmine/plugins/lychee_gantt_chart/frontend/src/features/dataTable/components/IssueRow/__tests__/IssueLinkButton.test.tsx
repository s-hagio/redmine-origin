import * as React from 'react';
import { render } from '@testing-library/react';
import { useAssociatedPrincipal } from '@/models/associatedResource';
import { user1, userWithAvatar } from '@/__fixtures__';
import IssueLinkButton from '../IssueLinkButton';

vi.mock('@/models/setting', () => ({
  useSettings: vi.fn().mockReturnValue({ baseURL: '/path/to/root' }),
}));

vi.mock('@/models/associatedResource', () => ({
  useAssociatedPrincipal: vi.fn(),
}));

vi.mock('../IssueFormOpenButton', () => ({
  default: vi.fn(({ children, ...props }) => React.createElement('issue-form-open-button', props, children)),
}));

test('Render without avatar', () => {
  vi.mocked(useAssociatedPrincipal).mockReturnValue(user1);

  const result = render(<IssueLinkButton issueId={123} assignedToId={null} />);

  expect(result.container).toMatchSnapshot();
});

test('Render with avatar', () => {
  vi.mocked(useAssociatedPrincipal).mockImplementation((id) =>
    userWithAvatar.id === id ? userWithAvatar : null
  );

  const result = render(<IssueLinkButton issueId={123} assignedToId={userWithAvatar.id} />);
  expect(result.container).toMatchSnapshot();

  result.rerender(<IssueLinkButton issueId={123} assignedToId={null} />);
  expect(result.container).toMatchSnapshot();
});
