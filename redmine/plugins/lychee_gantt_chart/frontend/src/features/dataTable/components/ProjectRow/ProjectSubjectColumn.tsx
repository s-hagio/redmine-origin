import * as React from 'react';
import { css } from '@linaria/core';
import { Project, useProjectURL } from '@/models/project';
import { RedmineIcon } from '@/components/Icon';
import { SubjectColumnProps } from '../types';
import { SubjectColumn } from '../Column';

type Props = SubjectColumnProps & {
  project: Project;
};

const ProjectSubjectColumn: React.FC<Props> = ({ rowId, isCollapsible, depth, project }) => {
  const url = useProjectURL(project.identifier);

  return (
    <SubjectColumn rowId={rowId} isCollapsible={isCollapsible} depth={depth}>
      <a className={linkStyle} href={url} target="_blank" rel="noreferrer">
        <RedmineIcon name="project" width={16} />
        {project.name}
      </a>
    </SubjectColumn>
  );
};

export default React.memo(ProjectSubjectColumn);

const linkStyle = css`
  display: flex;
  align-items: center;

  img {
    margin-right: 3px;
  }
`;
