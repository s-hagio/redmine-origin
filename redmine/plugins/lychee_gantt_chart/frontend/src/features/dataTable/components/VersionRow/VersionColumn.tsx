import * as React from 'react';
import { VersionResourceIdentifier } from '@/models/coreResource';
import { Version } from '@/models/version';
import Column, { Cell, ColumnContent } from '../Column';
import { getVersionColumnContent, DataColumn } from '../../models/column';

type Props = {
  rowId: VersionResourceIdentifier;
  version: Version;
  column: DataColumn;
};

const VersionColumn: React.FC<Props> = ({ rowId, version, column }) => {
  return (
    <Column key={column.name} rowId={rowId} name={column.name}>
      <Cell>
        <ColumnContent column={column} value={getVersionColumnContent(column, version)} />
      </Cell>
    </Column>
  );
};

export default React.memo(VersionColumn);
