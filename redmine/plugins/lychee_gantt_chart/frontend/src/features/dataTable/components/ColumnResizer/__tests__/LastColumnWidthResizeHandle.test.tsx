import * as React from 'react';
import { render } from '@testing-library/react';
import { useDisplayColumns } from '../../../models/column';
import { descriptionColumn, startDateColumn, subjectColumn } from '../../../__fixtures__';
import LastColumnWidthResizeHandle from '../LastColumnResizeHandle';

vi.mock('../../../models/column', () => ({
  useDisplayColumns: vi.fn().mockReturnValue(vi.fn()),
}));

vi.mock('../ColumnResizeHandle', () => ({
  default: vi.fn(({ column, ...props }) => `ColumnResizeHandle: ${column.name} ${JSON.stringify(props)}`),
}));

test('Render', () => {
  vi.mocked(useDisplayColumns).mockReturnValue([subjectColumn, startDateColumn, descriptionColumn]);

  const result = render(<LastColumnWidthResizeHandle />);

  expect(result.container).toMatchSnapshot();
});

test('表示列が無い時はnull', () => {
  vi.mocked(useDisplayColumns).mockReturnValue([]);

  const result = render(<LastColumnWidthResizeHandle />);

  expect(result.container.innerHTML).toBe('');
});
