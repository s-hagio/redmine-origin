import * as React from 'react';
import { css, cx } from '@linaria/core';
import { OptionItem } from './types';

export type Props = JSX.IntrinsicElements['div'] & {
  item: OptionItem;
  isFocused?: boolean;
  onEnter?: (item: OptionItem) => void;
};

const Item: React.FC<Props> = ({ item, isFocused, onEnter, className, children, ...props }) => {
  const handleMouseEnter = React.useCallback(() => {
    !isFocused && onEnter && onEnter(item);
  }, [isFocused, item, onEnter]);

  return (
    <div
      className={cx(style, className)}
      data-focused={isFocused ? '' : null}
      {...props}
      onMouseEnter={handleMouseEnter}
    >
      {children}
    </div>
  );
};

export default Item;

const style = css`
  display: flex;
  align-items: center;
  gap: 0.65rem;
  min-height: 2rem;
  padding: 0.5rem 1.25rem;
  cursor: default;
  white-space: pre;
`;
