import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { rootIssueRow } from '@/__fixtures__';
import RowNumberColumn from '../RowNumberColumn';

const useRowNumberMock = vi.hoisted(() => vi.fn());
const selectRowMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useSelectRow: vi.fn().mockReturnValue(selectRowMock),
  useRowNumber: useRowNumberMock,
}));

const deselectCellMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/cell', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useDeselectCell: vi.fn().mockReturnValue(deselectCellMock),
}));

vi.mock('../RowNumberColumnContent', () => ({
  default: vi.fn((props) => `RowNumberColumnContent: ${JSON.stringify(props)}`),
}));

beforeEach(() => {
  useRowNumberMock.mockReturnValue(123);
});

test('Render', () => {
  const { container } = render(<RowNumberColumn rowId={rootIssueRow.id} />);

  expect(container).toMatchSnapshot();
  expect(useRowNumberMock).toBeCalledWith(rootIssueRow.id);
});

test('mousedown行の選択処理', () => {
  const { container } = render(<RowNumberColumn rowId={rootIssueRow.id} />);

  expect(selectRowMock).not.toBeCalled();
  expect(deselectCellMock).not.toBeCalled();

  fireEvent.mouseDown(container.firstElementChild!);

  expect(selectRowMock).toBeCalledWith(rootIssueRow.id);
  expect(deselectCellMock).toBeCalled();
});
