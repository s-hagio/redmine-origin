import * as React from 'react';
import DatePicker from 'react-datepicker';
import { css } from '@linaria/core';
import { isValid } from 'date-fns';
import { useTranslation } from 'react-i18next';
import { formatISODate, toDate } from '@/lib/date';
import { FieldProps } from './types';

const DateFieldInput: React.FC<FieldProps> = ({ value, onChange, onCommit }) => {
  const { t } = useTranslation();

  const [selectedDate, setSelectedDate] = React.useState(value ? toDate(value + '') : null);

  const commitSelectedDate = React.useCallback(() => {
    onCommit(selectedDate ? formatISODate(selectedDate) : '');
  }, [onCommit, selectedDate]);

  const handleChange = React.useCallback(
    (date: Date | null) => {
      setSelectedDate(date);
      onChange(date ? formatISODate(date) : '');
    },
    [onChange]
  );

  const handleChangeRaw = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      try {
        if (!event.target.value) {
          return handleChange(null);
        }

        const date = toDate(event.target.value);

        if (isValid(date)) {
          handleChange(date);
        }
      } catch (e) {
        // noop
      }
    },
    [handleChange]
  );

  const handleSelect = React.useCallback(
    (date: Date | null) => {
      const value = date ? formatISODate(date) : '';
      onChange(value);
      onCommit(value);
    },
    [onChange, onCommit]
  );

  const handleKeyDown = React.useCallback(
    (event: React.KeyboardEvent<HTMLInputElement>) => {
      if (
        !event.nativeEvent.isComposing &&
        event.target instanceof HTMLInputElement &&
        event.code === 'Enter'
      ) {
        commitSelectedDate();
      }
    },
    [commitSelectedDate]
  );

  return (
    <DatePicker
      wrapperClassName={wrapperStyle}
      selected={selectedDate}
      todayButton={t('label.selectToday')}
      isClearable
      clearButtonClassName={clearButtonStyle}
      onChange={handleChange}
      onChangeRaw={handleChangeRaw}
      onKeyDown={handleKeyDown}
      onSelect={handleSelect}
      dateFormatCalendar="yyyy/MM"
      dateFormat="yyyy-MM-dd"
      autoFocus
    />
  );
};

export default DateFieldInput;

const wrapperStyle = css`
  &,
  > * {
    display: flex;
    align-items: center;
  }

  input,
  input:focus {
    width: 100%;
    height: 100%;
    padding: 0.25rem 0.5rem;
    border: none;
    outline: none;
    background: transparent;
    font: inherit;
    color: inherit;
  }
`;

const clearButtonStyle = css`
  right: 0.25em;
  padding: 0;

  ::after {
    background-color: transparent;
    color: #7d7d7d;
    width: auto;
    height: auto;
    padding: 0;
    font-size: 1.5em;
  }
`;
