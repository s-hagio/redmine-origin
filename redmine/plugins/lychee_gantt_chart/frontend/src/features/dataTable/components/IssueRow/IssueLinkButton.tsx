import * as React from 'react';
import { css } from '@linaria/core';
import { Issue, IssueID } from '@/models/issue';
import { useAssociatedPrincipal } from '@/models/associatedResource';
import { RedmineIcon } from '@/components/Icon';
import IssueFormOpenButton from './IssueFormOpenButton';

type Props = {
  issueId: IssueID;
  assignedToId: Issue['assignedToId'];
};

const IssueLinkButton: React.FC<Props> = ({ issueId, assignedToId }) => {
  const assignedUser = useAssociatedPrincipal(assignedToId);

  return (
    <IssueFormOpenButton issueId={issueId} className={style}>
      {assignedUser?.avatar ? (
        <span className={avatarStyle} dangerouslySetInnerHTML={{ __html: assignedUser.avatar }} />
      ) : (
        <span className={ticketIconStyle}>
          <RedmineIcon name="issue" width={16} />
        </span>
      )}
    </IssueFormOpenButton>
  );
};

export default IssueLinkButton;

const style = css`
  position: relative;
  display: inline-flex;
  align-items: center;
  width: 16px;
  margin-right: 2px;
  vertical-align: middle;
`;

const ticketIconStyle = css`
  height: 17px;
`;

const avatarStyle = css`
  display: inline-flex;
  align-items: center;
  height: 17px;

  img {
    width: 15px;
  }
`;
