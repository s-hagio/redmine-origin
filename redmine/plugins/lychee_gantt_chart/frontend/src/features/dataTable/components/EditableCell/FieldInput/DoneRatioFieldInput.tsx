import * as React from 'react';
import { FieldProps } from './types';
import ListFieldInput, { OptionItem } from './ListFieldInput';

const optionItems: OptionItem[] = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100].map((doneRatio) => {
  return {
    id: doneRatio + '',
    label: `${doneRatio}%`,
    value: doneRatio,
  };
});

const DoneRatioFieldInput: React.FC<FieldProps> = ({ value, isRequired, onChange, onCommit, ...props }) => {
  return (
    <ListFieldInput
      {...props}
      options={optionItems}
      value={value}
      isRequired={isRequired}
      onChange={onChange}
      onCommit={onCommit}
    />
  );
};

export default DoneRatioFieldInput;
