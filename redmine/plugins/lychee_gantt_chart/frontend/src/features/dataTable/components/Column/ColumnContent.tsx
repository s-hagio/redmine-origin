import * as React from 'react';
import {
  FieldContent,
  FieldValue,
  getCustomFieldContentFormat,
  isCustomFieldDefinition,
} from '@/features/field';
import { DataColumn } from '../../models/column';

type Props = {
  column: DataColumn;
  value: FieldValue;
};

const ColumnContent: React.FC<Props> = ({ column, value }) => {
  if (isCustomFieldDefinition(column.field)) {
    return <FieldContent format={getCustomFieldContentFormat(column.field)} value={value} />;
  }
  return <FieldContent format={column.format ?? column.field.format} value={value} />;
};

export default ColumnContent;
