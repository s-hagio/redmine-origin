import * as React from 'react';
import { render } from '@testing-library/react';
import { expect } from 'vitest';
import { mainProject, mainProjectRow } from '@/__fixtures__';
import { startDateColumn } from '../../../__fixtures__';
import ProjectRow from '../ProjectRow';

const useProjectMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/project', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useProject: useProjectMock,
}));

vi.mock('../../Row', () => ({
  default: vi.fn(({ children, ...props }) => {
    return React.createElement('row-mock', props, children);
  }),
}));

const ProjectRowContentSpy = vi.hoisted(() => vi.fn());

vi.mock('../ProjectRowContent', () => ({
  default: vi.fn((props) => {
    ProjectRowContentSpy(props);
    return 'ProjectRowContent';
  }),
}));

const project = mainProject;
const rowProps = { ...mainProjectRow, columns: [startDateColumn] };

beforeEach(() => {
  useProjectMock.mockReturnValue(project);
});

test('Render', () => {
  const { container } = render(<ProjectRow {...rowProps} />);

  expect(container).toMatchSnapshot();
  expect(ProjectRowContentSpy).toBeCalledWith({ ...rowProps, project });
});

describe('詳細データがない場合', () => {
  beforeEach(() => {
    useProjectMock.mockReturnValueOnce(null);
  });

  test('レンダリングをスキップ', () => {
    const { container } = render(<ProjectRow {...rowProps} />);

    expect(container.innerHTML).toBe('');
  });
});
