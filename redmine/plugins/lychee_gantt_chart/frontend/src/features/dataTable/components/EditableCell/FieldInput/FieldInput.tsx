import * as React from 'react';
import { FieldFormat, isCustomFieldDefinition } from '@/features/field';
import { DataColumn } from '../../../models/column';
import { FieldProps } from './types';
import PlainTextFieldInput from './PlainTextFieldInput';
import FormattableTextFieldInput from './FormattableTextFieldInput';
import StringFieldInput from './StringFieldInput';
import NumberFieldInput from './NumberFieldInput';
import DoneRatioFieldInput from './DoneRatioFieldInput';
import DateFieldInput from './DateFieldInput';
import BoolFieldInput from './BoolFieldInput';
import ParentIssueIdFieldInput from './ParentIssueIdFieldInput';
import ProjectFieldInput from './ProjectFieldInput';
import VersionFieldInput from './VersionFieldInput';
import TrackerFieldInput from './TrackerFieldInput';
import AssignedToFieldInput from './AssignedToFieldInput';
import IssueStatusFieldInput from './IssueStatusFieldInput';
import IssuePriorityFieldInput from './IssuePriorityFieldInput';
import IssueCategoryFieldInput from './IssueCategoryFieldInput';
import CustomFieldInput from './CustomFieldInput';

type Props = Omit<FieldProps, 'field'> & {
  column: DataColumn;
};

const FieldInput: React.FC<Props> = ({ column, ...props }) => {
  const field = column.field;
  const fieldProps: FieldProps = { field, ...props };

  if (isCustomFieldDefinition(field)) {
    return <CustomFieldInput {...fieldProps} field={field} />;
  }

  switch (field.format) {
    case FieldFormat.PlainText:
      return <PlainTextFieldInput {...fieldProps} />;

    case FieldFormat.FormattableText:
      return <FormattableTextFieldInput {...fieldProps} />;

    case FieldFormat.String:
      return <StringFieldInput {...fieldProps} />;

    case FieldFormat.Int:
    case FieldFormat.Float:
    case FieldFormat.Percent:
      return <NumberFieldInput {...fieldProps} />;

    case FieldFormat.DoneRatio:
      return <DoneRatioFieldInput {...fieldProps} />;

    case FieldFormat.Date:
      return <DateFieldInput {...fieldProps} />;

    case FieldFormat.Bool:
      return <BoolFieldInput {...fieldProps} />;

    case FieldFormat.ParentIssueId:
      return <ParentIssueIdFieldInput {...fieldProps} />;

    case FieldFormat.Project:
      return <ProjectFieldInput {...fieldProps} />;

    case FieldFormat.Version:
      return <VersionFieldInput {...fieldProps} />;

    case FieldFormat.Tracker:
      return <TrackerFieldInput {...fieldProps} />;

    case FieldFormat.AssignedTo:
      return <AssignedToFieldInput {...fieldProps} />;

    case FieldFormat.IssueStatusWithColor:
    case FieldFormat.IssueStatus:
      return <IssueStatusFieldInput {...fieldProps} />;

    case FieldFormat.IssuePriority:
      return <IssuePriorityFieldInput {...fieldProps} />;

    case FieldFormat.IssueCategory:
      return <IssueCategoryFieldInput {...fieldProps} />;
  }

  console.error(`Unsupported field format: ${field.format}`);

  return null;
};

export default FieldInput;
