import { FieldValue } from '@/features/field';

type Value = FieldValue;

export type OptionItem = {
  id: string;
  label: string;
  value: Value;
};

export type EventHandlers = {
  onChange: (value: Value) => void;
  onCommit: (value: Value) => void;
};
