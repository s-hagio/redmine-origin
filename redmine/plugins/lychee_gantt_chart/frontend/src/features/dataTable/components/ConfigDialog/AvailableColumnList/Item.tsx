import * as React from 'react';
import { css } from '@linaria/core';
import { DataColumn, isSubjectColumn, useToggleSelectedColumn } from '../../../models/column';
import LabelText from '../LabelText';

type Props = {
  column: DataColumn;
  checked: boolean;
};

const Item: React.FC<Props> = ({ column, checked }) => {
  const toggleSelectColumn = useToggleSelectedColumn();

  const handleChange = React.useCallback(() => {
    toggleSelectColumn(column.name);
  }, [column.name, toggleSelectColumn]);

  return (
    <li className={style} key={column.name}>
      <label title={column.field.label}>
        <input type="checkbox" checked={checked} onChange={handleChange} disabled={isSubjectColumn(column)} />
        <LabelText>{column.field.label}</LabelText>
      </label>
    </li>
  );
};

export default React.memo(Item);

const style = css`
  label {
    display: flex;
    align-items: center;
    width: 100%;
    margin: 0;
  }
`;
