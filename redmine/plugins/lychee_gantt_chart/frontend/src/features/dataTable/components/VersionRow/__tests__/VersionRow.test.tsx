import * as React from 'react';
import { render } from '@testing-library/react';
import { expect } from 'vitest';
import { mainProjectVersionRow, versionSharedByHierarchy } from '@/__fixtures__';
import { startDateColumn } from '../../../__fixtures__';
import VersionRow from '../VersionRow';

const useVersionMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/version', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useVersion: useVersionMock,
}));

vi.mock('../../Row', () => ({
  default: vi.fn(({ children, ...props }) => {
    return React.createElement('row-mock', props, children);
  }),
}));

const VersionRowContentSpy = vi.hoisted(() => vi.fn());

vi.mock('../VersionRowContent', () => ({
  default: vi.fn((props) => {
    VersionRowContentSpy(props);
    return 'VersionRowContent';
  }),
}));

const version = versionSharedByHierarchy;
const rowProps = { ...mainProjectVersionRow, columns: [startDateColumn] };

beforeEach(() => {
  useVersionMock.mockReturnValue(version);
});

test('Render', () => {
  const { container } = render(<VersionRow {...rowProps} />);

  expect(container).toMatchSnapshot();
  expect(VersionRowContentSpy).toBeCalledWith({ ...rowProps, version });
});

describe('詳細データがない場合', () => {
  beforeEach(() => {
    useVersionMock.mockReturnValueOnce(null);
  });

  test('レンダリングをスキップ', () => {
    const { container } = render(<VersionRow {...rowProps} />);

    expect(container.innerHTML).toBe('');
  });
});
