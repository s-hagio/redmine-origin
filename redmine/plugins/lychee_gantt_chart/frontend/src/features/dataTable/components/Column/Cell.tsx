import * as React from 'react';
import { css } from '@linaria/core';

type Props = {
  children: React.ReactNode;
};

const Cell: React.FC<Props> = ({ children }) => {
  const handleClick = React.useCallback((event: React.MouseEvent) => {
    const el = event.target as HTMLElement;

    if (el.tagName === 'A') {
      event.stopPropagation();
      el.setAttribute('target', '_blank');
    }
  }, []);

  return (
    <span className={style} onClick={handleClick}>
      {children}
    </span>
  );
};

export default Cell;

const style = css`
  &,
  div {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  p {
    display: inline;
    margin: 0;
  }
`;
