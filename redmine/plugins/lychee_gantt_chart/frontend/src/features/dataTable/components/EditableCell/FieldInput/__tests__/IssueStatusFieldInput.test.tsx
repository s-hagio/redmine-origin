import * as React from 'react';
import { render } from '@testing-library/react';
import { inProgressIssueStatus, newIssueStatus, statusFieldDefinition } from '@/__fixtures__';
import IssueStatusFieldInput from '../IssueStatusFieldInput';
import { basicProps } from '../__fixtures__';

const useAssignableIssueStatusesMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldValue', () => ({
  useAssignableIssueStatuses: useAssignableIssueStatusesMock,
}));

const AssociatedResourceFieldInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../AssociatedResourceFieldInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    AssociatedResourceFieldInputSpy(props);
    return React.createElement('associated-resource-field-input', null, children);
  }),
}));

const statuses = [newIssueStatus, inProgressIssueStatus];
const props = {
  ...basicProps,
  field: statusFieldDefinition,
  value: inProgressIssueStatus.id,
};

beforeEach(() => {
  useAssignableIssueStatusesMock.mockReturnValue(statuses);
});

test('割り当て可能な値の配列を渡してAssociatedResourceFieldInputを表示', () => {
  const { container } = render(<IssueStatusFieldInput {...props} />);

  expect(useAssignableIssueStatusesMock).toBeCalledWith(props.issue);
  expect(AssociatedResourceFieldInputSpy).toBeCalledWith({
    ...props,
    resources: statuses,
  });
  expect(container).toMatchSnapshot();
});
