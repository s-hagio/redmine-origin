import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import Item from '../Item';
import { OptionItem } from '../types';

const item: OptionItem = {
  id: 'item-1',
  label: 'Item!',
  value: 123,
};

const onEnterSpy = vi.fn();

test('Render', () => {
  const { container, rerender } = render(<Item item={item} />);

  expect(container).toMatchSnapshot();

  rerender(<Item item={item} isFocused={true} />);

  expect(container).toMatchSnapshot();
});

test('マウスエンターでonEnterをコール', () => {
  const { getByText } = render(
    <Item item={item} onEnter={onEnterSpy}>
      text!
    </Item>
  );

  expect(onEnterSpy).not.toBeCalled();
  fireEvent.mouseEnter(getByText('text!'));
  expect(onEnterSpy).toBeCalledWith(item);
});
