import * as React from 'react';
import { SortableContext, SortableContextProps } from '@dnd-kit/sortable';
import { closestCenter } from '@dnd-kit/core';
import { indexBy } from '@/utils/array';
import { isString } from '@/utils/string';
import { DndContext, DragEvent, DragEventHandlerMap } from '@/lib/dnd';
import { ColumnName, DataColumn, useMoveSelectedColumn } from '../../models/column';

type Props = React.PropsWithChildren<
  Omit<SortableContextProps, 'items'> & {
    columns: DataColumn[];
  } & DragEventHandlerMap
>;

const ColumnSortableContext: React.FC<Props> = ({
  columns,
  onDragStart,
  onDragMove,
  onDragOver,
  onDragEnd,
  onDragCancel,
  children,
  ...props
}) => {
  const columnNames = React.useMemo(() => columns.map((column) => column.name), [columns]);
  const columnsByName = React.useMemo(() => indexBy(columns, 'name'), [columns]);
  const moveSelectedColumn = useMoveSelectedColumn();

  const moveSelectedColumnByNames = React.useCallback(
    (sourceName: ColumnName, targetName: ColumnName) => {
      const sourceColumn = columnsByName[sourceName];
      const targetColumn = columnsByName[targetName];

      if (sourceColumn && targetColumn) {
        moveSelectedColumn(sourceColumn, targetColumn);
      }
    },
    [columnsByName, moveSelectedColumn]
  );

  const handleMoveColumn = React.useCallback(
    ({ active, over }: DragEvent) => {
      if (isString(active?.id) && isString(over?.id)) {
        moveSelectedColumnByNames(active.id, over.id);
      }
    },
    [moveSelectedColumnByNames]
  );

  const dragEventHandlers = React.useMemo<DragEventHandlerMap>(() => {
    return {
      onDragStart,
      onDragMove,
      onDragOver: (event) => {
        onDragOver?.(event);
        handleMoveColumn(event);
      },
      onDragEnd: (event) => {
        onDragEnd?.(event);
        handleMoveColumn(event);
      },
      onDragCancel,
    };
  }, [handleMoveColumn, onDragCancel, onDragEnd, onDragMove, onDragOver, onDragStart]);

  return (
    <DndContext {...dragEventHandlers} collisionDetection={closestCenter}>
      <SortableContext items={columnNames} {...props}>
        {children}
      </SortableContext>
    </DndContext>
  );
};

export default React.memo(ColumnSortableContext);
