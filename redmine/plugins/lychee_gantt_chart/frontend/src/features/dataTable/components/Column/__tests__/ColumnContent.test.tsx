import * as React from 'react';
import { render } from '@testing-library/react';

import ColumnContent from '../ColumnContent';
import {
  attachmentCustomFieldColumn,
  boolCustomFieldColumn,
  dateCustomFieldColumn,
  enumerationCustomFieldColumn,
  floatCustomFieldColumn,
  formattableStringCustomFieldColumn,
  formattableTextCustomFieldColumn,
  intCustomFieldColumn,
  linkCustomFieldColumn,
  listCustomFieldColumn,
  parentSubjectColumn,
  stringCustomFieldColumn,
  textCustomFieldColumn,
  userCustomFieldColumn,
  versionCustomFieldColumn,
} from '../../../__fixtures__';

vi.mock('@/features/field', async () => ({
  ...(await vi.importActual<object>('@/features/field')),
  FieldContent: vi.fn((props) => `FieldContent: ${JSON.stringify(props)}`),
}));

test('コアフィールドのColumnはformatに応じてレンダリング', () => {
  const result = render(<ColumnContent column={parentSubjectColumn} value="親チケットの題名！" />);

  expect(result.container).toMatchSnapshot();
});

test.each([
  enumerationCustomFieldColumn,
  stringCustomFieldColumn,
  versionCustomFieldColumn,
  attachmentCustomFieldColumn,
  userCustomFieldColumn,
  listCustomFieldColumn,
  linkCustomFieldColumn,
  floatCustomFieldColumn,
  intCustomFieldColumn,
  dateCustomFieldColumn,
  boolCustomFieldColumn,
  textCustomFieldColumn,
])('カスタムフィールドの$field.format型のレンダリング', (column) => {
  const result = render(<ColumnContent column={column} value={`${column.field.format}の値！`} />);

  expect(result.container).toMatchSnapshot();
});

test.each([formattableStringCustomFieldColumn, formattableTextCustomFieldColumn])(
  'テキスト形式ONの$field.format型',
  (column) => {
    const result = render(<ColumnContent column={column} value={`Formatted ${column.field.format}の値！`} />);

    expect(result.container).toMatchSnapshot();
  }
);
