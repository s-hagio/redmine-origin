import * as React from 'react';
import { ProjectResourceIdentifier } from '@/models/coreResource';
import { Project } from '@/models/project';
import { DataColumn, getProjectColumnContent } from '../../models/column';
import Column, { Cell, ColumnContent } from '../Column';

type Props = {
  rowId: ProjectResourceIdentifier;
  project: Project;
  column: DataColumn;
};

const ProjectColumn: React.FC<Props> = ({ rowId, project, column }) => {
  return (
    <Column key={column.name} rowId={rowId} name={column.name}>
      <Cell>
        <ColumnContent column={column} value={getProjectColumnContent(column, project)} />
      </Cell>
    </Column>
  );
};

export default React.memo(ProjectColumn);
