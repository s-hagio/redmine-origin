import * as React from 'react';
import { useAssignableIssueStatuses } from '@/models/fieldValue';
import { FieldProps } from './types';
import AssociatedResourceFieldInput from './AssociatedResourceFieldInput';

const IssueStatusFieldInput: React.FC<FieldProps> = ({ issue, ...props }) => {
  const statuses = useAssignableIssueStatuses(issue);

  return <AssociatedResourceFieldInput {...props} issue={issue} resources={statuses} />;
};

export default IssueStatusFieldInput;
