import * as React from 'react';
import { css } from '@linaria/core';
import { Icon } from '@/components/Icon';
import { NakedButton } from '@/components/Button';
import { ColumnListType, useColumnListVisibilityState } from '../../../models/column';

const AllColumnToggleButton: React.FC = () => {
  const [isVisible, setVisible] = useColumnListVisibilityState(ColumnListType.All);

  const handleClick = React.useCallback(() => {
    setVisible(!isVisible);
  }, [isVisible, setVisible]);

  return (
    <NakedButton className={style} type="button" onClick={handleClick}>
      <Icon name="start" deg={isVisible ? 180 : 0} />
    </NakedButton>
  );
};

export default AllColumnToggleButton;

export const style = css``;
