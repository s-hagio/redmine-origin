import * as React from 'react';
import { renderWithTranslation } from '@/test-utils';
import { createdOnFieldDefinition, intCustomFieldDefinition, trackerFieldDefinition } from '@/__fixtures__';
import GroupLabel from '../GroupLabel';

const useGroupFieldMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/group', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useGroupField: useGroupFieldMock,
}));

vi.mock('@/features/field', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  FieldContent: vi.fn((props) => `FieldContent: ${JSON.stringify(props)}`),
}));

test('Render: coreField', () => {
  useGroupFieldMock.mockReturnValue(trackerFieldDefinition);

  const { container } = renderWithTranslation(<GroupLabel value={1} />);

  expect(container).toMatchSnapshot();
});

test('Render: DateTime field', () => {
  useGroupFieldMock.mockReturnValue(createdOnFieldDefinition);

  const { container } = renderWithTranslation(<GroupLabel value="2024-01-20T05:40:17.965Z" />);

  expect(container).toMatchSnapshot();
});

test('Render: customField', () => {
  useGroupFieldMock.mockReturnValue(intCustomFieldDefinition);

  const { container } = renderWithTranslation(<GroupLabel value={456} />);

  expect(container).toMatchSnapshot();
});

test('Render: null value', () => {
  useGroupFieldMock.mockReturnValue(intCustomFieldDefinition);

  const { container } = renderWithTranslation(<GroupLabel value={null} />);

  expect(container.textContent).toBe('(空)');
});

test('Render: blank string value', () => {
  useGroupFieldMock.mockReturnValue(intCustomFieldDefinition);

  const { container } = renderWithTranslation(<GroupLabel value="" />);

  expect(container.textContent).toBe('(空)');
});
