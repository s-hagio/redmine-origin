import * as React from 'react';
import { render } from '@testing-library/react';
import { useAvailableColumnMap, useSelectedColumnNamesState } from '../../../../models/column';
import {
  descriptionColumn,
  idColumn,
  issueSubjectColumn,
  parentSubjectColumn,
  startDateColumn,
  stringCustomFieldColumn,
  trackerColumn,
} from '../../../../__fixtures__';
import AvailableColumnList from '../AvailableColumnList';

vi.mock('../../../../models/column', () => ({
  useAvailableColumnMap: vi.fn(),
  useSelectedColumnNamesState: vi.fn(),
}));

vi.mock('../Item', () => ({
  default: vi.fn(({ column, ...props }) => `Item: ${column.name} ${JSON.stringify(props)}`),
}));

beforeEach(() => {
  vi.mocked(useAvailableColumnMap).mockReturnValue(
    new Map([
      [idColumn.name, idColumn],
      [trackerColumn.name, trackerColumn],
      [issueSubjectColumn.name, issueSubjectColumn],
      [parentSubjectColumn.name, parentSubjectColumn],
      [startDateColumn.name, startDateColumn],
      [descriptionColumn.name, descriptionColumn],
      [stringCustomFieldColumn.name, stringCustomFieldColumn],
    ])
  );
});

test('Render', () => {
  vi.mocked(useSelectedColumnNamesState).mockReturnValue([
    [idColumn.name, trackerColumn.name, issueSubjectColumn.name, startDateColumn.name],
    vi.fn(),
  ]);

  const { container } = render(<AvailableColumnList />);

  expect(container).toMatchSnapshot();
});
