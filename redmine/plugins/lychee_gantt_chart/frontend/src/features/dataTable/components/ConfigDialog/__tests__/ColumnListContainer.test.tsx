import * as React from 'react';
import { render } from '@testing-library/react';
import ColumnListContainer from '../ColumnListContainer';

vi.mock('../AvailableColumnList', () => ({
  default: vi.fn((props) => `AvailableColumnList: ${JSON.stringify(props)}`),
}));

vi.mock('../SelectedColumnList', () => ({
  default: vi.fn((props) => `SelectedColumnList: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<ColumnListContainer />);

  expect(container).toMatchSnapshot();
});
