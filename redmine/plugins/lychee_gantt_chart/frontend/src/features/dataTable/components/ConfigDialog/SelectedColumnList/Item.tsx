import * as React from 'react';
import { css, cx } from '@linaria/core';
import { CSS } from '@dnd-kit/utilities';
import { useSortable } from '@/lib/dnd';
import { NakedButton } from '@/components/Button';
import { Icon } from '@/components/Icon';
import LabelText, { style as labelTextStyle } from '../LabelText';
import { DataColumn, isSubjectColumn, useToggleSelectedColumn } from '../../../models/column';

type Props = {
  column: DataColumn;
};

const Item: React.FC<Props> = ({ column }) => {
  const toggleSelectedColumn = useToggleSelectedColumn();

  const { isDragging, isSorting, listeners, setNodeRef, transform, transition } = useSortable({
    id: column.name,
  });

  const isSubject = isSubjectColumn(column);
  const cssProps: React.CSSProperties = {};

  if (!isSubject) {
    Object.assign(cssProps, {
      transform: CSS.Transform.toString(transform),
      transition,
    });
  }

  const handleClick = React.useCallback(() => {
    toggleSelectedColumn(column.name);
  }, [column.name, toggleSelectedColumn]);

  const isRemovable = !isSubject && !isSorting;

  return (
    <li
      ref={setNodeRef}
      style={cssProps}
      className={cx(style, isDragging && draggingStyle)}
      data-draggable={!isSubject}
      {...listeners}
    >
      <LabelText>{column.field.label}</LabelText>

      {isRemovable && (
        <NakedButton type="button" onClick={handleClick}>
          <Icon name="remove" size={12} />
        </NakedButton>
      )}
    </li>
  );
};

export default React.memo(Item);

const style = css`
  display: flex;
  align-items: center;
  width: 9rem;
  height: 1.75rem;
  padding: 0 0.15rem 0 0.45rem;
  border: 1px dashed #999;
  background: #fff;
  white-space: nowrap;
  cursor: default;

  :not(:first-of-type) {
    margin-top: 0.25rem;
  }

  &[data-draggable='true'] {
    cursor: grab;
  }

  &[data-draggable='false'] {
    border-color: #c0c0c0;
    cursor: not-allowed;
  }

  .${labelTextStyle} {
    margin-right: 0.85rem;
  }

  button {
    display: inline-flex;
    align-items: center;
    margin-left: auto;
    visibility: hidden;
  }

  :hover button {
    visibility: visible;
  }
`;

const draggingStyle = css`
  border-color: #dedede;
  color: transparent;
`;
