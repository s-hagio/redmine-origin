import * as React from 'react';
import { Issue } from '@/models/issue';
import { IssueResourceIdentifier } from '@/models/coreResource';
import Column, { Cell, ColumnContent } from '../Column';
import { getIssueColumnContent, DataColumn } from '../../models/column';
import { EditableCell } from '../EditableCell';
import { useCellSelectedStatusState } from '../../models/cell';

type Props = {
  rowId: IssueResourceIdentifier;
  issue: Issue;
  column: DataColumn;
};

const IssueColumn: React.FC<Props> = ({ rowId, issue, column }) => {
  const status = useCellSelectedStatusState(rowId, column.name);

  return (
    <Column rowId={rowId} name={column.name}>
      <Cell>
        <ColumnContent column={column} value={getIssueColumnContent(column, issue)} />
      </Cell>

      {status?.isEditing && <EditableCell status={status} />}
    </Column>
  );
};

export default React.memo(IssueColumn);
