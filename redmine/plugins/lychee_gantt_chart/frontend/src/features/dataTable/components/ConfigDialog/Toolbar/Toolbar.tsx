import * as React from 'react';
import { css } from '@linaria/core';
import { NakedButton } from '@/components/Button';
import { Icon } from '@/components/Icon';
import { useCloseConfigDialog } from '../../../hooks';
import DefaultColumnsSelectButton from './DefaultColumnsSelectButton';

const Toolbar: React.FC = () => {
  const close = useCloseConfigDialog();

  return (
    <div className={style}>
      <DefaultColumnsSelectButton />

      <div className={rightButtonListStyle}>
        <NakedButton type="button" onClick={close}>
          <Icon name="close" size={24} />
        </NakedButton>
      </div>
    </div>
  );
};

export default Toolbar;

export const style = css`
  display: flex;

  button {
    font-size: 0.75rem;
  }
`;

const rightButtonListStyle = css`
  margin-left: auto;
`;
