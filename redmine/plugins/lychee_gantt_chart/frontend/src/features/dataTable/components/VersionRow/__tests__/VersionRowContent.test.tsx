import * as React from 'react';
import { render } from '@testing-library/react';
import { Version } from '@/models/version';
import { mainProjectVersionRow, versionSharedByHierarchy } from '@/__fixtures__';
import VersionRowContent from '../VersionRowContent';
import { DataColumn } from '../../../models/column';

vi.mock('../VersionSubjectColumn', () => ({
  default: vi.fn((props) => `VersionSubjectColumn: ${JSON.stringify(props)}`),
}));

vi.mock('../VersionColumn', () => ({
  default: vi.fn((props) => `VersionColumn: ${JSON.stringify(props)}`),
}));

const version = { id: versionSharedByHierarchy.id, name: versionSharedByHierarchy.name } as Version;
const columns = [{ name: 'startDate' }, { name: 'description' }] as DataColumn[];

test('Render', () => {
  const { container } = render(
    <VersionRowContent {...mainProjectVersionRow} columns={columns} version={version} />
  );

  expect(container).toMatchSnapshot();
});
