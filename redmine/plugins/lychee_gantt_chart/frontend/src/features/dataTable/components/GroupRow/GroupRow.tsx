import * as React from 'react';
import { useGroup } from '@/models/group';
import Row from '../Row';
import GroupRowContent from './GroupRowContent';

type Props = Omit<React.ComponentProps<typeof GroupRowContent>, 'group'>;

const GroupRow: React.FC<Props> = ({ id, resourceId, ...props }) => {
  const group = useGroup(resourceId);

  if (!group) {
    return null;
  }

  return (
    <Row id={id}>
      <GroupRowContent id={id} resourceId={resourceId} group={group} {...props} />
    </Row>
  );
};

export default React.memo(GroupRow);
