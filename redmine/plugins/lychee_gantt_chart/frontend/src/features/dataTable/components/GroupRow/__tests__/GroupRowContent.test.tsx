import * as React from 'react';
import { render } from '@testing-library/react';
import { newStatusGroup, newStatusGroupRow } from '@/__fixtures__';
import { descriptionColumn, startDateColumn } from '../../../__fixtures__';
import GroupRowContent from '../GroupRowContent';

vi.mock('../../Column', () => ({
  default: vi.fn(({ children, ...props }) => React.createElement('column-mock', props, children)),
  SubjectColumn: vi.fn((props) => React.createElement('subject-column', props)),
}));

vi.mock('../GroupLabel', () => ({
  default: vi.fn((props) => `GroupLabel: ${JSON.stringify(props)}`),
}));

const group = newStatusGroup;
const columns = [startDateColumn, descriptionColumn];

test('Render', () => {
  const { container } = render(<GroupRowContent {...newStatusGroupRow} columns={columns} group={group} />);

  expect(container).toMatchSnapshot();
});
