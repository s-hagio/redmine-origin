import * as React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithTranslation } from '@/test-utils';
import { isPrivateFieldDefinition } from '@/__fixtures__';
import { basicProps } from '../__fixtures__';
import BoolFieldInput from '../BoolFieldInput';

const props = {
  ...basicProps,
  field: isPrivateFieldDefinition,
  value: false,
};

test('真偽値の選択肢を表示する', () => {
  const { container } = renderWithTranslation(<BoolFieldInput {...props} />);

  expect(container).toMatchSnapshot();
});

test('選択時にonCommitが呼ばれる', () => {
  const handleCommit = vi.fn();
  const { getByText } = renderWithTranslation(<BoolFieldInput {...props} onCommit={handleCommit} />);

  fireEvent.click(getByText('はい'));
  expect(handleCommit).toBeCalledWith(true);

  fireEvent.click(getByText('いいえ'));
  expect(handleCommit).toBeCalledWith(false);
});

test('asNumericStringがtrueの場合、選択肢の値を数値文字列にする', () => {
  const handleCommit = vi.fn();
  const { getByText } = renderWithTranslation(
    <BoolFieldInput {...props} asNumericString onCommit={handleCommit} />
  );

  fireEvent.click(getByText('はい'));
  expect(handleCommit).toBeCalledWith('1');

  fireEvent.click(getByText('いいえ'));
  expect(handleCommit).toBeCalledWith('0');
});
