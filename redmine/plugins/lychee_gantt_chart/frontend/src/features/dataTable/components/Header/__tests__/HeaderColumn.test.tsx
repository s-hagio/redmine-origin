import * as React from 'react';
import { render } from '@testing-library/react';
import HeaderColumn from '../HeaderColumn';
import { descriptionColumn } from '../../../__fixtures__';

const useSortStatusMock = vi.hoisted(() => vi.fn());
const selectSortColumnMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/sort', () => ({
  useSortStatus: useSortStatusMock,
  useSelectSortColumn: vi.fn().mockReturnValue(selectSortColumnMock),
  SortDirectionIndicator: vi.fn((props) => `SortDirectionIndicator: ${JSON.stringify(props)}`),
}));

vi.mock('../../ColumnResizer', () => ({
  ColumnResizeHandle: vi.fn(
    ({ column, ...props }) => `ColumnResizeHandle: ${column.name} ${JSON.stringify(props)}`
  ),
}));

vi.mock('../../Column', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn(({ children, ...props }) => React.createElement('column-mock', props, children)),
  Cell: vi.fn((props) => `Cell: ${JSON.stringify(props)}`),
}));

describe('isSortable: false', () => {
  beforeEach(() => {
    useSortStatusMock.mockReturnValue({
      isSortable: false,
    });
  });

  test('Render', () => {
    const result = render(<HeaderColumn column={descriptionColumn} />);

    expect(result.container).toMatchSnapshot();
  });

  test.todo('要素のクリックでselectColumnを実行しない');
});

describe('isSortable: true', () => {
  beforeEach(() => {
    useSortStatusMock.mockReturnValue({ isSortable: true, isSelected: true, direction: 'asc' });
  });

  test('Render', () => {
    const result = render(<HeaderColumn column={descriptionColumn} />);

    expect(result.container).toMatchSnapshot();
  });

  test.todo('要素のクリックでselectColumnを実行する');
  test.todo('AutoAdjusterのダブルクリック時にはselectColumnを実行しない');
});
