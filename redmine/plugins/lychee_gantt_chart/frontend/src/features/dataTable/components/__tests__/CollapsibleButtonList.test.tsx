import * as React from 'react';
import { render } from '@testing-library/react';
import { beforeEach } from 'vitest';
import { grandchildIssueRow, mainProjectRow, rootIssueRow } from '@/__fixtures__';
import CollapsibleButtonList from '../CollapsibleButtonList';

const useVisibleRowsMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useVisibleRows: useVisibleRowsMock,
  ToggleRowCollapseButton: vi.fn((props) => `ToggleRowCollapseButton: ${JSON.stringify(props)}`),
}));

vi.mock('../Row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn(({ children, ...props }) => React.createElement('row-mock', props, children)),
}));

beforeEach(() => {
  useVisibleRowsMock.mockReturnValue([mainProjectRow, rootIssueRow, grandchildIssueRow]);
});

test('Render', () => {
  const { container } = render(<CollapsibleButtonList />);

  expect(container).toMatchSnapshot();
});
