import * as React from 'react';
import { render } from '@testing-library/react';
import { startDateColumn, stringCustomFieldColumn, subjectColumn } from '../../../__fixtures__';
import { useOptionalColumns, useSubjectColumn } from '../../../models/column';
import ColumnList from '../ColumnList';

vi.mock('../../../models/column', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useSubjectColumn: vi.fn(),
  useOptionalColumns: vi.fn(),
}));

vi.mock('../../Column', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn(({ children, ...props }) => React.createElement('column-mock', props, children)),
}));

vi.mock('../HeaderColumn', () => ({
  default: vi.fn(({ column, ...props }) => `HeaderColumn: ${column.name} ${JSON.stringify(props)}`),
}));

vi.mock('../OptionalColumn', () => ({
  default: vi.fn(({ column, ...props }) => `OptionalColumn: ${column.name} ${JSON.stringify(props)}`),
}));

beforeEach(() => {
  vi.mocked(useSubjectColumn).mockReturnValue(subjectColumn);
  vi.mocked(useOptionalColumns).mockReturnValue([startDateColumn, stringCustomFieldColumn]);
});

test('Render', () => {
  const { container } = render(<ColumnList />);

  expect(container).toMatchSnapshot();
});
