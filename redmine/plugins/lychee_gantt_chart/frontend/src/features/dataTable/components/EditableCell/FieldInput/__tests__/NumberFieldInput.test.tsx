import * as React from 'react';
import { render } from '@testing-library/react';
import { estimatedHoursFieldDefinition } from '@/__fixtures__';
import NumberFieldInput from '../NumberFieldInput';
import { basicProps } from '../__fixtures__';
import { FieldProps } from '../types';

const CellBasicInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../CellBasicInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    CellBasicInputSpy(props);
    return React.createElement('cell-basic-input', null, children);
  }),
}));

const props: FieldProps = {
  ...basicProps,
  value: 999,
  field: estimatedHoursFieldDefinition,
  isRequired: true,
};

test('CellBasicInputを表示', () => {
  const { container } = render(<NumberFieldInput {...props} />);

  expect(CellBasicInputSpy).toBeCalledWith({
    ...props,
    type: 'tel',
  });
  expect(container).toMatchSnapshot();
});
