import * as React from 'react';
import { render } from '@testing-library/react';
import DataTable from '../DataTable';
import { useIsDataTableVisible } from '../../models/column';

vi.mock('@/features/row', () => ({
  useTotalRowHeight: vi.fn().mockReturnValue(100),
}));

vi.mock('../../models/width', () => ({
  useTotalColumnWidth: vi.fn().mockReturnValue(200),
}));

vi.mock('../../models/column', () => ({
  useIsDataTableVisible: vi.fn().mockReturnValue(true),
}));

vi.mock('../RowList', () => ({
  default: vi.fn((props) => `RowList: ${JSON.stringify(props)}`),
}));

vi.mock('../CollapsibleButtonList', () => ({
  default: vi.fn((props) => `CollapsibleButtonList: ${JSON.stringify(props)}`),
}));

vi.mock('../Column/ColumnStyles', () => ({
  default: vi.fn((props) => `ColumnStyles: ${JSON.stringify(props)}`),
}));

vi.mock('../RowNumber/RowNumberColumnWidthComputer', () => ({
  default: vi.fn((props) => `RowNumberColumnWidthComputer: ${JSON.stringify(props)}`),
}));

vi.mock('../ColumnResizer', () => ({
  ColumnSizeAutoAdjuster: vi.fn((props) => `ColumnSizeAutoAdjuster: ${JSON.stringify(props)}`),
  LastColumnResizeHandle: vi.fn((props) => `LastColumnResizeHandle: ${JSON.stringify(props)}`),
}));

vi.mock('../SelectableCell', () => ({
  SelectableCellContainer: vi.fn((props) => `SelectableCellContainer: ${JSON.stringify(props)}`),
}));

test('データ列が表示されている時のレンダリング', () => {
  vi.mocked(useIsDataTableVisible).mockReturnValue(true);

  const result = render(<DataTable />);

  expect(result.container).toMatchSnapshot();
});

test('データ列が表示されていない時のレンダリング', () => {
  vi.mocked(useIsDataTableVisible).mockReturnValue(false);

  const result = render(<DataTable />);

  expect(result.container).toMatchSnapshot();
});
