import * as React from 'react';
import { render } from '@testing-library/react';
import { rootIssueRow } from '@/__fixtures__';
import Row from '../Row';

vi.mock('@/features/row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  RowComponent: vi.fn(({ children, ...props }) => {
    return React.createElement('row-component', props, children);
  }),
}));

vi.mock('../RowNumber', () => ({
  RowNumberColumn: vi.fn((props) => `RowNumberColumn: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(
    <Row id={rootIssueRow.id} className="fooClass">
      CHILD!
    </Row>
  );

  expect(container).toMatchSnapshot();
});
