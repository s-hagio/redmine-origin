import * as React from 'react';
import { css } from '@linaria/core';
import { Dialog } from '@/features/dialog';
import { borderColor } from '@/styles';
import { CONFIG_DIALOG_ID } from '../../constants';
import Toolbar, { toolbarStyle } from './Toolbar';
import ColumnListContainer from './ColumnListContainer';

const ConfigDialog: React.FC = () => {
  return (
    <Dialog className={style} id={CONFIG_DIALOG_ID}>
      <Toolbar />
      <ColumnListContainer />
    </Dialog>
  );
};

export default React.memo(ConfigDialog);

const style = css`
  position: absolute;
  top: 3px;
  left: 3px;
  padding: 1.25rem;
  border: 1px solid ${borderColor};
  border-radius: 4px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);
  background: #fff;
  z-index: 100;

  .${toolbarStyle} {
    margin-bottom: 1rem;
  }
`;
