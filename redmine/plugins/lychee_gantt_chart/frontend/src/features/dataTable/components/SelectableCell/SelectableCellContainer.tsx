import * as React from 'react';
import SelectedCellKeyboardNavigator from './SelectableCellKeyboardNavigator';
import SelectedCellDeselector from './SelectableCellDeselector';

type Props = {
  containerRef: React.RefObject<HTMLElement>;
};

const SelectableCellContainer: React.FC<Props> = ({ containerRef }) => {
  return (
    <>
      <SelectedCellKeyboardNavigator />
      <SelectedCellDeselector containerRef={containerRef} />
    </>
  );
};

export default SelectableCellContainer;
