import * as React from 'react';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act, fireEvent } from '@testing-library/react';
import { renderWithRecoilHook } from '@/test-utils';
import OptionalColumnToggleButton from '../OptionalColumnToggleButton';
import { columnVisibilityState } from '../../../../models/column/states';
import { ColumnListType } from '../../../../models/column';

test('Render', () => {
  const { result, container } = renderWithRecoilHook(
    <OptionalColumnToggleButton />,
    () => ({
      setVisible: useSetRecoilState(columnVisibilityState(ColumnListType.Optional)),
    }),
    {
      initializeState: ({ set }) => {
        set(columnVisibilityState(ColumnListType.Optional), true);
      },
    }
  );

  expect(container).toMatchSnapshot('展開時');

  act(() => result.current.setVisible(false));

  expect(container).toMatchSnapshot('縮小時');
});

test('クリックでオプションデータ列の表示をトグル', () => {
  const { result, container } = renderWithRecoilHook(
    <OptionalColumnToggleButton />,
    () => ({
      isVisible: useRecoilValue(columnVisibilityState(ColumnListType.Optional)),
    }),
    {
      initializeState: ({ set }) => {
        set(columnVisibilityState(ColumnListType.Optional), true);
      },
    }
  );

  const button = container.querySelector('button') as HTMLButtonElement;

  expect(result.current.isVisible).toBe(true);

  fireEvent.click(button);
  expect(result.current.isVisible).toBe(false);

  fireEvent.click(button);
  expect(result.current.isVisible).toBe(true);
});
