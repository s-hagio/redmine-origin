import * as React from 'react';
import { css } from '@linaria/core';

type Props = {
  number: number;
};

const RowNumberColumnContent: React.FC<Props> = ({ number }) => {
  return <div className={style}>{number}</div>;
};

export default RowNumberColumnContent;

const style = css`
  font-size: 10px;
  padding: 0 0.35rem;
`;
