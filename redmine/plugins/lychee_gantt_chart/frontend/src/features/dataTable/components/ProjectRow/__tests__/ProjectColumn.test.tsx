import * as React from 'react';
import { render } from '@testing-library/react';
import { mainProject, rootProjectRow } from '@/__fixtures__';
import { startDateColumn } from '../../../__fixtures__';
import ProjectColumn from '../ProjectColumn';

vi.mock('../../Column', async () => ({
  ...(await vi.importActual<object>('../../Column')),
  default: vi.fn(({ children, ...props }) => React.createElement('column-mock', props, children)),
  Cell: vi.fn(({ children, ...props }) => React.createElement('cell-mock', props, children)),
  ColumnContent: vi.fn(({ column, ...props }) => `ColumnContent: ${column.name} ${JSON.stringify(props)}`),
}));

const project = {
  ...mainProject,
  startDate: '2022-01-01',
};

test('Render', () => {
  const result = render(
    <ProjectColumn rowId={rootProjectRow.id} column={startDateColumn} project={project} />
  );

  expect(result.container).toMatchSnapshot();
});
