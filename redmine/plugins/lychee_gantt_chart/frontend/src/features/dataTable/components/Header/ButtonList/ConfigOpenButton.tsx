import * as React from 'react';
import { css } from '@linaria/core';
import { NakedButton } from '@/components/Button';
import { Icon } from '@/components/Icon';
import { useOpenConfigDialog } from '../../../hooks';

const ConfigOpenButton: React.FC = () => {
  const open = useOpenConfigDialog();

  return (
    <NakedButton className={style} type="button" onClick={open}>
      <Icon name="tune" />
    </NakedButton>
  );
};

export default ConfigOpenButton;

const style = css``;
