import * as React from 'react';
import { render } from '@testing-library/react';
import { newStatusGroup, newStatusGroupRow } from '@/__fixtures__';
import { startDateColumn } from '../../../__fixtures__';
import GroupRow from '../GroupRow';

const useGroupMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/group', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useGroup: useGroupMock,
}));

vi.mock('../../Row', () => ({
  default: vi.fn((props) => React.createElement('row-mock', props)),
}));

const GroupRowContentSpy = vi.hoisted(() => vi.fn());

vi.mock('../GroupRowContent', () => ({
  default: vi.fn((props) => {
    GroupRowContentSpy(props);
    return `GroupRowContent`;
  }),
}));

const group = newStatusGroup;
const rowProps = { ...newStatusGroupRow, columns: [startDateColumn] };

beforeEach(() => {
  useGroupMock.mockReturnValue(group);
});

test('Render', () => {
  const { container } = render(<GroupRow {...rowProps} />);

  expect(container).toMatchSnapshot();
  expect(GroupRowContentSpy).toBeCalledWith({ ...rowProps, group });
});

describe('詳細データがない場合', () => {
  beforeEach(() => {
    useGroupMock.mockReturnValueOnce(null);
  });

  test('レンダリングをスキップ', () => {
    const { container } = render(<GroupRow {...rowProps} />);

    expect(container.innerHTML).toBe('');
  });
});
