import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import ConfigOpenButton from '../ConfigOpenButton';
import { useOpenConfigDialog } from '../../../../hooks';

vi.mock('../../../../hooks', () => ({
  useOpenConfigDialog: vi.fn(),
}));

test('Render', () => {
  const { container } = render(<ConfigOpenButton />);

  expect(container).toMatchSnapshot();
});

test('クリックでConfigDialogを開く', () => {
  const openConfigDialogMock = vi.fn();
  vi.mocked(useOpenConfigDialog).mockReturnValue(openConfigDialogMock);

  const { container } = render(<ConfigOpenButton />);
  const el = container.querySelector('button') as HTMLButtonElement;

  expect(openConfigDialogMock).not.toHaveBeenCalled();
  fireEvent.click(el);
  expect(openConfigDialogMock).toHaveBeenCalled();
});
