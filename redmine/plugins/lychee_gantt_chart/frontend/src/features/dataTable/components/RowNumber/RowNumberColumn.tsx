import * as React from 'react';
import { css } from '@linaria/core';
import { RowID, useRowNumber, useSelectRow } from '@/features/row';
import { lightBorderColor } from '@/styles';
import { ROW_NUMBER_COLUMN_NAME } from '../../models/column';
import { style as rowStyle } from '../Row';
import { useDeselectCell } from '../../models/cell';
import RowNumberColumnContent from './RowNumberColumnContent';

type Props = {
  rowId: RowID;
};

const RowNumberColumn: React.FC<Props> = ({ rowId }) => {
  const selectRow = useSelectRow();
  const deselectCell = useDeselectCell();
  const number = useRowNumber(rowId);

  const handleMouseDown = React.useCallback(() => {
    selectRow(rowId);
    deselectCell();
  }, [selectRow, deselectCell, rowId]);

  return (
    <div className={style} data-column={ROW_NUMBER_COLUMN_NAME} onMouseDown={handleMouseDown}>
      <RowNumberColumnContent number={number} />
    </div>
  );
};

export default React.memo(RowNumberColumn);

const style = css`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  user-select: none;

  .${rowStyle}:not(:last-child) & {
    border-bottom: 1px solid ${lightBorderColor};
  }

  ::after {
    position: absolute;
    top: 0;
    bottom: -1px;
    right: -1px;
    content: '';
    border-right: 1px solid ${lightBorderColor};
  }
`;
