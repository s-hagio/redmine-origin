import * as React from 'react';
import { css } from '@linaria/core';
import { horizontalListSortingStrategy } from '@dnd-kit/sortable';
import { DragOverlay } from '@dnd-kit/core';
import { DragStartEvent } from '@/lib/dnd';
import { lightBorderColor } from '@/styles';
import { DataColumn, useSelectedColumns } from '../../../models/column';
import { ColumnSortableContext } from '../../Column';
import Item from './Item';

const SelectedColumnList: React.FC = () => {
  const columns = useSelectedColumns();
  const [draggingColumn, setDraggingColumn] = React.useState<DataColumn | null>(null);

  const handleDragStart = React.useCallback(
    (event: DragStartEvent) => {
      if (event.active?.id) {
        setDraggingColumn(columns.find((column) => column.name === event.active.id) ?? null);
      }
    },
    [columns]
  );

  const unsetDraggingColumn = React.useCallback(() => {
    setDraggingColumn(null);
  }, []);

  return (
    <ColumnSortableContext
      columns={columns}
      strategy={horizontalListSortingStrategy}
      onDragStart={handleDragStart}
      onDragEnd={unsetDraggingColumn}
      onDragCancel={unsetDraggingColumn}
    >
      <ul className={style}>
        {columns.map((column) => (
          <Item key={column.name} column={column} />
        ))}
      </ul>

      <DragOverlay>{draggingColumn && <Item column={draggingColumn} />}</DragOverlay>
    </ColumnSortableContext>
  );
};

export default SelectedColumnList;

const style = css`
  width: 10rem;
  margin: -0.5rem 0 0;
  padding: 0.5rem 0 0.5rem 1rem;
  border-left: 2px solid ${lightBorderColor};
`;
