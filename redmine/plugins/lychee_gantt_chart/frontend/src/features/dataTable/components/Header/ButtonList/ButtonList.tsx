import * as React from 'react';
import { css } from '@linaria/core';
import { useIsDataTableVisible } from '../../../models/column';
import AllColumnToggleButton from './AllColumnToggleButton';
import OptionalColumnToggleButton from './OptionalColumnToggleButton';
import ConfigOpenButton from './ConfigOpenButton';

const ButtonList: React.FC = () => {
  const isDataTableVisible = useIsDataTableVisible();

  return (
    <div className={style}>
      <div className={leftButtonsStyle}>{isDataTableVisible && <ConfigOpenButton />}</div>

      <div className={rightButtonsStyle}>
        {isDataTableVisible && <OptionalColumnToggleButton />}
        <AllColumnToggleButton />
      </div>
    </div>
  );
};

export default React.memo(ButtonList);

export const style = css`
  display: flex;
  align-items: center;

  > div {
    display: flex;
  }

  button {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const leftButtonsStyle = css`
  button {
    margin-left: 3px;
  }
`;

const rightButtonsStyle = css`
  margin-left: auto;

  button {
    margin-right: 3px;
  }
`;
