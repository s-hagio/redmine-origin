import * as React from 'react';
import { useDeselectCell, useSelectedCell } from '../../models/cell';

type Props = {
  containerRef: React.RefObject<HTMLElement>;
};

const SelectableCellDeselector: React.FC<Props> = ({ containerRef }) => {
  const deselectCell = useDeselectCell();
  const selectedCell = useSelectedCell();

  React.useEffect(() => {
    const el = containerRef.current;

    if (!el) {
      return;
    }

    const handleClick = (event: MouseEvent) => {
      if (!event.composedPath().includes(el) && !selectedCell?.isEditing) {
        deselectCell();
      }
    };

    document.addEventListener('click', handleClick);

    return () => {
      document.removeEventListener('click', handleClick);
    };
  }, [containerRef, deselectCell, selectedCell]);

  return null;
};

export default React.memo(SelectableCellDeselector);
