import * as React from 'react';
import { useAssignableIssueUsers } from '@/models/fieldValue';
import { FieldProps } from './types';
import AssociatedResourceFieldInput from './AssociatedResourceFieldInput';

const AssignedToFieldInput: React.FC<FieldProps> = ({ issue, ...props }) => {
  const users = useAssignableIssueUsers(issue);

  return <AssociatedResourceFieldInput {...props} issue={issue} resources={users} />;
};

export default AssignedToFieldInput;
