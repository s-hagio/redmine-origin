import * as React from 'react';
import { css, cx } from '@linaria/core';
import { RowComponent, RowID } from '@/features/row';
import { RowNumberColumn } from './RowNumber';

type Props = React.PropsWithChildren<{
  id: RowID;
  className?: string | undefined;
}>;

const Row: React.FC<Props> = ({ id, className, children }) => {
  return (
    <RowComponent className={cx(style, className)} rowId={id}>
      <RowNumberColumn rowId={id} />

      {children}
    </RowComponent>
  );
};

export default Row;

export const style = css`
  position: absolute;
  display: flex;
  align-items: center;

  :first-of-type {
    margin-top: 0;
    border-top: none;
  }
`;
