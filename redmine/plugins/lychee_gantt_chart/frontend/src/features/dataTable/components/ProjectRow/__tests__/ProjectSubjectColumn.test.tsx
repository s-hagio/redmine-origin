import * as React from 'react';
import { render } from '@testing-library/react';
import { useProjectURL } from '@/models/project';
import { rootProject, rootProjectRow } from '@/__fixtures__';
import ProjectSubjectColumn from '../ProjectSubjectColumn';

vi.mock('@/models/project', async () => ({
  ...(await vi.importActual<object>('@/models/project')),
  useProjectURL: vi.fn(),
}));

vi.mock('@/components/Icon', () => ({
  RedmineIcon: vi.fn((props) => `RedmineIcon: ${JSON.stringify(props)}`),
}));

vi.mock('../../Column', () => ({
  SubjectColumn: vi.fn(({ children, ...props }) => React.createElement('subject-column', props, children)),
}));

beforeEach(() => {
  vi.mocked(useProjectURL).mockReturnValue(`/projects/${rootProject.id}`);
});

test('Render', () => {
  const result = render(
    <ProjectSubjectColumn rowId={rootProjectRow.id} project={rootProject} depth={3} isCollapsible={false} />
  );

  expect(result.container).toMatchSnapshot();
  expect(useProjectURL).toHaveBeenCalledWith(rootProject.identifier);
});
