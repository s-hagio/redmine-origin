import * as React from 'react';
import { css, cx } from '@linaria/core';
import { autoUpdate, flip, useFloating } from '@floating-ui/react';
import { useToggleSelectedCellEdit } from '../../../../models/cell';
import { FieldProps } from '../types';
import { EventHandlers, OptionItem } from './types';
import SingleItem from './SingleItem';
import MultipleItem from './MultipleItem';

type Value = OptionItem['value'];

type Props = FieldProps &
  EventHandlers & {
    options: OptionItem[];
    className?: string;
    isMultiple?: boolean;
  };

const noneOption: OptionItem = {
  id: '__none__',
  label: '',
  value: '',
};

const ListFieldInput: React.FC<Props> = ({
  cellRef,
  options,
  value,
  onChange,
  onCommit,
  isRequired,
  isMultiple,
  className,
}) => {
  const { refs, floatingStyles, isPositioned } = useFloating({
    placement: 'bottom-start',
    whileElementsMounted: autoUpdate,
    middleware: [flip()],
  });
  const ref = React.useRef<HTMLDivElement>(null);

  React.useEffect(() => {
    refs.setReference(cellRef.current);
    refs.setFloating(ref.current);
  }, [cellRef, refs]);

  const items = React.useMemo<OptionItem[]>(() => {
    const items = options.map((item) => ({
      ...item,
      id: `${item.label}-${item.value}`,
    }));

    if (!isRequired && !isMultiple) {
      items.unshift(noneOption);
    }

    const ids = new Set();

    return items.filter(({ id }) => {
      if (ids.has(id)) {
        return false;
      }

      ids.add(id);
      return true;
    });
  }, [options, isRequired, isMultiple]);

  const [focusedIndex, setFocusedIndex] = React.useState<number>(
    items.findIndex((item) => item.value === value)
  );

  const handleFocus = React.useCallback(
    (item: OptionItem) => {
      const index = items.findIndex(({ id }) => id === item.id);
      setFocusedIndex(index);

      const el = ref.current;

      if (!el?.children[index]) {
        return;
      }

      el.children[index].scrollIntoView({
        block: 'nearest',
      });
    },
    [items]
  );

  const lastIndex = items.length - 1;

  const normalizeIndex = React.useCallback(
    (newIndex: number) => {
      if (newIndex < 0) {
        return lastIndex;
      }

      if (newIndex > lastIndex) {
        return 0;
      }

      return newIndex;
    },
    [lastIndex]
  );

  const [selectedValues, setSelectedValues] = React.useState<Value[]>(Array.isArray(value) ? value : []);

  const toggleSelect = React.useCallback(
    (value: Value) => {
      const newSelectedValues = selectedValues.includes(value)
        ? selectedValues.filter((v) => v !== value)
        : [...selectedValues, value];

      setSelectedValues(newSelectedValues);
      onChange(newSelectedValues);
    },
    [onChange, selectedValues]
  );

  const toggleCurrentValueSelect = React.useCallback(() => {
    focusedIndex >= 0 && toggleSelect(items[focusedIndex].value);
  }, [focusedIndex, items, toggleSelect]);

  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      event.stopPropagation();
      event.preventDefault();

      switch (event.code) {
        case 'ArrowUp':
          handleFocus(items[normalizeIndex(focusedIndex - 1)]);
          break;
        case 'ArrowDown':
          handleFocus(items[normalizeIndex(focusedIndex + 1)]);
          break;
        case 'Enter':
          if (isMultiple) {
            toggleCurrentValueSelect();
          } else {
            onCommit(items[focusedIndex]?.value ?? value);
          }
          break;
        case 'Space':
          if (isMultiple) {
            toggleCurrentValueSelect();
          }
          break;
      }
    };

    document.addEventListener('keydown', handleKeyDown);

    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [
    focusedIndex,
    handleFocus,
    isMultiple,
    items,
    normalizeIndex,
    onCommit,
    selectedValues,
    toggleCurrentValueSelect,
    toggleSelect,
    value,
  ]);

  const toggleEdit = useToggleSelectedCellEdit();

  React.useEffect(() => {
    const handleClick = (event: MouseEvent) => {
      if (!ref.current || event.composedPath().includes(ref.current)) {
        return;
      }

      toggleEdit(false);
    };

    document.addEventListener('click', handleClick);

    return () => {
      document.removeEventListener('click', handleClick);
    };
  }, [isMultiple, onChange, selectedValues, toggleEdit, value]);

  const itemBaseProps = React.useMemo(
    () => ({
      onEnter: handleFocus,
    }),
    [handleFocus]
  );

  return (
    <div ref={ref} className={cx(style, !isPositioned && hiddenStyle, className)} style={floatingStyles}>
      {items.map((item, index) => {
        const isFocused = index === focusedIndex;

        if (!isMultiple) {
          return (
            <SingleItem
              key={item.id}
              item={item}
              isFocused={isFocused}
              onClick={() => onCommit(item.value)}
              {...itemBaseProps}
            />
          );
        }

        return (
          <MultipleItem
            key={item.id}
            item={item}
            isFocused={isFocused}
            isChecked={selectedValues.includes(item.value)}
            toggle={toggleSelect}
            {...itemBaseProps}
          />
        );
      })}
    </div>
  );
};

export default ListFieldInput;

const style = css`
  position: absolute;
  top: 100%;
  left: 0;
  min-width: 100px;
  max-height: 300px;
  border-radius: 3px;
  background: #fff;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);
  overflow-y: auto;
`;

const hiddenStyle = css`
  visibility: hidden;
`;
