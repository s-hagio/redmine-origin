import * as React from 'react';
import { render } from '@testing-library/react';
import RowNumberColumnContent from '../RowNumberColumnContent';

test('Render', () => {
  const { container } = render(<RowNumberColumnContent number={999} />);

  expect(container).toMatchSnapshot();
});
