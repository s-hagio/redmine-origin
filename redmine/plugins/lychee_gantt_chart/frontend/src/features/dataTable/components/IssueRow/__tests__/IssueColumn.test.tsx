import * as React from 'react';
import { render } from '@testing-library/react';
import { rootIssue, rootIssueRow } from '@/__fixtures__';
import { descriptionColumn } from '../../../__fixtures__';
import IssueColumn from '../IssueColumn';
import { SelectedCellStatus } from '../../../models/cell';

vi.mock('../../Column', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn(({ children, ...props }) => React.createElement('column-mock', props, children)),
  Cell: vi.fn(({ children, ...props }) => React.createElement('cell-mock', props, children)),
  ColumnContent: vi.fn(({ column, ...props }) => `ColumnContent: ${column.name} ${JSON.stringify(props)}`),
}));

vi.mock('../../EditableCell', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  EditableCell: vi.fn((props) => `EditableCell: ${JSON.stringify(props)}`),
}));

const useCellSelectedStatusStateMock = vi.hoisted(() => vi.fn<never, SelectedCellStatus | null>());

vi.mock('../../../models/cell', () => ({
  useCellSelectedStatusState: useCellSelectedStatusStateMock,
}));

beforeEach(() => {
  useCellSelectedStatusStateMock.mockReturnValue(null);
});

test('Render', () => {
  const result = render(<IssueColumn rowId={rootIssueRow.id} issue={rootIssue} column={descriptionColumn} />);

  expect(result.container).toMatchSnapshot();
  expect(useCellSelectedStatusStateMock).toBeCalledWith(rootIssueRow.id, descriptionColumn.name);
});

test('編集状態ならEditableCellを表示する', () => {
  useCellSelectedStatusStateMock.mockReturnValue({
    rowId: rootIssueRow.id,
    columnName: descriptionColumn.name,
    isEditable: true,
    isEditing: true,
  });

  const result = render(<IssueColumn rowId={rootIssueRow.id} issue={rootIssue} column={descriptionColumn} />);

  expect(result.container).toMatchSnapshot();
});
