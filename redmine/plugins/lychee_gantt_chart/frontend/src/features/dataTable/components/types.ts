import { PropsWithChildren } from 'react';
import { Row, RowID } from '@/features/row';
import { DataColumn } from '../models/column';

export type RowProps<ResourceRow extends Row> = ResourceRow & {
  columns: DataColumn[];
};

export type SubjectColumnProps = PropsWithChildren<{
  rowId: RowID;
  isCollapsible: boolean;
  depth: number;
}>;
