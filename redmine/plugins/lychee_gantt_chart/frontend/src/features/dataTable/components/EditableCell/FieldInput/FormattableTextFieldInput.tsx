import * as React from 'react';
import { FieldProps } from './types';
import PlainTextFieldInput from './PlainTextFieldInput';

const FormattableTextFieldInput: React.FC<FieldProps> = ({ ...props }) => {
  return <PlainTextFieldInput {...props} />;
};

export default FormattableTextFieldInput;
