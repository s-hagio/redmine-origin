import * as React from 'react';
import { css } from '@linaria/core';
import { useAvailableColumnMap, useSelectedColumnNamesState, DataColumn } from '../../../models/column';
import Item from './Item';

const AvailableColumnList: React.FC = () => {
  const availableColumnsMap = useAvailableColumnMap();
  const availableColumnsPerSubList: Array<DataColumn[]> = React.useMemo(() => {
    const columns = [...availableColumnsMap.values()];
    const columnsPerSubList: Array<DataColumn[]> = [[], [], []];
    const numberOfColumnsPerSubList = Math.ceil(columns.length / columnsPerSubList.length);

    return columnsPerSubList.map(() => columns.splice(0, numberOfColumnsPerSubList));
  }, [availableColumnsMap]);

  const [selectedColumnNames] = useSelectedColumnNamesState();

  return (
    <>
      {availableColumnsPerSubList.map((columns, index) => (
        <ul className={style} key={index}>
          {columns.map((column) => (
            <Item key={column.name} column={column} checked={selectedColumnNames.includes(column.name)} />
          ))}
        </ul>
      ))}
    </>
  );
};

export default AvailableColumnList;

const style = css`
  margin: 0 1.5rem 0 0;
  padding: 0;
`;
