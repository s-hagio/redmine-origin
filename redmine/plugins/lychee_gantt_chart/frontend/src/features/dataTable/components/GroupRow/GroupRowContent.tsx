import * as React from 'react';
import { css } from '@linaria/core';
import { Group } from '@/models/group';
import { GroupRow } from '@/features/row';
import { RowProps } from '../types';
import Column, { SubjectColumn } from '../Column';
import GroupLabel from './GroupLabel';

type Props = RowProps<GroupRow> & {
  group: Group;
};

const GroupRowContent: React.FC<Props> = ({ id, group, isCollapsible, depth, columns }) => {
  return (
    <>
      <SubjectColumn rowId={id} isCollapsible={isCollapsible} depth={depth}>
        <span className={labelStyle}>
          <GroupLabel value={group.content} />
        </span>
        <span className={countStyle}>{group.issueIds.length}</span>
      </SubjectColumn>

      {columns.map((column) => (
        <Column key={column.name} rowId={id} name={column.name} />
      ))}
    </>
  );
};

export default React.memo(GroupRowContent);

const labelStyle = css`
  font-weight: bold;
`;

const countStyle = css`
  margin-left: 0.35rem;
  padding: 1px 6px;
  border-radius: 3px;
  background: #2c5782;
  color: #fff;
  font-size: 0.85em;
`;
