import * as React from 'react';
import { css } from '@linaria/core';
import { useTotalRowHeight } from '@/features/row';
import { useTotalColumnWidth } from '../models/width';
import { useIsDataTableVisible } from '../models/column';
import { ColumnStyles } from './Column';
import { RowNumberColumnWidthComputer } from './RowNumber';
import { ColumnSizeAutoAdjuster, LastColumnResizeHandle } from './ColumnResizer';
import RowList from './RowList';
import CollapsibleButtonList from './CollapsibleButtonList';
import { SelectableCellContainer } from '././SelectableCell';

const DataTable: React.FC = () => {
  const ref = React.useRef<HTMLDivElement>(null);
  const width = useTotalColumnWidth();
  const height = useTotalRowHeight();
  const isDataTableVisible = useIsDataTableVisible();

  return (
    <div ref={ref} className={style} style={{ width, height }}>
      <ColumnStyles />
      <RowNumberColumnWidthComputer />
      <ColumnSizeAutoAdjuster />
      <LastColumnResizeHandle />

      {isDataTableVisible ? <RowList /> : <CollapsibleButtonList />}

      <SelectableCellContainer containerRef={ref} />
    </div>
  );
};

export default React.memo(DataTable);

const style = css`
  position: relative;
`;
