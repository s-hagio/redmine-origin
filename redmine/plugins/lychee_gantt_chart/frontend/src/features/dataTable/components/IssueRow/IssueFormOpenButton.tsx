import * as React from 'react';
import { IssueID } from '@/models/issue';
import { useOpenIssueForm } from '@/features/issueForm';
import { NakedButton } from '@/components/Button';

type Props = Omit<React.ComponentProps<'button'>, 'onClick'> & {
  issueId: IssueID;
};

const IssueFormOpenButton: React.FC<Props> = ({ issueId, children, ...props }) => {
  const openIssueForm = useOpenIssueForm();

  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      event.stopPropagation();

      openIssueForm({ id: issueId });
    },
    [openIssueForm, issueId]
  );

  return (
    <NakedButton {...props} onClick={handleClick}>
      {children}
    </NakedButton>
  );
};

export default IssueFormOpenButton;
