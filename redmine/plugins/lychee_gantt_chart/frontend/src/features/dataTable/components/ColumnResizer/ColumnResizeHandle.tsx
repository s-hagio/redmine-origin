import * as React from 'react';
import { css, cx } from '@linaria/core';
import { useSimpleDrag } from '@/lib/hooks';
import { DataColumn } from '../../models/column';
import { useColumnToAdjustWidthState, useSetColumnWidth } from '../../models/width';

type Props = {
  column: DataColumn;
  className?: string | undefined;
};

const ColumnResizeHandle: React.FC<Props> = ({ column, className }) => {
  const [, setColumnToAdjustWidth] = useColumnToAdjustWidthState();
  const setColumnWidth = useSetColumnWidth();

  const baseOffsetRef = React.useRef<number>(-1);

  const [ref] = useSimpleDrag({
    onDragStart: ({ x }, event) => {
      event.stopImmediatePropagation();

      baseOffsetRef.current = x;
    },
    onDrag: ({ x }, event) => {
      event.stopImmediatePropagation();

      setColumnWidth(column.name, (currentWidth) => currentWidth + (x - baseOffsetRef.current));
      baseOffsetRef.current = x;
    },
  });

  const handleDoubleClick = React.useCallback(() => {
    setColumnToAdjustWidth(column);
  }, [column, setColumnToAdjustWidth]);

  return <div ref={ref} className={cx(style, className)} onDoubleClick={handleDoubleClick} />;
};

export default ColumnResizeHandle;

const style = css`
  position: absolute;
  top: 0;
  right: -4px;
  bottom: 0;
  width: 7px;
  cursor: col-resize;
  z-index: 1;
`;
