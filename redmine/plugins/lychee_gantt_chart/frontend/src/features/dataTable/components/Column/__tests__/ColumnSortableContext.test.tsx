import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { verticalListSortingStrategy } from '@dnd-kit/sortable';
import { CollisionDetection } from '@dnd-kit/core';
import { useSortable } from '@/lib/dnd';
import { DataColumn, MoveSelectedColumn } from '../../../models/column';
import { descriptionColumn, idColumn, issueSubjectColumn, trackerColumn } from '../../../__fixtures__';
import ColumnSortableContext from '../ColumnSortableContext';

const { closestCenterMock, moveSelectedColumnMock } = vi.hoisted(() => ({
  closestCenterMock: vi.mocked<CollisionDetection>(vi.fn()),
  moveSelectedColumnMock: vi.mocked<MoveSelectedColumn>(vi.fn()),
}));

vi.mock('@dnd-kit/core', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  closestCenter: closestCenterMock,
}));

vi.mock('../../../models/column', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useMoveSelectedColumn: vi.fn().mockReturnValue(moveSelectedColumnMock),
}));

const SortableColumn: React.FC<{ column: DataColumn }> = ({ column }) => {
  const { setNodeRef, listeners } = useSortable({ id: column.name });

  return (
    <div ref={setNodeRef} {...listeners} data-testid={column.name}>
      {column.name}
    </div>
  );
};

const columns = [trackerColumn, idColumn, issueSubjectColumn, descriptionColumn];

test('ドラッグで表示列を並び替える', () => {
  const { getByTestId } = render(
    <ColumnSortableContext columns={columns} strategy={verticalListSortingStrategy}>
      {columns.map((column) => (
        <SortableColumn key={column.name} column={column} />
      ))}
    </ColumnSortableContext>
  );

  const idColumnElement = getByTestId(idColumn.name);

  // DragStart
  fireEvent.pointerDown(idColumnElement, { clientX: 0, clientY: 0, isPrimary: true });

  // DragOver: trackerColumnと交差
  closestCenterMock.mockReturnValue([{ id: trackerColumn.name }]);
  fireEvent.pointerMove(idColumnElement, { clientX: 0, clientY: 2 });

  expect(moveSelectedColumnMock).toBeCalledWith(idColumn, trackerColumn);

  // DragOver: descriptionColumnと交差
  closestCenterMock.mockReturnValue([{ id: descriptionColumn.name }]);
  fireEvent.pointerMove(idColumnElement, { clientX: 0, clientY: 3 });

  expect(moveSelectedColumnMock).toBeCalledWith(idColumn, descriptionColumn);

  // DragEnd: descriptionColumnと交差した状態でドラッグ終了
  fireEvent.pointerUp(idColumnElement, { clientX: 0, clientY: 3 });

  expect(moveSelectedColumnMock).toBeCalledWith(idColumn, descriptionColumn);
});
