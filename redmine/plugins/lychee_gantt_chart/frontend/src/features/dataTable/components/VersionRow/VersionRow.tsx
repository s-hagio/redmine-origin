import * as React from 'react';
import { useVersion } from '@/models/version';
import Row from '../Row';
import VersionRowContent from './VersionRowContent';

type Props = Omit<React.ComponentProps<typeof VersionRowContent>, 'version'>;

const VersionRow: React.FC<Props> = ({ id, resourceId, ...props }) => {
  const version = useVersion(resourceId);

  if (!version) {
    return null;
  }

  return (
    <Row id={id}>
      <VersionRowContent id={id} resourceId={resourceId} version={version} {...props} />
    </Row>
  );
};

export default React.memo(VersionRow);
