import * as React from 'react';
import { css } from '@linaria/core';
import { ToggleRowCollapseButton, toggleRowCollapseButtonStyle, useVisibleRows } from '@/features/row';
import { COLLAPSIBLE_BUTTON_COLUMN_NAME } from '../models/column';
import Row, { style as rowStyle } from './Row';

const CollapsibleButtonList: React.FC = () => {
  const rows = useVisibleRows();

  return (
    <div className={style}>
      {rows.map((row) => (
        <Row key={row.id} id={row.id}>
          {row.isCollapsible && (
            <div data-column={COLLAPSIBLE_BUTTON_COLUMN_NAME}>
              <ToggleRowCollapseButton id={row.id} />
            </div>
          )}
        </Row>
      ))}
    </div>
  );
};

export default CollapsibleButtonList;

const style = css`
  .${rowStyle} {
    width: 100%;
  }

  .${toggleRowCollapseButtonStyle} {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 100%;
  }
`;
