import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { FieldFormat, isCustomFieldDefinition } from '@/models/fieldDefinition';
import { useGroupField } from '@/models/group';
import { FieldContent, FieldValue, getCustomFieldContentFormat } from '@/features/field';

type Props = {
  value: FieldValue;
};

const GroupLabel: React.FC<Props> = ({ value }) => {
  const { t } = useTranslation();
  const field = useGroupField();

  if (!field) {
    return null;
  }

  const format = isCustomFieldDefinition(field) ? getCustomFieldContentFormat(field) : field.format;

  if ((value === null && format !== FieldFormat.Bool) || value === '') {
    return `(${t('label.blank')})`;
  }

  switch (format) {
    case FieldFormat.DateTime:
      return <FieldContent format={FieldFormat.Date} value={value} />;
    default:
      return <FieldContent format={format} value={value} />;
  }
};

export default GroupLabel;
