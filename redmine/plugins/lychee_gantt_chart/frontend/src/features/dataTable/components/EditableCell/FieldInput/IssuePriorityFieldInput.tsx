import * as React from 'react';
import { useAssignableIssuePriorities } from '@/models/fieldValue';
import { FieldProps } from './types';
import AssociatedResourceFieldInput from './AssociatedResourceFieldInput';

const IssuePriorityFieldInput: React.FC<FieldProps> = ({ ...props }) => {
  const priorities = useAssignableIssuePriorities();

  return <AssociatedResourceFieldInput {...props} resources={priorities} />;
};

export default IssuePriorityFieldInput;
