import * as React from 'react';
import { render } from '@testing-library/react';
import { rootIssueRow } from '@/__fixtures__';
import { startDateColumn } from '../../../__fixtures__';
import Column from '../Column';

const useCellSelectedStatusStateMock = vi.hoisted(() => vi.fn());
const selectCellMock = vi.hoisted(() => vi.fn());
const toggleSelectedCellEditMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/cell', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useCellSelectedStatusState: useCellSelectedStatusStateMock,
  useSelectCell: vi.fn().mockReturnValue(selectCellMock),
  useToggleSelectedCellEdit: vi.fn().mockReturnValue(toggleSelectedCellEditMock),
}));

vi.mock('../../SelectableCell', () => ({
  SelectableCellAutoScroller: vi.fn((props) => `SelectableCellAutoScroller: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(
    <Column rowId={rootIssueRow.id} name={startDateColumn.name}>
      CHILD!
    </Column>
  );

  expect(container).toMatchSnapshot();
});

describe('選択セルの場合', () => {
  beforeEach(() => {
    useCellSelectedStatusStateMock.mockReturnValue({
      rowId: rootIssueRow.id,
      isEditable: true,
      isEditing: false,
    });
  });

  test('Render', () => {
    const { container } = render(
      <Column rowId={rootIssueRow.id} name={startDateColumn.name}>
        CHILD!
      </Column>
    );

    expect(container).toMatchSnapshot();
  });
});

describe('セル編集中の場合', () => {
  beforeEach(() => {
    useCellSelectedStatusStateMock.mockReturnValue({
      rowId: rootIssueRow.id,
      isEditable: true,
      isEditing: true,
    });
  });

  test('Render', () => {
    const { container } = render(
      <Column rowId={rootIssueRow.id} name={startDateColumn.name}>
        CHILD!
      </Column>
    );

    expect(container).toMatchSnapshot();
  });
});
