import * as React from 'react';
import { render } from '@testing-library/react';
import ColumnStyles from '../ColumnStyles';

const useDisplayColumnWidthsMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/width', async () => ({
  ...(await vi.importActual<object>('../../../models/width')),
  useDisplayColumnWidths: useDisplayColumnWidthsMock,
}));

beforeEach(() => {
  useDisplayColumnWidthsMock.mockReturnValue(
    new Map([
      ['status', 30],
      ['startDate', 20],
    ])
  );
});

test('表示項目のnameをセレクタにしてwidthをセットするstyleをレンダリング', () => {
  const result = render(<ColumnStyles />);

  expect(result.container.querySelector('style')?.textContent).toBe(
    '[data-column="status"]{width:30px}[data-column="startDate"]{width:20px}'
  );
});

test('最小幅を下回る場合は最小幅をstyleにセットする', () => {
  useDisplayColumnWidthsMock.mockReturnValue(
    new Map([
      ['status', 19],
      ['startDate', 1],
    ])
  );

  const result = render(<ColumnStyles />);

  expect(result.container.querySelector('style')?.textContent).toBe(
    '[data-column="status"]{width:20px}[data-column="startDate"]{width:20px}'
  );
});
