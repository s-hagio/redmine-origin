import * as React from 'react';
import {
  MoveDirection,
  useSelectedCell,
  useMoveSelectedCellCursor,
  useToggleSelectedCellEdit,
} from '../../models/cell';
import { isActiveFormPartElement } from '../../helpers';

const keyDirectionMap: Record<string, MoveDirection> = {
  ArrowUp: 'up',
  ArrowDown: 'down',
  ArrowLeft: 'left',
  ArrowRight: 'right',
  Tab: 'right',
  'Shift+Tab': 'left',
} as const;

const SelectableCellKeyboardNavigator: React.FC = () => {
  const selectedCell = useSelectedCell();
  const moveCursor = useMoveSelectedCellCursor();
  const toggleEdit = useToggleSelectedCellEdit();

  React.useEffect(() => {
    if (!selectedCell) {
      return;
    }

    const handleKeyDown = (event: KeyboardEvent) => {
      if (isActiveFormPartElement(event.target)) {
        return;
      }

      const isEditing = selectedCell.isEditing;

      if (event.key === 'Enter') {
        if (selectedCell.isEditable && !isEditing) {
          event.preventDefault();
          toggleEdit(true);
        }
        return;
      }

      const isTabKeyPressed = event.key === 'Tab';

      if (!isTabKeyPressed && isEditing) {
        return;
      }

      const key = isTabKeyPressed && event.shiftKey ? 'Shift+Tab' : event.key;

      if (keyDirectionMap[key]) {
        event.preventDefault();
        moveCursor(keyDirectionMap[key]);
      }
    };

    document.addEventListener('keydown', handleKeyDown);

    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [selectedCell, moveCursor, toggleEdit]);

  return null;
};

export default React.memo(SelectableCellKeyboardNavigator);
