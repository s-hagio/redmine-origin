import * as React from 'react';
import { render } from '@testing-library/react';
import { rootIssue, rootIssueRow } from '@/__fixtures__';
import { useSubjectColumn } from '../../../models/column';
import { idColumn, issueSubjectColumn, subjectColumn, trackerColumn } from '../../../__fixtures__';
import IssueSubjectColumn from '../IssueSubjectColumn';
import { SelectedCellStatus } from '../../../models/cell';

vi.mock('../../../models/column', async () => ({
  ...(await vi.importActual<object>('../../../models/column')),
  useSubjectColumn: vi.fn(),
}));

vi.mock('../../Column', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  SubjectColumn: vi.fn(({ children, ...props }) => React.createElement('subject-column', props, children)),
  ColumnContent: vi.fn(({ column, ...props }) => `ColumnContent: ${column.name} ${JSON.stringify(props)}`),
}));

vi.mock('../../EditableCell', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  EditableCell: vi.fn((props) => `EditableCell: ${JSON.stringify(props)}`),
}));

const useCellSelectedStatusStateMock = vi.hoisted(() => vi.fn<never, SelectedCellStatus | null>());

vi.mock('../../../models/cell', () => ({
  useCellSelectedStatusState: useCellSelectedStatusStateMock,
}));

vi.mock('../IssueFormOpenButton', () => ({
  default: vi.fn(({ children, ...props }) => React.createElement('issue-form-open-button', props, children)),
}));

vi.mock('../IssueLinkButton', () => ({
  default: vi.fn((props) => `IssueLinkButton: ${JSON.stringify(props)}`),
}));

beforeEach(() => {
  useCellSelectedStatusStateMock.mockReturnValue(null);

  vi.mocked(useSubjectColumn).mockReturnValue({
    ...issueSubjectColumn,
    partColumns: [trackerColumn, idColumn],
  });
});

test('Render with part columns', () => {
  vi.mocked(useSubjectColumn).mockReturnValueOnce({
    ...issueSubjectColumn,
    partColumns: [trackerColumn, idColumn],
  });

  const result = render(
    <IssueSubjectColumn rowId={rootIssueRow.id} issue={rootIssue} depth={3} isCollapsible={false} />
  );

  expect(result.container).toMatchSnapshot();

  vi.mocked(useSubjectColumn).mockReturnValueOnce({
    ...issueSubjectColumn,
    partColumns: [idColumn, trackerColumn],
  });

  result.rerender(
    <IssueSubjectColumn rowId={rootIssueRow.id} issue={rootIssue} depth={3} isCollapsible={false} />
  );

  expect(result.container).toMatchSnapshot();
});

test('Render without part columns', () => {
  vi.mocked(useSubjectColumn).mockReturnValue({ ...issueSubjectColumn, partColumns: [] });

  const result = render(
    <IssueSubjectColumn rowId={rootIssueRow.id} issue={rootIssue} depth={3} isCollapsible={false} />
  );

  expect(result.container).toMatchSnapshot();
});

test('編集状態ならEditableCellを表示する', () => {
  useCellSelectedStatusStateMock.mockReturnValue({
    rowId: rootIssueRow.id,
    columnName: subjectColumn.name,
    isEditable: true,
    isEditing: true,
  });

  const result = render(
    <IssueSubjectColumn rowId={rootIssueRow.id} issue={rootIssue} depth={3} isCollapsible={false} />
  );

  expect(result.container).toMatchSnapshot();
});
