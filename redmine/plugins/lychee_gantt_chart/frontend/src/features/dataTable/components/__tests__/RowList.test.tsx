import * as React from 'react';
import { render } from '@testing-library/react';
import {
  mainProjectRow,
  mainProjectVersionRow,
  newStatusGroupRow,
  placeholderRow,
  versionIssueRow,
} from '@/__fixtures__';
import RowList from '../RowList';
import { DataColumn } from '../../models/column';

const useVisibleRowsMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useVisibleRows: useVisibleRowsMock,
}));

const useOptionalColumnsMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/column', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useOptionalColumns: useOptionalColumnsMock,
}));

const ProjectRowSpy = vi.hoisted(() => vi.fn());

vi.mock('../ProjectRow', () => ({
  ProjectRow: vi.fn((props) => {
    ProjectRowSpy(props);
    return 'ProjectRow';
  }),
}));

const VersionRowSpy = vi.hoisted(() => vi.fn());

vi.mock('../VersionRow', () => ({
  VersionRow: vi.fn((props) => {
    VersionRowSpy(props);
    return 'VersionRow';
  }),
}));

const IssueRowSpy = vi.hoisted(() => vi.fn());

vi.mock('../IssueRow', () => ({
  IssueRow: vi.fn((props) => {
    IssueRowSpy(props);
    return 'IssueRow';
  }),
}));

const GroupRowSpy = vi.hoisted(() => vi.fn());

vi.mock('../GroupRow', () => ({
  GroupRow: vi.fn((props) => {
    GroupRowSpy(props);
    return 'GroupRow';
  }),
}));

const PlaceholderRowSpy = vi.hoisted(() => vi.fn());

vi.mock('../PlaceholderRow', () => ({
  PlaceholderRow: vi.fn((props) => {
    PlaceholderRowSpy(props);
    return 'PlaceholderRow';
  }),
}));

const visibleRows = [
  ...[mainProjectRow, mainProjectVersionRow, versionIssueRow, placeholderRow].map((row, depth) => ({
    ...row,
    depth,
  })),
  { ...newStatusGroupRow, depth: 0 },
];
const columns = [{ name: 'startDate' }, { name: 'description' }] as DataColumn[];

beforeEach(() => {
  useVisibleRowsMock.mockReturnValue(visibleRows);
  useOptionalColumnsMock.mockReturnValue(columns);
});

test('Render rows', () => {
  const result = render(<RowList />);

  expect(result.container).toMatchSnapshot();

  expect(ProjectRowSpy).toBeCalledWith({ ...mainProjectRow, depth: 0, columns });
  expect(VersionRowSpy).toBeCalledWith({ ...mainProjectVersionRow, depth: 1, columns });
  expect(IssueRowSpy).toBeCalledWith({ ...versionIssueRow, depth: 2, columns });
  expect(PlaceholderRowSpy).toBeCalledWith({ ...placeholderRow, depth: 3, columns });
  expect(GroupRowSpy).toBeCalledWith({ ...newStatusGroupRow, depth: 0, columns });
});
