import * as React from 'react';
import { useAssignableIssueCategories } from '@/models/fieldValue';
import { FieldProps } from './types';
import AssociatedResourceFieldInput from './AssociatedResourceFieldInput';

const IssueCategoryFieldInput: React.FC<FieldProps> = ({ issue, ...props }) => {
  const categories = useAssignableIssueCategories(issue.projectId);

  return <AssociatedResourceFieldInput {...props} issue={issue} resources={categories} />;
};

export default IssueCategoryFieldInput;
