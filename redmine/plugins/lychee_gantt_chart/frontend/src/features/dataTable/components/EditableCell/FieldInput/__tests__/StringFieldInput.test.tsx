import * as React from 'react';
import { render } from '@testing-library/react';
import { stringCustomFieldDefinition } from '@/__fixtures__';
import StringFieldInput from '../StringFieldInput';
import { basicProps } from '../__fixtures__';
import { FieldProps } from '../types';

const CellBasicInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../CellBasicInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    CellBasicInputSpy(props);
    return React.createElement('cell-basic-input', null, children);
  }),
}));

const props: FieldProps = {
  ...basicProps,
  value: 'string!',
  field: stringCustomFieldDefinition,
  isRequired: true,
};

test('CellBasicInputを表示', () => {
  const { container } = render(<StringFieldInput {...props} />);

  expect(CellBasicInputSpy).toBeCalledWith(props);
  expect(container).toMatchSnapshot();
});
