import * as React from 'react';
import { useAssignableVersions } from '@/models/fieldValue';
import { FieldProps } from './types';
import AssociatedResourceFieldInput from './AssociatedResourceFieldInput';

const VersionFieldInput: React.FC<FieldProps> = ({ issue, ...props }) => {
  const versions = useAssignableVersions(issue.projectId);

  return <AssociatedResourceFieldInput {...props} issue={issue} resources={versions} />;
};

export default VersionFieldInput;
