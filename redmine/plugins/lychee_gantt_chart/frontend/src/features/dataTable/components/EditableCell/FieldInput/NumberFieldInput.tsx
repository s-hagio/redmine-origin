import * as React from 'react';
import { FieldProps } from './types';
import CellBasicInput from './CellBasicInput';

const NumberFieldInput: React.FC<FieldProps> = ({ ...props }) => {
  return <CellBasicInput {...props} type="tel" />;
};

export default NumberFieldInput;
