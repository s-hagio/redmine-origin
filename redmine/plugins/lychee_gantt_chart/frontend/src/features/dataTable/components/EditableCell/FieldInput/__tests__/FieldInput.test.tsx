vi.mock('../PlainTextFieldInput', () => ({
  default: vi.fn((props) => `PlainTextFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../FormattableTextFieldInput', () => ({
  default: vi.fn((props) => `FormattableTextFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../StringFieldInput', () => ({
  default: vi.fn((props) => `StringFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../NumberFieldInput', () => ({
  default: vi.fn((props) => `NumberFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../DoneRatioFieldInput', () => ({
  default: vi.fn((props) => `DoneRatioFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../DateFieldInput', () => ({
  default: vi.fn((props) => `DateFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../ParentIssueIdFieldInput', () => ({
  default: vi.fn((props) => `ParentIssueIdFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../ProjectFieldInput', () => ({
  default: vi.fn((props) => `ProjectFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../VersionFieldInput', () => ({
  default: vi.fn((props) => `VersionFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../TrackerFieldInput', () => ({
  default: vi.fn((props) => `TrackerFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../AssignedToFieldInput', () => ({
  default: vi.fn((props) => `AssignedToFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../IssueStatusFieldInput', () => ({
  default: vi.fn((props) => `IssueStatusFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../IssuePriorityFieldInput', () => ({
  default: vi.fn((props) => `IssuePriorityFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../IssueCategoryFieldInput', () => ({
  default: vi.fn((props) => `IssueCategoryFieldInput: ${JSON.stringify(props)}`),
}));
vi.mock('../CustomFieldInput', () => ({
  default: vi.fn((props) => `CustomFieldInput: ${JSON.stringify(props)}`),
}));

test.todo('カラムの種類に応じて適切なFieldInputを表示する');
