import * as React from 'react';
import { css } from '@linaria/core';
import { ToggleRowCollapseButton } from '@/features/row';
import { SubjectColumnProps } from '../types';
import { INDENT_WIDTH } from './constants';
import Column from './Column';
import Cell from './Cell';

const SubjectColumn: React.FC<SubjectColumnProps> = ({ rowId, isCollapsible, depth, children }) => {
  return (
    <Column rowId={rowId} name="subject">
      <div className={indentStyle} style={{ marginLeft: depth * INDENT_WIDTH }}>
        {isCollapsible && <ToggleRowCollapseButton id={rowId} />}
      </div>

      <Cell>{children}</Cell>
    </Column>
  );
};

export default React.memo(SubjectColumn);

const indentStyle = css`
  > * {
    margin-left: -${INDENT_WIDTH}px;
  }

  img {
    vertical-align: middle;
  }
`;
