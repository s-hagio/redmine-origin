import * as React from 'react';
import { css } from '@linaria/core';
import { useCommitIssueCreation, useIssueCreationOrThrow } from '@/features/issueCreation';

type Props = {
  toggleContinuousInput: () => void;
};

const SubjectInputForm: React.FC<Props> = ({ toggleContinuousInput }) => {
  const { params } = useIssueCreationOrThrow();
  const commit = useCommitIssueCreation();

  const inputRef = React.useRef<HTMLInputElement>(null);

  const [isSubmitting, setSubmitting] = React.useState<boolean>(false);
  const [subject, setSubject] = React.useState<string>('');

  const handleChange = React.useCallback(({ target }: React.ChangeEvent<HTMLInputElement>) => {
    setSubject(target.value);
  }, []);

  const handleSubmit = React.useCallback(
    async (event: React.FormEvent) => {
      event.preventDefault();

      try {
        setSubmitting(true);
        await commit({ ...params, subject });
        setSubject('');
      } finally {
        setSubmitting(false);
      }
    },
    [commit, params, subject]
  );

  React.useEffect(() => {
    !isSubmitting && inputRef.current?.focus();
  }, [isSubmitting]);

  return (
    <form onSubmit={handleSubmit} className={style}>
      <input
        ref={inputRef}
        type="text"
        value={subject}
        disabled={isSubmitting}
        required
        onChange={handleChange}
        onBlur={toggleContinuousInput}
      />
    </form>
  );
};

export default SubjectInputForm;

const style = css`
  width: 100%;

  input {
    width: 100%;
    min-width: 20rem;
  }
`;
