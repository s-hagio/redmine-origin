import * as React from 'react';
import { Project } from '@/models/project';
import { ProjectRow } from '@/features/row';
import { RowProps } from '../types';
import ProjectSubjectColumn from './ProjectSubjectColumn';
import ProjectColumn from './ProjectColumn';

type Props = RowProps<ProjectRow> & {
  project: Project;
};

const ProjectRowContent: React.FC<Props> = ({ id, project, isCollapsible, depth, columns }) => {
  return (
    <>
      <ProjectSubjectColumn rowId={id} isCollapsible={isCollapsible} depth={depth} project={project} />

      {columns.map((column) => (
        <ProjectColumn key={column.name} rowId={id} project={project} column={column} />
      ))}
    </>
  );
};

export default React.memo(ProjectRowContent);
