import * as React from 'react';
import { useAssignableTrackers } from '@/models/fieldValue';
import { FieldProps } from './types';
import AssociatedResourceFieldInput from './AssociatedResourceFieldInput';

const TrackerFieldInput: React.FC<FieldProps> = ({ issue, ...props }) => {
  const trackers = useAssignableTrackers(issue.projectId);

  return <AssociatedResourceFieldInput {...props} issue={issue} resources={trackers} />;
};

export default TrackerFieldInput;
