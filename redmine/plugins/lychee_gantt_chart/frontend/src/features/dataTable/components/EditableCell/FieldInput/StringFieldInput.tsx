import * as React from 'react';
import { FieldProps } from './types';
import CellBasicInput from './CellBasicInput';

const StringFieldInput: React.FC<FieldProps> = (props) => {
  return <CellBasicInput {...props} />;
};

export default StringFieldInput;
