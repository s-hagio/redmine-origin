import * as React from 'react';
import { css } from '@linaria/core';
import { useSetColumnWidth, useColumnToAdjustWidthState } from '../../models/width';

const ColumnSizeAutoAdjuster: React.FC = () => {
  const [element, setElement] = React.useState<HTMLDivElement | null>(null);

  const [column, setColumn] = useColumnToAdjustWidthState();
  const setColumnWidth = useSetColumnWidth();

  React.useEffect(() => {
    if (!element || !column) {
      return;
    }

    setColumn(null);

    element.appendChild(
      Array.from(document.querySelectorAll(`[data-column="${column.name}"]`)).reduce((fragment, el) => {
        const clonedElement = el.cloneNode(true) as HTMLElement;
        clonedElement.removeAttribute('data-column');
        fragment.appendChild(clonedElement);

        return fragment;
      }, document.createDocumentFragment())
    );

    // ギリギリの幅だとCellのtext-overflowが発動して...になるので1px追加
    setColumnWidth(column.name, element.clientWidth + 1);
  }, [element, column, setColumn, setColumnWidth]);

  if (!column) {
    return null;
  }

  return (
    <div ref={setElement} className={style}>
      <div>{column.field.label}</div>
    </div>
  );
};

export default React.memo(ColumnSizeAutoAdjuster);

const style = css`
  position: fixed;
  visibility: hidden;
`;
