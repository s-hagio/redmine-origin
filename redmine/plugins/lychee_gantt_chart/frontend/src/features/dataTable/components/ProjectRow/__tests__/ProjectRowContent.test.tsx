import * as React from 'react';
import { render } from '@testing-library/react';
import { Project } from '@/models/project';
import { mainProject, mainProjectRow } from '@/__fixtures__';
import ProjectRowContent from '../ProjectRowContent';
import { DataColumn } from '../../../models/column';

vi.mock('../ProjectSubjectColumn', () => ({
  default: vi.fn((props) => `ProjectSubjectColumn: ${JSON.stringify(props)}`),
}));

vi.mock('../ProjectColumn', () => ({
  default: vi.fn((props) => `ProjectColumn: ${JSON.stringify(props)}`),
}));

const project = { id: mainProject.id, name: mainProject.name } as Project;
const columns = [{ name: 'startDate' }, { name: 'description' }] as DataColumn[];

test('Render', () => {
  const { container } = render(<ProjectRowContent {...mainProjectRow} columns={columns} project={project} />);

  expect(container).toMatchSnapshot();
});
