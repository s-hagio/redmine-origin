import * as React from 'react';
import { render } from '@testing-library/react';
import { doneRatioFieldDefinition } from '@/__fixtures__';
import DoneRatioFieldInput from '../DoneRatioFieldInput';
import { basicProps } from '../__fixtures__';

const ListFieldInputSpy = vi.hoisted(() => vi.fn());

vi.mock('../ListFieldInput', () => ({
  default: vi.fn(({ children, ...props }) => {
    ListFieldInputSpy(props);
    return React.createElement('list-field-input', null, children);
  }),
}));

const props = {
  ...basicProps,
  field: doneRatioFieldDefinition,
  value: 10,
};

test('ListFieldInputを表示する', () => {
  const { container } = render(<DoneRatioFieldInput {...props} />);

  expect(container).toMatchSnapshot();
  expect(ListFieldInputSpy).toBeCalledWith({
    ...props,
    options: [
      { id: '0', label: '0%', value: 0 },
      { id: '10', label: '10%', value: 10 },
      { id: '20', label: '20%', value: 20 },
      { id: '30', label: '30%', value: 30 },
      { id: '40', label: '40%', value: 40 },
      { id: '50', label: '50%', value: 50 },
      { id: '60', label: '60%', value: 60 },
      { id: '70', label: '70%', value: 70 },
      { id: '80', label: '80%', value: 80 },
      { id: '90', label: '90%', value: 90 },
      { id: '100', label: '100%', value: 100 },
    ],
  });
});
