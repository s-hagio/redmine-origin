import * as React from 'react';
import { css } from '@linaria/core';
import AvailableColumnList from './AvailableColumnList';
import SelectedColumnList from './SelectedColumnList';

const ColumnListContainer: React.FC = () => {
  return (
    <div className={style}>
      <AvailableColumnList />
      <SelectedColumnList />
    </div>
  );
};

export default ColumnListContainer;

const style = css`
  display: flex;

  ul {
    max-width: 10rem;
    list-style: none;
  }
`;
