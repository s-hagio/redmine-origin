import * as React from 'react';
import { css } from '@linaria/core';
import { useDisplayColumns } from '../../models/column';
import ColumnResizeHandle from './ColumnResizeHandle';

const LastColumnResizeHandle: React.FC = () => {
  const displayColumns = useDisplayColumns();
  const column = displayColumns.at(-1);

  if (!column) {
    return null;
  }

  return <ColumnResizeHandle className={style} column={column} />;
};

export default LastColumnResizeHandle;

export const style = css`
  z-index: 1;
`;
