import * as React from 'react';
import { css } from '@linaria/core';
import { Issue } from '@/models/issue';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { linkColor } from '@/styles';
import { useSubjectColumn, getIssueColumnContent } from '../../models/column';
import { ColumnContent, SubjectColumn } from '../Column';
import { SubjectColumnProps } from '../types';
import { useCellSelectedStatusState } from '../../models/cell';
import { EditableCell } from '../EditableCell';
import IssueLinkButton from './IssueLinkButton';
import IssueFormOpenButton from './IssueFormOpenButton';

type Props = React.PropsWithChildren<
  Omit<SubjectColumnProps, 'rowId'> & {
    rowId: IssueResourceIdentifier;
    issue: Issue;
  }
>;

const IssueSubjectColumn: React.FC<Props> = ({ rowId, issue, isCollapsible, depth }) => {
  const subjectColumn = useSubjectColumn();
  const status = useCellSelectedStatusState(rowId, 'subject');

  return (
    <SubjectColumn rowId={rowId} isCollapsible={isCollapsible} depth={depth}>
      <IssueLinkButton issueId={issue.id} assignedToId={issue.assignedToId} />

      {!!subjectColumn.partColumns.length && (
        <IssueFormOpenButton issueId={issue.id} className={statusStyle}>
          {subjectColumn.partColumns.map((column) => (
            <span key={column.name}>
              <ColumnContent column={column} value={getIssueColumnContent(column, issue)} />
            </span>
          ))}
        </IssueFormOpenButton>
      )}

      <span className={subjectTextStyle}>{issue.subject}</span>

      {status?.isEditing && <EditableCell status={status} />}
    </SubjectColumn>
  );
};

export default React.memo(IssueSubjectColumn);

const statusStyle = css`
  color: ${linkColor};

  ::after {
    content: ':';
    margin-right: 0.15rem;
  }

  span:not(:first-of-type) {
    margin-left: 0.25rem;
  }
`;

const subjectTextStyle = css`
  vertical-align: middle;
`;
