import * as React from 'react';
import { css } from '@linaria/core';
import { create as createObject } from '@/utils/object';
import { getResourceIdFromIdentifier } from '@/models/coreResource';
import { useIssueOrThrow } from '@/models/issue';
import { useIsRequiredIssueField } from '@/models/fieldRule';
import { FieldValue } from '@/features/field';
import { LoadingIndicator } from '@/features/resourceLoader';
import { useSaveIssue } from '@/features/resourceController';
import { getZIndex } from '@/styles';
import { IssueSelectedCellStatus, useToggleSelectedCellEdit } from '../../models/cell';
import { getIssueColumnValue, useColumnOrThrow } from '../../models/column';
import FieldInput from './FieldInput';

type Props = {
  status: IssueSelectedCellStatus;
};

const EditableCell: React.FC<Props> = ({ status }) => {
  const ref = React.useRef<HTMLDivElement>(null);

  const issue = useIssueOrThrow(getResourceIdFromIdentifier(status.rowId));
  const column = useColumnOrThrow(status.columnName);
  const value = getIssueColumnValue(column, issue);

  const isRequired = useIsRequiredIssueField(issue.id);

  const saveIssue = useSaveIssue();
  const toggleEdit = useToggleSelectedCellEdit();

  const currentValueRef = React.useRef(value);

  const handleChange = React.useCallback((value: FieldValue) => {
    currentValueRef.current = value;
  }, []);

  const handleCommit = React.useCallback(
    (newValue: FieldValue) => {
      setTimeout(() => toggleEdit(false), 0);

      if (newValue === value) {
        return;
      }

      saveIssue(
        {
          id: issue.id,
          ...createObject(column.field.valuePath, newValue),
        },
        {
          shouldOpenFormOnError: true,
          alwaysNotifyOnError: true,
        }
      );
    },
    [value, column.field.valuePath, saveIssue, issue.id, toggleEdit]
  );

  React.useEffect(() => {
    const handleClick = (event: MouseEvent) => {
      if (!ref.current || event.composedPath().includes(ref.current)) {
        return;
      }

      if (currentValueRef.current !== value) {
        handleCommit(currentValueRef.current);
      }

      toggleEdit(false);
    };

    const handleKeyDown = (event: KeyboardEvent) => {
      switch (event.code) {
        case 'Tab':
          if (currentValueRef.current !== value) {
            handleCommit(currentValueRef.current);
          }
          break;
        case 'Escape':
          toggleEdit(false);
          break;
      }
    };

    document.addEventListener('click', handleClick);
    document.addEventListener('keydown', handleKeyDown);

    return () => {
      document.removeEventListener('click', handleClick);
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [handleCommit, toggleEdit, value]);

  return (
    <div ref={ref} className={style} data-format={column.field.format}>
      <React.Suspense fallback={<LoadingIndicator />}>
        <FieldInput
          cellRef={ref}
          issue={issue}
          column={column}
          value={value}
          isRequired={!!column.field.fieldName && isRequired(column.field.fieldName)}
          onChange={handleChange}
          onCommit={handleCommit}
        />
      </React.Suspense>
    </div>
  );
};

export default EditableCell;

const style = css`
  position: absolute;
  top: -1px;
  left: -1px;
  min-width: calc(100% + 2px);
  height: calc(100% + 2px);
  z-index: ${getZIndex('dataTable/EditableCell')};

  border-radius: 3px;
  box-shadow: rgba(13, 103, 159, 0.63) 0 0 4px 0;
  border: 1px solid #1b71a6;

  &[data-format='date'] {
    min-width: 6.35rem;
    margin-right: -1px;
    background: #fff;
  }
`;
