import * as React from 'react';
import { useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderWithRecoilHook } from '@/test-utils';
import ButtonList from '../ButtonList';
import { columnVisibilityState } from '../../../../models/column/states';
import { ColumnListType } from '../../../../models/column';

vi.mock('../AllColumnToggleButton', () => ({
  default: vi.fn((props) => `AllColumnToggleButton: ${JSON.stringify(props)}`),
}));

vi.mock('../OptionalColumnToggleButton', () => ({
  default: vi.fn((props) => `OptionalColumnToggleButton: ${JSON.stringify(props)}`),
}));

vi.mock('../ConfigOpenButton', () => ({
  default: vi.fn((props) => `ConfigOpenButton: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { result, container } = renderWithRecoilHook(
    <ButtonList />,
    () => ({
      setVisible: useSetRecoilState(columnVisibilityState(ColumnListType.All)),
    }),
    {
      initializeState: ({ set }) => {
        set(columnVisibilityState(ColumnListType.All), true);
      },
    }
  );

  expect(container).toMatchSnapshot('全列展開時');

  act(() => result.current.setVisible(false));

  expect(container).toMatchSnapshot('全列縮小時');
});
