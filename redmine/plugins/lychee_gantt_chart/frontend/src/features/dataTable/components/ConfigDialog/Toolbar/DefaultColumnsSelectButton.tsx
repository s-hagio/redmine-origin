import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { useDefaultIssueQuery } from '@/models/query';
import { NakedButton } from '@/components/Button';
import { Icon } from '@/components/Icon';
import { dialogBorderColor } from '@/styles';
import { useSelectedColumnNamesState } from '../../../models/column';

const DefaultColumnsSelectButton: React.FC = () => {
  const { t } = useTranslation();
  const { c: defaultColumnNames } = useDefaultIssueQuery();
  const [, setSelectedColumnNames] = useSelectedColumnNamesState();

  const handleClick = React.useCallback(() => {
    setSelectedColumnNames(defaultColumnNames);
  }, [defaultColumnNames, setSelectedColumnNames]);

  return (
    <NakedButton className={style} type="button" onClick={handleClick}>
      <Icon name="selectAll" />
      {t('label.selectDefaultColumns')}
    </NakedButton>
  );
};

export default DefaultColumnsSelectButton;

const style = css`
  padding: 0.75rem 0.75rem 0.75rem 0.55rem;
  border: 1px solid ${dialogBorderColor};
  border-radius: 2px;
  background: #fff;

  :hover {
    background: #f9f9f9;
  }
`;
