export const isActiveFormPartElement = (element: EventTarget | HTMLElement | null): boolean => {
  return (
    element instanceof HTMLElement &&
    element === document.activeElement &&
    element.matches('input, textarea, button, select, [contenteditable="true"]')
  );
};
