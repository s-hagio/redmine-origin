import { get } from '@/utils/object';
import { Issue } from '@/models/issue';
import { FieldValue } from '@/features/field';
import { DataColumn } from '../types';
import { getIssueColumnValue } from './getIssueColumnValue';

export const getIssueColumnContent = (column: DataColumn, issue: Issue): FieldValue => {
  return column.field.contentPath ? get(issue, column.field.contentPath) : getIssueColumnValue(column, issue);
};
