import { useDisplayColumnWidths } from '../hooks';

export const useTotalColumnWidth = (): number => {
  const columnWidths = useDisplayColumnWidths();

  return [...columnWidths.values()].reduce((totalWidth, width) => totalWidth + width, 0);
};
