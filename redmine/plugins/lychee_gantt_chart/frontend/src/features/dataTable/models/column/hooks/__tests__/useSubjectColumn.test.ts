import { renderHook } from '@testing-library/react';
import { useSubjectColumn } from '../useSubjectColumn';
import { useAvailableColumnMap } from '../useAvailableColumnMap';
import { descriptionColumn, idColumn, issueSubjectColumn, trackerColumn } from '../../../../__fixtures__';
import { useSelectedColumnNamesState } from '../useSelectedColumnNamesState';

vi.mock('../useSelectedColumnNamesState', () => ({
  useSelectedColumnNamesState: vi.fn(),
}));

vi.mock('../useAvailableColumnMap', async () => ({ useAvailableColumnMap: vi.fn() }));

beforeEach(() => {
  vi.mocked(useSelectedColumnNamesState).mockReturnValue([
    ['tracker', 'id', 'subject', 'description'],
    vi.fn(),
  ]);

  vi.mocked(useAvailableColumnMap).mockReturnValue(
    new Map([
      [idColumn.name, idColumn],
      [trackerColumn.name, trackerColumn],
      [issueSubjectColumn.name, issueSubjectColumn],
      [descriptionColumn.name, descriptionColumn],
    ])
  );
});

test('subjectより前の選択項目を部品としてSubjectColumnを返す', () => {
  const { result } = renderHook(() => useSubjectColumn());

  expect(result.current).toStrictEqual({
    ...issueSubjectColumn,
    partColumns: [trackerColumn, idColumn],
  });
});

test('subjectより後ろにある部品項目は除外', () => {
  vi.mocked(useSelectedColumnNamesState).mockReturnValue([
    ['tracker', 'subject', 'id', 'description'],
    vi.fn(),
  ]);

  const { result } = renderHook(() => useSubjectColumn());

  expect(result.current).toStrictEqual({
    ...issueSubjectColumn,
    partColumns: [trackerColumn],
  });
});

test('利用不可の部品項目は除外', () => {
  vi.mocked(useAvailableColumnMap).mockReturnValueOnce(
    new Map([
      [idColumn.name, idColumn],
      [issueSubjectColumn.name, issueSubjectColumn],
      [descriptionColumn.name, descriptionColumn],
    ])
  );

  const { result } = renderHook(() => useSubjectColumn());

  expect(result.current).toStrictEqual({
    ...issueSubjectColumn,
    partColumns: [idColumn],
  });
});
