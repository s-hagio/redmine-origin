import { isIssueRow, Row } from '@/features/row';

export const isCellSelectableRow = (row: Row) => {
  return isIssueRow(row);
};
