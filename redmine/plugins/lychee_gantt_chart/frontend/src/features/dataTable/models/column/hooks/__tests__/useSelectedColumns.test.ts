import { useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { selectedColumnNamesState } from '../../states';
import { idColumn, issueSubjectColumn, startDateColumn } from '../../../../__fixtures__';
import { useSelectedColumns } from '../useSelectedColumns';
import { useAvailableColumnMap } from '../useAvailableColumnMap';

vi.mock('../useAvailableColumnMap', () => ({
  useAvailableColumnMap: vi.fn(),
}));

beforeEach(() => {
  vi.mocked(useAvailableColumnMap).mockReturnValue(
    new Map([
      [idColumn.name, idColumn],
      [issueSubjectColumn.name, issueSubjectColumn],
      [startDateColumn.name, startDateColumn],
    ])
  );
});

test('表示項目のDateColumn配列を返す', () => {
  const { result } = renderRecoilHook(
    () => ({
      selectedColumns: useSelectedColumns(),
      setSelectedColumnNames: useSetRecoilState(selectedColumnNamesState),
    }),
    {
      initializeState: ({ set }) => {
        set(selectedColumnNamesState, [idColumn.name, issueSubjectColumn.name]);
      },
    }
  );

  expect(result.current.selectedColumns).toStrictEqual([idColumn, issueSubjectColumn]);

  act(() => result.current.setSelectedColumnNames([issueSubjectColumn.name, startDateColumn.name]));
  expect(result.current.selectedColumns).toStrictEqual([issueSubjectColumn, startDateColumn]);
});
