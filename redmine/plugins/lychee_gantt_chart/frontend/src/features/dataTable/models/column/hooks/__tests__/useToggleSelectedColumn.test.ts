import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { useToggleSelectedColumn } from '../useToggleSelectedColumn';
import { selectedColumnNamesState } from '../../states';
import { descriptionColumn, idColumn } from '../../../../__fixtures__';

test('実行ごとに選択と非選択をトグルする', () => {
  const { result } = renderRecoilHook(
    () => ({
      toggle: useToggleSelectedColumn(),
      selectedColumnNames: useRecoilValue(selectedColumnNamesState),
    }),
    {
      initializeState: ({ set }) => {
        set(selectedColumnNamesState, [idColumn.name]);
      },
    }
  );

  expect(result.current.selectedColumnNames).toStrictEqual([idColumn.name]);

  act(() => result.current.toggle(descriptionColumn.name));
  expect(result.current.selectedColumnNames).toStrictEqual([idColumn.name, descriptionColumn.name]);

  act(() => result.current.toggle(descriptionColumn.name));
  expect(result.current.selectedColumnNames).toStrictEqual([idColumn.name]);
});
