import { isLabelColumn } from '../isLabelColumn';
import {
  idColumn,
  trackerColumn,
  parentSubjectColumn,
  startDateColumn,
  descriptionColumn,
  stringCustomFieldColumn,
  subjectColumn,
} from '../../../../__fixtures__';

test('ラベル用のカラムならtrueを返す', () => {
  expect(isLabelColumn(idColumn)).toBe(true);
  expect(isLabelColumn(trackerColumn)).toBe(true);
});

test('それ以外はfalseを返す', () => {
  expect(isLabelColumn(parentSubjectColumn)).toBe(false);
  expect(isLabelColumn(startDateColumn)).toBe(false);
  expect(isLabelColumn(descriptionColumn)).toBe(false);
  expect(isLabelColumn(stringCustomFieldColumn)).toBe(false);
  expect(isLabelColumn(subjectColumn)).toBe(false);
});
