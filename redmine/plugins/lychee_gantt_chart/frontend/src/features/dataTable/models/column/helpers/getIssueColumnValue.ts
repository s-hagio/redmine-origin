import { get } from '@/utils/object';
import { Issue } from '@/models/issue';
import { FieldValue } from '@/features/field';
import { DataColumn } from '../types';

export const getIssueColumnValue = (column: DataColumn, issue: Issue): FieldValue => {
  return get(issue, column.field.valuePath);
};
