import { useRecoilCallback } from 'recoil';
import { useIsEditableField } from '@/models/fieldRule';
import { ColumnName } from '@/models/query';
import { issueState } from '@/models/issue';
import { CustomFieldFormat } from '@/models/customField';
import { isCustomFieldDefinition } from '@/models/fieldDefinition';
import { RowID, RowResourceType } from '@/features/row';
import { useAvailableColumnMap } from '../../column';

type IsEditableCell = (rowId: RowID | null, columnName: ColumnName | null) => boolean;

const uneditableCustomFieldFormats: ReadonlyArray<CustomFieldFormat> = [
  CustomFieldFormat.Attachment,
] as const;

export const useIsEditableCell = (): IsEditableCell => {
  const isEditableField = useIsEditableField();
  const availableColumnMap = useAvailableColumnMap();

  return useRecoilCallback(
    ({ snapshot }) => {
      return (rowId, columnName) => {
        if (!rowId || !columnName) {
          return false;
        }

        const column = availableColumnMap.get(columnName);

        if (
          !column ||
          (isCustomFieldDefinition(column.field) &&
            uneditableCustomFieldFormats.includes(column.field.format))
        ) {
          return false;
        }

        const [type, ...ids] = rowId.split('-');

        if (type !== RowResourceType.Issue) {
          return false;
        }

        const resourceId = +ids[ids.length - 1];
        const issue = snapshot.getLoadable(issueState(resourceId)).getValue();

        return !!(issue && column.field.fieldName) && isEditableField(issue, column.field.fieldName);
      };
    },
    [availableColumnMap, isEditableField]
  );
};
