import { versionSharedByHierarchy } from '@/__fixtures__';
import { descriptionColumn, startDateColumn } from '../../../../__fixtures__';
import { getVersionColumnContent } from '../getVersionColumnContent';

const version = {
  ...versionSharedByHierarchy,
  startDate: '2022-01-01',
};

test('field.contentPathまたはfield.valuePathを元にVersionの持つ値を返す', () => {
  expect(getVersionColumnContent(startDateColumn, version)).toBe(version.startDate);
  expect(getVersionColumnContent(descriptionColumn, version)).toBe(null);
});
