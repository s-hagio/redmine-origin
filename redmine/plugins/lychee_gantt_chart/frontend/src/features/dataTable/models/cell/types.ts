import { ColumnName } from '@/models/query';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { RowID } from '@/features/row';

export type CellIdentifier = `${RowID}-${ColumnName}`;

type BaseSelectedCellStatus = {
  columnName: ColumnName;
  isEditable: boolean;
  isEditing: boolean;
};

export type IssueSelectedCellStatus = BaseSelectedCellStatus & {
  rowId: IssueResourceIdentifier;
};

export type EditableSelectedCellStatus = IssueSelectedCellStatus;

export type UneditableSelectedCellStatus = BaseSelectedCellStatus & {
  rowId: Exclude<RowID, EditableSelectedCellStatus['rowId']>;
};

export type SelectedCellStatus = EditableSelectedCellStatus | UneditableSelectedCellStatus;

export type MoveDirection = 'up' | 'down' | 'left' | 'right';
