import { atom } from 'recoil';
import { DataColumn } from '../../column';

export const columnToAdjustWidthState = atom<DataColumn | null>({
  key: 'dataTable/columnToAdjustWidth',
  default: null,
});
