import { useRecoilCallback } from 'recoil';
import { selectedCellState } from '../states';

export const useToggleSelectedCellEdit = () => {
  return useRecoilCallback(({ set }) => {
    return (isEditing?: boolean) => {
      set(selectedCellState, (selectedCell) => {
        if (!selectedCell) {
          return null;
        }

        return {
          ...selectedCell,
          isEditing: typeof isEditing === 'boolean' ? isEditing : !selectedCell.isEditing,
        };
      });
    };
  }, []);
};
