import { DataColumn } from '../types';
import { SUBJECT_COLUMN_NAME } from '../constants';

export const isSubjectColumn = (column: DataColumn): boolean => column.name === SUBJECT_COLUMN_NAME;
