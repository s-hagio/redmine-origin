import { useMemo } from 'react';
import { indexOfSubject } from '../helpers';
import { DataColumn, ColumnListType } from '../types';
import { useAvailableColumnMap } from './useAvailableColumnMap';
import { useSelectedColumnNamesState } from './useSelectedColumnNamesState';
import { useColumnListVisibilityState } from './useColumnListVisibilityState';

export const useOptionalColumns = (): DataColumn[] => {
  const [columnNames] = useSelectedColumnNamesState();
  const availableColumnMap = useAvailableColumnMap();
  const [isOptionalColumnVisible] = useColumnListVisibilityState(ColumnListType.Optional);

  return useMemo(() => {
    if (!isOptionalColumnVisible) {
      return [];
    }

    return columnNames
      .slice(indexOfSubject(columnNames) + 1)
      .map((name) => availableColumnMap.get(name))
      .filter(Boolean) as DataColumn[];
  }, [isOptionalColumnVisible, columnNames, availableColumnMap]);
};
