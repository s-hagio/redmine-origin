import { rootIssue } from '@/__fixtures__';
import { descriptionColumn } from '../../../../__fixtures__';
import { getIssueColumnValue } from '../getIssueColumnValue';

const issue = {
  ...rootIssue,
  description: 'DESCRIPTION!',
};

test('field.valuePathを元にIssueの持つ値を返す', () => {
  expect(getIssueColumnValue(descriptionColumn, issue)).toBe(issue.description);
});
