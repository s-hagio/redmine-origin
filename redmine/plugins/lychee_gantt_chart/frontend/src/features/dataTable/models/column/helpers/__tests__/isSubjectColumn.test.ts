import {
  idColumn,
  trackerColumn,
  parentSubjectColumn,
  startDateColumn,
  descriptionColumn,
  stringCustomFieldColumn,
  subjectColumn,
} from '../../../../__fixtures__';
import { isSubjectColumn } from '../isSubjectColumn';

test('Subjectカラムならtrueを返す', () => {
  expect(isSubjectColumn(subjectColumn)).toBe(true);
});

test('それ以外はfalseを返す', () => {
  expect(isSubjectColumn(idColumn)).toBe(false);
  expect(isSubjectColumn(trackerColumn)).toBe(false);
  expect(isSubjectColumn(parentSubjectColumn)).toBe(false);
  expect(isSubjectColumn(startDateColumn)).toBe(false);
  expect(isSubjectColumn(descriptionColumn)).toBe(false);
  expect(isSubjectColumn(stringCustomFieldColumn)).toBe(false);
});
