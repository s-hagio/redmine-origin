import { get } from '@/utils/object';
import { Version } from '@/models/version';
import { FieldValue } from '@/features/field';
import { DataColumn, ColumnName } from '../types';

const availableColumnNames = new Set<ColumnName>(['start_date', 'due_date']);

export const getVersionColumnContent = (column: DataColumn, version: Version): FieldValue => {
  if (!availableColumnNames.has(column.name)) {
    return null;
  }

  return get(version, column.field.contentPath ?? column.field.valuePath);
};
