import { ColumnName } from '@/models/query';
import { FieldDefinition, FieldFormat } from '@/features/field';

export type { ColumnName };

export type DataColumn = {
  name: ColumnName;
  field: FieldDefinition;
  format?: FieldFormat;
};

export type SubjectDataColumn = DataColumn & {
  partColumns: DataColumn[];
};

export const ColumnListType = {
  All: 'all',
  Optional: 'optional',
} as const;

export type ColumnListType = typeof ColumnListType[keyof typeof ColumnListType];
