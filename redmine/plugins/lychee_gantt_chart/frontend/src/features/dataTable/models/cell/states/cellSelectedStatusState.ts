import { atomFamily } from 'recoil';
import { CellIdentifier, SelectedCellStatus } from '../types';

export const cellSelectedStatusState = atomFamily<SelectedCellStatus | null, CellIdentifier | null>({
  key: 'dataTable/cellSelectedStatus',
  default: null,
});
