import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { selectedColumnNamesState } from '../../states';
import {
  descriptionColumn,
  idColumn,
  issueSubjectColumn,
  startDateColumn,
  stringCustomFieldColumn,
  trackerColumn,
} from '../../../../__fixtures__';
import { useMoveSelectedColumn } from '../useMoveSelectedColumn';

const initialSelectedColumnNames = [
  trackerColumn.name,
  idColumn.name,
  issueSubjectColumn.name,
  startDateColumn.name,
  descriptionColumn.name,
  stringCustomFieldColumn.name,
];

const render = () => {
  return renderRecoilHook(
    () => ({
      moveSelectedColumn: useMoveSelectedColumn(),
      selectedColumnNames: useRecoilValue(selectedColumnNamesState),
    }),
    {
      initializeState: ({ set }) => {
        set(selectedColumnNamesState, [...initialSelectedColumnNames]);
      },
    }
  );
};

test('与えられた2つのColumnの位置を入れ替える', () => {
  const { result } = render();

  act(() => result.current.moveSelectedColumn(trackerColumn, idColumn));

  expect(result.current.selectedColumnNames).toStrictEqual([
    idColumn.name,
    trackerColumn.name,
    issueSubjectColumn.name,
    startDateColumn.name,
    descriptionColumn.name,
    stringCustomFieldColumn.name,
  ]);

  act(() => result.current.moveSelectedColumn(descriptionColumn, stringCustomFieldColumn));

  expect(result.current.selectedColumnNames).toStrictEqual([
    idColumn.name,
    trackerColumn.name,
    issueSubjectColumn.name,
    startDateColumn.name,
    stringCustomFieldColumn.name,
    descriptionColumn.name,
  ]);
});

test('題名部品のColumn以外は題名より上に並び替えることは出来ない', () => {
  const { result } = render();

  act(() => result.current.moveSelectedColumn(startDateColumn, issueSubjectColumn));
  expect(result.current.selectedColumnNames).toStrictEqual(initialSelectedColumnNames);

  act(() => result.current.moveSelectedColumn(startDateColumn, trackerColumn));
  expect(result.current.selectedColumnNames).toStrictEqual(initialSelectedColumnNames);
});
