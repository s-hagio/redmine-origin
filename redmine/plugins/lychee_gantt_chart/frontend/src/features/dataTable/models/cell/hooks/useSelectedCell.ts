import { useRecoilValue } from 'recoil';
import { selectedCellState } from '../states';

export const useSelectedCell = () => useRecoilValue(selectedCellState);
