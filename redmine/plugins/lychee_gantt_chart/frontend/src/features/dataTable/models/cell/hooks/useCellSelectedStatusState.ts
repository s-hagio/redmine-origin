import { useRecoilValue } from 'recoil';
import { ColumnName } from '@/models/query';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { RowID } from '@/features/row';
import { cellSelectedStatusState } from '../states';
import { IssueSelectedCellStatus, SelectedCellStatus } from '../types';

export const useCellSelectedStatusState = <
  T extends RowID,
  R = T extends IssueResourceIdentifier ? IssueSelectedCellStatus : SelectedCellStatus
>(
  rowId: T | null,
  columnName: ColumnName
): R | null => {
  const status = useRecoilValue(cellSelectedStatusState(rowId ? `${rowId}-${columnName}` : null));

  return status ? (status as R) : null;
};
