import { ColumnName, DataColumn } from '../types';
import { useColumn } from './useColumn';

export const useColumnOrThrow = (name: ColumnName): DataColumn => {
  const column = useColumn(name);

  if (!column) {
    throw new Error(`Column "${name}" is not found.`);
  }

  return column;
};
