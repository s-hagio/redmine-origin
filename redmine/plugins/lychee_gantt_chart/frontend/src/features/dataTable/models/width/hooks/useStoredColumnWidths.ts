import { useRecoilValue } from 'recoil';
import { storedColumnWidthsState } from '../states';

export const useStoredColumnWidths = () => useRecoilValue(storedColumnWidthsState);
