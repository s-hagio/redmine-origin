import { rootIssue } from '@/__fixtures__';
import { issueSubjectColumn, descriptionColumn } from '../../../../__fixtures__';
import { getIssueColumnContent } from '../getIssueColumnContent';

const issue = {
  ...rootIssue,
  formattedDescription: 'FORMATTED DESCRIPTION!',
};

test('field.contentPathなし', () => {
  expect(getIssueColumnContent(issueSubjectColumn, issue)).toBe(issue.subject);
});

test('field.contentPathあり', () => {
  expect(getIssueColumnContent(descriptionColumn, issue)).toBe(issue.formattedDescription);
});
