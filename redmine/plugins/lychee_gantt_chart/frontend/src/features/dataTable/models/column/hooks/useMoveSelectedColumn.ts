import { useRecoilCallback } from 'recoil';
import { DataColumn } from '../types';
import { indexOfSubject, isLabelColumn } from '../helpers';
import { selectedColumnNamesState } from '../states';

export type MoveSelectedColumn = (sourceColumn: DataColumn, targetColumn: DataColumn) => void;

export const useMoveSelectedColumn = (): MoveSelectedColumn => {
  return useRecoilCallback(({ set }) => {
    return (sourceColumn, targetColumn) => {
      set(selectedColumnNamesState, (columnNames) => {
        const subjectIndex = indexOfSubject(columnNames);
        const targetIndex = columnNames.indexOf(targetColumn.name);

        if (!isLabelColumn(sourceColumn) && subjectIndex >= targetIndex) {
          return columnNames;
        }

        const newColumnNames = [...columnNames];
        const sourceIndex = columnNames.indexOf(sourceColumn.name);

        newColumnNames.splice(sourceIndex, 1);
        newColumnNames.splice(targetIndex, 0, sourceColumn.name);

        return newColumnNames;
      });
    };
  }, []);
};
