import { useSetRecoilState } from 'recoil';
import { rowNumberColumnWidthState } from '../states';

export const useSetRowNumberColumnWidth = () => useSetRecoilState(rowNumberColumnWidthState);
