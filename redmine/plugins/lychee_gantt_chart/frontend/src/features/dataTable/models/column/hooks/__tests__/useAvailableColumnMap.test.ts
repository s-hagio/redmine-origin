import { renderHook } from '@testing-library/react';
import { Settings, useSettings } from '@/models/setting';
import { FieldDefinition, useFieldDefinitionMap } from '@/features/field';
import {
  descriptionFieldDefinition,
  idFieldDefinition,
  intCustomFieldDefinition,
  issueSubjectFieldDefinition,
  parentSubjectFieldDefinition,
  startDateFieldDefinition,
  stringCustomFieldDefinition,
  trackerFieldDefinition,
} from '@/__fixtures__';
import {
  descriptionColumn,
  idColumn,
  parentSubjectColumn,
  stringCustomFieldColumn,
  issueSubjectColumn,
} from '../../../../__fixtures__';
import { useAvailableColumnMap } from '../useAvailableColumnMap';
import { DataColumn } from '../../types';

vi.mock('@/models/setting/hooks', () => ({
  useSettings: vi.fn(),
}));

vi.mock('@/features/field', async () => ({
  ...(await vi.importActual<object>('@/features/field')),
  useFieldDefinitionMap: vi.fn(),
}));

beforeEach(() => {
  vi.mocked(useSettings).mockReturnValue({
    availableColumnNames: [
      'id',
      'subject',
      'parent.subject',
      'description',
      stringCustomFieldDefinition.name,
    ],
  } as Settings);

  vi.mocked(useFieldDefinitionMap).mockReturnValue(
    new Map<string, FieldDefinition>([
      [idFieldDefinition.name, idFieldDefinition],
      [trackerFieldDefinition.name, trackerFieldDefinition],
      [issueSubjectFieldDefinition.name, issueSubjectFieldDefinition],
      [parentSubjectFieldDefinition.name, parentSubjectFieldDefinition],
      [startDateFieldDefinition.name, startDateFieldDefinition],
      [descriptionFieldDefinition.name, descriptionFieldDefinition],
      [stringCustomFieldDefinition.name, stringCustomFieldDefinition],
      [intCustomFieldDefinition.name, intCustomFieldDefinition],
    ])
  );
});

test('利用可能な列情報（Column）リストを返す', () => {
  const { result } = renderHook(() => useAvailableColumnMap());

  expect(result.current).toStrictEqual(
    new Map<string, DataColumn>([
      [idColumn.name, idColumn],
      [issueSubjectColumn.name, issueSubjectColumn],
      [parentSubjectColumn.name, parentSubjectColumn],
      [descriptionColumn.name, descriptionColumn],
      [stringCustomFieldColumn.name, stringCustomFieldColumn],
    ])
  );
});
