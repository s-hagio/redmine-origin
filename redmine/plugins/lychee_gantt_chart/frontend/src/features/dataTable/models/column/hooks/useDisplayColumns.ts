import { useMemo } from 'react';
import { ColumnListType, DataColumn } from '../types';
import { useColumnListVisibilityState } from './useColumnListVisibilityState';
import { useOptionalColumns } from './useOptionalColumns';
import { useSubjectColumn } from './useSubjectColumn';

export const useDisplayColumns = (): DataColumn[] => {
  const subjectColumn = useSubjectColumn();
  const optionalColumns = useOptionalColumns();
  const [isAllColumnVisible] = useColumnListVisibilityState(ColumnListType.All);

  return useMemo(() => {
    if (!isAllColumnVisible) {
      return [];
    }

    return [subjectColumn, ...optionalColumns];
  }, [isAllColumnVisible, subjectColumn, optionalColumns]);
};
