import { useMemo } from 'react';
import { useSettings } from '@/models/setting';
import { FieldFormat, useFieldDefinitionMap } from '@/features/field';
import { DataColumn, ColumnName } from '../types';

const columnFormatMap: Record<string, FieldFormat> = {
  [FieldFormat.FormattableText]: FieldFormat.PlainText,
  [FieldFormat.ParentIssueId]: FieldFormat.Id,
  [FieldFormat.AssignedTo]: FieldFormat.Principal,
  [FieldFormat.DoneRatio]: FieldFormat.Percent,
} as const;

export const useAvailableColumnMap = (): ReadonlyMap<ColumnName, DataColumn> => {
  const settings = useSettings();
  const fieldDefinitionMap = useFieldDefinitionMap();

  return useMemo(() => {
    const availableColumnNames = new Set(settings.availableColumnNames);

    return new Map(
      [...availableColumnNames].reduce<[ColumnName, DataColumn][]>((memo, name) => {
        const field = fieldDefinitionMap.get(name);

        if (!field) {
          return memo;
        }

        const column: DataColumn = { name, field };

        if (columnFormatMap[field.format]) {
          column.format = columnFormatMap[field.format];
        }

        memo.push([name, column]);

        return memo;
      }, [])
    );
  }, [settings.availableColumnNames, fieldDefinitionMap]);
};
