import { atomFamily } from 'recoil';
import { persistJSON, restorePersistedJSON } from '@/utils/storage';
import { ColumnListType } from '../types';
import { COLUMN_LIST_VISIBLE_STORED_KEY } from '../constants';

type StoredColumnVisibility = Partial<Record<ColumnListType, boolean>>;

const restore = () => {
  return restorePersistedJSON<StoredColumnVisibility>(COLUMN_LIST_VISIBLE_STORED_KEY, {});
};

export const columnVisibilityState = atomFamily<boolean, ColumnListType>({
  key: 'dataTable/columnVisibility',
  default: (type) => {
    const persistedJSON = restore();

    return persistedJSON[type] ?? true;
  },
  effects: (type) => [
    ({ onSet }) => {
      onSet((newValue) => {
        persistJSON(COLUMN_LIST_VISIBLE_STORED_KEY, {
          ...restore(),
          [type]: newValue,
        });
      });
    },
  ],
});
