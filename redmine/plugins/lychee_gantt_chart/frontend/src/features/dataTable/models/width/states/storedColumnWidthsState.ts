import { atom } from 'recoil';
import { persistJSON, restorePersistedJSON } from '@/utils/storage';
import { ColumnName } from '../../column';
import { STORED_WIDTHS_KEY } from '../constants';

export const storedColumnWidthsState = atom<Record<ColumnName, number>>({
  key: 'dataTable/storedColumnWidths',
  default: {},
  effects: [
    ({ setSelf, onSet }) => {
      setSelf(restorePersistedJSON(STORED_WIDTHS_KEY) ?? {});

      onSet((columnWidths) => {
        persistJSON(STORED_WIDTHS_KEY, columnWidths);
      });
    },
  ],
});
