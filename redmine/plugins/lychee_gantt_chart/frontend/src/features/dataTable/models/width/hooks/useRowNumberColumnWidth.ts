import { useRecoilValue } from 'recoil';
import { rowNumberColumnWidthState } from '../states';

export const useRowNumberColumnWidth = () => useRecoilValue(rowNumberColumnWidthState);
