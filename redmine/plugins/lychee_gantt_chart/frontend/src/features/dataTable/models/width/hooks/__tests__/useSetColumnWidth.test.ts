import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { useSetColumnWidth } from '../useSetColumnWidth';
import { storedColumnWidthsState } from '../../states';
import { MIN_WIDTH } from '../../constants';

test('幅またはアップデータ関数を渡して幅をstateにセット', () => {
  const { result } = renderRecoilHook(() => ({
    setColumnWidth: useSetColumnWidth(),
    widths: useRecoilValue(storedColumnWidthsState),
  }));

  act(() => result.current.setColumnWidth('startDate', 222));
  expect(result.current.widths.startDate).toBe(222);

  act(() => result.current.setColumnWidth('startDate', (currentWidth) => currentWidth + 10));
  expect(result.current.widths.startDate).toBe(232);
});

test('指定幅が最低幅を下回る場合は最低幅をセット', () => {
  const { result } = renderRecoilHook(() => ({
    setColumnWidth: useSetColumnWidth(),
    widths: useRecoilValue(storedColumnWidthsState),
  }));

  act(() => result.current.setColumnWidth('subject', 100));
  expect(result.current.widths.subject).toBe(100);

  act(() => result.current.setColumnWidth('subject', MIN_WIDTH - 1));
  expect(result.current.widths.subject).toBe(MIN_WIDTH);
});
