import { useRecoilCallback } from 'recoil';
import { availableRowsState, RowID } from '@/features/row';
import { MoveDirection } from '../types';
import { selectedCellState } from '../states';
import { ColumnName, useDisplayColumns } from '../../column';
import { useSelectCell } from './useSelectCell';
import { isCellSelectableRow } from './helpers';

export const useMoveSelectedCellCursor = () => {
  const displayColumns = useDisplayColumns();
  const selectCell = useSelectCell();

  return useRecoilCallback(
    ({ snapshot }) => {
      const moveRow = (rowId: RowID, columnName: ColumnName, direction: 'up' | 'down') => {
        const availableRows = snapshot.getLoadable(availableRowsState).getValue();
        const moveValue = direction === 'up' ? -1 : 1;

        let index = availableRows.findIndex((row) => row.id === rowId);

        do {
          index += moveValue;
          const targetRow = availableRows[index];

          if (targetRow && isCellSelectableRow(targetRow)) {
            selectCell(targetRow.id, columnName);
            break;
          }
        } while (availableRows[index]);
      };

      const moveColumn = (rowId: RowID, columnName: ColumnName, direction: 'left' | 'right') => {
        const index = displayColumns.findIndex(({ name }) => name === columnName);
        const newIndex = (() => {
          const newIndex = direction === 'left' ? index - 1 : index + 1;
          const lastIndex = displayColumns.length - 1;

          if (newIndex === -1 || newIndex > lastIndex) {
            return index;
          }

          return newIndex;
        })();

        selectCell(rowId, displayColumns[newIndex].name);
      };

      return (direction: MoveDirection) => {
        const selectedCell = snapshot.getLoadable(selectedCellState).getValue();

        if (!selectedCell) {
          return;
        }

        switch (direction) {
          case 'up':
          case 'down':
            moveRow(selectedCell.rowId, selectedCell.columnName, direction);
            break;
          case 'left':
          case 'right':
            moveColumn(selectedCell.rowId, selectedCell.columnName, direction);
            break;
        }
      };
    },
    [displayColumns, selectCell]
  );
};
