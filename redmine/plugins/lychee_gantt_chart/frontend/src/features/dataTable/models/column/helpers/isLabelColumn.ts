import { DataColumn } from '../types';

const labelColumnNames = ['id', 'tracker'];

export const isLabelColumn = (column: DataColumn): boolean => labelColumnNames.includes(column.name);
