import { useMemo } from 'react';
import { DataColumn, SubjectDataColumn } from '../types';
import { indexOfSubject } from '../helpers';
import { useAvailableColumnMap } from './useAvailableColumnMap';
import { useSelectedColumnNamesState } from './useSelectedColumnNamesState';

export const useSubjectColumn = (): SubjectDataColumn => {
  const [columnNames] = useSelectedColumnNamesState();
  const availableColumnMap = useAvailableColumnMap();

  return useMemo(() => {
    return {
      ...(availableColumnMap.get('subject') as DataColumn),
      partColumns: columnNames
        .slice(0, indexOfSubject(columnNames))
        .map((name) => availableColumnMap.get(name))
        .filter(Boolean) as DataColumn[],
    };
  }, [columnNames, availableColumnMap]);
};
