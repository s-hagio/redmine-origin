import { get } from '@/utils/object';
import { Project } from '@/models/project';
import { FieldValue } from '@/features/field';
import { DataColumn, ColumnName } from '../types';

const availableColumnNames = new Set<ColumnName>(['start_date', 'due_date']);

export const getProjectColumnContent = (column: DataColumn, project: Project): FieldValue => {
  if (!availableColumnNames.has(column.name)) {
    return null;
  }

  return get(project, column.field.contentPath ?? column.field.valuePath);
};
