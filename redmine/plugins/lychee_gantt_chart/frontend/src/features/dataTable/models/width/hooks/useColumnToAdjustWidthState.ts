import { useRecoilState } from 'recoil';
import { columnToAdjustWidthState } from '../states';

export const useColumnToAdjustWidthState = () => useRecoilState(columnToAdjustWidthState);
