import { useRecoilCallback } from 'recoil';
import { ColumnName } from '@/models/query';
import { isIssueResourceIdentifier } from '@/models/coreResource';
import { RowID } from '@/features/row';
import { selectedCellState } from '../states';
import { useIsEditableCell } from './useIsEditableCell';

export const useSelectCell = () => {
  const isEditableCell = useIsEditableCell();

  return useRecoilCallback(
    ({ set, reset }) => {
      return (rowId: RowID | null, columnName: ColumnName | null) => {
        if (rowId !== null && !isIssueResourceIdentifier(rowId)) {
          return;
        }

        if (!rowId || !columnName) {
          reset(selectedCellState);
          return;
        }

        set(selectedCellState, {
          rowId,
          columnName,
          isEditable: isEditableCell(rowId, columnName),
          isEditing: false,
        });
      };
    },
    [isEditableCell]
  );
};
