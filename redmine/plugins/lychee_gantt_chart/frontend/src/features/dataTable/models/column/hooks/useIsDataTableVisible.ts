import { useRecoilValue } from 'recoil';
import { ColumnListType } from '../types';
import { columnVisibilityState } from '../states';

export const useIsDataTableVisible = () => useRecoilValue(columnVisibilityState(ColumnListType.All));
