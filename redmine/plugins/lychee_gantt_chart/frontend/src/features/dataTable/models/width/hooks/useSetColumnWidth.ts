import { useRecoilCallback } from 'recoil';
import { ColumnName } from '@/models/query';
import { storedColumnWidthsState } from '../states';
import { getDefaultWidth } from '../helpers';
import { MIN_WIDTH } from '../constants';

type SetColumnWidth = (name: ColumnName, width: number | ((currentWidth: number) => number)) => void;

export const useSetColumnWidth = (): SetColumnWidth => {
  return useRecoilCallback(({ set }) => {
    return (name, widthOrUpdater) => {
      set(storedColumnWidthsState, (current) => {
        const newWidth =
          typeof widthOrUpdater === 'function'
            ? widthOrUpdater(current[name] ?? getDefaultWidth(name))
            : widthOrUpdater;

        return {
          ...current,
          [name]: Math.max(newWidth, MIN_WIDTH),
        };
      });
    };
  }, []);
};
