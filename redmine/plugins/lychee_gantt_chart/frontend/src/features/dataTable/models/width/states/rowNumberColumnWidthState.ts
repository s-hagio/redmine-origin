import { DefaultValue, selector } from 'recoil';
import { ROW_NUMBER_COLUMN_NAME } from '../../column';
import { storedColumnWidthsState } from './storedColumnWidthsState';

const DEFAULT_WIDTH = 50;

export const rowNumberColumnWidthState = selector<number>({
  key: 'dataTable/rowNumberColumnWidth',
  get: ({ get }) => {
    const storedColumnWidthsMap = get(storedColumnWidthsState);

    return storedColumnWidthsMap[ROW_NUMBER_COLUMN_NAME] ?? DEFAULT_WIDTH;
  },
  set: ({ set }, newValue) => {
    if (newValue instanceof DefaultValue) {
      return;
    }

    set(storedColumnWidthsState, (current) => ({
      ...current,
      [ROW_NUMBER_COLUMN_NAME]: newValue,
    }));
  },
});
