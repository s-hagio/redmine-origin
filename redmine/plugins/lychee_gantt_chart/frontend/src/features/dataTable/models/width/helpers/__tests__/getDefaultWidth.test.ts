import { getDefaultWidth } from '../getDefaultWidth';

test('ColumnNameに応じたデフォルト幅を返す', () => {
  expect(getDefaultWidth('subject')).toBe(200);
  expect(getDefaultWidth('startDate')).toBe(60);
  expect(getDefaultWidth('description')).toBe(60);
});
