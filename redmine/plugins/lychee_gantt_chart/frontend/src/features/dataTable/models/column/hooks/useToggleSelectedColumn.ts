import { useRecoilCallback } from 'recoil';
import { ColumnName } from '@/models/query';
import { selectedColumnNamesState } from '../states';

type ToggleSelectedColumn = (name: ColumnName) => void;

export const useToggleSelectedColumn = (): ToggleSelectedColumn => {
  return useRecoilCallback(({ set }) => {
    return (targetColumnName: string) => {
      set(selectedColumnNamesState, (columnNames) => {
        const newColumnNames = [...columnNames];
        const index = newColumnNames.findIndex((name) => name === targetColumnName);

        if (index !== -1) {
          newColumnNames.splice(index, 1);
        } else {
          newColumnNames.push(targetColumnName);
        }

        return newColumnNames;
      });
    };
  });
};
