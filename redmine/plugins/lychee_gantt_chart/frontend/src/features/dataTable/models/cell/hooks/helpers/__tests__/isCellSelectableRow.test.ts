import { mainProjectVersionRow, newStatusGroupRow, rootIssueRow, rootProjectRow } from '@/__fixtures__';
import { isCellSelectableRow } from '../isCellSelectableRow';

test.each([rootIssueRow])('$resourceTypeのRowはtrueを返す', (row) => {
  expect(isCellSelectableRow(row)).toBe(true);
});

test.each([rootProjectRow, mainProjectVersionRow, newStatusGroupRow])(
  '$resourceTypeのRowはfalseを返す',
  (row) => {
    expect(isCellSelectableRow(row)).toBe(false);
  }
);
