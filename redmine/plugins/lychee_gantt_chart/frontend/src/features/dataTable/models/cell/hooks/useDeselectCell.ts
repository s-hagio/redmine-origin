import { useCallback } from 'react';
import { useSelectCell } from './useSelectCell';

export const useDeselectCell = () => {
  const selectCell = useSelectCell();

  return useCallback(() => {
    selectCell(null, null);
  }, [selectCell]);
};
