import { atom, DefaultValue, selector } from 'recoil';
import { CellIdentifier, SelectedCellStatus } from '../types';
import { cellSelectedStatusState } from './cellSelectedStatusState';

export const selectedCellState = selector<SelectedCellStatus | null>({
  key: 'dataTable/selectedCell',
  get: ({ get }) => {
    const currentId = get(currentSelectedCellIdentifierState);

    return currentId ? get(cellSelectedStatusState(currentId)) : null;
  },
  set: ({ get, set, reset }, newValue) => {
    const currentId = get(currentSelectedCellIdentifierState);

    if (currentId) {
      reset(cellSelectedStatusState(currentId));
    }

    if (!newValue || newValue instanceof DefaultValue) {
      reset(currentSelectedCellIdentifierState);
      return;
    }

    const newIdentifier: CellIdentifier = `${newValue.rowId}-${newValue.columnName}`;

    set(currentSelectedCellIdentifierState, newIdentifier);
    set(cellSelectedStatusState(newIdentifier), newValue);
  },
});

const currentSelectedCellIdentifierState = atom<CellIdentifier | null>({
  key: 'dataTable/currentSelectedCellIdentifier',
  default: null,
});
