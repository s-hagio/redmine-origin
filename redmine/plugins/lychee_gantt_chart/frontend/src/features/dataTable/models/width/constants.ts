export const STORED_WIDTHS_KEY = 'lycheeGantt:columnWidths';

export const MIN_WIDTH = 20;
export const DEFAULT_WIDTH = 60;
export const SUBJECT_COLUMN_DEFAULT_WIDTH = 200;
