import { ColumnName } from '../types';
import { useAvailableColumnMap } from './useAvailableColumnMap';

export const useColumn = (name: ColumnName) => {
  const map = useAvailableColumnMap();

  return map.get(name);
};
