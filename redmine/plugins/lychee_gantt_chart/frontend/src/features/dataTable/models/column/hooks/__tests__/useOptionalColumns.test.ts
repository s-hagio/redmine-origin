import { renderHook } from '@testing-library/react';
import {
  idColumn,
  trackerColumn,
  issueSubjectColumn,
  parentSubjectColumn,
  stringCustomFieldColumn,
} from '../../../../__fixtures__';
import { useAvailableColumnMap } from '../useAvailableColumnMap';
import { useSelectedColumnNamesState } from '../useSelectedColumnNamesState';
import { useColumnListVisibilityState } from '../useColumnListVisibilityState';
import { useOptionalColumns } from '../useOptionalColumns';
import { ColumnListType } from '../../types';

vi.mock('../useSelectedColumnNamesState', () => ({
  useSelectedColumnNamesState: vi.fn(),
}));

vi.mock('../useAvailableColumnMap', () => ({
  useAvailableColumnMap: vi.fn(),
}));

vi.mock('../useColumnListVisibilityState', () => ({
  useColumnListVisibilityState: vi.fn().mockReturnValue([true, vi.fn()]),
}));

beforeEach(() => {
  vi.mocked(useSelectedColumnNamesState).mockReturnValue([
    ['tracker', 'id', 'subject', 'description', 'parent.subject', stringCustomFieldColumn.name],
    vi.fn(),
  ]);

  vi.mocked(useAvailableColumnMap).mockReturnValue(
    new Map([
      [idColumn.name, idColumn],
      [trackerColumn.name, trackerColumn],
      [issueSubjectColumn.name, issueSubjectColumn],
      [parentSubjectColumn.name, parentSubjectColumn],
      [stringCustomFieldColumn.name, stringCustomFieldColumn],
    ])
  );
});

test('subjectを除いた利用可能な表示項目（Column）を返す', () => {
  const { result } = renderHook(() => useOptionalColumns());

  expect(result.current).toStrictEqual([parentSubjectColumn, stringCustomFieldColumn]);
  expect(useColumnListVisibilityState).toHaveBeenCalledWith(ColumnListType.Optional);
});

test('オプション項目が非表示の場合は空の配列を返す', () => {
  vi.mocked(useColumnListVisibilityState).mockReturnValueOnce([false, vi.fn()]);

  const { result } = renderHook(() => useOptionalColumns());

  expect(result.current).toStrictEqual([]);
  expect(useColumnListVisibilityState).toHaveBeenCalledWith(ColumnListType.Optional);
});
