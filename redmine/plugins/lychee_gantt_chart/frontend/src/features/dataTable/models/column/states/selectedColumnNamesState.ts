import { atom, selector } from 'recoil';
import { persistJSON, restorePersistedJSON } from '@/utils/storage';
import { defaultIssueQueryState } from '@/models/query';
import { ColumnName } from '../types';
import { SELECTED_COLUMN_NAMES_STORED_KEY, SUBJECT_COLUMN_NAME } from '../constants';

export const selectedColumnNamesState = atom<ColumnName[]>({
  key: 'dataTable/selectedColumnNames',
  default: selector({
    key: 'dataTable/selectedColumnNames/default',
    get: ({ get }) => {
      const { c: querySelectedColumnNames } = get(defaultIssueQueryState);

      const storedSelectedColumnNames = restorePersistedJSON(SELECTED_COLUMN_NAMES_STORED_KEY, null);
      const selectedColumnNames = storedSelectedColumnNames ?? querySelectedColumnNames;

      if (!selectedColumnNames.includes(SUBJECT_COLUMN_NAME)) {
        return [SUBJECT_COLUMN_NAME, ...selectedColumnNames];
      }

      return selectedColumnNames;
    },
  }),
  effects: [
    ({ onSet }) => {
      onSet((selectedColumnNames) => {
        persistJSON(SELECTED_COLUMN_NAMES_STORED_KEY, selectedColumnNames);
      });
    },
  ],
});
