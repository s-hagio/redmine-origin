import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { ROW_NUMBER_COLUMN_NAME } from '../../../column';
import { storedColumnWidthsState } from '../storedColumnWidthsState';
import { rowNumberColumnWidthState } from '../rowNumberColumnWidthState';

test('get: 保存済みwidthがある場合はそれを返し、なければデフォルト幅を返す', () => {
  const { result } = renderRecoilHook(() => ({
    width: useRecoilValue(rowNumberColumnWidthState),
    setStoredWidth: useSetRecoilState(storedColumnWidthsState),
  }));

  expect(result.current.width, 'Default').toBe(50);

  act(() => {
    result.current.setStoredWidth({
      [ROW_NUMBER_COLUMN_NAME]: 111,
    });
  });

  expect(result.current.width, 'Saved').toBe(111);
});

test('set: 保存済みwidthを更新する', () => {
  const { result } = renderRecoilHook(() => ({
    setWidth: useSetRecoilState(rowNumberColumnWidthState),
    storedWidthMap: useRecoilValue(storedColumnWidthsState),
  }));

  act(() => {
    result.current.setWidth(222);
  });

  expect(result.current.storedWidthMap[ROW_NUMBER_COLUMN_NAME]).toBe(222);
});
