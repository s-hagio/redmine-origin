import { useRecoilState } from 'recoil';
import { ColumnListType } from '../types';
import { columnVisibilityState } from '../states';

export const useColumnListVisibilityState = (type: ColumnListType) => {
  return useRecoilState(columnVisibilityState(type));
};
