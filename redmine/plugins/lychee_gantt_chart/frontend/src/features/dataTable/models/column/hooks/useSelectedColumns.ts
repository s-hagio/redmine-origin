import { useMemo } from 'react';
import { DataColumn } from '../types';
import { useAvailableColumnMap } from './useAvailableColumnMap';
import { useSelectedColumnNamesState } from './useSelectedColumnNamesState';

export const useSelectedColumns = (): DataColumn[] => {
  const availableColumnMap = useAvailableColumnMap();
  const [selectedColumnNames] = useSelectedColumnNamesState();

  return useMemo(() => {
    return selectedColumnNames
      .map((name) => availableColumnMap.get(name))
      .filter((column): column is DataColumn => !!column);
  }, [availableColumnMap, selectedColumnNames]);
};
