import { ColumnName } from '@/models/query';
import { SUBJECT_COLUMN_NAME } from '../constants';

export const indexOfSubject = (columnNames: ColumnName[]) => {
  return Math.max(columnNames.indexOf(SUBJECT_COLUMN_NAME), 0);
};
