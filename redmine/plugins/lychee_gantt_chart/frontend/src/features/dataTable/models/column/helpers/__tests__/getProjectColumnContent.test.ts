import { mainProject } from '@/__fixtures__';
import { descriptionColumn, startDateColumn } from '../../../../__fixtures__';
import { getProjectColumnContent } from '../getProjectColumnContent';

const project = {
  ...mainProject,
  startDate: '2022-01-01',
};

test('field.contentPathまたはfield.valuePathを元にProjectの持つ値を返す', () => {
  expect(getProjectColumnContent(startDateColumn, project)).toBe(project.startDate);
  expect(getProjectColumnContent(descriptionColumn, project)).toBe(null);
});
