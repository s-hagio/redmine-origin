import { renderHook } from '@testing-library/react';
import { useSubjectColumn } from '../useSubjectColumn';
import { useOptionalColumns } from '../useOptionalColumns';
import { useColumnListVisibilityState } from '../useColumnListVisibilityState';
import { ColumnListType } from '../../types';
import { descriptionColumn, startDateColumn, subjectColumn } from '../../../../__fixtures__';
import { useDisplayColumns } from '../useDisplayColumns';

vi.mock('../useSubjectColumn', () => ({
  useSubjectColumn: vi.fn(),
}));

vi.mock('../useOptionalColumns', () => ({
  useOptionalColumns: vi.fn(),
}));

vi.mock('../useColumnListVisibilityState', () => ({
  useColumnListVisibilityState: vi.fn(),
}));

beforeEach(() => {
  vi.mocked(useSubjectColumn).mockReturnValue(subjectColumn);
  vi.mocked(useOptionalColumns).mockReturnValue([startDateColumn, descriptionColumn]);
});

test('SubjectColumnとオプション項目リストを合わせて返す', () => {
  vi.mocked(useColumnListVisibilityState).mockReturnValue([true, vi.fn()]);

  const { result } = renderHook(() => useDisplayColumns());
  expect(result.current).toStrictEqual([subjectColumn, startDateColumn, descriptionColumn]);
  expect(useColumnListVisibilityState).toBeCalledWith(ColumnListType.All);
});

test('すべての列が表示されていない時は空の配列を返す', () => {
  vi.mocked(useColumnListVisibilityState).mockReturnValue([false, vi.fn()]);

  const { result } = renderHook(() => useDisplayColumns());
  expect(result.current).toStrictEqual([]);
  expect(useColumnListVisibilityState).toBeCalledWith(ColumnListType.All);
});
