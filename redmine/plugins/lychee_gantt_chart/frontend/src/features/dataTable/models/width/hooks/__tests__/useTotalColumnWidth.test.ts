import { renderHook } from '@testing-library/react';
import { useTotalColumnWidth } from '../useTotalColumnWidth';

vi.mock('../useDisplayColumnWidths', () => ({
  useDisplayColumnWidths: vi.fn().mockReturnValue(
    new Map([
      ['startDate', 10],
      ['subject', 100],
      ['description', 30],
    ])
  ),
}));

test('表示列幅の合計を返す', () => {
  const { result } = renderHook(() => useTotalColumnWidth());

  expect(result.current).toBe(140);
});
