import { act, renderHook } from '@testing-library/react';
import { useDeselectCell } from '../useDeselectCell';

const selectCellMock = vi.hoisted(() => vi.fn());

vi.mock('../useSelectCell', () => ({
  useSelectCell: vi.fn().mockReturnValue(selectCellMock),
}));

test('selectCellをnull, nullで実行する', () => {
  const { result } = renderHook(() => useDeselectCell());

  act(() => {
    result.current();
  });

  expect(selectCellMock).toBeCalledWith(null, null);
});
