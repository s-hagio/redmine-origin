import { renderHook } from '@testing-library/react';
import { COLLAPSIBLE_BUTTON_COLUMN_WIDTH, useIsDataTableVisible } from '../../../column';
import { DEFAULT_WIDTH, SUBJECT_COLUMN_DEFAULT_WIDTH } from '../../constants';
import { useStoredColumnWidths } from '../useStoredColumnWidths';
import { useDisplayColumnWidths } from '../useDisplayColumnWidths';

vi.mock('../../../column', async () => ({
  ...(await vi.importActual<object>('../../../column')),
  useOptionalColumns: vi.fn().mockReturnValue([{ name: 'status' }, { name: 'startDate' }]),
  useIsDataTableVisible: vi.fn().mockReturnValue(true),
}));

vi.mock('../useStoredColumnWidths', () => ({
  useStoredColumnWidths: vi.fn(),
}));

const useRowNumberColumnWidthMock = vi.hoisted(() => vi.fn());
const rowNumberColumnWidth = 55;

vi.mock('../useRowNumberColumnWidth', () => ({
  useRowNumberColumnWidth: useRowNumberColumnWidthMock,
}));

beforeEach(() => {
  useRowNumberColumnWidthMock.mockReturnValue(rowNumberColumnWidth);
});

test('表示列の幅をMapで返す', () => {
  vi.mocked(useStoredColumnWidths).mockReturnValue({});

  const { result } = renderHook(() => useDisplayColumnWidths());

  expect([...result.current]).toStrictEqual([
    ['rowNumber', rowNumberColumnWidth],
    ['subject', SUBJECT_COLUMN_DEFAULT_WIDTH],
    ['status', DEFAULT_WIDTH],
    ['startDate', DEFAULT_WIDTH],
  ]);
});

test('保持してる列幅があれば適用する', () => {
  const storedWidths = {
    subject: SUBJECT_COLUMN_DEFAULT_WIDTH + 10,
    status: DEFAULT_WIDTH + 20,
  } as const;

  vi.mocked(useStoredColumnWidths).mockReturnValue(storedWidths);

  const { result } = renderHook(() => useDisplayColumnWidths());

  expect([...result.current]).toStrictEqual([
    ['rowNumber', rowNumberColumnWidth],
    ['subject', storedWidths.subject],
    ['status', storedWidths.status],
    ['startDate', DEFAULT_WIDTH],
  ]);
});

test('オプション項目が非表示の時はその分減らしてMapを返す', () => {
  vi.mocked(useIsDataTableVisible).mockReturnValue(false);

  const { result } = renderHook(() => useDisplayColumnWidths());

  expect([...result.current]).toStrictEqual([
    ['rowNumber', rowNumberColumnWidth],
    ['collapsibleButton', COLLAPSIBLE_BUTTON_COLUMN_WIDTH],
  ]);
});
