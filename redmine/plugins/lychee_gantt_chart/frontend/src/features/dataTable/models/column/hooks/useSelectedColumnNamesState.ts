import { useRecoilState } from 'recoil';
import { selectedColumnNamesState } from '../states';

export const useSelectedColumnNamesState = () => useRecoilState(selectedColumnNamesState);
