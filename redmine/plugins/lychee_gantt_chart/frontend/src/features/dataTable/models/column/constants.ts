export const SUBJECT_COLUMN_NAME = 'subject';
export const ROW_NUMBER_COLUMN_NAME = 'rowNumber';
export const COLLAPSIBLE_BUTTON_COLUMN_NAME = 'collapsibleButton';
export const COLLAPSIBLE_BUTTON_COLUMN_WIDTH = 24;

export const COLUMN_LIST_VISIBLE_STORED_KEY = 'lycheeGantt:columnListVisible';
export const SELECTED_COLUMN_NAMES_STORED_KEY = 'lycheeGantt:selectedColumnNames';
