import { useCallback } from 'react';
import { useRecoilValue, waitForAll } from 'recoil';
import { ColumnListType, ColumnName } from '../types';
import { SUBJECT_COLUMN_NAME } from '../constants';
import { columnVisibilityState } from '../states';
import { useSelectedColumnNamesState } from './useSelectedColumnNamesState';

type IsColumnVisible = (name: ColumnName) => boolean;

export const useIsColumnVisible = (): IsColumnVisible => {
  const visibleState = useRecoilValue(
    waitForAll({
      all: columnVisibilityState(ColumnListType.All),
      optional: columnVisibilityState(ColumnListType.Optional),
    })
  );
  const [selectedColumnNames] = useSelectedColumnNamesState();

  return useCallback(
    (name) => {
      if (!visibleState.all) {
        return false;
      }

      if (!visibleState.optional && name !== SUBJECT_COLUMN_NAME) {
        return false;
      }

      return selectedColumnNames.includes(name);
    },
    [selectedColumnNames, visibleState]
  );
};
