import { constSelector, useRecoilCallback, useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { selectedColumnNamesState } from '../selectedColumnNamesState';
import { SELECTED_COLUMN_NAMES_STORED_KEY } from '../../constants';

const defaultIssueQueryStateMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/query', async (importOriginal) =>
  Object.defineProperties(await importOriginal(), {
    defaultIssueQueryState: { get: defaultIssueQueryStateMock },
  })
);

beforeEach(() => {
  vi.spyOn(localStorage, 'setItem').mockImplementation(() => {});
  vi.spyOn(localStorage, 'getItem').mockReturnValue(null);
});

test('"subject"を確実に含んだ選択済みの表示項目名を配列で返す', () => {
  defaultIssueQueryStateMock.mockReturnValueOnce(constSelector({ c: ['id', 'subject', 'start_date'] }));

  const { result } = renderRecoilHook(() => ({
    names: useRecoilValue(selectedColumnNamesState),
    refresh: useRecoilCallback(({ refresh }) => {
      return () => {
        refresh(selectedColumnNamesState);
      };
    }, []),
  }));

  expect(result.current.names).toStrictEqual(['id', 'subject', 'start_date']);

  defaultIssueQueryStateMock.mockReturnValueOnce(constSelector({ c: ['id', 'start_date'] }));
  act(() => result.current.refresh());

  expect(result.current.names).toStrictEqual(['subject', 'id', 'start_date']);
});

test('set: localStorageに保存する', () => {
  const { result } = renderRecoilHook(() => ({
    set: useSetRecoilState(selectedColumnNamesState),
  }));

  expect(localStorage.setItem).not.toBeCalled();

  act(() => result.current.set(['id', 'subject']));

  expect(localStorage.setItem).toHaveBeenCalledWith(
    SELECTED_COLUMN_NAMES_STORED_KEY,
    JSON.stringify(['id', 'subject'])
  );
});
