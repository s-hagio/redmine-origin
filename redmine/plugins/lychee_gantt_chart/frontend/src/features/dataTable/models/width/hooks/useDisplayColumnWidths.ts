import { useMemo } from 'react';
import {
  ColumnName,
  ROW_NUMBER_COLUMN_NAME,
  COLLAPSIBLE_BUTTON_COLUMN_NAME,
  COLLAPSIBLE_BUTTON_COLUMN_WIDTH,
  SUBJECT_COLUMN_NAME,
  useIsDataTableVisible,
  useOptionalColumns,
} from '../../column';
import { getDefaultWidth } from '../helpers';
import { useStoredColumnWidths } from './useStoredColumnWidths';
import { useRowNumberColumnWidth } from './useRowNumberColumnWidth';

export const useDisplayColumnWidths = (): ReadonlyMap<ColumnName, number> => {
  const rowNumberColumnWidth = useRowNumberColumnWidth();
  const optionalColumns = useOptionalColumns();
  const storedColumnWidths = useStoredColumnWidths();
  const isDataTableVisible = useIsDataTableVisible();

  return useMemo(() => {
    const widths = new Map();

    widths.set(ROW_NUMBER_COLUMN_NAME, rowNumberColumnWidth);

    if (!isDataTableVisible) {
      widths.set(COLLAPSIBLE_BUTTON_COLUMN_NAME, COLLAPSIBLE_BUTTON_COLUMN_WIDTH);
      return widths;
    }

    widths.set(
      SUBJECT_COLUMN_NAME,
      storedColumnWidths[SUBJECT_COLUMN_NAME] || getDefaultWidth(SUBJECT_COLUMN_NAME)
    );

    optionalColumns.forEach(({ name }) => {
      widths.set(name, storedColumnWidths[name] || getDefaultWidth(name));
    });

    return widths;
  }, [isDataTableVisible, optionalColumns, rowNumberColumnWidth, storedColumnWidths]);
};
