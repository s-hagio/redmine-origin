import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { DEFAULT_WIDTH, STORED_WIDTHS_KEY, SUBJECT_COLUMN_DEFAULT_WIDTH } from '../../constants';
import { storedColumnWidthsState } from '../storedColumnWidthsState';

const columnWidths = {
  subject: SUBJECT_COLUMN_DEFAULT_WIDTH + 10,
  startDate: DEFAULT_WIDTH + 20,
} as const;

test('localStorageから幅データを復元する', () => {
  vi.spyOn(localStorage, 'getItem').mockReturnValue(JSON.stringify(columnWidths));

  const { result } = renderRecoilHook(() => useRecoilValue(storedColumnWidthsState));

  expect(result.current).toStrictEqual(columnWidths);
});

test('変更時にlocalStorageに幅データを保存', () => {
  vi.resetAllMocks();
  vi.spyOn(localStorage, 'setItem');

  const { result } = renderRecoilHook(() => useSetRecoilState(storedColumnWidthsState));

  expect(localStorage.setItem).not.toBeCalled();

  act(() => result.current(columnWidths));

  expect(localStorage.setItem).toBeCalledWith(STORED_WIDTHS_KEY, JSON.stringify(columnWidths));
});
