import { ColumnName, SUBJECT_COLUMN_NAME } from '../../column';
import { DEFAULT_WIDTH, SUBJECT_COLUMN_DEFAULT_WIDTH } from '../constants';

export const getDefaultWidth = (name: ColumnName): number => {
  switch (name) {
    case SUBJECT_COLUMN_NAME:
      return SUBJECT_COLUMN_DEFAULT_WIDTH;
    default:
      return DEFAULT_WIDTH;
  }
};
