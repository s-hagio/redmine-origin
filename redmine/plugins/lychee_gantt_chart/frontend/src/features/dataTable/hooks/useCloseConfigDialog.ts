import { useCloseDialog } from '@/features/dialog';
import { CONFIG_DIALOG_ID } from '../constants';

export const useCloseConfigDialog = () => useCloseDialog(CONFIG_DIALOG_ID);
