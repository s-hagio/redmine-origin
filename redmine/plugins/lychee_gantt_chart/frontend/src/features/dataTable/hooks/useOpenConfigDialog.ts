import { useOpenDialog } from '@/features/dialog';
import { CONFIG_DIALOG_ID } from '../constants';

export const useOpenConfigDialog = () => useOpenDialog(CONFIG_DIALOG_ID);
