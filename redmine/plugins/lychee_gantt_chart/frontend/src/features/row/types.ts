import {
  CoreResourceType,
  ProjectResourceIdentifier,
  VersionResourceIdentifier,
  IssueResourceIdentifier,
  GroupResourceIdentifier,
} from '@/models/coreResource';
import { ProjectID } from '@/models/project';
import { VersionID } from '@/models/version';
import { IssueID } from '@/models/issue';
import { GroupID } from '@/models/group';

export const RowResourceType = {
  ...CoreResourceType,
  Placeholder: 'placeholder',
  ParallelView: 'parallelView',
} as const;

export type RowResourceType = (typeof RowResourceType)[keyof typeof RowResourceType];

type BaseRow = {
  depth: number;
  isCollapsible: boolean;
  subtreeRootIds: RowID[];
};

export type ProjectRow = BaseRow & {
  id: ProjectResourceIdentifier;
  resourceType: 'project';
  resourceId: ProjectID;
};

export type VersionRow = BaseRow & {
  id: VersionResourceIdentifier;
  resourceType: 'version';
  resourceId: VersionID;
};

export type IssueRow = BaseRow & {
  id: IssueResourceIdentifier;
  resourceType: 'issue';
  resourceId: IssueID;
};

export type GroupRow = BaseRow & {
  id: GroupResourceIdentifier;
  resourceType: 'group';
  resourceId: GroupID;
};

export type PlaceholderRow = BaseRow & {
  id: 'placeholder';
  resourceType: 'placeholder';
  resourceId?: null;
};

export type CoreResourceRow = ProjectRow | VersionRow | IssueRow | GroupRow;

export type Row = CoreResourceRow | PlaceholderRow;
export type RowID = Row['id'];
