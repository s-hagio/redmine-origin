import { rootProjectRow } from '@/__fixtures__';
import { toIdentifiableCoreResource } from '../toIdentifiableCoreResource';

test('Rowを特定可能なリソースに変換', () => {
  expect(toIdentifiableCoreResource(rootProjectRow)).toStrictEqual({
    type: rootProjectRow.resourceType,
    id: rootProjectRow.resourceId,
  });
});
