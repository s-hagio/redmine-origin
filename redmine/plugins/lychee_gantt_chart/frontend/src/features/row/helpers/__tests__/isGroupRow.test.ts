import { mainProjectVersionRow, newStatusGroupRow, rootIssueRow, rootProjectRow } from '@/__fixtures__';
import { isGroupRow } from '../isGroupRow';

test('GroupタイプのRowならtrue', () => {
  expect(isGroupRow(newStatusGroupRow)).toBe(true);
});

test('それ以外ならfalse', () => {
  expect(isGroupRow(rootProjectRow)).toBe(false);
  expect(isGroupRow(mainProjectVersionRow)).toBe(false);
  expect(isGroupRow(rootIssueRow)).toBe(false);
});
