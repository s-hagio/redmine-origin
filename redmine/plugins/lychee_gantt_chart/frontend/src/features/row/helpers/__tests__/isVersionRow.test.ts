import { mainProjectVersionRow, newStatusGroupRow, rootIssueRow, rootProjectRow } from '@/__fixtures__';
import { isVersionRow } from '../isVersionRow';

test('GroupタイプのRowならtrue', () => {
  expect(isVersionRow(mainProjectVersionRow)).toBe(true);
});

test('それ以外ならfalse', () => {
  expect(isVersionRow(rootProjectRow)).toBe(false);
  expect(isVersionRow(newStatusGroupRow)).toBe(false);
  expect(isVersionRow(rootIssueRow)).toBe(false);
});
