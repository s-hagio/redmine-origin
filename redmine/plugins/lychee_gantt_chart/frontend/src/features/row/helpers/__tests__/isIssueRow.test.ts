import { mainProjectVersionRow, newStatusGroupRow, rootIssueRow, rootProjectRow } from '@/__fixtures__';
import { isIssueRow } from '../isIssueRow';

test('GroupタイプのRowならtrue', () => {
  expect(isIssueRow(rootIssueRow)).toBe(true);
});

test('それ以外ならfalse', () => {
  expect(isIssueRow(rootProjectRow)).toBe(false);
  expect(isIssueRow(mainProjectVersionRow)).toBe(false);
  expect(isIssueRow(newStatusGroupRow)).toBe(false);
});
