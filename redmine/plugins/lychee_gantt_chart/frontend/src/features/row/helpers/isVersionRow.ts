import { VersionRow, Row, RowResourceType } from '../types';

export const isVersionRow = (row: Row): row is VersionRow => {
  return row.resourceType === RowResourceType.Version;
};
