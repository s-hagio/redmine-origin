import { ProjectRow, Row, RowResourceType } from '../types';

export const isProjectRow = (row: Row): row is ProjectRow => {
  return row.resourceType === RowResourceType.Project;
};
