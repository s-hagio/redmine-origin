import { IdentifiableCoreResource, CoreResourceType, CoreResourceID } from '@/models/coreResource';
import { CoreResourceRow } from '../types';

export const toIdentifiableCoreResource = (row: CoreResourceRow): IdentifiableCoreResource => ({
  type: row.resourceType as CoreResourceType,
  id: row.resourceId as CoreResourceID,
});
