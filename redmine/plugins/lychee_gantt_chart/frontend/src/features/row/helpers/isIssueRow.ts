import { IssueRow, Row, RowResourceType } from '../types';

export const isIssueRow = (row: Row): row is IssueRow => {
  return row.resourceType === RowResourceType.Issue;
};
