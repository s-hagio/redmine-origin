import { mainProjectVersionRow, newStatusGroupRow, rootIssueRow, rootProjectRow } from '@/__fixtures__';
import { isProjectRow } from '../isProjectRow';

test('GroupタイプのRowならtrue', () => {
  expect(isProjectRow(rootProjectRow)).toBe(true);
});

test('それ以外ならfalse', () => {
  expect(isProjectRow(mainProjectVersionRow)).toBe(false);
  expect(isProjectRow(newStatusGroupRow)).toBe(false);
  expect(isProjectRow(rootIssueRow)).toBe(false);
});
