import { GroupRow, Row, RowResourceType } from '../types';

export const isGroupRow = (row: Row): row is GroupRow => {
  return row.resourceType === RowResourceType.Group;
};
