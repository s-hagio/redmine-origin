import { useRecoilValue } from 'recoil';
import { visibleRowsState } from '../states';

export const useVisibleRows = () => useRecoilValue(visibleRowsState);
