import { useRecoilTransaction_UNSTABLE } from 'recoil';
import { rowBoundingRectState } from '../states';
import { useRowBoundingRects } from './useRowBoundingRects';

export const useRefreshSingleRowBoundingRects = () => {
  const rowBoundingRects = useRowBoundingRects();

  return useRecoilTransaction_UNSTABLE(
    ({ set }) => {
      return () => {
        rowBoundingRects.forEach((bounds) => {
          set(rowBoundingRectState(bounds.id), bounds);
        });
      };
    },
    [rowBoundingRects]
  );
};
