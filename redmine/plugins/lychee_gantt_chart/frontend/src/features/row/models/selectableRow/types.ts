export type { RowID } from '../../types';

export type RowSelectOptions = {
  multiple?: boolean;
  range?: boolean;
};
