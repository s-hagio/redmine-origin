import { RowID } from '../../types';

export type RowBoundingRect = {
  id: RowID;
  top: number;
  height: number;
};

export type AdditionalRowHeightAdderName = string;
