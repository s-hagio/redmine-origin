import { DefaultValue, selectorFamily } from 'recoil';
import { RowOptions } from '../types';
import { overwrittenRowOptionsMapState } from './overwrittenRowOptionsMapState';

export type OverwriterID = string;

export const overwriteRowOptionsState = selectorFamily<Partial<RowOptions> | null, OverwriterID>({
  key: 'row/overwriteRowOptions',
  get: (id) => {
    return ({ get }) => {
      const map = get(overwrittenRowOptionsMapState);

      return map.get(id) ?? null;
    };
  },
  set: (id) => {
    return ({ set }, newValue) => {
      if (newValue instanceof DefaultValue) {
        return;
      }

      set(overwrittenRowOptionsMapState, (map) => {
        const newMap = new Map(map);

        if (newValue == null) {
          newMap.delete(id);
        } else {
          newMap.set(id, newValue);
        }

        return newMap;
      });
    };
  },
});
