import { atom } from 'recoil';
import { IssueRow } from '../../../types';

type IssueRowID = IssueRow['id'];

export const visibleIssueTreeRootRowIdsState = atom<IssueRowID[]>({
  key: 'row/visibleIssueTreeRootRowIds',
  default: [],
});
