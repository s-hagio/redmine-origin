import { useRecoilValue } from 'recoil';
import { RowID } from '../../../types';
import { isClosedResourceRowIdState } from '../states';

export const useIsClosedResourceRowId = (rowId: RowID) => {
  return useRecoilValue(isClosedResourceRowIdState(rowId));
};
