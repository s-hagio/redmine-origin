import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { childIssueRow, parentIssueRow, rootIssueRow } from '@/__fixtures__';
import { RowBoundingRect } from '../../types';
import { rowBoundingRectsState } from '../../states';
import { findRowBoundingRectState } from '../findRowBoundingRectState';

const rowBoundingRects: RowBoundingRect[] = [
  { id: rootIssueRow.id, top: 0, height: 100 },
  { id: parentIssueRow.id, top: 100, height: 100 },
  { id: childIssueRow.id, top: 200, height: 100 },
];

test('Y軸のオフセット値から行の境界矩形を取得', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(findRowBoundingRectState), {
    initializeState: ({ set }) => {
      set(rowBoundingRectsState, rowBoundingRects);
    },
  });

  expect(result.current(0)).toBe(rowBoundingRects[0]);
  expect(result.current(100)).toBe(rowBoundingRects[0]);
  expect(result.current(101)).toBe(rowBoundingRects[1]);
  expect(result.current(201)).toBe(rowBoundingRects[2]);
  expect(result.current(301)).toBe(null);
});
