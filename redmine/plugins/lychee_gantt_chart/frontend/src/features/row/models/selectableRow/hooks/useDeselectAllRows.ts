import { useRecoilCallback } from 'recoil';
import { skipDeselectAllRowsState } from '../states';
import { useSetSelectedRowIds } from './useSetSelectedRowIds';

export const useDeselectAllRows = () => {
  const setSelectedRowIds = useSetSelectedRowIds();

  return useRecoilCallback(
    ({ set, snapshot }) => {
      return () => {
        if (snapshot.getLoadable(skipDeselectAllRowsState).getValue()) {
          set(skipDeselectAllRowsState, false);
          return;
        }

        setSelectedRowIds([]);
      };
    },
    [setSelectedRowIds]
  );
};
