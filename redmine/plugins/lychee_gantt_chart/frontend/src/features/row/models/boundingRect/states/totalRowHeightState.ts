import { selector } from 'recoil';
import { MIN_TOTAL_HEIGHT } from '../constants';
import { rowBoundingRectsState } from './rowBoundingRectsState';

export const totalRowHeightState = selector<number>({
  key: 'row/totalRowHeight',
  get: ({ get }) => {
    const rowBoundingRects = get(rowBoundingRectsState);
    const lastRow = rowBoundingRects[rowBoundingRects.length - 1];

    const height = lastRow ? lastRow.top + lastRow.height : 0;

    return Math.max(height, MIN_TOTAL_HEIGHT);
  },
});
