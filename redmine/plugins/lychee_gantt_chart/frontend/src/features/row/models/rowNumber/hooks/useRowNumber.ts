import { useRecoilValue } from 'recoil';
import { RowID } from '../../../types';
import { rowNumberState } from '../states';

export const useRowNumber = (rowId: RowID) => useRecoilValue(rowNumberState(rowId));
