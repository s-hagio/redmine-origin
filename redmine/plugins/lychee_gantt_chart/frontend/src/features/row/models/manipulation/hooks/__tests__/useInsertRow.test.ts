import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { mainProjectRow, parentIssueRow, rootIssueRow, versionIssueRow } from '@/__fixtures__';
import { rowsState } from '../../../row';
import { useInsertRow } from '../useInsertRow';

const renderHookForTesting = () => {
  return renderRecoilHook(
    () => ({
      insertRow: useInsertRow(),
      rows: useRecoilValue(rowsState),
    }),
    {
      initializeState: ({ set }) => {
        set(rowsState, [mainProjectRow, rootIssueRow, parentIssueRow]);
      },
    }
  );
};

test('indexを指定した場合はそのindexに指定Rowを挿入する', () => {
  const { result } = renderHookForTesting();

  act(() => result.current.insertRow(versionIssueRow, { index: 1 }));

  expect(result.current.rows).toEqual([mainProjectRow, versionIssueRow, rootIssueRow, parentIssueRow]);
});

test('appendを指定した場合は末尾に指定Rowを挿入する', () => {
  const { result } = renderHookForTesting();

  act(() => result.current.insertRow(versionIssueRow, { append: true }));

  expect(result.current.rows).toEqual([mainProjectRow, rootIssueRow, parentIssueRow, versionIssueRow]);
});

test('beforeを指定した場合は指定Rowの前に指定Rowを挿入する', () => {
  const { result } = renderHookForTesting();

  act(() => result.current.insertRow(versionIssueRow, { before: rootIssueRow }));

  expect(result.current.rows).toEqual([mainProjectRow, versionIssueRow, rootIssueRow, parentIssueRow]);
});

test('afterを指定した場合は指定Rowの後に指定Rowを挿入する', () => {
  const { result } = renderHookForTesting();

  act(() => result.current.insertRow(versionIssueRow, { after: rootIssueRow }));

  expect(result.current.rows).toEqual([mainProjectRow, rootIssueRow, versionIssueRow, parentIssueRow]);
});
