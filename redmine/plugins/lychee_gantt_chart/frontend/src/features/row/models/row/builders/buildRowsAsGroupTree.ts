import { indexBy } from '@/utils/array';
import { getGroupResourceIdentifier } from '@/models/coreResource';
import { BuildRowsAsGroupTree, Row, RowTreeAttributes } from './types';
import { buildIssueTree, GroupNode, IssueNode } from './tree';
import { buildGroupNodeSorter, buildIssueNodeSorter } from './sorter';
import { buildChildRowTreeAttributes, buildGroupRow, buildIssueRow } from './buildTypedRow';

export const buildRowsAsGroupTree: BuildRowsAsGroupTree = (groups, issues, sorterFor, options) => {
  const rows: Row[] = [];

  const sortGroupNodes = buildGroupNodeSorter(sorterFor);
  const sortIssueNodes = buildIssueNodeSorter(sorterFor);

  const issuesById = indexBy(issues, 'id');

  const addIssueRow = (issueNode: IssueNode, treeAttributes: RowTreeAttributes) => {
    const row = buildIssueRow(issueNode, treeAttributes);
    const treeAttrs = buildChildRowTreeAttributes(row);

    rows.push(row);

    sortIssueNodes(issueNode, issueNode.childNodes).forEach((node) => addIssueRow(node, treeAttrs));
  };

  sortGroupNodes(
    groups.map<GroupNode>((group) => ({
      ...group,
      identifier: getGroupResourceIdentifier(group.id),
    }))
  ).forEach((groupNode) => {
    const groupRow = buildGroupRow(groupNode);
    rows.push(groupRow);

    const treeAttrs = {
      depth: groupRow.depth + 1,
      subtreeRootIds: [groupRow.id],
    };

    const issueNodes = buildIssueTree(
      groupNode.issueIds.map((id) => issuesById[id]),
      options
    );

    sortIssueNodes(groupNode, issueNodes).forEach((node) => addIssueRow(node, treeAttrs));
  });

  return rows;
};
