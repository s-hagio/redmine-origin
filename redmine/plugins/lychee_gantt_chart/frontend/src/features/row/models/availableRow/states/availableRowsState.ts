import { selector } from 'recoil';
import { Row, RowID } from '../../../types';
import { collapsedRowIdsState } from '../../collapsibleRow';
import { rowsState } from '../../row';

export const availableRowsState = selector<Row[]>({
  key: 'availableRows',
  get: ({ get }) => {
    const rows = get(rowsState);
    const collapsedRowIds = get(collapsedRowIdsState);

    const collapsibleRowIds = new Set<RowID>(rows.filter((row) => row.isCollapsible).map((row) => row.id));

    return rows.filter(
      (row) => !row.subtreeRootIds.some((id) => collapsibleRowIds.has(id) && collapsedRowIds.has(id))
    );
  },
});
