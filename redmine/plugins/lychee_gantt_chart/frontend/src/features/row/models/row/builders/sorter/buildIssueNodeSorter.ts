import { CoreResourceType } from '@/models/coreResource';
import { SorterFor } from '@/features/sort';
import { SortIssueNodes } from './types';

export const buildIssueNodeSorter = (sorterFor: SorterFor): SortIssueNodes => {
  const sort = sorterFor(CoreResourceType.Issue);

  return (subtreeRootNode, issueNodes) => {
    return sort(issueNodes, { subtreeRootId: subtreeRootNode.identifier });
  };
};
