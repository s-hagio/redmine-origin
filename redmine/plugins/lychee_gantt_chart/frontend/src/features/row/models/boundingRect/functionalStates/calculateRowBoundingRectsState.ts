import { selector } from 'recoil';
import { Row, RowID } from '../../../types';
import { RowBoundingRect } from '../types';
import { rowHeightState } from '../states';

type HeightResolver = (id: RowID) => number;

type CalculateRowBoundingRects = (sortedRows: Row[], heightResolver?: HeightResolver) => RowBoundingRect[];

export const calculateRowBoundingRectsState = selector<CalculateRowBoundingRects>({
  key: 'row/calculateRowBoundingRects',
  get: ({ getCallback }) => {
    return getCallback(({ snapshot }) => {
      return (sortedRows, heightResolver = (id) => snapshot.getLoadable(rowHeightState(id)).getValue()) => {
        return sortedRows.reduce<RowBoundingRect[]>((list, row) => {
          const height = heightResolver(row.id);

          const prevBounds = list.at(-1) ?? { top: 0, height: 0 };
          const bounds = {
            id: row.id,
            top: prevBounds.top + prevBounds.height,
            height,
          };

          list.push(bounds);

          return list;
        }, []);
      };
    });
  },
});
