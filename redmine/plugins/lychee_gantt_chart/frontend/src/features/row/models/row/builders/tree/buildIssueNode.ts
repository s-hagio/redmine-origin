import { Issue } from '@/models/issue';
import { getIssueResourceIdentifier } from '@/models/coreResource';
import { IssueNode } from './types';

export const buildIssueNode = (issue: Issue): IssueNode => ({
  ...issue,
  identifier: getIssueResourceIdentifier(issue.id),
  childNodes: [],
});
