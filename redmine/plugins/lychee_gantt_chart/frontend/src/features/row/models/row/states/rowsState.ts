import { atom } from 'recoil';
import { Row } from '../../../types';

export const rowsState = atom<Row[]>({
  key: 'row/rows',
  default: [],
});
