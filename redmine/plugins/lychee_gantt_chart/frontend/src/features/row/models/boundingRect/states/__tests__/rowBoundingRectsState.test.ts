import { constSelector, useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { rootIssueRow, parentIssueRow, childIssueRow } from '@/__fixtures__';
import { rowBoundingRectsState } from '../rowBoundingRectsState';
import { rowsState } from '../../../row';

const rowHeightStateMock = vi.hoisted(() => vi.fn());

vi.mock('../rowHeightState', () => ({
  rowHeightState: rowHeightStateMock,
}));

const heightMap = new Map([
  [rootIssueRow.id, 10],
  [parentIssueRow.id, 100],
  [childIssueRow.id, 200],
]);

beforeEach(() => {
  rowHeightStateMock.mockImplementation((id) => constSelector(heightMap.get(id)));
});

test('有効行の高さと位置を計算', () => {
  const { result } = renderRecoilHook(() => ({
    rects: useRecoilValue(rowBoundingRectsState),
    setRows: useSetRecoilState(rowsState),
  }));

  expect(result.current.rects).toStrictEqual([]);

  act(() => {
    result.current.setRows([rootIssueRow, parentIssueRow, childIssueRow]);
  });

  expect(result.current.rects).toStrictEqual([
    { id: rootIssueRow.id, top: 0, height: 10 },
    { id: parentIssueRow.id, top: 10, height: 100 },
    { id: childIssueRow.id, top: 110, height: 200 },
  ]);
});
