import { atomFamily } from 'recoil';
import { RowID } from '../../../types';
import { RowBoundingRect } from '../types';

export const rowBoundingRectState = atomFamily<RowBoundingRect | null, RowID>({
  key: 'row/rowBoundingRect',
  default: null,
});
