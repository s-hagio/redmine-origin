import { CoreResourceType } from '@/models/coreResource';
import { SorterFor } from '@/features/sort';
import { SortProjectNodes } from './types';

export const buildProjectNodeSorter = (sorterFor: SorterFor): SortProjectNodes => {
  const sort = sorterFor(CoreResourceType.Project);

  return (subtreeRootNode, projectNodes) => {
    return sort(projectNodes, { subtreeRootId: subtreeRootNode.identifier });
  };
};
