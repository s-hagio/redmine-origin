import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { childIssueRow, parentIssueRow, rootIssueRow } from '@/__fixtures__';
import { rowHeightState } from '../rowHeightState';
import { DEFAULT_HEIGHT } from '../../constants';
import { additionalRowHeightState } from '../additionalRowHeightState';

test('additionalRowHeightsStateを合成した行の高さを返す', () => {
  const { result } = renderRecoilHook(
    () => ({
      rootIssueRowHeight: useRecoilValue(rowHeightState(rootIssueRow.id)),
      parentIssueRowHeight: useRecoilValue(rowHeightState(parentIssueRow.id)),
      childIssueRowHeight: useRecoilValue(rowHeightState(childIssueRow.id)),
    }),
    {
      initializeState: ({ set }) => {
        set(additionalRowHeightState({ id: rootIssueRow.id, adderName: 'a' }), 10);
        set(additionalRowHeightState({ id: rootIssueRow.id, adderName: 'b' }), 11);
        set(additionalRowHeightState({ id: parentIssueRow.id, adderName: 'b' }), 111);
      },
    }
  );

  expect(result.current.rootIssueRowHeight).toBe(DEFAULT_HEIGHT + 10 + 11);
  expect(result.current.parentIssueRowHeight).toBe(DEFAULT_HEIGHT + 111);
  expect(result.current.childIssueRowHeight).toBe(DEFAULT_HEIGHT);
});
