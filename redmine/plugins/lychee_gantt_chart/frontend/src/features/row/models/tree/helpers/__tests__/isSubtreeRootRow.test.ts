import {
  grandchildIssueRow,
  mainProjectVersionRow,
  newStatusGroupRow,
  parentIssueRow,
  rootProjectRow,
} from '@/__fixtures__';
import { isSubtreeRootRow } from '../isSubtreeRootRow';

test('プロジェクトはtrue', () => {
  expect(isSubtreeRootRow(rootProjectRow)).toBe(true);
});

test('バージョンはtrue', () => {
  expect(isSubtreeRootRow(mainProjectVersionRow)).toBe(true);
});

test('折りたたみ可能なRowはtrue', () => {
  expect(isSubtreeRootRow(parentIssueRow)).toBe(true);
  expect(isSubtreeRootRow(newStatusGroupRow)).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isSubtreeRootRow(grandchildIssueRow)).toBe(false);
});
