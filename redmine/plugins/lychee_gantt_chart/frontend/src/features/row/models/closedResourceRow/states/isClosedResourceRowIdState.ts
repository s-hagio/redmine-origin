import { selectorFamily } from 'recoil';
import {
  getResourceIdFromIdentifier,
  isIssueResourceIdentifier,
  isVersionResourceIdentifier,
} from '@/models/coreResource';
import { versionState } from '@/models/version';
import { isClosedIssue, issueState } from '@/models/issue';
import { RowID } from '../../../types';

export const isClosedResourceRowIdState = selectorFamily<boolean, RowID>({
  key: 'row/isClosedResourceRowId',
  get: (id) => {
    return ({ get }) => {
      if (isVersionResourceIdentifier(id)) {
        return !!get(versionState(getResourceIdFromIdentifier(id)))?.isClosed;
      } else if (isIssueResourceIdentifier(id)) {
        return isClosedIssue(get(issueState(getResourceIdFromIdentifier(id))));
      }

      return false;
    };
  },
});
