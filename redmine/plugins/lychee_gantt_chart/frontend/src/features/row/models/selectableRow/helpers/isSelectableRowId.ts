import { isIssueResourceIdentifier } from '@/models/coreResource';
import { RowID } from '../types';

export const isSelectableRowId = (id: RowID): boolean => {
  return isIssueResourceIdentifier(id);
};
