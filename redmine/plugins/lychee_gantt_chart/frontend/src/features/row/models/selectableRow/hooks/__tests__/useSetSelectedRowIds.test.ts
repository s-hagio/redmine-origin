import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { expect } from 'vitest';
import { renderRecoilHook } from '@/test-utils';
import { childIssueRow, parentIssueRow, rootIssueRow } from '@/__fixtures__';
import { selectedRowIdsState } from '../../states';
import { useSetSelectedRowIds } from '../useSetSelectedRowIds';
import { rowStatusState } from '../../../rowStatus';

test('指定の行ID配列をselectedRowIdsにセットする', () => {
  const { result } = renderRecoilHook(() => ({
    setSelectedRowIds: useSetSelectedRowIds(),
    selectedRowIds: useRecoilValue(selectedRowIdsState),
  }));

  act(() => {
    result.current.setSelectedRowIds([rootIssueRow.id, parentIssueRow.id]);
  });

  expect(result.current.selectedRowIds).toEqual([rootIssueRow.id, parentIssueRow.id]);

  act(() => {
    result.current.setSelectedRowIds((current) => [...current, childIssueRow.id]);
  });

  expect(result.current.selectedRowIds).toEqual([rootIssueRow.id, parentIssueRow.id, childIssueRow.id]);
});

test('選択された行のisSelectedをtrueに、選択が外れた行にisSelectedをfalseにする', () => {
  const { result } = renderRecoilHook(() => ({
    setSelectedRowIds: useSetSelectedRowIds(),
    rootIssueRowStatus: useRecoilValue(rowStatusState(rootIssueRow.id)),
    parentIssueRowStatus: useRecoilValue(rowStatusState(parentIssueRow.id)),
    childIssueRowStatus: useRecoilValue(rowStatusState(childIssueRow.id)),
  }));

  act(() => {
    result.current.setSelectedRowIds([rootIssueRow.id, parentIssueRow.id]);
  });

  expect(result.current.rootIssueRowStatus.isSelected).toBe(true);
  expect(result.current.parentIssueRowStatus.isSelected).toBe(true);
  expect(result.current.childIssueRowStatus.isSelected).toBe(false);

  act(() => {
    result.current.setSelectedRowIds([parentIssueRow.id, childIssueRow.id]);
  });

  expect(result.current.rootIssueRowStatus.isSelected).toBe(false);
  expect(result.current.parentIssueRowStatus.isSelected).toBe(true);
  expect(result.current.childIssueRowStatus.isSelected).toBe(true);

  act(() => {
    result.current.setSelectedRowIds([]);
  });

  expect(result.current.rootIssueRowStatus.isSelected).toBe(false);
  expect(result.current.parentIssueRowStatus.isSelected).toBe(false);
  expect(result.current.childIssueRowStatus.isSelected).toBe(false);
});
