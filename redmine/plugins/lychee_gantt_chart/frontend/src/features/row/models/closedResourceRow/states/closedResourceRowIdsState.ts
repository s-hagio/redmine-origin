import { selector } from 'recoil';
import { RowID } from '../../../types';
import { visibleRowsState } from '../../visibleRow';
import { isClosedResourceRowIdState } from './isClosedResourceRowIdState';

export const closedResourceRowIdsState = selector<ReadonlySet<string>>({
  key: 'row/closedResourceRowIds',
  get: ({ get }) => {
    const visibleRows = get(visibleRowsState);

    return new Set(
      visibleRows.reduce<RowID[]>((memo, row) => {
        const isClosed = get(isClosedResourceRowIdState(row.id));

        if (isClosed) {
          memo.push(row.id);
        }

        return memo;
      }, [])
    );
  },
});
