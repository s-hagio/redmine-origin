import { useRecoilValue } from 'recoil';
import { rowSelectOptionsState } from '../states';

export const useRowSelectOptions = () => useRecoilValue(rowSelectOptionsState);
