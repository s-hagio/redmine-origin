import { CoreResourceCollection } from '@/models/coreResource';
import { ProjectID } from '@/models/project';
import { GroupBy } from '@/models/query';
import { Group } from '@/models/group';
import { Issue } from '@/models/issue';
import { SorterFor } from '@/features/sort';
import { Row, RowResourceType } from '../../../types';
import { RowOptions } from '../types';

export type { Row };
export { RowResourceType };

export type RowBuildOptions = RowOptions & {
  mainProjectId: ProjectID | null;
  groupBy: GroupBy | null;
  rootDepth: number;
};

export type BuildRowsAsProjectTree = (
  collection: CoreResourceCollection,
  sorterFor: SorterFor,
  options: RowBuildOptions
) => Row[];

export type BuildRowsAsGroupTree = (
  groups: Group[],
  issues: Issue[],
  sorterFor: SorterFor,
  options: RowBuildOptions
) => Row[];

export type RowTreeAttributes = Pick<Row, 'depth' | 'subtreeRootIds'>;
