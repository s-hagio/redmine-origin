import { getIssueResourceIdentifier } from '@/models/coreResource';
import {
  childIssue,
  grandchildIssue,
  grandchildIssue2,
  grandchildProjectIssue,
  otherTreeIssue,
  parentIssue,
  versionIssue,
} from '@/__fixtures__';
import { IssueNode } from '../../types';

export const parentIssueNode: Readonly<IssueNode> = {
  ...parentIssue,
  identifier: getIssueResourceIdentifier(parentIssue.id),
  childNodes: [],
};

export const childIssueNode: Readonly<IssueNode> = {
  ...childIssue,
  identifier: getIssueResourceIdentifier(childIssue.id),
  childNodes: [],
};

export const versionIssueNode: Readonly<IssueNode> = {
  ...versionIssue,
  identifier: getIssueResourceIdentifier(versionIssue.id),
  childNodes: [],
};

export const grandchildIssueNode: Readonly<IssueNode> = {
  ...grandchildIssue,
  identifier: getIssueResourceIdentifier(grandchildIssue.id),
  childNodes: [],
};

export const grandchildIssue2Node: Readonly<IssueNode> = {
  ...grandchildIssue2,
  identifier: getIssueResourceIdentifier(grandchildIssue2.id),
  childNodes: [],
};

export const otherTreeIssueNode: Readonly<IssueNode> = {
  ...otherTreeIssue,
  identifier: getIssueResourceIdentifier(otherTreeIssue.id),
  childNodes: [],
};

export const grandchildProjectIssueNode: Readonly<IssueNode> = {
  ...grandchildProjectIssue,
  identifier: getIssueResourceIdentifier(grandchildProjectIssue.id),
  childNodes: [],
};
