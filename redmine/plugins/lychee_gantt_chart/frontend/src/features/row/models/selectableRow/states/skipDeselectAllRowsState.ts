import { atom } from 'recoil';

export const skipDeselectAllRowsState = atom<boolean>({
  key: 'row/skipUnselectAllRows',
  default: false,
});
