import { constSelector, useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { childIssueRow, rootIssueRow, rootProjectRow } from '@/__fixtures__';
import { maxRowNumberState } from '../maxRowNumberState';

const rowsStateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../row', async (importOriginal) =>
  Object.defineProperties(await importOriginal<object>(), {
    rowsState: { get: rowsStateMock },
  })
);

const availableRowsStateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../availableRow', async (importOriginal) =>
  Object.defineProperties(await importOriginal<object>(), {
    availableRowsState: { get: availableRowsStateMock },
  })
);

beforeEach(() => {
  rowsStateMock.mockReturnValue(constSelector([rootProjectRow, rootIssueRow, childIssueRow]));
  availableRowsStateMock.mockReturnValue(constSelector([rootProjectRow, rootIssueRow]));
});

test('最大行番号を返す', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(maxRowNumberState));

  expect(result.current).toBe(3);
});
