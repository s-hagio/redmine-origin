import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import {
  mainProjectRow,
  childProject1Row,
  grandchildProjectRow,
  childProject2Row,
  otherTreeProjectRow,
} from '@/__fixtures__';
import { rowsState } from '../../../row';
import { collapsedRowIdsState } from '../../../collapsibleRow';
import { availableRowsState } from '../availableRowsState';

test('rowsStateから折りたたまれている行を除外', () => {
  const rows = [
    mainProjectRow,
    { ...childProject1Row, subtreeRootIds: [mainProjectRow.id] },
    { ...grandchildProjectRow, subtreeRootIds: [mainProjectRow.id, childProject1Row.id] },
    { ...childProject2Row, subtreeRootIds: [mainProjectRow.id] },
    otherTreeProjectRow,
  ];

  const { result } = renderRecoilHook(
    () => ({
      availableRows: useRecoilValue(availableRowsState),
      setCollapsedRowIds: useSetRecoilState(collapsedRowIdsState),
    }),
    {
      initializeState: ({ set }) => {
        set(rowsState, rows);
      },
    }
  );

  expect(result.current.availableRows).toStrictEqual(rows);

  act(() => {
    result.current.setCollapsedRowIds(new Set([mainProjectRow.id]));
  });

  expect(result.current.availableRows).toStrictEqual([mainProjectRow, otherTreeProjectRow]);
});
