import { useRecoilValue, useRecoilCallback } from 'recoil';
import { RowID } from '../../../types';
import { collapsedRowIdsState } from '../states';
import { isRowCollapsedState } from '../states/isRowCollapsedState';

type ReturnObject = {
  isCollapsed: boolean;
  toggle: () => void;
};

export const useRowCollapseToggle = (id: RowID): ReturnObject => {
  const isCollapsed = useRecoilValue(isRowCollapsedState(id));

  const toggle = useRecoilCallback(
    ({ set }) => {
      return () => {
        set(isRowCollapsedState(id), (isCollapsed) => !isCollapsed);

        set(collapsedRowIdsState, (current) => {
          const newRowIds = new Set(current);

          if (newRowIds.has(id)) {
            newRowIds.delete(id);
          } else {
            newRowIds.add(id);
          }

          return newRowIds;
        });
      };
    },
    [id]
  );

  return { isCollapsed, toggle };
};
