import { groupBy, sortBy } from '@/utils/array';
import { isDescendantOf } from '@/lib/nestedSet';
import { Issue } from '@/models/issue';
import { RowBuildOptions } from '../types';
import { IssueNode } from './types';
import { buildIssueNode } from './buildIssueNode';

type Options = Pick<RowBuildOptions, 'keepIssueTreeStructure'>;

export const buildIssueTree = (issues: Issue[] | undefined, options: Options): IssueNode[] => {
  if (!issues?.length) {
    return [];
  }

  if (!options.keepIssueTreeStructure) {
    return issues.map(buildIssueNode);
  }

  const rootNodes: IssueNode[] = [];
  const ancestorNodes: IssueNode[] = [];
  const issueListsByRootId = groupBy(issues, 'rootId');

  issues.forEach(({ rootId }) => {
    if (!issueListsByRootId[rootId]) {
      return;
    }

    const issuesInTree = issueListsByRootId[rootId];
    delete issueListsByRootId[rootId];

    sortBy(issuesInTree, 'lft').forEach((issue) => {
      const issueNode = buildIssueNode(issue);

      while (ancestorNodes.length && !isDescendantOf(issueNode, ancestorNodes[ancestorNodes.length - 1])) {
        ancestorNodes.pop();
      }

      const closestAncestorNode = ancestorNodes[ancestorNodes.length - 1];

      if (closestAncestorNode) {
        closestAncestorNode.childNodes.push(issueNode);
      } else {
        rootNodes.push(issueNode);
      }

      ancestorNodes.push(issueNode);
    });
  });

  return rootNodes;
};
