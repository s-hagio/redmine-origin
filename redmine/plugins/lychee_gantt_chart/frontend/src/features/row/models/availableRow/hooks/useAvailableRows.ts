import { useRecoilValue } from 'recoil';
import { availableRowsState } from '../states';

export const useAvailableRows = () => useRecoilValue(availableRowsState);
