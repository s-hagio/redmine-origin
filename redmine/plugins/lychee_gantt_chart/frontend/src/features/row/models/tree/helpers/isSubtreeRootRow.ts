import { Row, RowResourceType } from '../../../types';

const issueContainerResourceRowTypes: Set<string> = new Set([
  RowResourceType.Project,
  RowResourceType.Version,
]);

export const isSubtreeRootRow = (row: Row) => {
  return issueContainerResourceRowTypes.has(row.resourceType) || row.isCollapsible;
};
