import { selector } from 'recoil';
import { Row } from '../../../types';
import { availableRowsState } from '../../availableRow';
import { visibleRowRangeState } from './visibleRowRangeState';

export const visibleRowsState = selector<Row[]>({
  key: 'row/visibleRows',
  get: ({ get }) => {
    const rows = get(availableRowsState);
    const { start, end } = get(visibleRowRangeState);

    return rows.slice(start, end + 1);
  },
});
