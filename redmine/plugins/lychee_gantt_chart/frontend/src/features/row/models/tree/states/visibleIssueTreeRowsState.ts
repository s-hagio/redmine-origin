import { atomFamily } from 'recoil';
import { IssueRow } from '../../../types';

export const visibleIssueTreeRowsState = atomFamily<IssueRow[], IssueRow['id']>({
  key: 'row/visibleIssueTreeRows',
  default: [],
});
