import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import {
  getIssueResourceIdentifier,
  getProjectResourceIdentifier,
  getVersionResourceIdentifier,
} from '@/models/coreResource';
import { versionState } from '@/models/version';
import { issueEntityState } from '@/models/issue';
import { mainProject, rootIssue, versionSharedByNone } from '@/__fixtures__';
import { isClosedResourceRowIdState } from '../isClosedResourceRowIdState';

describe('Project', () => {
  const project = mainProject;
  const identifier = getProjectResourceIdentifier(project.id);

  test('常にfalse', () => {
    const { result } = renderRecoilHook(() => useRecoilValue(isClosedResourceRowIdState(identifier)));

    expect(result.current).toBe(false);
  });
});

describe('Version', () => {
  const version = versionSharedByNone;
  const identifier = getVersionResourceIdentifier(mainProject.id, version.id);

  test.each([true, false])('isClosedが%sなら%s', (isClosed) => {
    const { result } = renderRecoilHook(() => useRecoilValue(isClosedResourceRowIdState(identifier)), {
      initializeState: ({ set }) => {
        set(versionState(version.id), { ...version, isClosed });
      },
    });

    expect(result.current).toBe(isClosed);
  });
});

describe('Issue', () => {
  const issue = rootIssue;
  const identifier = getIssueResourceIdentifier(issue.id);

  test.each([true, false])('isClosedが%sなら%s', (isClosed) => {
    const { result } = renderRecoilHook(() => useRecoilValue(isClosedResourceRowIdState(identifier)), {
      initializeState: ({ set }) => {
        set(issueEntityState(issue.id), { ...issue, isClosed });
      },
    });

    expect(result.current).toBe(isClosed);
  });

  test('isClosedがfalseでも進捗率が100%ならtrue', () => {
    const { result } = renderRecoilHook(
      () => ({
        isClosed: useRecoilValue(isClosedResourceRowIdState(identifier)),
        setIssue: useSetRecoilState(issueEntityState(issue.id)),
      }),
      {
        initializeState: ({ set }) => {
          set(issueEntityState(issue.id), { ...issue, isClosed: false, doneRatio: 99 });
        },
      }
    );

    expect(result.current.isClosed).toBe(false);
    act(() => result.current.setIssue({ ...issue, doneRatio: 100 }));
    expect(result.current.isClosed).toBe(true);
  });
});
