import { useRecoilTransaction_UNSTABLE } from 'recoil';
import { rowStatusState } from '../../rowStatus';
import { selectedRowIdsState } from '../states';
import { RowID } from '../types';
import { isSelectableRowId } from '../helpers';

type Updater = (selectedRowIds: RowID[]) => RowID[];
type SetSelectedRowIds = (valueOrUpdater: RowID[] | Updater) => void;

export const useSetSelectedRowIds = (): SetSelectedRowIds => {
  return useRecoilTransaction_UNSTABLE(({ get, set }) => {
    return (valueOrUpdater) => {
      const currentSelectedRowIds = get(selectedRowIdsState);
      const newSelectedRowIds = (
        typeof valueOrUpdater === 'function' ? valueOrUpdater(currentSelectedRowIds) : valueOrUpdater
      ).filter(isSelectableRowId);

      set(selectedRowIdsState, newSelectedRowIds);

      new Set([...currentSelectedRowIds, ...newSelectedRowIds]).forEach((id) => {
        set(rowStatusState(id), (current) => ({
          ...current,
          isSelected: newSelectedRowIds.includes(id),
        }));
      });
    };
  }, []);
};
