import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import {
  mainProjectRow,
  parentIssueRow,
  parentProjectRow,
  rootIssueRow,
  rootProjectRow,
} from '@/__fixtures__';
import { visibleRowRangeState, visibleRowsState } from '../../states';
import { rowsState } from '../../../row';

test('visibleRowRange内の行を返す', () => {
  const { result } = renderRecoilHook(
    () => ({
      visibleRows: useRecoilValue(visibleRowsState),
      setRange: useSetRecoilState(visibleRowRangeState),
    }),
    {
      initializeState: ({ set }) => {
        set(rowsState, [rootProjectRow, parentProjectRow, mainProjectRow, rootIssueRow, parentIssueRow]);
        set(visibleRowRangeState, { start: 1, end: 3 });
      },
    }
  );

  expect(result.current.visibleRows).toStrictEqual([parentProjectRow, mainProjectRow, rootIssueRow]);

  act(() => {
    result.current.setRange({ start: 0, end: 4 });
  });

  expect(result.current.visibleRows).toStrictEqual([
    rootProjectRow,
    parentProjectRow,
    mainProjectRow,
    rootIssueRow,
    parentIssueRow,
  ]);
});
