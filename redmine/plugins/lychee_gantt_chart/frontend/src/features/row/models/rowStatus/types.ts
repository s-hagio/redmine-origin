export type RowStatus = {
  isHighlight: boolean;
  isAdditionEffectNeeded: boolean;
  isSelected: boolean;
};
