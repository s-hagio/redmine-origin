import { atom } from 'recoil';
import { RowOptions } from '../types';

export const rowOptionsState = atom<RowOptions>({
  key: 'row/rowOptions',
  default: {
    keepIssueTreeStructure: true,
    includeMainAndAncestorProjects: true,
    includeEmptyProjects: false,
    includeEmptyVersions: false,
    parallelView: false,
    skipRefresh: false,
  },
});
