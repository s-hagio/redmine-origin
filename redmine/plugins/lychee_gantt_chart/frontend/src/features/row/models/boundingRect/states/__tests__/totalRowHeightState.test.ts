import { constSelector, useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { rootIssueRow, parentIssueRow, childIssueRow } from '@/__fixtures__';
import { rowsState } from '../../../row';
import { MIN_TOTAL_HEIGHT } from '../../constants';
import { totalRowHeightState } from '../totalRowHeightState';

const rowHeightStateMock = vi.hoisted(() => vi.fn());

vi.mock('../rowHeightState', () => ({
  rowHeightState: rowHeightStateMock,
}));

const renderTotalRowHeightHook = () => {
  return renderRecoilHook(() => ({
    totalHeight: useRecoilValue(totalRowHeightState),
    setRows: useSetRecoilState(rowsState),
  }));
};

test('利用可能な行のすべての高さを合算', () => {
  rowHeightStateMock
    .mockReturnValueOnce(constSelector(10))
    .mockReturnValueOnce(constSelector(100))
    .mockReturnValueOnce(constSelector(200));

  const { result } = renderTotalRowHeightHook();

  expect(result.current.totalHeight).toBe(MIN_TOTAL_HEIGHT);

  act(() => {
    result.current.setRows([rootIssueRow, parentIssueRow, childIssueRow]);
  });

  expect(result.current.totalHeight).toBe(310);
});

test('合計が規定最小値より小さい場合は規定最小値にする', () => {
  rowHeightStateMock.mockReturnValueOnce(constSelector(10));

  const { result } = renderTotalRowHeightHook();

  act(() => {
    result.current.setRows([rootIssueRow]);
  });

  expect(result.current.totalHeight).toBe(MIN_TOTAL_HEIGHT);
});
