import { DefaultValue, selectorFamily } from 'recoil';
import { RowID } from '../../../types';
import { AdditionalRowHeightAdderName } from '../types';
import { additionalRowHeightMapObjectState } from './additionalRowHeightMapObjectState';

export type AdditionalRowHeightParam = {
  id: RowID;
  adderName: AdditionalRowHeightAdderName;
};

export const additionalRowHeightState = selectorFamily<number, AdditionalRowHeightParam>({
  key: 'row/additionalRowHeight',
  get: ({ id, adderName }) => {
    return ({ get }) => {
      const map = get(additionalRowHeightMapObjectState(id));

      return map[adderName] ?? 0;
    };
  },
  set: ({ id, adderName }) => {
    return ({ set }, additionalHeight) => {
      if (additionalHeight instanceof DefaultValue) {
        return;
      }

      set(additionalRowHeightMapObjectState(id), (current) => ({
        ...current,
        [adderName]: additionalHeight,
      }));
    };
  },
});
