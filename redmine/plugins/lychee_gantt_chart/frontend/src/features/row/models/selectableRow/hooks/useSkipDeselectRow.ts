import { useRecoilCallback } from 'recoil';
import { skipDeselectAllRowsState } from '../states';

type SkipDeselectRow = () => void;

export const useSkipDeselectRow = (): SkipDeselectRow => {
  return useRecoilCallback(({ set }) => {
    return () => {
      set(skipDeselectAllRowsState, true);
    };
  }, []);
};
