import { atom, useRecoilCallback } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { mainProjectRow, parentIssueRow, rootIssueRow } from '@/__fixtures__';
import { RowID } from '../../../../types';
import { RowBoundingRect } from '../../types';
import { rowBoundingRectState } from '../../states';
import { useRefreshSingleRowBoundingRects } from '../useRefreshSingleRowBoundingRects';

const rowBoundingRectsMock = vi.hoisted(() => vi.fn());

vi.mock('../../states', async (importOriginal) =>
  Object.defineProperties(await importOriginal(), {
    rowBoundingRectsState: { get: rowBoundingRectsMock },
  })
);

const rowBoundingRects: RowBoundingRect[] = [
  { id: mainProjectRow.id, top: 0, height: 20 },
  { id: rootIssueRow.id, top: 20, height: 30 },
  { id: parentIssueRow.id, top: 50, height: 20 },
];

rowBoundingRectsMock.mockReturnValue(
  atom({
    key: 'rowBoundingRectsState/mock',
    default: rowBoundingRects,
  })
);

test('渡されたBoundingRectの配列で個別のRowBoundingRectStateを更新する', () => {
  const { result } = renderRecoilHook(() => ({
    refresh: useRefreshSingleRowBoundingRects(),
    getRowBounds: useRecoilCallback(
      ({ snapshot }) =>
        (id: RowID) =>
          snapshot.getLoadable(rowBoundingRectState(id)).getValue()
    ),
  }));

  act(() => result.current.refresh());

  expect.assertions(3);

  rowBoundingRects.forEach((bounds) => {
    expect(result.current.getRowBounds(bounds.id)).toEqual(bounds);
  });
});
