import { useCallback } from 'react';
import { RowID } from '../types';
import { useRowSelectOptions } from './useRowSelectOptions';
import { useSetSelectedRowIds } from './useSetSelectedRowIds';
import { useToggleSelectedRow } from './useToggleSelectedRow';
import { useSelectRowsInRangeFromFirstSelection } from './useSelectRowsInRangeFromFirstSelection';
import { useSkipDeselectRow } from './useSkipDeselectRow';

type SelectRow = (id: RowID) => void;

export const useSelectRow = (): SelectRow => {
  const rowSelectOptions = useRowSelectOptions();
  const setSelectedRowIds = useSetSelectedRowIds();
  const toggleSelectedRow = useToggleSelectedRow();
  const selectRowsInRangeFromFirstSelection = useSelectRowsInRangeFromFirstSelection();
  const skipUnselectRow = useSkipDeselectRow();

  return useCallback(
    (id) => {
      skipUnselectRow();

      if (rowSelectOptions.multiple) {
        toggleSelectedRow(id);
      } else if (rowSelectOptions.range) {
        selectRowsInRangeFromFirstSelection(id);
      } else {
        setSelectedRowIds([id]);
      }
    },
    [
      skipUnselectRow,
      rowSelectOptions,
      setSelectedRowIds,
      toggleSelectedRow,
      selectRowsInRangeFromFirstSelection,
    ]
  );
};
