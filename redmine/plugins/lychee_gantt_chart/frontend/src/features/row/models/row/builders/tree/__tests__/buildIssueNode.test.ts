import { getIssueResourceIdentifier } from '@/models/coreResource';
import { rootIssue } from '@/__fixtures__';
import { buildIssueNode } from '../buildIssueNode';

test('IssueNodeを構築する', () => {
  expect(buildIssueNode(rootIssue)).toEqual({
    ...rootIssue,
    identifier: getIssueResourceIdentifier(rootIssue.id),
    childNodes: [],
  });
});
