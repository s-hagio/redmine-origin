import { selectorFamily } from 'recoil';
import { RowID } from '../../../types';
import { DEFAULT_HEIGHT } from '../constants';
import { additionalRowHeightsState } from './additionalRowHeightsState';

export const rowHeightState = selectorFamily<number, RowID>({
  key: 'row/rowHeight',
  get: (id) => {
    return ({ get }) => {
      const additionalHeights = get(additionalRowHeightsState(id));

      return additionalHeights.reduce((total, height) => total + height, DEFAULT_HEIGHT);
    };
  },
});
