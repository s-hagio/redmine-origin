import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { parentIssueRow, rootIssueRow } from '@/__fixtures__';
import { selectedRowIdsState, skipDeselectAllRowsState } from '../../states';
import { useDeselectAllRows } from '../useDeselectAllRows';

test('selectedRowIdsに空の配列をセット', () => {
  const { result } = renderRecoilHook(
    () => ({
      deselectAllRows: useDeselectAllRows(),
      selectedRowIds: useRecoilValue(selectedRowIdsState),
    }),
    {
      initializeState: ({ set }) => {
        set(selectedRowIdsState, [rootIssueRow.id, parentIssueRow.id]);
      },
    }
  );

  expect(result.current.selectedRowIds).not.toEqual([]);

  act(() => {
    result.current.deselectAllRows();
  });

  expect(result.current.selectedRowIds).toEqual([]);
});

test('skipDeselectAllRowsStateがtrueの場合、selectedRowIdsを空にしない', () => {
  const { result } = renderRecoilHook(
    () => ({
      deselectAllRows: useDeselectAllRows(),
      selectedRowIds: useRecoilValue(selectedRowIdsState),
    }),
    {
      initializeState: ({ set }) => {
        set(skipDeselectAllRowsState, true);
        set(selectedRowIdsState, [rootIssueRow.id, parentIssueRow.id]);
      },
    }
  );

  act(() => {
    result.current.deselectAllRows();
  });

  expect(result.current.selectedRowIds).toEqual([rootIssueRow.id, parentIssueRow.id]);
});
