import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { mainProjectRow, parentIssueRow, rootIssueRow } from '@/__fixtures__';
import { rowsState } from '../../../row';
import { useRemoveRow } from '../useRemoveRow';

test('指定したRowをリストから削除する', () => {
  const { result } = renderRecoilHook(
    () => ({
      removeRow: useRemoveRow(),
      rows: useRecoilValue(rowsState),
    }),
    {
      initializeState: ({ set }) => {
        set(rowsState, [mainProjectRow, rootIssueRow, parentIssueRow]);
      },
    }
  );

  act(() => result.current.removeRow(rootIssueRow));

  expect(result.current.rows).toStrictEqual([mainProjectRow, parentIssueRow]);
});
