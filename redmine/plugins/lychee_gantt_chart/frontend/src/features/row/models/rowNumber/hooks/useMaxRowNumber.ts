import { useRecoilValue } from 'recoil';
import { maxRowNumberState } from '../states';

export const useMaxRowNumber = () => useRecoilValue(maxRowNumberState);
