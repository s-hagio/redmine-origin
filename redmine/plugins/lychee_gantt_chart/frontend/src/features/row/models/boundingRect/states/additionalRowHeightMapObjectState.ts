import { atomFamily } from 'recoil';
import { RowID } from '../../../types';
import { AdditionalRowHeightAdderName } from '../types';

export const additionalRowHeightMapObjectState = atomFamily<
  Record<AdditionalRowHeightAdderName, number>,
  RowID
>({
  key: 'row/additionalRowHeightMapObject',
  default: () => ({}),
});
