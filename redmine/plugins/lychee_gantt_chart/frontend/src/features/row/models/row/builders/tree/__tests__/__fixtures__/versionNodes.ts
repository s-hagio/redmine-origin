import { getVersionResourceIdentifier } from '@/models/coreResource';
import {
  grandchildProject,
  grandchildProjectVersionSharedByHierarchy,
  grandchildProjectVersionSharedByTree,
  mainProject,
  versionSharedByDescendants,
  versionSharedByHierarchy,
  versionSharedByNone,
  versionSharedBySystem,
  versionSharedByTree,
} from '@/__fixtures__';
import { VersionNode } from '../../types';

const defaults = {
  hasIssueNodes: true,
  issueNodes: [],
};

export const versionSharedByNoneNode: Readonly<VersionNode> = {
  ...versionSharedByNone,
  ...defaults,
  identifier: getVersionResourceIdentifier(mainProject.id, versionSharedByNone.id),
};

export const versionSharedByDescendantsNode: Readonly<VersionNode> = {
  ...versionSharedByDescendants,
  ...defaults,
  identifier: getVersionResourceIdentifier(mainProject.id, versionSharedByDescendants.id),
};

export const versionSharedByHierarchyNode: Readonly<VersionNode> = {
  ...versionSharedByHierarchy,
  ...defaults,
  identifier: getVersionResourceIdentifier(mainProject.id, versionSharedByHierarchy.id),
};

export const versionSharedByTreeNode: Readonly<VersionNode> = {
  ...versionSharedByTree,
  ...defaults,
  identifier: getVersionResourceIdentifier(mainProject.id, versionSharedByTree.id),
};

export const versionSharedBySystemNode: Readonly<VersionNode> = {
  ...versionSharedBySystem,
  ...defaults,
  identifier: getVersionResourceIdentifier(mainProject.id, versionSharedBySystem.id),
};

export const grandchildProjectVersionSharedByHierarchyNode: Readonly<VersionNode> = {
  ...grandchildProjectVersionSharedByHierarchy,
  ...defaults,
  identifier: getVersionResourceIdentifier(
    grandchildProject.id,
    grandchildProjectVersionSharedByHierarchy.id
  ),
};

export const grandchildProjectVersionSharedByTreeNode: Readonly<VersionNode> = {
  ...grandchildProjectVersionSharedByTree,
  ...defaults,
  identifier: getVersionResourceIdentifier(grandchildProject.id, grandchildProjectVersionSharedByTree.id),
};
