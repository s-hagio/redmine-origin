import { Project } from '@/models/project';
import { Version } from '@/models/version';
import { Issue } from '@/models/issue';
import {
  GroupResourceIdentifier,
  IssueResourceIdentifier,
  ProjectResourceIdentifier,
  VersionResourceIdentifier,
} from '@/models/coreResource';
import { Group } from '@/models/group';

export type ProjectNode = Project & {
  identifier: ProjectResourceIdentifier;
  issueNodes: IssueNode[];
  versionNodes: VersionNode[];
  childNodes: ProjectNode[];
  isMainOrAncestorProject: boolean;
  hasVisibleNodes: boolean;
};

export type VersionNode = Version & {
  identifier: VersionResourceIdentifier;
  issueNodes: IssueNode[];
  hasIssueNodes: boolean;
};

export type IssueNode = Issue & {
  identifier: IssueResourceIdentifier;
  childNodes: IssueNode[];
};

export type GroupNode = Group & {
  identifier: GroupResourceIdentifier;
};
