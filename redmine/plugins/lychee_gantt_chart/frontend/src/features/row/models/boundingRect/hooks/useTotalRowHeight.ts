import { useRecoilValue } from 'recoil';
import { totalRowHeightState } from '../states';

export const useTotalRowHeight = () => useRecoilValue(totalRowHeightState);
