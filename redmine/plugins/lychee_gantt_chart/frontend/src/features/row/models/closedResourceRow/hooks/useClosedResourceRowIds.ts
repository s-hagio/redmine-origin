import { useRecoilValue } from 'recoil';
import { closedResourceRowIdsState } from '../states';

export const useClosedResourceRowIds = () => useRecoilValue(closedResourceRowIdsState);
