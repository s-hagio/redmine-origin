import { CoreResourceType } from '@/models/coreResource';
import { SorterFor } from '@/features/sort';
import { SortVersionNodes } from './types';

export const buildVersionNodeSorter = (sorterFor: SorterFor): SortVersionNodes => {
  const sort = sorterFor(CoreResourceType.Version);

  return (subtreeRootNode, versionNodes) => {
    return sort(versionNodes, { subtreeRootId: subtreeRootNode.identifier });
  };
};
