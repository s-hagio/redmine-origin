import { constSelector, useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { coreResourceCollectionState, CoreResourceCollection } from '@/models/coreResource';
import { useSorterFor } from '@/features/sort';
import { mainProject, newStatusGroup, rootIssue, versionSharedByNone } from '@/__fixtures__';
import { buildRowsAsProjectTree, buildRowsAsGroupTree } from '../../builders';
import { rowsState, rowBuildOptionsState, rowOptionsState } from '../../states';
import { useRefreshRows } from '../useRefreshRows';

const groupsStateMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/group', async (importOriginal) =>
  Object.defineProperties(await importOriginal<object>(), {
    groupsState: { get: groupsStateMock },
  })
);

groupsStateMock.mockReturnValue(constSelector(null));

vi.mock('@/features/sort', async () => ({
  ...(await vi.importActual<object>('@/features/sort')),
  useSorterFor: vi.fn().mockReturnValue(vi.fn()),
}));

vi.mock('../../builders', () => ({
  buildRowsAsProjectTree: vi.fn().mockReturnValue([{ id: '__PROJECT_ROW__' }]),
  buildRowsAsGroupTree: vi.fn().mockReturnValue([{ id: '__GROUP_ROW__' }]),
}));

const coreResourceCollection: Readonly<CoreResourceCollection> = {
  projects: [mainProject],
  versions: [versionSharedByNone],
  issues: [rootIssue],
};

const renderForTesting = () => {
  return renderRecoilHook(
    () => ({
      refresh: useRefreshRows(),
      rows: useRecoilValue(rowsState),
      sortFor: useSorterFor(),
      setOptions: useSetRecoilState(rowOptionsState),
      options: useRecoilValue(rowBuildOptionsState),
    }),
    {
      initializeState: ({ set }) => {
        set(coreResourceCollectionState, coreResourceCollection);
      },
    }
  );
};

test('行状態を再構築する', () => {
  const { result } = renderForTesting();

  act(() => result.current.refresh());

  expect(buildRowsAsProjectTree).toBeCalledWith(
    coreResourceCollection,
    result.current.sortFor,
    result.current.options
  );

  expect(buildRowsAsGroupTree).not.toBeCalled();
  expect(result.current.rows).toStrictEqual([{ id: '__PROJECT_ROW__' }]);
});

test('グループがある場合はグループ行を再構築する', () => {
  const groups = [newStatusGroup];
  groupsStateMock.mockReturnValue(constSelector(groups));

  const { result } = renderForTesting();

  act(() => result.current.refresh());

  expect(buildRowsAsGroupTree).toBeCalledWith(
    groups,
    coreResourceCollection.issues,
    result.current.sortFor,
    result.current.options
  );

  expect(buildRowsAsProjectTree).not.toBeCalled();
  expect(result.current.rows).toStrictEqual([{ id: '__GROUP_ROW__' }]);
});

test('skipRefreshオプションがtrueの場合は再構築しない', () => {
  const { result } = renderForTesting();

  act(() => result.current.setOptions((current) => ({ ...current, skipRefresh: true })));
  act(() => result.current.refresh());

  expect(buildRowsAsProjectTree).not.toBeCalled();
  expect(buildRowsAsGroupTree).not.toBeCalled();
  expect(result.current.rows).toStrictEqual([]);
});
