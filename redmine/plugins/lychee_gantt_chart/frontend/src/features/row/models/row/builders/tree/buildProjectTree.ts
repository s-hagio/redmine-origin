import { groupBy, sortBy } from '@/utils/array';
import { isSelfOrAncestorOf, isDescendantOf } from '@/lib/nestedSet';
import {
  CoreResourceCollection,
  getProjectResourceIdentifier,
  getVersionResourceIdentifier,
} from '@/models/coreResource';
import { selectVersionsByProject } from '@/models/version';
import { RowBuildOptions } from '../types';
import { ProjectNode, VersionNode } from './types';
import { buildIssueTree } from './buildIssueTree';

export const buildProjectTree = (
  { projects, versions, issues }: CoreResourceCollection,
  options: RowBuildOptions
): ProjectNode[] => {
  const rootNodes: ProjectNode[] = [];
  const ancestorNodes: ProjectNode[] = [];

  const issuesBySubtreeIdentifier = groupBy(issues, (issue) => {
    if (issue.fixedVersionId) {
      return getVersionResourceIdentifier(issue.projectId, issue.fixedVersionId);
    } else {
      return getProjectResourceIdentifier(issue.projectId);
    }
  });

  const mainProject = options.mainProjectId ? projects.find(({ id }) => options.mainProjectId === id) : null;

  sortBy(projects, 'lft').forEach((project) => {
    const identifier = getProjectResourceIdentifier(project.id);
    const issueNodes = buildIssueTree(issuesBySubtreeIdentifier[identifier], options);

    const versionNodes = selectVersionsByProject(versions, project)
      .map<VersionNode>((version) => {
        const identifier = getVersionResourceIdentifier(project.id, version.id);
        const issueNodes = buildIssueTree(issuesBySubtreeIdentifier[identifier], options);
        const hasIssueNodes = !!issueNodes.length;

        return { ...version, identifier, issueNodes, hasIssueNodes };
      })
      .filter((versionNode) => options.includeEmptyVersions || versionNode.hasIssueNodes);

    const projectNode: ProjectNode = {
      ...project,
      identifier,
      issueNodes,
      versionNodes,
      childNodes: [],
      isMainOrAncestorProject: !!(mainProject && isSelfOrAncestorOf(project, mainProject)),
      hasVisibleNodes: !!(issueNodes.length || versionNodes.length),
    };

    while (ancestorNodes.length && !isDescendantOf(projectNode, ancestorNodes[ancestorNodes.length - 1])) {
      ancestorNodes.pop();
    }

    const closestAncestorNode = ancestorNodes[ancestorNodes.length - 1];

    if (closestAncestorNode) {
      closestAncestorNode.childNodes.push(projectNode);
    } else {
      rootNodes.push(projectNode);
    }

    if (projectNode.hasVisibleNodes) {
      ancestorNodes.forEach((node) => (node.hasVisibleNodes = true));
    }

    ancestorNodes.push(projectNode);
  });

  return rootNodes;
};
