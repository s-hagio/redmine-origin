import { Row, BuildRowsAsProjectTree, RowTreeAttributes } from './types';
import { ProjectNode, VersionNode, IssueNode, buildProjectTree } from './tree';
import {
  buildProjectRow,
  buildVersionRow,
  buildIssueRow,
  buildChildRowTreeAttributes,
} from './buildTypedRow';
import { buildIssueNodeSorter, buildProjectNodeSorter, buildVersionNodeSorter } from './sorter';

export const buildRowsAsProjectTree: BuildRowsAsProjectTree = (collection, sorterFor, options) => {
  const rows: Row[] = [];

  if (!collection.issues.length) {
    return rows;
  }

  const isProjectNodeVisible = (node: ProjectNode): boolean => {
    if (node.isMainOrAncestorProject) {
      return options.includeMainAndAncestorProjects;
    }
    return options.includeEmptyProjects || node.hasVisibleNodes;
  };

  const sortProjectNodes = buildProjectNodeSorter(sorterFor);
  const sortVersionNodes = buildVersionNodeSorter(sorterFor);
  const sortIssueNodes = buildIssueNodeSorter(sorterFor);

  const addProjectRow = (projectNode: ProjectNode, treeAttributes: RowTreeAttributes) => {
    const row = buildProjectRow(projectNode, treeAttributes);
    const treeAttrs = { depth: row.depth, subtreeRootIds: [] };

    if (isProjectNodeVisible(projectNode)) {
      Object.assign(treeAttrs, buildChildRowTreeAttributes(row));
      rows.push(row);
    }

    sortIssueNodes(projectNode, projectNode.issueNodes).forEach((node) => addIssueRow(node, treeAttrs));
    sortVersionNodes(projectNode, projectNode.versionNodes).forEach((node) => addVersionRow(node, treeAttrs));
    sortProjectNodes(projectNode, projectNode.childNodes).forEach((node) => addProjectRow(node, treeAttrs));
  };

  const addVersionRow = (versionNode: VersionNode, treeAttributes: RowTreeAttributes) => {
    if (!options.includeEmptyVersions && !versionNode.issueNodes.length) {
      return;
    }

    const row = buildVersionRow(versionNode, treeAttributes);
    const treeAttrs = buildChildRowTreeAttributes(row);

    rows.push(row);

    sortIssueNodes(versionNode, versionNode.issueNodes).forEach((node) => addIssueRow(node, treeAttrs));
  };

  const addIssueRow = (issueNode: IssueNode, treeAttributes: RowTreeAttributes) => {
    const row = buildIssueRow(issueNode, treeAttributes);
    const treeAttrs = buildChildRowTreeAttributes(row);

    rows.push(row);

    sortIssueNodes(issueNode, issueNode.childNodes).forEach((node) => addIssueRow(node, treeAttrs));
  };

  buildProjectTree(collection, options).forEach((projectNode) => {
    addProjectRow(projectNode, {
      depth: options.rootDepth,
      subtreeRootIds: [],
    });
  });

  return rows;
};
