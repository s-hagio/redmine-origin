import { useCallback, useMemo } from 'react';
import { useAvailableRows } from '../../availableRow';
import { RowID } from '../types';
import { isSelectableRowId } from '../helpers';
import { useSetSelectedRowIds } from './useSetSelectedRowIds';

type SelectRowsInRangeFromFirstSelection = (id: RowID) => void;

export const useSelectRowsInRangeFromFirstSelection = (): SelectRowsInRangeFromFirstSelection => {
  const setSelectedRowIds = useSetSelectedRowIds();

  const availableRows = useAvailableRows();
  const availableRowIds = useMemo(() => availableRows.map(({ id }) => id), [availableRows]);

  return useCallback(
    (id) => {
      setSelectedRowIds((currentSelectedRowIds) => {
        const rowIdFrom = currentSelectedRowIds[0] ?? availableRowIds.find(isSelectableRowId);
        const indexes = [availableRowIds.indexOf(rowIdFrom), availableRowIds.indexOf(id)];

        if (indexes[0] === -1 || indexes[1] === -1) {
          return currentSelectedRowIds;
        }

        if (indexes[0] > indexes[1]) {
          indexes.reverse();
        }

        const selectedRowIds = [currentSelectedRowIds[0] ?? id];

        availableRowIds.slice(indexes[0], indexes[1] + 1).forEach((id) => {
          if (!selectedRowIds.includes(id)) {
            selectedRowIds.push(id);
          }
        });

        return selectedRowIds;
      });
    },
    [availableRowIds, setSelectedRowIds]
  );
};
