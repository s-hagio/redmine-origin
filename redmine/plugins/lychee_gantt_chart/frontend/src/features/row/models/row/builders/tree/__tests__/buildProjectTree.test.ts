import {
  childIssue,
  childProject1,
  childProject2,
  grandchildProject,
  grandchildProjectIssue,
  mainProject,
  parentIssue,
  rootProject,
  versionIssue,
  versionSharedByHierarchy,
  versionSharedByNone,
} from '@/__fixtures__';
import { buildProjectTree } from '../buildProjectTree';
import { ProjectNode } from '../types';
import { RowBuildOptions } from '../../types';
import {
  childIssueNode,
  childProject1Node,
  childProject2Node,
  grandchildProjectIssueNode,
  grandchildProjectNode,
  mainProjectNode,
  parentIssueNode,
  rootProjectNode,
  versionIssueNode,
  versionSharedByHierarchyNode,
  versionSharedByNoneNode,
} from './__fixtures__';

const defaultOptions = {
  keepIssueTreeStructure: true,
  includeMainAndAncestorProjects: true,
  includeEmptyProjects: false,
  includeEmptyVersions: false,
  parallelView: false,
  mainProjectId: null,
  groupBy: null,
  skipRefresh: false,
  rootDepth: 0,
};

describe('mainProjectId: あり', () => {
  const projects = [rootProject, mainProject, childProject1, grandchildProject, childProject2];
  const versions = [versionSharedByNone, versionSharedByHierarchy];
  const issues = [parentIssue, childIssue, versionIssue, grandchildProjectIssue];

  const options: RowBuildOptions = {
    ...defaultOptions,
    mainProjectId: mainProject.id,
  };

  test('ProjectNodeの配列を返す', () => {
    expect(buildProjectTree({ projects, versions, issues }, options)).toStrictEqual<ProjectNode[]>([
      {
        ...rootProjectNode,
        isMainOrAncestorProject: true,
        childNodes: [
          {
            ...mainProjectNode,
            isMainOrAncestorProject: true,
            issueNodes: [
              {
                ...parentIssueNode,
                childNodes: [childIssueNode],
              },
            ],
            versionNodes: [
              {
                ...versionSharedByHierarchyNode,
                issueNodes: [versionIssueNode],
              },
            ],
            childNodes: [
              {
                ...childProject1Node,
                childNodes: [
                  {
                    ...grandchildProjectNode,
                    issueNodes: [grandchildProjectIssueNode],
                  },
                ],
              },
              {
                ...childProject2Node,
                hasVisibleNodes: false,
                childNodes: [],
              },
            ],
          },
        ],
      },
    ]);
  });
});

describe('includeEmptyVersions: true', () => {
  const projects = [mainProject];
  const versions = [versionSharedByNone, versionSharedByHierarchy];
  const issues = [versionIssue];

  const options: RowBuildOptions = {
    ...defaultOptions,
    includeEmptyVersions: true,
  };

  test('チケットを持たないバージョンも含めてProjectNodeの配列を返す', () => {
    expect(buildProjectTree({ projects, versions, issues }, options)).toStrictEqual([
      {
        ...mainProjectNode,
        versionNodes: [
          {
            ...versionSharedByNoneNode,
            hasIssueNodes: false,
          },
          {
            ...versionSharedByHierarchyNode,
            issueNodes: [versionIssueNode],
          },
        ],
      },
    ]);
  });
});
