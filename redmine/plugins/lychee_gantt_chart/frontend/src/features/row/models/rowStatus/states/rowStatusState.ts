import { atomFamily } from 'recoil';
import { RowID } from '../../../types';
import { RowStatus } from '../types';

export const rowStatusState = atomFamily<RowStatus, RowID>({
  key: 'row/rowStatus',
  default: () => ({
    isHighlight: false,
    isAdditionEffectNeeded: false,
    isSelected: false,
  }),
});
