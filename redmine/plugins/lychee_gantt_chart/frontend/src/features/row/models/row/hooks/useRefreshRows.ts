import { useRecoilCallback, useRecoilValue } from 'recoil';
import { coreResourceCollectionState } from '@/models/coreResource';
import { groupsState } from '@/models/group';
import { useSorterFor } from '@/features/sort';
import { rowBuildOptionsState, rowsState } from '../states';
import { buildRowsAsProjectTree, buildRowsAsGroupTree } from '../builders';

export const useRefreshRows = () => {
  const collection = useRecoilValue(coreResourceCollectionState);
  const groups = useRecoilValue(groupsState);
  const sorterFor = useSorterFor();
  const options = useRecoilValue(rowBuildOptionsState);

  return useRecoilCallback(
    ({ set }) => {
      return () => {
        if (options.skipRefresh) {
          return;
        }

        set(rowsState, () => {
          if (groups) {
            return buildRowsAsGroupTree(groups, collection.issues, sorterFor, options);
          }

          return buildRowsAsProjectTree(collection, sorterFor, options);
        });
      };
    },
    [collection, groups, sorterFor, options]
  );
};
