import { selector } from 'recoil';
import { settingsState } from '@/models/setting';
import { issueQueryState } from '@/models/query';
import { RowBuildOptions } from '../builders';
import { rowOptionsState } from './rowOptionsState';
import { overwrittenRowOptionsMapState } from './overwrittenRowOptionsMapState';

const MIN_DEPTH = 0;
const MIN_DEPTH_FOR_ALL = 1;

export const rowBuildOptionsState = selector<RowBuildOptions>({
  key: 'row/rowBuildOptions',
  get: ({ get }) => {
    const rowOptions = get(rowOptionsState);
    const overwrittenRowOptionsMap = get(overwrittenRowOptionsMapState);
    const { mainProject } = get(settingsState);
    const { groupBy } = get(issueQueryState);

    const overwrittenRowOptions = [...overwrittenRowOptionsMap.values()].reduce(
      (memo, opts) => ({ ...memo, ...opts }),
      {}
    );

    return {
      ...rowOptions,
      mainProjectId: mainProject?.id || null,
      groupBy,
      rootDepth: mainProject && rowOptions.includeMainAndAncestorProjects ? MIN_DEPTH : MIN_DEPTH_FOR_ALL,
      ...overwrittenRowOptions,
    };
  },
});
