import { useRecoilValue } from 'recoil';
import { findRowBoundingRectState } from '../functionalStates';

export const useFindRowBoundingRect = () => useRecoilValue(findRowBoundingRectState);
