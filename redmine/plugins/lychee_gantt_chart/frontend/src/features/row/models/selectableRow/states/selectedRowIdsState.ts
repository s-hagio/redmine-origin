import { atom } from 'recoil';
import { RowID } from '../types';

export const selectedRowIdsState = atom<RowID[]>({
  key: 'row/selectedRowIds',
  default: [],
});
