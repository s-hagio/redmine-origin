import { useRecoilCallback } from 'recoil';
import { Row } from '../../row';
import { RowBoundingRect } from '../types';
import { rowBoundingRectsState, rowBoundingRectState } from '../states';
import { useCalculateRowBoundingRects } from './useCalculateRowBoundingRects';

type ResetRowBoundingRectsByRows = (rows: Row[]) => RowBoundingRect[];

export const useResetRowBoundingRectsByRows = (): ResetRowBoundingRectsByRows => {
  const calculateRowBoundingRects = useCalculateRowBoundingRects();

  return useRecoilCallback(
    ({ set, transact_UNSTABLE }) => {
      return (rows) => {
        const calculatedRowBoundingRects = calculateRowBoundingRects(rows);

        set(rowBoundingRectsState, calculatedRowBoundingRects);

        transact_UNSTABLE(({ set }) => {
          calculatedRowBoundingRects.forEach((rect) => {
            set(rowBoundingRectState(rect.id), rect);
          });
        });

        return calculatedRowBoundingRects;
      };
    },
    [calculateRowBoundingRects]
  );
};
