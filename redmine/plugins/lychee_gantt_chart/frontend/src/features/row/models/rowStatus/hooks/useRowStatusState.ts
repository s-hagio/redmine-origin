import { useRecoilState } from 'recoil';
import { RowID } from '../../../types';
import { rowStatusState } from '../states';

export const useRowStatusState = (id: RowID) => useRecoilState(rowStatusState(id));
