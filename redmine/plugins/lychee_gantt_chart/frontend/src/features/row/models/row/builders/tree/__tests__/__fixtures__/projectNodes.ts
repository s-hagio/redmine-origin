import { getProjectResourceIdentifier } from '@/models/coreResource';
import {
  childProject1,
  childProject2,
  grandchildProject,
  mainProject,
  otherTreeProject,
  parentProject,
  rootProject,
} from '@/__fixtures__';
import { ProjectNode } from '../../types';

const defaults = {
  isMainOrAncestorProject: false,
  hasVisibleNodes: true,
  issueNodes: [],
  versionNodes: [],
  childNodes: [],
};

export const rootProjectNode: Readonly<ProjectNode> = {
  ...rootProject,
  ...defaults,
  identifier: getProjectResourceIdentifier(rootProject.id),
};

export const parentProjectNode: Readonly<ProjectNode> = {
  ...parentProject,
  ...defaults,
  identifier: getProjectResourceIdentifier(parentProject.id),
};

export const mainProjectNode: Readonly<ProjectNode> = {
  ...mainProject,
  ...defaults,
  identifier: getProjectResourceIdentifier(mainProject.id),
};

export const childProject1Node: Readonly<ProjectNode> = {
  ...childProject1,
  ...defaults,
  identifier: getProjectResourceIdentifier(childProject1.id),
};

export const grandchildProjectNode: Readonly<ProjectNode> = {
  ...grandchildProject,
  ...defaults,
  identifier: getProjectResourceIdentifier(grandchildProject.id),
};

export const childProject2Node: Readonly<ProjectNode> = {
  ...childProject2,
  ...defaults,
  identifier: getProjectResourceIdentifier(childProject2.id),
};

export const otherTreeProjectNode: Readonly<ProjectNode> = {
  ...otherTreeProject,
  ...defaults,
  identifier: getProjectResourceIdentifier(otherTreeProject.id),
};
