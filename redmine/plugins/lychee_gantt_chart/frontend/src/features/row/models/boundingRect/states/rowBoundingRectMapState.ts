import { selector } from 'recoil';
import { RowID } from '../../../types';
import { RowBoundingRect } from '../types';
import { rowBoundingRectsState } from './rowBoundingRectsState';

type RowBoundingRectsMap = ReadonlyMap<RowID, RowBoundingRect>;

export const rowBoundingRectMapState = selector<RowBoundingRectsMap>({
  key: 'row/rowBoundingRectMap',
  get: ({ get }) => {
    const rowBoundingRects = get(rowBoundingRectsState);

    return new Map(rowBoundingRects.map((bounds) => [bounds.id, bounds]));
  },
});
