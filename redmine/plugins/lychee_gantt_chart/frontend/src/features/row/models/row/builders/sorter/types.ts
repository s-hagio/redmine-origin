import { GroupNode, IssueNode, ProjectNode, VersionNode } from '../tree';

export type SortProjectNodes = <T extends ProjectNode>(subtreeRootNode: ProjectNode, nodes: T[]) => T[];
export type SortVersionNodes = <T extends VersionNode>(subtreeRootNode: ProjectNode, nodes: T[]) => T[];

export type SortIssueNodes = <T extends IssueNode>(
  subtreeRootNode: ProjectNode | VersionNode | IssueNode | GroupNode,
  nodes: T[]
) => T[];

export type SortGroupNodes = <T extends GroupNode>(nodes: T[]) => T[];
