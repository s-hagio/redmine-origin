import { useRecoilValue } from 'recoil';
import { rowBoundingRectMapState } from '../states';

export const useRowBoundingRectMap = () => useRecoilValue(rowBoundingRectMapState);
