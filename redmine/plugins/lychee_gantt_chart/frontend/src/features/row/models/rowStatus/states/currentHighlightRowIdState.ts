import { atom } from 'recoil';
import { RowID } from '../../../types';

export const currentHighlightRowIdState = atom<RowID | null>({
  key: 'row/currentHighlightRowId',
  default: null,
});
