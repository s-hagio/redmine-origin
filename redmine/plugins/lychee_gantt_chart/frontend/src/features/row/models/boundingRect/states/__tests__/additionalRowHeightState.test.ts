import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { additionalRowHeightState } from '../additionalRowHeightState';
import { additionalRowHeightMapObjectState } from '../additionalRowHeightMapObjectState';

const id = 'project-1';

test('set: additionalRowHeightMapObjectにAdderNameごとのRowHeightをセットする', () => {
  const { result } = renderRecoilHook(
    () => ({
      set: useSetRecoilState(additionalRowHeightState({ id, adderName: 'c' })),
      additionalRowHeights: useRecoilValue(additionalRowHeightMapObjectState(id)),
    }),
    {
      initializeState: ({ set }) => {
        set(additionalRowHeightMapObjectState(id), { a: 10, b: 20 });
      },
    }
  );

  expect(result.current.additionalRowHeights).toEqual({ a: 10, b: 20 });

  act(() => result.current.set(30));

  expect(result.current.additionalRowHeights).toEqual({ a: 10, b: 20, c: 30 });
});

test('get: 対象のadditionalHeightを返す', () => {
  const { result } = renderRecoilHook(
    () => useRecoilValue(additionalRowHeightState({ id, adderName: 'a' })),
    {
      initializeState: ({ set }) => {
        set(additionalRowHeightMapObjectState(id), { a: 10, b: 20 });
      },
    }
  );

  expect(result.current).toBe(10);
});
