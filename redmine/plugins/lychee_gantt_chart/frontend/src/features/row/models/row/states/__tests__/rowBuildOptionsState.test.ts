import { MutableSnapshot, useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { omit } from '@/utils/object';
import { renderRecoilHook } from '@/test-utils';
import { issueQueryState } from '@/models/query';
import { settingsState } from '@/models/setting';
import { rowOptionsState } from '../rowOptionsState';
import { rowBuildOptionsState } from '../rowBuildOptionsState';
import { overwrittenRowOptionsMapState } from '../overwrittenRowOptionsMapState';

const renderForTesting = (initializeState?: (mutableSnapshot: MutableSnapshot) => void) => {
  return renderRecoilHook(
    () => ({
      rowBuildOptions: useRecoilValue(rowBuildOptionsState),
      rowOptions: useRecoilValue(rowOptionsState),
      setIssueQuery: useSetRecoilState(issueQueryState),
    }),
    { initializeState }
  );
};

describe('with Main project', () => {
  const mainProject = { id: 123, identifier: 'foo' };

  test('メインプロジェクトがある場合のRow構築オプション', () => {
    const { result } = renderForTesting(({ set }) => {
      set(settingsState, (current) => ({ ...current, mainProject }));
    });

    expect(result.current.rowBuildOptions).toEqual({
      ...result.current.rowOptions,
      mainProjectId: mainProject.id,
      groupBy: null,
      rootDepth: 0,
    });
  });

  test('「自プロジェクトを含む上位プロジェクト」が非表示ならrootDepthを1にする', () => {
    const { result } = renderForTesting(({ set }) => {
      set(settingsState, (current) => ({ ...current, mainProject }));
      set(rowOptionsState, (current) => ({ ...current, includeMainAndAncestorProjects: false }));
    });

    expect(result.current.rowBuildOptions).toEqual({
      ...result.current.rowOptions,
      mainProjectId: mainProject.id,
      groupBy: null,
      rootDepth: 1,
    });
  });

  test('グループ条件指定', () => {
    const { result } = renderForTesting(({ set }) => {
      set(settingsState, (current) => ({ ...current, mainProject }));
    });

    act(() => {
      result.current.setIssueQuery((current) => ({ ...current, groupBy: 'foo' }));
    });

    expect(result.current.rowBuildOptions).toEqual({
      ...result.current.rowOptions,
      mainProjectId: mainProject.id,
      groupBy: 'foo',
      rootDepth: 0,
    });
  });
});

describe('without Main project', () => {
  test('rootDepthを1にする', () => {
    const { result } = renderForTesting(({ set }) => {
      set(settingsState, (current) => omit({ ...current }, ['mainProject']));
    });

    expect(result.current.rowBuildOptions).toEqual({
      ...result.current.rowOptions,
      mainProjectId: null,
      groupBy: null,
      rootDepth: 1,
    });
  });
});

test('上書きオプションがあれば適用', () => {
  const { result } = renderForTesting(({ set }) => {
    set(rowOptionsState, (current) => ({ ...current, includeEmptyProjects: false }));

    set(overwrittenRowOptionsMapState, (current) => {
      const map = new Map(current);
      map.set('foo', { includeEmptyProjects: true });
      return map;
    });
  });

  expect(result.current.rowBuildOptions).toEqual({
    ...result.current.rowOptions,
    mainProjectId: null,
    groupBy: null,
    includeEmptyProjects: true,
    rootDepth: 1,
  });
});
