import { useRecoilValue } from 'recoil';
import { rowBuildOptionsState } from '../states';

export const useRowBuildOptions = () => useRecoilValue(rowBuildOptionsState);
