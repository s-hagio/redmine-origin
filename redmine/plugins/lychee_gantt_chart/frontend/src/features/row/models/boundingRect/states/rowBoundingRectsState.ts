import { atom, selector } from 'recoil';
import { RowID } from '../../../types';
import { availableRowsState } from '../../availableRow';
import { RowBoundingRect } from '../types';
import { calculateRowBoundingRectsState } from '../functionalStates';
import { rowHeightState } from './rowHeightState';

export const rowBoundingRectsState = atom<RowBoundingRect[]>({
  key: 'row/rowBoundingRects',
  default: selector({
    key: 'row/rowBoundingRects/default',
    get: ({ get }) => {
      const availableRows = get(availableRowsState);
      const calculateRowBoundingRects = get(calculateRowBoundingRectsState);

      return calculateRowBoundingRects(availableRows, (id: RowID) => {
        return get(rowHeightState(id));
      });
    },
  }),
});
