import { selector, selectorFamily } from 'recoil';
import { RowID } from '../../../types';
import { rowsState } from '../../row';

export const rowNumberState = selectorFamily<number, RowID>({
  key: 'row/rowNumber',
  get: (id) => {
    return ({ get }) => {
      const rowIndexMap = get(rowIndexMapState);
      const index = rowIndexMap.get(id);

      if (index == null) {
        throw new Error('Row not found');
      }

      return index + 1;
    };
  },
});

const rowIndexMapState = selector<ReadonlyMap<RowID, number>>({
  key: 'row/rowIndexMap',
  get: ({ get }) => {
    const rows = get(rowsState);

    return new Map(rows.map((row, index) => [row.id, index]));
  },
});
