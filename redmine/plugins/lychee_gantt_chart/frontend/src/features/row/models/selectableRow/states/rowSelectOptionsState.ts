import { atom } from 'recoil';
import { RowSelectOptions } from '../types';

export const rowSelectOptionsState = atom<RowSelectOptions>({
  key: 'row/rowSelectOptionsState',
  default: {
    multiple: false,
    range: false,
  },
});
