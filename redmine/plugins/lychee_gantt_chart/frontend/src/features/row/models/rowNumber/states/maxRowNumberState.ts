import { selector } from 'recoil';
import { rowsState } from '../../row';
import { rowNumberState } from './rowNumberState';

export const maxRowNumberState = selector({
  key: 'row/maxRowNumber',
  get: ({ get }) => {
    const rows = get(rowsState);
    const lastRow = rows.at(-1);

    if (!lastRow) {
      return 0;
    }

    return get(rowNumberState(lastRow.id));
  },
});
