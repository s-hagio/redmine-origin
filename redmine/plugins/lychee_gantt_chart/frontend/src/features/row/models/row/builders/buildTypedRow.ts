import { GroupRow, IssueRow, ProjectRow, VersionRow } from '../../../types';
import { GroupNode, IssueNode, ProjectNode, VersionNode } from './tree/types';
import { Row, RowResourceType, RowTreeAttributes } from './types';

export const buildChildRowTreeAttributes = (row: Row): RowTreeAttributes => ({
  depth: row.depth + 1,
  subtreeRootIds: [...row.subtreeRootIds, row.id],
});

export const buildProjectRow = (node: ProjectNode, treeAttributes: RowTreeAttributes): ProjectRow => ({
  id: node.identifier,
  resourceType: RowResourceType.Project,
  resourceId: node.id,
  isCollapsible: !node.isMainOrAncestorProject && (node.hasVisibleNodes || !!node.childNodes.length),
  ...treeAttributes,
});

export const buildVersionRow = (node: VersionNode, treeAttributes: RowTreeAttributes): VersionRow => ({
  id: node.identifier,
  resourceType: RowResourceType.Version,
  resourceId: node.id,
  isCollapsible: !!node.issueNodes.length,
  ...treeAttributes,
});

export const buildIssueRow = (node: IssueNode, treeAttributes: RowTreeAttributes): IssueRow => ({
  id: node.identifier,
  resourceType: RowResourceType.Issue,
  resourceId: node.id,
  isCollapsible: !!node.childNodes.length,
  ...treeAttributes,
});

export const buildGroupRow = (node: GroupNode): GroupRow => ({
  id: node.identifier,
  resourceType: RowResourceType.Group,
  resourceId: node.id,
  isCollapsible: true,
  depth: 1,
  subtreeRootIds: [],
});
