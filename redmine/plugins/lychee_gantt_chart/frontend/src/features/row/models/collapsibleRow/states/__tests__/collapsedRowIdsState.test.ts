import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { COLLAPSED_ROW_IDS_STORAGE_KEY, collapsedRowIdsState } from '../collapsedRowIdsState';

test('初期化時にlocalStorageから折りたたみ状態を復元', () => {
  vi.spyOn(localStorage, 'getItem').mockReturnValue('["project-1","version-1-2"]');

  const { result } = renderRecoilHook(() => useRecoilValue(collapsedRowIdsState));

  expect([...result.current.values()]).toStrictEqual(['project-1', 'version-1-2']);
});

test('折りたたみ状態の変更時にlocalStorageにIDリストを保存', () => {
  vi.spyOn(localStorage, 'setItem');

  const { result } = renderRecoilHook(() => useSetRecoilState(collapsedRowIdsState));

  act(() => result.current(new Set(['project-1', 'version-1-2'])));

  expect(localStorage.setItem).toBeCalledWith(COLLAPSED_ROW_IDS_STORAGE_KEY, '["project-1","version-1-2"]');
});
