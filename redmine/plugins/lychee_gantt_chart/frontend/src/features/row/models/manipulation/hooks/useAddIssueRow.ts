import { useRecoilCallback } from 'recoil';
import { Issue } from '@/models/issue';
import { RowID } from '../../../types';
import { buildIssueNode, buildIssueRow, useRowBuildOptions } from '../../row';
import { rowStatusState } from '../../rowStatus';
import { useInsertRow } from './useInsertRow';

type Options = {
  subtreeRootIds?: RowID[];
  index?: number;
};

type AddIssueRow = (issue: Issue, options?: Options) => void;

export const useAddIssueRow = (): AddIssueRow => {
  const { rootDepth } = useRowBuildOptions();
  const insertRow = useInsertRow();

  return useRecoilCallback(
    ({ set }) => {
      return (issue, options = {}) => {
        const issueNode = buildIssueNode(issue);
        const subtreeRootIds = options.subtreeRootIds ?? [];

        const newRow = buildIssueRow(issueNode, {
          depth: rootDepth + subtreeRootIds.length,
          subtreeRootIds,
        });

        const insertOptions = options.index != null ? { index: options.index } : { append: true };

        insertRow(newRow, insertOptions);

        set(rowStatusState(newRow.id), (current) => ({
          ...current,
          isAdditionEffectNeeded: true,
        }));
      };
    },
    [rootDepth, insertRow]
  );
};
