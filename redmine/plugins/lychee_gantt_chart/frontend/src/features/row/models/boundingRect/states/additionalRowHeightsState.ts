import { selectorFamily } from 'recoil';
import { RowID } from '../../../types';
import { additionalRowHeightMapObjectState } from './additionalRowHeightMapObjectState';

export const additionalRowHeightsState = selectorFamily<number[], RowID>({
  key: 'row/additionalRowHeights',
  get: (id) => {
    return ({ get }) => {
      const mapObject = get(additionalRowHeightMapObjectState(id));

      return Object.values(mapObject);
    };
  },
});
