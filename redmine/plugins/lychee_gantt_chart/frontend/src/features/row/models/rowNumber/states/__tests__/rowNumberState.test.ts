import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { childIssueRow, mainProjectRow, parentIssueRow, rootIssueRow } from '@/__fixtures__';
import { rowsState } from '../../../row';
import { rowNumberState } from '../rowNumberState';

test('Rowのindexを元にした行番号を返す', () => {
  const { result } = renderRecoilHook(
    () => ({
      rootIssueRowNumber: useRecoilValue(rowNumberState(rootIssueRow.id)),
      childIssueRowNumber: useRecoilValue(rowNumberState(childIssueRow.id)),
    }),
    {
      initializeState: ({ set }) => {
        set(rowsState, [mainProjectRow, rootIssueRow, parentIssueRow, childIssueRow]);
      },
    }
  );

  expect(result.current.rootIssueRowNumber).toBe(2);
  expect(result.current.childIssueRowNumber).toBe(4);
});
