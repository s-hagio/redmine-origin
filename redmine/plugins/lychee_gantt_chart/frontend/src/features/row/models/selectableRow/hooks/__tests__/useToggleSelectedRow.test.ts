import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { parentIssueRow, rootIssueRow } from '@/__fixtures__';
import { selectedRowIdsState } from '../../states';
import { useToggleSelectedRow } from '../useToggleSelectedRow';

test('選択をトグルする', () => {
  const { result } = renderRecoilHook(
    () => ({
      toggleSelectedRow: useToggleSelectedRow(),
      selectedRowIds: useRecoilValue(selectedRowIdsState),
    }),
    {
      initializeState: ({ set }) => {
        set(selectedRowIdsState, [rootIssueRow.id]);
      },
    }
  );

  act(() => result.current.toggleSelectedRow(parentIssueRow.id));
  expect(result.current.selectedRowIds).toEqual([rootIssueRow.id, parentIssueRow.id]);

  act(() => result.current.toggleSelectedRow(parentIssueRow.id));
  expect(result.current.selectedRowIds).toEqual([rootIssueRow.id]);

  act(() => result.current.toggleSelectedRow(parentIssueRow.id));
  expect(result.current.selectedRowIds).toEqual([rootIssueRow.id, parentIssueRow.id]);
});
