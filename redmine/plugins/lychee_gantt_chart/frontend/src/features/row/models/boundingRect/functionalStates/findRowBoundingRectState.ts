import { selector } from 'recoil';
import { RowBoundingRect } from '../types';
import { rowBoundingRectsState } from '../states';

type FindRowBoundingRect = (offset: number) => RowBoundingRect | null;

export const findRowBoundingRectState = selector<FindRowBoundingRect>({
  key: 'row/findRowBoundingRect',
  get: ({ get }) => {
    const rowBoundingRects = get(rowBoundingRectsState);

    return (offsetY) => {
      return (
        rowBoundingRects.find((rect) => rect.top <= offsetY && offsetY <= rect.top + rect.height) ?? null
      );
    };
  },
});
