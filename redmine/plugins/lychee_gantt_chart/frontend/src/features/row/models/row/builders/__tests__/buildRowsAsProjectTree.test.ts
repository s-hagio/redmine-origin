import { destructiveSortBy as sortBy } from '@/utils/array';
import { CoreResourceCollection } from '@/models/coreResource';
import { Project } from '@/models/project';
import { Issue } from '@/models/issue';
import { Version } from '@/models/version';
import { Group } from '@/models/group';
import { SorterFor } from '@/features/sort';
import {
  // Project rows
  rootProjectRow,
  parentProjectRow,
  mainProjectRow,
  childProject1Row,
  grandchildProjectRow,
  childProject2Row,

  // Version rows
  childProject1VersionRow,
  childProject2VersionRow,
  grandchildProjectVersionRow,
  mainProjectVersionRow,
  parentProjectVersionRow,

  // Issue rows
  rootIssueRow,
  parentIssueRow,
  childIssueRow,
  grandchildIssueRow,
  otherTreeIssueRow,
  versionIssueRow,
  grandchildProjectIssueRow,
  rootProjectVersionRow,
  rootIssue,
} from '@/__fixtures__';

import { Row, RowBuildOptions } from '../types';
import { buildRowsAsProjectTree } from '../buildRowsAsProjectTree';
import { projects, versions, issues, treedIssues } from './__fixtures__';

const collection: CoreResourceCollection = { projects, versions, issues };
const sortFor: SorterFor = vi.fn(
  () =>
    <T extends Project | Version | Issue | Group>(resources: T[]) =>
      resources
);
const defaultOptions: RowBuildOptions = {
  keepIssueTreeStructure: true,
  includeMainAndAncestorProjects: true,
  includeEmptyProjects: false,
  includeEmptyVersions: false,
  parallelView: false,
  skipRefresh: false,
  mainProjectId: mainProjectRow.resourceId,
  groupBy: null,
  rootDepth: 0,
};

const projectSubtreeIds = [rootProjectRow.id, parentProjectRow.id, mainProjectRow.id];
const treedIssueSubtreeIds = [...projectSubtreeIds, rootIssueRow.id, parentIssueRow.id, childIssueRow.id];
const childProject1SubtreeIds = [...projectSubtreeIds, childProject1Row.id, grandchildProjectRow.id];

test.todo('各リソースをソーターでソートする');

test('プロジェクトツリーの行を返す', () => {
  const rows = buildRowsAsProjectTree(collection, sortFor, defaultOptions);

  expect(rows).toStrictEqual<Row[]>([
    // メインを含む先祖プロジェクトツリー
    { ...rootProjectRow, depth: 0, isCollapsible: false },
    { ...parentProjectRow, depth: 1, isCollapsible: false, subtreeRootIds: projectSubtreeIds.slice(0, 1) },
    { ...mainProjectRow, depth: 2, isCollapsible: false, subtreeRootIds: projectSubtreeIds.slice(0, 2) },

    // メインチケットツリー
    { ...rootIssueRow, depth: 3, subtreeRootIds: treedIssueSubtreeIds.slice(0, 3) },
    { ...parentIssueRow, depth: 4, subtreeRootIds: treedIssueSubtreeIds.slice(0, 4) },
    { ...childIssueRow, depth: 5, subtreeRootIds: treedIssueSubtreeIds.slice(0, 5) },
    { ...grandchildIssueRow, depth: 6, subtreeRootIds: treedIssueSubtreeIds.slice(0, 6) },

    // 別のチケットツリー
    { ...otherTreeIssueRow, depth: 3, subtreeRootIds: projectSubtreeIds },

    // 共有バージョンツリー
    { ...mainProjectVersionRow, depth: 3, subtreeRootIds: projectSubtreeIds },
    { ...versionIssueRow, depth: 4, subtreeRootIds: [...projectSubtreeIds, mainProjectVersionRow.id] },

    // 子プロジェクト1のツリー
    {
      ...childProject1Row,
      depth: 3,
      isCollapsible: true,
      subtreeRootIds: childProject1SubtreeIds.slice(0, 3),
    },
    {
      ...grandchildProjectRow,
      depth: 4,
      isCollapsible: true,
      subtreeRootIds: childProject1SubtreeIds.slice(0, 4),
    },
    { ...grandchildProjectIssueRow, depth: 5, subtreeRootIds: childProject1SubtreeIds.slice(0, 5) },
  ]);
});

describe('keepIssueTreeStructure: false', () => {
  const options = { ...defaultOptions, keepIssueTreeStructure: false };
  // ツリー構造を完全に無視するか確認するため逆順でソート
  const sortFor: SorterFor = vi.fn(
    () =>
      <T extends Project | Version | Issue | Group>(resources: T[]) =>
        sortBy(resources, 'rgt')
  );
  const rows = buildRowsAsProjectTree({ ...collection, issues: treedIssues }, sortFor, options);

  test('チケットをツリー化をしない', () => {
    expect(rows).toStrictEqual([
      { ...rootProjectRow, depth: 0, isCollapsible: false },
      { ...parentProjectRow, depth: 1, isCollapsible: false, subtreeRootIds: projectSubtreeIds.slice(0, 1) },
      { ...mainProjectRow, depth: 2, isCollapsible: false, subtreeRootIds: projectSubtreeIds.slice(0, 2) },
      { ...grandchildIssueRow, depth: 3, subtreeRootIds: projectSubtreeIds },
      { ...childIssueRow, depth: 3, isCollapsible: false, subtreeRootIds: projectSubtreeIds },
      { ...parentIssueRow, depth: 3, isCollapsible: false, subtreeRootIds: projectSubtreeIds },
      { ...rootIssueRow, depth: 3, isCollapsible: false, subtreeRootIds: projectSubtreeIds },
    ]);
  });
});

describe('includeMainAndAncestorProjects: false', () => {
  const options = { ...defaultOptions, includeMainAndAncestorProjects: false, rootDepth: 1 };
  const subtreeRootIds = [rootIssueRow.id, parentIssueRow.id, childIssueRow.id];

  test('自身を含む上位プロジェクトを除外してツリー化', () => {
    const rows = buildRowsAsProjectTree({ ...collection, issues: treedIssues }, sortFor, options);

    expect(rows).toStrictEqual([
      { ...rootIssueRow, depth: 1 },
      { ...parentIssueRow, depth: 2, subtreeRootIds: subtreeRootIds.slice(0, 1) },
      { ...childIssueRow, depth: 3, subtreeRootIds: subtreeRootIds.slice(0, 2) },
      { ...grandchildIssueRow, depth: 4, subtreeRootIds: subtreeRootIds },
    ]);
  });
});

describe('includeEmptyProjects: true', () => {
  const options = { ...defaultOptions, includeEmptyProjects: true };

  test('空のプロジェクトを含めてツリー化', () => {
    const rows = buildRowsAsProjectTree({ ...collection, issues: [rootIssue] }, sortFor, options);

    expect(rows).toStrictEqual([
      { ...rootProjectRow, depth: 0, isCollapsible: false },
      { ...parentProjectRow, depth: 1, isCollapsible: false, subtreeRootIds: projectSubtreeIds.slice(0, 1) },
      { ...mainProjectRow, depth: 2, isCollapsible: false, subtreeRootIds: projectSubtreeIds.slice(0, 2) },
      { ...rootIssueRow, depth: 3, isCollapsible: false, subtreeRootIds: projectSubtreeIds },
      { ...childProject1Row, depth: 3, subtreeRootIds: projectSubtreeIds },
      { ...grandchildProjectRow, depth: 4, subtreeRootIds: [...projectSubtreeIds, childProject1Row.id] },
      { ...childProject2Row, depth: 3, subtreeRootIds: projectSubtreeIds },
    ]);
  });
});

describe('includeEmptyVersions: true', () => {
  const options = { ...defaultOptions, includeEmptyVersions: true };

  test('空のバージョンを含めてツリー化', () => {
    const rows = buildRowsAsProjectTree({ ...collection, issues: [rootIssue] }, sortFor, options);

    expect(rows).toStrictEqual([
      { ...rootProjectRow, depth: 0, isCollapsible: false },
      { ...rootProjectVersionRow, depth: 1, subtreeRootIds: projectSubtreeIds.slice(0, 1) },

      { ...parentProjectRow, depth: 1, isCollapsible: false, subtreeRootIds: projectSubtreeIds.slice(0, 1) },
      { ...parentProjectVersionRow, depth: 2, subtreeRootIds: projectSubtreeIds.slice(0, 2) },

      { ...mainProjectRow, depth: 2, isCollapsible: false, subtreeRootIds: projectSubtreeIds.slice(0, 2) },
      { ...rootIssueRow, depth: 3, isCollapsible: false, subtreeRootIds: projectSubtreeIds },
      { ...mainProjectVersionRow, depth: 3, isCollapsible: false, subtreeRootIds: projectSubtreeIds },

      // 子プロジェクト1のツリー
      { ...childProject1Row, depth: 3, subtreeRootIds: childProject1SubtreeIds.slice(0, 3) },
      { ...childProject1VersionRow, depth: 4, subtreeRootIds: childProject1SubtreeIds.slice(0, 4) },
      {
        ...grandchildProjectRow,
        depth: 4,
        isCollapsible: true,
        subtreeRootIds: childProject1SubtreeIds.slice(0, 4),
      },
      { ...grandchildProjectVersionRow, depth: 5, subtreeRootIds: childProject1SubtreeIds.slice(0, 5) },

      // 子プロジェクト2のツリー
      { ...childProject2Row, depth: 3, isCollapsible: true, subtreeRootIds: projectSubtreeIds },
      { ...childProject2VersionRow, depth: 4, subtreeRootIds: [...projectSubtreeIds, childProject2Row.id] },
    ]);
  });
});

describe('mainProjectId: null', () => {
  const options = { ...defaultOptions, mainProjectId: null, rootDepth: 1 };

  test('最小depthを1でルートも折りたためるプロジェクトツリー行を返す', () => {
    const rows = buildRowsAsProjectTree(collection, sortFor, options);

    expect(rows).toStrictEqual<Row[]>([
      // メインを含む先祖プロジェクトツリー
      { ...rootProjectRow, depth: 1 },
      { ...parentProjectRow, depth: 2, subtreeRootIds: projectSubtreeIds.slice(0, 1) },
      { ...mainProjectRow, depth: 3, subtreeRootIds: projectSubtreeIds.slice(0, 2) },

      // メインチケットツリー
      { ...rootIssueRow, depth: 4, subtreeRootIds: treedIssueSubtreeIds.slice(0, 3) },
      { ...parentIssueRow, depth: 5, subtreeRootIds: treedIssueSubtreeIds.slice(0, 4) },
      { ...childIssueRow, depth: 6, subtreeRootIds: treedIssueSubtreeIds.slice(0, 5) },
      { ...grandchildIssueRow, depth: 7, subtreeRootIds: treedIssueSubtreeIds.slice(0, 6) },

      // 別のチケットツリー
      { ...otherTreeIssueRow, depth: 4, subtreeRootIds: projectSubtreeIds },

      // 共有バージョンツリー
      { ...mainProjectVersionRow, depth: 4, isCollapsible: true, subtreeRootIds: projectSubtreeIds },
      { ...versionIssueRow, depth: 5, subtreeRootIds: [...projectSubtreeIds, mainProjectVersionRow.id] },

      // 子プロジェクトツリー
      {
        ...childProject1Row,
        depth: 4,
        isCollapsible: true,
        subtreeRootIds: childProject1SubtreeIds.slice(0, 3),
      },
      {
        ...grandchildProjectRow,
        depth: 5,
        isCollapsible: true,
        subtreeRootIds: childProject1SubtreeIds.slice(0, 4),
      },
      { ...grandchildProjectIssueRow, depth: 6, subtreeRootIds: childProject1SubtreeIds.slice(0, 5) },
    ]);
  });

  describe('includeMainAndAncestorProjects: false', () => {
    const options = {
      ...defaultOptions,
      includeMainAndAncestorProjects: false,
      mainProjectId: null,
      rootDepth: 1,
    };
    const subtreeRootIds = [...projectSubtreeIds, rootIssueRow.id, parentIssueRow.id, childIssueRow.id];

    test('自身を含む上位プロジェクトの除外は適用しない', () => {
      const rows = buildRowsAsProjectTree({ ...collection, issues: treedIssues }, sortFor, options);

      expect(rows).toStrictEqual([
        { ...rootProjectRow, depth: 1 },
        { ...parentProjectRow, depth: 2, subtreeRootIds: projectSubtreeIds.slice(0, 1) },
        { ...mainProjectRow, depth: 3, subtreeRootIds: projectSubtreeIds.slice(0, 2) },
        { ...rootIssueRow, depth: 4, subtreeRootIds: subtreeRootIds.slice(0, 3) },
        { ...parentIssueRow, depth: 5, subtreeRootIds: subtreeRootIds.slice(0, 4) },
        { ...childIssueRow, depth: 6, subtreeRootIds: subtreeRootIds.slice(0, 5) },
        { ...grandchildIssueRow, depth: 7, subtreeRootIds: subtreeRootIds },
      ]);
    });
  });
});

test('issuesが空なら空配列を返す', () => {
  const rows = buildRowsAsProjectTree({ ...collection, issues: [] }, sortFor, defaultOptions);

  expect(rows).toStrictEqual([]);
});
