import { act, renderHook } from '@testing-library/react';
import { rootIssueRow } from '@/__fixtures__';
import { useSelectRowByOffset } from '../useSelectRowByOffset';

const findRowBoundingRectMock = vi.hoisted(() => vi.fn());
const selectRowMock = vi.hoisted(() => vi.fn());

vi.mock('../../../boundingRect', () => ({
  useFindRowBoundingRect: vi.fn().mockReturnValue(findRowBoundingRectMock),
}));

vi.mock('../useSelectRow', () => ({
  useSelectRow: vi.fn().mockReturnValue(selectRowMock),
}));

test('指定オフセットにマッチする行を選択する', () => {
  const { result } = renderHook(() => useSelectRowByOffset());

  findRowBoundingRectMock.mockReturnValue({ id: rootIssueRow.id });

  act(() => {
    result.current(100);
  });

  expect(findRowBoundingRectMock).toBeCalledWith(100);
  expect(selectRowMock).toBeCalledWith(rootIssueRow.id);
});
