import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { expect } from 'vitest';
import { renderRecoilHook } from '@/test-utils';
import {
  mainProjectRow,
  parentIssueRow,
  rootIssue,
  rootIssueRow,
  rootProjectRow,
  versionIssue,
  versionIssueRow,
} from '@/__fixtures__';
import { rowsState } from '../../../row';
import { useAddIssueRow } from '../useAddIssueRow';
import { rowStatusState } from '../../../rowStatus';

const useRowBuildOptionsMock = vi.hoisted(() => vi.fn());

vi.mock('../../../row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useRowBuildOptions: useRowBuildOptionsMock,
}));

beforeEach(() => {
  useRowBuildOptionsMock.mockReturnValue({ rootDepth: 0 });
});

test('渡したチケットのRowをリストに追加する', () => {
  const { result } = renderRecoilHook(() => ({
    addIssueRow: useAddIssueRow(),
    rows: useRecoilValue(rowsState),
  }));

  expect(result.current.rows).toHaveLength(0);

  act(() => result.current.addIssueRow(rootIssue));

  expect(result.current.rows).toHaveLength(1);
  expect(result.current.rows[0]).toEqual(
    expect.objectContaining({
      id: rootIssueRow.id,
      resourceId: rootIssue.id,
    })
  );
});

test('indexを指定した場合はそのindexに挿入する', () => {
  const { result } = renderRecoilHook(
    () => ({
      addIssueRow: useAddIssueRow(),
      rows: useRecoilValue(rowsState),
    }),
    {
      initializeState: ({ set }) => {
        set(rowsState, [mainProjectRow, rootIssueRow, parentIssueRow]);
      },
    }
  );

  expect(result.current.rows).toHaveLength(3);

  act(() => result.current.addIssueRow(versionIssue, { index: 1 }));

  expect(result.current.rows).toHaveLength(4);
  expect(result.current.rows[1]).toEqual(
    expect.objectContaining({
      id: versionIssueRow.id,
      resourceId: versionIssue.id,
    })
  );
});

test('追加したRowのステータスをセット', () => {
  const { result } = renderRecoilHook(() => ({
    addIssueRow: useAddIssueRow(),
    rowStatus: useRecoilValue(rowStatusState(versionIssueRow.id)),
  }));

  expect(result.current.rowStatus.isAdditionEffectNeeded).toBe(false);

  act(() => result.current.addIssueRow(versionIssue));

  expect(result.current.rowStatus.isAdditionEffectNeeded).toBe(true);
});

test('subtreeRootIdsの要素数とrowBuildOptionsのrootDepthに応じてdepthをセット', () => {
  useRowBuildOptionsMock.mockReturnValue({ rootDepth: 1 });

  const { result } = renderRecoilHook(
    () => ({
      addIssueRow: useAddIssueRow(),
      rows: useRecoilValue(rowsState),
    }),
    {
      initializeState: ({ set }) => {
        set(rowsState, []);
      },
    }
  );

  act(() => {
    result.current.addIssueRow(rootIssue, {
      index: 1,
      subtreeRootIds: [rootProjectRow.id, mainProjectRow.id],
    });
  });

  expect(result.current.rows[0]).toEqual(
    expect.objectContaining({
      depth: 3,
    })
  );
});
