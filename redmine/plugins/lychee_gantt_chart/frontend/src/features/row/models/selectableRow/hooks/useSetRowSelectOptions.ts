import { useSetRecoilState } from 'recoil';
import { rowSelectOptionsState } from '../states';

export const useSetRowSelectOptions = () => useSetRecoilState(rowSelectOptionsState);
