import { renderHook } from '@testing-library/react';
import { childIssueRow, rootIssueRow } from '@/__fixtures__';
import { useRowBoundingRect } from '../useRowBoundingRect';

const useRowBoundingRectMapMock = vi.hoisted(() => vi.fn());

vi.mock('../useRowBoundingRectMap', () => ({
  useRowBoundingRectMap: useRowBoundingRectMapMock,
}));

test('RowごとのRowBoundingRectを返す', () => {
  const map = new Map([
    [rootIssueRow.id, { id: rootIssueRow.id, top: 0, height: 100 }],
    [childIssueRow.id, { id: childIssueRow.id, top: 100, height: 100 }],
  ]);

  useRowBoundingRectMapMock.mockReturnValue(map);

  const { result } = renderHook(() => useRowBoundingRect(rootIssueRow.id));

  expect(result.current).toEqual(map.get(rootIssueRow.id));
});
