export type RowOptions = {
  keepIssueTreeStructure: boolean;
  includeMainAndAncestorProjects: boolean;
  includeEmptyProjects: boolean;
  includeEmptyVersions: boolean;
  parallelView: boolean;
  skipRefresh: boolean;
};
