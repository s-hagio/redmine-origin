import { useRecoilCallback } from 'recoil';
import { Row } from '../../../types';
import { rowsState } from '../../row';

export const useRemoveRow = () => {
  return useRecoilCallback(({ set }) => {
    return (row: Row) => {
      set(rowsState, (rows) => {
        const index = rows.indexOf(row);

        const newRows = [...rows];

        if (index !== -1) {
          newRows.splice(index, 1);
        }

        return newRows;
      });
    };
  }, []);
};
