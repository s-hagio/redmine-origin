import { useRecoilCallback } from 'recoil';
import { binarySearch } from '@/utils/array';
import { useRowBoundingRects } from '../../boundingRect';
import { currentHighlightRowIdState, rowStatusState } from '../states';

type HighlightRowByOffsetTop = (offsetTop: number) => void;

export const useHighlightRowByOffsetTop = (): HighlightRowByOffsetTop => {
  const rowBoundingRects = useRowBoundingRects();

  return useRecoilCallback(
    ({ set, snapshot }) => {
      return (offsetTop) => {
        const rowBounds = binarySearch(rowBoundingRects, (bounds) => {
          if (offsetTop >= bounds.top && offsetTop <= bounds.top + bounds.height) {
            return 0;
          }

          return bounds.top - offsetTop;
        });

        const matchedRowId = rowBounds?.id;
        const currentRowId = snapshot.getLoadable(currentHighlightRowIdState).contents;

        if (currentRowId !== rowBounds) {
          set(rowStatusState(currentRowId), (current) => ({ ...current, isHighlight: false }));
          set(currentHighlightRowIdState, null);
        }

        if (matchedRowId) {
          set(rowStatusState(matchedRowId), (current) => ({ ...current, isHighlight: true }));
          set(currentHighlightRowIdState, matchedRowId);
        }
      };
    },
    [rowBoundingRects]
  );
};
