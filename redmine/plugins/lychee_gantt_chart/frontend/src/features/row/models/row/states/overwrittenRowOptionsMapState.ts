import { atom } from 'recoil';
import { RowOptions } from '../types';

export const overwrittenRowOptionsMapState = atom<ReadonlyMap<string, Partial<RowOptions>>>({
  key: 'row/overwrittenRowOptionsMap',
  default: new Map(),
});
