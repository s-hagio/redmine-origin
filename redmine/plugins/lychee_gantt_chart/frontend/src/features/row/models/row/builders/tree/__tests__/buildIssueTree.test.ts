import { childIssue, grandchildIssue, grandchildIssue2, otherTreeIssue, parentIssue } from '@/__fixtures__';
import { buildIssueTree } from '../buildIssueTree';
import { IssueNode } from '../types';
import {
  childIssueNode,
  grandchildIssue2Node,
  grandchildIssueNode,
  otherTreeIssueNode,
  parentIssueNode,
} from './__fixtures__';

const issues = [otherTreeIssue, grandchildIssue2, parentIssue, childIssue, grandchildIssue];

describe('keepIssueTreeStructure: false', () => {
  const options = { keepIssueTreeStructure: false };

  test('渡された順番通りのIssueNode配列を返す', () => {
    expect(buildIssueTree(issues, options)).toStrictEqual([
      otherTreeIssueNode,
      grandchildIssue2Node,
      parentIssueNode,
      childIssueNode,
      grandchildIssueNode,
    ]);
  });
});

describe('keepIssueTreeStructure: true', () => {
  const options = { keepIssueTreeStructure: true };

  test('ツリーごとにlftでソートしたIssueNode配列を返す', () => {
    expect(buildIssueTree(issues, options)).toStrictEqual<IssueNode[]>([
      otherTreeIssueNode,
      {
        ...parentIssueNode,
        childNodes: [
          {
            ...childIssueNode,
            childNodes: [grandchildIssueNode, grandchildIssue2Node],
          },
        ],
      },
    ]);
  });
});
