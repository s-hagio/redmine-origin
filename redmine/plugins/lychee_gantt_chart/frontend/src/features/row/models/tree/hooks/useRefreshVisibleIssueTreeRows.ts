import { useRecoilTransaction_UNSTABLE } from 'recoil';
import { isIssueResourceIdentifier } from '@/models/coreResource';
import { isIssueRow } from '../../../helpers';
import { IssueRow } from '../../../types';
import { useVisibleRows } from '../../visibleRow';
import { visibleIssueTreeRootRowIdsState, visibleIssueTreeRowsState } from '../states';

export const useRefreshVisibleIssueTreeRows = () => {
  const visibleRows = useVisibleRows();

  return useRecoilTransaction_UNSTABLE(
    ({ set }) => {
      return () => {
        const issueRows = visibleRows.filter(isIssueRow);
        const issueRowIds = new Set<IssueRow['id']>(issueRows.map(({ id }) => id));
        const groupedIssueRows = new Map<IssueRow['id'], IssueRow[]>();

        issueRows.forEach((row) => {
          row.subtreeRootIds.forEach((rootId) => {
            if (!isIssueResourceIdentifier(rootId) || !issueRowIds.has(rootId)) {
              return;
            }

            const rows = groupedIssueRows.get(rootId) || [];
            rows.push(row);
            groupedIssueRows.set(rootId, rows);
          });
        });

        groupedIssueRows.forEach((rows, rootId) => {
          set(visibleIssueTreeRowsState(rootId), rows);
        });

        set(visibleIssueTreeRootRowIdsState, [...groupedIssueRows.keys()]);
      };
    },
    [visibleRows]
  );
};
