import { CoreResourceType } from '@/models/coreResource';
import { SorterFor } from '@/features/sort';
import { SortGroupNodes } from './types';

export const buildGroupNodeSorter = (sorterFor: SorterFor): SortGroupNodes => {
  const sort = sorterFor(CoreResourceType.Group);

  return (groupNodes) => {
    return sort(groupNodes);
  };
};
