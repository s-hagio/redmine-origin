import { useRecoilCallback } from 'recoil';
import { isString } from '@/utils/string';
import { Row } from '../../../types';
import { rowsState } from '../../row';
import { InsertRowParams } from '../types';

type InsertRow = (newRow: Row, params: InsertRowParams) => void;

export const useInsertRow = (): InsertRow => {
  return useRecoilCallback(({ set }) => {
    return (newRow, params) => {
      set(rowsState, (rows) => {
        const newRows = [...rows];

        const currentNewRowIndex = newRows.findIndex(({ id }) => newRow.id === id);

        if (currentNewRowIndex !== -1) {
          newRows.splice(currentNewRowIndex, 1);
        }

        const indexToInsert = (() => {
          if (params.index != null) {
            return params.index;
          }

          if (params.append) {
            return newRows.length;
          }

          const targetRowOrId = params.before || params.after;

          if (!targetRowOrId) {
            throw new Error('Either index or before or after must be specified');
          }

          const targetRowId = isString(targetRowOrId) ? targetRowOrId : targetRowOrId.id;
          const index = newRows.findIndex(({ id }) => targetRowId === id);

          return Math.max(params.before ? index : index + 1, 0);
        })();

        newRows.splice(indexToInsert, 0, newRow);

        return newRows;
      });
    };
  }, []);
};
