import { atom } from 'recoil';
import { restorePersistedJSON, persistJSON } from '@/utils/storage';
import { RowID } from '../../../types';

export const COLLAPSED_ROW_IDS_STORAGE_KEY = 'lycheeGantt:collapsedRowIds';

export const collapsedRowIdsState = atom<Set<RowID>>({
  key: 'collapsedRowIds',
  default: new Set(),
  effects: [
    ({ setSelf, onSet }) => {
      setSelf(new Set(restorePersistedJSON(COLLAPSED_ROW_IDS_STORAGE_KEY, [])));

      onSet((ids) => {
        persistJSON(COLLAPSED_ROW_IDS_STORAGE_KEY, [...ids]);
      });
    },
  ],
});
