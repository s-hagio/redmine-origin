import { renderRecoilHook } from '@/test-utils';
import { rootIssueRow } from '@/__fixtures__';
import { useSelectRow } from '../useSelectRow';
import { rowSelectOptionsState } from '../../states';

const setSelectedRowIdsMock = vi.hoisted(() => vi.fn());
const toggleSelectedRowMock = vi.hoisted(() => vi.fn());
const selectRowsInRangeFromFirstSelectionMock = vi.hoisted(() => vi.fn());
const skipUnselectRowMock = vi.hoisted(() => vi.fn());

vi.mock('../useSetSelectedRowIds', () => ({
  useSetSelectedRowIds: vi.fn().mockReturnValue(setSelectedRowIdsMock),
}));

vi.mock('../useToggleSelectedRow', () => ({
  useToggleSelectedRow: vi.fn().mockReturnValue(toggleSelectedRowMock),
}));

vi.mock('../useSelectRowsInRangeFromFirstSelection', () => ({
  useSelectRowsInRangeFromFirstSelection: vi.fn().mockReturnValue(selectRowsInRangeFromFirstSelectionMock),
}));

vi.mock('../useSkipDeselectRow', () => ({
  useSkipDeselectRow: vi.fn().mockReturnValue(skipUnselectRowMock),
}));

test('multiple: trueならtoggleSelectedRowを呼ぶ', () => {
  const { result } = renderRecoilHook(() => useSelectRow(), {
    initializeState: ({ set }) => {
      set(rowSelectOptionsState, { multiple: true, range: false });
    },
  });

  result.current(rootIssueRow.id);

  expect(skipUnselectRowMock).toBeCalled();
  expect(setSelectedRowIdsMock).not.toBeCalled();
  expect(toggleSelectedRowMock).toBeCalledWith(rootIssueRow.id);
  expect(selectRowsInRangeFromFirstSelectionMock).not.toBeCalled();
});

test('range: trueならselectRowsInRangeFromFirstSelectionを呼ぶ', () => {
  const { result } = renderRecoilHook(() => useSelectRow(), {
    initializeState: ({ set }) => {
      set(rowSelectOptionsState, { multiple: false, range: true });
    },
  });

  result.current(rootIssueRow.id);

  expect(skipUnselectRowMock).toBeCalled();
  expect(setSelectedRowIdsMock).not.toBeCalled();
  expect(toggleSelectedRowMock).not.toBeCalled();
  expect(selectRowsInRangeFromFirstSelectionMock).toBeCalledWith(rootIssueRow.id);
});

test('両方falseならsetSelectedRowIdsを呼ぶ', () => {
  const { result } = renderRecoilHook(() => useSelectRow(), {
    initializeState: ({ set }) => {
      set(rowSelectOptionsState, { multiple: false, range: false });
    },
  });

  result.current(rootIssueRow.id);

  expect(skipUnselectRowMock).toBeCalled();
  expect(setSelectedRowIdsMock).toBeCalledWith([rootIssueRow.id]);
  expect(toggleSelectedRowMock).not.toBeCalled();
  expect(selectRowsInRangeFromFirstSelectionMock).not.toBeCalled();
});
