import {
  // Projects
  rootProject,
  parentProject,
  mainProject,
  childProject1,
  grandchildProject,
  childProject2,

  // Versions
  versionSharedByHierarchy,

  // Issues
  rootIssue,
  parentIssue,
  childIssue,
  grandchildIssue,
  otherTreeIssue,
  versionIssue,
  grandchildProjectIssue,
} from '@/__fixtures__';

export const projects = [
  rootProject,
  parentProject,
  mainProject,
  childProject1,
  grandchildProject,
  childProject2,
];

export const versions = [versionSharedByHierarchy];

export const treedIssues = [rootIssue, parentIssue, childIssue, grandchildIssue];
export const issues = [...treedIssues, otherTreeIssue, versionIssue, grandchildProjectIssue];
