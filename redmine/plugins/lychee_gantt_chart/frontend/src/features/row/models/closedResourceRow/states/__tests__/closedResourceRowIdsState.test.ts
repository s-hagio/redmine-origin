import { constSelector, useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import {
  childIssueRow,
  mainProjectVersionRow,
  parentIssueRow,
  rootIssueRow,
  rootProjectRow,
  versionIssueRow,
} from '@/__fixtures__';
import { rowsState } from '../../../row';
import { closedResourceRowIdsState } from '../closedResourceRowIdsState';

const isClosedResourceRowIdStateMock = vi.hoisted(() => vi.fn());

vi.mock('../isClosedResourceRowIdState', () => ({
  isClosedResourceRowIdState: isClosedResourceRowIdStateMock,
}));

const closedSelector = constSelector(true);
const openSelector = constSelector(false);

test('完了しているリソースのRowIDを配列で返す', () => {
  isClosedResourceRowIdStateMock
    .mockReturnValue(openSelector)
    .mockReturnValueOnce(openSelector)
    .mockReturnValueOnce(closedSelector)
    .mockReturnValueOnce(openSelector)
    .mockReturnValueOnce(closedSelector);

  const { result } = renderRecoilHook(() => useRecoilValue(closedResourceRowIdsState), {
    initializeState: ({ set }) => {
      set(rowsState, [
        rootProjectRow,
        rootIssueRow, // closed
        parentIssueRow,
        childIssueRow, // closed
        mainProjectVersionRow,
        versionIssueRow,
      ]);
    },
  });

  expect(result.current).toEqual(new Set([rootIssueRow.id, childIssueRow.id]));
});
