import { atomFamily, selectorFamily } from 'recoil';
import { RowID } from '../../../types';
import { collapsedRowIdsState } from './collapsedRowIdsState';

export const isRowCollapsedState = atomFamily<boolean, RowID>({
  key: 'row/isRowCollapsed',
  default: selectorFamily({
    key: 'row/isRowCollapsed/default',
    get: (id) => {
      return ({ get }) => {
        const collapsedIds = get(collapsedRowIdsState);

        return collapsedIds.has(id);
      };
    },
  }),
});
