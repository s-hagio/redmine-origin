import { useRecoilSnapshot, useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { mainProjectRow, rootIssueRow, rootProjectRow } from '@/__fixtures__';
import { useResetRowBoundingRectsByRows } from '../useResetRowBoundingRectsByRows';
import { rowBoundingRectsState, rowBoundingRectState } from '../../states';

const rows = [rootProjectRow, mainProjectRow, rootIssueRow];

test('Rowの配列からBoundingRectsを再計算してセット', () => {
  const { result } = renderRecoilHook(() => ({
    resetRowBoundingRectsByRows: useResetRowBoundingRectsByRows(),
    rowBoundingRects: useRecoilValue(rowBoundingRectsState),
    snapshot: useRecoilSnapshot(),
  }));

  expect(result.current.rowBoundingRects).toEqual([]);

  act(() => result.current.resetRowBoundingRectsByRows(rows));

  const rowBoundingRects = [
    { id: rows[0].id, top: 0, height: 30 },
    { id: rows[1].id, top: 30, height: 30 },
    { id: rows[2].id, top: 60, height: 30 },
  ];

  expect(result.current.rowBoundingRects).toEqual(rowBoundingRects);

  expect(result.current.snapshot.getLoadable(rowBoundingRectState(rows[0].id)).getValue()).toEqual(
    rowBoundingRects[0]
  );

  expect(result.current.snapshot.getLoadable(rowBoundingRectState(rows[1].id)).getValue()).toEqual(
    rowBoundingRects[1]
  );

  expect(result.current.snapshot.getLoadable(rowBoundingRectState(rows[2].id)).getValue()).toEqual(
    rowBoundingRects[2]
  );
});
