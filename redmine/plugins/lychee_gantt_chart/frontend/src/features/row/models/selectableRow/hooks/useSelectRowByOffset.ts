import { useCallback } from 'react';
import { useFindRowBoundingRect } from '../../boundingRect';
import { useSelectRow } from './useSelectRow';

type SelectRowByOffset = (offset: number) => void;

export const useSelectRowByOffset = (): SelectRowByOffset => {
  const findRowBindingRect = useFindRowBoundingRect();
  const selectRow = useSelectRow();

  return useCallback(
    (offset) => {
      const bounds = findRowBindingRect(offset);

      if (bounds) {
        selectRow(bounds.id);
      }
    },
    [findRowBindingRect, selectRow]
  );
};
