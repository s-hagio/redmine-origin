import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { mainProjectRow } from '@/__fixtures__';
import { collapsedRowIdsState } from '../../states';
import { useRowCollapseToggle } from '../useRowCollapseToggle';

test('Rowの開閉状態とトグル関数を含むobjectを返す', () => {
  const { result } = renderRecoilHook(() => {
    const { isCollapsed, toggle } = useRowCollapseToggle(mainProjectRow.id);

    return {
      isCollapsed,
      toggle,
      collapsedRowIds: useRecoilValue(collapsedRowIdsState),
    };
  });

  expect(result.current.isCollapsed).toBe(false);
  expect(result.current.collapsedRowIds.has(mainProjectRow.id)).toBe(false);

  act(() => {
    result.current.toggle();
  });

  expect(result.current.isCollapsed).toBe(true);
  expect(result.current.collapsedRowIds.has(mainProjectRow.id)).toBe(true);

  act(() => {
    result.current.toggle();
  });

  expect(result.current.isCollapsed).toBe(false);
  expect(result.current.collapsedRowIds.has(mainProjectRow.id)).toBe(false);
});
