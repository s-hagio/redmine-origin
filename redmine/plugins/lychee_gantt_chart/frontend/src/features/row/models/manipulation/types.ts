import { Row, RowID } from '../../types';

export type InsertRowParams = {
  index?: number;
  after?: Row | RowID | null;
  before?: Row | RowID | null;
  append?: boolean;
};
