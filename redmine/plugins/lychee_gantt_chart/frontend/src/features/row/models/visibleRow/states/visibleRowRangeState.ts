import { atom } from 'recoil';
import { VisibleRowRange } from '../types';

export const visibleRowRangeState = atom<VisibleRowRange>({
  key: 'row/visibleRowRange',
  default: { start: 0, end: 200 },
});
