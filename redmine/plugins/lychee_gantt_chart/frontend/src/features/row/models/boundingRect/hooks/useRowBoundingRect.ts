import { RowID } from '../../../types';
import { RowBoundingRect } from '../types';
import { useRowBoundingRectMap } from './useRowBoundingRectMap';

export const useRowBoundingRect = (id: RowID): RowBoundingRect => {
  const map = useRowBoundingRectMap();

  return map.get(id) ?? { id, top: 0, height: 0 };
};
