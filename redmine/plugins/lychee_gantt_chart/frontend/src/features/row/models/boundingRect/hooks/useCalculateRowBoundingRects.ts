import { useRecoilValue } from 'recoil';
import { calculateRowBoundingRectsState } from '../functionalStates';

export const useCalculateRowBoundingRects = () => useRecoilValue(calculateRowBoundingRectsState);
