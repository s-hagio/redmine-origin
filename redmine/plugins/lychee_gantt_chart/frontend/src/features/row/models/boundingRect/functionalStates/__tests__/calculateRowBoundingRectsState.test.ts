import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { mainProjectRow, rootIssueRow, rootProjectRow } from '@/__fixtures__';
import { RowID } from '../../../../types';
import { additionalRowHeightState } from '../../states';
import { calculateRowBoundingRectsState } from '../calculateRowBoundingRectsState';

const rows = [rootProjectRow, mainProjectRow, rootIssueRow];

const renderHookForTesting = () => {
  return renderRecoilHook(() => useRecoilValue(calculateRowBoundingRectsState), {
    initializeState: ({ set }) => {
      set(additionalRowHeightState({ id: rootProjectRow.id, adderName: 'test' }), 70);
      set(additionalRowHeightState({ id: rootIssueRow.id, adderName: 'test' }), 20);
    },
  });
};

test('Row[]とそれぞれのRowHeightからRowBoundingRectsを計算して返す', () => {
  const { result } = renderHookForTesting();

  expect(result.current(rows)).toEqual([
    { id: rootProjectRow.id, top: 0, height: 100 },
    { id: mainProjectRow.id, top: 100, height: 30 },
    { id: rootIssueRow.id, top: 130, height: 50 },
  ]);
});

test('RowHeightのリゾルバ関数を渡せる', () => {
  const { result } = renderHookForTesting();

  const heightResolver = (id: RowID) => {
    switch (id) {
      case rootProjectRow.id:
        return 200;
      case mainProjectRow.id:
        return 100;
      case rootIssueRow.id:
        return 150;
      default:
        return -1;
    }
  };

  expect(result.current(rows, heightResolver)).toEqual([
    { id: rootProjectRow.id, top: 0, height: 200 },
    { id: mainProjectRow.id, top: 200, height: 100 },
    { id: rootIssueRow.id, top: 300, height: 150 },
  ]);
});
