import { useRecoilValue } from 'recoil';
import { rowBoundingRectsState } from '../states';

export const useRowBoundingRects = () => useRecoilValue(rowBoundingRectsState);
