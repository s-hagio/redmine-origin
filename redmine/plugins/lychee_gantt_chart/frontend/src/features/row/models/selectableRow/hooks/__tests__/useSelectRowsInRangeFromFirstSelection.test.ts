import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import {
  childIssueRow,
  grandchildIssueRow,
  mainProjectRow,
  mainProjectVersionRow,
  parentIssueRow,
  rootIssueRow,
  versionIssueRow,
} from '@/__fixtures__';
import { useSelectRowsInRangeFromFirstSelection } from '../useSelectRowsInRangeFromFirstSelection';
import { selectedRowIdsState } from '../../states';

const useAvailableRowsMock = vi.hoisted(() => vi.fn());

vi.mock('../../../availableRow', () => ({
  useAvailableRows: useAvailableRowsMock,
}));

const availableRows = [
  mainProjectRow,
  rootIssueRow,
  parentIssueRow,
  childIssueRow,
  grandchildIssueRow,
  mainProjectVersionRow,
  versionIssueRow,
] as const;

beforeEach(() => {
  useAvailableRowsMock.mockReturnValue(availableRows);
});

test('最初に選択された行からのレンジで行を選択', () => {
  const { result } = renderRecoilHook(
    () => ({
      selectRowsInRange: useSelectRowsInRangeFromFirstSelection(),
      selectedRowIds: useRecoilValue(selectedRowIdsState),
    }),
    {
      initializeState: ({ set }) => {
        set(selectedRowIdsState, [childIssueRow.id]);
      },
    }
  );

  act(() => {
    result.current.selectRowsInRange(rootIssueRow.id);
  });

  expect(result.current.selectedRowIds).toEqual([childIssueRow.id, rootIssueRow.id, parentIssueRow.id]);

  act(() => {
    result.current.selectRowsInRange(versionIssueRow.id);
  });

  expect(result.current.selectedRowIds).toEqual([
    childIssueRow.id,
    grandchildIssueRow.id,
    versionIssueRow.id,
  ]);
});

test('選択された行がない場合は最初の有効行からの範囲で選択', () => {
  const { result } = renderRecoilHook(
    () => ({
      selectRowsInRange: useSelectRowsInRangeFromFirstSelection(),
      selectedRowIds: useRecoilValue(selectedRowIdsState),
    }),
    {
      initializeState: ({ set }) => {
        set(selectedRowIdsState, []);
      },
    }
  );

  act(() => {
    result.current.selectRowsInRange(childIssueRow.id);
  });

  expect(result.current.selectedRowIds).toEqual([childIssueRow.id, rootIssueRow.id, parentIssueRow.id]);

  act(() => {
    result.current.selectRowsInRange(versionIssueRow.id);
  });

  expect(result.current.selectedRowIds).toEqual([
    childIssueRow.id,
    grandchildIssueRow.id,
    versionIssueRow.id,
  ]);
});
