import { useCallback } from 'react';
import { RowID } from '../types';
import { useSetSelectedRowIds } from './useSetSelectedRowIds';

type ToggleSelectedRow = (id: RowID) => void;

export const useToggleSelectedRow = (): ToggleSelectedRow => {
  const setSelectedRowIds = useSetSelectedRowIds();

  return useCallback(
    (id) => {
      setSelectedRowIds((currentSelectedRowIds) => {
        if (currentSelectedRowIds.includes(id)) {
          return currentSelectedRowIds.filter((selectedId) => selectedId !== id);
        }

        return [...currentSelectedRowIds, id];
      });
    },
    [setSelectedRowIds]
  );
};
