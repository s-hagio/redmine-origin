export type VisibleRowRange = {
  start: number;
  end: number;
};
