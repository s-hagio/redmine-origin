import { useRecoilCallback } from 'recoil';
import { RowOptions } from '../types';
import { OverwriterID, overwriteRowOptionsState } from '../states';

type OverwriteRowOptions = (options: Partial<RowOptions> | null) => void;

export const useOverwriteRowOptions = (id: OverwriterID): OverwriteRowOptions => {
  return useRecoilCallback(
    ({ set }) => {
      return (options) => {
        set(overwriteRowOptionsState(id), options);
      };
    },
    [id]
  );
};
