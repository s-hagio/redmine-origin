import * as React from 'react';
import { css } from '@linaria/core';
import { useRowCollapseToggle } from '../../models/collapsibleRow';
import { RowID } from '../../types';
import CollapsedIcon from './CollapsedIcon';
import ExpandedIcon from './ExpandedIcon';

type Props = {
  id: RowID;
};

const ToggleRowCollapseButton: React.FC<Props> = ({ id }) => {
  const { isCollapsed, toggle } = useRowCollapseToggle(id);

  const handleClick = React.useCallback(
    (event: React.MouseEvent) => {
      event.preventDefault();

      toggle();
    },
    [toggle]
  );

  return (
    <span className={style} onClick={handleClick}>
      {isCollapsed ? <CollapsedIcon /> : <ExpandedIcon />}
    </span>
  );
};

export default React.memo(ToggleRowCollapseButton);

export const style = css`
  display: block;
  width: 18px;
  height: 18px;
  cursor: pointer;
  text-align: center;
`;
