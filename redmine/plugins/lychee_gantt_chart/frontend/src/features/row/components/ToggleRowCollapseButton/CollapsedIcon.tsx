import * as React from 'react';
import { Icon } from '@/components/Icon';

const CollapsedIcon: React.FC = () => {
  return <Icon name="chevronRight" size={18} />;
};

export default CollapsedIcon;
