import * as React from 'react';
import { useRefreshRows } from '../models/row';
import { useRefreshVisibleIssueTreeRows } from '../models/tree';
import { useRefreshSingleRowBoundingRects } from '../models/boundingRect';

const RowStateRefresher: React.FC = () => {
  const refreshRows = useRefreshRows();
  React.useEffect(refreshRows, [refreshRows]);

  const refreshVisibleIssueTreeRows = useRefreshVisibleIssueTreeRows();
  React.useEffect(refreshVisibleIssueTreeRows, [refreshVisibleIssueTreeRows]);

  const refreshSingleRowBoundingRects = useRefreshSingleRowBoundingRects();
  React.useLayoutEffect(refreshSingleRowBoundingRects, [refreshSingleRowBoundingRects]);

  return null;
};

export default React.memo(RowStateRefresher);
