import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import RowSelectKeyboardObserver from '../RowSelectKeyboardObserver';
import { RowSelectOptions } from '../../models/selectableRow/types';

const setSelectRowOptionsMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/selectableRow', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useSetRowSelectOptions: vi.fn().mockReturnValue(setSelectRowOptionsMock),
}));

test.each<[string, RowSelectOptions]>([
  ['shiftKey', { multiple: false, range: true }],
  ['ctrlKey', { multiple: true, range: false }],
  ['metaKey', { multiple: true, range: false }],
])('対象キーの押下状態の変化に応じて行選択オプションを更新', (eventPropKey, expected) => {
  render(<RowSelectKeyboardObserver />);

  expect(setSelectRowOptionsMock).not.toBeCalled();

  fireEvent.keyDown(document, { [eventPropKey]: true });
  expect(setSelectRowOptionsMock).toBeCalledWith(expected);

  fireEvent.keyUp(document, { [eventPropKey]: false });
  expect(setSelectRowOptionsMock).toBeCalledWith({ multiple: false, range: false });
});

test('アンロード時に初期値に戻す', () => {
  render(<RowSelectKeyboardObserver />);

  window.dispatchEvent(new Event('unload'));
  expect(setSelectRowOptionsMock).toBeCalledWith({ multiple: false, range: false });
});
