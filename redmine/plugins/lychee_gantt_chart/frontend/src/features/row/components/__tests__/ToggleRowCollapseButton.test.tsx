import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { mainProjectRow } from '@/__fixtures__';
import { useRowCollapseToggle } from '../../models/collapsibleRow';
import ToggleRowCollapseButton from '../ToggleRowCollapseButton';

vi.mock('../../models/collapsibleRow', async () => ({
  ...(await vi.importActual<object>('../../models/collapsibleRow')),
  useRowCollapseToggle: vi.fn(),
}));

vi.mock('../ToggleRowCollapseButton/CollapsedIcon', () => ({ default: () => <>collapsedIcon</> }));
vi.mock('../ToggleRowCollapseButton/ExpandedIcon', () => ({ default: () => <>expandedIcon</> }));

test('閉じている状態の表示', () => {
  vi.mocked(useRowCollapseToggle).mockReturnValue({ isCollapsed: true, toggle: vi.fn() });

  const { container } = render(<ToggleRowCollapseButton id={mainProjectRow.id} />);

  expect(container).toMatchSnapshot();
});

test('開いている状態の表示', () => {
  vi.mocked(useRowCollapseToggle).mockReturnValue({ isCollapsed: false, toggle: vi.fn() });

  const { container } = render(<ToggleRowCollapseButton id={mainProjectRow.id} />);

  expect(container).toMatchSnapshot();
});

test('クリックで折りたたみのtoggleを実行', () => {
  const toggle = vi.fn();

  vi.mocked(useRowCollapseToggle).mockReturnValue({ isCollapsed: true, toggle });

  const { container } = render(<ToggleRowCollapseButton id={mainProjectRow.id} />);

  fireEvent.click(container.firstChild as HTMLElement);

  expect(toggle).toBeCalledTimes(1);
});
