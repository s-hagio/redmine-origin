import * as React from 'react';
import { renderWithRecoil } from '@/test-utils';
import { rootIssueRow } from '@/__fixtures__';
import Row from '../Row';

test('Render', () => {
  const { container } = renderWithRecoil(<Row rowId={rootIssueRow.id}>CHILD!!</Row>);

  expect(container).toMatchSnapshot();
});
