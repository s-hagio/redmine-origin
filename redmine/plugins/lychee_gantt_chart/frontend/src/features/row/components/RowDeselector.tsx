import * as React from 'react';
import { useDeselectAllRows } from '../models/selectableRow';

const DISTANCE_THRESHOLD = 1;

const RowDeselector: React.FC = () => {
  const deselectAllRows = useDeselectAllRows();

  React.useEffect(() => {
    const handleMouseDown = (event: MouseEvent) => {
      const startClientX = event.clientX;

      const removeEventListeners = () => {
        document.removeEventListener('mousemove', handleMouseMove);
        document.removeEventListener('mouseup', handleMouseUp);
      };

      const handleMouseMove = (event: MouseEvent) => {
        if (Math.abs(startClientX - event.clientX) > DISTANCE_THRESHOLD) {
          removeEventListeners();
        }
      };

      const handleMouseUp = () => {
        deselectAllRows();
        removeEventListeners();
      };

      document.addEventListener('mousemove', handleMouseMove);
      document.addEventListener('mouseup', handleMouseUp);
    };

    document.addEventListener('mousedown', handleMouseDown);

    return () => {
      document.removeEventListener('mousedown', handleMouseDown);
    };
  }, [deselectAllRows]);

  return null;
};

export default React.memo(RowDeselector);
