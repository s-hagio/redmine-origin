import * as React from 'react';
import { useAnimationFrame } from '@/lib/hooks';
import { useVisualizeRows } from '../hooks/useVisualizeRows';

const RowVisualizer: React.FC = () => {
  const elementRef = React.useRef<HTMLDivElement>(null);

  const [request, cancel] = useAnimationFrame();
  const visualizeRows = useVisualizeRows();

  const visualize = React.useCallback(() => {
    if (!elementRef.current) {
      return;
    }

    const offsetTop = Math.round(window.scrollY + elementRef.current.getBoundingClientRect().top);
    const scrollTop = window.scrollY - offsetTop;
    const offsetFrom = Math.max(scrollTop, 0);
    const offsetTo = scrollTop + window.innerHeight;

    visualizeRows(offsetFrom, offsetTo);
  }, [visualizeRows]);

  const handleVisualizableEvent = React.useCallback(() => {
    cancel();
    request(visualize);
  }, [request, cancel, visualize]);

  React.useEffect(() => {
    visualize();

    window.addEventListener('scroll', handleVisualizableEvent, { passive: true });
    window.addEventListener('resize', handleVisualizableEvent, { passive: true });

    return () => {
      window.removeEventListener('scroll', handleVisualizableEvent);
      window.removeEventListener('resize', handleVisualizableEvent);

      cancel();
    };
  }, [visualize, cancel, handleVisualizableEvent]);

  return <div ref={elementRef} />;
};

export default RowVisualizer;
