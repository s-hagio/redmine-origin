import * as React from 'react';
import { render } from '@testing-library/react';
import { rootProjectRow, rootIssueRow } from '@/__fixtures__';
import RowBoundingRectStyles from '../RowBoundingRectStyles';

const useRowBoundingRectsMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/boundingRect', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useRowBoundingRects: useRowBoundingRectsMock,
}));

beforeEach(() => {
  useRowBoundingRectsMock.mockReturnValue([
    { id: rootProjectRow.id, top: 0, height: 10 },
    { id: rootIssueRow.id, top: 10, height: 20 },
  ]);
});

test('RowBoundingRectを元にRowの高さと位置のstyleをレンダリング', () => {
  const { container } = render(<RowBoundingRectStyles />);

  expect(container).toMatchSnapshot();
});
