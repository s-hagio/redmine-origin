import * as React from 'react';
import { Icon } from '@/components/Icon';

const ExpandedIcon: React.FC = () => {
  return <Icon name="chevronDown" size={18} />;
};

export default ExpandedIcon;
