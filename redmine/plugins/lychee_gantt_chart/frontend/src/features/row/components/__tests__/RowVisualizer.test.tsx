import * as React from 'react';
import { render, fireEvent, act } from '@testing-library/react';
import { useVisualizeRows } from '../../hooks/useVisualizeRows';
import RowVisualizer from '../RowVisualizer';

vi.useFakeTimers();

vi.mock('../../hooks/useVisualizeRows', () => ({
  useVisualizeRows: vi.fn(),
}));

beforeEach(() => {
  // Windowの高さ
  vi.spyOn(window, 'innerHeight', 'get').mockImplementation(() => 768);

  vi.spyOn(window, 'requestAnimationFrame').mockImplementation((fn) => setTimeout(fn));
  vi.spyOn(window, 'cancelAnimationFrame').mockImplementation((handle) => clearTimeout(handle));
});

// RowVisualizer要素のオフセット
const OFFSET_TOP = 10;

test('ガントの可視範囲のオフセット値をvisualizeRowsに渡す', () => {
  const visualizeRowsMock = vi.fn();
  vi.mocked(useVisualizeRows).mockReturnValue(visualizeRowsMock);

  // ピンポイントでMockする方法を思いつかないからPrototypeで無理やりMock
  const getBoundingClientRectSpy = vi.spyOn(HTMLDivElement.prototype, 'getBoundingClientRect');

  const scrollTo = (scrollY: number) => {
    getBoundingClientRectSpy.mockReturnValue({ top: OFFSET_TOP - scrollY } as DOMRect);
    fireEvent.scroll(window, { target: { scrollY } });
    vi.advanceTimersToNextTimer();
  };

  act(() => scrollTo(0));

  render(<RowVisualizer />);

  // 初期化時
  expect(visualizeRowsMock).lastCalledWith(0, 758);

  act(() => scrollTo(100));

  // スクロールイベント時
  expect(visualizeRowsMock).lastCalledWith(90, 858);
});
