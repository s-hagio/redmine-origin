import * as React from 'react';
import { useSetRowSelectOptions } from '../models/selectableRow';

const RowSelectKeyboardObserver: React.FC = () => {
  const setRowSelectOptions = useSetRowSelectOptions();

  React.useEffect(() => {
    const handleWindowUnload = () => {
      setRowSelectOptions({ multiple: false, range: false });
    };

    const handleKeyboardEvent = (event: KeyboardEvent) => {
      setRowSelectOptions({
        multiple: event.ctrlKey || event.metaKey,
        range: event.shiftKey,
      });
    };

    window.addEventListener('unload', handleWindowUnload);
    document.addEventListener('keydown', handleKeyboardEvent);
    document.addEventListener('keyup', handleKeyboardEvent);

    return () => {
      window.removeEventListener('unload', handleWindowUnload);
      document.removeEventListener('keydown', handleKeyboardEvent);
      document.removeEventListener('keyup', handleKeyboardEvent);
    };
  }, [setRowSelectOptions]);

  return null;
};

export default React.memo(RowSelectKeyboardObserver);
