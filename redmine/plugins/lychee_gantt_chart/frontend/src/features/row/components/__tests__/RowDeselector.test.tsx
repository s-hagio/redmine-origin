import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import RowDeselector from '../RowDeselector';

const deselectAllRowsMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/selectableRow', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useDeselectAllRows: vi.fn().mockReturnValue(deselectAllRowsMock),
}));

test('mouseUpで行選択の解除処理', () => {
  render(<RowDeselector />);

  fireEvent.mouseDown(document);
  expect(deselectAllRowsMock).not.toBeCalled();

  fireEvent.mouseUp(document);
  expect(deselectAllRowsMock).toBeCalled();
});

test('横方向のドラッグ操作がある場合は行選択の解除処理を行わない', () => {
  render(<RowDeselector />);

  fireEvent.mouseDown(document, { clientX: 1 });
  fireEvent.mouseMove(document, { clientX: 3 });
  fireEvent.mouseUp(document);

  expect(deselectAllRowsMock).not.toBeCalled();
});
