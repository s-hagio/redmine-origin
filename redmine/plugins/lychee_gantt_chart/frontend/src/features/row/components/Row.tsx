import * as React from 'react';
import { css, cx } from '@linaria/core';
import { selectedTransparentBackgroundColor, selectedTransparentBorderColor } from '@/styles';
import { RowID } from '../types';
import { useRowStatusState } from '../models/rowStatus';

type Props = React.ComponentProps<'div'> & {
  rowId: RowID;
};

const Row = React.forwardRef<HTMLDivElement, Props>(function Row(
  { rowId, className, children, onAnimationEnd, ...props },
  ref
) {
  const [status, setStatus] = useRowStatusState(rowId);

  const handleAnimationEnd = React.useCallback(
    (event: React.AnimationEvent<HTMLDivElement>) => {
      onAnimationEnd?.(event);
      setStatus((status) => ({ ...status, isAdditionEffectNeeded: false }));
    },
    [onAnimationEnd, setStatus]
  );

  return (
    <div
      ref={ref}
      className={cx(
        status.isHighlight && highlightStyle,
        status.isAdditionEffectNeeded && additionEffectStyle,
        status.isSelected && selectedStyle,
        className
      )}
      data-row={rowId}
      onAnimationEnd={handleAnimationEnd}
      {...props}
    >
      {children}
    </div>
  );
});

export default Row;

const highlightStyle = css`
  background-color: rgba(135, 152, 171, 0.15);
`;

const additionEffectStyle = css`
  @keyframes additionEffect {
    0% {
      background-color: rgba(255, 243, 185, 0.75);
    }
    100% {
      background-color: rgba(253, 241, 180, 0);
    }
  }

  animation: additionEffect 1.5s ease-in-out;
`;

const selectedStyle = css`
  box-sizing: content-box !important;
  margin: -1px;
  border: 1px solid ${selectedTransparentBorderColor};
  background-color: ${selectedTransparentBackgroundColor};
`;
