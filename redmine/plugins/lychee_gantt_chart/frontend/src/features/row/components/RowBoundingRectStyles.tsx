import * as React from 'react';
import { useRowBoundingRects, DEFAULT_HEIGHT } from '../models/boundingRect';

const RowBoundingRectStyles: React.FC = () => {
  const rowBoundingRects = useRowBoundingRects();

  const styles = React.useMemo(
    () =>
      [
        `[data-row]{height:${DEFAULT_HEIGHT}px}`,

        ...rowBoundingRects.map(({ id, top, height }) => {
          const styles = [`top:${top}px`];

          if (height !== DEFAULT_HEIGHT) {
            styles.push(`height:${height}px`);
          }

          return `[data-row="${id}"]{${styles.join(';')}}`;
        }),
      ].join(''),
    [rowBoundingRects]
  );

  return <style>{styles}</style>;
};

export default React.memo(RowBoundingRectStyles);
