import { useRecoilCallback } from 'recoil';
import { useRowBoundingRects } from '../models/boundingRect';
import { visibleRowRangeState } from '../models/visibleRow';

const NUMBER_OF_BUFFER_ROWS = 20;

export const useVisualizeRows = () => {
  const rowBoundingRects = useRowBoundingRects();

  return useRecoilCallback(
    ({ set }) => {
      return (offsetFrom: number, offsetTo: number) => {
        let startIndex = Infinity;
        let endIndex = -1;

        rowBoundingRects.every((rect, index) => {
          if (rect.top >= offsetFrom && rect.top <= offsetTo) {
            startIndex = Math.min(startIndex, index);
            endIndex = Math.max(endIndex, index);
          }

          // 範囲を下方向に超えた場合は即時ストップ
          return rect.top < offsetTo;
        });

        // 範囲内の行が見つかってない場合は終了
        if (startIndex > endIndex) {
          return;
        }

        startIndex = Math.max(startIndex - NUMBER_OF_BUFFER_ROWS, 0);
        endIndex = Math.min(endIndex + NUMBER_OF_BUFFER_ROWS, rowBoundingRects.length - 1);

        set(visibleRowRangeState, {
          start: startIndex,
          end: endIndex,
        });
      };
    },
    [rowBoundingRects]
  );
};
