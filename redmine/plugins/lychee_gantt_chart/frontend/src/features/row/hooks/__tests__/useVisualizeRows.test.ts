import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { RowBoundingRect } from '../../models/boundingRect';
import { visibleRowRangeState } from '../../models/visibleRow';
import { useVisualizeRows } from '../useVisualizeRows';

const useRowBoundingRectsMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/boundingRect', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useRowBoundingRects: useRowBoundingRectsMock,
}));

const ROW_HEIGHT = 10;

// 100件のRowBoundingRect
const rowBoundingRects: RowBoundingRect[] = [...Array(100).keys()].map((i) => ({
  id: `project-${i + 1}`,
  top: i * ROW_HEIGHT,
  height: ROW_HEIGHT,
}));

beforeEach(() => {
  useRowBoundingRectsMock.mockReturnValue(rowBoundingRects);
});

const renderForTesting = () => {
  return renderRecoilHook(() => ({
    visualizeRows: useVisualizeRows(),
    visibleRowRange: useRecoilValue(visibleRowRangeState),
  }));
};

test('渡されたオフセット値から行の表示範囲を割り当て', () => {
  const { result } = renderForTesting();

  act(() => {
    result.current.visualizeRows(321, 435);
  });

  // ({top: 320} = index: 33) ~ ({ top: 430 } = index: 43) + バッファ前後20件
  expect(result.current.visibleRowRange).toStrictEqual({ start: 13, end: 63 });
});

test('最小・最大indexを超える場合はそれぞれのインデックスを割り当て', () => {
  const { result } = renderForTesting();

  const lastIndex = rowBoundingRects.length - 1;

  act(() => {
    // (最小index - 20) ~ (最大index + 20)が対象
    result.current.visualizeRows(rowBoundingRects[0].top, rowBoundingRects[lastIndex].top);
  });

  expect(result.current.visibleRowRange).toStrictEqual({ start: 0, end: lastIndex });
});
