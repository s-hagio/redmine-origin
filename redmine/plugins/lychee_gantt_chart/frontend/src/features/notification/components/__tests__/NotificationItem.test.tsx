import * as React from 'react';
import { act, fireEvent, render } from '@testing-library/react';
import { NotificationItem as ItemObject, NotificationType } from '../../models/notification';
import Notification from '../NotificationItem';

const removeNotificationItemMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/notification', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useRemoveNotificationItem: vi.fn().mockReturnValue(removeNotificationItemMock),
}));

const item: ItemObject = {
  id: 123,
  type: NotificationType.Error,
  body: 'Error message',
};

beforeEach(() => {
  vi.useFakeTimers();
});

test('Render', () => {
  const { container } = render(<Notification {...item} />);

  expect(container).toMatchSnapshot();
});

test('クローズボタンがクリックされたらフェードアウト後にItemを削除', () => {
  const { container, getByRole } = render(<Notification {...item} />);

  fireEvent.click(getByRole('button'));

  expect(removeNotificationItemMock).not.toBeCalled();

  fireEvent.transitionEnd(container.firstChild as HTMLElement);

  expect(removeNotificationItemMock).toBeCalledWith(item.id);
});

test('5秒後に自動で閉じる', () => {
  const { container } = render(<Notification {...item} />);

  act(() => vi.advanceTimersByTime(5000));

  expect(removeNotificationItemMock).not.toBeCalled();

  fireEvent.transitionEnd(container.firstChild as HTMLElement);

  expect(removeNotificationItemMock).toBeCalledWith(item.id);
});

describe('disableAutoClose: true', () => {
  test('自動で閉じない', () => {
    const { container } = render(<Notification {...item} disableAutoClose />);

    act(() => vi.advanceTimersByTime(5000));

    fireEvent.transitionEnd(container.firstChild as HTMLElement);

    expect(removeNotificationItemMock).not.toBeCalled();
  });
});
