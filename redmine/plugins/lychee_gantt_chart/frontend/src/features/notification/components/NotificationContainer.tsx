import * as React from 'react';
import { css } from '@linaria/core';
import { getZIndex } from '@/styles';
import { useNotificationItems } from '../models/notification';
import Item from './NotificationItem';

const NotificationContainer: React.FC = () => {
  const items = useNotificationItems();

  return (
    <div className={style}>
      {items.map((item) => (
        <Item key={item.id} {...item} />
      ))}
    </div>
  );
};

export default React.memo(NotificationContainer);

const style = css`
  position: fixed;
  top: 0;
  right: 10px;
  left: 10px;
  z-index: ${getZIndex('notification/NotificationContainer')};
`;
