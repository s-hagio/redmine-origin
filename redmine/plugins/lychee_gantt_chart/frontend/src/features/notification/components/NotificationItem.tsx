import * as React from 'react';
import { css, cx } from '@linaria/core';
import { NakedButton } from '@/components/Button';
import { Icon, IconName } from '@/components/Icon';
import {
  NotificationItem as ItemObject,
  NotificationType,
  useRemoveNotificationItem,
} from '../models/notification';

type Props = ItemObject;

const itemNamesByType: Record<NotificationType, IconName> = {
  error: 'error',
  warning: 'warning',
  success: 'check',
  info: 'info',
} as const;

const AUTO_CLOSE_INTERVAL = 5000;

const NotificationItem: React.FC<Props> = ({ id, type, body, ...options }) => {
  const ref = React.useRef<HTMLDivElement>(null);

  const autoCloseTimerIdRef = React.useRef<number | null>(null);
  const [isClosing, setClosing] = React.useState(false);

  const removeNotificationItem = useRemoveNotificationItem();

  const close = React.useCallback(() => {
    setClosing(true);
    ref.current?.addEventListener('transitionend', () => removeNotificationItem(id), false);
  }, [id, removeNotificationItem]);

  const startAutoClose = React.useCallback(() => {
    if (!options.disableAutoClose) {
      autoCloseTimerIdRef.current ??= window.setTimeout(close, AUTO_CLOSE_INTERVAL);
    }
  }, [close, options.disableAutoClose]);

  const cancelAutoClose = React.useCallback(() => {
    if (autoCloseTimerIdRef.current) {
      window.clearTimeout(autoCloseTimerIdRef.current);
      autoCloseTimerIdRef.current = null;
    }
  }, []);

  React.useEffect(() => {
    startAutoClose();
  }, [startAutoClose]);

  return (
    <div
      ref={ref}
      className={cx(style, isClosing && closingStyle)}
      data-type={type}
      onMouseEnter={cancelAutoClose}
      onMouseLeave={startAutoClose}
    >
      <Icon name={itemNamesByType[type]} size={24} />

      <div>
        {options.title && <div className={titleStyle}>{options.title}</div>}
        <div className={messageStyle}>{body}</div>
      </div>

      <NakedButton className={closeButtonStyle} onClick={close}>
        <Icon name="close" />
      </NakedButton>
    </div>
  );
};

export default NotificationItem;

const style = css`
  position: relative;
  display: flex;
  align-items: center;
  gap: 0.75rem;
  width: 100%;
  max-height: 100vh;
  margin-top: 5px;
  padding: 0.75rem 1rem;
  border: 1px solid;
  border-radius: 4px;
  transition:
    max-height ease 0.2s,
    opacity ease-out 0.2s;

  &[data-type='${NotificationType.Error}'] {
    border-color: #f5c6cb;
    background-color: #f8d7da;
    color: #721c24;
  }

  &[data-type='${NotificationType.Warning}'] {
    border-color: #ffeeba;
    background-color: #fff3cd;
    color: #5c4503;
  }

  &[data-type='${NotificationType.Success}'] {
    border-color: #c3e6cb;
    background-color: #d4edda;
    color: #155724;
  }

  &[data-type='${NotificationType.Info}'] {
    border-color: #b8daff;
    background-color: #cce5ff;
    color: #004085;
  }
`;

const closingStyle = css`
  max-height: 0;
  opacity: 0;
`;

const titleStyle = css`
  margin-bottom: 0.25rem;
  font-weight: bold;
`;

const messageStyle = css`
  white-space: pre-wrap;
`;

const closeButtonStyle = css`
  margin-left: auto;
`;
