import * as React from 'react';
import { act } from '@testing-library/react';
import { renderWithRecoilHook } from '@/test-utils';
import { NotificationType, useNotify } from '../../models/notification';
import NotificationContainer from '../NotificationContainer';

vi.mock('../NotificationItem', () => ({
  default: vi.fn((props) => `NotificationItem: ${JSON.stringify(props)}`),
}));

test('Render with items', () => {
  const { result, container } = renderWithRecoilHook(<NotificationContainer />, () => ({
    notify: useNotify(),
  }));

  expect(container).toMatchSnapshot();

  act(() => {
    result.current.notify(NotificationType.Success, 'Success message', { disableAutoClose: true });
    result.current.notify(NotificationType.Error, 'Error!!!!!', { title: 'ERROR' });
  });

  expect(container).toMatchSnapshot();
});
