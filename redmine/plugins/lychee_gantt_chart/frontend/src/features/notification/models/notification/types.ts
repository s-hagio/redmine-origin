import { ReactNode } from 'react';

export type NotificationID = number;

export const NotificationType = {
  Error: 'error',
  Warning: 'warning',
  Success: 'success',
  Info: 'info',
} as const;

export type NotificationType = (typeof NotificationType)[keyof typeof NotificationType];
export type NotificationBody = ReactNode;

export type NotificationItem = {
  id: NotificationID;
  type: NotificationType;
  body: NotificationBody;
  title?: string;
  disableAutoClose?: boolean;
};

export type NotificationOptions = Omit<NotificationItem, 'id' | 'type' | 'body'>;
