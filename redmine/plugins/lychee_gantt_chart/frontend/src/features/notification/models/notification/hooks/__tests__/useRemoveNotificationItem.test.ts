import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { NotificationType } from '../../types';
import { notificationItemsState } from '../../states';
import { useRemoveNotificationItem } from '../useRemoveNotificationItem';

const successItem = { id: 1, type: NotificationType.Success, body: 'Success!' };
const errorItem = { id: 2, type: NotificationType.Error, body: 'Error!' };

test('notificationItemsを削除する', () => {
  const { result } = renderRecoilHook(
    () => ({
      remove: useRemoveNotificationItem(),
      items: useRecoilValue(notificationItemsState),
    }),
    {
      initializeState: ({ set }) => {
        set(notificationItemsState, [successItem, errorItem]);
      },
    }
  );

  act(() => result.current.remove(errorItem.id));

  expect(result.current.items).toStrictEqual([successItem]);
});
