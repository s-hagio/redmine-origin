import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { ErrorResponse, isAPIError } from '@/api';
import { NotificationBody, NotificationOptions } from '../types';
import { useNotify } from './useNotify';

type NotifyAPIError = (error: unknown, options?: NotificationOptions) => void;

export const useNotifyAPIError = (): NotifyAPIError => {
  const { t } = useTranslation();
  const notify = useNotify();

  return useCallback(
    (error, options) => {
      if (!isAPIError(error)) {
        return;
      }

      const body = (() => {
        if (error.response.status === 500) {
          return t('message.unexpectedError');
        }

        return convertErrorResponseToBody(error.response.data);
      })();

      notify('error', body, options);
    },
    [notify, t]
  );
};

const convertErrorResponseToBody = ({ errors, error }: ErrorResponse): NotificationBody => {
  if (errors) {
    return Array.isArray(errors) ? errors.join('\n') : errors;
  }

  return error;
};
