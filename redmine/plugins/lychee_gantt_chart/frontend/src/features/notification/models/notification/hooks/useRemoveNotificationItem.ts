import { useRecoilCallback } from 'recoil';
import { NotificationID } from '../types';
import { notificationItemsState } from '../states';

type RemoveNotification = (id: NotificationID) => void;

export const useRemoveNotificationItem = (): RemoveNotification => {
  return useRecoilCallback(({ set }) => {
    return (id) => {
      set(notificationItemsState, (current) => current.filter((item) => item.id !== id));
    };
  });
};
