import { useMemo } from 'react';
import { useRecoilCallback } from 'recoil';
import { NotificationBody, NotificationItem, NotificationOptions, NotificationType } from '../types';
import { notificationItemsState } from '../states';

type AddNotification = (
  type: NotificationType,
  body: NotificationBody,
  options?: NotificationOptions
) => void;
type TypedAddNotification = (body: NotificationBody, options?: NotificationOptions) => void;

interface Notify extends AddNotification {
  error: TypedAddNotification;
  warning: TypedAddNotification;
  success: TypedAddNotification;
  info: TypedAddNotification;
}

let idCounter = 0;

export const useNotify = (): Notify => {
  const notify = useRecoilCallback(({ set }): AddNotification => {
    return (type, body, options = {}) => {
      const id = ++idCounter;
      const newItem: NotificationItem = { ...options, id, type, body };

      set(notificationItemsState, (current) => [...current, newItem]);
    };
  }, []);

  return useMemo(() => {
    const error: TypedAddNotification = (...args) => notify(NotificationType.Error, ...args);
    const warning: TypedAddNotification = (...args) => notify(NotificationType.Warning, ...args);
    const success: TypedAddNotification = (...args) => notify(NotificationType.Success, ...args);
    const info: TypedAddNotification = (...args) => notify(NotificationType.Info, ...args);

    return Object.defineProperties(notify as Notify, {
      error: { value: error, writable: false },
      warning: { value: warning, writable: false },
      success: { value: success, writable: false },
      info: { value: info, writable: false },
    });
  }, [notify]);
};
