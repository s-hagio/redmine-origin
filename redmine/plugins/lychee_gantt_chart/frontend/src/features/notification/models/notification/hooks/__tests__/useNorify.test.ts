import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { notificationItemsState } from '../../states';
import { NotificationType } from '../../types';
import { useNotify } from '../useNotify';

const renderForTesting = () => {
  return renderRecoilHook(() => ({
    notify: useNotify(),
    items: useRecoilValue(notificationItemsState),
  }));
};

test('通知', () => {
  const { result } = renderForTesting();

  act(() => {
    result.current.notify(NotificationType.Error, 'エラーが発生しました');
  });

  expect(result.current.items).toStrictEqual([
    { id: 1, type: NotificationType.Error, body: 'エラーが発生しました' },
  ]);

  act(() => {
    result.current.notify(NotificationType.Success, '成功です！');
    result.current.notify(NotificationType.Info, '果報は寝て待て');
  });

  expect(result.current.items).toStrictEqual([
    { id: 1, type: NotificationType.Error, body: 'エラーが発生しました' },
    { id: 2, type: NotificationType.Success, body: '成功です！' },
    { id: 3, type: NotificationType.Info, body: '果報は寝て待て' },
  ]);
});

describe('wrappers', () => {
  test.each(Object.values(NotificationType))('notify.%s', (type) => {
    const { result } = renderForTesting();

    act(() => result.current.notify[type]('BODY!!', { disableAutoClose: true }));

    expect(result.current.items).toStrictEqual([
      expect.objectContaining({ type, body: 'BODY!!', disableAutoClose: true }),
    ]);
  });
});
