import { useEffect, useRef } from 'react';
import { Loadable } from 'recoil';
import { APIError, isAPIError } from '@/api';
import { NotificationOptions } from '../types';
import { useNotifyAPIError } from './useNotifyAPIError';

export const useHandleNotifyFromLoadable = <T>(
  loadable: Loadable<T>,
  options?: NotificationOptions
): Loadable<T> => {
  const notifyAPIError = useNotifyAPIError();
  const notifiedErrorRef = useRef<APIError | null>(null);

  useEffect(() => {
    if (
      loadable.state !== 'hasError' ||
      !isAPIError(loadable.contents) ||
      loadable.contents === notifiedErrorRef.current
    ) {
      return;
    }

    notifyAPIError(loadable.contents, options);
    notifiedErrorRef.current = loadable.contents;
  }, [loadable, notifyAPIError, options]);

  return loadable;
};
