import { atom } from 'recoil';
import { NotificationItem } from '../types';

export const notificationItemsState = atom<NotificationItem[]>({
  key: 'notification/notificationItems',
  default: [],
});
