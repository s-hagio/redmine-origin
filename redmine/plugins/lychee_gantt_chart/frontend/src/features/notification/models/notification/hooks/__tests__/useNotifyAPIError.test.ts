import { useRecoilValue } from 'recoil';
import { AxiosError, AxiosResponse } from 'axios';
import { t } from 'i18next';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { NotificationType } from '../../types';
import { notificationItemsState } from '../../states';
import { useNotifyAPIError } from '../useNotifyAPIError';

const renderForTesting = () => {
  return renderRecoilHook(() => ({
    notify: useNotifyAPIError(),
    items: useRecoilValue(notificationItemsState),
  }));
};

describe('StatusCode', () => {
  const message = 'error message!!';
  const createError = (status: number) => {
    return new AxiosError('error message!!', AxiosError.ERR_BAD_REQUEST, undefined, undefined, {
      status,
      data: {
        error: message,
      },
    } as AxiosResponse);
  };

  test.each([403, 404, 422])('%i', (statusCode) => {
    const { result } = renderForTesting();
    const error = createError(statusCode);

    act(() => result.current.notify(error, { title: 'TITLE!!' }));

    expect(result.current.items).toEqual([
      expect.objectContaining({
        id: expect.any(Number),
        type: NotificationType.Error,
        body: 'error message!!',
        title: 'TITLE!!',
      }),
    ]);
  });

  test('500', () => {
    const { result } = renderForTesting();
    const error = createError(500);

    act(() => result.current.notify(error, { title: 'TITLE!!' }));

    expect(result.current.items).toEqual([
      expect.objectContaining({
        id: expect.any(Number),
        type: NotificationType.Error,
        body: t('message.unexpectedError'),
      }),
    ]);
  });
});

test('APIのエラー以外は何もしない', () => {
  const { result } = renderForTesting();
  const error = new Error('error message!!');

  act(() => result.current.notify(error, { title: 'TITLE!!' }));

  expect(result.current.items).toEqual([]);
});
