import { RecoilLoadable } from 'recoil';
import { AxiosError } from 'axios';
import { renderHook } from '@testing-library/react';
import { useHandleNotifyFromLoadable } from '../useHandleNotifyFromLoadable';

const notifyAPIErrorMock = vi.hoisted(() => vi.fn());

vi.mock('../useNotifyAPIError', () => ({
  useNotifyAPIError: vi.fn().mockReturnValue(notifyAPIErrorMock),
}));

test('loadable.contentsがAPIのエラーならnotify', () => {
  const error = new AxiosError('error message!');
  const loadable = RecoilLoadable.error(error);
  const options = { title: 'TITLE!!' };

  expect(notifyAPIErrorMock).not.toBeCalled();

  renderHook(() => useHandleNotifyFromLoadable(loadable, options));

  expect(notifyAPIErrorMock).toBeCalledWith(error, options);
});

test('APIのエラー以外のエラーなら何もしない', () => {
  const error = new Error('error message!');
  const loadable = RecoilLoadable.error(error);

  renderHook(() => useHandleNotifyFromLoadable(loadable));

  expect(notifyAPIErrorMock).not.toBeCalled();
});

test('loadableがエラーでなければ何もしない', () => {
  renderHook(() => useHandleNotifyFromLoadable(RecoilLoadable.loading()));

  expect(notifyAPIErrorMock).not.toBeCalled();

  renderHook(() => useHandleNotifyFromLoadable(RecoilLoadable.of('foo!!')));

  expect(notifyAPIErrorMock).not.toBeCalled();
});

test('重複通知はしない', () => {
  const error = new AxiosError('error message!');
  const loadable = RecoilLoadable.error(error);

  const { rerender } = renderHook(({ options }) => useHandleNotifyFromLoadable(loadable, options), {
    initialProps: { options: {} },
  });

  expect(notifyAPIErrorMock).toBeCalledTimes(1);

  rerender({ options: { title: '' } });

  expect(notifyAPIErrorMock).toBeCalledTimes(1);
});
