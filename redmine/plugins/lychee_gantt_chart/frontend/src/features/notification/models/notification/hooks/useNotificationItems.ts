import { useRecoilValue } from 'recoil';
import { notificationItemsState } from '../states';

export const useNotificationItems = () => useRecoilValue(notificationItemsState);
