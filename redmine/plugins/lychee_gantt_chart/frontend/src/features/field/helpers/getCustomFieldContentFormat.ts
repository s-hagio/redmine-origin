import { CustomFieldFormat } from '@/models/customField';
import { CustomFieldDefinition, FieldFormat } from '@/models/fieldDefinition';

export const getCustomFieldContentFormat = (field: CustomFieldDefinition): FieldFormat => {
  switch (field.format) {
    case CustomFieldFormat.Int:
    case CustomFieldFormat.Float:
    case CustomFieldFormat.Date:
    case CustomFieldFormat.Bool:
      return field.format;

    case CustomFieldFormat.String:
    case CustomFieldFormat.Text:
      return FieldFormat.PlainText;

    default:
      return FieldFormat.Html;
  }
};
