import * as React from 'react';
import { render } from '@testing-library/react';
import ResourceIdContent from '../ResourceIdContent';

test('リソースのIDをレンダリング', () => {
  const { container } = render(<ResourceIdContent value={123} />);

  expect(container.textContent).toBe('#123');
});

test('リソースのIDがない場合は空文字をレンダリング', () => {
  const { container } = render(<ResourceIdContent value="" />);

  expect(container.textContent).toBe('');
});
