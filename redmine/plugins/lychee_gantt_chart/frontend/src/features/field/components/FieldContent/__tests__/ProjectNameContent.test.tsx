import * as React from 'react';
import { render } from '@testing-library/react';
import ProjectNameContent from '../ProjectNameContent';

vi.mock('../AssociatedResourceNameContent', () => ({
  default: vi.fn((props) => `AssociatedResourceNameContent: ${JSON.stringify(props)}`),
}));

test('プロジェクト名をレンダリング', () => {
  const { container } = render(<ProjectNameContent value={123} />);

  expect(container).toMatchSnapshot();
});
