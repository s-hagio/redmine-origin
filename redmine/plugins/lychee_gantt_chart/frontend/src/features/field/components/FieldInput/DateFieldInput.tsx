import * as React from 'react';
import { css, cx } from '@linaria/core';

export type Props = Omit<React.ComponentProps<'input'>, 'type'>;

const DateFieldInput = React.forwardRef<HTMLInputElement, Props>(function DateFieldInput(
  { className, ...props },
  ref
) {
  return <input ref={ref} type="date" className={cx(style, className)} {...props} />;
});

export default React.memo(DateFieldInput);

const style = css``;
