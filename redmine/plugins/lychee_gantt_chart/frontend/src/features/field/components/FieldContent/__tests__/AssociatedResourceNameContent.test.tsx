import * as React from 'react';
import { render } from '@testing-library/react';
import { AssociatedResourceType, useAssociatedResource } from '@/models/associatedResource';
import AssociatedResourceNameContent from '../AssociatedResourceNameContent';

const useAssociatedResourceMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/associatedResource', async () => ({
  ...(await vi.importActual<object>('@/models/associatedResource')),
  useAssociatedResource: useAssociatedResourceMock,
}));

test('AssociatedResourceをレンダリングする', () => {
  const type = AssociatedResourceType.Tracker;
  const id = 1;
  const tracker = { id, name: 'Tracker name!' };

  useAssociatedResourceMock.mockReturnValue(tracker);

  const { container } = render(<AssociatedResourceNameContent type={type} value={id} />);

  expect(container.textContent).toBe(tracker.name);
  expect(useAssociatedResource).toBeCalledWith(type, id);
});
