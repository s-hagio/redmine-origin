import * as React from 'react';
import { render } from '@testing-library/react';
import DateTimeContent from '../DateTimeContent';

const formatTimeMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/dateFormat', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useFormatTime: vi.fn().mockReturnValue(formatTimeMock),
}));

vi.mock('../DateContent', () => ({
  default: vi.fn((props) => `DateContent: ${JSON.stringify(props)}`),
}));

test('時間をフォーマットしてレンダリングする', () => {
  formatTimeMock.mockReturnValue('formatted time string!');

  const { container } = render(<DateTimeContent value="2022-01-01 12:34:56" />);

  expect(container).toMatchSnapshot();
});
