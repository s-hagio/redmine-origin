import * as React from 'react';
import { css } from '@linaria/core';
import { InlineIssueRelation, useIssueURL } from '@/models/issue';
import ResourceIdContent from './ResourceIdContent';

export type Value = InlineIssueRelation[];

type Props = {
  value: Value;
};

const Item: React.FC<InlineIssueRelation> = ({ label, otherIssue }) => {
  const url = useIssueURL(otherIssue.id);

  return (
    <span className={itemStyle}>
      {label}
      <a href={url} title={otherIssue.subject} target="_blank" rel="noreferrer">
        <ResourceIdContent value={otherIssue.id} />
      </a>
    </span>
  );
};

const RelationsContent: React.FC<Props> = ({ value: inlineRelations }) => {
  return (
    <span className={style}>
      {inlineRelations.map((relation) => (
        <Item key={relation.id} {...relation} />
      ))}
    </span>
  );
};

export default RelationsContent;

const style = css``;

const itemStyle = css`
  :not(:last-child)::after {
    content: ',';
    margin-right: 0.5rem;
  }
`;
