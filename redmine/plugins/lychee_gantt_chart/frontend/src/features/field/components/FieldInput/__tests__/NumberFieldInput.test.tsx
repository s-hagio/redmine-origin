import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import NumberFieldInput from '../NumberFieldInput';

test('Render', () => {
  const { container } = render(<NumberFieldInput />);

  expect(container).toMatchSnapshot();
});

test('入力時に数字系の値を正規化', () => {
  const { container } = render(<NumberFieldInput />);
  const input = container.querySelector('input')!;

  fireEvent.change(input, { target: { value: 'ー１２３．９９９あああ' } });

  expect(input.value).toBe('-123.999あああ');
});
