import * as React from 'react';
import { renderWithTranslation } from '@/test-utils';
import BoolContent from '../BoolContent';

test('true相当の値なら「はい」をレンダリング', () => {
  expect(renderWithTranslation(<BoolContent value={true} />).container.textContent).toBe('はい');
  expect(renderWithTranslation(<BoolContent value="1" />).container.textContent).toBe('はい');
});

test('false相当の値なら「いいえ」をレンダリング', () => {
  expect(renderWithTranslation(<BoolContent value={false} />).container.textContent).toBe('いいえ');
  expect(renderWithTranslation(<BoolContent value="0" />).container.textContent).toBe('いいえ');
});

test('空の場合は何もレンダリングしない', () => {
  expect(renderWithTranslation(<BoolContent value={null} />).container.textContent).toBe('');
  expect(renderWithTranslation(<BoolContent value="" />).container.textContent).toBe('');
});
