import * as React from 'react';
import {
  AssociatedResourceType,
  AssociatedResourceID,
  useAssociatedResource,
} from '@/models/associatedResource';

export type Value = AssociatedResourceID;

type Props = {
  type: AssociatedResourceType;
  value: Value;
};

const AssociatedResourceNameContent: React.FC<Props> = ({ type, value }) => {
  const resource = useAssociatedResource(type, value);

  if (!resource) {
    return null;
  }

  return <>{resource.name}</>;
};

export default AssociatedResourceNameContent;
