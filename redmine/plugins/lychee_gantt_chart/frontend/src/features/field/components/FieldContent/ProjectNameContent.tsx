import * as React from 'react';
import { ProjectID } from '@/models/project';
import { AssociatedResourceType } from '@/models/associatedResource';
import AssociatedResourceNameContent from './AssociatedResourceNameContent';

export type Value = ProjectID;

type Props = {
  value: Value;
};

const ProjectNameContent: React.FC<Props> = ({ value }) => {
  return <AssociatedResourceNameContent type={AssociatedResourceType.Project} value={value} />;
};

export default ProjectNameContent;
