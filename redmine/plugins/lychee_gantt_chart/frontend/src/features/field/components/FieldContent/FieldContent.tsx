import * as React from 'react';
import { FieldFormat, FieldValue } from '../../types';
import HtmlContent, { Value as HtmlFieldValue } from './HtmlContent';
import PlainTextContent, { Value as PlainTextFieldValue } from './PlainTextContent';
import StringContent, { Value as StringFieldValue } from './StringContent';
import TextContent, { Value as TextFieldValue } from './TextContent';
import IntContent, { Value as IntFieldValue } from './IntContent';
import FloatContent, { Value as FloatFieldValue } from './FloatContent';
import PercentContent, { Value as PercentFieldValue } from './PercentContent';
import DateContent, { Value as DateFieldValue } from './DateContent';
import DateTimeContent, { Value as DateTimeFieldValue } from './DateTimeContent';
import BoolContent, { Value as BoolFieldValue } from './BoolContent';
import RelationsContent, { Value as RelationsFieldValue } from './RelationsContent';
import AttachmentsContent, { Value as AttachmentsFieldValue } from './AttachmentsContent';
import ResourceIdContent, { Value as ResourceIdFieldValue } from './ResourceIdContent';
import ProjectNameContent, { Value as ProjectNameFieldValue } from './ProjectNameContent';
import VersionNameContent, { Value as VersionNameFieldValue } from './VersionNameContent';
import AssociatedPrincipalNameContent, {
  Value as AssociatedPrincipalFieldValue,
} from './AssociatedPrincipalNameContent';
import IssueStatusColorContent, { Value as IssueStatusColorFieldValue } from './IssueStatusColorContent';
import AssociatedResourceNameContent, {
  Value as AssociatedResourceNameFieldValue,
} from './AssociatedResourceNameContent';

type Props = {
  format: FieldFormat;
  value: FieldValue;
};

const FieldContent: React.FC<Props> = ({ format, value }) => {
  if (value == null) {
    return null;
  }

  switch (format) {
    case FieldFormat.Html:
    case FieldFormat.FormattableText:
      return <HtmlContent value={value as HtmlFieldValue} />;

    case FieldFormat.PlainText:
      return <PlainTextContent value={value as PlainTextFieldValue} />;

    case FieldFormat.String:
      return <StringContent value={value as StringFieldValue} />;

    case FieldFormat.Text:
      return <TextContent value={value as TextFieldValue} />;

    case FieldFormat.Int:
      return <IntContent value={value as IntFieldValue} />;

    case FieldFormat.Float:
    case FieldFormat.FixedFloat:
      return <FloatContent value={value as FloatFieldValue} fixed={format === FieldFormat.FixedFloat} />;

    case FieldFormat.Percent:
    case FieldFormat.DoneRatio:
      return <PercentContent value={value as PercentFieldValue} />;

    case FieldFormat.Date:
      return <DateContent value={value as DateFieldValue} />;

    case FieldFormat.DateTime:
      return <DateTimeContent value={value as DateTimeFieldValue} />;

    case FieldFormat.Bool:
      return <BoolContent value={value as BoolFieldValue} />;

    case FieldFormat.Relations:
      return <RelationsContent value={value as RelationsFieldValue} />;

    case FieldFormat.Attachments:
      return <AttachmentsContent value={value as AttachmentsFieldValue} />;

    case FieldFormat.Id:
      return <ResourceIdContent value={value as ResourceIdFieldValue} />;

    case FieldFormat.Project:
      return <ProjectNameContent value={value as ProjectNameFieldValue} />;

    case FieldFormat.Version:
      return <VersionNameContent value={value as VersionNameFieldValue} />;

    case FieldFormat.AssignedTo:
    case FieldFormat.Principal:
      return <AssociatedPrincipalNameContent value={value as AssociatedPrincipalFieldValue} />;

    case FieldFormat.Tracker:
    case FieldFormat.IssueStatus:
    case FieldFormat.IssuePriority:
    case FieldFormat.IssueCategory:
      return (
        <AssociatedResourceNameContent type={format} value={value as AssociatedResourceNameFieldValue} />
      );

    case FieldFormat.IssueStatusWithColor:
      return <IssueStatusColorContent value={value as IssueStatusColorFieldValue} />;

    default:
      throw new Error(`Unknown field format: ${format}`);
  }
};

export default React.memo(FieldContent);
