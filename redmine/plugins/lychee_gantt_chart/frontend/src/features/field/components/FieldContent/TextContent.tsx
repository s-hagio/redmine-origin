import * as React from 'react';
import { css } from '@linaria/core';

export type Value = string;

type Props = {
  value: Value;
};

const TextContent: React.FC<Props> = ({ value }) => {
  return <span className={style}>{value}</span>;
};

export default TextContent;

const style = css`
  white-space: pre-wrap;
`;
