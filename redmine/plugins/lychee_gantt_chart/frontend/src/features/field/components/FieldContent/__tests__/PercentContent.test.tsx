import * as React from 'react';
import { render } from '@testing-library/react';
import PercentContent from '../PercentContent';

test('パーセントをレンダリング', () => {
  const { container } = render(<PercentContent value={56.78} />);

  expect(container.textContent).toBe('56.78%');
});
