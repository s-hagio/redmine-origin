import * as React from 'react';
import { render } from '@testing-library/react';
import StringFieldInput from '../StringFieldInput';

test('Render', () => {
  const { container } = render(<StringFieldInput />);

  expect(container).toMatchSnapshot();
});
