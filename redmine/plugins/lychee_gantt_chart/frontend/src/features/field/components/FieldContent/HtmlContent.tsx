import * as React from 'react';

export type Value = string;

type Props = {
  value: Value;
};

const HtmlContent: React.FC<Props> = ({ value }) => {
  return <div dangerouslySetInnerHTML={{ __html: value }} />;
};

export default HtmlContent;
