import * as React from 'react';
import { css, cx } from '@linaria/core';

export type Props = Omit<React.ComponentProps<'input'>, 'type'>;

const NumberFieldInput = React.forwardRef<HTMLInputElement, Props>(function IntFieldInput(
  { className, onChange, ...props },
  ref
) {
  const handleChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const el = event.currentTarget;

      el.value = el.value
        .replace(/[０-９．]/g, (s) => String.fromCharCode(s.charCodeAt(0) - 0xfee0)) // 全角数字 to 半角数字
        .replace(/[－―−‐ー]/g, '-'); // 全角マイナスっぽいもの to 半角マイナス

      onChange?.(event);
    },
    [onChange]
  );

  return <input ref={ref} type="tel" className={cx(style, className)} onChange={handleChange} {...props} />;
});

export default React.memo(NumberFieldInput);

const style = css``;
