import * as React from 'react';
import { css, cx } from '@linaria/core';

export type Props = Omit<React.ComponentProps<'input'>, 'type'>;

const StringFieldInput = React.forwardRef<HTMLInputElement, Props>(function StringFieldInput(
  { className, ...props },
  ref
) {
  return <input ref={ref} type="text" className={cx(style, className)} {...props} />;
});

export default React.memo(StringFieldInput);

const style = css``;
