import * as React from 'react';
import { renderWithRecoil } from '@/test-utils';
import { AssociatedPrincipal, associatedResourceCollectionState } from '@/models/associatedResource';
import { associatedResourceCollection, lockedUser, user1 } from '@/__fixtures__';
import AssociatedPrincipalNameContent from '../AssociatedPrincipalNameContent';

const renderForTesting = (userId: AssociatedPrincipal['id']) => {
  return renderWithRecoil(<AssociatedPrincipalNameContent value={userId} />, {
    initializeState: ({ set }) => {
      set(associatedResourceCollectionState, {
        ...associatedResourceCollection,
        principals: [...associatedResourceCollection.principals, lockedUser],
      });
    },
  });
};

test('Render', () => {
  const { container } = renderForTesting(user1.id);

  expect(container).toMatchSnapshot();
});

test('Render: Locked user', () => {
  const { container } = renderForTesting(lockedUser.id);

  expect(container).toMatchSnapshot();
});
