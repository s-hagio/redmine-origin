import * as React from 'react';
import { css, cx } from '@linaria/core';
import {
  useAssociatedResource,
  AssociatedPrincipal,
  AssociatedResourceType,
} from '@/models/associatedResource';
import { disabledTextColor } from '@/styles';

export type Value = AssociatedPrincipal['id'];

type Props = {
  value: Value;
};

const AssociatedPrincipalNameContent: React.FC<Props> = ({ value }) => {
  const principal = useAssociatedResource(AssociatedResourceType.Principal, value);

  if (!principal) {
    return null;
  }

  return <span className={cx(principal.isLocked && lockedStyle)}>{principal.name}</span>;
};

export default AssociatedPrincipalNameContent;

const lockedStyle = css`
  color: ${disabledTextColor};
`;
