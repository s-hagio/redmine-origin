import * as React from 'react';
import { render } from '@testing-library/react';
import { AssociatedResourceType } from '@/models/associatedResource';
import { newIssueStatus } from '@/__fixtures__';
import IssueStatusColorContent from '../IssueStatusColorContent';

const useAssociatedResourceMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/associatedResource', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useAssociatedResource: useAssociatedResourceMock,
}));

test('Render', () => {
  useAssociatedResourceMock.mockReturnValue(newIssueStatus);

  const { container } = render(<IssueStatusColorContent value={1} />);

  expect(useAssociatedResourceMock).toBeCalledWith(AssociatedResourceType.IssueStatus, 1);
  expect(container).toMatchSnapshot();
});

test('Render with null', () => {
  useAssociatedResourceMock.mockReturnValue(null);

  const { container } = render(<IssueStatusColorContent value={1} />);

  expect(container.innerHTML).toBe('');
});
