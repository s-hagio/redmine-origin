import * as React from 'react';
import { useTranslation } from 'react-i18next';

export type Value = boolean | '1' | '0' | '' | null;

type Props = {
  value: Value;
};

const BoolContent: React.FC<Props> = ({ value }) => {
  const { t } = useTranslation();

  if (value === null || value === '') {
    return null;
  }

  return <>{t(value === true || value === '1' ? 'label.yes' : 'label.no')}</>;
};

export default BoolContent;
