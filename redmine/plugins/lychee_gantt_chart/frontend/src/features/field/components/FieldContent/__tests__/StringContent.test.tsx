import * as React from 'react';
import { render } from '@testing-library/react';
import StringContent from '../StringContent';

test('文字列をレンダリング', () => {
  const { container } = render(<StringContent value="lorem ipsum" />);

  expect(container.textContent).toBe('lorem ipsum');
});
