import * as React from 'react';
import { render } from '@testing-library/react';
import HtmlContent from '../HtmlContent';

test('HTML文字列をHTMLとしてレンダリング', () => {
  const { getByTestId } = render(<HtmlContent value={'<p data-testid="myParagraph">Lorem ipsum</p>'} />);

  expect(getByTestId('myParagraph').textContent).toBe('Lorem ipsum');
});
