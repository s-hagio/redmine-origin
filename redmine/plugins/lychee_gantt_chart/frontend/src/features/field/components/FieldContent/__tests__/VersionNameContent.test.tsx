import * as React from 'react';
import { render } from '@testing-library/react';
import VersionNameContent from '../VersionNameContent';

vi.mock('../AssociatedResourceNameContent', () => ({
  default: vi.fn((props) => `AssociatedResourceNameContent: ${JSON.stringify(props)}`),
}));

test('バージョン名をレンダリング', () => {
  const { container } = render(<VersionNameContent value={999} />);

  expect(container).toMatchSnapshot();
});
