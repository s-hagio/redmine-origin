import * as React from 'react';
import { render } from '@testing-library/react';
import FloatContent from '../FloatContent';

test('浮動小数点数をレンダリングする', () => {
  const { container } = render(<FloatContent value={12.3} />);

  expect(container.textContent).toBe('12.3');
});

test('fixedがtrueなら小数点第二位までゼロ埋め', () => {
  const { container, rerender } = render(<FloatContent value={12} fixed />);
  expect(container.textContent).toBe('12.00');

  rerender(<FloatContent value={12.1} fixed />);
  expect(container.textContent).toBe('12.10');

  rerender(<FloatContent value={12.01} fixed />);
  expect(container.textContent).toBe('12.01');

  rerender(<FloatContent value={12.001} fixed />);
  expect(container.textContent).toBe('12.00');
});
