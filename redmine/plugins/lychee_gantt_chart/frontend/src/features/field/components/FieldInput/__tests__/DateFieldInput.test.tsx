import * as React from 'react';
import { render } from '@testing-library/react';
import DateFieldInput from '../DateFieldInput';

test('Render', () => {
  const { container } = render(<DateFieldInput />);

  expect(container).toMatchSnapshot();
});
