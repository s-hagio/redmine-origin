import * as React from 'react';
import { ISODateString, toDate } from '@/lib/date';
import { useFormatTime } from '@/models/dateFormat';
import DateContent from './DateContent';

export type Value = Date | ISODateString;

type Props = {
  value: Value;
};

const DateTimeContent: React.FC<Props> = ({ value }) => {
  const date = toDate(value, { preserveTime: true });
  const format = useFormatTime();

  return (
    <>
      <DateContent value={value} /> {format(date)}
    </>
  );
};

export default DateTimeContent;
