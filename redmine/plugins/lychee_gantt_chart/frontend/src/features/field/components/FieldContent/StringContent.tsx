import * as React from 'react';

export type Value = string;

type Props = {
  value: Value;
};

const StringContent: React.FC<Props> = ({ value }) => {
  return <>{value}</>;
};

export default StringContent;
