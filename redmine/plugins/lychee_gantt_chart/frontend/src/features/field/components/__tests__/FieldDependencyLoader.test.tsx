import * as React from 'react';
import { render } from '@testing-library/react';
import FieldDependencyLoader from '../FieldDependencyLoader';

const useWorkflowRulesCollectionMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/workflowRule', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useWorkflowRulesCollection: useWorkflowRulesCollectionMock,
}));

test('Hookを実行', () => {
  expect(useWorkflowRulesCollectionMock).not.toBeCalled();

  render(<FieldDependencyLoader />);

  expect(useWorkflowRulesCollectionMock).toBeCalled();
});
