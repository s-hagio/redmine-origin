import * as React from 'react';
import { render } from '@testing-library/react';
import TextContent from '../TextContent';

test('テキストをレンダリング', () => {
  const { container } = render(<TextContent value="lorem ipsum" />);

  expect(container.textContent).toBe('lorem ipsum');
});
