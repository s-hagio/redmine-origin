import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import ColorFieldInput from '../ColorFieldInput';

test('Render', () => {
  const { container } = render(<ColorFieldInput color="#21428b" />);

  expect(container).toMatchSnapshot();
});

test('カラーを選択した時にonSelectColorイベントが発火', () => {
  const onSelectColorSpy = vi.fn();

  const { container } = render(<ColorFieldInput color="#21428b" onSelectColor={onSelectColorSpy} />);
  const newColorElement = container.querySelector('[data-color="#ff887c"]') as HTMLLIElement;

  fireEvent.click(newColorElement);

  expect(onSelectColorSpy).toBeCalledWith('#ff887c');
});
