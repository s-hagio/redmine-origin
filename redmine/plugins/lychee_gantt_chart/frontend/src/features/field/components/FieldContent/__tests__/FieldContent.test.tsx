import * as React from 'react';
import { render } from '@testing-library/react';
import { FieldFormat } from '../../../types';
import FieldContent from '../FieldContent';

/* eslint-disable @typescript-eslint/no-explicit-any */
vi.mock('../HtmlContent', () => ({ default: (p: any) => `Html: ${p.value}` }));
vi.mock('../PlainTextContent', () => ({ default: (p: any) => `PlainText: ${p.value}` }));
vi.mock('../StringContent', () => ({ default: (p: any) => `String: ${p.value}` }));
vi.mock('../TextContent', () => ({ default: (p: any) => `Text: ${p.value}` }));
vi.mock('../IntContent', () => ({ default: (p: any) => `Int: ${p.value}` }));
vi.mock('../FloatContent', () => ({ default: (p: any) => `Float: ${p.value}` }));
vi.mock('../PercentContent', () => ({ default: (p: any) => `Percent: ${p.value}` }));
vi.mock('../DateContent', () => ({ default: (p: any) => `Date: ${p.value}` }));
vi.mock('../DateTimeContent', () => ({ default: (p: any) => `DateTime: ${p.value}` }));
vi.mock('../BoolContent', () => ({ default: (p: any) => `Bool: ${p.value}` }));
vi.mock('../ResourceIdContent', () => ({ default: (p: any) => `ResourceId: ${p.value}` }));
vi.mock('../RelationsContent', () => ({ default: (p: any) => `RelationsContent: ${p.value}` }));
vi.mock('../AttachmentsContent', () => ({ default: (p: any) => `AttachmentsContent: ${p.value}` }));
vi.mock('../ResourceIdContent', () => ({ default: (p: any) => `ResourceId: ${p.value}` }));
vi.mock('../ProjectNameContent', () => ({ default: (p: any) => `ProjectName: ${p.value}` }));
vi.mock('../VersionNameContent', () => ({ default: (p: any) => `VersionName: ${p.value}` }));
vi.mock('../AssociatedPrincipalNameContent', () => ({
  default: (p: any) => `AssociatedPrincipalNameContent: ${p.value}`,
}));
vi.mock('../AssociatedResourceNameContent', () => ({
  default: (p: any) => `AssociatedResourceName: ${p.type} ${p.value}`,
}));
vi.mock('../IssueStatusColorContent', () => ({ default: (p: any) => `IssueStatusColor: ${p.value}` }));
/* eslint-enable @typescript-eslint/no-explicit-any */

test('format, valueに応じたContentComponentをレンダリング', () => {
  const { container } = render(
    <>
      <FieldContent format={FieldFormat.Html} value="<p>Lorem ipsum</p>" />
      <FieldContent format={FieldFormat.PlainText} value="<p>Lorem ipsum</p><p>dolor sit</p>" />
      <FieldContent format={FieldFormat.FormattableText} value="<p>Lorem ipsum</p><p>dolor sit</p>" />
      <FieldContent format={FieldFormat.String} value="text" />
      <FieldContent format={FieldFormat.Text} value="long long text" />
      <FieldContent format={FieldFormat.Int} value={123} />
      <FieldContent format={FieldFormat.Float} value={1.23} />
      <FieldContent format={FieldFormat.Percent} value={12.34} />
      <FieldContent format={FieldFormat.Date} value="2022-03-01" />
      <FieldContent format={FieldFormat.DateTime} value="2022-03-01T05:43:00" />
      <FieldContent format={FieldFormat.Bool} value={false} />
      <FieldContent format={FieldFormat.Id} value={1} />
      <FieldContent format={FieldFormat.Relations} value="Relations value!" />
      <FieldContent format={FieldFormat.Attachments} value="Attachments value!" />
      <FieldContent format={FieldFormat.Project} value={2} />
      <FieldContent format={FieldFormat.Version} value={3} />
      <FieldContent format={FieldFormat.Tracker} value={4} />
      <FieldContent format={FieldFormat.Principal} value={5} />
      <FieldContent format={FieldFormat.AssignedTo} value={5} />
      <FieldContent format={FieldFormat.IssueStatus} value={6} />
      <FieldContent format={FieldFormat.IssuePriority} value={7} />
      <FieldContent format={FieldFormat.IssueCategory} value={8} />
      <FieldContent format={FieldFormat.IssueStatusWithColor} value={1} />
      <FieldContent format={FieldFormat.DoneRatio} value={56.78} />
    </>
  );

  expect(container).toMatchSnapshot();
});
