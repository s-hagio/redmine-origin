import * as React from 'react';
import { renderWithRecoil } from '@/test-utils';
import { InlineIssueRelation } from '@/models/issue';
import RelationsContent from '../RelationsContent';

const inlineIssueRelations: InlineIssueRelation[] = [
  { id: 1, label: '次のチケットに先行 (-26日)', otherIssue: { id: 123, subject: '123の題名' } },
  { id: 2, label: 'ブロック先', otherIssue: { id: 456, subject: '456のチケット！' } },
];

test('「関連するチケット」をレンダリング', () => {
  const { container } = renderWithRecoil(<RelationsContent value={inlineIssueRelations} />);

  expect(container).toMatchSnapshot();
});
