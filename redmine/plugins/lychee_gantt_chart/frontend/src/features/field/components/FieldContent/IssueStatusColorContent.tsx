import * as React from 'react';
import { css } from '@linaria/core';
import {
  AssociatedResourceType,
  AssociatedIssueStatus,
  useAssociatedResource,
} from '@/models/associatedResource';

export type Value = AssociatedIssueStatus['id'];

type Props = {
  value: Value;
};

const IssueStatusColorContent: React.FC<Props> = ({ value }) => {
  const status = useAssociatedResource(AssociatedResourceType.IssueStatus, value);

  if (!status) {
    return null;
  }

  return (
    <span className={style}>
      <span className={colorStyle} style={{ backgroundColor: status.color }} />
      {status.name}
    </span>
  );
};

export default IssueStatusColorContent;

const style = css`
  display: inline-flex;
  align-items: center;
  gap: 4px;
`;

const colorStyle = css`
  width: 12px;
  height: 12px;
  border: 1px solid #888;
`;
