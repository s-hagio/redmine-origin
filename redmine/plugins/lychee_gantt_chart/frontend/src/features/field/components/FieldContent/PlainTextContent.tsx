import * as React from 'react';

export type Value = string;

type Props = {
  value: Value;
};

const PlainTextContent: React.FC<Props> = ({ value }) => {
  const el = document.createElement('div');

  el.innerHTML = value;

  return <>{el.textContent}</>;
};

export default PlainTextContent;
