import * as React from 'react';
import { render } from '@testing-library/react';
import IntContent from '../IntContent';

test('整数をレンダリング', () => {
  const { container } = render(<IntContent value={123} />);

  expect(container.textContent).toBe('123');
});
