import * as React from 'react';
import { render } from '@testing-library/react';
import PlainTextContent from '../PlainTextContent';

test('HTML文字列をHTMLとしてレンダリング', () => {
  const { container } = render(<PlainTextContent value="<p><b>Lorem ipsum</b></p><p>dolor sit <^^></p>" />);

  expect(container.innerHTML).toBe('Lorem ipsumdolor sit <^^>');
});
