import * as React from 'react';
import { useWorkflowRulesCollection } from '@/models/workflowRule';

const FieldDependencyLoader: React.FC = () => {
  useWorkflowRulesCollection();

  return null;
};

export default React.memo(FieldDependencyLoader);
