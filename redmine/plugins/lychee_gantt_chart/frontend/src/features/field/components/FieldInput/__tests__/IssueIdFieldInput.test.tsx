import * as React from 'react';
import { render } from '@testing-library/react';
import IssueIdFieldInput from '../IssueIdFieldInput';

test('Render', () => {
  const { container } = render(<IssueIdFieldInput />);

  expect(container).toMatchSnapshot();
});

test.todo('入力補完でチケットリストを出す');
