import * as React from 'react';
import { render } from '@testing-library/react';
import { useFormatDate } from '@/models/dateFormat';
import DateContent from '../DateContent';

vi.mock('@/models/dateFormat', async () => ({
  ...(await vi.importActual<object>('@/models/dateFormat')),
  useFormatDate: vi.fn().mockReturnValue(vi.fn()),
}));

test('日付をフォーマットしてレンダリングする', () => {
  const dateString = '2022-01-01';
  const formattedString = 'formatted date string!';

  const formatDateMock = vi.mocked(useFormatDate()).mockReturnValue(formattedString);

  const { container } = render(<DateContent value={dateString} />);

  expect(formatDateMock).toBeCalledWith(dateString);
  expect(container.textContent).toBe(formattedString);
});
