import * as React from 'react';
import { ISODateString } from '@/lib/date';
import { useFormatDate } from '@/models/dateFormat';

export type Value = Date | ISODateString;

type Props = {
  value: Value;
};

const DateContent: React.FC<Props> = ({ value }) => {
  const format = useFormatDate();

  if (!value) {
    return null;
  }

  return <>{format(value)}</>;
};

export default DateContent;
