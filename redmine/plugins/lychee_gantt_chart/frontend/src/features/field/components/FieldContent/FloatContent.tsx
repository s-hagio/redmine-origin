import * as React from 'react';

export type Value = number;

type Props = {
  value: Value;
  fixed?: boolean;
};

const FloatContent: React.FC<Props> = ({ value, fixed }) => {
  const parts = (value + '').split('.');

  if (fixed) {
    parts[1] = ((parts[1] ?? '') + '00').slice(0, 2);
  }

  return parts.join('.');
};

export default FloatContent;
