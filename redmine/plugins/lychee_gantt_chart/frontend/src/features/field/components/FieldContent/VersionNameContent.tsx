import * as React from 'react';
import { VersionID } from '@/models/version';
import { AssociatedResourceType } from '@/models/associatedResource';
import AssociatedResourceNameContent from './AssociatedResourceNameContent';

export type Value = VersionID;

type Props = {
  value: Value;
};

const VersionNameContent: React.FC<Props> = ({ value }) => {
  return <AssociatedResourceNameContent type={AssociatedResourceType.Version} value={value} />;
};

export default VersionNameContent;
