import * as React from 'react';
import { css, cx } from '@linaria/core';

export type Props = Omit<React.ComponentProps<'input'>, 'type'>;

const IssueIdFieldInput = React.forwardRef<HTMLInputElement, Props>(function StringFieldInput(
  { className, ...props },
  ref
) {
  return (
    <div className={containerStyle}>
      <input ref={ref} type="text" className={cx(style, className)} {...props} />
    </div>
  );
});

export default React.memo(IssueIdFieldInput);

const containerStyle = css`
  position: relative;
`;

const style = css`
  width: 10em;
`;
