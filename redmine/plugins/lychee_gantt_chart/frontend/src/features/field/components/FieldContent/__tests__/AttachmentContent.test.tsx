import * as React from 'react';
import { render } from '@testing-library/react';
import { Attachment } from '@/models/attachment';
import AttachmentsContent from '../AttachmentsContent';

const attachments: Attachment[] = [
  { id: 1, filename: 'ファイル1！', url: 'https://example.com/attachments/1' },
  { id: 2, filename: 'ファイル2！', url: 'https://example.com/attachments/2' },
];

test('「ファイル」をレンダリング', () => {
  const { container } = render(<AttachmentsContent value={attachments} />);

  expect(container).toMatchSnapshot();
});
