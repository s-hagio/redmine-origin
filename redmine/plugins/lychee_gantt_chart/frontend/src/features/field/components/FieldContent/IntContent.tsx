import * as React from 'react';

export type Value = number;

type Props = {
  value: Value;
};

const IntContent: React.FC<Props> = ({ value }) => {
  return <>{value}</>;
};

export default IntContent;
