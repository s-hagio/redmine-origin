import * as React from 'react';

export type Value = number;

type Props = {
  value: Value;
};

const PercentContent: React.FC<Props> = ({ value }) => {
  return <>{value}%</>;
};

export default PercentContent;
