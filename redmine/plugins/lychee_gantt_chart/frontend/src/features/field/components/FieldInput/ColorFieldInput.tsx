import * as React from 'react';
import { css } from '@linaria/core';
import { lightBorderColor } from '@/styles';

type Color = string;

export type Props = {
  color: Color;
  required?: boolean;
  onSelectColor?: (color: Color) => void;
};

const COLORS = [
  '#ffffff',
  '#7bd148',
  '#21428b',
  '#5484ed',
  '#a4bdfc',
  '#46d6db',
  '#7ae7bf',
  '#51b749',
  '#D9CA00',
  '#fbd75b',
  '#ffb878',
  '#ff887c',
  '#dc2127',
  '#ff0099',
  '#dbadff',
  '#e1e1e1',
  '#7b7b7b',
  '#000000',
];

const ColorFieldInput: React.FC<Props> = ({ color: currentColor, onSelectColor }) => {
  const handleSelect = React.useCallback(
    (event: React.MouseEvent<HTMLSpanElement>) => {
      const color = event.currentTarget.getAttribute('data-color') + '';

      onSelectColor?.(color);
    },
    [onSelectColor]
  );

  return (
    <div className={style}>
      <ul>
        {COLORS.map((color) => (
          <li key={color} data-color={color} data-selected={currentColor === color} onClick={handleSelect}>
            <span style={{ backgroundColor: color }} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default React.memo(ColorFieldInput);

const style = css`
  position: relative;
  cursor: auto;

  ul {
    display: flex;
    gap: 7px;
    flex-wrap: wrap;
    list-style: none;
    margin: 0;
    padding: 0;
  }

  li {
    width: 25px;
    height: 25px;
    cursor: pointer;

    span {
      display: block;
      width: 100%;
      height: 100%;
    }
  }

  [data-color^='#fff'] {
    border: 1px solid ${lightBorderColor};
  }

  [data-selected='true'] {
    padding: 1px;
    border: 1px solid #49a2d7;
  }
`;
