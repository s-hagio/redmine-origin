import * as React from 'react';

export type Value = string | number;

type Props = {
  value: Value;
};

const ResourceIdContent: React.FC<Props> = ({ value }) => {
  if (!value) {
    return null;
  }

  return <>#{value}</>;
};

export default ResourceIdContent;
