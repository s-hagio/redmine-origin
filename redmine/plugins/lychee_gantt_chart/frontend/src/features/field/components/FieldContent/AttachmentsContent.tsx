import * as React from 'react';
import { css } from '@linaria/core';
import { Attachment } from '@/models/attachment';

export type Value = Attachment[];

type Props = {
  value: Value;
};

const AttachmentsContent: React.FC<Props> = ({ value: attachments }) => {
  return (
    <>
      {attachments.map((attachment) => (
        <span key={attachment.id} className={style}>
          <a href={attachment.url} target="_blank" rel="noreferrer">
            {attachment.filename}
          </a>
        </span>
      ))}
    </>
  );
};

export default AttachmentsContent;

const style = css`
  :not(:last-child)::after {
    content: ',';
    margin-right: 0.5rem;
  }
`;
