import * as React from 'react';
import { useRefreshSelectedBaseline } from '@/models/baseline';

const BaselineSnapshotLoader: React.FC = () => {
  const loadSelectedBaseline = useRefreshSelectedBaseline();

  React.useEffect(() => {
    loadSelectedBaseline();
  }, [loadSelectedBaseline]);

  return null;
};

export default React.memo(BaselineSnapshotLoader);
