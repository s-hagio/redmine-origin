import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { BaselineSummary, useFormatBaselineLabel } from '@/models/baseline';
import { useIsAllowedInMainProject } from '@/models/currentUser';
import { useDeleteBaseline, useUpdateBaseline } from '@/features/resourceController';
import { Icon, iconStyle } from '@/components/Icon';
import { NakedButton } from '@/components/Button';
import BaselineForm from '../BaselineForm';

type Props = {
  baseline: BaselineSummary;
};

const ItemContent: React.FC<Props> = ({ baseline }) => {
  const { t } = useTranslation();

  const isAllowedTo = useIsAllowedInMainProject();
  const updateBaseline = useUpdateBaseline();
  const deleteBaseline = useDeleteBaseline();
  const formatBaselineLabel = useFormatBaselineLabel();

  const [isEditing, setEditing] = React.useState<boolean>(false);

  const toggleEdit = React.useCallback(() => {
    setEditing((current) => !current);
  }, []);

  const label = formatBaselineLabel(baseline);

  const handleDeleteButtonClick = React.useCallback(() => {
    if (confirm(t('message.areYouSureYouWantToDelete', { target: label }))) {
      deleteBaseline(baseline.id);
    }
  }, [baseline.id, deleteBaseline, label, t]);

  return (
    <>
      {isEditing ? (
        <BaselineForm save={updateBaseline} baseline={baseline} onComplete={toggleEdit} />
      ) : (
        <div className={style}>
          <span className={labelStyle}>{label}</span>

          {isAllowedTo('manage_lychee_gantt_baselines') && (
            <div className={buttonListStyle} onClick={(e) => e.stopPropagation()}>
              <NakedButton onClick={toggleEdit}>
                <Icon name="edit" size={14} />
              </NakedButton>

              <NakedButton onClick={handleDeleteButtonClick}>
                <Icon name="delete" size={14} />
              </NakedButton>
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default ItemContent;

export const buttonListStyle = css`
  align-items: center;
  margin: 0 0.25rem 0 auto;
  list-style: none;

  button {
    --padding: 0.35rem;
    margin: calc(var(--padding) * -1) 0;
    padding: var(--padding);

    :hover {
      padding: calc(var(--padding) - 1px);
      border: 1px solid #ccc;
      background: #fff;
    }
  }
`;

const style = css`
  display: flex;
  overflow: hidden;
  align-items: center;
  width: 100%;
  padding: 0.25rem 0;

  .${buttonListStyle} {
    display: none;
  }
  :hover .${buttonListStyle} {
    display: flex;
  }
`;

const labelStyle = css`
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;

  .${iconStyle} {
    visibility: hidden;
  }
  :hover .${iconStyle} {
    visibility: visible;
  }
`;
