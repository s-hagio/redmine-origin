import * as React from 'react';
import { css } from '@linaria/core';
import { BaselineID, useBaselineSummaries, useSelectedBaselineIdState } from '@/models/baseline';
import { useCloseDialog } from '@/features/dialog';
import { BASELINE_SELECT_DIALOG_ID } from '../constants';
import Item from './Item';

const BaselineList: React.FC = () => {
  const closeDialog = useCloseDialog(BASELINE_SELECT_DIALOG_ID);

  const baselines = useBaselineSummaries();
  const [selectedId, setSelectedId] = useSelectedBaselineIdState();

  const handleSelect = React.useCallback(
    (id: BaselineID | null) => {
      setSelectedId(id);
      closeDialog();
    },
    [closeDialog, setSelectedId]
  );

  return (
    <ul className={style}>
      <Item selected={selectedId === null} onSelect={handleSelect} />

      {baselines.map((baseline) => (
        <Item
          key={baseline.id}
          baseline={baseline}
          selected={selectedId === baseline.id}
          onSelect={handleSelect}
        />
      ))}
    </ul>
  );
};

export default BaselineList;

const style = css`
  margin: 0;
  padding: 0.5rem 0;
  max-width: 350px;
  max-height: calc(100vh - 200px);
  overflow-y: scroll;
`;
