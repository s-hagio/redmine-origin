import * as React from 'react';
import { useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderWithRecoilHook } from '@/test-utils';
import { selectedBaselineIdState } from '@/models/baseline';
import { baselineSummary1, baselineSummary2 } from '@/__fixtures__';
import SelectedItem from '../SelectedItem';

const useBaselineSummariesMock = vi.hoisted(() => vi.fn());
const formatBaselineLabelMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/baseline', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useBaselineSummaries: useBaselineSummariesMock,
  useFormatBaselineLabel: vi.fn().mockReturnValue(formatBaselineLabelMock),
}));

beforeEach(() => {
  useBaselineSummariesMock.mockReturnValue([baselineSummary1, baselineSummary2]);
  formatBaselineLabelMock.mockReturnValue('Formatted Label!');
});

test('選択されたベースラインのラベルを表示する', () => {
  const { container, result } = renderWithRecoilHook(<SelectedItem />, () => ({
    setSelectedId: useSetRecoilState(selectedBaselineIdState),
  }));

  act(() => {
    result.current.setSelectedId(baselineSummary1.id);
  });

  expect(formatBaselineLabelMock).toBeCalledWith(baselineSummary1);
  expect(container).toMatchSnapshot('選択状態');

  act(() => {
    result.current.setSelectedId(null);
  });

  expect(container).toMatchSnapshot('未選択状態');
});

test.todo('クリックでメニューを表示する');
