import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { useForm } from 'react-hook-form';
import { BaselineParams, BaselineSummary } from '@/models/baseline';
import { StringFieldInput } from '@/features/field';
import { useCreateBaseline, useUpdateBaseline } from '@/features/resourceController';
import { NakedButton, SecondaryButton } from '@/components/Button';
import { Icon } from '@/components/Icon';
import { errorTextColor } from '@/styles';

type Props = {
  save: ReturnType<typeof useCreateBaseline> | ReturnType<typeof useUpdateBaseline>;
  baseline?: BaselineSummary;
  onComplete?: () => void;
};

type Fields = Pick<BaselineParams, 'name'>;

const BaselineForm: React.FC<Props> = ({ save, baseline, onComplete }) => {
  const { t } = useTranslation();
  const {
    register,
    handleSubmit,
    setFocus,
    reset,
    formState: { isSubmitting, errors },
  } = useForm<Fields>({
    defaultValues: {
      name: baseline?.name ?? '',
    },
  });

  const onSubmit = handleSubmit((data) => {
    const params: BaselineParams = { ...data };

    if (baseline) {
      params.id = baseline.id;
    }

    const promise = save(params);

    if (onComplete) {
      onComplete();
    } else {
      promise.then(() => reset());
    }

    return promise;
  });

  React.useEffect(() => {
    setFocus('name');
  }, [setFocus]);

  return (
    <form className={style} onSubmit={onSubmit} onClick={(e) => e.stopPropagation()}>
      <div className={formRowStyle}>
        <StringFieldInput
          {...register('name', {
            maxLength: { value: 255, message: t('error.maxLength', { field: t('field.name'), count: 255 }) },
          })}
          placeholder={t('label.optionalBaselineName')}
          disabled={isSubmitting}
        />
        <SecondaryButton type="submit" disabled={isSubmitting}>
          {t(baseline?.id ? 'label.save' : 'label.create')}
        </SecondaryButton>

        {baseline && (
          <NakedButton className={cancelButtonStyle} onClick={onComplete}>
            <Icon name="close" />
          </NakedButton>
        )}
      </div>

      {errors.name && <div className={errorStyle}>{errors.name.message}</div>}
    </form>
  );
};

export default BaselineForm;

const style = css`
  width: 100%;
  margin: -0.25rem 0;
  padding: 0.25rem 0;
`;

const formRowStyle = css`
  display: flex;
  align-items: center;
  gap: 0.15rem;
  width: 100%;
  padding-right: 0.15rem;

  input {
    width: 100%;
    margin-right: 0.05rem;
  }

  button:disabled {
    opacity: 0.5;
  }
`;

const cancelButtonStyle = css`
  --padding: 0.25rem;

  margin-left: -var(--padding);
  padding: var(--padding);
  font-size: 0.9em;

  :hover {
    padding: calc(var(--padding) - 1px);
    border: 1px solid #ccc;
    background: #fff;
  }
`;

const errorStyle = css`
  margin-top: 0.15rem;
  color: ${errorTextColor};
`;
