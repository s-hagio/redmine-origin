import * as React from 'react';
import { renderWithRecoil } from '@/test-utils';
import { baselineSummary1 } from '@/__fixtures__';
import ItemContent from '../ItemContent';

const isAllowedMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/currentUser', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIsAllowedInMainProject: vi.fn().mockReturnValue(isAllowedMock),
}));

const formatBaselineLabelMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/baseline', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useFormatBaselineLabel: vi.fn().mockReturnValue(formatBaselineLabelMock),
}));

beforeEach(() => {
  isAllowedMock.mockReturnValue(false);
  formatBaselineLabelMock.mockReturnValue('Formatted Label!');
});

test('フォーマットされたラベルを表示', () => {
  const { container } = renderWithRecoil(<ItemContent baseline={baselineSummary1} />);

  expect(formatBaselineLabelMock).toBeCalledWith(baselineSummary1);
  expect(container.textContent).toBe('Formatted Label!');
});

test('ベースラインの管理権限がある場合は各種操作ボタンを表示する', () => {
  // manage_lychee_gantt_baselines 権限を持っているとする
  isAllowedMock.mockImplementation((name) => name === 'manage_lychee_gantt_baselines');

  const { container } = renderWithRecoil(<ItemContent baseline={baselineSummary1} />);

  expect(container).toMatchSnapshot();
});

test('ベースラインの管理権限がなければボタンを表示しない', () => {
  isAllowedMock.mockReturnValue(false);

  const { container } = renderWithRecoil(<ItemContent baseline={baselineSummary1} />);

  expect(container).toMatchSnapshot();
});

test.todo('編集ボタンを押した時に編集モードに移行する');
test.todo('削除ボタンを押した時に削除確認ダイアログが表示される');
