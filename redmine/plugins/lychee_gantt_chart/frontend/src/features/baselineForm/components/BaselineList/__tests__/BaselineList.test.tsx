import * as React from 'react';
import { renderWithRecoil } from '@/test-utils';
import BaselineList from '../BaselineList';

const useBaselineSummariesMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/baseline', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useBaselineSummaries: useBaselineSummariesMock,
}));

vi.mock('../Item', () => ({
  default: vi.fn((props) => `Item: ${JSON.stringify(props)}`),
}));

beforeEach(() => {
  useBaselineSummariesMock.mockReturnValue([
    { id: 1, name: 'Baseline 1' },
    { id: 2, name: 'Baseline 2' },
    { id: 3, name: 'Baseline 3' },
  ]);
});

test('Render', () => {
  const { container } = renderWithRecoil(<BaselineList />);

  expect(container).toMatchSnapshot();
});

describe('項目をクリックした時', () => {
  test.todo('クリックした項目のIDが選択状態になる');
  test.todo('ダイアログが閉じる');
});
