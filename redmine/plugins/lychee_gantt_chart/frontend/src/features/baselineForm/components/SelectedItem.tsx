import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { useBaselineSummaries, useFormatBaselineLabel, useSelectedBaselineIdState } from '@/models/baseline';
import { useOpenDialog } from '@/features/dialog';
import { Icon } from '@/components/Icon';
import { BASELINE_SELECT_DIALOG_ID } from './constants';

const SelectedItem: React.FC = () => {
  const { t } = useTranslation();
  const openDialog = useOpenDialog(BASELINE_SELECT_DIALOG_ID);

  const formatBaselineLabel = useFormatBaselineLabel();
  const baselines = useBaselineSummaries();
  const [selectedId] = useSelectedBaselineIdState();

  const selectedBaseline = React.useMemo(
    () => baselines.find((summary) => summary.id === selectedId) || null,
    [baselines, selectedId]
  );

  return (
    <div className={style} onClick={openDialog}>
      <span className={labelStyle}>
        {selectedBaseline ? formatBaselineLabel(selectedBaseline) : t('label.noBaseline')}
      </span>
      <Icon name="chevronDown" />
    </div>
  );
};

export default SelectedItem;

const style = css`
  display: flex;
  align-items: center;
  max-width: 10rem;
  font-size: 0.95em;
  color: #555;
  cursor: pointer;
  word-break: keep-all;
`;

const labelStyle = css`
  overflow: hidden;
  text-overflow: ellipsis;
`;
