import * as React from 'react';
import { css } from '@linaria/core';
import { Dialog } from '@/features/dialog';
import SelectedItem from './SelectedItem';
import BaselineList from './BaselineList';
import NewBaselineForm from './NewBaselineForm';
import { BASELINE_SELECT_DIALOG_ID } from './constants';

const BaselineMenu: React.FC = () => {
  return (
    <div className={style}>
      <SelectedItem />

      <Dialog id={BASELINE_SELECT_DIALOG_ID} className={dialogStyle}>
        <NewBaselineForm />
        <BaselineList />
      </Dialog>
    </div>
  );
};

export default React.memo(BaselineMenu);

const style = css`
  display: flex;
  white-space: nowrap;
  align-items: center;
  margin-left: 2px;
`;

const dialogStyle = css`
  top: 5px;
  right: 5px;
  border-radius: 4px;
  background: #fff;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);
`;
