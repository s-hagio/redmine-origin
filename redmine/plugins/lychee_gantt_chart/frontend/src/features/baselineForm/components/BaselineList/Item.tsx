import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { BaselineSummary, BaselineID } from '@/models/baseline';
import { Icon } from '@/components/Icon';
import { lightBorderColor, primaryColor } from '@/styles';
import ItemContent, { buttonListStyle as contentButtonListStyle } from './ItemContent';

type Props = {
  baseline?: BaselineSummary;
  selected: boolean;
  onSelect: (id: BaselineID | null) => void;
};

const Item: React.FC<Props> = ({ baseline, selected, onSelect }) => {
  const { t } = useTranslation();

  const handleSelectButtonClick = React.useCallback(
    (event: React.MouseEvent<HTMLLIElement>) => {
      if (event.nativeEvent.composedPath().some((el) => (el as HTMLElement).tagName === 'BUTTON')) {
        return;
      }

      onSelect(baseline?.id ?? null);
    },
    [baseline, onSelect]
  );

  return (
    <li className={style} data-selected={selected} onClick={handleSelectButtonClick}>
      <span className={selectedIconStyle}>
        <Icon name="check" />
      </span>

      {baseline ? <ItemContent baseline={baseline} /> : t('label.noBaseline')}
    </li>
  );
};

export default Item;

const selectedIconStyle = css`
  display: flex;
  align-items: center;
  margin: 0 0.5rem;
  visibility: hidden;
  color: ${primaryColor};
`;

const style = css`
  display: flex;
  align-items: center;
  padding: 0.5rem 0;
  border-bottom: 1px solid ${lightBorderColor};
  cursor: default;

  &[data-selected='true'] {
    .${selectedIconStyle} {
      visibility: visible;
    }
  }

  .${contentButtonListStyle} {
    visibility: hidden;
  }

  :hover {
    background: #f5f5f5;

    .${contentButtonListStyle} {
      visibility: visible;
    }
  }
`;
