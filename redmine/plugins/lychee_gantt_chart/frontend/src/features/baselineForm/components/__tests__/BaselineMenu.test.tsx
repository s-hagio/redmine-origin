import * as React from 'react';
import { render } from '@testing-library/react';
import BaselineMenu from '../BaselineMenu';

vi.mock('@/features/dialog', () => ({
  Dialog: vi.fn(({ children, ...props }) => React.createElement('dialog-mock', props, children)),
}));

vi.mock('../SelectedItem', () => ({
  default: vi.fn((props) => `SelectedItem: ${JSON.stringify(props)}`),
}));

vi.mock('../NewBaselineForm', () => ({
  default: vi.fn((props) => `NewBaselineForm: ${JSON.stringify(props)}`),
}));

vi.mock('../BaselineList', () => ({
  default: vi.fn((props) => `BaselineList: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<BaselineMenu />);

  expect(container).toMatchSnapshot();
});
