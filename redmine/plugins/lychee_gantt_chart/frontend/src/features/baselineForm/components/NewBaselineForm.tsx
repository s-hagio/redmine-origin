import * as React from 'react';
import { css } from '@linaria/core';
import { useIsAllowedInMainProject } from '@/models/currentUser';
import { useCreateBaseline } from '@/features/resourceController';
import { borderColor } from '@/styles';
import BaselineForm from './BaselineForm';

const NewBaselineForm: React.FC = () => {
  const isAllowedTo = useIsAllowedInMainProject();
  const createBaseline = useCreateBaseline();

  if (!isAllowedTo('manage_lychee_gantt_baselines')) {
    return null;
  }

  return (
    <div className={style}>
      <BaselineForm save={createBaseline} />
    </div>
  );
};

export default React.memo(NewBaselineForm);

const style = css`
  display: flex;
  align-items: center;
  gap: 0.5rem;
  padding: 0.5rem;
  border-bottom: 1px solid ${borderColor};

  input[type='text'] {
    width: 100%;
  }
`;
