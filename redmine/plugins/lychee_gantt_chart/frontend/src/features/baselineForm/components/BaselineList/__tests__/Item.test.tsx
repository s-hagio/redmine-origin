import * as React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithRecoil } from '@/test-utils';
import { baselineSummary1 } from '@/__fixtures__';
import Item from '../Item';

const isAllowedMock = vi.hoisted(() => vi.fn().mockReturnValue(true));

vi.mock('@/models/currentUser', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIsAllowedInMainProject: vi.fn().mockReturnValue(isAllowedMock),
}));

const ItemContentSpy = vi.hoisted(() => vi.fn());

vi.mock('../ItemContent', () => ({
  default: vi.fn((props) => {
    ItemContentSpy(props);
    return React.createElement('item-content');
  }),
}));

test('Render', () => {
  const { container, rerender } = renderWithRecoil(<Item selected={false} onSelect={vi.fn()} />);

  expect(container).toMatchSnapshot('ベースラインなし');
  expect(ItemContentSpy).not.toBeCalled();

  rerender(<Item baseline={baselineSummary1} selected={false} onSelect={vi.fn()} />);

  expect(container).toMatchSnapshot('ベースラインあり');
  expect(ItemContentSpy).toBeCalledWith({ baseline: baselineSummary1 });
});

test('クリックでonSelectが呼ばれる', () => {
  const onSelect = vi.fn();
  const { container, rerender } = renderWithRecoil(
    <Item baseline={baselineSummary1} selected={false} onSelect={onSelect} />
  );

  fireEvent.click(container.firstChild as HTMLElement);
  expect(onSelect).toBeCalledWith(baselineSummary1.id);

  rerender(<Item selected={false} onSelect={onSelect} />);

  fireEvent.click(container.firstChild as HTMLElement);
  expect(onSelect).toBeCalledWith(null);
});
