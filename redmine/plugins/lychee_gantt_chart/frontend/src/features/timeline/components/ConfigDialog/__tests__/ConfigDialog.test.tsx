import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import ConfigDialog from '../ConfigDialog';
import { MORE_CONFIG_DIALOG_ID } from '../constants';

const closeDialogMock = vi.hoisted(() => vi.fn());
const useCloseDialogMock = vi.hoisted(() => vi.fn().mockReturnValue(closeDialogMock));

vi.mock('@/features/dialog', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  Dialog: vi.fn((props) => React.createElement('custom-dialog', props)),
  useCloseDialog: useCloseDialogMock,
}));

vi.mock('../DisplayOptionList', () => ({
  default: vi.fn((props) => `DisplayOptionList: ${JSON.stringify(props)}`),
}));

vi.mock('../MetadataOptionList', () => ({
  default: vi.fn((props) => `MetadataOptionList: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<ConfigDialog />);

  expect(container).toMatchSnapshot();
});

test('クローズボタンのクリックでダイアログを閉じる', () => {
  const { getByRole } = render(<ConfigDialog />);

  expect(useCloseDialogMock).toBeCalledWith(MORE_CONFIG_DIALOG_ID);
  expect(closeDialogMock).not.toBeCalled();

  fireEvent.click(getByRole('button'));

  expect(closeDialogMock).toBeCalled();
});
