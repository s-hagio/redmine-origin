import * as React from 'react';
import { css } from '@linaria/core';
import { borderColor, ganttHeaderRowHeight } from '@/styles';
import { useTotalTickWidth } from '../../models/tick';
import { useGridRows } from '../../models/grid';
import { useScrollLeft } from '../../models/scroll';
import ConfigMenu, { style as configMenuStyle } from '../ConfigMenu';
import { SelectedTimeRange, selectedTimeRangeStyle } from '../SelectedTimeRange';
import GridRow, { style as gridRowStyle } from './GridRow';

const TimelineHeader: React.FC = () => {
  const totalWidth = useTotalTickWidth();
  const scrollLeft = useScrollLeft();
  const gridRows = useGridRows();

  const elementRef = React.useRef<HTMLDivElement>(null);
  const [width, setWidth] = React.useState<number>(0);

  React.useEffect(() => {
    if (!elementRef.current) {
      return;
    }

    const resizeObserver = new ResizeObserver(([entry]) => setWidth(entry.contentRect.width));
    resizeObserver.observe(elementRef.current);

    return () => {
      resizeObserver.disconnect();
    };
  }, []);

  return (
    <div ref={elementRef} className={style}>
      <div className={gridContainerStyle}>
        <div className={gridStyle} style={{ width: totalWidth, marginLeft: -scrollLeft }}>
          {gridRows.slice(0, 2).map((row) => (
            <GridRow key={row.type} row={row} />
          ))}

          <SelectedTimeRange containerWidth={width} scrollLeft={scrollLeft} />
        </div>
      </div>

      <ConfigMenu />
    </div>
  );
};

export default React.memo(TimelineHeader);

export const style = css`
  position: relative;
  height: fit-content;
  border-left: 1px solid ${borderColor};
  clip-path: inset(0 0 -100vh); // DataTableにConfigMenuがめり込む問題対応

  .${selectedTimeRangeStyle} {
    bottom: 0;
    height: ${ganttHeaderRowHeight + 2}px;
    z-index: 1;
  }

  .${configMenuStyle} {
    position: absolute;
    top: calc(100% + 5px);
    right: 5px;
  }
`;

const gridContainerStyle = css`
  overflow: hidden;
`;

const gridStyle = css`
  position: relative;
  font-size: 0.9em;

  .${gridRowStyle} {
    border-bottom: 1px solid ${borderColor};
  }
`;
