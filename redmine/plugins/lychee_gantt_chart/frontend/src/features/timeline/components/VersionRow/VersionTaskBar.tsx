import * as React from 'react';
import { Version } from '@/models/version';
import { VersionResourceIdentifier } from '@/models/coreResource';
import { extractVersionBarParams } from '../../models/taskBar';
import TaskBar from '../TaskBar';

type Props = {
  identifier: VersionResourceIdentifier;
  version: Version;
};

const VersionTaskBar: React.FC<Props> = ({ identifier, version }) => {
  const barParams = extractVersionBarParams(version);

  return <TaskBar identifier={identifier} barParams={barParams} showRangeMarker />;
};

export default React.memo(VersionTaskBar);
