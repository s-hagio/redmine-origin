import * as React from 'react';
import { useProject } from '@/models/project';
import { ProjectRow as ProjectRowObject } from '@/features/row';
import { RowProps } from '../types';
import Row from '../Row';
import ProjectRowContent from './ProjectRowContent';

type Props = RowProps<ProjectRowObject>;

const ProjectRow: React.FC<Props> = ({ id, resourceId }) => {
  const project = useProject(resourceId);

  if (!project) {
    return null;
  }

  return (
    <Row id={id}>
      <ProjectRowContent id={id} project={project} />
    </Row>
  );
};

export default React.memo(ProjectRow);
