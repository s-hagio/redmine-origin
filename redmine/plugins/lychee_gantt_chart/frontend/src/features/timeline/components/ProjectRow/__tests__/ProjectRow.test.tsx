import * as React from 'react';
import { render } from '@testing-library/react';
import { mainProject, mainProjectRow } from '@/__fixtures__';
import ProjectRow from '../ProjectRow';

const useProjectMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/project', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useProject: useProjectMock,
}));

vi.mock('../../Row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn((props) => React.createElement('row-mock', props)),
}));

const ProjectRowContentSpy = vi.hoisted(() => vi.fn());

vi.mock('../ProjectRowContent', () => ({
  default: vi.fn((props) => {
    ProjectRowContentSpy(props);
    return 'ProjectRowContent';
  }),
}));

const project = mainProject;
const row = mainProjectRow;

beforeEach(() => {
  useProjectMock.mockReturnValue(project);
});

test('Render', () => {
  const { container } = render(<ProjectRow id={row.id} resourceId={row.resourceId} />);

  expect(container).toMatchSnapshot();
  expect(ProjectRowContentSpy).toBeCalledWith({ id: row.id, project });
});
