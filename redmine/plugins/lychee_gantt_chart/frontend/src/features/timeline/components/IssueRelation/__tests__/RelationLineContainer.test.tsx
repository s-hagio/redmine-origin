import * as React from 'react';
import { render } from '@testing-library/react';
import RelationLineContainer from '../RelationLineContainer';

vi.mock('react', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  Suspense: vi.fn(({ children }) => React.createElement('suspense-mock', null, children)),
}));

vi.mock('../RelationStateRefresher', () => ({
  default: vi.fn((props) => `RelationStateRefresher: ${JSON.stringify(props)}`),
}));

vi.mock('../IssueRelationCreationCanceller', () => ({
  default: vi.fn((props) => `IssueRelationCreationCanceller: ${JSON.stringify(props)}`),
}));

vi.mock('../Line', () => ({
  IssueRelationLineContainer: vi.fn((props) => `IssueRelationLineContainer: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<RelationLineContainer width={3000} />);

  expect(container).toMatchSnapshot();
});
