import * as React from 'react';
import { IssueID } from '@/models/issue';
import { useIsIssueRelationManageable } from '@/models/issueRelation';
import { useIssueRelationCreationStatus } from '../../../models/issueRelation';
import IssueRelationCreationActivateButton from './IssueRelationCreateButton';
import IssueRelationCreationTarget from './IssueRelationCreationTarget';

type Props = {
  issueId: IssueID;
};

const IssueTaskBarContent: React.FC<Props> = ({ issueId }) => {
  const isManageable = useIsIssueRelationManageable(issueId);
  const status = useIssueRelationCreationStatus(issueId);

  if (!isManageable) {
    return null;
  }

  return (
    <>
      {!status.isCreating && (
        <>
          <IssueRelationCreationActivateButton issueId={issueId} type="to" />
          <IssueRelationCreationActivateButton issueId={issueId} type="from" />
        </>
      )}

      {status.isRelatable && <IssueRelationCreationTarget issueId={issueId} />}
    </>
  );
};

export default React.memo(IssueTaskBarContent);
