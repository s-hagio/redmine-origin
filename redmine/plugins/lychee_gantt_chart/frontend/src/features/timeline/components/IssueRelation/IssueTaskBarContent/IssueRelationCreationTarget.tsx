import * as React from 'react';
import { css } from '@linaria/core';
import { IssueID } from '@/models/issue';
import {
  useSetIssueRelationCreationTarget,
  useCommitDraftIssueRelation,
  useIssueRelationCreation,
} from '../../../models/issueRelation';
import RelationTypeSelector, { style as typeSelectStyle } from '../RelationTypeSelector';

type Props = {
  issueId: IssueID;
};

const IssueRelationCreationTarget: React.FC<Props> = ({ issueId }) => {
  const [isTypeSelecting, setIsTypeSelecting] = React.useState<boolean>(false);

  const { targetIssueId } = useIssueRelationCreation() || {};
  const setIssueRelationTargetIssue = useSetIssueRelationCreationTarget();
  const commit = useCommitDraftIssueRelation();

  const handleMouseEnter = React.useCallback(() => {
    setIssueRelationTargetIssue(issueId);
  }, [issueId, setIssueRelationTargetIssue]);

  const handleMouseLeave = React.useCallback(() => {
    setIssueRelationTargetIssue(null);
  }, [setIssueRelationTargetIssue]);

  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLDivElement>) => {
      event.preventDefault();

      if (!isTypeSelecting && targetIssueId === issueId) {
        commit();
      }
    },
    [commit, isTypeSelecting, targetIssueId, issueId]
  );

  const handleContextMenu = React.useCallback(
    (event: React.MouseEvent<HTMLDivElement>) => {
      event.preventDefault();

      setIssueRelationTargetIssue(issueId, { lock: true });
      setIsTypeSelecting(true);
    },
    [issueId, setIssueRelationTargetIssue]
  );

  const handleCloseButtonClick = React.useCallback(
    (event: React.MouseEvent<HTMLDivElement>) => {
      event.preventDefault();
      event.stopPropagation();

      setIsTypeSelecting(false);
      setIssueRelationTargetIssue(null, { lock: false, force: true });
    },
    [setIssueRelationTargetIssue]
  );

  return (
    <div
      className={style}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onClick={handleClick}
      onContextMenu={handleContextMenu}
    >
      {isTypeSelecting && <RelationTypeSelector onCloseButtonClick={handleCloseButtonClick} />}
    </div>
  );
};

export default React.memo(IssueRelationCreationTarget);

export const style = css`
  width: 100%;
  height: 100%;
  z-index: 11; // TODO: タイムライン内でのz-index管理

  .${typeSelectStyle} {
    position: absolute;
    top: -2px;
    left: 50%;
    transform: translate(-50%, -100%);
  }
`;
