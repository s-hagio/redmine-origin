import * as React from 'react';
import { Project } from '@/models/project';
import { ProjectResourceIdentifier } from '@/models/coreResource';
import { extractProjectBarParams } from '../../models/taskBar';
import TaskBar from '../TaskBar';

type Props = {
  identifier: ProjectResourceIdentifier;
  project: Project;
};

const ProjectTaskBar: React.FC<Props> = ({ identifier, project }) => {
  const barParams = extractProjectBarParams(project);

  return <TaskBar identifier={identifier} barParams={barParams} showRangeMarker />;
};

export default React.memo(ProjectTaskBar);
