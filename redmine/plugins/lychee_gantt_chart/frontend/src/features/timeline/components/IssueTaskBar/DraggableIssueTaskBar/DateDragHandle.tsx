import * as React from 'react';
import { css } from '@linaria/core';
import { Icon } from '@/components/Icon';

type Props = {
  type: 'start' | 'end';
  hidden?: boolean;
};

export default React.forwardRef<HTMLDivElement, Props>(function DateDragHandle({ type, hidden }, ref) {
  const cssProps: React.CSSProperties = React.useMemo(() => {
    return hidden ? { visibility: 'hidden' } : {};
  }, [hidden]);

  return (
    <div ref={ref} className={style} style={cssProps} data-handle-type={type}>
      <Icon name="dragIndicator" size={12} />
    </div>
  );
});

const radius = 2;
export const width = 12;

export const style = css`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${width}px;
  height: inherit;
  border: 1px solid #ccc;
  background: #fff;
  cursor: ew-resize;

  &[data-handle-type='start'] {
    border-radius: ${radius}px 0 0 ${radius}px;
  }

  &[data-handle-type='end'] {
    border-radius: 0 ${radius}px ${radius}px 0;
  }
`;
