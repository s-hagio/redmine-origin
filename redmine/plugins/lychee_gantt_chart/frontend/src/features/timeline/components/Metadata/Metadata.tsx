import * as React from 'react';
import { css } from '@linaria/core';
import { MetadataType, useMetadataVisible } from '../../models/metadata';

type Props = React.PropsWithChildren<{
  type: MetadataType;
}>;

const Metadata: React.FC<Props> = ({ type, children }) => {
  const isVisible = useMetadataVisible(type);

  if (!isVisible) {
    return null;
  }

  return (
    <div className={style} data-meta={type}>
      {children}
    </div>
  );
};

export default React.memo(Metadata);

export const style = css`
  display: flex;
  align-items: center;
  width: fit-content;
  white-space: nowrap;
`;
