import * as React from 'react';
import { render } from '@testing-library/react';
import IssueTaskBarContent from '../IssueTaskBarContent';
import { IssueRelationCreationStatus } from '../../../../models/issueRelation';

const useIsIssueRelationManageableMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/issueRelation', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIsIssueRelationManageable: useIsIssueRelationManageableMock,
}));

const useIssueRelationCreationStatusMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/issueRelation', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIssueRelationCreationStatus: useIssueRelationCreationStatusMock,
}));

vi.mock('../IssueRelationCreateButton', () => ({
  default: vi.fn((props) => `IssueRelationCreateButton: ${JSON.stringify(props)}`),
}));

vi.mock('../IssueRelationCreationTarget', () => ({
  default: vi.fn((props) => `IssueRelationCreationTarget: ${JSON.stringify(props)}`),
}));

const defaultStatus: IssueRelationCreationStatus = {
  isCreating: false,
  isSourceIssue: false,
  isTargetIssue: false,
  isUnrelated: false,
  isRelatable: false,
};

beforeEach(() => {
  useIsIssueRelationManageableMock.mockReturnValue(true);
  useIssueRelationCreationStatusMock.mockReturnValue(defaultStatus);
});

test('Render', () => {
  const { container } = render(<IssueTaskBarContent issueId={1} />);

  expect(container).toMatchSnapshot();
});

test('Render: { isRelatable: true }', () => {
  useIssueRelationCreationStatusMock.mockReturnValue({ ...defaultStatus, isRelatable: true });

  const { container } = render(<IssueTaskBarContent issueId={1} />);

  expect(container).toMatchSnapshot();
});

test('Render: { isCreating: true }', () => {
  useIssueRelationCreationStatusMock.mockReturnValue({ ...defaultStatus, isCreating: true });

  const { container } = render(<IssueTaskBarContent issueId={1} />);

  expect(container).toMatchSnapshot();
});

describe('when not issueRelation manageable', () => {
  beforeEach(() => {
    useIsIssueRelationManageableMock.mockReturnValue(false);
  });

  test('関連付け機能を無効化（何も表示しない）', () => {
    const { container } = render(<IssueTaskBarContent issueId={1} />);

    expect(container.innerHTML).toBe('');
  });
});
