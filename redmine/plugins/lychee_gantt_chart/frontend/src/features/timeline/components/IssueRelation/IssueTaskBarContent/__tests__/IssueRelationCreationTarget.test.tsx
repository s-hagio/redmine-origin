import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import IssueRelationCreationTarget from '../IssueRelationCreationTarget';

const useIssueRelationCreationMock = vi.hoisted(() => vi.fn());
const setIssueRelationCreationTargetMock = vi.hoisted(() => vi.fn());
const commitDraftIssueRelationMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/issueRelation', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIssueRelationCreation: useIssueRelationCreationMock,
  useSetIssueRelationCreationTarget: vi.fn().mockReturnValue(setIssueRelationCreationTargetMock),
  useCommitDraftIssueRelation: vi.fn().mockReturnValue(commitDraftIssueRelationMock),
}));

const RelationTypeSelectorSpy = vi.hoisted(() => vi.fn());

vi.mock('../../RelationTypeSelector', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn((props) => {
    RelationTypeSelectorSpy(props);
    return `RelationTypeSelector: ${JSON.stringify(props)}`;
  }),
}));

const issueId = 123;

beforeEach(() => {
  useIssueRelationCreationMock.mockReturnValue(null);
});

test('Render', () => {
  const { container } = render(<IssueRelationCreationTarget issueId={issueId} />);

  expect(container).toMatchSnapshot();
});

test('カーソルが乗ったらtargetIssueとして設定、カーソルが離れたら解除', () => {
  const { container } = render(<IssueRelationCreationTarget issueId={issueId} />);
  const el = container.firstChild as HTMLElement;

  expect(setIssueRelationCreationTargetMock).not.toBeCalled();

  fireEvent.mouseEnter(el);

  expect(setIssueRelationCreationTargetMock).lastCalledWith(issueId);

  fireEvent.mouseLeave(el);

  expect(setIssueRelationCreationTargetMock).lastCalledWith(null);
});

test('クリックで確定', () => {
  useIssueRelationCreationMock.mockReturnValue({ targetIssueId: issueId });

  const { container } = render(<IssueRelationCreationTarget issueId={issueId} />);
  const el = container.firstChild as HTMLElement;

  expect(commitDraftIssueRelationMock).not.toBeCalled();

  fireEvent.click(el);

  expect(commitDraftIssueRelationMock).toBeCalled();
});

test('コンテキストメニューで関連の種類選択ダイアログを表示', () => {
  const { container } = render(<IssueRelationCreationTarget issueId={issueId} />);
  const el = container.firstChild as HTMLElement;

  fireEvent.contextMenu(el);

  expect(setIssueRelationCreationTargetMock).lastCalledWith(issueId, { lock: true });
  expect(container).toMatchSnapshot();

  const closeButton = document.createElement('div');
  closeButton.onclick = RelationTypeSelectorSpy.mock.lastCall[0].onCloseButtonClick;

  fireEvent.click(closeButton);

  expect(setIssueRelationCreationTargetMock).lastCalledWith(null, { lock: false, force: true });
  expect(container).toMatchSnapshot();
});

test('種類選択ダイアログ表示中はクリックで確定しない', () => {
  const { container } = render(<IssueRelationCreationTarget issueId={issueId} />);
  const el = container.firstChild as HTMLElement;

  fireEvent.contextMenu(el);
  fireEvent.click(el);

  expect(commitDraftIssueRelationMock).not.toBeCalled();
});

test('他のチケットで種類選択ダイアログが開かれている場合はクリックで確定しない', () => {
  useIssueRelationCreationMock.mockReturnValue({ targetIssueId: issueId + 1 });

  const { container } = render(<IssueRelationCreationTarget issueId={issueId} />);
  const el = container.firstChild as HTMLElement;

  fireEvent.click(el);

  expect(commitDraftIssueRelationMock).not.toBeCalled();
});
