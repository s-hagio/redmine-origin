import * as React from 'react';
import { useEffect } from 'react';
import { useRowSelectOptions, useSelectRowByOffset, useSkipDeselectRow } from '@/features/row';

type Props = {
  containerRef: React.RefObject<HTMLElement | null>;
};

const RowSelector: React.FC<Props> = ({ containerRef }) => {
  const rowSelectOptions = useRowSelectOptions();
  const selectRowByOffset = useSelectRowByOffset();
  const skipDeselectRow = useSkipDeselectRow();

  useEffect(() => {
    const el = containerRef.current;

    if (!el) {
      return;
    }

    const handleMouseDown = (event: MouseEvent) => {
      event.shiftKey && event.preventDefault();

      if (rowSelectOptions.multiple || rowSelectOptions.range || isSelectorTargetElement(event.target)) {
        selectRowByOffset(event.clientY - el.getBoundingClientRect().top);
      } else {
        skipDeselectRow();
      }
    };

    el.addEventListener('mousedown', handleMouseDown);

    return () => {
      el.removeEventListener('mousedown', handleMouseDown);
    };
  }, [containerRef, rowSelectOptions, selectRowByOffset, skipDeselectRow]);

  return null;
};

export default React.memo(RowSelector);

const isSelectorTargetElement = (el: unknown): boolean => {
  return !!el && (el as HTMLElement).tagName.toLowerCase() === 'svg'; // IssueRelationContainer
};
