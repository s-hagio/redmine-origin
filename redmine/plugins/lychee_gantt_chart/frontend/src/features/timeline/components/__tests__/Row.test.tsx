import * as React from 'react';
import { render } from '@testing-library/react';
import { rootIssueRow } from '@/__fixtures__';
import Row from '../Row';

vi.mock('@/features/row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  RowComponent: vi.fn(({ children, ...props }) => {
    return React.createElement('row-component', props, children);
  }),
}));

test('Render', () => {
  const result = render(
    <Row id={rootIssueRow.id} className="fooClass">
      CHILD!!
    </Row>
  );

  expect(result.container).toMatchSnapshot();
});
