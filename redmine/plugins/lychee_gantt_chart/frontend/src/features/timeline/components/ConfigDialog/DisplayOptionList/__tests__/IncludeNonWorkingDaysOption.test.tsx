import * as React from 'react';
import { atom, useRecoilState, useRecoilValue } from 'recoil';
import { beforeEach } from 'vitest';
import { fireEvent } from '@testing-library/react';
import { renderWithRecoilHook, renderWithTranslation } from '@/test-utils';
import IncludeNonWorkingDaysOption from '../IncludeNonWorkingDaysOption';

const setTimeAxisOptionsMock = vi.hoisted(() => vi.fn());
const useTimeAxisOptionsStateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/timeAxis', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useTimeAxisOptionsState: useTimeAxisOptionsStateMock,
}));

test('Render', () => {
  useTimeAxisOptionsStateMock.mockReturnValue([{ includeNonWorkingDays: true }, setTimeAxisOptionsMock]);

  const { container } = renderWithTranslation(<IncludeNonWorkingDaysOption />);

  expect(container).toMatchSnapshot();
});

describe('change', () => {
  const timeAxisOptionsState = atom({
    key: 'timeAxisOptionsState',
    default: { otherKey: 'otherValue', includeNonWorkingDays: true },
  });

  beforeEach(() => {
    useTimeAxisOptionsStateMock.mockImplementation(() => useRecoilState(timeAxisOptionsState));
  });

  test('checkboxのチェック状態に応じてトグル', () => {
    const { getByRole, result } = renderWithRecoilHook(<IncludeNonWorkingDaysOption />, () =>
      useRecoilValue(timeAxisOptionsState)
    );
    const checkbox = getByRole('checkbox');

    fireEvent.click(checkbox);
    expect(result.current).toEqual({ otherKey: 'otherValue', includeNonWorkingDays: false });

    fireEvent.click(checkbox);
    expect(result.current).toEqual({ otherKey: 'otherValue', includeNonWorkingDays: true });
  });
});
