import * as React from 'react';
import { css, cx } from '@linaria/core';
import { pick } from '@/utils/object';
import { BaselineBar as BaselineBarObject } from '../../../models/baseline';

type Props = React.PropsWithChildren<{
  bar: BaselineBarObject;
}>;

const BaselineBar: React.FC<Props> = ({ bar, children }) => {
  if (!bar.isVisible) {
    return null;
  }

  return (
    <div className={style} style={pick(bar, ['top', 'left', 'width', 'height'])}>
      <div className={cx(barStyle, todoBarStyle)} />
      {!!bar.doneWidth && <div className={cx(barStyle, doneBarStyle)} style={{ width: bar.doneWidth }} />}

      {children}
    </div>
  );
};

export default React.memo(BaselineBar);

const style = css`
  position: absolute;
`;

const barStyle = css`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  border: 1px solid;
  border-radius: 4px;
  background-color: #fff;
  background-size: 2px 2px;
  background-position:
    0 0,
    1px 1px;
`;

const todoBarStyle = css`
  width: 100%;
  border-color: #00b3ee;
  background: #d1e3eb;
  z-index: 1;
`;

export const doneBarStyle = css`
  border-color: #00b3ee;
  background: #7ed8eb;
  z-index: 2;
`;
