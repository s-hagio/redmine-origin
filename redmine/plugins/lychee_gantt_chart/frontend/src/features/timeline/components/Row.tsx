import * as React from 'react';
import { css, cx } from '@linaria/core';
import { RowComponent, RowID } from '@/features/row';

type Props = React.PropsWithChildren<{
  id: RowID;
  className?: string | undefined;
}>;

const Row: React.FC<Props> = ({ id, className, children }) => {
  return (
    <RowComponent className={cx(style, className)} rowId={id}>
      {children}
    </RowComponent>
  );
};

export default Row;

const style = css`
  position: absolute;
  width: 100%;
`;
