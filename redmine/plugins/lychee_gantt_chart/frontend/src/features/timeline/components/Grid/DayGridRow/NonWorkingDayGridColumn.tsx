import * as React from 'react';
import { css } from '@linaria/core';
import { nonWorkingDayColor } from './colors';

type Props = {
  width: number;
  index: number;
};

const NonWorkingDayGridColumn: React.FC<Props> = ({ width, index }) => {
  return <rect className={style} x={index * width} y="0" width={width} height="100%" />;
};

export default React.memo(NonWorkingDayGridColumn);

const style = css`
  fill: ${nonWorkingDayColor};
`;
