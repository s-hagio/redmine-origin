import * as React from 'react';
import { formatISODate } from '@/lib/date';
import { useSimpleDrag } from '@/lib/hooks';
import { Issue } from '@/models/issue';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { TimeBarRange, useSelectTimeRange } from '../../../models/timeBar';
import {
  extractIssueBarParams,
  useComputeMovedRange,
  useIssueTaskBarDraggability,
  MovedRangeValues,
  TaskBarParams,
  useBulkUpdateTaskBars,
  useCommitTaskBarChanges,
} from '../../../models/taskBar';
import IssueTaskBar from '../IssueTaskBar';
import RangeDragHandle from './RangeDragHandle';
import DateDragHandle from './DateDragHandle';

type Props = {
  identifier: IssueResourceIdentifier;
  issue: Issue;
  initialParams: TaskBarParams;
};

const DraggableIssueTaskBar: React.FC<Props> = ({ identifier, issue, initialParams }) => {
  const baseBarRangeRef = React.useRef<TimeBarRange>(initialParams);
  const [barParams, setBarParams] = React.useState<TaskBarParams>(initialParams);

  const draggability = useIssueTaskBarDraggability(issue);
  const computeMovedRange = useComputeMovedRange();
  const selectTimeRange = useSelectTimeRange();

  const bulkUpdateTaskBars = useBulkUpdateTaskBars(identifier);
  const commitTaskBarChanges = useCommitTaskBarChanges(identifier);

  const updateBarParams = React.useCallback(
    (movedValues: MovedRangeValues) => {
      const newRange = computeMovedRange(baseBarRangeRef.current, movedValues);

      selectTimeRange({ ...newRange, showGuideline: true, lock: true });
      bulkUpdateTaskBars(movedValues);
      setBarParams((current) => ({ ...current, ...newRange }));
    },
    [computeMovedRange, selectTimeRange, bulkUpdateTaskBars]
  );

  const save = React.useCallback(
    async (movedValues: MovedRangeValues) => {
      const barRange = computeMovedRange(baseBarRangeRef.current, movedValues);
      const params = {
        startDate: formatISODate(barRange.startDate),
        dueDate: formatISODate(barRange.endDate),
      };

      selectTimeRange(null, { unlock: true });

      if (issue.startDate === params.startDate && issue.dueDate === params.dueDate) {
        return;
      }

      baseBarRangeRef.current = barRange;

      try {
        await commitTaskBarChanges();
      } catch (e) {
        const barParams = extractIssueBarParams(issue);

        if (barParams) {
          baseBarRangeRef.current = barParams;
          setBarParams(barParams);
          bulkUpdateTaskBars({ both: 0 });
        }
      }
    },
    [computeMovedRange, selectTimeRange, issue, commitTaskBarChanges, bulkUpdateTaskBars]
  );

  const [rangeDragSource, isRangeDragging] = useSimpleDrag({
    collect: ({ x }) => ({ both: x }),
    onDrag: updateBarParams,
    onDragEnd: save,
  });

  const [startDateDragSource, isStartDateDragging] = useSimpleDrag({
    collect: ({ x }) => ({ start: x }),
    onDrag: updateBarParams,
    onDragEnd: save,
  });

  const [dueDateDragSource, isDueDateDragging] = useSimpleDrag({
    collect: ({ x }) => ({ end: x }),
    onDrag: updateBarParams,
    onDragEnd: save,
  });

  React.useEffect(() => {
    const newBarParams = extractIssueBarParams(issue);

    if (newBarParams) {
      baseBarRangeRef.current = newBarParams;
      setBarParams(newBarParams);
    }
  }, [issue]);

  const isDragging = isRangeDragging || isStartDateDragging || isDueDateDragging;
  const isRangeDraggable = !isDragging && draggability.isRangeDraggable;
  const isStartDateDraggable = !isDragging && draggability.isStartDateDraggable;
  const isDueDateDraggable = !isDragging && draggability.isDueDateDraggable;

  const draggingIssue = {
    ...issue,
    startDate: formatISODate(barParams.startDate),
    dueDate: formatISODate(barParams.endDate),
  };

  return (
    <IssueTaskBar identifier={identifier} issue={draggingIssue} isDragging={isDragging}>
      <RangeDragHandle ref={rangeDragSource} hidden={!isRangeDraggable} />
      <DateDragHandle ref={startDateDragSource} type="start" hidden={!isStartDateDraggable} />
      <DateDragHandle ref={dueDateDragSource} type="end" hidden={!isDueDateDraggable} />
    </IssueTaskBar>
  );
};

export default DraggableIssueTaskBar;
