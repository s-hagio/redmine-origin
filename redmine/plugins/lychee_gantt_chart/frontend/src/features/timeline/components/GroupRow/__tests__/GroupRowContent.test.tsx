import * as React from 'react';
import { render } from '@testing-library/react';
import { newStatusGroup, newStatusGroupRow } from '@/__fixtures__';
import GroupRowContent from '../GroupRowContent';

const group = newStatusGroup;
const row = newStatusGroupRow;

test('Render', () => {
  const { container } = render(<GroupRowContent id={row.id} group={group} />);

  expect(container.innerHTML).toBe('');
});
