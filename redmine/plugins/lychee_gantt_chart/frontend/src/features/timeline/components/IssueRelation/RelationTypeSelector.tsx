import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { RelationType } from '@/models/issueRelation';
import { NakedButton } from '@/components/Button';
import { Icon } from '@/components/Icon';
import {
  IssueRelationConnectionType,
  useIssueRelationCreation,
  useDraftIssueRelationDelay,
  useCommitDraftIssueRelation,
} from '../../models/issueRelation';

type LabelledRelationType = Extract<RelationType, 'precedes' | 'blocks' | 'follows' | 'blocked'>;

const labelledTypesByConnectionType: Record<IssueRelationConnectionType, LabelledRelationType[]> = {
  from: [RelationType.Precedes, RelationType.Blocks],
  to: [RelationType.Follows, RelationType.Blocked],
};

type Props = {
  onCloseButtonClick: React.MouseEventHandler;
};

const RelationTypeSelector: React.FC<Props> = ({ onCloseButtonClick }) => {
  const { t } = useTranslation();

  const issueRelationCreation = useIssueRelationCreation();
  const commitCreatingIssueRelation = useCommitDraftIssueRelation();

  const [delay, setDelay] = React.useState<number>(useDraftIssueRelationDelay());

  const handleChangeDelay = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setDelay(+event.target.value);
  }, []);

  if (!issueRelationCreation) {
    return null;
  }

  const relationTypes = labelledTypesByConnectionType[issueRelationCreation.type];

  const selectRelationType = (relationType: RelationType) => {
    commitCreatingIssueRelation({ relationType, delay });
  };

  return (
    <div className={style}>
      <label>
        {t('label.issueRelationTypes.delay')}:
        <input type="number" className={delayInputStyle} value={delay} onChange={handleChangeDelay} />
        {t('label.dayPlural')}
      </label>

      {relationTypes.map((type) => (
        <button type="button" key={type} onClick={() => selectRelationType(type)}>
          {t(`label.issueRelationTypes.${type}`)}
        </button>
      ))}

      <NakedButton type="button" onClick={onCloseButtonClick}>
        <Icon name="close" />
      </NakedButton>
    </div>
  );
};

export default RelationTypeSelector;

export const style = css`
  display: flex;
  align-items: center;
  padding: 3px 8px;
  border: 1px solid #ccc;
  background: #eee;
  white-space: nowrap;

  button {
    margin-left: 8px;
  }
`;

const delayInputStyle = css`
  width: 3.5rem;
  margin-right: 0.15rem;
`;
