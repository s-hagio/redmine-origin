import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { borderColor, ganttHeaderRowHeight } from '@/styles';
import { GridRow as Row } from '../../models/grid';

type Props = {
  row: Row;
};

const GridRow: React.FC<Props> = ({ row }) => {
  const { t } = useTranslation();

  return (
    <div className={style}>
      {row.columns.map(({ id, width, label, labelPath }) => (
        <div key={id} className={columnStyle} style={{ minWidth: width }}>
          <span>{labelPath ? t(labelPath) : label}</span>
        </div>
      ))}
    </div>
  );
};

export default React.memo(GridRow);

export const style = css`
  position: relative;
  display: flex;
  flex-wrap: nowrap;
`;

const columnStyle = css`
  display: flex;
  justify-content: center;
  align-items: center;
  height: ${ganttHeaderRowHeight}px;
  border-right: 1px solid ${borderColor};
  text-align: center;
`;
