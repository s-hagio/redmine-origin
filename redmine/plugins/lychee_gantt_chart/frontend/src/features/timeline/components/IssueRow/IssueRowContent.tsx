import * as React from 'react';
import { css } from '@linaria/core';
import { Issue } from '@/models/issue';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { RowID } from '@/features/row';
import { TreeBlockItemContainer } from '../TreeBlock';
import { ScheduleStatusRowContent } from '../ScheduleStatus';
import { IssueTaskBarContainer } from '../IssueTaskBar';
import { IssueBaselineBar } from '../Baseline';

type Props = {
  id: IssueResourceIdentifier;
  subtreeRootIds: RowID[];
  issue: Issue;
};

const IssueRowContent: React.FC<Props> = ({ id, subtreeRootIds, issue }) => {
  return (
    <TreeBlockItemContainer className={style} rowId={id} subtreeRootIds={subtreeRootIds}>
      <ScheduleStatusRowContent identifier={id} />
      <IssueTaskBarContainer identifier={id} id={issue.id} />
      <IssueBaselineBar identifier={id} />
    </TreeBlockItemContainer>
  );
};

export default IssueRowContent;

const style = css`
  width: 100%;
  height: inherit;
`;
