import * as React from 'react';
import { css, cx } from '@linaria/core';
import { TreeBlockRootID, useTreeBlock, useTreeBlockHighlight } from '../../models/treeBlock';

type Props = {
  id: TreeBlockRootID;
};

const PADDING_VALUE = 2.5;

const TreeBlock: React.FC<Props> = ({ id }) => {
  const treeBlock = useTreeBlock(id);
  const isHighlighted = useTreeBlockHighlight(id);

  if (!treeBlock) {
    return null;
  }

  const top = treeBlock.top - PADDING_VALUE;
  const right = treeBlock.right + PADDING_VALUE;
  const bottom = treeBlock.bottom + PADDING_VALUE;
  const left = treeBlock.left - PADDING_VALUE;

  const path = [
    `M${left},${top}`,
    `L${right},${top}`,
    `L${right},${bottom}`,
    `L${left},${bottom}`,
    `L${left},${top}`,
    'Z',
  ].join('');

  return <path className={cx(style, isHighlighted && highlightStyle)} d={path} radius={2} />;
};

export default TreeBlock;

const style = css`
  stroke: #888;
  stroke-width: 1px;
  stroke-dasharray: 2, 2;
  fill: transparent;
  transition: fill 0.2s;
`;

const highlightStyle = css`
  stroke: #000;
  fill: rgba(181, 219, 248, 0.5);
`;
