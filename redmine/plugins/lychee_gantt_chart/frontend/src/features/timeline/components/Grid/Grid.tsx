import * as React from 'react';
import { useTotalRowHeight } from '@/features/row';
import { useTotalTickWidth } from '../../models/tick';
import { useMainGridRow, isDayGridRow, useRefreshGrid } from '../../models/grid';
import DayGridRow from './DayGridRow';
import UnitedGridRow from './UnitedGridRow';
import TodayLine from './TodayLine';

const Grid: React.FC = () => {
  const width = useTotalTickWidth();
  const height = useTotalRowHeight();
  const row = useMainGridRow();

  const refresh = useRefreshGrid();
  React.useEffect(refresh, [refresh]);

  if (!row) {
    return null;
  }

  return (
    <svg style={{ width, height }}>
      {isDayGridRow(row) && <DayGridRow columns={row.columns} />}
      <UnitedGridRow columns={row.columns} />

      <TodayLine />
    </svg>
  );
};

export default Grid;
