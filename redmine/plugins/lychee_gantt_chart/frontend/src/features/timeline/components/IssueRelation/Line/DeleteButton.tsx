import * as React from 'react';
import { css } from '@linaria/core';
import { IssueRelationID } from '@/models/issueRelation';
import { useSettings } from '@/models/setting';
import { useDeleteIssueRelation } from '@/features/resourceController';
import { OffsetFrom, OffsetTo } from './types';

type Props = {
  id: IssueRelationID;
  offsetFrom: OffsetFrom;
  offsetTo: OffsetTo;
  marginX: number;
};

const DeleteButton: React.FC<Props> = ({ id, offsetFrom, offsetTo, marginX }) => {
  const { baseURL } = useSettings();
  const deleteIssueRelation = useDeleteIssueRelation();

  const handleClick = React.useCallback(
    (event: React.MouseEvent<SVGGElement>) => {
      event.stopPropagation();

      deleteIssueRelation(id);
    },
    [deleteIssueRelation, id]
  );

  const url = `${baseURL}/images/link_break.png`;

  const width = 16;
  const height = 16;

  const top = Math.min(offsetFrom.middle, offsetTo.middle);
  const bottom = Math.max(offsetFrom.middle, offsetTo.middle);

  const x = offsetFrom.right + marginX - width / 2 + 2;
  const y = top + (bottom - top) / 2 - height / 2 - 2;

  return (
    <g className={style} onClick={handleClick}>
      <rect x={x - 2} y={y - 2} width={width + 4} height={height + 4} rx="10" ry="10" />
      <image href={url} x={x} y={y} width={width} height={height} />
    </g>
  );
};

export default DeleteButton;

const style = css`
  cursor: default;

  rect {
    fill: rgba(255, 255, 255, 0.6);
  }
  &:hover rect {
    fill: rgba(255, 255, 255, 0.8);
  }
`;
