import * as React from 'react';
import { act, fireEvent, render } from '@testing-library/react';
import { formatISODate } from '@/lib/date';
import { getIssueResourceIdentifier } from '@/models/coreResource';
import { leafIssue } from '@/__fixtures__';
import { issueTaskBar } from '../../../__fixtures__';
import IssueTaskBarCreator from '../IssueTaskBarCreator';

const useRowSelectOptionsMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useRowSelectOptions: useRowSelectOptionsMock,
}));

const selectTimeRangeMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/timeBar', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useSelectTimeRange: vi.fn().mockReturnValue(selectTimeRangeMock),
}));

const buildTaskBarMock = vi.hoisted(() => vi.fn());
const computeMovedDateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/taskBar', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useBuildTaskBar: vi.fn().mockReturnValue(buildTaskBarMock),
  useComputeMovedDate: vi.fn().mockReturnValue(computeMovedDateMock),
}));

const getTickByPositionMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/tick', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useGetTickByPosition: vi.fn().mockReturnValue(getTickByPositionMock),
}));

const issue = leafIssue;
const identifier = getIssueResourceIdentifier(issue.id);
const startingPointDate = new Date(2023, 9, 15);
const endPointDate = new Date(2023, 9, 20);

beforeEach(() => {
  useRowSelectOptionsMock.mockReturnValue({ multiple: false, range: false });
  getTickByPositionMock.mockReturnValue(startingPointDate);
  computeMovedDateMock.mockReturnValue(endPointDate);
  buildTaskBarMock.mockReturnValue(issueTaskBar);
});

const saveIssueMock = vi.fn().mockResolvedValue(void 0);

test('Render', () => {
  const { container } = render(<IssueTaskBarCreator identifier={identifier} saveIssue={saveIssueMock} />);

  expect(container).toMatchSnapshot();
});

test('ドラッグでガントバー（DateRange）を作成する', async () => {
  const ui = <IssueTaskBarCreator identifier={identifier} saveIssue={saveIssueMock} />;
  const { container, rerender } = render(ui);
  const el = container.firstChild as HTMLDivElement;

  const pointerDownEvent = new PointerEvent('pointerdown', { clientX: 100, clientY: 200, button: 0 });
  Object.defineProperty(pointerDownEvent, 'offsetX', { value: 999 });

  fireEvent(el, pointerDownEvent);
  expect(getTickByPositionMock).toBeCalledWith(999);

  fireEvent.pointerMove(el, { clientX: 121, clientY: 200 });

  expect(computeMovedDateMock).toBeCalledWith(startingPointDate, 21);
  expect(selectTimeRangeMock).toBeCalledWith({
    startDate: startingPointDate,
    endDate: endPointDate,
    showGuideline: true,
  });

  rerender(ui);
  expect(container).toMatchSnapshot('ドラッグ中');

  await act(() => fireEvent.pointerUp(el, { clientX: 121, clientY: 200 }));
  expect(container).toMatchSnapshot('ドラッグ終了後');

  expect(saveIssueMock).toBeCalledWith({
    startDate: formatISODate(startingPointDate),
    dueDate: formatISODate(endPointDate),
  });
});
