import * as React from 'react';
import { css } from '@linaria/core';
import { useStepWidth } from '../../../models/timeAxis';

type Props = {
  left: number;
};

const GridRect: React.FC<Props> = ({ left }) => {
  const stepWidth = useStepWidth();

  return <rect className={style} x={left} y={0} width={stepWidth - 1} height="100%" />;
};

export default GridRect;

const style = css`
  fill: #f7e6db;
`;
