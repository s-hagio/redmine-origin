import * as React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithRecoil } from '@/test-utils';
import Scrollable from '../Scrollable';

vi.mock('react', async () => ({
  ...(await vi.importActual<object>('react')),
  useRef: vi.fn().mockReturnValue({ current: null }),
}));

const setScrollLeftMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/scroll', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useScrollLeft: vi.fn().mockReturnValue(888),
  useSetScrollLeft: vi.fn().mockReturnValue(setScrollLeftMock),
}));

const stretchGridMock = vi.hoisted(() => vi.fn().mockReturnValue({ stretchedWidth: 0, direction: null }));

vi.mock('../../../hooks/useStretchGrid', () => ({
  useStretchGrid: vi.fn().mockReturnValue(stretchGridMock),
}));

test('Render', () => {
  const { container } = renderWithRecoil(<Scrollable>CHILD!</Scrollable>);

  expect(container).toMatchSnapshot();
});

test('スクロール時にscrollLeftStateを更新してstretchGrid()を呼ぶ', () => {
  const { container } = renderWithRecoil(<Scrollable>CHILD!</Scrollable>);

  const el = container.firstChild as HTMLDivElement;
  const scrollLeft = 999;

  expect(setScrollLeftMock).not.toBeCalled();

  fireEvent.scroll(el, { target: { scrollLeft } });

  expect(setScrollLeftMock).toBeCalledWith(scrollLeft);
});
