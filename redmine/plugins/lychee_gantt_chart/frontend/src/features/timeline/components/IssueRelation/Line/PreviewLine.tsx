import * as React from 'react';
import { RelationType } from '@/models/issueRelation';
import IssueRelationLine from './IssueRelationLine';

const PreviewLine: React.FC = () => {
  return <IssueRelationLine id={null} relationType={RelationType.Draft} />;
};

export default PreviewLine;
