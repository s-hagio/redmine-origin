import * as React from 'react';
import { render } from '@testing-library/react';
import { IssueRelationID, RelationType } from '@/models/issueRelation';
import IssueRelationLine from '../IssueRelationLine';
import { IssueRelationLineOffsets } from '../../../../models/issueRelation';
import { offsetFrom, offsetTo } from '../__fixtures__';

const useIsDeletableIssueRelationMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/issueRelation', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIsDeletableIssueRelation: useIsDeletableIssueRelationMock,
}));

const useIssueRelationLineOffsetsMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/issueRelation', () => ({
  useIssueRelationLineOffsets: useIssueRelationLineOffsetsMock,
}));

vi.mock('../Line', () => ({
  default: vi.fn((props) => `Line: ${JSON.stringify(props)}`),
}));

vi.mock('Arrow', () => ({
  default: vi.fn((props) => `Arrow: ${JSON.stringify(props)}`),
}));

const id = 123;
const relationType = RelationType.Precedes;
const offsets: IssueRelationLineOffsets = { from: offsetFrom, to: offsetTo };

beforeEach(() => {
  useIsDeletableIssueRelationMock.mockReturnValue(true);
  useIssueRelationLineOffsetsMock.mockReturnValue(offsets);
});

const renderForTesting = (selectedIssueRelationId?: IssueRelationID) => {
  return render(
    <IssueRelationLine
      id={id}
      relationType={relationType}
      selectedIssueRelationId={selectedIssueRelationId}
    />,
    { wrapper: ({ children }) => <svg>{children}</svg> }
  );
};

test('Render', () => {
  const { container } = renderForTesting();

  expect(container).toMatchSnapshot();
});

test('selected', () => {
  const { container } = renderForTesting(id);

  expect(container).toMatchSnapshot();
});

test('isDeletableIssueRelation: false', () => {
  useIsDeletableIssueRelationMock.mockReturnValue(false);

  const { container } = renderForTesting();

  expect(container).toMatchSnapshot();
});

test('offsetsがない場合は表示しない', () => {
  useIssueRelationLineOffsetsMock.mockReturnValue(null);

  const { container } = renderForTesting();

  expect(container.textContent).toBe('');
});
