import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { getIssueResourceIdentifier } from '@/models/coreResource';
import { issueBarParams, issueTaskBar } from '../../../__fixtures__';
import TaskBar from '../TaskBar';

const useTaskBarMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/taskBar', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useTaskBar: useTaskBarMock,
}));

const selectTimeRangeMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/timeBar', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useSelectTimeRange: vi.fn().mockReturnValue(selectTimeRangeMock),
}));

beforeEach(() => {
  useTaskBarMock.mockReturnValue(issueTaskBar);
});

const identifier = getIssueResourceIdentifier(1);

test('タスクバーをレンダリングする', () => {
  const result = render(
    <TaskBar identifier={identifier} barParams={issueBarParams}>
      CHILD!
    </TaskBar>
  );

  expect(useTaskBarMock).toBeCalledWith(identifier);
  expect(result.container).toMatchSnapshot();
});

test('showRangeMarkerがtrueならrangeMarkerのstyleを適用', () => {
  const result = render(<TaskBar identifier={identifier} barParams={issueBarParams} showRangeMarker />);

  expect(result.container).toMatchSnapshot();
});

test('hover/leaveでSelectedTimeRangeを表示したり非表示にしたりする', () => {
  const { container } = render(
    <TaskBar identifier={identifier} barParams={issueBarParams}>
      CHILD!
    </TaskBar>
  );

  const el = container.firstChild as HTMLDivElement;

  expect(selectTimeRangeMock).not.toBeCalled();

  fireEvent.mouseEnter(el);

  expect(selectTimeRangeMock).lastCalledWith({
    startDate: issueBarParams.startDate,
    endDate: issueBarParams.endDate,
  });

  fireEvent.mouseLeave(el);

  expect(selectTimeRangeMock).lastCalledWith(null);
});
