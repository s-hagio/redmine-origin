import * as React from 'react';
import { atom, useRecoilState, useRecoilValue } from 'recoil';
import { fireEvent } from '@testing-library/react';
import { beforeEach } from 'vitest';
import { renderWithRecoilHook, renderWithTranslation } from '@/test-utils';
import ZoomButton from '../ZoomButton';
import { TimeAxisType, timeAxisTypes } from '../../../../models/timeAxis';

const setTimeAxisOptionsMock = vi.hoisted(() => vi.fn());
const useTimeAxisOptionsStateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/timeAxis', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useTimeAxisOptionsState: useTimeAxisOptionsStateMock,
}));

test.each(timeAxisTypes)('Render: %s', (type) => {
  useTimeAxisOptionsStateMock.mockReturnValue([{ type }, setTimeAxisOptionsMock]);

  const { container } = renderWithTranslation(
    <>
      <ZoomButton type="zoomOut" />
      <ZoomButton type="zoomIn" />
    </>
  );

  expect(container).toMatchSnapshot();
});

describe('click', () => {
  // 週別初期表示 with 他の値
  const timeAxisOptionsState = atom({
    key: 'timeAxisOptionsState',
    default: { otherKey: 'otherValue', type: TimeAxisType.Week },
  });

  const renderForTesting = (ui: React.ReactElement) => {
    return renderWithRecoilHook(ui, () => useRecoilValue(timeAxisOptionsState));
  };

  beforeEach(() => {
    useTimeAxisOptionsStateMock.mockImplementation(() => useRecoilState(timeAxisOptionsState));
  });

  test('zoomOutの場合はクリックで縮小処理', () => {
    const { getByRole, result } = renderForTesting(<ZoomButton type="zoomOut" />);
    const button = getByRole('button');

    fireEvent.click(button);
    expect(result.current).toEqual({ otherKey: 'otherValue', type: TimeAxisType.MonthLarge });

    fireEvent.click(button);
    expect(result.current).toEqual({ otherKey: 'otherValue', type: TimeAxisType.MonthSmall });
  });

  test('zoomInの場合はクリックで拡大処理', () => {
    const { getByRole, result } = renderForTesting(<ZoomButton type="zoomIn" />);
    const button = getByRole('button');

    fireEvent.click(button);
    expect(result.current).toEqual({ otherKey: 'otherValue', type: TimeAxisType.DaySmall });

    fireEvent.click(button);
    expect(result.current).toEqual({ otherKey: 'otherValue', type: TimeAxisType.DayLarge });
  });
});
