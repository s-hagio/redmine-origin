import * as React from 'react';
import { ProjectResourceIdentifier } from '@/models/coreResource';
import { useBaselineBar } from '../../../models/baseline';
import BaselineBar from './BaselineBar';

type Props = {
  identifier: ProjectResourceIdentifier;
};

const ProjectBaselineBar: React.FC<Props> = ({ identifier }) => {
  const bar = useBaselineBar(identifier);

  if (!bar) {
    return null;
  }

  return <BaselineBar bar={bar} />;
};

export default ProjectBaselineBar;
