import * as React from 'react';
import { useTimelineViewport } from '../../models/viewport';
import { useIsNeedToCentering, useSetNeedToCentering, useSetScrollLeft } from '../../models/scroll';
import { useCenterTickPosition } from '../../models/tick';

const CenteringExecutor: React.FC = () => {
  const viewport = useTimelineViewport();

  const isNeedToCentering = useIsNeedToCentering();
  const setNeedToCentering = useSetNeedToCentering();
  const centerTickPosition = useCenterTickPosition();

  const setScrollLeft = useSetScrollLeft();

  React.useLayoutEffect(() => {
    if (isNeedToCentering) {
      setScrollLeft(centerTickPosition - viewport.width / 2);
      setNeedToCentering(false);
    }
  }, [isNeedToCentering, centerTickPosition, viewport.width, setNeedToCentering, setScrollLeft]);

  return null;
};

export default React.memo(CenteringExecutor);
