import * as React from 'react';
import { render } from '@testing-library/react';
import { newStatusGroup, newStatusGroupRow } from '@/__fixtures__';
import GroupRow from '../GroupRow';

const useGroupMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/group', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useGroup: useGroupMock,
}));

vi.mock('../../Row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn((props) => React.createElement('row-mock', props)),
}));

const GroupRowContentSpy = vi.hoisted(() => vi.fn());

vi.mock('../GroupRowContent', () => ({
  default: vi.fn((props) => {
    GroupRowContentSpy(props);
    return 'GroupRowContent';
  }),
}));

const group = newStatusGroup;
const row = newStatusGroupRow;

beforeEach(() => {
  useGroupMock.mockReturnValue(group);
});

test('Render', () => {
  const { container } = render(<GroupRow id={row.id} resourceId={row.resourceId} />);

  expect(container).toMatchSnapshot();
  expect(GroupRowContentSpy).toBeCalledWith({ id: row.id, group });
});
