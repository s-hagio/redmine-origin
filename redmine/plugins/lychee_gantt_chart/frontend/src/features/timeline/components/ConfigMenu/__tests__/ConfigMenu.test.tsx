import * as React from 'react';
import { render } from '@testing-library/react';
import ConfigMenu from '../ConfigMenu';

vi.mock('../../ConfigDialog', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn((props) => `ConfigDialog: ${JSON.stringify(props)}`),
}));

vi.mock('../ButtonList', () => ({
  default: vi.fn((props) => `ButtonList: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<ConfigMenu />);

  expect(container).toMatchSnapshot();
});
