import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { css } from '@linaria/core';
import { Icon } from '@/components/Icon';
import { NakedButton } from '@/components/Button';
import { ICON_SIZE } from '../constants';
import { TimeAxisType, timeAxisTypes, useTimeAxisOptionsState } from '../../../models/timeAxis';

const TimeAxisTypeSelect: React.FC = () => {
  const { t } = useTranslation();

  const [timeAxisOptions, setTimeAxisOptions] = useTimeAxisOptionsState();

  const handleChange = React.useCallback(
    ({ currentTarget }: React.ChangeEvent<HTMLSelectElement>) => {
      const type = currentTarget.value as TimeAxisType;
      setTimeAxisOptions((current) => ({ ...current, type }));
    },
    [setTimeAxisOptions]
  );

  return (
    <NakedButton className={style}>
      <Icon name="chevronDown" size={ICON_SIZE} />

      <select value={timeAxisOptions.type} onChange={handleChange}>
        {timeAxisTypes.map((type) => (
          <option key={type} value={type}>
            {t(`timeline.gridType.${type}`)}
          </option>
        ))}
      </select>
    </NakedButton>
  );
};

export default TimeAxisTypeSelect;

const style = css`
  position: relative;
  overflow: hidden;

  select {
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    cursor: pointer;
  }
`;
