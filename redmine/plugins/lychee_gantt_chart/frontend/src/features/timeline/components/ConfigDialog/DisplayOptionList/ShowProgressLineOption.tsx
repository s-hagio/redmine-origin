import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { useTimelineOptionsState } from '../../../models/options';

const ShowProgressLineOption: React.FC = () => {
  const { t } = useTranslation();

  const [timelineOptions, setTimelineOptions] = useTimelineOptionsState();

  const handleChange = React.useCallback(() => {
    setTimelineOptions((current) => ({
      ...current,
      showProgressLine: !current.showProgressLine,
    }));
  }, [setTimelineOptions]);

  return (
    <label>
      <input
        type="checkbox"
        name="showProgressLine"
        checked={timelineOptions.showProgressLine}
        onChange={handleChange}
      />
      {t('timeline.progressLine')}
    </label>
  );
};

export default ShowProgressLineOption;
