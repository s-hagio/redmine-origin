import * as React from 'react';
import IssueRelationCreationCanceller from './IssueRelationCreationCanceller';
import { IssueRelationLineContainer } from './Line';
import RelationStateRefresher from './RelationStateRefresher';

type Props = {
  width: number;
};

const RelationLineContainer: React.FC<Props> = ({ width }) => {
  return (
    <>
      <React.Suspense>
        <RelationStateRefresher />
      </React.Suspense>

      <IssueRelationLineContainer width={width} />
      <IssueRelationCreationCanceller width={width} />
    </>
  );
};

export default React.memo(RelationLineContainer);
