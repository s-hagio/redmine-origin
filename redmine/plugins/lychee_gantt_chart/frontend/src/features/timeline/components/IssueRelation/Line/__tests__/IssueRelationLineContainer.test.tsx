import * as React from 'react';
import { useRecoilValue } from 'recoil';
import { fireEvent } from '@testing-library/react';
import { renderWithRecoil, renderWithRecoilHook } from '@/test-utils';
import { IssueRelation, RelationType } from '@/models/issueRelation';
import IssueRelationLineContainer from '../IssueRelationLineContainer';
import { deletableLineStyle } from '../Line';
import { selectedIssueRelationIdState } from '../../../../models/issueRelation';

const useTimelineOptionsStateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/options', () => ({
  useTimelineOptionsState: useTimelineOptionsStateMock,
}));

const useVisibleIssueRelationsMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/issueRelation', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useVisibleIssueRelations: useVisibleIssueRelationsMock,
}));

vi.mock('../IssueRelationLine', () => ({
  default: vi.fn((props) => `IssueRelationLine: ${JSON.stringify(props)}`),
}));

vi.mock('../PreviewLine', () => ({
  default: vi.fn((props) => `PreviewLine: ${JSON.stringify(props)}`),
}));

const issueRelations: IssueRelation[] = [
  { id: 1, relationType: RelationType.Precedes, issueFromId: 1, issueToId: 2 },
  { id: 2, relationType: RelationType.Blocks, issueFromId: 3, issueToId: 4 },
  { id: 3, relationType: RelationType.Precedes, issueFromId: 5, issueToId: 6 },
];

beforeEach(() => {
  useTimelineOptionsStateMock.mockReturnValue([{ showRelationLine: true }]);
  useVisibleIssueRelationsMock.mockReturnValue(issueRelations);
});

test('Render', () => {
  const { container } = renderWithRecoil(<IssueRelationLineContainer width={100} />, {
    initializeState: ({ set }) => {
      set(selectedIssueRelationIdState, 123);
    },
  });

  expect(container).toMatchSnapshot();
});

test('Render: 「関連線の表示」がOFFの時は関連線をレンダリングしない', () => {
  useTimelineOptionsStateMock.mockReturnValue([{ showRelationLine: false }]);

  const { container } = renderWithRecoil(<IssueRelationLineContainer width={100} />);

  expect(container).toMatchSnapshot();
});

describe('when clicked', () => {
  const elementsFromPointSpy = vi.fn();

  if (!document.elementsFromPoint) {
    Object.defineProperty(document, 'elementsFromPoint', {
      value: () => [],
      configurable: true,
    });
  }

  beforeEach(() => {
    vi.spyOn(document, 'elementsFromPoint').mockImplementation(elementsFromPointSpy);
  });

  const deletableLineElements = issueRelations.map(({ id }) => {
    const el = document.createElement('path');
    el.setAttribute('class', deletableLineStyle);
    el.setAttribute('data-id', id + '');
    return el;
  });

  const renderForTesting = () => {
    return renderWithRecoilHook(<IssueRelationLineContainer width={100} />, () => ({
      selectedId: useRecoilValue(selectedIssueRelationIdState),
    }));
  };

  test('クリック座標から関連線要素が取得できたらその要素を選択する', () => {
    elementsFromPointSpy.mockReturnValue([deletableLineElements[0]]);

    const { container, result } = renderForTesting();

    fireEvent.click(container.querySelector('svg')!, { clientX: 123, clientY: 456 });

    expect(result.current.selectedId).toBe(issueRelations[0].id);
    expect(elementsFromPointSpy).toBeCalledWith(123, 456);
  });

  test('線が重なっている場合はクリックするたびに選択を移す', () => {
    elementsFromPointSpy.mockReturnValue(deletableLineElements);

    const { container, result } = renderForTesting();
    const svg = container.querySelector('svg')!;

    fireEvent.click(svg, { clientX: 0, clientY: 0 });
    expect(result.current.selectedId).toBe(issueRelations[0].id);

    fireEvent.click(svg, { clientX: 0, clientY: 0 });
    expect(result.current.selectedId).toBe(issueRelations[1].id);

    fireEvent.click(svg, { clientX: 0, clientY: 0 });
    expect(result.current.selectedId).toBe(issueRelations[2].id);

    fireEvent.click(svg, { clientX: 0, clientY: 0 });
    expect(result.current.selectedId).toBe(issueRelations[0].id);

    fireEvent.click(svg, { clientX: 0, clientY: 0 });
    expect(result.current.selectedId).toBe(issueRelations[1].id);
  });

  test('何もないところをクリックしたらunselect', () => {
    elementsFromPointSpy.mockReturnValue([]);

    const { container, result } = renderForTesting();

    fireEvent.click(container.querySelector('svg')!, { clientX: 0, clientY: 0 });

    expect(result.current.selectedId).toBeNull();
  });
});
