import * as React from 'react';
import { css } from '@linaria/core';
import { UnitedGridColumn } from '../../models/grid';
import { unitedGridColor } from './DayGridRow/colors';

type Props = {
  columns: UnitedGridColumn[];
};

const UnitedGridRow: React.FC<Props> = ({ columns }) => {
  const positions = React.useMemo(
    () =>
      columns.reduce<number[]>((arr, column) => {
        const prevValue = arr.at(-1) ?? 0;

        arr.push(prevValue + column.width);

        return arr;
      }, []),
    [columns]
  );

  return (
    <g className={style}>
      {positions.map((x) => (
        <line key={x} x1={x - 0.5} x2={x - 0.5} y1="0" y2="100%" />
      ))}
    </g>
  );
};

export default React.memo(UnitedGridRow);

const style = css`
  line {
    stroke: ${unitedGridColor};
  }
`;
