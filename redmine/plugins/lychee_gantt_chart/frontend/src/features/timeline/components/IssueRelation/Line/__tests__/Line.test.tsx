import * as React from 'react';
import { render } from '@testing-library/react';
import { RelationType } from '@/models/issueRelation';
import Line from '../Line';
import { offsetFrom, offsetTo } from '../__fixtures__';

vi.mock('../DeleteButton', () => ({
  default: vi.fn((props) => `DeleteButton: ${JSON.stringify(props)}`),
}));

const renderForTesting = (props: Partial<React.ComponentProps<typeof Line>> = {}) => {
  return render(
    <Line
      id={123}
      relationType={RelationType.Precedes}
      isDeletable={true}
      isSelected={false}
      offsetFrom={offsetFrom}
      offsetTo={offsetTo}
      color="#f33"
      {...props}
    />
  );
};

test('Render', () => {
  const { container } = renderForTesting();

  expect(container).toMatchSnapshot();
});

describe('isSelected', () => {
  test('Render', () => {
    const { container } = renderForTesting({ isSelected: true, isDeletable: true });

    expect(container).toMatchSnapshot();
  });

  test('Render: not deletable', () => {
    const { container } = renderForTesting({ isSelected: true, isDeletable: false });

    expect(container).toMatchSnapshot();
  });
});
