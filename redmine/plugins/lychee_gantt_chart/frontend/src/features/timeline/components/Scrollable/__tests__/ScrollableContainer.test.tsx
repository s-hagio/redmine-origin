import * as React from 'react';
import { renderWithRecoil } from '@/test-utils';
import ScrollableContainer from '../ScrollableContainer';

vi.mock('../Scrollable', () => ({
  default: vi.fn(({ children, ...props }) => React.createElement('scrollable-mock', props, children)),
}));

vi.mock('../CenteringExecutor', () => ({
  default: vi.fn(({ children, ...props }) => React.createElement('centering-executor', props, children)),
}));

vi.mock('../ScrollButton', () => ({
  ScrollButtonContainer: vi.fn(({ children, ...props }) =>
    React.createElement('scroll-button-container', props, children)
  ),
}));

test('Render', () => {
  const { container } = renderWithRecoil(<ScrollableContainer>CHILD!</ScrollableContainer>);

  expect(container).toMatchSnapshot();
});
