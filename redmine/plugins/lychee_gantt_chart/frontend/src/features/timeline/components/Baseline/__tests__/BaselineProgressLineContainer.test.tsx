import * as React from 'react';
import { render } from '@testing-library/react';
import BaselineProgressLineContainer from '../BaselineProgressLineContainer';

const useBaselineProgressLineOffsetsMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/baseline', () => ({
  useBaselineProgressLineOffsets: useBaselineProgressLineOffsetsMock,
}));

vi.mock('../../ProgressLine', () => ({
  ProgressLine: vi.fn((props) => `ProgressLine: ${JSON.stringify(props)}`),
}));

beforeEach(() => {
  useBaselineProgressLineOffsetsMock.mockReturnValue([
    { top: 0, left: 10 },
    { top: 1, left: 11 },
  ]);
});

test('Render', () => {
  const { container } = render(<BaselineProgressLineContainer width={100} />);

  expect(container).toMatchSnapshot();
});

test('イナズマ線のオフセットリストがない場合は描画しない', () => {
  useBaselineProgressLineOffsetsMock.mockReturnValue([]);

  const { container } = render(<BaselineProgressLineContainer width={100} />);

  expect(container.innerHTML).toBe('');
});
