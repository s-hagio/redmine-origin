import * as React from 'react';
import { render } from '@testing-library/react';
import PreviewLine from '../PreviewLine';

vi.mock('../IssueRelationLine', () => ({
  default: vi.fn((props) => `IssueRelationLine: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<PreviewLine />);

  expect(container).toMatchSnapshot();
});
