import * as React from 'react';
import { css } from '@linaria/core';
import { IssueID, useIssue } from '@/models/issue';
import { FieldContent, FieldFormat } from '@/features/field';
import Metadata from '../Metadata';

type Props = {
  issueId: IssueID;
};

const MetadataList: React.FC<Props> = ({ issueId }) => {
  const issue = useIssue(issueId);

  if (!issue) {
    return null;
  }

  return (
    <div className={style}>
      <div className={titleMetaStyle}>
        <Metadata type="title">{issue.subject}</Metadata>
      </div>

      <div className={statusMetaStyle}>
        <Metadata type="status">
          <FieldContent format={FieldFormat.IssueStatus} value={issue.statusId} />
        </Metadata>

        {issue.doneRatio != null && (
          <Metadata type="progressRate">
            <FieldContent format={FieldFormat.Percent} value={issue.doneRatio} />
          </Metadata>
        )}

        {issue.assignedToId && (
          <Metadata type="assignedUser">
            <FieldContent format={FieldFormat.Principal} value={issue.assignedToId} />
          </Metadata>
        )}
      </div>
    </div>
  );
};

export default MetadataList;

const style = css`
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  width: 100%;
  height: 100%;
  z-index: 9;
`;

const titleMetaStyle = css`
  display: flex;
  align-items: stretch;
  flex-shrink: 0;
  min-width: 100%;
  height: 100%;
  padding: 1px;
  color: #000;

  [data-meta='title'] {
    padding: 0 0.25rem;
    border-radius: 2px;
  }
`;

const statusMetaStyle = css`
  display: flex;
  align-items: center;
  padding-left: 0.5rem;

  .${titleMetaStyle} + & {
    margin-left: 0;
  }

  [data-meta] + [data-meta] {
    ::before {
      display: inline-block;
      content: '';
      width: 2px;
      height: 2px;
      margin: 0 0.25em;
      border-radius: 100%;
      background: #333;
    }
  }
`;
