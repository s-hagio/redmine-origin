import * as React from 'react';
import { css } from '@linaria/core';
import Scrollable from './Scrollable';
import CenteringExecutor from './CenteringExecutor';
import { ScrollButtonContainer } from './ScrollButton';

type Props = {
  children: React.ReactNode;
};

const ScrollableContainer: React.FC<Props> = ({ children }) => {
  return (
    <div className={style}>
      <Scrollable>{children}</Scrollable>
      <CenteringExecutor />
      <ScrollButtonContainer />
    </div>
  );
};

export default ScrollableContainer;

const style = css`
  position: absolute;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;
