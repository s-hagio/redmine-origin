import * as React from 'react';
import { css, cx } from '@linaria/core';
import { IssueRelationID, RelationType, useIsDeletableIssueRelation } from '@/models/issueRelation';
import { useIssueRelationLineOffsets } from '../../../models/issueRelation';
import Line from './Line';
import Arrow from './Arrow';

type Props = {
  id: IssueRelationID | null;
  relationType: RelationType;
  selectedIssueRelationId?: IssueRelationID | null;
};

const DEFAULT_COLOR = '#CCC';

const colors: ReadonlyMap<string, string> = new Map([
  [RelationType.Precedes, '#373ed5'],
  [RelationType.Blocks, '#f3944f'],
  [RelationType.Draft, '#000'],
]);

const IssueRelationLine: React.FC<Props> = ({ id, relationType, selectedIssueRelationId }) => {
  const isDeletable = useIsDeletableIssueRelation(id);
  const offsets = useIssueRelationLineOffsets(id);

  if (!offsets) {
    return null;
  }

  const color = colors.get(relationType) || DEFAULT_COLOR;
  const isSelected = selectedIssueRelationId + '' === id + '';
  const isLowlight = selectedIssueRelationId != null && !isSelected;

  return (
    <g className={cx(isLowlight && lowlightStyle)}>
      <Line
        id={id}
        relationType={relationType}
        isDeletable={isDeletable}
        isSelected={isSelected}
        offsetFrom={offsets.from}
        offsetTo={offsets.to}
        color={color}
      />
      <Arrow offsetTo={offsets.to} color={color} />
    </g>
  );
};

export default React.memo(IssueRelationLine);

const lowlightStyle = css`
  path {
    opacity: 0.7;
  }
`;
