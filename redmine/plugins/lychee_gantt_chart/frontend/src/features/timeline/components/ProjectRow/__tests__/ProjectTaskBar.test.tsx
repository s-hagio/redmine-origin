import * as React from 'react';
import { render } from '@testing-library/react';
import { getProjectResourceIdentifier } from '@/models/coreResource';
import { rootProject } from '@/__fixtures__';
import { projectBarParams } from '../../../__fixtures__';
import ProjectTaskBar from '../ProjectTaskBar';

const extractProjectBarParamsMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/taskBar', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  extractProjectBarParams: extractProjectBarParamsMock,
}));

const TaskBarSpy = vi.hoisted(() => vi.fn());

vi.mock('../../TaskBar', () => ({
  default: vi.fn(({ children, ...props }) => {
    TaskBarSpy(props);
    return React.createElement('task-bar', null, children);
  }),
}));

const project = rootProject;
const identifier = getProjectResourceIdentifier(project.id);
const barParams = projectBarParams;

beforeEach(() => {
  extractProjectBarParamsMock.mockReturnValue(barParams);
});

test('Render', () => {
  const result = render(<ProjectTaskBar identifier={identifier} project={project} />);

  expect(result.container).toMatchSnapshot();
  expect(TaskBarSpy).toBeCalledWith({ identifier, barParams, showRangeMarker: true });
  expect(extractProjectBarParamsMock).toHaveBeenCalledWith(project);
});
