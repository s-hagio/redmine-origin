import * as React from 'react';
import { css } from '@linaria/core';
import { useStepWidth } from '../../../models/timeAxis';

type Props = {
  left: number;
};

const TodayLine: React.FC<Props> = ({ left }) => {
  const stepWidth = useStepWidth();
  const x = left + stepWidth + -0.25;

  return <line className={style} x1={x} x2={x} y1="0" y2="100%" />;
};

export default TodayLine;

const style = css`
  stroke: #333;
  stroke-width: 1px;
  stroke-dasharray: 4px 2px;
`;
