import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import IssueRelationCreationActivateButton from '../IssueRelationCreateButton';

const activateIssueRelationCreationMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/issueRelation', () => ({
  useActivateIssueRelationCreation: vi.fn().mockReturnValue(activateIssueRelationCreationMock),
}));

test('Render', () => {
  const { container } = render(<IssueRelationCreationActivateButton issueId={1} type="to" />);

  expect(container).toMatchSnapshot();
});

test('クリックでIssueRelationの作成モード開始', () => {
  const { getByRole } = render(<IssueRelationCreationActivateButton issueId={1} type="from" />);

  expect(activateIssueRelationCreationMock).not.toBeCalled();

  fireEvent.click(getByRole('button'), { button: 0 });

  expect(activateIssueRelationCreationMock).toBeCalledWith({
    type: 'from',
    sourceIssueId: 1,
  });
});
