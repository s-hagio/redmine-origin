import * as React from 'react';
import { css } from '@linaria/core';

type Props = {
  hidden?: boolean;
};

export default React.forwardRef<HTMLDivElement, Props>(function RangeDragHandle({ hidden }, ref) {
  const cssProps: React.CSSProperties = React.useMemo(() => {
    return hidden ? { visibility: 'hidden' } : {};
  }, [hidden]);

  return <div ref={ref} className={style} style={cssProps} />;
});

export const style = css`
  width: inherit;
  height: inherit;
  cursor: grab;
`;
