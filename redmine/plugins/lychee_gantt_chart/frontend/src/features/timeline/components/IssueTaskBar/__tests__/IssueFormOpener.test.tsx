import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { rootIssue } from '@/__fixtures__';
import IssueFormOpener from '../IssueFormOpener';

const openIssueFormMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/issueForm', () => ({
  useOpenIssueForm: vi.fn().mockReturnValue(openIssueFormMock),
}));

test('mouseDown -> mouseUp中のカーソル移動がしきい値以内ならIssueFormを開く', () => {
  const { container } = render(<IssueFormOpener issueId={rootIssue.id}>CHILD!</IssueFormOpener>);
  const el = container.firstChild as HTMLDivElement;

  expect(container).toMatchSnapshot();

  fireEvent.mouseDown(el, { screenX: 100, screenY: 100 });
  expect(openIssueFormMock).not.toBeCalled();

  fireEvent.mouseUp(document);
  expect(openIssueFormMock).toBeCalledWith({ id: rootIssue.id });
});

test('しきい値を1度でも超えたら開かない', () => {
  const { container } = render(<IssueFormOpener issueId={rootIssue.id}>CHILD!</IssueFormOpener>);
  const el = container.firstChild as HTMLDivElement;

  fireEvent.mouseDown(el, { screenX: 100, screenY: 100 });
  expect(openIssueFormMock).not.toBeCalled();

  fireEvent.mouseMove(document, { screenX: 101, screenY: 100 });

  fireEvent.mouseUp(document);
  expect(openIssueFormMock).not.toBeCalled();

  fireEvent.mouseUp(document);
  expect(openIssueFormMock).not.toBeCalled();
});

test('disabledなら開かない', () => {
  const { container } = render(
    <IssueFormOpener issueId={rootIssue.id} disabled>
      CHILD!
    </IssueFormOpener>
  );

  fireEvent.mouseDown(container.firstChild as HTMLDivElement, { screenX: 100, screenY: 100 });
  fireEvent.mouseUp(document);

  expect(openIssueFormMock).not.toBeCalled();
});
