import * as React from 'react';
import { css, cx } from '@linaria/core';
import { getZIndex } from '@/styles';
import { TimeDirection } from '../../../models/timeAxis';
import ScrollButton, { style as scrollButtonStyle } from './ScrollButton';

const ScrollButtonContainer: React.FC = () => {
  const ref = React.useRef<HTMLDivElement>(null);
  const [isFixed, setFixed] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (!ref.current?.parentElement) {
      return;
    }

    const observer = new ResizeObserver(([entry]) => {
      const el = entry.target as HTMLElement;
      const offsetBottom = el.offsetTop + entry.contentRect.bottom;

      setFixed(window.innerHeight < offsetBottom);
    });

    observer.observe(ref.current.parentElement);
  }, []);

  return (
    <div ref={ref} className={cx(style, isFixed && fixedStyle)}>
      <div className={backwardScrollButtonContainerStyle}>
        <ScrollButton direction={TimeDirection.Backward} />
      </div>
      <div className={forwardScrollButtonContainerStyle}>
        <ScrollButton direction={TimeDirection.Forward} />
      </div>
    </div>
  );
};

export default React.memo(ScrollButtonContainer);

const style = css`
  position: absolute;
  top: calc(50% - 20px);
  left: 0;
  right: 0;
  z-index: ${getZIndex('timeline/ScrollButton')};

  > div {
    position: absolute;
  }
`;

const backwardScrollButtonContainerStyle = css`
  left: 5px;
`;

const forwardScrollButtonContainerStyle = css`
  right: 5px;
`;

const fixedStyle = css`
  .${scrollButtonStyle} {
    position: fixed;
    top: 50%;
  }

  .${forwardScrollButtonContainerStyle} {
    .${scrollButtonStyle} {
      transform: translateX(-100%);
    }
  }
`;
