import * as React from 'react';
import { render } from '@testing-library/react';
import { mainProjectVersionRow, versionSharedByHierarchy } from '@/__fixtures__';
import { versionBarParams } from '../../../__fixtures__';
import VersionTaskBar from '../VersionTaskBar';

const extractVersionBarParamsMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/taskBar', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  extractVersionBarParams: extractVersionBarParamsMock,
}));

const TaskBarSpy = vi.hoisted(() => vi.fn());

vi.mock('../../TaskBar', () => ({
  default: vi.fn(({ children, ...props }) => {
    TaskBarSpy(props);
    return React.createElement('task-bar', null, children);
  }),
}));

const version = versionSharedByHierarchy;
const identifier = mainProjectVersionRow.id;
const barParams = versionBarParams;

beforeEach(() => {
  extractVersionBarParamsMock.mockReturnValue(barParams);
});

test('Render', () => {
  const result = render(<VersionTaskBar identifier={identifier} version={version} />);

  expect(result.container).toMatchSnapshot();
  expect(TaskBarSpy).toBeCalledWith({ identifier, barParams, showRangeMarker: true });
  expect(extractVersionBarParamsMock).toHaveBeenLastCalledWith(version);
});
