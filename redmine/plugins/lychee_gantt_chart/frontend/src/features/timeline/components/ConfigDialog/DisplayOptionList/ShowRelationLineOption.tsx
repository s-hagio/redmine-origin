import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { useTimelineOptionsState } from '../../../models/options';

const ShowRelationLineOption: React.FC = () => {
  const { t } = useTranslation();

  const [timelineOptions, setTimelineOptions] = useTimelineOptionsState();

  const handleChange = React.useCallback(
    ({ currentTarget }: React.ChangeEvent<HTMLInputElement>) => {
      setTimelineOptions((current) => ({
        ...current,
        showRelationLine: currentTarget.checked,
      }));
    },
    [setTimelineOptions]
  );

  return (
    <label>
      <input
        type="checkbox"
        name="showRelationLine"
        checked={timelineOptions.showRelationLine}
        onChange={handleChange}
      />
      {t('timeline.relationLine')}
    </label>
  );
};

export default ShowRelationLineOption;
