import * as React from 'react';
import { useRecoilValue } from 'recoil';
import { fireEvent } from '@testing-library/react';
import { renderWithRecoil, renderWithRecoilHook } from '@/test-utils';
import MetadataVisibilityRadioButton from '../MetadataVisibilityRadioButton';
import { metadataVisibilityState } from '../../../../models/metadata';

test('Render', () => {
  const { container } = renderWithRecoil(
    <>
      <MetadataVisibilityRadioButton type="title" visibility="visible" />
      <MetadataVisibilityRadioButton type="assignedUser" visibility="visibleOnlyWhenColumnIsHidden" />
    </>
  );

  expect(container).toMatchSnapshot();
});

test('選択されたら項目のVisibilityを変更する', () => {
  const { getByDisplayValue, result } = renderWithRecoilHook(
    <>
      <MetadataVisibilityRadioButton type="status" visibility="hidden" />
      <MetadataVisibilityRadioButton type="status" visibility="visible" />
    </>,
    () => useRecoilValue(metadataVisibilityState('status')),
    {
      initializeState: ({ set }) => {
        set(metadataVisibilityState('status'), 'hidden');
      },
    }
  );

  const hiddenButton = getByDisplayValue('hidden') as HTMLInputElement;
  const visibleButton = getByDisplayValue('visible') as HTMLInputElement;

  expect(result.current).toBe('hidden');
  expect(hiddenButton.checked).toBe(true);
  expect(visibleButton.checked).toBe(false);

  fireEvent.click(visibleButton);

  expect(result.current).toBe('visible');
  expect(hiddenButton.checked).toBe(false);
  expect(visibleButton.checked).toBe(true);

  fireEvent.click(hiddenButton);

  expect(result.current).toBe('hidden');
  expect(hiddenButton.checked).toBe(true);
  expect(visibleButton.checked).toBe(false);
});
