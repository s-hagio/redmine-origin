import * as React from 'react';
import { render } from '@testing-library/react';
import ButtonList from '../ButtonList';

vi.mock('../buttons', () => ({
  ScrollToCenterButton: vi.fn((props) => `ScrollToCenterButton: ${JSON.stringify(props)}`),
  ZoomButton: vi.fn((props) => `ZoomButton: ${JSON.stringify(props)}`),
  TimeAxisTypeSelect: vi.fn((props) => `TimeAxisTypeSelect: ${JSON.stringify(props)}`),
  MoreConfigButton: vi.fn((props) => `MoreConfigButton: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<ButtonList />);

  expect(container).toMatchSnapshot();
});
