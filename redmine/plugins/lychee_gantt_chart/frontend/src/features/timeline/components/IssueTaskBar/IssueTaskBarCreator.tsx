import * as React from 'react';
import { css, cx } from '@linaria/core';
import { formatISODate } from '@/lib/date';
import { MovedPosition, useSimpleDrag } from '@/lib/hooks';
import { IssueParams } from '@/models/issue';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { useRowSelectOptions } from '@/features/row';
import { TaskBar, TaskBarResourceType, useBuildTaskBar, useComputeMovedDate } from '../../models/taskBar';
import { TimeBarRange, useSelectTimeRange } from '../../models/timeBar';
import { useGetTickByPosition } from '../../models/tick';

type Props = {
  identifier: IssueResourceIdentifier;
  saveIssue: (params: IssueParams) => Promise<void>;
};

const IssueTaskBarCreator: React.FC<Props> = ({ identifier, saveIssue }) => {
  const rowSelectOptions = useRowSelectOptions();
  const getTickByPosition = useGetTickByPosition();
  const computeMovedDate = useComputeMovedDate();
  const buildTaskBar = useBuildTaskBar(identifier);
  const selectTimeRange = useSelectTimeRange();

  const baseDateRef = React.useRef<Date>(new Date());
  const [bar, setBar] = React.useState<TaskBar | null>(null);

  const buildBarRange = ({ x }: MovedPosition): TimeBarRange => {
    const dates = [baseDateRef.current, computeMovedDate(baseDateRef.current, x)];

    if (dates[0] > dates[1]) {
      dates.reverse();
    }

    return {
      startDate: dates[0],
      endDate: dates[1],
    };
  };

  const [ref, isDragging] = useSimpleDrag({
    draggable: !rowSelectOptions.multiple && !rowSelectOptions.range,
    onDragStart: (_position, event) => {
      baseDateRef.current = getTickByPosition(event.offsetX);
    },
    onDrag: (position: MovedPosition) => {
      const range = buildBarRange(position);

      selectTimeRange({ ...range, showGuideline: true });
      setBar(
        buildTaskBar({
          resourceType: TaskBarResourceType.Issue,
          progressRate: 0,
          ...range,
        })
      );
    },
    onDragEnd: async (position: MovedPosition) => {
      const range = buildBarRange(position);

      try {
        await saveIssue({
          startDate: formatISODate(range.startDate),
          dueDate: formatISODate(range.endDate),
        });
      } finally {
        setBar(null);
        selectTimeRange(null);
      }
    },
  });

  const handleContextMenu = React.useCallback((event: React.MouseEvent) => {
    // macOSでcontrol + clickでcontextmenuが表示されるのを抑制する
    if (event.ctrlKey) {
      event.preventDefault();
    }
  }, []);

  return (
    <div
      ref={ref}
      className={cx(
        style,
        !isDragging && nonDraggingStyle,
        (rowSelectOptions.multiple || rowSelectOptions.range) && undraggableStyle
      )}
      onContextMenu={handleContextMenu}
    >
      {bar && <div className={barStyle} style={{ width: bar.width, left: bar.left }} />}
    </div>
  );
};

export default React.memo(IssueTaskBarCreator);

export const style = css`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 11;
  user-select: none;
`;

const nonDraggingStyle = css`
  cursor: ew-resize;
`;

const undraggableStyle = css`
  cursor: default;
`;

export const barStyle = css`
  position: absolute;
  top: 5px;
  height: 20px;
  border: 1px solid rgba(0, 0, 0, 0.4);
  background: rgba(0, 0, 0, 0.25);
`;
