import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import DeleteButton from '../DeleteButton';
import { offsetFrom, offsetTo } from '../__fixtures__';

vi.mock('@/models/setting', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useSettings: vi.fn().mockReturnValue({ baseURL: '/path/to/root' }),
}));

const deleteIssueRelationMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/resourceController', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useDeleteIssueRelation: vi.fn().mockReturnValue(deleteIssueRelationMock),
}));

const id = 123;

const renderForTesting = () => {
  return render(<DeleteButton id={id} offsetFrom={offsetFrom} offsetTo={offsetTo} marginX={222} />, {
    wrapper: ({ children }) => <svg>{children}</svg>,
  });
};

test('Render', () => {
  const { container } = renderForTesting();

  expect(container).toMatchSnapshot();
});

test('クリックでIssueRelationを削除', () => {
  const { container } = renderForTesting();
  const el = container.querySelector('g')!;

  expect(deleteIssueRelationMock).not.toBeCalled();

  fireEvent.click(el);

  expect(deleteIssueRelationMock).toBeCalledWith(id);
});
