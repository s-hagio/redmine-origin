import * as React from 'react';
import { useVersion } from '@/models/version';
import { VersionRow as VersionRowObject } from '@/features/row';
import { RowProps } from '../types';
import Row from '../Row';
import VersionRowContent from './VersionRowContent';

type Props = RowProps<VersionRowObject>;

const VersionRow: React.FC<Props> = ({ id, resourceId }) => {
  const version = useVersion(resourceId);

  if (!version) {
    return null;
  }

  return (
    <Row id={id}>
      <VersionRowContent id={id} version={version} />
    </Row>
  );
};

export default React.memo(VersionRow);
