import * as React from 'react';
import { render } from '@testing-library/react';
import { leafIssue } from '@/__fixtures__';
import Metadata from '../Metadata';

const useIssueMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/issue', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIssue: useIssueMock,
}));

const useMetadataVisibleMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/metadata', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useMetadataVisible: useMetadataVisibleMock,
}));

const issue = leafIssue;

beforeEach(() => {
  useIssueMock.mockReturnValue(issue);
});

test('Render: visible', () => {
  useMetadataVisibleMock.mockReturnValue(true);

  const { container } = render(<Metadata type="status">CHILD!</Metadata>);

  expect(container.textContent).toBe('CHILD!');
});

test('Render: invisible', () => {
  useMetadataVisibleMock.mockReturnValue(false);

  const { container } = render(<Metadata type="status">CHILD!</Metadata>);

  expect(container.textContent).toBe('');
});
