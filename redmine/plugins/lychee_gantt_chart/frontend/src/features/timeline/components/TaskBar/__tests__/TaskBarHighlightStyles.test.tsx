import * as React from 'react';
import { render } from '@testing-library/react';
import TaskBarHighlightStyles from '../TaskBarHighlightStyles';

const useIssueRelationCreationMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/issueRelation', () => ({
  useIssueRelationCreation: useIssueRelationCreationMock,
}));

const issueRelationCreation = {
  type: 'from',
  sourceIssueId: 1,
  targetIssueId: 2,
  isLocked: false,
} as const;

test('IssueRelationの作成中にTaskBarのハイライトを適用するstyle要素を作成', () => {
  useIssueRelationCreationMock.mockReturnValue({ ...issueRelationCreation });

  const { container } = render(<TaskBarHighlightStyles />);

  expect(container).toMatchSnapshot();
});

test('Render: { targetIssueId: null }', () => {
  useIssueRelationCreationMock.mockReturnValue({ ...issueRelationCreation, targetIssueId: null });

  const { container } = render(<TaskBarHighlightStyles />);

  expect(container).toMatchSnapshot();
});

test('IssueRelationの作成中じゃない場合は何も描画しない', () => {
  useIssueRelationCreationMock.mockReturnValueOnce(null);

  const { container } = render(<TaskBarHighlightStyles />);

  expect(container).toMatchSnapshot();
});
