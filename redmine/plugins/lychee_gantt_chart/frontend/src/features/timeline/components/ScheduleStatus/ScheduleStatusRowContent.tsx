import * as React from 'react';
import { css, cx } from '@linaria/core';
import { useIsClosedResourceRowId } from '@/features/row';
import { TaskBarResourceIdentifier, useTaskBar } from '../../models/taskBar';
import { useTodayTickEndPosition } from '../../models/tick';

type Props = {
  identifier: TaskBarResourceIdentifier;
};

const ScheduleStatusRowContent: React.FC<Props> = ({ identifier }) => {
  const taskBar = useTaskBar(identifier);
  const isClosed = useIsClosedResourceRowId(identifier);
  const todayTickEndPosition = useTodayTickEndPosition();

  if (!taskBar || isClosed) {
    return null;
  }

  const isOverdue = taskBar.right < todayTickEndPosition;
  const isBehindSchedule = !isOverdue && taskBar.left + taskBar.doneWidth < todayTickEndPosition;

  if (!isOverdue && !isBehindSchedule) {
    return null;
  }

  return <div className={cx(style, isBehindSchedule && behindScheduleStyle, isOverdue && overdueStyle)} />;
};

export default ScheduleStatusRowContent;

const style = css`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

export const behindScheduleStyle = css`
  background: rgb(245 214 196 / 35%);
`;

export const overdueStyle = css`
  background: rgb(237 105 105 / 15%);
`;
