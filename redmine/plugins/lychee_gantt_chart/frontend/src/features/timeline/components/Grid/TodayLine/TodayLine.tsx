import * as React from 'react';
import { useStepWidth } from '../../../models/timeAxis';
import { useTodayTickPosition } from '../../../models/tick';
import GridRect from './GridRect';
import GridLine from './GridLine';

const TodayLine: React.FC = () => {
  const stepWidth = useStepWidth();
  const position = useTodayTickPosition();

  if (stepWidth > 4) {
    return <GridRect left={position} />;
  }

  return <GridLine left={position} />;
};

export default React.memo(TodayLine);
