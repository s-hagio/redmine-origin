import * as React from 'react';
import { css } from '@linaria/core';
import { DayGridColumn } from '../../../models/grid';
import { nonWorkingDayColor } from './colors';

type Props = Pick<DayGridColumn, 'width' | 'isBeforeInvisibleDay' | 'isAfterInvisibleDay'> & {
  index: number;
};

const AdjacentToInvisibleDayGridColumn: React.FC<Props> = ({
  width: stepWidth,
  isBeforeInvisibleDay,
  isAfterInvisibleDay,
  index,
}) => {
  const width = stepWidth * 0.25;
  const offsetLeft = index * stepWidth;

  return (
    <>
      {isBeforeInvisibleDay && (
        <rect
          className={style}
          x={offsetLeft + stepWidth - width - 1}
          y="0"
          width={width + 1}
          height="100%"
        />
      )}

      {isAfterInvisibleDay && <rect className={style} x={offsetLeft} y="0" width={width} height="100%" />}
    </>
  );
};

export default React.memo(AdjacentToInvisibleDayGridColumn);

const style = css`
  fill: ${nonWorkingDayColor};
`;
