import * as React from 'react';
import { render } from '@testing-library/react';
import { useMainGridRow, useRefreshGrid } from '../../../models/grid';
import Grid from '../Grid';
import { dayGridRow, monthGridRow } from '../../../__fixtures__';

vi.mock('../DayGridRow', () => ({
  default: vi.fn((props) => `DayGridRow: ${JSON.stringify(props)}`),
}));
vi.mock('../UnitedGridRow', () => ({
  default: vi.fn((props) => `UnitedGridRow: ${JSON.stringify(props)}`),
}));
vi.mock('../TodayLine', () => ({
  default: vi.fn(() => `TodayLine`),
}));

vi.mock('@/features/row', () => ({
  useTotalRowHeight: vi.fn().mockReturnValue(1200),
}));

vi.mock('../../../models/tick', async () => ({
  ...(await vi.importActual<object>('../../../models/tick')),
  useTotalTickWidth: vi.fn().mockReturnValue(3000),
}));

vi.mock('../../../models/grid', async () => ({
  ...(await vi.importActual<object>('../../../models/grid')),
  useMainGridRow: vi.fn().mockReturnValue([]),
  useRefreshGrid: vi.fn(),
}));

const refreshGridMock = vi.fn();
vi.mocked(useRefreshGrid).mockReturnValue(refreshGridMock);

test('メイングリッドがnullの時はレンダリング中断', () => {
  vi.mocked(useMainGridRow).mockReturnValue(null);

  const result = render(<Grid />);

  expect(result.container.textContent).toBe('');
});

test('メイングリッドがUnitedGridRowの場合のレンダリング', () => {
  vi.mocked(useMainGridRow).mockReturnValue(monthGridRow);

  const result = render(<Grid />);

  expect(result.container).toMatchSnapshot();
});

test('メイングリッドがDayGridRowの場合のレンダリング', () => {
  vi.mocked(useMainGridRow).mockReturnValue(dayGridRow);

  const result = render(<Grid />);

  expect(result.container).toMatchSnapshot();
});

test('effectでrefreshGridを実行', () => {
  expect(refreshGridMock).not.toHaveBeenCalled();

  render(<Grid />);

  expect(refreshGridMock).toHaveBeenCalled();
});
