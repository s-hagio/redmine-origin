import * as React from 'react';
import { render } from '@testing-library/react';
import { ProgressLineOffset } from '../../../models/progressLine';
import ProgressLine from '../ProgressLine';

const useProgressLineCenterPositionMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/progressLine', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useProgressLineCenterPosition: useProgressLineCenterPositionMock,
}));

const centerPosition = 123;
const progressLineOffsets: ProgressLineOffset[] = [
  { top: 0, left: centerPosition },
  { top: 15, left: 10 },
  { top: 50, left: -60 },
  { top: 80, left: 20 },
];

beforeEach(() => {
  useProgressLineCenterPositionMock.mockReturnValue(centerPosition);
});

test('Render', () => {
  const { container } = render(<ProgressLine width={1200} offsets={progressLineOffsets} color="#abcabc" />);

  expect(container).toMatchSnapshot();
});
