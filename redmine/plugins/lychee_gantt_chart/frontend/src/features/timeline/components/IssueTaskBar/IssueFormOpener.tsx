import * as React from 'react';
import { css } from '@linaria/core';
import { IssueID } from '@/models/issue';
import { useOpenIssueForm } from '@/features/issueForm';

type Props = React.PropsWithChildren<{
  issueId: IssueID;
  disabled?: boolean;
}>;

const THRESHOLD_DISTANCE = 1;

const IssueFormOpener: React.FC<Props> = ({ issueId, disabled, children }) => {
  const openIssueForm = useOpenIssueForm();
  const isDisabledRef = React.useRef<boolean>(!!disabled);

  const handleMouseDown = React.useCallback(
    (event: React.MouseEvent<HTMLDivElement>) => {
      const startMousePosition = { x: event.screenX, y: event.screenY };

      const handleMouseMove = (event: MouseEvent) => {
        if (
          Math.abs(startMousePosition.x - event.screenX) >= THRESHOLD_DISTANCE ||
          Math.abs(startMousePosition.y - event.screenY) >= THRESHOLD_DISTANCE
        ) {
          isDisabledRef.current = true;
        }
      };

      const handleMouseUp = () => {
        document.removeEventListener('mousemove', handleMouseMove);
        document.removeEventListener('mouseup', handleMouseUp);

        if (!isDisabledRef.current) {
          openIssueForm({ id: issueId });
        }

        isDisabledRef.current = false;
      };

      document.addEventListener('mousemove', handleMouseMove);
      document.addEventListener('mouseup', handleMouseUp, { once: true });
    },
    [issueId, openIssueForm]
  );

  React.useEffect(() => {
    isDisabledRef.current = !!disabled;
  }, [disabled]);

  return (
    <div className={style} onMouseDown={handleMouseDown}>
      {children}
    </div>
  );
};

export default IssueFormOpener;

const style = css`
  position: relative;
  width: inherit;
  height: inherit;
  z-index: 10; // TODO: タイムライン内でのz-index管理
`;
