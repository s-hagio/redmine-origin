import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import IssueRelationCreationCanceller from '../IssueRelationCreationCanceller';

const useIssueRelationCreationMock = vi.hoisted(() => vi.fn());
const deactivateIssueRelationCreationMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/issueRelation', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIssueRelationCreation: useIssueRelationCreationMock,
  useDeactivateIssueRelationCreation: vi.fn().mockReturnValue(deactivateIssueRelationCreationMock),
}));

beforeEach(() => {
  useIssueRelationCreationMock.mockReturnValue({ type: 'from', sourceIssueId: 1, isLocked: false });
});

test('Render', () => {
  const ui = <IssueRelationCreationCanceller width={200} />;
  const { container, rerender } = render(ui);

  expect(container).toMatchSnapshot();

  useIssueRelationCreationMock.mockReturnValue(null);
  rerender(ui);

  expect(container).toMatchSnapshot();
});

test('クリックでIssueRelation作成モードを終了', () => {
  const { container } = render(<IssueRelationCreationCanceller width={200} />);
  const el = container.firstChild as HTMLDivElement;

  expect(deactivateIssueRelationCreationMock).not.toBeCalled();

  fireEvent.click(el);

  expect(deactivateIssueRelationCreationMock).toBeCalled();
});
