import * as React from 'react';
import { render } from '@testing-library/react';
import { mainProjectVersionRow, versionSharedByHierarchy } from '@/__fixtures__';
import VersionRow from '../VersionRow';

const useVersionMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/version', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useVersion: useVersionMock,
}));

vi.mock('../../Row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn((props) => React.createElement('row-mock', props)),
}));

const VersionRowContentSpy = vi.hoisted(() => vi.fn());

vi.mock('../VersionRowContent', () => ({
  default: vi.fn((props) => {
    VersionRowContentSpy(props);
    return 'VersionRowContent';
  }),
}));

const version = versionSharedByHierarchy;
const row = mainProjectVersionRow;

beforeEach(() => {
  useVersionMock.mockReturnValue(version);
});

test('Render', () => {
  const { container } = render(<VersionRow id={row.id} resourceId={row.resourceId} />);

  expect(container).toMatchSnapshot();
  expect(VersionRowContentSpy).toBeCalledWith({ id: row.id, version });
});
