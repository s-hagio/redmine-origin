import * as React from 'react';
import { render } from '@testing-library/react';
import DisplayOptionList from '../DisplayOptionList';

vi.mock('../IncludeNonWorkingDaysOption', () => ({
  default: vi.fn((props) => `IncludeNonWorkingDaysOption: ${JSON.stringify(props)}`),
}));

vi.mock('../ShowRelationLineOption', () => ({
  default: vi.fn((props) => `ShowRelationLineOption: ${JSON.stringify(props)}`),
}));

vi.mock('../ShowProgressLineOption', () => ({
  default: vi.fn((props) => `ShowProgressLineOption: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<DisplayOptionList />);

  expect(container).toMatchSnapshot();
});
