import * as React from 'react';
import { renderWithTranslation } from '@/test-utils';
import GridRow from '../GridRow';
import { monthGridRow } from '../../../__fixtures__';

test('Render', () => {
  const result = renderWithTranslation(<GridRow row={monthGridRow} />);

  expect(result.container).toMatchSnapshot();
});
