import * as React from 'react';
import { ProjectResourceIdentifier } from '@/models/coreResource';
import { Project } from '@/models/project';
import { ProjectBaselineBar } from '../Baseline';
import ProjectTaskBar from './ProjectTaskBar';

type Props = {
  id: ProjectResourceIdentifier;
  project: Project;
};

const ProjectRowContent: React.FC<Props> = ({ id, project }) => {
  return (
    <>
      <ProjectTaskBar identifier={id} project={project} />
      <ProjectBaselineBar identifier={id} />
    </>
  );
};

export default React.memo(ProjectRowContent);
