import * as React from 'react';
import { css } from '@linaria/core';
import IncludeNonWorkingDaysOption from './IncludeNonWorkingDaysOption';
import ShowRelationLineOption from './ShowRelationLineOption';
import ShowProgressLineOption from './ShowProgressLineOption';

const DisplayOptionList: React.FC = () => {
  return (
    <ul className={style}>
      <li>
        <IncludeNonWorkingDaysOption />
      </li>
      <li>
        <ShowRelationLineOption />
      </li>
      <li>
        <ShowProgressLineOption />
      </li>
    </ul>
  );
};

export default React.memo(DisplayOptionList);

export const style = css`
  margin: 0;
  padding: 0;
  list-style: none;
  white-space: nowrap;

  input[type='checkbox'] {
    margin: 0 3px 0 0;
    height: auto;
  }
`;
