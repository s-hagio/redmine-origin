import * as React from 'react';
import { render } from '@testing-library/react';
import { mainProjectRow, parentIssue, parentIssueRow, rootIssueRow } from '@/__fixtures__';
import IssueRow from '../IssueRow';

const useIssueMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/issue', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIssue: useIssueMock,
}));

vi.mock('../../Row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn((props) => React.createElement('row-mock', props)),
}));

const IssueRowContentSpy = vi.hoisted(() => vi.fn());

vi.mock('../IssueRowContent', () => ({
  default: vi.fn((props) => {
    IssueRowContentSpy(props);
    return 'IssueRowContent';
  }),
}));

const issue = parentIssue;
const row = parentIssueRow;
const subtreeRootIds = [mainProjectRow.id, rootIssueRow.id];

beforeEach(() => {
  useIssueMock.mockReturnValue(issue);
});

test('Render', () => {
  const { container } = render(
    <IssueRow id={row.id} resourceId={row.resourceId} subtreeRootIds={subtreeRootIds} />
  );

  expect(container).toMatchSnapshot();
  expect(IssueRowContentSpy).toBeCalledWith({ id: row.id, subtreeRootIds, issue });
});
