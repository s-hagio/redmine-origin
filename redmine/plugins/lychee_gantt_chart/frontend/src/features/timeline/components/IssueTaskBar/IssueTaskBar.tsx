import * as React from 'react';
import { Issue } from '@/models/issue';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { useRowSelectOptions } from '@/features/row';
import TaskBar from '../TaskBar';
import { extractIssueBarParams } from '../../models/taskBar';
import { IssueRelationTaskBarContent } from '.././IssueRelation';
import MetadataList from './MetadataList';
import IssueFormOpener from './IssueFormOpener';

type Props = React.PropsWithChildren<{
  identifier: IssueResourceIdentifier;
  issue: Issue;
  isDragging?: boolean;
}>;

const IssueTaskBar: React.FC<Props> = ({ identifier, issue, isDragging, children }) => {
  const rowSelectOptions = useRowSelectOptions();
  const barParams = extractIssueBarParams(issue);

  return (
    <TaskBar identifier={identifier} barParams={barParams}>
      <IssueFormOpener issueId={issue.id} disabled={rowSelectOptions.multiple || rowSelectOptions.range}>
        {children}
      </IssueFormOpener>

      {!isDragging && (
        <>
          <IssueRelationTaskBarContent issueId={issue.id} />
          <MetadataList issueId={issue.id} />
        </>
      )}
    </TaskBar>
  );
};

export default React.memo(IssueTaskBar);
