import * as React from 'react';
import { act } from '@testing-library/react';
import { renderWithRecoilHook } from '@/test-utils';
import CenteringExecutor from '../CenteringExecutor';
import { timelineViewportState } from '../../../models/viewport';
import {
  isNeedToCenteringState,
  scrollLeftState,
  useIsNeedToCentering,
  useScrollLeft,
  useSetNeedToCentering,
} from '../../../models/scroll';

const useCenterTickPositionMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/tick', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useCenterTickPosition: useCenterTickPositionMock,
}));

const centerTickPosition = 1200;

beforeEach(() => {
  useCenterTickPositionMock.mockReturnValue(centerTickPosition);
});

test('センタリングフラグがtrueならセンタリング', () => {
  const ui = <CenteringExecutor />;
  const { result, rerender } = renderWithRecoilHook(
    ui,
    () => ({
      setNeedToCentering: useSetNeedToCentering(),
      isNeedToCentering: useIsNeedToCentering(),
      scrollLeft: useScrollLeft(),
    }),
    {
      initializeState: ({ set }) => {
        set(isNeedToCenteringState, false);
        set(scrollLeftState, 1);
        set(timelineViewportState, (current) => ({ ...current, width: 500, height: 1000 }));
      },
    }
  );

  expect(result.current.scrollLeft).toBe(1);

  act(() => result.current.setNeedToCentering(true));

  rerender(ui);

  expect(result.current.scrollLeft).toBe(950); // centerTickPosition - viewport.width / 2
  expect(result.current.isNeedToCentering).toBe(false);
});
