import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { useTimeAxisOptionsState } from '../../../models/timeAxis';

const IncludeNonWorkingDaysOption: React.FC = () => {
  const { t } = useTranslation();

  const [{ includeNonWorkingDays }, setTimeAxisOptions] = useTimeAxisOptionsState();

  const handleChange = React.useCallback(
    ({ currentTarget }: React.ChangeEvent<HTMLInputElement>) => {
      setTimeAxisOptions((current) => ({
        ...current,
        includeNonWorkingDays: currentTarget.checked,
      }));
    },
    [setTimeAxisOptions]
  );

  return (
    <label>
      <input type="checkbox" checked={includeNonWorkingDays} onChange={handleChange} />
      {t('timeline.nonWorkingDays')}
    </label>
  );
};

export default IncludeNonWorkingDaysOption;
