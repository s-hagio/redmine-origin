import * as React from 'react';
import { isIssueResourceIdentifier } from '@/models/coreResource';
import { RowID } from '@/features/row';
import { TreeBlockRootID, useSetTreeBlockHighlight } from '../../models/treeBlock';

type Props = React.PropsWithChildren<
  React.ComponentProps<'div'> & {
    rowId: RowID;
    subtreeRootIds: RowID[];
  }
>;

const TreeBlockItemContainer: React.FC<Props> = ({
  rowId,
  subtreeRootIds,
  onMouseEnter,
  onMouseLeave,
  children,
  ...props
}) => {
  const sortedTreeBlockRootIds = React.useMemo<TreeBlockRootID[]>(() => {
    if (!isIssueResourceIdentifier(rowId)) {
      return [];
    }

    return [rowId, ...subtreeRootIds.filter(isIssueResourceIdentifier).reverse()];
  }, [rowId, subtreeRootIds]);

  const setTreeBlockHighlight = useSetTreeBlockHighlight(sortedTreeBlockRootIds);

  const handleMouseEnter = React.useCallback(
    (event: React.MouseEvent<HTMLDivElement>) => {
      onMouseEnter?.(event);
      setTreeBlockHighlight(true);
    },
    [onMouseEnter, setTreeBlockHighlight]
  );

  const handleMouseLeave = React.useCallback(
    (event: React.MouseEvent<HTMLDivElement>) => {
      onMouseLeave?.(event);
      setTreeBlockHighlight(false);
    },
    [onMouseLeave, setTreeBlockHighlight]
  );

  return (
    <div {...props} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
      {children}
    </div>
  );
};

export default React.memo(TreeBlockItemContainer);
