import * as React from 'react';
import { render } from '@testing-library/react';
import { getIssueResourceIdentifier } from '@/models/coreResource';
import { leafIssue } from '@/__fixtures__';
import { issueBarParams } from '../../../__fixtures__';
import IssueTaskBar from '../IssueTaskBar';

const useRowSelectOptionsMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useRowSelectOptions: useRowSelectOptionsMock,
}));

const extractIssueBarParamsMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/taskBar', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  extractIssueBarParams: extractIssueBarParamsMock,
}));

const TaskBarSpy = vi.hoisted(() => vi.fn());

vi.mock('../../TaskBar', () => ({
  default: vi.fn(({ children, ...props }) => {
    TaskBarSpy(props);
    return React.createElement('task-bar', null, children);
  }),
}));

vi.mock('../../IssueRelation', () => ({
  IssueRelationTaskBarContent: vi.fn((props) => `IssueRelationTaskBarContent: ${JSON.stringify(props)}`),
}));

const IssueFormOpenerSpy = vi.hoisted(() => vi.fn());

vi.mock('../IssueFormOpener', () => ({
  default: vi.fn(({ children, ...props }) => {
    IssueFormOpenerSpy(props);
    return React.createElement('issue-form-opener', null, children);
  }),
}));

vi.mock('../MetadataList', () => ({
  default: vi.fn((props) => `MetadataList: ${JSON.stringify(props)}`),
}));

const issue = leafIssue;
const identifier = getIssueResourceIdentifier(issue.id);
const barParams = issueBarParams;

beforeEach(() => {
  useRowSelectOptionsMock.mockReturnValue({ multiple: false, range: false });
  extractIssueBarParamsMock.mockReturnValue(barParams);
});

test('Render', () => {
  const { container } = render(
    <IssueTaskBar identifier={identifier} issue={issue} isDragging={false}>
      CHILD!!
    </IssueTaskBar>
  );

  expect(container).toMatchSnapshot();
  expect(IssueFormOpenerSpy).toBeCalledWith({ issueId: issue.id, disabled: false });
  expect(TaskBarSpy).toBeCalledWith({ identifier, barParams });
});

test('Render: dragging', () => {
  const { container } = render(
    <IssueTaskBar identifier={identifier} issue={issue} isDragging={true}>
      CHILD!!
    </IssueTaskBar>
  );

  expect(container).toMatchSnapshot();
});

test.each([
  { multiple: true, range: false },
  { multiple: false, range: true },
])('ShiftキーやCtrl(Meta)キーなどで行選択している時はIssueFormOpenerを無効化する', (opts) => {
  useRowSelectOptionsMock.mockReturnValue(opts);

  render(
    <IssueTaskBar identifier={identifier} issue={issue}>
      CHILD!!
    </IssueTaskBar>
  );

  expect(IssueFormOpenerSpy).toBeCalledWith({ issueId: issue.id, disabled: true });
});
