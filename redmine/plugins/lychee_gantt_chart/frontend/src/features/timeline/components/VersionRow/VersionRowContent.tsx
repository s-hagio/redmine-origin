import * as React from 'react';
import { Version } from '@/models/version';
import { VersionResourceIdentifier } from '@/models/coreResource';
import { VersionBaselineBar } from '../Baseline';
import VersionTaskBar from './VersionTaskBar';

type Props = {
  id: VersionResourceIdentifier;
  version: Version;
};

const VersionRowContent: React.FC<Props> = ({ id, version }) => {
  return (
    <>
      <VersionTaskBar identifier={id} version={version} />
      <VersionBaselineBar identifier={id} />
    </>
  );
};

export default React.memo(VersionRowContent);
