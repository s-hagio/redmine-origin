import * as React from 'react';
import { render } from '@testing-library/react';
import { mainProject, mainProjectRow } from '@/__fixtures__';
import ProjectRowContent from '../ProjectRowContent';

vi.mock('../../Baseline', () => ({
  ProjectBaselineBar: vi.fn((props) => `ProjectBaselineBar: ${JSON.stringify(props)}`),
}));

const ProjectTaskBarSpy = vi.hoisted(() => vi.fn());

vi.mock('../ProjectTaskBar', () => ({
  default: vi.fn((props) => {
    ProjectTaskBarSpy(props);
    return 'ProjectTaskBar';
  }),
}));

const project = mainProject;
const row = mainProjectRow;

test('Render', () => {
  const { container } = render(<ProjectRowContent id={row.id} project={project} />);

  expect(container).toMatchSnapshot();
  expect(ProjectTaskBarSpy).toBeCalledWith({ identifier: row.id, project });
});
