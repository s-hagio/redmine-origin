import * as React from 'react';
import { css } from '@linaria/core';
import ConfigDialog, { dialogStyle } from '../ConfigDialog';
import ButtonList from './ButtonList';

const ConfigMenu: React.FC = () => {
  return (
    <div className={style}>
      <ButtonList />
      <ConfigDialog />
    </div>
  );
};

export default React.memo(ConfigMenu);

export const style = css`
  .${dialogStyle} {
    position: absolute;
    top: 0;
    right: 0;
    z-index: 1;
  }
`;
