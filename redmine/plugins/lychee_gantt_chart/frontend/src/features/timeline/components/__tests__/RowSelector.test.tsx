import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import RowSelector from '../RowSelector';

const useRowSelectOptionsMock = vi.hoisted(() => vi.fn());
const selectRowByOffsetMock = vi.hoisted(() => vi.fn());
const skipDeselectRowMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useRowSelectOptions: useRowSelectOptionsMock,
  useSelectRowByOffset: vi.fn().mockReturnValue(selectRowByOffsetMock),
  useSkipDeselectRow: vi.fn().mockReturnValue(skipDeselectRowMock),
}));

const offsetTop = 100;
const containerElement = document.createElement('div');
vi.spyOn(containerElement, 'getBoundingClientRect').mockReturnValue({ top: offsetTop } as DOMRect);

beforeEach(() => {
  useRowSelectOptionsMock.mockReturnValue({ multiple: false, range: false });
});

test.each([
  { multiple: true, range: false },
  { multiple: false, range: true },
])('コンテナ要素をクリック（MouseDown）した時に行選択オプションが有効なら行選択処理', (opts) => {
  useRowSelectOptionsMock.mockReturnValue(opts);

  render(<RowSelector containerRef={{ current: containerElement }} />);

  expect(selectRowByOffsetMock).not.toBeCalled();

  fireEvent.mouseDown(containerElement, { clientY: 123 });

  expect(selectRowByOffsetMock).toBeCalledWith(23);
  expect(skipDeselectRowMock).not.toBeCalled();
});

test('コンテナ要素クリック時にターゲット要素が特定の要素なら行選択処理', () => {
  render(<RowSelector containerRef={{ current: containerElement }} />);

  const event = new MouseEvent('mousedown', { clientY: 150 });
  vi.spyOn(event, 'target', 'get').mockReturnValue(document.createElement('svg'));
  containerElement.dispatchEvent(event);

  expect(selectRowByOffsetMock).toBeCalledWith(50);
  expect(skipDeselectRowMock).not.toBeCalled();
});

test('条件にマッチしない時は行選択解除のスキップ処理', () => {
  render(<RowSelector containerRef={{ current: containerElement }} />);

  expect(skipDeselectRowMock).not.toBeCalled();

  fireEvent.mouseDown(containerElement, { clientY: 999 });

  expect(selectRowByOffsetMock).not.toBeCalled();
  expect(skipDeselectRowMock).toBeCalled();
});
