import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { MetadataType, MetadataVisibility, useMetadataVisibilityState } from '../../../models/metadata';

type Props = {
  type: MetadataType;
  visibility: MetadataVisibility;
};

const MetadataVisibilityRadioButton: React.FC<Props> = ({ type, visibility }) => {
  const { t } = useTranslation();

  const [currentVisibility, setVisibility] = useMetadataVisibilityState(type);

  const handleChange = React.useCallback(() => {
    setVisibility(visibility);
  }, [setVisibility, visibility]);

  return (
    <label
      className={style}
      aria-label={`${t(`timeline.metadataTypes.${type}`)}: ${t(
        `timeline.metadataVisibilities.${visibility}`
      )}`}
    >
      <input
        type="radio"
        name={type}
        value={visibility}
        checked={visibility === currentVisibility}
        onChange={handleChange}
      />
    </label>
  );
};

export default MetadataVisibilityRadioButton;

const style = css`
  display: flex;
  justify-content: center;
`;
