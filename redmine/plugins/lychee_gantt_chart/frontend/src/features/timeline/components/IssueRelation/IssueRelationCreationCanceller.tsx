import * as React from 'react';
import { css } from '@linaria/core';
import { useIssueRelationCreation, useDeactivateIssueRelationCreation } from '../../models/issueRelation';

type Props = {
  width: number;
};

const IssueRelationCreationCanceller: React.FC<Props> = ({ width }) => {
  const ref = React.useRef<HTMLDivElement>(null);

  const issueRelationCreation = useIssueRelationCreation();
  const deactivateIssueRelationCreation = useDeactivateIssueRelationCreation();

  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLDivElement>) => {
      if (event.target === ref.current) {
        event.preventDefault();
        deactivateIssueRelationCreation();
      }
    },
    [deactivateIssueRelationCreation]
  );

  if (!issueRelationCreation) {
    return null;
  }

  return <div ref={ref} className={style} style={{ width }} onClick={handleClick} />;
};

export default React.memo(IssueRelationCreationCanceller);

const style = css`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 10; // TODO: z-index整理
`;
