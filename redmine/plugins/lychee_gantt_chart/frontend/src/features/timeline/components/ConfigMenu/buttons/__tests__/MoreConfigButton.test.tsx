import * as React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithTranslation } from '@/test-utils';
import MoreConfigButton from '../MoreConfigButton';
import { MORE_CONFIG_DIALOG_ID } from '../../../ConfigDialog';

const openDialogMock = vi.hoisted(() => vi.fn());
const useOpenDialogMock = vi.hoisted(() => vi.fn().mockReturnValue(openDialogMock));

vi.mock('@/features/dialog', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useOpenDialog: useOpenDialogMock,
}));

test('Render', () => {
  const { container } = renderWithTranslation(<MoreConfigButton />);

  expect(container).toMatchSnapshot();
  expect(useOpenDialogMock).toBeCalledWith(MORE_CONFIG_DIALOG_ID);
});

test('クリックでダイアログを開く', () => {
  const { getByRole } = renderWithTranslation(<MoreConfigButton />);
  const button = getByRole('button');

  expect(openDialogMock).not.toBeCalled();

  fireEvent.click(button);

  expect(openDialogMock).toBeCalled();
});
