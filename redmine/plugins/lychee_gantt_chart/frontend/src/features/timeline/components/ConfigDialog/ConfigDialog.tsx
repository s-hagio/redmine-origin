import * as React from 'react';
import { css } from '@linaria/core';
import { Dialog, useCloseDialog } from '@/features/dialog';
import { NakedButton } from '@/components/Button';
import { Icon } from '@/components/Icon';
import { MORE_CONFIG_DIALOG_ID } from './constants';
import DisplayOptionList, { displayOptionListStyle } from './DisplayOptionList';
import MetadataOptionList from './MetadataOptionList';

const ConfigDialog: React.FC = () => {
  const close = useCloseDialog(MORE_CONFIG_DIALOG_ID);

  return (
    <Dialog id={MORE_CONFIG_DIALOG_ID} className={style}>
      <NakedButton type="button" className={closeButtonStyle} onClick={close}>
        <Icon name="close" size={24} />
      </NakedButton>

      <DisplayOptionList />
      <MetadataOptionList />
    </Dialog>
  );
};

export default React.memo(ConfigDialog);

export const style = css`
  display: flex;
  padding: 1rem 3rem 1rem 1.5rem;
  border-radius: 4px;
  background: #fff;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);

  .${displayOptionListStyle} {
    margin-right: 2rem;
  }
`;

const closeButtonStyle = css`
  position: absolute;
  top: 0.5rem;
  right: 0.5rem;
`;
