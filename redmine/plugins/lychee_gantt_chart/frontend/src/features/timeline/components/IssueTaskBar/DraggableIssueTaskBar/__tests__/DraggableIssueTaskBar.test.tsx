import * as React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithRecoil } from '@/test-utils';
import { addDays, formatISODate } from '@/lib/date';
import { IssueID } from '@/models/issue';
import { getIssueResourceIdentifier } from '@/models/coreResource';
import { leafIssue } from '@/__fixtures__';
import {
  ComputeMovedRange,
  extractIssueBarParams,
  IssueTaskBarDraggability,
} from '../../../../models/taskBar';
import DraggableIssueTaskBar from '../DraggableIssueTaskBar';

const useIssueTaskBarDraggabilityMock = vi.hoisted(() => vi.fn<[IssueID], IssueTaskBarDraggability>());
const computeMovedRangeMock = vi.hoisted(() =>
  vi.fn<Parameters<ComputeMovedRange>, ReturnType<ComputeMovedRange>>()
);
const bulkUpdateTaskBarsMock = vi.hoisted(() => vi.fn());
const commitTaskBarChangesMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/taskBar', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIssueTaskBarDraggability: useIssueTaskBarDraggabilityMock,
  useComputeMovedRange: vi.fn().mockReturnValue(computeMovedRangeMock),
  useBulkUpdateTaskBars: vi.fn().mockReturnValue(bulkUpdateTaskBarsMock),
  useCommitTaskBarChanges: vi.fn().mockReturnValue(commitTaskBarChangesMock),
}));

const selectTimeRangeMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/timeBar', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useSelectTimeRange: vi.fn().mockReturnValue(selectTimeRangeMock),
}));

const IssueTaskBarSpy = vi.hoisted(() => vi.fn());

vi.mock('../../IssueTaskBar', () => ({
  default: ({ children, ...props }: React.PropsWithChildren) => {
    IssueTaskBarSpy(props);
    return React.createElement('issue-task-bar', null, children);
  },
}));

vi.mock('../RangeDragHandle', () => ({
  default: React.forwardRef<HTMLDivElement>(function RangeDragHandle(props, ref) {
    return (
      <div ref={ref} data-testid="rangeDragHandle">
        {JSON.stringify(props)}
      </div>
    );
  }),
}));

vi.mock('../DateDragHandle', () => ({
  default: React.forwardRef<HTMLDivElement, { type: string }>(function DateDragHandle(props, ref) {
    return (
      <div ref={ref} data-testid={`dateDragHandle-${props.type}`}>
        {JSON.stringify(props)}
      </div>
    );
  }),
}));

const issue = { ...leafIssue, startDate: '2023-09-10', dueDate: '2023-09-15' };
const identifier = getIssueResourceIdentifier(issue.id);
const initialParams = extractIssueBarParams(issue)!;

const baseDraggability: IssueTaskBarDraggability = {
  isDraggable: true,
  isRangeDraggable: true,
  isStartDateDraggable: true,
  isDueDateDraggable: true,
};

beforeEach(() => {
  useIssueTaskBarDraggabilityMock.mockReturnValue(baseDraggability);
});

const ui = <DraggableIssueTaskBar identifier={identifier} issue={issue} initialParams={initialParams} />;

test('Draggabilityに対応したドラッグハンドルを表示する', () => {
  const { rerender, container } = renderWithRecoil(ui);

  expect(useIssueTaskBarDraggabilityMock).toBeCalledWith(issue);
  expect(container).toMatchSnapshot('すべて表示する');

  useIssueTaskBarDraggabilityMock.mockReturnValue({ ...baseDraggability, isRangeDraggable: false });
  rerender(ui);

  expect(container).toMatchSnapshot('RangeDragHandleが表示されない');

  useIssueTaskBarDraggabilityMock.mockReturnValue({ ...baseDraggability, isStartDateDraggable: false });
  rerender(ui);

  expect(container).toMatchSnapshot('DateDragHandle[type="start"]が表示されない');

  useIssueTaskBarDraggabilityMock.mockReturnValue({ ...baseDraggability, isDueDateDraggable: false });
  rerender(ui);

  expect(container).toMatchSnapshot('DateDragHandle[type="end"]が表示されない');
});

describe('drag', () => {
  beforeEach(() => {
    // 移動値をそのまま移動日数としてaddDaysする
    computeMovedRangeMock.mockImplementation((range, values) => {
      return {
        startDate: addDays(range.startDate, values.start ?? values.both ?? 0),
        endDate: addDays(range.endDate, values.end ?? values.both ?? 0),
      };
    });
  });

  test.each([
    { type: 'both', testId: 'rangeDragHandle', label: '開始日／期日' },
    { type: 'start', testId: 'dateDragHandle-start', label: '開始日' },
    { type: 'end', testId: 'dateDragHandle-end', label: '期日' },
  ])('$labelを変更する', ({ type, testId }) => {
    const { getByTestId, rerender } = renderWithRecoil(ui);
    const dragHandle = getByTestId(testId);

    // ドラッグ開始
    fireEvent.pointerDown(dragHandle, { clientX: 100, clientY: 0, primary: true });

    // ドラッグ中
    [
      { clientX: 101, movedValues: { [type]: 1 } },
      { clientX: 93, movedValues: { [type]: -7 } },
      { clientX: 125, movedValues: { [type]: 25 } },
    ].forEach(({ clientX, movedValues }) => {
      fireEvent.pointerMove(dragHandle, { clientX, clientY: 0 });
      rerender(ui);

      const rangeAtDrag = computeMovedRangeMock(initialParams, movedValues);

      expect(IssueTaskBarSpy).lastCalledWith({
        identifier,
        issue: {
          ...issue,
          startDate: formatISODate(rangeAtDrag.startDate),
          dueDate: formatISODate(rangeAtDrag.endDate),
        },
        isDragging: true,
      });

      expect(selectTimeRangeMock).lastCalledWith({ ...rangeAtDrag, showGuideline: true, lock: true });
      expect(bulkUpdateTaskBarsMock).lastCalledWith(movedValues);
    });

    // ドラッグ終了
    fireEvent.pointerUp(dragHandle, { clientX: 190, clientY: 0 });

    expect(commitTaskBarChangesMock).toBeCalled();
    expect(selectTimeRangeMock).lastCalledWith(null, { unlock: true });
  });
});
