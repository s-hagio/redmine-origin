import * as React from 'react';
import { render } from '@testing-library/react';
import { issueResourceIdentifier } from '@/__fixtures__';
import ScheduleStatusRowContent, { behindScheduleStyle, overdueStyle } from '../ScheduleStatusRowContent';

const useIsClosedResourceRowIdMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIsClosedResourceRowId: useIsClosedResourceRowIdMock,
}));

const useTaskBarMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/taskBar', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useTaskBar: useTaskBarMock,
}));

const useTodayTickEndPositionMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/tick', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useTodayTickEndPosition: useTodayTickEndPositionMock,
}));

beforeEach(() => {
  useIsClosedResourceRowIdMock.mockReturnValue(false);
  useTodayTickEndPositionMock.mockReturnValue(200);
});

const identifier = issueResourceIdentifier;

describe('Just as planned', () => {
  beforeEach(() => {
    useTaskBarMock.mockReturnValue({ left: 150, right: 250, doneWidth: 50 });
  });

  test('何も表示しない', () => {
    const { container } = render(<ScheduleStatusRowContent identifier={identifier} />);

    expect(container.firstChild).toBeNull();
  });
});

describe('Behind schedule', () => {
  beforeEach(() => {
    useTaskBarMock.mockReturnValue({ left: 149, right: 250, doneWidth: 50 });
  });

  test('Render with behind schedule style', () => {
    const { container } = render(<ScheduleStatusRowContent identifier={identifier} />);
    const el = container.firstChild as HTMLElement;

    expect(el.classList.contains(behindScheduleStyle)).toBe(true);
    expect(el.classList.contains(overdueStyle)).toBe(false);
  });

  test('完了している場合は何も表示しない', () => {
    useIsClosedResourceRowIdMock.mockReturnValue(true);

    const { container } = render(<ScheduleStatusRowContent identifier={identifier} />);

    expect(container.firstChild).toBeNull();
  });
});

describe('Overdue', () => {
  beforeEach(() => {
    useTaskBarMock.mockReturnValue({ left: 100, right: 199, doneWidth: 0 });
  });

  test('Render with overdue style', () => {
    const { container } = render(<ScheduleStatusRowContent identifier={identifier} />);
    const el = container.firstChild as HTMLElement;

    expect(el.classList.contains(behindScheduleStyle)).toBe(false);
    expect(el.classList.contains(overdueStyle)).toBe(true);
  });

  test('完了している場合は何も表示しない', () => {
    useIsClosedResourceRowIdMock.mockReturnValue(true);

    const { container } = render(<ScheduleStatusRowContent identifier={identifier} />);

    expect(container.firstChild).toBeNull();
  });
});
