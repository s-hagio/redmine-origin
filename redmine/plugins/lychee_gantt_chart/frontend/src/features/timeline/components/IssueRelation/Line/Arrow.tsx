import * as React from 'react';
import { css } from '@linaria/core';
import { OffsetTo } from './types';

type Props = {
  offsetTo: OffsetTo;
  color: string;
};

export const width = 5;
export const height = 8;

const Arrow: React.FC<Props> = ({ offsetTo, color }) => {
  const c = { x: offsetTo.left, y: offsetTo.middle };
  const paths = [`M ${c.x} ${c.y}`, `l -${width} -${height / 2}`, `v${height}z`];

  return <path className={style} d={paths.join('')} fill={color} />;
};

export default Arrow;

const style = css`
  stroke: none;
  stroke-linecap: butt;
  stroke-linejoin: miter;
`;
