import * as React from 'react';
import { render } from '@testing-library/react';
import { useVisibleRows } from '@/features/row';
import {
  newStatusGroupRow,
  placeholderRow,
  rootIssueRow,
  rootProjectRow,
  rootProjectVersionRow,
  versionIssueRow,
} from '@/__fixtures__';
import RowList from '../RowList';

vi.mock('@/features/row', async () => ({
  ...(await vi.importActual<object>('@/features/row/types')),
  useVisibleRows: vi.fn(),
  useTotalRowHeight: vi.fn().mockReturnValue(999),
}));

vi.mock('../../models/tick', () => ({
  useTotalTickWidth: vi.fn().mockReturnValue(123),
}));

vi.mock('../ProjectRow', () => ({
  ProjectRow: vi.fn((props) => `ProjectRow: ${JSON.stringify(props)}`),
}));
vi.mock('../VersionRow', () => ({
  VersionRow: vi.fn((props) => `VersionRow: ${JSON.stringify(props)}`),
}));
vi.mock('../IssueRow', () => ({
  IssueRow: vi.fn((props) => `IssueRow: ${JSON.stringify(props)}`),
}));
vi.mock('../GroupRow', () => ({
  GroupRow: vi.fn((props) => `GroupRow: ${JSON.stringify(props)}`),
}));
vi.mock('../PlaceholderRow', () => ({
  PlaceholderRow: vi.fn((props) => `PlaceholderRow: ${JSON.stringify(props)}`),
}));

vi.mocked(useVisibleRows).mockReturnValue([
  rootProjectRow,
  rootIssueRow,
  rootProjectVersionRow,
  versionIssueRow,
  newStatusGroupRow,
  placeholderRow,
]);

test('Render', () => {
  const result = render(<RowList />);

  expect(result.container).toMatchSnapshot();
});
