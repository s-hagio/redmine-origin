import * as React from 'react';
import { render } from '@testing-library/react';
import TimelineHeader from '../Header';

vi.mock('../../../models/tick', () => ({
  useTotalTickWidth: vi.fn().mockReturnValue(123),
}));

vi.mock('../../../models/scroll', () => ({
  useScrollLeft: vi.fn().mockReturnValue(999),
}));

vi.mock('../../../models/grid', () => ({
  useGridRows: vi.fn().mockReturnValue([{ type: 'month' }, { type: 'day' }]),
}));

vi.mock('../../ConfigMenu', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  default: vi.fn((props) => `ConfigMenu: ${JSON.stringify(props)}`),
}));

vi.mock('../../SelectedTimeRange', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  SelectedTimeRange: vi.fn((props) => `SelectedTimeRange: ${JSON.stringify(props)}`),
}));

vi.mock('../GridRow', () => ({
  default: vi.fn((props) => `GridRow: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const result = render(<TimelineHeader />);

  expect(result.container).toMatchSnapshot();
});

test.todo('Headerのサイズが変わった時にSelectedTimeRangeに渡すwidthを更新');
