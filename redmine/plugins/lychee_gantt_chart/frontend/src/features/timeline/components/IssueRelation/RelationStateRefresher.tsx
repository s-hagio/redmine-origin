import * as React from 'react';
import { useRefreshIssueRelations } from '@/models/issueRelation';

const RelationStateRefresher: React.FC = () => {
  const refresh = useRefreshIssueRelations();
  React.useEffect(refresh, [refresh]);

  return null;
};

export default RelationStateRefresher;
