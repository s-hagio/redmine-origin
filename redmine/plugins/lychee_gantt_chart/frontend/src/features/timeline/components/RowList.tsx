import * as React from 'react';
import { css } from '@linaria/core';
import { pick } from '@/utils/object';
import { RowResourceType, useTotalRowHeight, useVisibleRows } from '@/features/row';
import { useTotalTickWidth } from '../models/tick';
import { ProjectRow } from './ProjectRow';
import { VersionRow } from './VersionRow';
import { IssueRow } from './IssueRow';
import { GroupRow } from './GroupRow';
import { PlaceholderRow } from './PlaceholderRow';

const RowList: React.FC = () => {
  const rows = useVisibleRows();
  const width = useTotalTickWidth();
  const height = useTotalRowHeight();

  return (
    <div className={style} style={{ width, height }}>
      {rows.map((row) => {
        switch (row.resourceType) {
          case RowResourceType.Project:
            return <ProjectRow key={row.id} id={row.id} resourceId={row.resourceId} />;
          case RowResourceType.Version:
            return <VersionRow key={row.id} id={row.id} resourceId={row.resourceId} />;
          case RowResourceType.Issue:
            return <IssueRow key={row.id} id={row.id} {...pick(row, ['resourceId', 'subtreeRootIds'])} />;
          case RowResourceType.Group:
            return <GroupRow key={row.id} id={row.id} resourceId={row.resourceId} />;
          case RowResourceType.Placeholder:
            return <PlaceholderRow key={row.id} id={row.id} />;
        }
      })}
    </div>
  );
};

export default RowList;

const style = css`
  position: absolute;
  top: 0;
  left: 0;
  overflow: hidden;
`;
