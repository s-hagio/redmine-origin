import * as React from 'react';
import { useRecoilValue } from 'recoil';
import { renderWithRecoil, renderWithRecoilHook } from '@/test-utils';
import Timeline from '../Timeline';
import { useRefreshTicks } from '../../models/tick';
import { useStepWidth } from '../../models/timeAxis';
import { isNeedToCenteringState } from '../../models/scroll';
import { timelineViewportState } from '../../models/viewport';

vi.mock('react', async () => ({
  ...(await vi.importActual<object>('react')),
  useRef: vi.fn().mockReturnValue({ current: null }),
}));

vi.mock('@/features/row', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useTotalRowHeight: vi.fn().mockReturnValue(888),
}));

vi.mock('../../models/tick', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useRefreshTicks: vi.fn().mockReturnValue(vi.fn()),
  useTotalTickWidth: vi.fn().mockReturnValue(999),
}));

vi.mock('../../models/timeAxis', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useStepWidth: vi.fn().mockReturnValue(2),
}));

vi.mock('../TaskBar', () => ({
  TaskBarsRefresher: vi.fn((props) => `TaskBarsRefresher: ${JSON.stringify(props)}`),
  TaskBarHighlightStyles: vi.fn(() => 'TaskBarHighlightStyles'),
}));

vi.mock('../Scrollable', () => ({
  ScrollableContainer: vi.fn(({ children, ...props }) =>
    React.createElement('scrollable-container', props, children)
  ),
}));

vi.mock('../Grid', () => ({ default: vi.fn(() => 'Grid') }));
vi.mock('../RowList', () => ({ default: vi.fn(() => 'RowList') }));
vi.mock('../RowSelector', () => ({ default: vi.fn(() => 'RowSelector') }));

vi.mock('../IssueRelation', () => ({
  RelationLineContainer: vi.fn((props) => `RelationLineContainer: ${JSON.stringify(props)}`),
}));

vi.mock('../ProgressLine', () => ({
  ProgressLineContainer: vi.fn((props) => `ProgressLineContainer: ${JSON.stringify(props)}`),
}));

vi.mock('../Baseline', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  BaselineStatesRefresher: vi.fn((props) => `BaselineStatesRefresher: ${JSON.stringify(props)}`),
}));

vi.mock('../SelectedTimeRange', () => ({
  TimeRangeGuideline: vi.fn((props) => `TimeRangeGuideline: ${JSON.stringify(props)}`),
}));

vi.mock('../TreeBlock', () => ({
  TreeBlockContainer: vi.fn((props) => `TreeBlockContainer: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const result = renderWithRecoil(<Timeline />);

  expect(result.container).toMatchSnapshot();
});

test('自要素幅とStep幅からTick数を計算してrefreshTicks()を実行', () => {
  const elementMock = { clientWidth: 1600 };
  const refMock = { current: null };
  const refreshTicksMock = vi.fn();

  Object.defineProperty(refMock, 'current', {
    set: () => vi.fn(),
    get: () => elementMock,
  });

  vi.mocked(React.useRef).mockReturnValue(refMock);
  vi.mocked(useStepWidth).mockReturnValue(1);
  vi.mocked(useRefreshTicks).mockReturnValue(refreshTicksMock);

  renderWithRecoil(<Timeline />);

  expect(refreshTicksMock).toHaveBeenLastCalledWith(3200); // clientWidthの2倍 ÷ stepWidth
});

describe('when layout effect occurs', () => {
  test('TimelineViewportStateに自要素のwidthをセット', () => {
    const { result } = renderWithRecoilHook(<Timeline />, () => ({
      viewport: useRecoilValue(timelineViewportState),
    }));

    expect(result.current.viewport.width).toBe(1600);
  });

  test('センタリングフラグを立てる', () => {
    const { result } = renderWithRecoilHook(
      <Timeline />,
      () => ({
        isNeedToCentering: useRecoilValue(isNeedToCenteringState),
      }),
      {
        initializeState: ({ set }) => {
          set(isNeedToCenteringState, false);
        },
      }
    );

    expect(result.current.isNeedToCentering).toBe(true);
  });
});

test.todo('要素のサイズが変わった時にviewportStateを更新');
