import * as React from 'react';
import { css } from '@linaria/core';
import { useTotalRowHeight } from '@/features/row';
import { borderColor } from '@/styles';
import { useStepWidth } from '../models/timeAxis';
import { useRefreshTicks, useTotalTickWidth } from '../models/tick';
import { useSetNeedToCentering } from '../models/scroll';
import { useSetTimelineViewport } from '../models/viewport';
import Grid from './Grid';
import RowList from './RowList';
import RowSelector from './RowSelector';
import { TaskBarsRefresher, TaskBarHighlightStyles } from './TaskBar';
import { ScrollableContainer } from './Scrollable';
import { RelationLineContainer } from './IssueRelation';
import { ProgressLineContainer } from './ProgressLine';
import { TimeRangeGuideline } from './SelectedTimeRange';
import { TreeBlockContainer } from './TreeBlock';
import { BaselineProgressLineContainer, BaselineStatesRefresher } from './Baseline';

const MIN_GRID_CONTAINER_WIDTH = 300;

const Timeline: React.FC = () => {
  const elementRef = React.useRef<HTMLDivElement>(null);

  const stepWidth = useStepWidth();
  const refreshTicks = useRefreshTicks();
  const setNeedToCentering = useSetNeedToCentering();

  const width = useTotalTickWidth();
  const height = useTotalRowHeight();

  const setViewport = useSetTimelineViewport();

  React.useLayoutEffect(() => {
    if (!elementRef.current) {
      return;
    }

    const width = elementRef.current.clientWidth;
    setViewport((current) => ({ ...current, width }));

    refreshTicks(Math.round((Math.max(width, MIN_GRID_CONTAINER_WIDTH) * 2) / stepWidth));
    setNeedToCentering(true);
  }, [refreshTicks, setNeedToCentering, setViewport, stepWidth]);

  React.useEffect(() => {
    const el = elementRef.current;
    if (!el) return;

    const resizeObserver = new ResizeObserver(([entry]) => {
      setViewport((current) => ({ ...current, width: entry.contentRect.width }));
    });
    resizeObserver.observe(elementRef.current);

    return () => {
      resizeObserver.disconnect();
    };
  }, [setViewport]);

  return (
    <>
      <TaskBarsRefresher />
      <BaselineStatesRefresher />
      <RowSelector containerRef={elementRef} />

      <div ref={elementRef} className={style} style={{ height }}>
        <TaskBarHighlightStyles />

        <ScrollableContainer>
          <Grid />
          <RowList />

          <RelationLineContainer width={width} />
          <BaselineProgressLineContainer width={width} />
          <ProgressLineContainer width={width} />

          <TimeRangeGuideline />
          <TreeBlockContainer width={width} />
        </ScrollableContainer>
      </div>
    </>
  );
};

export default Timeline;

const style = css`
  position: relative;
  border-left: 1px solid ${borderColor};
`;
