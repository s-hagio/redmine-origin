import { OffsetFrom, OffsetTo } from '../types';

export const offsetFrom: OffsetFrom = {
  top: 1,
  bottom: 2,
  right: 11,
  middle: 50,
};

export const offsetTo: OffsetTo = {
  top: 101,
  bottom: 102,
  left: 111,
  middle: 150,
};
