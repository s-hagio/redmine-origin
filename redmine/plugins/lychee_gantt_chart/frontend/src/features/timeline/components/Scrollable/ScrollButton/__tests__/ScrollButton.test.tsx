import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import ScrollButton from '../ScrollButton';
import { TimeDirection } from '../../../../models/timeAxis';

const startAutoScrollMock = vi.hoisted(() => vi.fn());
const stopAutoScrollMock = vi.hoisted(() => vi.fn());
const useAutoScrollMock = vi.hoisted(() =>
  vi.fn().mockReturnValue({ start: startAutoScrollMock, stop: stopAutoScrollMock })
);

vi.mock('../../../../models/scroll', () => ({
  useAutoScroll: useAutoScrollMock,
}));

test('Render: backward', () => {
  const { container } = render(<ScrollButton direction={TimeDirection.Backward} />);

  expect(container).toMatchSnapshot();
});

test('Render: forward', () => {
  const { container } = render(<ScrollButton direction={TimeDirection.Forward} />);

  expect(container).toMatchSnapshot();
});

test('クリックし続けて間はオートスクロールし続け、クリックをやめたら停止', () => {
  const direction = TimeDirection.Backward;
  const { getByRole } = render(<ScrollButton direction={direction} />);

  expect(useAutoScrollMock).toBeCalledWith(direction);
  expect(startAutoScrollMock).not.toBeCalled();

  const button = getByRole('button');
  fireEvent.mouseDown(button);

  expect(startAutoScrollMock).toBeCalled();
  expect(stopAutoScrollMock).not.toBeCalled();

  fireEvent.mouseUp(document);

  expect(stopAutoScrollMock).toBeCalled();
});
