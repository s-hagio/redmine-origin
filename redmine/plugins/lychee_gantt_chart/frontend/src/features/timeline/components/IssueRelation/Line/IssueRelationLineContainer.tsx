import * as React from 'react';
import { MouseEventHandler } from 'react';
import { css, cx } from '@linaria/core';
import { sortBy } from '@/utils/array';
import { useVisibleIssueRelations, useSelectedIssueRelationIdState } from '../../../models/issueRelation';
import { useTimelineOptionsState } from '../../../models/options';
import IssueRelationLine from './IssueRelationLine';
import PreviewLine from './PreviewLine';
import { deletableLineStyle } from './Line';

type Props = {
  width: number;
};

const IssueRelationLineContainer: React.FC<Props> = ({ width }) => {
  const [timelineOptions] = useTimelineOptionsState();

  const issueRelations = useVisibleIssueRelations();
  const [selectedId, select] = useSelectedIssueRelationIdState();

  const handleClick = React.useCallback<MouseEventHandler<SVGSVGElement>>(
    (event) => {
      const matchedRelationsIds: string[] = document
        .elementsFromPoint(event.clientX, event.clientY)
        .filter((el) => el.classList.contains(deletableLineStyle))
        .map((el) => el.getAttribute('data-id') + '');

      switch (matchedRelationsIds.length) {
        case 0:
          select(null);
          return;
        case 1:
          select(+matchedRelationsIds[0]);
          return;
      }

      const index = selectedId ? matchedRelationsIds.indexOf(selectedId + '') : -1;

      if (index !== -1 && matchedRelationsIds[index + 1]) {
        select(+matchedRelationsIds[index + 1]);
      } else {
        select(+matchedRelationsIds[0]);
      }
    },
    [select, selectedId]
  );

  const sortedIssueRelations = sortBy(issueRelations, ({ id }) => selectedId === id + '');

  return (
    <svg className={cx(style, selectedId && hasSelectedStyle)} style={{ width }} onClick={handleClick}>
      {timelineOptions.showRelationLine &&
        sortedIssueRelations.map((issueRelation) => (
          <IssueRelationLine
            key={issueRelation.id}
            id={issueRelation.id}
            relationType={issueRelation.relationType}
            selectedIssueRelationId={selectedId}
          />
        ))}

      <PreviewLine />
    </svg>
  );
};

export default React.memo(IssueRelationLineContainer);

const style = css`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  z-index: 6;
`;

const hasSelectedStyle = css`
  z-index: 100;
  background: rgba(255, 255, 255, 0.25);
`;
