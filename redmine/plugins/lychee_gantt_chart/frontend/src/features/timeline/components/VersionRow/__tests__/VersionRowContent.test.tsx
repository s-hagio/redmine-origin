import * as React from 'react';
import { render } from '@testing-library/react';
import { mainProjectVersionRow, versionSharedByHierarchy } from '@/__fixtures__';
import VersionRowContent from '../VersionRowContent';

vi.mock('../../Baseline', () => ({
  VersionBaselineBar: vi.fn((props) => `VersionBaselineBar: ${JSON.stringify(props)}`),
}));

const VersionTaskBarSpy = vi.hoisted(() => vi.fn());

vi.mock('../VersionTaskBar', () => ({
  default: vi.fn((props) => {
    VersionTaskBarSpy(props);
    return 'VersionTaskBar';
  }),
}));

const version = versionSharedByHierarchy;
const row = mainProjectVersionRow;

test('Render', () => {
  const { container } = render(<VersionRowContent id={row.id} version={version} />);

  expect(container).toMatchSnapshot();
  expect(VersionTaskBarSpy).toBeCalledWith({ identifier: row.id, version });
});
