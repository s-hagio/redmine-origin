import * as React from 'react';
import { VersionResourceIdentifier } from '@/models/coreResource';
import { useBaselineBar } from '../../../models/baseline';
import BaselineBar from './BaselineBar';

type Props = {
  identifier: VersionResourceIdentifier;
};

const VersionBaselineBar: React.FC<Props> = ({ identifier }) => {
  const bar = useBaselineBar(identifier);

  if (!bar) {
    return null;
  }

  return <BaselineBar bar={bar} />;
};

export default VersionBaselineBar;
