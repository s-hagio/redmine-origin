import * as React from 'react';
import { css } from '@linaria/core';
import { primaryColor } from '@/styles';
import { ICON_SIZE } from './constants';
import { ScrollToCenterButton, ZoomButton, TimeAxisTypeSelect, MoreConfigButton } from './buttons';

const ButtonList: React.FC = () => {
  return (
    <ul className={style}>
      <li>
        <ScrollToCenterButton />
      </li>

      <li>
        <ZoomButton type="zoomOut" />
        <TimeAxisTypeSelect />
        <ZoomButton type="zoomIn" />
      </li>

      <li>
        <MoreConfigButton />
      </li>
    </ul>
  );
};

export default React.memo(ButtonList);

export const style = css`
  display: flex;
  align-items: center;
  margin: 0;
  padding: 0;

  gap: 15px;
  background: rgba(255, 255, 255, 0.6);
  transition: opacity ease-in-out 0.2s;
  opacity: 0.5;

  :hover {
    opacity: 1;
  }

  li {
    display: flex;
    align-items: center;
    gap: 2px;
    position: relative;
    margin: 0;
    overflow: hidden;
    list-style: none;

    button {
      display: flex;
      align-items: center;
      justify-content: center;
      width: ${ICON_SIZE + 8}px;
      height: ${ICON_SIZE + 6}px;
      padding: 0;
      border: 1px solid #aaa;
      background: #fff;
      cursor: pointer;

      :hover {
        border-color: ${primaryColor};
      }

      :disabled {
        opacity: 0.3;
      }
    }
  }
`;
