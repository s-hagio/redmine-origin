import * as React from 'react';
import { css } from '@linaria/core';
import { NakedButton } from '@/components/Button';
import { Icon } from '@/components/Icon';
import { useAutoScroll } from '../../../models/scroll';
import { TimeDirection } from '../../../models/timeAxis';

type Props = {
  direction: TimeDirection;
};

const ScrollButton: React.FC<Props> = ({ direction }) => {
  const { start: startAutoScroll, stop: stopAutoScroll } = useAutoScroll(direction);

  const handleMouseUp = React.useCallback(() => {
    stopAutoScroll();
    document.removeEventListener('mouseup', handleMouseUp);
  }, [stopAutoScroll]);

  const handleMouseDown = React.useCallback(() => {
    startAutoScroll();
    document.addEventListener('mouseup', handleMouseUp);
  }, [startAutoScroll, handleMouseUp]);

  return (
    <div className={style} data-direction={direction} onMouseDown={handleMouseDown}>
      <NakedButton>
        <Icon name={direction === TimeDirection.Backward ? 'chevronLeft' : 'chevronRight'} />
      </NakedButton>
    </div>
  );
};

export default ScrollButton;

export const style = css`
  button {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 25px;
    height: 25px;
    border: 1px solid #c9c9c9;
    border-radius: 2px;
    background: #f9f9f9;
  }
`;
