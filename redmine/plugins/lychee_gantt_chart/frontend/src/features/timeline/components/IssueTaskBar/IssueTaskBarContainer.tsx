import * as React from 'react';
import { css, cx } from '@linaria/core';
import { IssueID, useIssueOrThrow, IssueParams } from '@/models/issue';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { extractIssueFieldContext } from '@/models/fieldRule';
import { useUpdateIssue } from '@/features/resourceController';
import { extractIssueBarParams, useIssueTaskBarDraggability } from '../../models/taskBar';
import { metadataStyle } from '../Metadata';
import { issueRelationCreateButtonStyle, issueRelationCreationTargetStyle } from '.././IssueRelation';
import DraggableIssueTaskBar, {
  rangeDragHandleStyle,
  dateDragHandleStyle,
  dateDragHandleWidth,
} from './DraggableIssueTaskBar';
import IssueTaskBar from './IssueTaskBar';
import IssueTaskBarCreator from './IssueTaskBarCreator';

type Props = {
  identifier: IssueResourceIdentifier;
  id: IssueID;
};

const IssueTaskBarContainer: React.FC<Props> = ({ identifier, id }) => {
  const issue = useIssueOrThrow(id);
  const { isDraggable, isStartDateDraggable, isDueDateDraggable } = useIssueTaskBarDraggability(
    extractIssueFieldContext(issue)
  );

  const updateIssue = useUpdateIssue(issue.id);
  const save = React.useCallback(
    async (params: IssueParams) => {
      await updateIssue(params);
    },
    [updateIssue]
  );

  const barParams = extractIssueBarParams(issue);

  if (!barParams) {
    return isDraggable ? <IssueTaskBarCreator identifier={identifier} saveIssue={save} /> : null;
  }

  return (
    <div
      className={cx(
        style,
        isStartDateDraggable && startDateDraggableStyle,
        isDueDateDraggable && dueDateDraggableStyle
      )}
    >
      {!isDraggable && <IssueTaskBar identifier={identifier} issue={issue} />}
      {isDraggable && (
        <DraggableIssueTaskBar identifier={identifier} issue={issue} initialParams={barParams} />
      )}
    </div>
  );
};

export default React.memo(IssueTaskBarContainer);

const style = css`
  .${rangeDragHandleStyle},
    .${dateDragHandleStyle},
    .${issueRelationCreateButtonStyle},
    .${issueRelationCreationTargetStyle} {
    position: absolute;
    top: 0;
    z-index: 11;
  }

  .${metadataStyle} {
    z-index: 11;
  }

  .${dateDragHandleStyle}, .${issueRelationCreateButtonStyle} {
    opacity: 0;
  }
  :hover {
    .${dateDragHandleStyle}, .${issueRelationCreateButtonStyle} {
      opacity: 1;
    }
  }

  .${dateDragHandleStyle} {
    &[data-handle-type='start'] {
      left: 0;
      transform: translateX(-100%);
    }
    &[data-handle-type='end'] {
      right: 0;
      transform: translateX(100%);
    }
  }

  .${issueRelationCreateButtonStyle} {
    top: 50%;

    &[data-create-type='to'] {
      left: 0;
      transform: translate(-100%, -50%);
    }
    &[data-create-type='from'] {
      right: 0;
      transform: translate(100%, -50%);
    }
  }
`;

const startDateDraggableStyle = css`
  .${issueRelationCreateButtonStyle}[data-create-type='to'] {
    left: -${dateDragHandleWidth}px;
  }
`;

const dueDateDraggableStyle = css`
  .${issueRelationCreateButtonStyle}[data-create-type='from'] {
    right: -${dateDragHandleWidth}px;
  }
`;
