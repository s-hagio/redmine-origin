import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { Icon } from '@/components/Icon';
import { NakedButton } from '@/components/Button';
import { ICON_SIZE } from '../constants';
import { timeAxisTypes, useTimeAxisOptionsState } from '../../../models/timeAxis';

type Props = {
  type: 'zoomIn' | 'zoomOut';
};

const ZoomButton: React.FC<Props> = ({ type }) => {
  const { t } = useTranslation();

  const [timeAxisOptions, setTimeAxisOptions] = useTimeAxisOptionsState();

  const isZoomIn = type === 'zoomIn';
  const zoomValue = isZoomIn ? 1 : -1;
  const isDisabled = timeAxisTypes.at(isZoomIn ? -1 : 0) === timeAxisOptions.type;

  const handleClick = React.useCallback(() => {
    setTimeAxisOptions((current) => ({
      ...current,
      type: timeAxisTypes[timeAxisTypes.indexOf(current.type) + zoomValue],
    }));
  }, [setTimeAxisOptions, zoomValue]);

  return (
    <NakedButton type="button" title={t(`label.${type}`)} disabled={isDisabled} onClick={handleClick}>
      <Icon name={type} size={ICON_SIZE + 4} />
    </NakedButton>
  );
};

export default ZoomButton;
