import * as React from 'react';
import { renderWithRecoil } from '@/test-utils';
import MetadataOptionList from '../MetadataOptionList';

vi.mock('../MetadataVisibilityRadioButton', () => ({
  default: vi.fn((props) => `MetadataVisibilityRadioButton: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = renderWithRecoil(<MetadataOptionList />);

  expect(container).toMatchSnapshot();
});
