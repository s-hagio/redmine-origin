import * as React from 'react';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act, fireEvent } from '@testing-library/react';
import { renderWithRecoil, renderWithRecoilHook } from '@/test-utils';
import { TranslationProvider } from '@/providers/translationProvider';
import { timelineOptionsState } from '../../../../models/options';
import ShowProgressLineOption from '../ShowProgressLineOption';

test('Render', () => {
  const { container } = renderWithRecoil(
    <TranslationProvider lang="ja">
      <ShowProgressLineOption />
    </TranslationProvider>
  );

  expect(container).toMatchSnapshot();
});

test('checkboxのチェック状態に応じてオプション値をセット', () => {
  const { getByRole, result } = renderWithRecoilHook(<ShowProgressLineOption />, () => ({
    timelineOptions: useRecoilValue(timelineOptionsState),
    setTimelineOptions: useSetRecoilState(timelineOptionsState),
  }));

  act(() => {
    result.current.setTimelineOptions((current) => ({ ...current, showProgressLine: false }));
  });

  const checkbox = getByRole('checkbox') as HTMLInputElement;

  expect(result.current.timelineOptions.showProgressLine).toBe(false);
  expect(checkbox.checked).toBe(false);

  fireEvent.click(checkbox);

  expect(result.current.timelineOptions.showProgressLine).toBe(true);
  expect(checkbox.checked).toBe(true);

  fireEvent.click(checkbox);

  expect(result.current.timelineOptions.showProgressLine).toBe(false);
  expect(checkbox.checked).toBe(false);
});
