import * as React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithTranslation } from '@/test-utils';
import ScrollToCenterButton from '../ScrollToCenterButton';

const setNeedToCenteringMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/scroll', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useSetNeedToCentering: vi.fn().mockReturnValue(setNeedToCenteringMock),
}));

test('Render', () => {
  const { container } = renderWithTranslation(<ScrollToCenterButton />);

  expect(container).toMatchSnapshot();
});

test('クリックでセンタリング処理', () => {
  const { getByRole } = renderWithTranslation(<ScrollToCenterButton />);
  const button = getByRole('button');

  expect(setNeedToCenteringMock).not.toBeCalled();

  fireEvent.click(button);

  expect(setNeedToCenteringMock).toBeCalledWith(true);
});
