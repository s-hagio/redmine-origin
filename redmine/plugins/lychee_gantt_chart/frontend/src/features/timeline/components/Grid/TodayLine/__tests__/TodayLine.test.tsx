import * as React from 'react';
import { renderWithRecoil } from '@/test-utils';
import TodayLine from '../TodayLine';
import { timeAxisOptionsState, TimeAxisType } from '../../../../models/timeAxis';

vi.mock('../../../../models/tick', () => ({
  useTodayTickPosition: vi.fn().mockReturnValue(123),
}));

test.each(Object.values(TimeAxisType))('Render: %s', (type) => {
  const result = renderWithRecoil(<TodayLine />, {
    initializeState: ({ set }) => {
      set(timeAxisOptionsState, (current) => ({ ...current, type }));
    },
  });

  expect(result.container).toMatchSnapshot();
});
