import { Row } from '@/features/row';

export type RowProps<ResourceRow extends Row> = Pick<ResourceRow, 'id' | 'resourceId'>;
