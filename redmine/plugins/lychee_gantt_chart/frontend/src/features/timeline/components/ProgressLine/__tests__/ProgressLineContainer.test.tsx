import * as React from 'react';
import { render } from '@testing-library/react';
import ProgressLineContainer from '../ProgressLineContainer';

const useProgressLineOffsetsMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/progressLine', () => ({
  useProgressLineOffsets: useProgressLineOffsetsMock,
}));

vi.mock('../ProgressLine', () => ({
  default: vi.fn((props) => `ProgressLine: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  useProgressLineOffsetsMock.mockReturnValue([
    { top: 1, left: 2 },
    { top: -3, left: -4 },
  ]);

  const { container } = render(<ProgressLineContainer width={999} />);

  expect(container).toMatchSnapshot();
});

test('Render: No offsets', () => {
  useProgressLineOffsetsMock.mockReturnValue([]);

  const { container } = render(<ProgressLineContainer width={999} />);

  expect(container).toMatchSnapshot();
});
