import * as React from 'react';
import { css, cx } from '@linaria/core';
import { IssueRelationID, RelationType } from '@/models/issueRelation';
import { OffsetFrom, OffsetTo } from './types';
import DeleteButton from './DeleteButton';
import { width as arrowWidth } from './Arrow';

type Props = {
  id: IssueRelationID | null;
  relationType: RelationType;
  isDeletable: boolean;
  isSelected: boolean;
  offsetFrom: OffsetFrom;
  offsetTo: OffsetTo;
  color: string;
};

const margins: ReadonlyMap<string, number> = new Map([
  [RelationType.Precedes, 6],
  [RelationType.Blocks, 10],
  [RelationType.Draft, 8],
]);

const Line: React.FC<Props> = ({
  id,
  relationType,
  isDeletable,
  isSelected,
  offsetFrom,
  offsetTo,
  color,
}) => {
  const marginX = margins.get(relationType) ?? 0;
  const marginY = 6;
  const curveX = 2;
  const curveY = offsetFrom.middle > offsetTo.middle ? -2 : 2;

  const c = { x: offsetFrom.right, y: offsetFrom.middle };

  const paths = [`M${c.x},${c.y}`, `L${(c.x += marginX)},${c.y}`];

  if (offsetFrom.right + marginX < offsetTo.left - marginX) {
    paths.push(`L${c.x},${(c.y = offsetTo.middle - curveY)}`);
    paths.push(`L${c.x},${(c.y += curveY)} ${(c.x += curveX)},${c.y}`);
  } else {
    if (offsetFrom.middle > offsetTo.middle) {
      c.y = offsetTo.bottom + marginY;
    } else {
      c.y = offsetTo.top - marginY;
    }

    paths.push(`L${c.x},${(c.y += curveY)}`);
    paths.push(`L${c.x},${(c.y += curveY)} ${(c.x -= curveX)},${c.y}`);
    paths.push(`L${(c.x = offsetTo.left - marginX)},${c.y}`);
    paths.push(`L${(c.x -= curveX)},${c.y} ${c.x},${(c.y += curveY)}`);
    paths.push(`L${c.x},${(c.y = offsetTo.middle - curveY)}`);
    paths.push(`L${c.x},${(c.y += curveY)} ${(c.x += curveX)},${c.y}`);
  }

  paths.push(`L${(c.x = offsetTo.left - arrowWidth)},${c.y}`);

  const d = paths.join('');

  return (
    <>
      <path className={cx(style, isSelected && selectedStyle)} d={d} stroke={color} />
      <path className={deletableLineStyle} d={d} data-id={id} />

      {isDeletable && isSelected && id && (
        <DeleteButton id={id} offsetFrom={offsetFrom} offsetTo={offsetTo} marginX={marginX} />
      )}
    </>
  );
};

export default React.memo(Line);

const style = css`
  fill: none;
  stroke-width: 1px;
  shape-rendering: crispEdges;
`;

const selectedStyle = css`
  stroke-width: 4px;
`;

export const deletableLineStyle = css`
  fill: none;
  stroke: transparent;
  stroke-width: 4px;
  cursor: pointer;
`;
