import * as React from 'react';
import { render } from '@testing-library/react';
import { DayGridColumn } from '../../../models/grid';
import DayGridRow from '../DayGridRow/DayGridRow';

vi.mock('@/features/row', () => ({
  useTotalRowHeight: vi.fn().mockReturnValue(1600),
}));

vi.mock('../../../models/tick', () => ({
  useTotalTickWidth: vi.fn().mockReturnValue(3000),
}));

const baseColumn: Pick<
  DayGridColumn,
  'type' | 'width' | 'isNonWorkingDay' | 'isBeforeInvisibleDay' | 'isAfterInvisibleDay'
> = {
  type: 'day',
  width: 2,
  isNonWorkingDay: false,
  isBeforeInvisibleDay: false,
  isAfterInvisibleDay: false,
};

test('休業日表示ONのレンダリング', () => {
  const columns: DayGridColumn[] = [
    { ...baseColumn, id: 'day-2022-07-01', label: '2022-07-01' },
    { ...baseColumn, id: 'day-2022-07-02', label: '2022-07-02', isNonWorkingDay: true },
    { ...baseColumn, id: 'day-2022-07-03', label: '2022-07-03' },
    { ...baseColumn, id: 'day-2022-07-04', label: '2022-07-04' },
    { ...baseColumn, id: 'day-2022-07-05', label: '2022-07-05' },
    { ...baseColumn, id: 'day-2022-07-06', label: '2022-07-06', isNonWorkingDay: true },
    { ...baseColumn, id: 'day-2022-07-07', label: '2022-07-07', isNonWorkingDay: true },
  ];

  const result = render(<DayGridRow columns={columns} />);

  expect(result.container).toMatchSnapshot();
});

test('休業日表示OFFのレンダリング', () => {
  const columns: DayGridColumn[] = [
    { ...baseColumn, id: 'day-2022-07-01', label: '2022-07-01', isBeforeInvisibleDay: true },
    { ...baseColumn, id: 'day-2022-07-03', label: '2022-07-03', isAfterInvisibleDay: true },
    { ...baseColumn, id: 'day-2022-07-04', label: '2022-07-04' },
    {
      ...baseColumn,
      id: 'day-2022-07-06',
      label: '2022-07-06',
      isBeforeInvisibleDay: true,
      isAfterInvisibleDay: true,
    },
  ];

  const result = render(<DayGridRow columns={columns} />);

  expect(result.container).toMatchSnapshot();
});
