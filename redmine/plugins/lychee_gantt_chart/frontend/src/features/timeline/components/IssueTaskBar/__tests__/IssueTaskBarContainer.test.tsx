import * as React from 'react';
import { renderWithTranslation } from '@/test-utils';
import { getIssueResourceIdentifier } from '@/models/coreResource';
import { leafIssue } from '@/__fixtures__';
import { extractIssueBarParams } from '../../../models/taskBar';
import IssueTaskBarContainer from '../IssueTaskBarContainer';

const useIssueOrThrowMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/issue', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIssueOrThrow: useIssueOrThrowMock,
}));

const useIssueTaskBarDraggabilityMock = vi.hoisted(() =>
  vi.fn().mockReturnValue({
    isDraggable: false,
    isStartDateDraggable: false,
    isDueDateDraggable: false,
  })
);

vi.mock('../../../models/taskBar', async () => ({
  ...(await vi.importActual<object>('../../../models/taskBar')),
  useIssueTaskBarDraggability: useIssueTaskBarDraggabilityMock,
}));

const IssueTaskBarSpy = vi.hoisted(() => vi.fn());

vi.mock('../IssueTaskBar', () => ({
  default: vi.fn(({ children, ...props }) => {
    IssueTaskBarSpy(props);
    return React.createElement('issue-task-bar', null, children);
  }),
}));

const IssueTaskBarCreatorSpy = vi.hoisted(() => vi.fn());

vi.mock('../IssueTaskBarCreator', () => ({
  default: vi.fn(({ children, ...props }) => {
    IssueTaskBarCreatorSpy(props);
    return React.createElement('issue-task-bar-creator', null, children);
  }),
}));

const DraggableIssueTaskBarSpy = vi.hoisted(() => vi.fn());

vi.mock('../DraggableIssueTaskBar', () => ({
  default: vi.fn(({ children, ...props }) => {
    DraggableIssueTaskBarSpy(props);
    return React.createElement('draggable-issue-task-bar', null, children);
  }),
}));

const issue = leafIssue;
const identifier = getIssueResourceIdentifier(issue.id);
const issueWithBarParams = { ...leafIssue, startDate: '2023-09-10', dueDate: '2023-09-15' };
const issueWithoutBarParams = { ...leafIssue, startDate: null, dueDate: null };

const renderForTesting = () => {
  return renderWithTranslation(<IssueTaskBarContainer identifier={identifier} id={issue.id} />);
};

describe('isDraggable: false', () => {
  beforeEach(() => {
    useIssueTaskBarDraggabilityMock.mockReturnValue({ isDraggable: false });
  });

  test('barParamsがある', () => {
    useIssueOrThrowMock.mockReturnValue(issueWithBarParams);

    const { container } = renderForTesting();

    expect(container).toMatchSnapshot();
    expect(IssueTaskBarSpy).toBeCalledWith({ identifier, issue: issueWithBarParams });
  });

  test('barParamsがない', () => {
    useIssueOrThrowMock.mockReturnValue(issueWithoutBarParams);

    const { container } = renderForTesting();

    expect(container).toMatchSnapshot();
  });
});

describe('isDraggable: true', () => {
  beforeEach(() => {
    useIssueTaskBarDraggabilityMock.mockReturnValue({ isDraggable: true });
  });

  test('barParamsがある', () => {
    useIssueOrThrowMock.mockReturnValue(issueWithBarParams);

    const { container } = renderForTesting();

    expect(container).toMatchSnapshot();
    expect(DraggableIssueTaskBarSpy).toBeCalledWith({
      identifier,
      issue: issueWithBarParams,
      initialParams: extractIssueBarParams(issueWithBarParams),
    });
  });

  test('barParamsがない', () => {
    useIssueOrThrowMock.mockReturnValue(issueWithoutBarParams);

    const { container } = renderForTesting();

    expect(container).toMatchSnapshot();
    expect(IssueTaskBarCreatorSpy).toBeCalledWith({
      identifier,
      saveIssue: expect.any(Function),
    });
  });
});

describe('isStartDateDraggable: true', () => {
  beforeEach(() => {
    useIssueOrThrowMock.mockReturnValue(issueWithBarParams);
    useIssueTaskBarDraggabilityMock.mockReturnValue({ isStartDateDraggable: true });
  });

  test('startDateがドラッグ可能な場合用のclassNameを付与', () => {
    const { container } = renderForTesting();

    expect(container).toMatchSnapshot();
  });
});

describe('isDueDateDraggable: true', () => {
  beforeEach(() => {
    useIssueOrThrowMock.mockReturnValue(issueWithBarParams);
    useIssueTaskBarDraggabilityMock.mockReturnValue({ isDueDateDraggable: true });
  });

  test('dueDateがドラッグ可能な場合用のclassNameを付与', () => {
    const { container } = renderForTesting();

    expect(container).toMatchSnapshot();
  });
});
