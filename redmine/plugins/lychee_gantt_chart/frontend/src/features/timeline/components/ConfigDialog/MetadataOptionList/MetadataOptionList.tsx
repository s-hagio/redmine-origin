import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { borderColor } from '@/styles';
import { MetadataType, MetadataVisibility } from '../../../models/metadata';
import MetadataVisibilityRadioButton from './MetadataVisibilityRadioButton';

const metadataTypes: ReadonlyArray<MetadataType> = Object.values(MetadataType);
const metadataVisibilities: ReadonlyArray<MetadataVisibility> = Object.values(MetadataVisibility);

const MetadataOptionList: React.FC = () => {
  const { t } = useTranslation();

  return (
    <fieldset className={style}>
      <legend>{t('timeline.metadata')}</legend>

      <table>
        <thead>
          <tr>
            <th scope="col" />

            {metadataVisibilities.map((visibility) => (
              <th key={visibility} scope="col">
                <span>{t(`timeline.metadataVisibilities.${visibility}`)}</span>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {metadataTypes.map((type) => (
            <tr key={type}>
              <th scope="row">{t(`timeline.metadataTypes.${type}`)}</th>

              {metadataVisibilities.map((visibility) => (
                <td key={visibility}>
                  <MetadataVisibilityRadioButton type={type} visibility={visibility} />
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </fieldset>
  );
};

export default React.memo(MetadataOptionList);

export const style = css`
  padding: 0;
  border: none;

  legend {
    padding: 0;
    font-weight: bold;
  }

  table {
    margin-top: 1em;
    border-collapse: collapse;
    font-size: 0.95em;
  }

  th {
    padding-right: 0.75rem;
    font-weight: normal;
  }

  th,
  td {
    border-bottom: 1px solid ${borderColor};
  }

  thead th {
    padding: 0 1em 1em;
    text-align: center;
    line-height: 1.2;

    :not(:last-of-type) {
      white-space: nowrap;
    }

    &:last-of-type span {
      display: block;
      width: 9em;
      word-break: break-all;
    }
  }

  tbody {
    white-space: nowrap;
  }
`;
