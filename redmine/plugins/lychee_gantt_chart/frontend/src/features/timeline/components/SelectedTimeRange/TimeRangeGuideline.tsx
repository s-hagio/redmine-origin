import * as React from 'react';
import { css } from '@linaria/core';
import { useSelectedTimeBar } from '../../models/timeBar';

const TimeRangeGuideline: React.FC = () => {
  const timeBar = useSelectedTimeBar();

  if (!timeBar || !timeBar.showGuideline) {
    return null;
  }

  return <div className={style} style={{ width: timeBar.width + 1, left: timeBar.left - 1 }} />;
};

export default React.memo(TimeRangeGuideline);

const style = css`
  position: absolute;
  top: 0;
  height: 100%;
  border: solid rgba(98, 158, 187, 0.2);
  border-width: 0 1px;
  background: rgba(119, 192, 227, 0.075);
  z-index: 100;
`;
