import * as React from 'react';
import { atom, useRecoilState, useRecoilValue } from 'recoil';
import { fireEvent } from '@testing-library/react';
import { renderWithRecoilHook, renderWithTranslation } from '@/test-utils';
import TimeAxisTypeSelect from '../TimeAxisTypeSelect';
import { TimeAxisType } from '../../../../models/timeAxis';

const setTimeAxisOptionsMock = vi.hoisted(() => vi.fn());
const useTimeAxisOptionsStateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../../models/timeAxis', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useTimeAxisOptionsState: useTimeAxisOptionsStateMock,
}));

test('Render', () => {
  useTimeAxisOptionsStateMock.mockReturnValue([{ type: TimeAxisType.DaySmall }, setTimeAxisOptionsMock]);

  const { getByRole, container } = renderWithTranslation(<TimeAxisTypeSelect />);

  expect(container).toMatchSnapshot();
  expect(getByRole('option', { selected: true })).toHaveProperty('value', TimeAxisType.DaySmall);
});

test('選択時にTimeAxisTypeを変更する', () => {
  // 週別初期表示 with 他の値
  const timeAxisOptionsState = atom({
    key: 'timeAxisOptionsState',
    default: { otherKey: 'otherValue', type: TimeAxisType.Week },
  });

  useTimeAxisOptionsStateMock.mockImplementation(() => useRecoilState(timeAxisOptionsState));

  const { result, container } = renderWithRecoilHook(<TimeAxisTypeSelect />, () =>
    useRecoilValue(timeAxisOptionsState)
  );

  const selectElement = container.querySelector('select') as HTMLSelectElement;

  fireEvent.change(selectElement, { target: { value: TimeAxisType.DaySmall } });
  expect(result.current).toEqual({ otherKey: 'otherValue', type: TimeAxisType.DaySmall });

  fireEvent.change(selectElement, { target: { value: TimeAxisType.MonthLarge } });
  expect(result.current).toEqual({ otherKey: 'otherValue', type: TimeAxisType.MonthLarge });
});
