import * as React from 'react';
import { getIssueResourceIdentifier } from '@/models/coreResource';
import { useIssueRelationCreation } from '../../models/issueRelation';
import { barStyle } from './TaskBar';

const TaskBarHighlightStyles: React.FC = () => {
  const { sourceIssueId, targetIssueId } = useIssueRelationCreation() || {};

  const styles = React.useMemo(() => {
    if (!sourceIssueId && !targetIssueId) {
      return null;
    }

    const notLowlightSelector = ([sourceIssueId, targetIssueId].filter(Boolean) as number[])
      .map((id) => `:not([data-row="${getIssueResourceIdentifier(id)}"])`)
      .join('');

    const lowlightStyles = `[data-row]${notLowlightSelector} .${barStyle} { opacity: 0.5; }`;

    return [lowlightStyles].join('');
  }, [sourceIssueId, targetIssueId]);

  if (!styles) {
    return null;
  }

  return <style>{styles}</style>;
};

export default TaskBarHighlightStyles;
