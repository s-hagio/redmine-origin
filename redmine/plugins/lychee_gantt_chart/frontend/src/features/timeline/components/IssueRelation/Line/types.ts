type Offset = {
  top: number;
  middle: number;
  bottom: number;
};

export type OffsetFrom = Offset & {
  right: number;
};

export type OffsetTo = Offset & {
  left: number;
};
