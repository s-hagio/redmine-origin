import * as React from 'react';
import { useRefreshBaselineStates } from '../../models/baseline';

const BaselineStatesRefresher: React.FC = () => {
  const refresh = useRefreshBaselineStates();

  React.useEffect(() => {
    refresh();
  }, [refresh]);

  return null;
};

export default BaselineStatesRefresher;
