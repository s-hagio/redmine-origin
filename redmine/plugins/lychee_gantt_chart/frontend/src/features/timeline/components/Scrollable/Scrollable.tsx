import * as React from 'react';
import { css } from '@linaria/core';
import { useScrollLeft, useSetScrollLeft } from '../../models/scroll';
import { TimeDirection } from '../../models/timeAxis';
import { useStretchGrid } from '../../hooks';

type Props = React.PropsWithChildren;

const Scrollable: React.FC<Props> = ({ children }) => {
  const ref = React.useRef<HTMLDivElement>(null);

  const scrollLeft = useScrollLeft();
  const setScrollLeft = useSetScrollLeft();

  const stretchGrid = useStretchGrid();

  const handleScroll = React.useCallback(
    ({ currentTarget }: React.UIEvent<HTMLDivElement>) => {
      setScrollLeft(currentTarget.scrollLeft);
    },
    [setScrollLeft]
  );

  React.useEffect(() => {
    const el = ref.current;
    if (!el) return;

    el.scrollLeft = scrollLeft;

    const { stretchedWidth, direction } = stretchGrid(el);

    if (direction === TimeDirection.Backward) {
      setScrollLeft((current) => current + stretchedWidth);
    }
  }, [scrollLeft, setScrollLeft, stretchGrid]);

  return (
    <div ref={ref} className={style} onScroll={handleScroll}>
      {children}
    </div>
  );
};

export default Scrollable;

const style = css`
  position: relative;
  overflow-x: scroll;
`;
