import * as React from 'react';
import { useProgressLineOffsets } from '../../models/progressLine';
import ProgressLine from './ProgressLine';

type Props = {
  width: number;
};

const ProgressLineContainer: React.FC<Props> = ({ width }) => {
  const offsets = useProgressLineOffsets();

  if (!offsets.length) {
    return null;
  }

  return <ProgressLine width={width} offsets={offsets} color="#ff3131" />;
};

export default React.memo(ProgressLineContainer);
