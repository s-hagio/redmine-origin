import * as React from 'react';
import { useBaselineProgressLineOffsets } from '../../models/baseline';
import { ProgressLine } from '../ProgressLine';

type Props = {
  width: number;
};

const BaselineProgressLineContainer: React.FC<Props> = ({ width }) => {
  const progressLines = useBaselineProgressLineOffsets();

  if (!progressLines.length) {
    return null;
  }

  return <ProgressLine width={width} offsets={progressLines} color="rgb(0, 179, 238)" />;
};

export default React.memo(BaselineProgressLineContainer);
