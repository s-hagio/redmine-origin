import * as React from 'react';
import { render } from '@testing-library/react';
import { mainProjectRow, parentIssue, parentIssueRow, rootIssueRow } from '@/__fixtures__';
import IssueRowContent from '../IssueRowContent';

vi.mock('../../TreeBlock', () => ({
  TreeBlockItemContainer: vi.fn(({ children, ...props }) => {
    return React.createElement('tree-block-item-container', props, children);
  }),
}));

vi.mock('../../ScheduleStatus', () => ({
  ScheduleStatusRowContent: vi.fn((props) => `ScheduleStatusRowContent: ${JSON.stringify(props)}`),
}));

vi.mock('../../IssueTaskBar', () => ({
  IssueTaskBarContainer: vi.fn((props) => `IssueTaskBarContainer: ${JSON.stringify(props)}`),
}));

vi.mock('../../Baseline', () => ({
  IssueBaselineBar: vi.fn((props) => `IssueBaselineBar: ${JSON.stringify(props)}`),
}));

const issue = parentIssue;
const row = parentIssueRow;
const subtreeRootIds = [mainProjectRow.id, rootIssueRow.id];

test('Render', () => {
  const { container } = render(<IssueRowContent id={row.id} subtreeRootIds={subtreeRootIds} issue={issue} />);

  expect(container).toMatchSnapshot();
});
