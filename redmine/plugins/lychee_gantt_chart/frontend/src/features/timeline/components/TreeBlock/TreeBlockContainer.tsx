import * as React from 'react';
import { css } from '@linaria/core';
import { useVisibleTreeBlockIds } from '../../models/treeBlock';
import TreeBlock from './TreeBlock';

type Props = {
  width: number;
};

const TreeBlockContainer: React.FC<Props> = ({ width }) => {
  const treeBlockIds = useVisibleTreeBlockIds();

  return (
    <svg className={style} width={width}>
      {treeBlockIds.map((id) => (
        <TreeBlock key={id} id={id} />
      ))}
    </svg>
  );
};

export default React.memo(TreeBlockContainer);

const style = css`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
`;
