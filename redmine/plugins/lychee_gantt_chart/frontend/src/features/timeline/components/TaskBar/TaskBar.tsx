import * as React from 'react';
import { cx, css } from '@linaria/core';
import { pick } from '@/utils/object';
import { TaskBarParams, TaskBarResourceIdentifier, useTaskBar } from '../../models/taskBar';
import { useSelectTimeRange } from '../../models/timeBar';

type Props = React.PropsWithChildren<{
  identifier: TaskBarResourceIdentifier;
  barParams: TaskBarParams | null;
  showRangeMarker?: boolean;
}>;

const TaskBar: React.FC<Props> = ({ identifier, barParams, showRangeMarker, children }) => {
  const bar = useTaskBar(identifier);
  const selectTimeRange = useSelectTimeRange();

  const handleSelectTimeRange = React.useCallback(() => {
    barParams && selectTimeRange(pick(barParams, ['startDate', 'endDate']));
  }, [selectTimeRange, barParams]);

  const handleDeselectTimeRange = React.useCallback(() => {
    selectTimeRange(null);
  }, [selectTimeRange]);

  if (!bar) {
    return null;
  }

  return (
    <div
      className={cx(style, showRangeMarker && rangeMarkerStyle)}
      style={pick(bar, ['top', 'left', 'width', 'height'])}
      onMouseEnter={handleSelectTimeRange}
      onMouseLeave={handleDeselectTimeRange}
    >
      <div className={cx(barStyle, todoBarStyle)} />
      {!!bar.doneWidth && <div className={cx(barStyle, doneBarStyle)} style={{ width: bar.doneWidth }} />}

      <div className={overlapAdjusterStyle} />

      {children}
    </div>
  );
};

export default React.memo(TaskBar);

export const style = css`
  position: absolute;
`;

const rangeMarkerStyle = css`
  ::before,
  ::after {
    --size: 7px;
    --offset: calc(var(--size) / -2);
    content: '';
    display: block;
    position: absolute;
    top: 50%;
    width: var(--size);
    height: var(--size);
    margin-top: var(--offset);
    transform: rotate(45deg) skew(7deg, 7deg);
    background: #333;
    z-index: 12;
  }

  ::before {
    left: var(--offset);
  }

  ::after {
    right: var(--offset);
  }
`;

export const barStyle = css`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  border: 1px solid;
  border-radius: 4px;
  background-color: #fff;
  background-size: 2px 2px;
  background-position:
    0 0,
    1px 1px;
`;

export const todoBarStyle = css`
  width: 100%;
  border-color: #aaa;
  background: #cdcdcd;
  z-index: 1;
`;

export const doneBarStyle = css`
  border-color: #00c600;
  background: #b9ddc0;
  z-index: 2;
`;

export const overlapAdjusterStyle = css`
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 9;
`;
