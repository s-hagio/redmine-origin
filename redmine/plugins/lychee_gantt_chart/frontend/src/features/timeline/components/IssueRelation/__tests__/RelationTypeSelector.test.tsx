import * as React from 'react';
import { t } from 'i18next';
import { fireEvent } from '@testing-library/react';
import { renderWithTranslation } from '@/test-utils';
import { RelationType } from '@/models/issueRelation';
import RelationTypeSelector from '../RelationTypeSelector';

const useIssueRelationCreationMock = vi.hoisted(() => vi.fn());
const useDraftIssueRelationDelayMock = vi.hoisted(() => vi.fn());
const commitDraftIssueRelationMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/issueRelation', () => ({
  useIssueRelationCreation: useIssueRelationCreationMock,
  useDraftIssueRelationDelay: useDraftIssueRelationDelayMock,
  useCommitDraftIssueRelation: vi.fn().mockReturnValue(commitDraftIssueRelationMock),
}));

const onCloseButtonClickMock = vi.fn();

beforeEach(() => {
  useIssueRelationCreationMock.mockReturnValue({
    type: 'from',
    sourceIssueId: 123,
    targetIssueId: 456,
    isLocked: true,
  });
  useDraftIssueRelationDelayMock.mockReturnValue(99);
});

test('Render', () => {
  const { container } = renderWithTranslation(
    <RelationTypeSelector onCloseButtonClick={onCloseButtonClickMock} />
  );

  expect(container).toMatchSnapshot();
});

test('種類ボタンのクリックで確定', () => {
  const { getByText } = renderWithTranslation(
    <RelationTypeSelector onCloseButtonClick={onCloseButtonClickMock} />
  );

  fireEvent.click(getByText(t('label.issueRelationTypes.precedes')));
  expect(commitDraftIssueRelationMock).toBeCalledWith({ relationType: RelationType.Precedes, delay: 99 });

  fireEvent.click(getByText(t('label.issueRelationTypes.blocks')));
  expect(commitDraftIssueRelationMock).toBeCalledWith({ relationType: RelationType.Blocks, delay: 99 });
});

test('遅延日数の変更', () => {
  const { container, getByText } = renderWithTranslation(
    <RelationTypeSelector onCloseButtonClick={onCloseButtonClickMock} />
  );

  const delay = -100;
  const delayInput = container.querySelector('input[type="number"]') as HTMLInputElement;

  fireEvent.change(delayInput, { target: { value: delay } });
  expect(delayInput.value).toBe(delay.toString());

  fireEvent.click(getByText(t('label.issueRelationTypes.precedes')));
  expect(commitDraftIssueRelationMock).toBeCalledWith({ relationType: RelationType.Precedes, delay });
});

test('クローズボタンのクリックで閉じる', () => {
  const { getAllByRole } = renderWithTranslation(
    <RelationTypeSelector onCloseButtonClick={onCloseButtonClickMock} />
  );

  expect(onCloseButtonClickMock).not.toBeCalled();

  fireEvent.click(getAllByRole('button').at(-1)!);

  expect(onCloseButtonClickMock).toBeCalled();
});
