import * as React from 'react';
import { render } from '@testing-library/react';
import { UnitedGridColumn } from '../../../models/grid';
import UnitedGridRow from '../UnitedGridRow';

test('Render', () => {
  const columns = [{ width: 33 }, { width: 20 }] as UnitedGridColumn[];
  const result = render(<UnitedGridRow columns={columns} />);

  expect(result.container).toMatchSnapshot();
});
