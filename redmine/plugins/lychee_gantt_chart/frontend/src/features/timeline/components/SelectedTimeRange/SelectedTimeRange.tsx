import * as React from 'react';
import { css } from '@linaria/core';
import { useFormatDate } from '@/models/dateFormat';
import { useSelectedTimeRange, useSelectedTimeBar } from '../../models/timeBar';

type Props = {
  containerWidth: number;
  scrollLeft: number;
};

const SelectedTimeRange: React.FC<Props> = ({ containerWidth, scrollLeft }) => {
  const containerRef = React.useRef<HTMLDivElement>(null);
  const labelWrapperRef = React.useRef<HTMLDivElement>(null);

  const [isSymbolVisible, setSymbolVisible] = React.useState<boolean>(false);

  const formatDate = useFormatDate();
  const timeRange = useSelectedTimeRange();
  const timeBar = useSelectedTimeBar();

  const scrollRight = containerWidth + scrollLeft;

  React.useEffect(() => {
    if (!timeBar || !containerRef.current || !labelWrapperRef.current) {
      return;
    }

    setSymbolVisible(timeBar.width + 1 < labelWrapperRef.current.clientWidth);
  }, [containerWidth, scrollLeft, scrollRight, timeBar]);

  if (!timeRange || !timeBar) {
    return null;
  }

  const width = Math.min(
    timeBar.width,
    timeBar.width - (scrollLeft - timeBar.left), // 左見切れ
    timeBar.width - (timeBar.right - scrollRight), // 右見切れ
    containerWidth // 両方見切れ
  );

  const left = Math.max(timeBar.left, scrollLeft);

  return (
    <div ref={containerRef} className={style} style={{ width: width + 1, left: left - 1 }}>
      <div ref={labelWrapperRef} className={labelWrapperStyle}>
        <div className={labelStyle} data-item-type="start">
          {timeBar.left < scrollLeft && <span>...</span>}
          {formatDate(timeRange.startDate)}
        </div>

        {isSymbolVisible && <div className={symbolStyle}>-</div>}

        <div className={labelStyle} data-item-type="end">
          {formatDate(timeRange.endDate)}
          {timeBar.right > scrollRight && <span>...</span>}
        </div>
      </div>
    </div>
  );
};

export default React.memo(SelectedTimeRange);

export const style = css`
  position: absolute;
  left: 0;
  background: #0068b7;
  color: #fff;
`;

const labelWrapperStyle = css`
  position: absolute;
  left: 50%;
  display: flex;
  align-items: center;
  min-width: max-content;
  width: 100%;
  height: inherit;
  padding: 0 0.1rem;
  transform: translate(-50%, 0);
`;

const symbolStyle = css`
  margin: 0 auto;
`;

const labelStyle = css`
  min-width: max-content;
  padding: 0.15rem 0.25rem;
  background: rgba(0, 104, 183, 0.9);

  &[data-item-type='start'] {
    margin-right: auto;
  }

  &[data-item-type='end'] {
    margin-left: auto;
  }
`;
