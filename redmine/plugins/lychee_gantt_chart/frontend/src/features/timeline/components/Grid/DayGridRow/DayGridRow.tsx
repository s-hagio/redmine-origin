import * as React from 'react';
import { DayGridColumn as Column } from '../../../models/grid';
import NonWorkingDayGridColumn from './NonWorkingDayGridColumn';
import AdjacentToInvisibleDayGridColumn from './AdjacentToInvisibleDayGridColumn';

type Props = {
  columns: Column[];
};

const DayGridRow: React.FC<Props> = ({ columns }) => {
  return (
    <g>
      {columns.map((column, index) => {
        if (column.isNonWorkingDay) {
          return <NonWorkingDayGridColumn key={column.id} index={index} width={column.width} />;
        }

        if (column.isBeforeInvisibleDay || column.isAfterInvisibleDay) {
          return (
            <AdjacentToInvisibleDayGridColumn
              key={column.id}
              index={index}
              width={column.width}
              isBeforeInvisibleDay={column.isBeforeInvisibleDay}
              isAfterInvisibleDay={column.isAfterInvisibleDay}
            />
          );
        }
      })}
    </g>
  );
};

export default React.memo(DayGridRow);
