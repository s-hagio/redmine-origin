import * as React from 'react';
import { render } from '@testing-library/react';
import { beforeEach } from 'vitest';
import { leafIssue } from '@/__fixtures__';
import MetadataList from '../MetadataList';

const useIssueMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/issue', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIssue: useIssueMock,
}));

vi.mock('@/features/field', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  FieldContent: vi.fn((props) => `FieldContent: ${JSON.stringify(props)}`),
}));

const useIsMetadataVisibleMock = vi.hoisted(() => vi.fn());

vi.mock('../../../models/metadata', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIsMetadataVisible: useIsMetadataVisibleMock,
}));

vi.mock('../../Metadata', () => ({
  default: vi.fn((props) => React.createElement('metadata-mock', props)),
}));

const issue = {
  ...leafIssue,
  subject: 'Metadata Issue!',
  statusId: 123,
  doneRatio: 65,
  assignedToId: 999,
};

beforeEach(() => {
  useIssueMock.mockReturnValue(issue);
});

test('Render', () => {
  const { container } = render(<MetadataList issueId={issue.id} />);

  expect(container).toMatchSnapshot();
});

test('進捗率と担当者はセットされていなければ完全非表示', () => {
  useIssueMock.mockReturnValue({ ...issue, doneRatio: null, assignedToId: null });

  const { container } = render(<MetadataList issueId={issue.id} />);

  expect(container).toMatchSnapshot();
});
