import * as React from 'react';
import { css } from '@linaria/core';
import { IssueID } from '@/models/issue';
import { borderColor } from '@/styles';
import { NakedButton } from '@/components/Button';
import { Icon } from '@/components/Icon';
import { IssueRelationCreation, useActivateIssueRelationCreation } from '../../../models/issueRelation';

type Props = {
  issueId: IssueID;
  type: IssueRelationCreation['type'];
};

const IssueRelationCreationActivateButton: React.FC<Props> = ({ issueId, type }) => {
  const activateIssueRelationCreation = useActivateIssueRelationCreation();

  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      event.preventDefault();

      activateIssueRelationCreation({
        type: type,
        sourceIssueId: issueId,
      });
    },
    [issueId, activateIssueRelationCreation, type]
  );

  return (
    <NakedButton className={style} data-create-type={type} type="button" onClick={handleClick}>
      <Icon name="add" />
    </NakedButton>
  );
};

export default React.memo(IssueRelationCreationActivateButton);

export const width = 17;

export const style = css`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${width}px;
  height: ${width}px;
  font-size: 13px;
  border: 1px solid ${borderColor};
  color: #666;
  border-radius: 3px;
  background: #fff;
`;
