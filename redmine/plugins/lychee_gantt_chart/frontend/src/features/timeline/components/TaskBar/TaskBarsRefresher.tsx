import * as React from 'react';
import { useRefreshTaskBars } from '../../models/taskBar';

const TaskBarsRefresher: React.FC = () => {
  const refreshTaskBars = useRefreshTaskBars();

  React.useEffect(() => {
    refreshTaskBars();
  }, [refreshTaskBars]);

  return null;
};

export default TaskBarsRefresher;
