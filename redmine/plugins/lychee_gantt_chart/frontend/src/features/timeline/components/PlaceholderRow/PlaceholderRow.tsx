import * as React from 'react';
import { css } from '@linaria/core';
import { RowID } from '@/features/row';
import {
  placeholderRowBackgroundColor,
  useIsHotkeyPressedState,
  useIssueCreationOrThrow,
  useCommitIssueCreation,
} from '@/features/issueCreation';
import { getFrontZIndex } from '@/styles';
import Row from '../Row';
import { useIssueTaskBarDraggability } from '../../models/taskBar';
import { IssueTaskBarCreator } from '../IssueTaskBar';

type Props = {
  id: RowID;
};

const PlaceholderRow: React.FC<Props> = ({ id }) => {
  const [isHotkeyPressed] = useIsHotkeyPressedState();

  const issueCreation = useIssueCreationOrThrow();
  const commit = useCommitIssueCreation();

  const { isDraggable } = useIssueTaskBarDraggability({
    ...issueCreation.params,
    rootId: -1,
    lft: 1,
    rgt: 2,
  });

  return (
    <Row id={id} className={style}>
      {isHotkeyPressed && isDraggable && (
        <IssueTaskBarCreator identifier="issue-provisional-0" saveIssue={commit} />
      )}
    </Row>
  );
};

export default React.memo(PlaceholderRow);

const style = css`
  background: ${placeholderRowBackgroundColor};
  z-index: ${getFrontZIndex('issueCreation/IssueCreationPreparer')};
`;
