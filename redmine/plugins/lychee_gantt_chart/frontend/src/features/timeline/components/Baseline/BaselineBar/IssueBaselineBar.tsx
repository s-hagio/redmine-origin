import * as React from 'react';
import { css } from '@linaria/core';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { useIssueBaselineSnapshot } from '@/models/baseline';
import { useBaselineBar } from '../../../models/baseline';
import Metadata, { metadataStyle } from '../../Metadata';
import BaselineBar from './BaselineBar';

type Props = {
  identifier: IssueResourceIdentifier;
};

const IssueBaselineBar: React.FC<Props> = ({ identifier }) => {
  const bar = useBaselineBar(identifier);
  const snapshot = useIssueBaselineSnapshot(identifier);

  if (!bar || !snapshot) {
    return null;
  }

  return (
    <BaselineBar bar={bar}>
      <div className={metaListStyle}>
        <Metadata type="status">{snapshot.status}</Metadata>
        <Metadata type="progressRate">{snapshot.doneRatio ?? 0}%</Metadata>
      </div>
    </BaselineBar>
  );
};

export default IssueBaselineBar;

const metaListStyle = css`
  position: absolute;
  top: 0;
  left: 100%;
  display: flex;
  align-items: center;
  width: 100%;
  height: 100%;
  padding: 0 0 0 0.5rem;
  z-index: 3;

  .${metadataStyle} + .${metadataStyle}::before {
    display: inline-block;
    content: '';
    width: 2px;
    height: 2px;
    margin: 0 0.25em;
    border-radius: 100%;
    background: #333;
  }
`;
