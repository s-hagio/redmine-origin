import * as React from 'react';
import { render } from '@testing-library/react';
import Arrow from '../Arrow';
import { offsetTo } from '../__fixtures__';

test('Render', () => {
  const { container } = render(<Arrow offsetTo={offsetTo} color="#f00" />);

  expect(container).toMatchSnapshot();
});
