import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { useOpenDialog } from '@/features/dialog';
import { Icon } from '@/components/Icon';
import { NakedButton } from '@/components/Button';
import { ICON_SIZE } from '../constants';
import { MORE_CONFIG_DIALOG_ID } from '../../ConfigDialog';

const MoreConfigButton: React.FC = () => {
  const { t } = useTranslation();

  const openConfigDialog = useOpenDialog(MORE_CONFIG_DIALOG_ID);

  return (
    <NakedButton type="button" title={t('label.moreConfig')} onClick={openConfigDialog}>
      <Icon name="moreHoriz" size={ICON_SIZE} />
    </NakedButton>
  );
};

export default MoreConfigButton;
