import * as React from 'react';
import { GroupResourceIdentifier } from '@/models/coreResource';
import { Group } from '@/models/group';

type Props = {
  id: GroupResourceIdentifier;
  group: Group;
};

const GroupRowContent: React.FC<Props> = () => {
  return null;
};

export default React.memo(GroupRowContent);
