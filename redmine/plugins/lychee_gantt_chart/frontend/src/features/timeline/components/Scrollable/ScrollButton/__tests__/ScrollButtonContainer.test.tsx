import * as React from 'react';
import { render } from '@testing-library/react';
import ScrollButtonContainer from '../ScrollButtonContainer';

vi.mock('../ScrollButton', () => ({
  default: vi.fn((props) => `ScrollButton: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<ScrollButtonContainer />);

  expect(container).toMatchSnapshot();
});

test.todo('親要素の下辺が外にある時はposition: fixedになる');
