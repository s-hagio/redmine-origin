import * as React from 'react';
import { useIssue } from '@/models/issue';
import { IssueRow as IssueRowObject } from '@/features/row';
import { RowProps } from '../types';
import Row from '../Row';
import IssueRowContent from './IssueRowContent';

type Props = RowProps<IssueRowObject> & Pick<IssueRowObject, 'subtreeRootIds'>;

const IssueRow: React.FC<Props> = ({ id, resourceId, subtreeRootIds }) => {
  const issue = useIssue(resourceId);

  if (!issue) {
    return null;
  }

  return (
    <Row id={id}>
      <IssueRowContent id={id} subtreeRootIds={subtreeRootIds} issue={issue} />
    </Row>
  );
};

export default React.memo(IssueRow);
