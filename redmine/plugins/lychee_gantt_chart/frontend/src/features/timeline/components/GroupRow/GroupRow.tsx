import * as React from 'react';
import { useGroup } from '@/models/group';
import { GroupRow as GroupRowObject } from '@/features/row';
import { RowProps } from '../types';
import Row from '../Row';
import GroupRowContent from './GroupRowContent';

type Props = RowProps<GroupRowObject>;

const GroupRow: React.FC<Props> = ({ id, resourceId }) => {
  const group = useGroup(resourceId);

  if (!group) {
    return null;
  }

  return (
    <Row id={id}>
      <GroupRowContent id={id} group={group} />
    </Row>
  );
};

export default React.memo(GroupRow);
