import * as React from 'react';
import { css } from '@linaria/core';
import { ProgressLineOffset } from '../../models/progressLine';

type Props = {
  width: number;
  offsets: ProgressLineOffset[];
  color: string;
};

const buildPathCommand = (offset: ProgressLineOffset, command: 'M' | 'L') => {
  return `${command}${offset.left},${offset.top}`;
};

const ProgressLine: React.FC<Props> = ({ width, offsets, color }) => {
  const path = React.useMemo(() => {
    return [
      buildPathCommand(offsets[0], 'M'),
      ...offsets.slice(1).map((offset) => buildPathCommand(offset, 'L')),
    ].join('');
  }, [offsets]);

  if (!path) {
    return null;
  }

  return (
    <svg className={style} style={{ width }}>
      <path d={path} stroke={color} />
    </svg>
  );
};

export default ProgressLine;

export const style = css`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  z-index: 5;

  path {
    fill: none;
    stroke-width: 1px;
    transform: translateX(0.5px);
  }
`;
