import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { Icon } from '@/components/Icon';
import { NakedButton } from '@/components/Button';
import { ICON_SIZE } from '../constants';
import { useSetNeedToCentering } from '../../../models/scroll';

const ScrollToCenterButton: React.FC = () => {
  const { t } = useTranslation();

  const setNeedToCentring = useSetNeedToCentering();

  const handleClick = React.useCallback(() => {
    setNeedToCentring(true);
  }, [setNeedToCentring]);

  return (
    <NakedButton type="button" title={t('timeline.scrollToCenter')} onClick={handleClick}>
      <Icon name="verticalAlignCenter" size={ICON_SIZE + 4} deg={90} />
    </NakedButton>
  );
};

export default ScrollToCenterButton;
