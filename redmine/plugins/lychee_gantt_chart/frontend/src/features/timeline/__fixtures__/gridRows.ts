import { DayGridRow, UnitedGridRow } from '../models/grid';

export const monthGridRow: UnitedGridRow = {
  type: 'month',
  columns: [
    {
      id: 'month-2022-07',
      type: 'month',
      label: '7',
      width: 60,
    },
  ],
};

export const dayGridRow: DayGridRow = {
  type: 'day',
  columns: [
    {
      id: 'day-2022-07-22',
      type: 'day',
      width: 2,
      label: '22',
      isNonWorkingDay: false,
      isBeforeInvisibleDay: false,
      isAfterInvisibleDay: false,
    },
  ],
};
