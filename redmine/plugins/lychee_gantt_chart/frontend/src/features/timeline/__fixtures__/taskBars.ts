import { TaskBar, TaskBarResourceType } from '../models/taskBar';

export const issueTaskBar: TaskBar = {
  resourceType: TaskBarResourceType.Issue,
  top: 10,
  left: 11,
  right: 12,
  width: 101,
  height: 102,
  lateWidth: 50,
  doneWidth: 30,
};
