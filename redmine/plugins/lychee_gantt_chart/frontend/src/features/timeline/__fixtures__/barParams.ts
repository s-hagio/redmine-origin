import { TaskBarParams, TaskBarResourceType } from '../models/taskBar';

export const projectBarParams: TaskBarParams = {
  resourceType: TaskBarResourceType.Project,
  startDate: '2022-01-01',
  endDate: '2022-01-11',
  progressRate: 12,
};

export const versionBarParams: TaskBarParams = {
  resourceType: TaskBarResourceType.Version,
  startDate: '2022-02-02',
  endDate: '2022-02-12',
  progressRate: 23,
};

export const issueBarParams: TaskBarParams = {
  resourceType: TaskBarResourceType.Issue,
  startDate: '2022-03-03',
  endDate: '2022-03-13',
  progressRate: 45,
};
