import { atom } from 'recoil';
import { persistJSON, restorePersistedJSON } from '@/utils/storage';
import { TimelineOptions } from '../types';
import { DEFAULT_OPTIONS, STORED_OPTIONS_KEY } from '../constants';

export const timelineOptionsState = atom<TimelineOptions>({
  key: 'timeline/timelineOptionsState',
  effects: [
    ({ setSelf, onSet }) => {
      setSelf(restorePersistedJSON(STORED_OPTIONS_KEY, DEFAULT_OPTIONS));
      onSet((newValue) => persistJSON(STORED_OPTIONS_KEY, newValue));
    },
  ],
});
