import { atom } from 'recoil';
import { SelectedTimeRange } from '../types';

export const selectedTimeRangeState = atom<SelectedTimeRange | null>({
  key: 'timeline/selectedTimeBarRange',
  default: null,
});
