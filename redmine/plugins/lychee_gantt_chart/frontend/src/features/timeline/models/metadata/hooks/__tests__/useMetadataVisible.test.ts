import { renderRecoilHook } from '@/test-utils';
import { useMetadataVisible } from '../useMetadataVisible';
import { metadataVisibilityState } from '../../states';
import { MetadataType, MetadataVisibility } from '../../types';

const isColumnVisibleMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/dataTable', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIsColumnVisible: vi.fn().mockReturnValue(isColumnVisibleMock),
}));

const renderForTesting = (visibility: MetadataVisibility) => {
  return renderRecoilHook(() => useMetadataVisible(MetadataType.Title), {
    initializeState: ({ set }) => {
      set(metadataVisibilityState(MetadataType.Title), visibility);
    },
  });
};

beforeEach(() => {
  isColumnVisibleMock.mockReturnValue(true);
});

describe('visibility: "hidden"', () => {
  test('常にfalse', () => {
    const { result, rerender } = renderForTesting(MetadataVisibility.Hidden);

    expect(result.current).toBe(false);

    isColumnVisibleMock.mockReturnValue(false);
    rerender();

    expect(result.current).toBe(false);
  });
});

describe('visibility: "visible"', () => {
  test('Visibilityがvisibleならtrue', () => {
    const { result, rerender } = renderForTesting(MetadataVisibility.Visible);

    expect(result.current).toBe(true);

    isColumnVisibleMock.mockReturnValue(false);
    rerender();

    expect(result.current).toBe(true);
  });
});

describe('visibility: visibleOnlyWhenColumnIsHidden', () => {
  test('データ列が非表示の時にtrue', () => {
    const { result, rerender } = renderForTesting(MetadataVisibility.VisibleOnlyWhenColumnIsHidden);

    expect(result.current).toBe(false);

    isColumnVisibleMock.mockReturnValue(false);
    rerender();

    expect(result.current).toBe(true);
  });

  test.each([
    { type: MetadataType.Title, columnName: 'subject' },
    { type: MetadataType.Status, columnName: 'status' },
    { type: MetadataType.ProgressRate, columnName: 'done_ratio' },
    { type: MetadataType.AssignedUser, columnName: 'assigned_to' },
  ])('visibilityが$typeなら$columnNameのデータ列表示をチェック', ({ type, columnName }) => {
    renderRecoilHook(() => useMetadataVisible(type), {
      initializeState: ({ set }) => {
        set(metadataVisibilityState(type), MetadataVisibility.VisibleOnlyWhenColumnIsHidden);
      },
    });

    expect(isColumnVisibleMock).toBeCalledWith(columnName);
  });
});
