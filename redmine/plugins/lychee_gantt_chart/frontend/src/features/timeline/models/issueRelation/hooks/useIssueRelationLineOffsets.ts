import { useRecoilValue } from 'recoil';
import { IssueRelationID } from '@/models/issueRelation';
import { issueRelationLineOffsetsState } from '../states';

export const useIssueRelationLineOffsets = (id: IssueRelationID | null) => {
  return useRecoilValue(issueRelationLineOffsetsState(id));
};
