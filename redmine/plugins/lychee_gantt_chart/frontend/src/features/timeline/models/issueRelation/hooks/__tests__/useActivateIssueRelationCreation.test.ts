import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { useActivateIssueRelationCreation } from '../useActivateIssueRelationCreation';
import { useIssueRelationCreation } from '../useIssueRelationCreation';

test('Activate', () => {
  const { result } = renderRecoilHook(() => ({
    activate: useActivateIssueRelationCreation(),
    issueRelationCreation: useIssueRelationCreation(),
  }));

  expect(result.current.issueRelationCreation).toBeNull();

  act(() => result.current.activate({ type: 'to', sourceIssueId: 123 }));

  expect(result.current.issueRelationCreation).toEqual({
    type: 'to',
    sourceIssueId: 123,
    isLocked: false,
  });
});
