import { TaskBarParams } from '../types';

export const isSameTaskBarParams = (
  aParams: TaskBarParams | null,
  bParams: TaskBarParams | null
): boolean => {
  return (
    (aParams === null && bParams === null) ||
    (!!aParams &&
      !!bParams &&
      aParams.resourceType === bParams.resourceType &&
      aParams.startDate === bParams.startDate &&
      aParams.endDate === bParams.endDate &&
      aParams.progressRate === bParams.progressRate)
  );
};
