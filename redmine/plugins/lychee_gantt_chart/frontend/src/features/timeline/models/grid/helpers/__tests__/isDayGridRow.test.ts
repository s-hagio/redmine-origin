import { isDayGridRow } from '../isDayGridRow';
import { GridType } from '../../types';

const dayGridTypes = ['day', 'weekDay'] as GridType[];
const otherTypes = (Object.values(GridType) as GridType[]).filter((type) => !dayGridTypes.includes(type));

test('オブジェクトがDayGridRowならtrue', () => {
  expect.assertions(2);

  dayGridTypes.forEach((type) => {
    expect(isDayGridRow({ type, columns: [] }), type).toBe(true);
  });
});

test('それ以外はfalse', () => {
  expect.assertions(otherTypes.length);

  otherTypes.forEach((type) => {
    expect(isDayGridRow({ type, columns: [] }), type).toBe(false);
  });
});
