import { toDate } from '@/lib/date';
import { buildYearColumn } from '../buildYearColumn';
import { helpers } from './__fixtures__';

test('YearColumnを構築', () => {
  const groupedTicks = [
    toDate('2022-01-01'),
    toDate('2022-01-02'),
    toDate('2022-06-15'),
    toDate('2022-12-31'),
  ];
  const stepWidth = 1;

  expect(buildYearColumn(groupedTicks, stepWidth, helpers)).toStrictEqual({
    id: 'year-2022',
    type: 'year',
    label: '2022',
    width: 4,
  });
});
