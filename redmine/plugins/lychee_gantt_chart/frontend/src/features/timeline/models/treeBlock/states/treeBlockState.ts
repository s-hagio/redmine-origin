import { selectorFamily } from 'recoil';
import { sortBy } from '@/utils/array';
import { visibleIssueTreeRowsState } from '@/features/row';
import {
  taskBarState,
  absoluteTaskBarVerticalPositionState,
  VerticalPosition,
  HorizontalPosition,
  TaskBar,
} from '../../taskBar';
import { TreeBlock, TreeBlockRootID } from '../types';

export const treeBlockState = selectorFamily<TreeBlock | null, TreeBlockRootID>({
  key: 'timeline/treeBlock',
  get: (rootId) => {
    return ({ get }) => {
      const taskBars: (TaskBar | null)[] = [get(taskBarState(rootId))];

      const horizontalPosition = taskBars.reduce<HorizontalPosition>((memo, bar) => {
        if (bar) {
          return {
            left: Math.min(memo.left ?? bar.left, bar.left),
            right: Math.max(memo.right ?? bar.right, bar.right),
          };
        }

        return memo;
      }, {} as HorizontalPosition);

      if (horizontalPosition.left === horizontalPosition.right) {
        return null;
      }

      const subtreeRows = get(visibleIssueTreeRowsState(rootId));
      const verticalPositions: VerticalPosition[] = [get(absoluteTaskBarVerticalPositionState(rootId))];

      subtreeRows.forEach((row) => {
        verticalPositions.push(get(absoluteTaskBarVerticalPositionState(row.id)));
        taskBars.push(get(taskBarState(row.id)));
      });

      const sortedVerticalPositions = sortBy(verticalPositions, 'bottom');

      return {
        ...horizontalPosition,
        top: sortedVerticalPositions[0].top,
        bottom: sortedVerticalPositions[sortedVerticalPositions.length - 1].bottom,
      };
    };
  },
});
