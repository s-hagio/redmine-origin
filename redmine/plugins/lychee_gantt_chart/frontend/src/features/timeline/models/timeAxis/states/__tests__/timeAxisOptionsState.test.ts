import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { timeAxisOptionsState } from '../../states';
import { DEFAULT_OPTIONS, STORED_OPTIONS_KEY } from '../../constants';
import { TimeAxisOptions } from '../../types';

test('初期化時にlocalStorageに保持しているデータを復元', () => {
  vi.spyOn(localStorage, 'getItem').mockReturnValue('{"type": "foo"}');

  const { result } = renderRecoilHook(() => useRecoilValue(timeAxisOptionsState));

  expect(result.current).toStrictEqual({ ...DEFAULT_OPTIONS, type: 'foo' });
});

test('localStorageのデータがない場合はデフォルト値で初期化', () => {
  vi.spyOn(localStorage, 'getItem').mockReturnValue(null);

  const { result } = renderRecoilHook(() => useRecoilValue(timeAxisOptionsState));

  expect(result.current).toStrictEqual(DEFAULT_OPTIONS);
});

test('セット時にlocalStorageにデータを保存', () => {
  vi.spyOn(localStorage, 'setItem');

  const { result } = renderRecoilHook(() => useSetRecoilState(timeAxisOptionsState));
  const newOptions: TimeAxisOptions = {
    ...DEFAULT_OPTIONS,
    includeNonWorkingDays: !DEFAULT_OPTIONS.includeNonWorkingDays,
  };

  expect(localStorage.setItem).not.toHaveBeenCalled();

  act(() => result.current(newOptions));

  expect(localStorage.setItem).toHaveBeenCalledWith(STORED_OPTIONS_KEY, JSON.stringify(newOptions));
});
