import { selector } from 'recoil';
import { ticksState } from '../states';
import { addVisibleDaysState } from '../../timeAxis';
import { Tick } from '../types';

export type TickAt = (index: number) => Tick;

export const tickAtState = selector<TickAt>({
  key: 'timeline/tickAt',
  get: ({ get }) => {
    const addVisibleDays = get(addVisibleDaysState);
    const ticks = get(ticksState);

    return (index) => {
      if (ticks[index]) {
        return ticks[index];
      }

      if (index < 0) {
        return addVisibleDays(ticks[0], index);
      }

      const lastIndex = ticks.length - 1;

      return addVisibleDays(ticks[lastIndex], Math.abs(index - lastIndex));
    };
  },
});
