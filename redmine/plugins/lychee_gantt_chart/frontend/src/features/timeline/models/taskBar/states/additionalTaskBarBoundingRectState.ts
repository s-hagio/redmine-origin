import { DefaultValue, selectorFamily } from 'recoil';
import { TaskBarAdditionalValueAdderName, TaskBarBoundingRect, TaskBarResourceIdentifier } from '../types';
import { additionalTaskBarBoundingRectMapObjectState } from './additionalTaskBarBoundingRectMapObjectState';

type Param = {
  identifier: TaskBarResourceIdentifier;
  adderName: TaskBarAdditionalValueAdderName;
};

export const additionalTaskBarBoundingRectState = selectorFamily<TaskBarBoundingRect, Param>({
  key: 'timeline/additionalTaskBarBoundingRect',
  get: () => {
    throw new Error('additionalTaskBarBoundingRectState is write only');
  },
  set: ({ identifier, adderName }) => {
    return ({ set }, newValue) => {
      if (!newValue || newValue instanceof DefaultValue) {
        return;
      }

      set(additionalTaskBarBoundingRectMapObjectState(identifier), (current) => {
        return {
          ...current,
          [adderName]: newValue,
        };
      });
    };
  },
});
