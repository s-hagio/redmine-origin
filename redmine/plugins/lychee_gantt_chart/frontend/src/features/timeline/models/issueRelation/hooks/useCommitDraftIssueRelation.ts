import { useCallback } from 'react';
import { RelationType, IssueRelationParams } from '@/models/issueRelation';
import { useCreateIssueRelation } from '@/features/resourceController';
import { useComputeIssueRelationDelay } from './useComputeIssueRelationDelay';
import { useDraftIssueRelation } from './useDraftIssueRelation';
import { useDeactivateIssueRelationCreation } from './useDeactivateIssueRelationCreation';

type OptionalParams = Partial<Pick<IssueRelationParams, 'relationType' | 'delay'>>;

type CommitCreatingIssueRelation = (optionalParams?: OptionalParams) => Awaited<void>;

const reversibleTypeMap: Record<string, RelationType> = {
  [RelationType.Follows]: RelationType.Precedes,
  [RelationType.Blocked]: RelationType.Blocks,
};

export const useCommitDraftIssueRelation = (): CommitCreatingIssueRelation => {
  const computeIssueRelationDelay = useComputeIssueRelationDelay();

  const createIssueRelation = useCreateIssueRelation();
  const draftIssueRelation = useDraftIssueRelation();
  const deactivateIssueRelationCreation = useDeactivateIssueRelationCreation();

  return useCallback(
    (optionalParams) => {
      deactivateIssueRelationCreation();

      if (!draftIssueRelation) {
        return;
      }

      const relationType = optionalParams?.relationType ?? RelationType.Precedes;

      return createIssueRelation({
        relationType: reversibleTypeMap[relationType] ?? relationType,
        issueFromId: draftIssueRelation.issueFromId,
        issueToId: draftIssueRelation.issueToId,
        delay: optionalParams?.delay ?? computeIssueRelationDelay(draftIssueRelation),
      });
    },
    [computeIssueRelationDelay, createIssueRelation, draftIssueRelation, deactivateIssueRelationCreation]
  );
};
