import { act, renderHook } from '@testing-library/react';
import { RelationType } from '@/models/issueRelation';
import { DraftIssueRelation } from '../../types';
import { useCommitDraftIssueRelation } from '../useCommitDraftIssueRelation';

const createIssueRelationMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/resourceController', () => ({
  useCreateIssueRelation: vi.fn().mockReturnValue(createIssueRelationMock),
}));

const deactivateIssueRelationCreationMock = vi.hoisted(() => vi.fn());

vi.mock('../useDeactivateIssueRelationCreation', () => ({
  useDeactivateIssueRelationCreation: vi.fn().mockReturnValue(deactivateIssueRelationCreationMock),
}));

const useDraftIssueRelationMock = vi.hoisted(() => vi.fn());

vi.mock('../useDraftIssueRelation', () => ({
  useDraftIssueRelation: useDraftIssueRelationMock,
}));

const computeIssueRelationDelayMock = vi.hoisted(() => vi.fn());

vi.mock('../useComputeIssueRelationDelay', () => ({
  useComputeIssueRelationDelay: vi.fn().mockReturnValue(computeIssueRelationDelayMock),
}));

const draftIssueRelation: DraftIssueRelation = {
  relationType: RelationType.Draft,
  issueToId: 1,
  issueFromId: 2,
};

beforeEach(() => {
  useDraftIssueRelationMock.mockReturnValue(draftIssueRelation);
});

test('draftIssueRelationと渡したパラメータを合成してIssueRelationを保存', () => {
  const params = {
    relationType: RelationType.Blocks,
    delay: 3,
  };

  const { result } = renderHook(() => useCommitDraftIssueRelation());

  expect(deactivateIssueRelationCreationMock).not.toBeCalled();
  expect(createIssueRelationMock).not.toBeCalled();

  act(() => result.current(params));

  expect(deactivateIssueRelationCreationMock).toBeCalled();
  expect(createIssueRelationMock).toBeCalledWith({ ...draftIssueRelation, ...params });
});

test('draftIssueRelationならdeactivateして終わり', () => {
  useDraftIssueRelationMock.mockReturnValue(null);

  const { result } = renderHook(() => useCommitDraftIssueRelation());

  act(() => result.current());

  expect(deactivateIssueRelationCreationMock).toBeCalled();
  expect(createIssueRelationMock).not.toBeCalled();
});

test('default: relationType, delay', () => {
  computeIssueRelationDelayMock.mockReturnValue(4);

  const { result } = renderHook(() => useCommitDraftIssueRelation());

  act(() => result.current());

  expect(createIssueRelationMock).toBeCalledWith({
    ...draftIssueRelation,
    relationType: RelationType.Precedes,
    delay: 4,
  });

  expect(computeIssueRelationDelayMock).toBeCalledWith(draftIssueRelation);
});
