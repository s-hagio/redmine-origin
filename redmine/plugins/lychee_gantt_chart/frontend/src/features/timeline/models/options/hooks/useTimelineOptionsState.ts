import { useRecoilState } from 'recoil';
import { timelineOptionsState } from '../states';

export const useTimelineOptionsState = () => useRecoilState(timelineOptionsState);
