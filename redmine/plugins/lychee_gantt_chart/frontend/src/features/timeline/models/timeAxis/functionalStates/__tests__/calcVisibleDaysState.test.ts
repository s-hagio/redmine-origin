import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { calcVisibleDaysState } from '../calcVisibleDaysState';
import { timeAxisOptionsState } from '../../states';

const renderForTesting = (includeNonWorkingDays: boolean) => {
  return renderRecoilHook(() => useRecoilValue(calcVisibleDaysState), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({
        ...current,
        nonWorkingDays: ['2022-07-09', '2022-07-10'],
      }));
      set(timeAxisOptionsState, (current) => ({ ...current, includeNonWorkingDays }));
    },
  });
};

describe('「休業日を表示」がOFFの場合', () => {
  test('期間内の表示日数を返す', () => {
    const { result } = renderForTesting(false);

    expect(result.current('2022-07-01', new Date(2022, 6, 20))).toBe(18);
    expect(result.current(new Date(2022, 6, 30), '2022-07-21')).toBe(10);
  });
});

describe('「休業日を表示」がONの場合', () => {
  test('単純に日数差を返す', () => {
    const { result } = renderForTesting(true);

    expect(result.current('2022-07-01', new Date(2022, 6, 20))).toBe(20);
  });
});
