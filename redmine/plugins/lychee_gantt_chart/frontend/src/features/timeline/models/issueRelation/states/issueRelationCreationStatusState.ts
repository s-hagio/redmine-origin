import { selectorFamily } from 'recoil';
import { IssueID } from '@/models/issue';
import { issueRelationCreationState } from './issueRelationCreationState';

export type IssueRelationCreationStatus = {
  isCreating: boolean;
  isSourceIssue: boolean;
  isTargetIssue: boolean;
  isUnrelated: boolean;
  isRelatable: boolean;
};

export const issueRelationCreationStatusState = selectorFamily<IssueRelationCreationStatus, IssueID>({
  key: 'timeline/issueRelationCreationStatus',
  get: (issueId) => {
    return ({ get }) => {
      const issueRelationCreation = get(issueRelationCreationState);

      if (!issueRelationCreation) {
        return {
          isCreating: false,
          isSourceIssue: false,
          isTargetIssue: false,
          isUnrelated: false,
          isRelatable: false,
        };
      }

      const isSourceIssue = issueRelationCreation.sourceIssueId === issueId;
      const isTargetIssue = issueRelationCreation.targetIssueId === issueId;

      return {
        isCreating: true,
        isSourceIssue,
        isTargetIssue,
        isUnrelated: !isSourceIssue && !isTargetIssue,
        isRelatable: !isSourceIssue,
      };
    };
  },
});
