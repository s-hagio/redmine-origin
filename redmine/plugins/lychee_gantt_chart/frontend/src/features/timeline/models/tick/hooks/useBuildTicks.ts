import { useCallback } from 'react';
import { sortBy } from '@/utils/array';
import { addDays, DateArgument } from '@/lib/date';
import { TimeDirection, useEnsureVisibleDay } from '../../timeAxis';
import { Tick } from '../types';

type TicksBuildOptions = Partial<{
  directions: TimeDirection[];
  sizePerDirection: number;
  includeBaseDate: boolean;
}>;

type BuildTicks = (baseDate: DateArgument, options?: TicksBuildOptions) => Tick[];

const DEFAULT_OPTIONS = {
  directions: [TimeDirection.Forward, TimeDirection.Backward],
  sizePerDirection: 500,
  includeBaseDate: true,
};

export const useBuildTicks = (): BuildTicks => {
  const ensureVisibleDay = useEnsureVisibleDay();

  return useCallback(
    (baseDate, options) => {
      const start = ensureVisibleDay(baseDate, TimeDirection.Backward);
      const opts = { ...DEFAULT_OPTIONS, ...options };

      const ticks = [];

      if (opts.includeBaseDate) {
        ticks.push(start);
      }

      opts.directions.forEach((direction) => {
        let current = start;
        let remainingTicks = opts.sizePerDirection;

        while (remainingTicks > 0) {
          current = ensureVisibleDay(addDays(current, direction), direction);
          ticks.push(current);
          --remainingTicks;
        }
      });

      return sortBy(ticks, Number);
    },
    [ensureVisibleDay]
  );
};
