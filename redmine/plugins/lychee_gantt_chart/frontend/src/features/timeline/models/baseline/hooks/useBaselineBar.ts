import { useRecoilValue } from 'recoil';
import { baselineBarState } from '../states';
import { BaselineSnapshotResourceIdentifier } from '../types';

export const useBaselineBar = (identifier: BaselineSnapshotResourceIdentifier) => {
  return useRecoilValue(baselineBarState(identifier));
};
