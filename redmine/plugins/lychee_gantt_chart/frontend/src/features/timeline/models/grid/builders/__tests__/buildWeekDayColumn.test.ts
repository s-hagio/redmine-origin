import { toDate } from '@/lib/date';
import { buildWeekDayColumn } from '../buildWeekDayColumn';
import { helpers } from './__fixtures__';

vi.mock('../buildDayAttributes', () => ({
  buildDayAttributes: vi.fn().mockReturnValue({ __DAY_ATTRIBUTES__: 'day attributes...' }),
}));

const dayAttributesMock = { __DAY_ATTRIBUTES__: 'day attributes...' };

test('WeekDayColumnを構築', () => {
  const stepWidth = 2;

  expect(buildWeekDayColumn(toDate('2022-07-03'), stepWidth, helpers)).toStrictEqual({
    id: 'weekDay-2022-07-03',
    type: 'weekDay',
    labelPath: 'date.dayNames.0',
    width: stepWidth,
    ...dayAttributesMock,
  });

  expect(buildWeekDayColumn(toDate('2022-07-04'), stepWidth, helpers)).toStrictEqual({
    id: 'weekDay-2022-07-04',
    type: 'weekDay',
    labelPath: 'date.dayNames.1',
    width: stepWidth,
    ...dayAttributesMock,
  });

  expect(buildWeekDayColumn(toDate('2022-07-05'), stepWidth, helpers)).toStrictEqual({
    id: 'weekDay-2022-07-05',
    type: 'weekDay',
    labelPath: 'date.dayNames.2',
    width: stepWidth,
    ...dayAttributesMock,
  });

  expect(buildWeekDayColumn(toDate('2022-07-06'), stepWidth, helpers)).toStrictEqual({
    id: 'weekDay-2022-07-06',
    type: 'weekDay',
    labelPath: 'date.dayNames.3',
    width: stepWidth,
    ...dayAttributesMock,
  });

  expect(buildWeekDayColumn(toDate('2022-07-07'), stepWidth, helpers)).toStrictEqual({
    id: 'weekDay-2022-07-07',
    type: 'weekDay',
    labelPath: 'date.dayNames.4',
    width: stepWidth,
    ...dayAttributesMock,
  });

  expect(buildWeekDayColumn(toDate('2022-07-08'), stepWidth, helpers)).toStrictEqual({
    id: 'weekDay-2022-07-08',
    type: 'weekDay',
    labelPath: 'date.dayNames.5',
    width: stepWidth,
    ...dayAttributesMock,
  });

  expect(buildWeekDayColumn(toDate('2022-07-09'), stepWidth, helpers)).toStrictEqual({
    id: 'weekDay-2022-07-09',
    type: 'weekDay',
    labelPath: 'date.dayNames.6',
    width: stepWidth,
    ...dayAttributesMock,
  });
});
