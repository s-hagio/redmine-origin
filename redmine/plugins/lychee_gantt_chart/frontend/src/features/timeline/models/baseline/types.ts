import {
  IssueResourceIdentifier,
  ProjectResourceIdentifier,
  VersionResourceIdentifier,
} from '@/models/coreResource';
import { IssueRow, ProjectRow, VersionRow } from '@/features/row';
import { TaskBar } from '../taskBar';

export type BaselineSnapshotResourceRow = ProjectRow | VersionRow | IssueRow;

export type BaselineSnapshotResourceIdentifier =
  | ProjectResourceIdentifier
  | VersionResourceIdentifier
  | IssueResourceIdentifier;

export type BaselineBar = TaskBar & {
  isVisible: boolean;
};

export type BaselineBarVisibleMap = ReadonlyMap<BaselineSnapshotResourceIdentifier, boolean>;
