import { useRecoilValue } from 'recoil';
import { totalTickWidthState } from '../states';

export const useTotalTickWidth = () => useRecoilValue(totalTickWidthState);
