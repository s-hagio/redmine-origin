import { TaskBarResourceType, TaskBarBoundingRect } from '../types';

const relativeBoundingRectsByType: Record<TaskBarResourceType, TaskBarBoundingRect> = {
  project: { top: 14, height: 3 },
  version: { top: 14, height: 3 },
  issue: { top: 5, height: 20 },
} as const;

export const getTaskBarRelativeBoundingRect = (resourceType: TaskBarResourceType): TaskBarBoundingRect => {
  return {
    ...relativeBoundingRectsByType[resourceType],
  };
};
