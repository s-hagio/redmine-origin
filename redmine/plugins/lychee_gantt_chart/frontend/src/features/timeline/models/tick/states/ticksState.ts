import { atom } from 'recoil';
import { Tick } from '../types';

export const ticksState = atom<Tick[]>({
  key: 'timeline/ticks',
  default: [],
});
