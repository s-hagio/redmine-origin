import { atom } from 'recoil';
import { IssueRelationID } from '@/models/issueRelation';

export const selectedIssueRelationIdState = atom<IssueRelationID | null>({
  key: 'timeline/selectedIssueRelationId',
  default: null,
});
