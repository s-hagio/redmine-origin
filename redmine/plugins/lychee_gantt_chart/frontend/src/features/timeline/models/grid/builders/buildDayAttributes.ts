import { addDays } from '@/lib/date';
import { DayGridColumn } from '../types';
import { Tick } from '../../tick';
import { BuildHelpers } from './types';

type DayAttributes = Pick<DayGridColumn, 'isNonWorkingDay' | 'isBeforeInvisibleDay' | 'isAfterInvisibleDay'>;

export const buildDayAttributes = (tick: Tick, helpers: BuildHelpers): DayAttributes => {
  const isNonWorkingDay = helpers.isNonWorkingDay(tick);

  return {
    isNonWorkingDay,
    isBeforeInvisibleDay: !isNonWorkingDay && !helpers.isVisibleDay(addDays(tick, 1)),
    isAfterInvisibleDay: !isNonWorkingDay && !helpers.isVisibleDay(addDays(tick, -1)),
  };
};
