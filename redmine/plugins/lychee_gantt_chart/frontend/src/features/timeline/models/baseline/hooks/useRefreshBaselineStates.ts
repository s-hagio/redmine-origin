import { useRecoilCallback, useRecoilValue } from 'recoil';
import { useSelectedBaseline } from '@/models/baseline';
import { additionalRowHeightMapObjectState } from '@/features/row';
import { buildTaskBarState, taskBarMapState } from '../../taskBar';
import { BaselineBar, BaselineSnapshotResourceIdentifier } from '../types';
import { baselineBarMapState, baselineBarParamsState, baselineBarState } from '../states';
import { getBaselineBarRelativeBoundingRect, isBaselineSnapshotResourceIdentifier } from '../helpers';

const FEATURE_NAME = 'baselineBar';

export const useRefreshBaselineStates = () => {
  const baseline = useSelectedBaseline();
  const taskBarMap = useRecoilValue(taskBarMapState);

  return useRecoilCallback(
    ({ snapshot, transact_UNSTABLE }) => {
      return () => {
        const baselineBarMap = new Map<BaselineSnapshotResourceIdentifier, BaselineBar | null>();
        const baselineRowHeightMap = new Map<BaselineSnapshotResourceIdentifier, number>();

        taskBarMap.forEach((taskBar, identifier) => {
          if (!taskBar || !isBaselineSnapshotResourceIdentifier(identifier)) {
            return;
          }

          baselineBarMap.set(identifier, null);
          baselineRowHeightMap.set(identifier, 0);

          if (!baseline) {
            return;
          }

          const baselineBarParams = snapshot.getLoadable(baselineBarParamsState(identifier)).getValue();

          if (!baselineBarParams) {
            return;
          }

          const buildTaskBar = snapshot.getLoadable(buildTaskBarState(identifier)).getValue();
          const baselineBar = {
            ...buildTaskBar(baselineBarParams),
            isVisible: false,
          };

          baselineBarMap.set(identifier, baselineBar);

          if (taskBar.left === baselineBar.left && taskBar.right === baselineBar.right) {
            return;
          }

          const bounds = getBaselineBarRelativeBoundingRect(identifier);
          const additionalTop = bounds.top + bounds.height;

          baselineBar.top += additionalTop;
          baselineBar.isVisible = true;

          baselineRowHeightMap.set(identifier, additionalTop);
        });

        transact_UNSTABLE(({ set }) => {
          set(baselineBarMapState, baselineBarMap);

          baselineBarMap.forEach((baselineBar, rowId) => {
            set(baselineBarState(rowId), baselineBar);
          });

          baselineRowHeightMap.forEach((height, rowId) => {
            set(additionalRowHeightMapObjectState(rowId), (current) => ({
              ...current,
              [FEATURE_NAME]: height,
            }));
          });
        });
      };
    },
    [baseline, taskBarMap]
  );
};
