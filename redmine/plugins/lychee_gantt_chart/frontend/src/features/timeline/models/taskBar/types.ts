import {
  ProjectResourceIdentifier,
  VersionResourceIdentifier,
  IssueResourceIdentifier,
} from '@/models/coreResource';
import { TimeBar, TimeBarRange } from '../timeBar';

export type TaskBarResourceIdentifier =
  | ProjectResourceIdentifier
  | VersionResourceIdentifier
  | IssueResourceIdentifier;

export const TaskBarResourceType = {
  Project: 'project',
  Version: 'version',
  Issue: 'issue',
} as const;

export type TaskBarResourceType = (typeof TaskBarResourceType)[keyof typeof TaskBarResourceType];

export type TaskBarBoundingRect = {
  top: number;
  height: number;
};

export type TaskBarAdditionalValueAdderName = string;

export type TaskBarParams = TimeBarRange & {
  resourceType: TaskBarResourceType;
  progressRate: number;
};

export type TaskBar = TimeBar &
  TaskBarBoundingRect & {
    resourceType: TaskBarResourceType;
    lateWidth: number;
    doneWidth: number;
  };

export type MovedRangeValues = {
  start?: number;
  end?: number;
  both?: number;
};

export type VerticalPosition = {
  top: number;
  middle: number;
  bottom: number;
};

export type HorizontalPosition = {
  left: number;
  right: number;
};
