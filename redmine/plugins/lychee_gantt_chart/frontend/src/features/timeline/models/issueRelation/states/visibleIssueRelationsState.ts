import { selector } from 'recoil';
import { issueRelationsState } from '@/models/issueRelation';
import { availableRowsState, visibleRowsState } from '@/features/row';
import { pluckIssueIdsFromRows } from '../helpers';

export const visibleIssueRelationsState = selector({
  key: 'timeline/visibleIssueRelations',
  get: ({ get }) => {
    const availableRows = get(availableRowsState);
    const visibleRows = get(visibleRowsState);
    const issueRelations = get(issueRelationsState);

    const availableIssueIds = new Set(pluckIssueIdsFromRows(availableRows));
    const visibleIssueIds = new Set(pluckIssueIdsFromRows(visibleRows));

    return issueRelations.filter(({ issueFromId, issueToId, isDeleting }) => {
      return (
        !isDeleting &&
        availableIssueIds.has(issueFromId) &&
        availableIssueIds.has(issueToId) &&
        (visibleIssueIds.has(issueFromId) || visibleIssueIds.has(issueToId))
      );
    });
  },
});
