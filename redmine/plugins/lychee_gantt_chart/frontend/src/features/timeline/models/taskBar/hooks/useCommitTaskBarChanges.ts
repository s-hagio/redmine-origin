import { useRecoilCallback } from 'recoil';
import { formatISODate } from '@/lib/date';
import {
  getResourceIdFromIdentifier,
  isIssueResourceIdentifier,
  IssueResourceIdentifier,
} from '@/models/coreResource';
import { BatchIssueParams, issueEntityState } from '@/models/issue';
import { useIsEditableField } from '@/models/fieldRule';
import { useBulkUpdateIssues, useSaveIssue } from '@/features/resourceController';
import { selectedRowIdsState } from '@/features/row';
import { taskBarState } from '../states';
import { useGetTickByPosition } from '../../tick';
import { useStepWidth } from '../../timeAxis';

type CommitTaskBarChanges = () => Promise<void>;

export const useCommitTaskBarChanges = (
  draggingTaskBarIdentifier: IssueResourceIdentifier
): CommitTaskBarChanges => {
  const isEditableField = useIsEditableField();
  const stepWidth = useStepWidth();
  const getTickByPosition = useGetTickByPosition();

  const saveIssue = useSaveIssue();
  const bukUpdateIssues = useBulkUpdateIssues();

  return useRecoilCallback(
    ({ snapshot }) => {
      return async () => {
        const selectedIssueRowIds = snapshot.getLoadable(selectedRowIdsState).getValue();

        const targetIdentifiers = selectedIssueRowIds.includes(draggingTaskBarIdentifier)
          ? selectedIssueRowIds.filter(isIssueResourceIdentifier)
          : [draggingTaskBarIdentifier];

        const issues = targetIdentifiers.reduce((memo, identifier) => {
          const id = getResourceIdFromIdentifier(identifier);

          const issue = snapshot.getLoadable(issueEntityState(id)).getValue();
          const taskBar = snapshot.getLoadable(taskBarState(identifier)).getValue();

          if (!issue || !taskBar) {
            return memo;
          }

          const params: BatchIssueParams = { id };

          if (isEditableField(issue, 'startDate')) {
            params.startDate = formatISODate(getTickByPosition(taskBar.left));
          }

          if (isEditableField(issue, 'dueDate')) {
            params.dueDate = formatISODate(getTickByPosition(taskBar.right - stepWidth));
          }

          if (params.startDate || params.dueDate) {
            memo.push(params);
          }

          return memo;
        }, [] as BatchIssueParams[]);

        if (issues.length === 1) {
          await saveIssue(issues[0]);
        } else {
          await bukUpdateIssues(issues);
        }
      };
    },
    [isEditableField, stepWidth, getTickByPosition, saveIssue, bukUpdateIssues, draggingTaskBarIdentifier]
  );
};
