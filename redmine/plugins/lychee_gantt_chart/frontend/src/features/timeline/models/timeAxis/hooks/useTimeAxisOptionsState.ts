import { useRecoilState } from 'recoil';
import { timeAxisOptionsState } from '../states';

export const useTimeAxisOptionsState = () => useRecoilState(timeAxisOptionsState);
