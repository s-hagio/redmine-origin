import { atomFamily } from 'recoil';
import { BaselineBar, BaselineSnapshotResourceIdentifier } from '../types';

export const baselineBarState = atomFamily<BaselineBar | null, BaselineSnapshotResourceIdentifier>({
  key: 'timeline/baselineBar',
  default: null,
});
