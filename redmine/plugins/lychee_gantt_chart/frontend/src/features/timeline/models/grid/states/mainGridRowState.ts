import { selector } from 'recoil';
import { GridRow } from '../types';
import { gridRowsState } from './gridRowsState';

export const mainGridRowState = selector<GridRow | null>({
  key: 'timeline/mainGridRow',
  get: ({ get }) => {
    const rows = get(gridRowsState);

    return rows[rows.length - 1] || null;
  },
});
