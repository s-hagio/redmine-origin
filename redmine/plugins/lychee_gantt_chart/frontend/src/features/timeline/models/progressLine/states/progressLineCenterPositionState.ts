import { selector } from 'recoil';
import { todayTickEndPositionState } from '../../tick';

export const progressLineCenterPositionState = selector<number>({
  key: 'timeline/progressLineCenterPosition',
  get: ({ get }) => {
    return get(todayTickEndPositionState);
  },
});
