import { useCallback } from 'react';
import { useStepWidth } from '../../timeAxis';
import { Tick } from '../types';
import { useTickAt } from './useTickAt';

type GetTickByPosition = (position: number) => Tick;

export const useGetTickByPosition = (): GetTickByPosition => {
  const stepWidth = useStepWidth();
  const tickAt = useTickAt();

  return useCallback(
    (position) => {
      return tickAt(Math.floor(position / stepWidth));
    },
    [stepWidth, tickAt]
  );
};
