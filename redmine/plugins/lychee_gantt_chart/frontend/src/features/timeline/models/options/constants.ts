export const DEFAULT_OPTIONS = {
  showRelationLine: true,
  showProgressLine: false,
};

export const STORED_OPTIONS_KEY = 'lycheeGantt:timelineOptions';
