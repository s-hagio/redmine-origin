import { TimeAxisType } from '../types';

const stepWidthsByType = {
  [TimeAxisType.MonthSmall]: 1,
  [TimeAxisType.MonthLarge]: 2,
  [TimeAxisType.Week]: 4,
  [TimeAxisType.DaySmall]: 8,
  [TimeAxisType.DayLarge]: 16,
};

export const getStepWidth = (type: TimeAxisType) => {
  if (stepWidthsByType[type] == null) {
    throw new Error(`Unknown TimeAxis type: ${type}`);
  }

  return stepWidthsByType[type];
};
