import { atom } from 'recoil';
import { BaselineBar, BaselineSnapshotResourceIdentifier } from '../types';

export const baselineBarMapState = atom<ReadonlyMap<BaselineSnapshotResourceIdentifier, BaselineBar | null>>({
  key: 'timeline/baselineBarMap',
  default: new Map(),
});
