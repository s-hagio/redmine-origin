import { useRecoilState } from 'recoil';
import { selectedIssueRelationIdState } from '../states';

export const useSelectedIssueRelationIdState = () => useRecoilState(selectedIssueRelationIdState);
