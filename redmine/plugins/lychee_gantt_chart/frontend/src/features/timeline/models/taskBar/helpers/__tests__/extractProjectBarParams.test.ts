import { Project } from '@/models/project';
import { rootProject } from '@/__fixtures__';
import { TaskBarResourceType } from '../../types';
import { extractProjectBarParams } from '../extractProjectBarParams';

const project: Project = {
  ...rootProject,
  startDate: '2022-01-01',
  dueDate: '2022-01-05',
  completedPercent: 25,
};

test('ProjectからTaskBarParamsを抽出する', () => {
  expect(extractProjectBarParams(project)).toStrictEqual({
    resourceType: TaskBarResourceType.Project,
    startDate: project.startDate,
    endDate: project.dueDate,
    progressRate: project.completedPercent,
  });
});

test('TaskBarParamsを持たない場合はnullを返す', () => {
  expect(extractProjectBarParams({ ...project, startDate: null })).toBeNull();
  expect(extractProjectBarParams({ ...project, dueDate: null })).toBeNull();
  expect(extractProjectBarParams({ ...project, startDate: null, dueDate: null })).toBeNull();
});
