export const MetadataVisibility = {
  Hidden: 'hidden',
  Visible: 'visible',
  VisibleOnlyWhenColumnIsHidden: 'visibleOnlyWhenColumnIsHidden',
} as const;

export type MetadataVisibility = (typeof MetadataVisibility)[keyof typeof MetadataVisibility];

export const MetadataType = {
  Title: 'title',
  Status: 'status',
  ProgressRate: 'progressRate',
  AssignedUser: 'assignedUser',
} as const;

export type MetadataType = (typeof MetadataType)[keyof typeof MetadataType];

export type MetadataVisibilityCollection = {
  title: MetadataVisibility;
  status: MetadataVisibility;
  progressRate: MetadataVisibility;
  assignedUser: MetadataVisibility;
};
