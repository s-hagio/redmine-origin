import { constSelector, RecoilValue, useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { IssueRelation, RelationType } from '@/models/issueRelation';
import {
  grandchildIssueRow,
  grandchildProjectIssueRow,
  mainProjectRow,
  mainProjectVersionRow,
  otherTreeIssueRow,
  parentIssueRow,
  rootProjectRow,
  versionIssueRow,
} from '@/__fixtures__';
import { visibleIssueRelationsState } from '../visibleIssueRelationsState';

const availableRowsStateMock = vi.hoisted(() => vi.fn());
const visibleRowsStateMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/row', async (importOriginal) => {
  return Object.defineProperties(await importOriginal<object>(), {
    availableRowsState: { get: availableRowsStateMock },
    visibleRowsState: { get: visibleRowsStateMock },
  });
});

const issueRelationsStateMock = vi.hoisted(() => vi.fn<never, RecoilValue<IssueRelation[]>>());

vi.mock('@/models/issueRelation', async (importOriginal) => {
  return Object.defineProperties(await importOriginal<object>(), {
    issueRelationsState: { get: issueRelationsStateMock },
  });
});

beforeEach(() => {
  availableRowsStateMock.mockReturnValue(
    constSelector([rootProjectRow, mainProjectRow, mainProjectVersionRow, ...issueRows])
  );

  visibleRowsStateMock.mockReturnValue(
    constSelector([mainProjectRow, mainProjectVersionRow, parentIssueRow, issueRows[0], issueRows[1]])
  );

  issueRelationsStateMock.mockReturnValue(constSelector(issueRelations));
});

const issueRows = [grandchildIssueRow, otherTreeIssueRow, versionIssueRow, grandchildProjectIssueRow];

const baseRelation = { relationType: RelationType.Precedes, delay: 0, isDeleting: false };
const issueRelations = [
  // issueFromが表示
  { ...baseRelation, id: 1, issueFromId: issueRows[0].resourceId, issueToId: issueRows[2].resourceId },
  // issueToが表示
  { ...baseRelation, id: 2, issueFromId: issueRows[2].resourceId, issueToId: issueRows[1].resourceId },
  // issueFromが表示されているがisDeletingがtrue
  {
    ...baseRelation,
    id: 3,
    issueFromId: issueRows[1].resourceId,
    issueToId: issueRows[3].resourceId,
    isDeleting: true,
  },
  // どちらも表示されていない
  { ...baseRelation, id: 4, issueFromId: issueRows[2].resourceId, issueToId: issueRows[3].resourceId },
];

test('表示可能なIssueRelationの配列', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(visibleIssueRelationsState));

  expect(result.current).toStrictEqual([issueRelations[0], issueRelations[1]]);
});
