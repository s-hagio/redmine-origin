export type ProgressLineOffset = {
  top: number;
  left: number;
};
