import { useRef } from 'react';
import { useRecoilCallback, useRecoilValue } from 'recoil';
import { Tick, ticksState } from '../../tick';
import { gridRowsState } from '../states';
import { useBuildGridRows } from './useBuildGridRows';

export const useRefreshGrid = () => {
  const ticks = useRecoilValue(ticksState);
  const buildGridRows = useBuildGridRows();
  const previousTicksRef = useRef<Tick[] | null>(null);

  return useRecoilCallback(
    ({ set }) => {
      return () => {
        // 更新が複数回走ることがあるのを抑制
        // TODO: もっと上の層で抑制できるか検討
        if (previousTicksRef.current !== ticks) {
          set(gridRowsState, buildGridRows(ticks));
        }
      };
    },
    [ticks, buildGridRows]
  );
};
