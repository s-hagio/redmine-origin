import { TaskBarResourceIdentifier, TaskBarResourceType } from '../types';

export const getTaskBarResourceTypeFromIdentifier = (
  identifier: TaskBarResourceIdentifier
): TaskBarResourceType => {
  const [resourceType] = identifier.split('-');

  return resourceType as TaskBarResourceType;
};
