import { toDate } from '@/lib/date';
import { buildYearMonthColumn } from '../buildYearMonthColumn';
import { helpers } from './__fixtures__';

test('YearMonthColumnを構築', () => {
  const groupedTicks = [toDate('2022-07-01'), toDate('2022-07-02'), toDate('2022-07-31')];
  const stepWidth = 2;

  expect(buildYearMonthColumn(groupedTicks, stepWidth, helpers)).toStrictEqual({
    id: 'yearMonth-2022-7',
    type: 'yearMonth',
    label: '2022-7',
    width: 6,
  });
});
