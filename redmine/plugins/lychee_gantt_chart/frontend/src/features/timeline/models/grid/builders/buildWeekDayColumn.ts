import { formatISODate } from '@/lib/date';
import { GridType } from '../types';
import { BuildDayColumn } from './types';
import { buildDayAttributes } from './buildDayAttributes';

export const buildWeekDayColumn: BuildDayColumn = (tick, stepWidth, helpers) => {
  return {
    id: `${GridType.WeekDay}-${formatISODate(tick)}`,
    type: GridType.WeekDay,
    labelPath: `date.dayNames.${tick.getDay()}`,
    width: stepWidth,
    ...buildDayAttributes(tick, helpers),
  };
};
