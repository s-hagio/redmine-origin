import { useCallback } from 'react';
import { DateArgument } from '@/lib/date';
import { useAddVisibleDays, useStepWidth } from '../../timeAxis';

export type ComputeMovedDate = (arg: DateArgument, movedValue: number) => Date;

export const useComputeMovedDate = (): ComputeMovedDate => {
  const stepWidth = useStepWidth();
  const addVisibleDays = useAddVisibleDays();

  return useCallback(
    (arg, movedValue) => {
      return addVisibleDays(arg, Math.round(movedValue / stepWidth));
    },
    [stepWidth, addVisibleDays]
  );
};
