import { useRecoilValue } from 'recoil';
import { scrollLeftState } from '../states';

export const useScrollLeft = () => useRecoilValue(scrollLeftState);
