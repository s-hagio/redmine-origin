import { useRecoilValue } from 'recoil';
import { positionOfState } from '../functionalStates';

export const usePositionOf = () => useRecoilValue(positionOfState);

