import { useRecoilCallback } from 'recoil';
import { IssueID } from '@/models/issue';
import { issueRelationCreationState } from '../states';

type SetIssueRelationCreationTarget = (targetIssueId: IssueID | null, options?: Options) => void;

type Options = {
  lock?: boolean;
  force?: boolean;
};

export const useSetIssueRelationCreationTarget = (): SetIssueRelationCreationTarget => {
  return useRecoilCallback(({ set }) => {
    return (targetIssueId, options = {}) => {
      set(issueRelationCreationState, (current) => {
        if (!current) {
          return null;
        }

        if (current.isLocked && !options.force) {
          return current;
        }

        return {
          ...current,
          targetIssueId,
          isLocked: options.lock ?? current.isLocked,
        };
      });
    };
  });
};
