import { formatISODate } from '@/lib/date';
import { GridType } from '../types';
import { BuildDayColumn } from './types';
import { buildDayAttributes } from './buildDayAttributes';

export const buildDayColumn: BuildDayColumn = (tick, stepWidth, helpers) => ({
  id: `${GridType.Day}-${formatISODate(tick)}`,
  type: GridType.Day,
  label: tick.getDate() + '',
  width: stepWidth,
  ...buildDayAttributes(tick, helpers),
});
