import { renderHook } from '@testing-library/react';
import { addDays, toDate } from '@/lib/date';
import { useComputeMovedRange } from '../useComputeMovedRange';

const addVisibleDaysMock = vi.hoisted(() => vi.fn());

vi.mock('../../../timeAxis', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useStepWidth: vi.fn().mockReturnValue(5),
  useAddVisibleDays: vi.fn().mockReturnValue(addVisibleDaysMock),
}));

// 可視日の追加処理をaddDaysに置き換え
addVisibleDaysMock.mockImplementation(addDays);

// 基本の日付範囲
const range = { startDate: '2023-09-10', endDate: '2023-09-15' };

test('範囲の移動量からそれぞれの移動日数を計算して適用した日付範囲を返す', () => {
  const { result } = renderHook(() => useComputeMovedRange());

  expect(addVisibleDaysMock).toBeCalledTimes(0);

  expect(result.current(range, { both: 12 })).toEqual({
    startDate: addDays(range.startDate, 2),
    endDate: addDays(range.endDate, 2),
  });

  expect(result.current(range, { both: -99 })).toEqual({
    startDate: addDays(range.startDate, -20),
    endDate: addDays(range.endDate, -20),
  });

  expect(addVisibleDaysMock).toBeCalledTimes(4);
});

test('start/end片方を指定した場合は指定した方向にだけ変化（移動）する', () => {
  const { result } = renderHook(() => useComputeMovedRange());

  expect(result.current(range, { start: -10 })).toEqual({
    startDate: addDays(range.startDate, -2),
    endDate: addDays(range.endDate, 0),
  });

  expect(result.current(range, { end: 15 })).toEqual({
    startDate: addDays(range.startDate, 0),
    endDate: addDays(range.endDate, 3),
  });
});

describe('計算後にstartDateがendDateより大きくなる場合', () => {
  const { result } = renderHook(() => useComputeMovedRange());

  test('start方向への移動ならstartDateをendDateと同値にする', () => {
    expect(result.current(range, { start: 100 })).toEqual({
      startDate: toDate(range.endDate),
      endDate: toDate(range.endDate),
    });
  });

  test('end方向への移動ならendDateをstartDateと同値にする', () => {
    expect(result.current(range, { end: -100 })).toEqual({
      startDate: toDate(range.startDate),
      endDate: toDate(range.startDate),
    });
  });
});
