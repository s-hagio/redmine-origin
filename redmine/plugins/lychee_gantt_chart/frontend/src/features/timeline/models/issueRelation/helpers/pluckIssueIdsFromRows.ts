import { IssueID } from '@/models/issue';
import { IssueRow, Row, RowResourceType } from '@/features/row';

export const pluckIssueIdsFromRows = (rows: Row[]): IssueID[] => {
  return rows
    .filter((row): row is IssueRow => row.resourceType === RowResourceType.Issue)
    .map((row) => row.resourceId);
};
