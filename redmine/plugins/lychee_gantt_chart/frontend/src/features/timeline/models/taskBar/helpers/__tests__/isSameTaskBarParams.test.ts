import { TaskBarParams } from '../../types';
import { isSameTaskBarParams } from '../isSameTaskBarParams';

const params: TaskBarParams = {
  resourceType: 'project',
  startDate: '2021-01-01',
  endDate: '2021-01-02',
  progressRate: 0,
};

test('二つのTaskBarParamsのリソース種別／開始日／終了日／進捗率が同じならtrue', () => {
  expect(isSameTaskBarParams(params, { ...params })).toBe(true);
});

test('二つともnullならtrue', () => {
  expect(isSameTaskBarParams(null, null)).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isSameTaskBarParams(params, { ...params, resourceType: 'issue' })).toBe(false);
  expect(isSameTaskBarParams(params, { ...params, startDate: '2021-01-02' })).toBe(false);
  expect(isSameTaskBarParams(params, { ...params, endDate: '2021-01-04' })).toBe(false);
  expect(isSameTaskBarParams(params, { ...params, progressRate: 100 })).toBe(false);
});
