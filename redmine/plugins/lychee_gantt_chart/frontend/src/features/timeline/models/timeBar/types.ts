import { DateArgument } from '@/lib/date';

export type TimeBarRange = {
  startDate: DateArgument;
  endDate: DateArgument;
};

export type TimeBar = {
  width: number;
  left: number;
  right: number;
};

export type SelectedTimeRange = TimeBarRange & {
  showGuideline?: boolean;
  lock?: boolean;
};

export type SelectedTimeBar = TimeBar & Pick<SelectedTimeRange, 'showGuideline'>;
