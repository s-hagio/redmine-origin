import { selectorFamily } from 'recoil';
import { TaskBarBoundingRect, TaskBarResourceIdentifier } from '../types';
import { additionalTaskBarBoundingRectMapObjectState } from './additionalTaskBarBoundingRectMapObjectState';

export const additionalTaskBarBoundingRectsState = selectorFamily<
  TaskBarBoundingRect[],
  TaskBarResourceIdentifier
>({
  key: 'timeline/additionalTaskBarBoundingRects',
  get: (identifier) => {
    return ({ get }) => {
      const mapObject = get(additionalTaskBarBoundingRectMapObjectState(identifier));

      return Object.values(mapObject);
    };
  },
});
