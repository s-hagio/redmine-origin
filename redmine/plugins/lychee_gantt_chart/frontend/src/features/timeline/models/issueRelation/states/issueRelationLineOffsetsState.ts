import { selectorFamily } from 'recoil';
import { IssueRelationID, issueRelationState } from '@/models/issueRelation';
import { getIssueResourceIdentifier } from '@/models/coreResource';
import { rowBoundingRectMapState } from '@/features/row';
import { taskBarState } from '../../taskBar';
import { IssueRelationLineOffsets } from '../types';
import { draftIssueRelationState } from './draftIssueRelationState';

export const issueRelationLineOffsetsState = selectorFamily<
  IssueRelationLineOffsets | null,
  IssueRelationID | null
>({
  key: 'timeline/issueRelationLineOffsets',
  get: (id) => {
    return ({ get }) => {
      const issueRelation = id ? get(issueRelationState(id)) : get(draftIssueRelationState);

      if (!issueRelation) {
        return null;
      }

      const identifierFrom = getIssueResourceIdentifier(issueRelation.issueFromId);
      const taskBarFrom = get(taskBarState(identifierFrom));

      const identifierTo = getIssueResourceIdentifier(issueRelation.issueToId);
      const taskBarTo = get(taskBarState(identifierTo));

      if (!taskBarFrom || !taskBarTo) {
        return null;
      }

      const rowBoundingRectMap = get(rowBoundingRectMapState);
      const rowBoundsFrom = rowBoundingRectMap.get(identifierFrom);
      const rowBoundsTo = rowBoundingRectMap.get(identifierTo);

      if (!rowBoundsFrom || !rowBoundsTo) {
        return null;
      }

      const offsetTopFrom = rowBoundsFrom.top + taskBarFrom.top;
      const offsetTopTo = rowBoundsTo.top + taskBarTo.top;

      return {
        from: {
          top: offsetTopFrom,
          middle: offsetTopFrom + taskBarFrom.height / 2,
          bottom: offsetTopFrom + taskBarFrom.height,
          right: taskBarFrom.right,
        },

        to: {
          top: offsetTopTo,
          middle: offsetTopTo + taskBarTo.height / 2,
          bottom: offsetTopTo + taskBarTo.height,
          left: taskBarTo.left,
        },
      };
    };
  },
});
