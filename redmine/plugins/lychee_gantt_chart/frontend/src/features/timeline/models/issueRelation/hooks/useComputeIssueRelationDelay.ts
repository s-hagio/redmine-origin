import { useRecoilCallback } from 'recoil';
import { issueState } from '@/models/issue';
import { useCalcWorkingDays } from '@/models/workingDay';
import { IssueRelation } from '@/models/issueRelation';

type ComputableIssueRelation = Pick<IssueRelation, 'issueFromId' | 'issueToId'>;

type ComputeIssueRelationDelay = (issueRelation: ComputableIssueRelation) => number;

export const useComputeIssueRelationDelay = (): ComputeIssueRelationDelay => {
  const calcWorkingDays = useCalcWorkingDays();

  return useRecoilCallback(
    ({ snapshot }) => {
      return ({ issueFromId, issueToId }) => {
        const [issueFrom, issueTo] = [issueFromId, issueToId].map((id) => {
          return snapshot.getLoadable(issueState(id)).getValue();
        });

        if (!issueFrom?.dueDate || !issueTo?.startDate) {
          return 0;
        }

        const days = calcWorkingDays(issueFrom.dueDate, issueTo.startDate);

        return days > 0 ? days - 2 : days;
      };
    },
    [calcWorkingDays]
  );
};
