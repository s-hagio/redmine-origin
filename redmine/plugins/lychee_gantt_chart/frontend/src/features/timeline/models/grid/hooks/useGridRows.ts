import { useRecoilValue } from 'recoil';
import { gridRowsState } from '../states';

export const useGridRows = () => useRecoilValue(gridRowsState);
