import { isSameTaskBar } from '../isSameTaskBar';
import { TaskBar } from '../../types';

const taskBar: TaskBar = {
  resourceType: 'project',
  top: 1,
  right: 2,
  left: 3,
  width: 4,
  doneWidth: 5,
  lateWidth: 6,
  height: 7,
};

test('二つのTaskBarのリソース種別／位置やサイズが同じならtrue', () => {
  expect(isSameTaskBar(taskBar, { ...taskBar })).toBe(true);
  expect(isSameTaskBar(null, null)).toBe(true);
});

test('リソース種別が異なるならfalse', () => {
  expect(isSameTaskBar(taskBar, { ...taskBar, resourceType: 'issue' })).toBe(false);
});

test.each(['top', 'right', 'left', 'width', 'doneWidth', 'lateWidth', 'height'])(
  '%sが異なるならfalse',
  (key) => {
    expect(isSameTaskBar(taskBar, { ...taskBar, [key]: 0 })).toBe(false);
  }
);
