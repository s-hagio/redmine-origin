import { useDraftIssueRelation } from './useDraftIssueRelation';
import { useComputeIssueRelationDelay } from './useComputeIssueRelationDelay';

export const useDraftIssueRelationDelay = () => {
  const draftIssueRelation = useDraftIssueRelation();
  const computeIssueRelationDelay = useComputeIssueRelationDelay();

  if (!draftIssueRelation) {
    return 0;
  }

  return computeIssueRelationDelay(draftIssueRelation);
};
