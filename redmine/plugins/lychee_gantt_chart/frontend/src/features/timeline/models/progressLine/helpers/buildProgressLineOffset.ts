import { RowBoundingRect } from '@/features/row';
import { TaskBar, TaskBarResourceType } from '../../taskBar';
import { ProgressLineOffset } from '../types';

export const buildProgressLineOffset = (
  taskBar: TaskBar | null,
  rowBounds: RowBoundingRect,
  todayLinePosition: number,
  isClosed: boolean
): ProgressLineOffset => {
  if (!taskBar) {
    return {
      top: rowBounds.top + rowBounds.height / 2,
      left: todayLinePosition,
    };
  }

  return {
    top: rowBounds.top + taskBar.top + taskBar.height / 2,
    left: computeLeft(taskBar, todayLinePosition, isClosed),
  };
};

const computeLeft = (taskBar: TaskBar, todayLinePosition: number, isClosed: boolean) => {
  if (taskBar.resourceType === TaskBarResourceType.Project) {
    return todayLinePosition;
  }

  const isFuture = taskBar.right > todayLinePosition;
  const isPast = taskBar.left <= todayLinePosition;
  const isOpen = !isClosed;
  const hasProgress = taskBar.doneWidth > 0;

  if (isFuture && isClosed) {
    return taskBar.right;
  }

  if ((isPast && isOpen) || (isFuture && hasProgress)) {
    return taskBar.left + taskBar.doneWidth;
  }

  return todayLinePosition;
};
