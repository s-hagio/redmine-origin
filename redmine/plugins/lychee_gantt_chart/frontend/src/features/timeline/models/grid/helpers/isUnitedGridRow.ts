import { GridRow } from '../types';
import { isDayGridRow } from './isDayGridRow';

export const isUnitedGridRow = (row: GridRow) => !isDayGridRow(row);
