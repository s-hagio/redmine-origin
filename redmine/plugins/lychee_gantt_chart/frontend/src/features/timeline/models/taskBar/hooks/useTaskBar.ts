import { useRecoilValue } from 'recoil';
import { taskBarState } from '../states';
import { TaskBarResourceIdentifier } from '../types';

export const useTaskBar = (id: TaskBarResourceIdentifier) => useRecoilValue(taskBarState(id));
