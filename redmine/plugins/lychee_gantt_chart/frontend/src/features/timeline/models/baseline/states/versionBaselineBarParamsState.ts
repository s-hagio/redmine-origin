import { selectorFamily } from 'recoil';
import { VersionResourceIdentifier } from '@/models/coreResource';
import { versionBaselineSnapshotState } from '@/models/baseline';
import { extractVersionBarParams, TaskBarParams } from '../../taskBar';

export const versionBaselineBarParamsState = selectorFamily<TaskBarParams | null, VersionResourceIdentifier>({
  key: 'timeline/versionBaselineBarParams',
  get: (identifier) => {
    return ({ get }) => {
      return extractVersionBarParams(get(versionBaselineSnapshotState(identifier)));
    };
  },
});
