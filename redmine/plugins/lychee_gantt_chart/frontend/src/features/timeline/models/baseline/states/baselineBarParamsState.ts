import { selectorFamily } from 'recoil';
import {
  isIssueResourceIdentifier,
  isProjectResourceIdentifier,
  isVersionResourceIdentifier,
} from '@/models/coreResource';
import { TaskBarParams } from '../../taskBar';
import { BaselineSnapshotResourceIdentifier } from '../types';
import { projectBaselineBarParamsState } from './projectBaselineBarParamsState';
import { versionBaselineBarParamsState } from './versionBaselineBarParamsState';
import { issueBaselineBarParamsState } from './issueBaselineBarParamsState';

export const baselineBarParamsState = selectorFamily<
  TaskBarParams | null,
  BaselineSnapshotResourceIdentifier
>({
  key: 'timeline/baselineBarParams',
  get: (identifier) => {
    return ({ get }) => {
      if (isProjectResourceIdentifier(identifier)) {
        return get(projectBaselineBarParamsState(identifier));
      } else if (isVersionResourceIdentifier(identifier)) {
        return get(versionBaselineBarParamsState(identifier));
      } else if (isIssueResourceIdentifier(identifier)) {
        return get(issueBaselineBarParamsState(identifier));
      }

      throw new Error(`Invalid resource identifier: ${identifier}`);
    };
  },
});
