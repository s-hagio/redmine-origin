import { constSelector, selector, useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { getIssueResourceIdentifier, getProjectResourceIdentifier } from '@/models/coreResource';
import { leafIssue, mainProject, mainProjectVersionRow } from '@/__fixtures__';
import { issueBarParams, projectBarParams, versionBarParams } from '../../../../__fixtures__';
import { TimeBar } from '../../../timeBar';
import { buildTaskBarState } from '../buildTaskBarState';
import {
  TaskBar,
  TaskBarBoundingRect,
  TaskBarParams,
  TaskBarResourceIdentifier,
  TaskBarResourceType,
} from '../../types';

const buildTimeBarStateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../timeBar', async (importOriginal) =>
  Object.defineProperties(await importOriginal(), {
    buildTimeBarState: { get: buildTimeBarStateMock },
  })
);

const todayTickEndPositionStateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../tick', async (importOriginal) =>
  Object.defineProperties(await importOriginal(), {
    todayTickEndPositionState: { get: todayTickEndPositionStateMock },
  })
);

const buildTaskBarBoundingRectStateMock = vi.hoisted(() => vi.fn());

vi.mock('../buildTaskBarBoundingRectState', async (importOriginal) =>
  Object.defineProperties(await importOriginal(), {
    buildTaskBarBoundingRectState: { get: buildTaskBarBoundingRectStateMock },
  })
);

const timeBar: TimeBar = { width: 200, left: 10, right: 210 };
const bounds: TaskBarBoundingRect = { top: 10, height: 100 };

todayTickEndPositionStateMock.mockReturnValue(constSelector(20));

const buildTimeBarMock = vi.fn().mockReturnValue(timeBar);

buildTimeBarStateMock.mockReturnValue(
  selector({
    key: 'timeBar/mock',
    get: () => buildTimeBarMock,
  })
);

const buildTaskBarBoundingRectMock = vi.fn().mockReturnValue(bounds);
const buildTaskBarBoundingRectFamilyMock = vi.fn().mockReturnValue(buildTaskBarBoundingRectMock);

buildTaskBarBoundingRectFamilyMock.mockReturnValue(
  selector({
    key: 'buildTaskBarBoundingRect/mock',
    get: () => buildTaskBarBoundingRectMock,
  })
);
buildTaskBarBoundingRectStateMock.mockReturnValue(buildTaskBarBoundingRectFamilyMock);

type TestCase = {
  identifier: TaskBarResourceIdentifier;
  params: TaskBarParams;
  expected: TaskBar;
};

test.each<TestCase>([
  {
    identifier: getIssueResourceIdentifier(leafIssue.id),
    params: issueBarParams,
    expected: {
      ...timeBar,
      ...bounds,
      resourceType: TaskBarResourceType.Issue,
      lateWidth: 10,
      doneWidth: 90,
    },
  },
  {
    identifier: getProjectResourceIdentifier(mainProject.id),
    params: projectBarParams,
    expected: {
      ...timeBar,
      ...bounds,
      resourceType: TaskBarResourceType.Project,
      lateWidth: 10,
      doneWidth: 24,
    },
  },
  {
    identifier: mainProjectVersionRow.id,
    params: versionBarParams,
    expected: {
      ...timeBar,
      ...bounds,
      resourceType: TaskBarResourceType.Version,
      lateWidth: 10,
      doneWidth: 46,
    },
  },
])('TaskBarParamsからTaskBarオブジェクトを作成する', ({ identifier, params, expected }) => {
  const { result } = renderRecoilHook(() => useRecoilValue(buildTaskBarState(identifier)));

  expect(result.current(params)).toStrictEqual(expected);
  expect(buildTimeBarMock).toBeCalledWith(params.startDate, params.endDate);
  expect(buildTaskBarBoundingRectFamilyMock).toBeCalledWith(identifier);
});
