import { RecoilValue, useRecoilCallback, useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { additionalTaskBarBoundingRectState } from '../additionalTaskBarBoundingRectState';
import { additionalTaskBarBoundingRectMapObjectState } from '../additionalTaskBarBoundingRectMapObjectState';

const identifier = 'issue-1';

test('set: additionalTaskBarBoundingRectMapObjectにAdderNameごとのBoundingRectをセットする', () => {
  const initialMapObject = {
    a: { top: 10, height: 100 },
    b: { top: 21, height: 201 },
  };

  const { result } = renderRecoilHook(
    () => ({
      set: useSetRecoilState(additionalTaskBarBoundingRectState({ identifier, adderName: 'c' })),
      additionalBoundingRects: useRecoilValue(additionalTaskBarBoundingRectMapObjectState(identifier)),
    }),
    {
      initializeState: ({ set }) => {
        set(additionalTaskBarBoundingRectMapObjectState(identifier), initialMapObject);
      },
    }
  );

  expect(result.current.additionalBoundingRects).toEqual(initialMapObject);

  act(() => result.current.set({ top: 1, height: 20 }));

  expect(result.current.additionalBoundingRects).toEqual({
    ...initialMapObject,
    c: { top: 1, height: 20 },
  });
});

test('get: 例外を投げる', () => {
  const { result } = renderRecoilHook(() => ({
    get: useRecoilCallback(({ snapshot }) => {
      return <T>(state: RecoilValue<T>) => {
        snapshot.getLoadable(state).getValue();
      };
    }),
  }));

  expect(() => {
    result.current.get(additionalTaskBarBoundingRectState({ identifier, adderName: 'test' }));
  }).toThrowError();
});
