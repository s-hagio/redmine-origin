import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { toDate } from '@/lib/date';
import { settingsState } from '@/models/setting';
import { ensureVisibleDayState } from '../ensureVisibleDayState';
import { TimeDirection } from '../../types';
import { timeAxisOptionsState } from '../../states';

const renderForTesting = (includeNonWorkingDays: boolean) => {
  return renderRecoilHook(() => useRecoilValue(ensureVisibleDayState), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({ ...current, nonWorkingDays: ['2022-07-09', '2022-07-10'] }));
      set(timeAxisOptionsState, (current) => ({ ...current, includeNonWorkingDays }));
    },
  });
};

describe('休業日の表示が「OFF」の場合', () => {
  test('時間の方向に応じた直近の表示日を返す', () => {
    const { result } = renderForTesting(false);

    expect(result.current('2022-07-05', TimeDirection.Forward)).toEqual(toDate('2022-07-05'));
    expect(result.current(new Date(2022, 6, 9), TimeDirection.Forward)).toEqual(toDate('2022-07-11'));
    expect(result.current(new Date(2022, 6, 10), TimeDirection.Backward)).toEqual(toDate('2022-07-08'));
  });
});

describe('休業日の表示が「ON」の場合', () => {
  test('すべて表示されるためそのまま帰す', () => {
    const { result } = renderForTesting(true);

    expect(result.current('2022-07-05', TimeDirection.Forward)).toEqual(toDate('2022-07-05'));
    expect(result.current(new Date(2022, 6, 9), TimeDirection.Forward)).toEqual(toDate('2022-07-09'));
    expect(result.current(new Date(2022, 6, 10), TimeDirection.Backward)).toEqual(toDate('2022-07-10'));
  });
});
