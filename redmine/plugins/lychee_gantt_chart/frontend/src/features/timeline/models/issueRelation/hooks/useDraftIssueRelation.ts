import { useRecoilValue } from 'recoil';
import { draftIssueRelationState } from '../states';

export const useDraftIssueRelation = () => useRecoilValue(draftIssueRelationState);
