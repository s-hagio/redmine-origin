import { selector, useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { ticksState } from '../../states';
import { tickAtState } from '../tickAtState';
import { AddVisibleDays } from '../../../timeAxis';

const addVisibleDaysMock = vi.hoisted(() => vi.fn());

vi.mock('../../../timeAxis', async (importOriginal) =>
  Object.defineProperties(importOriginal<object>(), {
    addVisibleDaysState: { get: addVisibleDaysMock },
  })
);

const ticks = [new Date(2023, 8, 1), new Date(2023, 8, 2), new Date(2023, 8, 3)];

const renderForTesting = () => {
  return renderRecoilHook(() => useRecoilValue(tickAtState), {
    initializeState: ({ set }) => {
      set(ticksState, ticks);
    },
  });
};

// 単純にdaysを追加
addVisibleDaysMock.mockReturnValue(
  selector<AddVisibleDays>({
    key: 'addVisibleDays/mock',
    get: () => (tick, days) => {
      const date = new Date(tick);
      date.setDate(date.getDate() + days);
      return date;
    },
  })
);

test('指定indexのTickを返す', () => {
  const { result } = renderForTesting();

  expect(result.current(0)).toBe(ticks[0]);
  expect(result.current(1)).toBe(ticks[1]);
  expect(result.current(2)).toBe(ticks[2]);
});

test('indexがticksの範囲を超えた場合は日数差から可視日を計算して返す', () => {
  const { result } = renderForTesting();

  expect(result.current(3)).toEqual(new Date(2023, 8, 4));
  expect(result.current(10)).toEqual(new Date(2023, 8, 11));

  expect(result.current(-1)).toEqual(new Date(2023, 7, 31));
  expect(result.current(-20)).toEqual(new Date(2023, 7, 12));
});
