import { toDate } from '@/lib/date';
import { buildDayColumn } from '../buildDayColumn';
import { buildDayAttributes } from '../buildDayAttributes';
import { helpers } from './__fixtures__';

vi.mock('../buildDayAttributes', () => ({
  buildDayAttributes: vi.fn().mockReturnValue({ __DAY_ATTRIBUTES__: 1 }),
}));

const tick = toDate('2022-07-13');
const stepWidth = 2;

test('DayColumnを構築', () => {
  expect(buildDayColumn(tick, stepWidth, helpers)).toStrictEqual({
    id: 'day-2022-07-13',
    type: 'day',
    label: '13',
    width: stepWidth,
    __DAY_ATTRIBUTES__: 1,
  });

  expect(buildDayAttributes).toHaveBeenLastCalledWith(tick, helpers);
});
