import { selector } from 'recoil';
import { closedResourceRowIdsState, rowBoundingRectMapState, visibleRowsState } from '@/features/row';
import { taskBarState, isTaskBarResourceIdentifier } from '../../taskBar';
import { ProgressLineOffset } from '../types';
import { buildProgressLineOffset, isProgressLineResourceType } from '../helpers';
import { timelineOptionsState } from '../../options';
import { progressLineCenterPositionState } from './progressLineCenterPositionState';

export const progressLineOffsetsState = selector<ProgressLineOffset[]>({
  key: 'timeline/progressLineOffsets',
  get: ({ get }) => {
    const { showProgressLine } = get(timelineOptionsState);

    if (!showProgressLine) {
      return [];
    }

    const centerPosition = get(progressLineCenterPositionState);

    if (Number.isNaN(centerPosition)) {
      return [];
    }

    const visibleRows = get(visibleRowsState);
    const rowBoundingRectMap = get(rowBoundingRectMapState);
    const closedResourceRowIds = get(closedResourceRowIdsState);

    return visibleRows.reduce<ProgressLineOffset[]>(
      (memo, row) => {
        if (!isProgressLineResourceType(row.resourceType)) {
          return memo;
        }

        const rowBounds = rowBoundingRectMap.get(row.id);
        const taskBar = isTaskBarResourceIdentifier(row.id) ? get(taskBarState(row.id)) : null;

        if (!rowBounds) {
          throw new Error(`Row bounding rect not found: ${row.id}`);
        }

        memo.push(
          buildProgressLineOffset(taskBar, rowBounds, centerPosition, closedResourceRowIds.has(row.id))
        );

        return memo;
      },
      [{ top: 0, left: centerPosition }]
    );
  },
});
