import { useRecoilValue } from 'recoil';
import { todayTickEndPositionState } from '../states';

export const useTodayTickEndPosition = () => useRecoilValue(todayTickEndPositionState);
