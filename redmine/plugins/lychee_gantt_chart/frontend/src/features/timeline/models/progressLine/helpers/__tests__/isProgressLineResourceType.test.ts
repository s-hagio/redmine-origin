import { RowResourceType } from '@/features/row';
import { isProgressLineResourceType } from '../isProgressLineResourceType';

test('イナズマ線のリソースタイプならtrueを返す', () => {
  expect(isProgressLineResourceType(RowResourceType.Version)).toBe(true);
  expect(isProgressLineResourceType(RowResourceType.Issue)).toBe(true);
});

test('それ以外はfalseを返す', () => {
  expect(isProgressLineResourceType(RowResourceType.Project)).toBe(false);
  expect(isProgressLineResourceType(RowResourceType.Group)).toBe(false);
  expect(isProgressLineResourceType(RowResourceType.Placeholder)).toBe(false);
  expect(isProgressLineResourceType(RowResourceType.ParallelView)).toBe(false);
});
