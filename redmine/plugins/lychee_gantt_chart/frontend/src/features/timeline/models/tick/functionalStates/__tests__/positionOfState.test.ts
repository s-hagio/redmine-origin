import { constSelector, useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { DateArgument, toDate } from '@/lib/date';
import { settingsState } from '@/models/setting';
import { ticksState } from '../../states';
import { timeAxisOptionsState, TimeDirection } from '../../../timeAxis';
import { positionOfState } from '../positionOfState';

const stepWidthStateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../timeAxis', async () =>
  Object.defineProperties(await vi.importActual<object>('../../../timeAxis'), {
    stepWidthState: { get: stepWidthStateMock },
  })
);

beforeEach(() => {
  stepWidthStateMock.mockReturnValue(constSelector(3));
});

const ticksWithNonWorkingDays = ['2023-03-01', '2023-03-02', '2023-03-03'] as const;
const ticksWithoutNonWorkingDays = ['2023-03-01', '2023-03-03'] as const;
const nonWorkingDays = ['2023-02-28', '2023-03-02'];

const render = (ticks: ReadonlyArray<DateArgument>) => {
  return renderRecoilHook(
    () => ({
      positionOf: useRecoilValue(positionOfState),
      setTimeAxisOptions: useSetRecoilState(timeAxisOptionsState),
    }),
    {
      initializeState: ({ set }) => {
        set(
          ticksState,
          ticks.map((dateString) => toDate(dateString))
        );
        set(settingsState, (current) => ({ ...current, nonWorkingDays }));
      },
    }
  );
};

test('日付の位置（px）を返す', () => {
  const { result } = render(ticksWithNonWorkingDays);

  expect(result.current.positionOf('2023-02-27')).toBe(-6);
  expect(result.current.positionOf('2023-03-01')).toBe(0);
  expect(result.current.positionOf(new Date(2023, 2, 2))).toBe(3);
  expect(result.current.positionOf('2023-03-05')).toBe(12);
});

describe('休業日が非表示の場合', () => {
  const renderWithoutNonWorkingDays = () => {
    const hookResult = render(ticksWithoutNonWorkingDays);

    act(() => {
      hookResult.result.current.setTimeAxisOptions((current) => ({
        ...current,
        includeNonWorkingDays: false,
      }));
    });

    return hookResult;
  };

  test('直近の可視日に変換して位置を返す', () => {
    const { result } = renderWithoutNonWorkingDays();

    expect(result.current.positionOf('2023-02-26')).toBe(-6);
    expect(result.current.positionOf('2023-02-28')).toBe(0);
    expect(result.current.positionOf('2023-03-01')).toBe(0);
    expect(result.current.positionOf('2023-03-02')).toBe(3);
    expect(result.current.positionOf('2023-03-03')).toBe(3);
    expect(result.current.positionOf('2023-03-04')).toBe(6);
    expect(result.current.positionOf('2023-03-05')).toBe(9);
  });

  test('可視日に変換する方向をオプションで指定可能', () => {
    const { result } = renderWithoutNonWorkingDays();

    expect(result.current.positionOf('2023-02-28', { directionForEnsure: TimeDirection.Backward })).toBe(-3);
    expect(result.current.positionOf('2023-03-05', { directionForEnsure: TimeDirection.Backward })).toBe(9);
  });
});
