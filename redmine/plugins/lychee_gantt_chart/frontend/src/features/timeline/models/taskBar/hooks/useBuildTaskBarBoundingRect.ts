import { useRecoilValue } from 'recoil';
import { TaskBarResourceIdentifier } from '../types';
import { buildTaskBarBoundingRectState } from '../functionalStates';

export const useBuildTaskBarBoundingRect = (identifier: TaskBarResourceIdentifier) => {
  return useRecoilValue(buildTaskBarBoundingRectState(identifier));
};
