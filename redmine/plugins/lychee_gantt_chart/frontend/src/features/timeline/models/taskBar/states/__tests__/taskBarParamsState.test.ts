import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import {
  getProjectResourceIdentifier,
  getVersionResourceIdentifier,
  getIssueResourceIdentifier,
} from '@/models/coreResource';
import { projectState } from '@/models/project';
import { versionState } from '@/models/version';
import { issueEntityState } from '@/models/issue';
import { rootIssue, rootProject, versionSharedByTree } from '@/__fixtures__';
import { TaskBarResourceIdentifier } from '../../types';
import { extractIssueBarParams, extractProjectBarParams, extractVersionBarParams } from '../../helpers';
import { taskBarParamsState } from '../taskBarParamsState';

const project = { ...rootProject, startDate: '2024-02-01', endDate: '2024-02-11' };
const version = { ...versionSharedByTree, startDate: '2024-02-02', endDate: '2024-02-12' };
const issue = { ...rootIssue, startDate: '2024-02-03', endDate: '2024-02-13' };

test.each<{
  identifier: TaskBarResourceIdentifier;
  expected: unknown;
}>([
  {
    identifier: getProjectResourceIdentifier(project.id),
    expected: extractProjectBarParams(project),
  },
  {
    identifier: getVersionResourceIdentifier(project.id, version.id),
    expected: extractVersionBarParams(version),
  },
  {
    identifier: getIssueResourceIdentifier(issue.id),
    expected: extractIssueBarParams(issue),
  },
])('$identifierのtaskBarParamsを返す', ({ identifier, expected }) => {
  const { result } = renderRecoilHook(() => useRecoilValue(taskBarParamsState(identifier)), {
    initializeState: ({ set }) => {
      set(projectState(project.id), project);
      set(versionState(version.id), version);
      set(issueEntityState(issue.id), issue);
    },
  });

  expect(result.current).toBe(expected);
});
