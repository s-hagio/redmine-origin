import { TaskBarResourceType } from '../types';

const taskBarResourceTypes: string[] = Object.values(TaskBarResourceType);

export const isTaskBarResourceType = (resourceType: string): resourceType is TaskBarResourceType => {
  return taskBarResourceTypes.includes(resourceType);
};
