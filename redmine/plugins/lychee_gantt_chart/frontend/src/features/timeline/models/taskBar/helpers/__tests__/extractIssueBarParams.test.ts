import { Issue } from '@/models/issue';
import { leafIssue } from '@/__fixtures__';
import { TaskBarResourceType } from '../../types';
import { extractIssueBarParams } from '../extractIssueBarParams';

const issue: Issue = {
  ...leafIssue,
  startDate: '2022-01-01',
  dueDate: '2022-01-05',
  doneRatio: 25,
};

test('IssueからTaskBarParamsを抽出する', () => {
  expect(extractIssueBarParams(issue)).toStrictEqual({
    resourceType: TaskBarResourceType.Issue,
    startDate: issue.startDate,
    endDate: issue.dueDate,
    progressRate: issue.doneRatio,
  });
});

test('TaskBarParamsを持たない場合はnullを返す', () => {
  expect(extractIssueBarParams({ ...issue, startDate: null })).toBeNull();
  expect(extractIssueBarParams({ ...issue, dueDate: null })).toBeNull();
  expect(extractIssueBarParams({ ...issue, startDate: null, dueDate: null })).toBeNull();
});
