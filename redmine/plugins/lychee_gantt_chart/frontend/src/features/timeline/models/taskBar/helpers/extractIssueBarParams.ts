import { Issue } from '@/models/issue';
import { TaskBarParams, TaskBarResourceType } from '../types';

type IssueTaskBarResource = Pick<Issue, 'startDate' | 'dueDate' | 'doneRatio'>;

export const extractIssueBarParams = (issue: IssueTaskBarResource | null): TaskBarParams | null => {
  if (!issue || !issue.startDate || !issue.dueDate) {
    return null;
  }

  return {
    resourceType: TaskBarResourceType.Issue,
    startDate: issue.startDate,
    endDate: issue.dueDate,
    progressRate: issue.doneRatio ?? 0,
  };
};
