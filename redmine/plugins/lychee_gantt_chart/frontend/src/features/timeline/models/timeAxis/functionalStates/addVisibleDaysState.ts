import { selector } from 'recoil';
import { DateArgument, toDate } from '@/lib/date';
import { isVisibleDayState } from './isVisibleDayState';

export type AddVisibleDays = (arg: DateArgument, days: number) => Date;

export const addVisibleDaysState = selector<AddVisibleDays>({
  key: 'timeline/addVisibleDays',
  get: ({ get }) => {
    const isVisibleDay = get(isVisibleDayState);

    return (arg, days) => {
      const date = toDate(arg);
      const value = days > 0 ? 1 : -1;

      let remainingDays = Math.abs(days);

      while (remainingDays > 0) {
        date.setDate(date.getDate() + value);

        if (isVisibleDay(date)) {
          --remainingDays;
        }
      }

      return date;
    };
  },
});
