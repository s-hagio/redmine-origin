import { useRecoilValue } from 'recoil';
import { buildTaskBarState } from '../functionalStates';
import { TaskBarResourceIdentifier } from '../types';

export const useBuildTaskBar = (identifier: TaskBarResourceIdentifier) => {
  return useRecoilValue(buildTaskBarState(identifier));
};
