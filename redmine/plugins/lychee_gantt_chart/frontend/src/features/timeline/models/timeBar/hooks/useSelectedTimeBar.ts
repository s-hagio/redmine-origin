import { useMemo } from 'react';
import { SelectedTimeBar } from '../types';
import { useBuildTimeBar } from './useBuildTimeBar';
import { useSelectedTimeRange } from './useSelectedTimeRange';

export const useSelectedTimeBar = (): SelectedTimeBar | null => {
  const buildTimeBar = useBuildTimeBar();
  const selectedTimeRange = useSelectedTimeRange();

  return useMemo(() => {
    if (!selectedTimeRange) {
      return null;
    }

    return {
      ...buildTimeBar(selectedTimeRange.startDate, selectedTimeRange.endDate),
      showGuideline: !!selectedTimeRange.showGuideline,
    };
  }, [buildTimeBar, selectedTimeRange]);
};
