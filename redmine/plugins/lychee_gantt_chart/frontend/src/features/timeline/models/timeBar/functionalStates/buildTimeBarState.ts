import { selector } from 'recoil';
import { DateArgument } from '@/lib/date';
import { ensureVisibleDayState, stepWidthState, TimeDirection } from '../../timeAxis';
import { indexOfState } from '../../tick';
import { TimeBar } from '../types';

export type BuildTimeBar = (startDateArg: DateArgument, endDateArg: DateArgument) => TimeBar;

export const buildTimeBarState = selector<BuildTimeBar>({
  key: 'timeline/buildTimeBar',
  get: ({ get }) => {
    const ensureVisibleDay = get(ensureVisibleDayState);
    const stepWidth = get(stepWidthState);
    const indexOf = get(indexOfState);

    return (startDateArg, endDateArg) => {
      const startIndex = indexOf(ensureVisibleDay(startDateArg, TimeDirection.Forward));
      const endIndex = indexOf(ensureVisibleDay(endDateArg, TimeDirection.Backward));

      const width = (endIndex - startIndex + 1) * stepWidth;
      const left = startIndex * stepWidth;
      const right = left + width;

      return { width, left, right };
    };
  },
});
