import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { isVisibleDayState } from '../isVisibleDayState';
import { timeAxisOptionsState } from '../../states';

const renderForTesting = (includeNonWorkingDays: boolean) => {
  return renderRecoilHook(() => useRecoilValue(isVisibleDayState), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({
        ...current,
        nonWorkingWeekDays: [6, 7], // 土、日
        nonWorkingDays: ['2022-07-18'], // 海の日
      }));

      set(timeAxisOptionsState, (current) => ({ ...current, includeNonWorkingDays }));
    },
  });
};

test('「休業日を表示」がONなら常にtrue', () => {
  const { result } = renderForTesting(true);

  expect(result.current(new Date(2022, 6, 5))).toBe(true);
  expect(result.current(new Date(2022, 6, 9))).toBe(true);
  expect(result.current('2022-07-10')).toBe(true);
  expect(result.current('2022-07-18')).toBe(true);
});

test('「休業日を表示」がOFFなら休業日はfalse', () => {
  const { result } = renderForTesting(false);

  expect(result.current(new Date(2022, 6, 5))).toBe(true);
  expect(result.current(new Date(2022, 6, 9))).toBe(false);
  expect(result.current('2022-07-10')).toBe(false);
  expect(result.current('2022-07-18')).toBe(false);
});
