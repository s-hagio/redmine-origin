import { GridRow, DayGridRow, GridType } from '../types';

export const isDayGridRow = (row: GridRow): row is DayGridRow => {
  return row.type === GridType.Day || row.type === GridType.WeekDay;
};
