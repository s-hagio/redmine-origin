import { toDate } from '@/lib/date';
import { buildMonthColumn } from '../buildMonthColumn';
import { helpers } from './__fixtures__';

test('MonthColumnを構築', () => {
  const groupedTicks = [toDate('2022-07-01'), toDate('2022-07-02'), toDate('2022-07-31')];
  const stepWidth = 2;

  expect(buildMonthColumn(groupedTicks, stepWidth, helpers)).toStrictEqual({
    id: 'month-2022-7',
    type: 'month',
    label: '7',
    width: 6,
  });
});
