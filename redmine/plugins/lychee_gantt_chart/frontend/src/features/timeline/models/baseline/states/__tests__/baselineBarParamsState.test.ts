import { constSelector, useRecoilValue } from 'recoil';
import { Mock } from 'vitest';
import { renderRecoilHook } from '@/test-utils';
import { baselineBarParamsState } from '../baselineBarParamsState';
import { BaselineSnapshotResourceIdentifier } from '../../types';

const projectBaselineBarParamsStateMock = vi.hoisted(() => vi.fn());
const projectBaselineBarParams = {};

vi.mock('../projectBaselineBarParamsState', () => ({
  projectBaselineBarParamsState: projectBaselineBarParamsStateMock,
}));

const versionBaselineBarParamsStateMock = vi.hoisted(() => vi.fn());
const versionBaselineBarParams = {};

vi.mock('../versionBaselineBarParamsState', () => ({
  versionBaselineBarParamsState: versionBaselineBarParamsStateMock,
}));

const issueBaselineBarParamsStateMock = vi.hoisted(() => vi.fn());
const issueBaselineBarParams = {};

vi.mock('../issueBaselineBarParamsState', () => ({
  issueBaselineBarParamsState: issueBaselineBarParamsStateMock,
}));

beforeEach(() => {
  projectBaselineBarParamsStateMock.mockReturnValue(constSelector(projectBaselineBarParams));
  versionBaselineBarParamsStateMock.mockReturnValue(constSelector(versionBaselineBarParams));
  issueBaselineBarParamsStateMock.mockReturnValue(constSelector(issueBaselineBarParams));
});

type TestCase = {
  identifier: BaselineSnapshotResourceIdentifier;
  state: Mock;
  expected: unknown;
};

test.each<TestCase>([
  {
    identifier: 'project-1',
    state: projectBaselineBarParamsStateMock,
    expected: projectBaselineBarParams,
  },
  {
    identifier: 'version-1-2',
    state: versionBaselineBarParamsStateMock,
    expected: versionBaselineBarParams,
  },
  {
    identifier: 'issue-1',
    state: issueBaselineBarParamsStateMock,
    expected: issueBaselineBarParams,
  },
])('$identifierのbaselineBarParamsを返す', ({ identifier, state, expected }) => {
  expect(state).not.toBeCalled();

  const { result } = renderRecoilHook(() => useRecoilValue(baselineBarParamsState(identifier)));

  expect(state).toBeCalledWith(identifier);
  expect(result.current).toBe(expected);
});
