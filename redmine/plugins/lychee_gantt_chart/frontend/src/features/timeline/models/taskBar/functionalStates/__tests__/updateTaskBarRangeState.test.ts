import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { mockSelector, renderRecoilHook } from '@/test-utils';
import { issueTaskBar } from '../../../../__fixtures__';
import { TimeBar, TimeBarRange } from '../../../timeBar';
import { taskBarState } from '../../states';
import { TaskBar } from '../../types';
import { updateTaskBarRangeState } from '../updateTaskBarRangeState';

const buildTimeBarStateMock = vi.hoisted(() => vi.fn());
const buildTimeBarMock = vi.fn();

vi.mock('../../../timeBar', async (importOriginal) =>
  Object.defineProperties(await importOriginal<object>(), {
    buildTimeBarState: { get: buildTimeBarStateMock },
  })
);

const taskBar: TaskBar = issueTaskBar;
const timeRange: TimeBar = { width: taskBar.width + 1, left: taskBar.left + 1, right: taskBar.right + 1 };
const timeBarRange: TimeBarRange = { startDate: '2024-01-01', endDate: '2024-01-02' };
const identifier = 'issue-123';

beforeEach(() => {
  buildTimeBarMock.mockReturnValue(timeRange);
  buildTimeBarStateMock.mockReturnValue(mockSelector(buildTimeBarMock));
});

const renderForTesting = () => {
  return renderRecoilHook(() => ({
    updateTaskBarRange: useRecoilValue(updateTaskBarRangeState(identifier)),
    setTaskBar: useSetRecoilState(taskBarState(identifier)),
    taskBar: useRecoilValue(taskBarState(identifier)),
  }));
};

test('taskBarStateのRangeだけ更新する', () => {
  const { result } = renderForTesting();

  act(() => {
    result.current.updateTaskBarRange(timeBarRange);
  });

  expect(result.current.taskBar).toBe(null);

  act(() => {
    result.current.setTaskBar(taskBar);
    result.current.updateTaskBarRange(timeBarRange);
  });

  expect(result.current.taskBar).toEqual({ ...taskBar, ...timeRange });
});
