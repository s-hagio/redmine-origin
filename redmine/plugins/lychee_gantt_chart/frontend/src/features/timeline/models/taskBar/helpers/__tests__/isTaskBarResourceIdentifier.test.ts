import { isTaskBarResourceIdentifier } from '../isTaskBarResourceIdentifier';

test('TaskBarResourceの識別子ならtrue', () => {
  expect(isTaskBarResourceIdentifier('project-1')).toBe(true);
  expect(isTaskBarResourceIdentifier('version-1-1')).toBe(true);
  expect(isTaskBarResourceIdentifier('issue-1')).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isTaskBarResourceIdentifier('group-1')).toBe(false);
});
