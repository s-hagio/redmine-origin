import { formatISODate } from '@/lib/date';
import { GridType } from '../types';
import { BuildUnitedColumn } from './types';

export const buildWeekColumn: BuildUnitedColumn = (groupedTicks, stepWidth) => {
  const beginningOfWeek = groupedTicks[0];

  return {
    id: `${GridType.Week}-${formatISODate(beginningOfWeek)}`,
    type: GridType.Week,
    label: beginningOfWeek.getDate() + '',
    width: groupedTicks.length * stepWidth,
  };
};
