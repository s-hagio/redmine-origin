import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { useSetIssueRelationCreationTarget } from '../useSetIssueRelationCreationTarget';
import { useIssueRelationCreation } from '../useIssueRelationCreation';
import { issueRelationCreationState } from '../../states';
import { IssueRelationCreation } from '../../types';

const renderForTesting = (issueRelationCreation: IssueRelationCreation | null) => {
  return renderRecoilHook(
    () => ({
      set: useSetIssueRelationCreationTarget(),
      issueRelationCreation: useIssueRelationCreation(),
    }),
    {
      initializeState: ({ set }) => {
        set(issueRelationCreationState, issueRelationCreation);
      },
    }
  );
};

const issueRelationCreation: IssueRelationCreation = {
  type: 'to',
  sourceIssueId: 123,
  targetIssueId: null,
  isLocked: false,
};

test('指定IDをIssueRelationCreationのtargetIssueIdにセット', () => {
  const { result } = renderForTesting({ ...issueRelationCreation, targetIssueId: null });

  expect(result.current.issueRelationCreation?.targetIssueId).toBeNull();

  act(() => result.current.set(111));

  expect(result.current.issueRelationCreation?.targetIssueId).toBe(111);
});

test('IssueRelationCreationがnullなら何もしない', () => {
  const { result } = renderForTesting(null);

  expect(result.current.issueRelationCreation).toBeNull();

  act(() => result.current.set(111));

  expect(result.current.issueRelationCreation).toBeNull();
});

test('lockオプションを渡すとロックしてforce: trueをつけない限り上書きセットしない', () => {
  const { result } = renderForTesting({ ...issueRelationCreation, isLocked: false });

  expect(result.current.issueRelationCreation?.isLocked).toBe(false);

  act(() => result.current.set(111, { lock: true }));

  expect(result.current.issueRelationCreation?.isLocked).toBe(true);
  expect(result.current.issueRelationCreation?.targetIssueId).toBe(111);

  act(() => result.current.set(222));

  expect(result.current.issueRelationCreation?.isLocked).toBe(true);
  expect(result.current.issueRelationCreation?.targetIssueId).toBe(111);

  act(() => result.current.set(333, { force: true }));

  expect(result.current.issueRelationCreation?.isLocked).toBe(true);
  expect(result.current.issueRelationCreation?.targetIssueId).toBe(333);
});
