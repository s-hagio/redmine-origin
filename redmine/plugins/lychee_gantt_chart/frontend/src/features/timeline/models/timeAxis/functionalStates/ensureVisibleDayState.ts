import { selector } from 'recoil';
import { addDays, DateArgument, toDate } from '@/lib/date';
import { TimeDirection } from '../types';
import { isVisibleDayState } from './isVisibleDayState';

export type EnsureVisibleDay = (arg: DateArgument, direction: TimeDirection) => Date;

export const ensureVisibleDayState = selector<EnsureVisibleDay>({
  key: 'timeline/ensureVisibleDay',
  get: ({ get }) => {
    const isVisibleDay = get(isVisibleDayState);

    return (arg, direction) => {
      let date = toDate(arg);

      while (!isVisibleDay(date)) {
        date = addDays(date, direction);
      }

      return date;
    };
  },
});
