import { selector } from 'recoil';
import { RelationType } from '@/models/issueRelation';
import { DraftIssueRelation } from '../types';
import { issueRelationCreationState } from './issueRelationCreationState';

export const draftIssueRelationState = selector<DraftIssueRelation | null>({
  key: 'timeline/draftIssueRelation',
  get: ({ get }) => {
    const issueRelationCreation = get(issueRelationCreationState);

    if (!issueRelationCreation?.targetIssueId) {
      return null;
    }

    const issueIds = [issueRelationCreation.sourceIssueId, issueRelationCreation.targetIssueId];

    if (issueRelationCreation.type === 'to') {
      issueIds.reverse();
    }

    return {
      relationType: RelationType.Draft,
      issueFromId: issueIds[0],
      issueToId: issueIds[1],
    };
  },
});
