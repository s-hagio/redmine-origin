import { atom } from 'recoil';
import { Viewport } from '../types';

export const timelineViewportState = atom<Viewport>({
  key: 'timeline/viewport',
  default: {
    width: 0,
    height: 0,
    offsetTop: 0,
    offsetLeft: 0,
  },
});
