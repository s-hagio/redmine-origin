import { useRecoilCallback } from 'recoil';
import { issueRelationCreationState } from '../states';
import { IssueRelationCreation } from '../types';

type Params = Pick<IssueRelationCreation, 'type' | 'sourceIssueId'>;

type ActivateIssueRelationCreation = (params: Params) => void;

export const useActivateIssueRelationCreation = (): ActivateIssueRelationCreation => {
  return useRecoilCallback(({ set }) => {
    return (params) => {
      set(issueRelationCreationState, { ...params, isLocked: false });
    };
  });
};
