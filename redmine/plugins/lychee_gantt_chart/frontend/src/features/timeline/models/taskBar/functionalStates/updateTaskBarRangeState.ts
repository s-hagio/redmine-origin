import { selectorFamily } from 'recoil';
import { buildTimeBarState, TimeBarRange } from '../../timeBar';
import { TaskBarResourceIdentifier } from '../types';
import { taskBarState } from '../states';

type UpdateTaskBarRange = (newRange: TimeBarRange) => void;

export const updateTaskBarRangeState = selectorFamily<UpdateTaskBarRange, TaskBarResourceIdentifier>({
  key: 'timeline/updateTaskBarRange',
  get:
    (identifier) =>
    ({ get, getCallback }) => {
      const buildTimeBar = get(buildTimeBarState);

      return getCallback(({ set }) => {
        return (newRange) => {
          set(taskBarState(identifier), (current) => {
            if (!current) {
              return current;
            }

            return {
              ...current,
              ...buildTimeBar(newRange.startDate, newRange.endDate),
            };
          });
        };
      });
    },
});
