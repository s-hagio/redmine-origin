import { selectorFamily } from 'recoil';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { issueBaselineSnapshotState } from '@/models/baseline';
import { extractIssueBarParams, TaskBarParams } from '../../taskBar';

export const issueBaselineBarParamsState = selectorFamily<TaskBarParams | null, IssueResourceIdentifier>({
  key: 'timeline/issueBaselineBarParams',
  get: (identifier) => {
    return ({ get }) => {
      return extractIssueBarParams(get(issueBaselineSnapshotState(identifier)));
    };
  },
});
