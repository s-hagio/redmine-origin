import { atom } from 'recoil';

export const isNeedToCenteringState = atom<boolean>({
  key: 'timeline/isNeedToCentering',
  default: false,
});
