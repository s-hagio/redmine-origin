import { getTaskBarResourceTypeFromIdentifier } from '../getTaskBarResourceTypeFromIdentifier';

test('TaskBarResourceIdentifierからResourceTypeを取り出す', () => {
  expect(getTaskBarResourceTypeFromIdentifier('project-1')).toBe('project');
  expect(getTaskBarResourceTypeFromIdentifier('version-1-2')).toBe('version');
  expect(getTaskBarResourceTypeFromIdentifier('issue-1')).toBe('issue');
});
