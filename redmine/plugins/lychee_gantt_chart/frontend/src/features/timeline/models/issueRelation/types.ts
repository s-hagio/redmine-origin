import { BaseIssueRelation } from '@/models/issueRelation';
import { IssueID } from '@/models/issue';

type YOffset = {
  top: number;
  middle: number;
  bottom: number;
};

export type IssueRelationLineOffsets = {
  from: YOffset & { right: number };
  to: YOffset & { left: number };
};

export type DraftIssueRelation = Pick<BaseIssueRelation, 'issueFromId' | 'issueToId'> & {
  relationType: 'draft';
};

export type IssueRelationConnectionType = 'from' | 'to';

export type IssueRelationCreation = {
  type: IssueRelationConnectionType;
  sourceIssueId: IssueID;
  targetIssueId?: IssueID | null;
  isLocked: boolean;
};
