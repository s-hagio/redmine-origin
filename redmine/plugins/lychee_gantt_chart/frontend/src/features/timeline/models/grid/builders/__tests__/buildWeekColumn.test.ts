import { toDate } from '@/lib/date';
import { buildWeekColumn } from '../buildWeekColumn';
import { helpers } from './__fixtures__';

test('WeekColumnを構築', () => {
  const groupedTicks = [toDate('2022-07-04'), toDate('2022-07-05'), toDate('2022-07-10')];
  const stepWidth = 4;

  expect(buildWeekColumn(groupedTicks, stepWidth, helpers)).toStrictEqual({
    id: 'week-2022-07-04',
    type: 'week',
    label: '4',
    width: 12,
  });
});
