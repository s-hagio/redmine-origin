import { useCallback } from 'react';
import { useRecoilValue } from 'recoil';
import { useIsNonWorkingDay } from '@/models/workingDay';
import { stepWidthState, timeAxisOptionsState, useIsVisibleDay } from '../../timeAxis';
import { buildRows } from '../builders';
import { GridRow } from '../types';

type BuildRows = (ticks: Date[]) => GridRow[];

export const useBuildGridRows = (): BuildRows => {
  const { type } = useRecoilValue(timeAxisOptionsState);
  const stepWidth = useRecoilValue(stepWidthState);

  const isNonWorkingDay = useIsNonWorkingDay();
  const isVisibleDay = useIsVisibleDay();

  return useCallback(
    (ticks) => buildRows(ticks, type, stepWidth, { isNonWorkingDay, isVisibleDay }),
    [type, stepWidth, isNonWorkingDay, isVisibleDay]
  );
};
