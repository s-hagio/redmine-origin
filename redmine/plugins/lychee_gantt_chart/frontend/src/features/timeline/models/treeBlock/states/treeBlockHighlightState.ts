import { atomFamily } from 'recoil';
import { TreeBlockRootID } from '../types';

export const treeBlockHighlightState = atomFamily<boolean, TreeBlockRootID>({
  key: 'timeline/treeBlockHighlight',
  default: false,
});
