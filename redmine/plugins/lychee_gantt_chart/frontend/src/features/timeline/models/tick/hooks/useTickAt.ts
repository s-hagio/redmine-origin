import { useRecoilValue } from 'recoil';
import { tickAtState } from '../functionalStates';

export const useTickAt = () => useRecoilValue(tickAtState);
