import {
  isIssueResourceIdentifier,
  isProjectResourceIdentifier,
  isVersionResourceIdentifier,
} from '@/models/coreResource';
import { BaselineSnapshotIdentifier } from '@/models/baseline';

export const isBaselineSnapshotResourceIdentifier = (
  identifier: unknown
): identifier is BaselineSnapshotIdentifier => {
  return (
    isProjectResourceIdentifier(identifier) ||
    isVersionResourceIdentifier(identifier) ||
    isIssueResourceIdentifier(identifier)
  );
};
