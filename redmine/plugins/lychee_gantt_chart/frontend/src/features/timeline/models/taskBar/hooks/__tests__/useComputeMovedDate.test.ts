import { renderHook } from '@testing-library/react';
import { expect } from 'vitest';
import { useComputeMovedDate } from '../useComputeMovedDate';

const useStepWidthMock = vi.hoisted(() => vi.fn());
const addVisibleDaysMock = vi.hoisted(() => vi.fn());

vi.mock('../../../timeAxis', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useStepWidth: useStepWidthMock,
  useAddVisibleDays: vi.fn().mockReturnValue(addVisibleDaysMock),
}));

test('移動量から移動日数を計算した可視日を返す', () => {
  const movedDate = new Date(2025, 1, 1);
  addVisibleDaysMock.mockReturnValue(movedDate);

  const { result } = renderHook(() => useComputeMovedDate());

  expect(result.current('2023-09-10', 10)).toBe(movedDate);
});

test('移動量をstepWidthで割って少数を丸めた値が移動日数', () => {
  useStepWidthMock.mockReturnValue(5);

  const { result } = renderHook(() => useComputeMovedDate());

  const movedValuePairs = [
    [9, 2],
    [10, 2],
    [12, 2],
    [13, 3],
    [99, 20],
  ] as const;

  expect.assertions(movedValuePairs.length);

  movedValuePairs.forEach(([movedValue, movedDays]) => {
    result.current('2023-09-12', movedValue);

    expect(addVisibleDaysMock).toBeCalledWith('2023-09-12', movedDays);
  });
});
