import { atomFamily } from 'recoil';
import { TaskBarResourceIdentifier, TaskBarAdditionalValueAdderName, TaskBarBoundingRect } from '../types';

export const additionalTaskBarBoundingRectMapObjectState = atomFamily<
  Record<TaskBarAdditionalValueAdderName, TaskBarBoundingRect>,
  TaskBarResourceIdentifier
>({
  key: 'timeline/additionalTaskBarBoundingRectMapObject',
  default: () => ({}),
});
