import { atom } from 'recoil';
import { GridRow } from '../types';

export const gridRowsState = atom<GridRow[]>({
  key: 'timeline/gridRows',
  default: [],
});
