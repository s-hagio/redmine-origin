import { constSelector, useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { ISODateString, toDate } from '@/lib/date';
import { settingsState } from '@/models/setting';
import { ticksState } from '../../../tick';
import { timeAxisOptionsState } from '../../../timeAxis';
import { buildTimeBarState } from '../buildTimeBarState';

const startDate = '2022-07-01';
const endDate = '2022-07-05';

const stepWidthStateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../timeAxis', async (importOriginal) =>
  Object.defineProperties(await importOriginal<object>(), {
    stepWidthState: { get: stepWidthStateMock },
  })
);

stepWidthStateMock.mockReturnValue(constSelector(1));

const renderForTesting = (nonWorkingDays: ISODateString[] = []) => {
  const renderResult = renderRecoilHook(
    () => ({
      build: useRecoilValue(buildTimeBarState),
      setSettings: useSetRecoilState(settingsState),
    }),
    {
      initializeState: ({ set }) => {
        set(timeAxisOptionsState, (current) => ({
          ...current,
          includeNonWorkingDays: false,
        }));
        set(
          ticksState,
          ['2022-06-30', '2022-07-01', '2022-07-02', '2022-07-03', '2022-07-04', '2022-07-05'].map((s) => {
            return toDate(s);
          })
        );
      },
    }
  );

  act(() => {
    renderResult.result.current.setSettings((current) => ({ ...current, nonWorkingDays }));
  });

  return renderResult;
};

test('開始日と終了日からTimeBarを作成する', () => {
  const { result } = renderForTesting();

  expect(result.current.build(startDate, endDate)).toStrictEqual({
    width: 5,
    left: 1,
    right: 6,
  });
});

test('開始日／終了日が不可視日の場合は内側にズラす', () => {
  const { result } = renderForTesting(['2022-07-01', '2022-07-05']);

  expect(result.current.build(startDate, endDate)).toStrictEqual({
    width: 3,
    left: 2,
    right: 5,
  });
});
