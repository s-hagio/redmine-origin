import { useRecoilCallback } from 'recoil';
import { visibleIssueTreeRootRowIdsState } from '@/features/row';
import { treeBlockHighlightState } from '../states';
import { TreeBlockRootID } from '../types';

type SetTreeBlockHighlight = (highlight: boolean) => void;

export const useSetTreeBlockHighlight = (rootIds: ReadonlyArray<TreeBlockRootID>): SetTreeBlockHighlight => {
  return useRecoilCallback(
    ({ snapshot, set }) => {
      return (highlight) => {
        const visibleTreeRootIds = snapshot.getLoadable(visibleIssueTreeRootRowIdsState).getValue();
        const closestRootId = rootIds.find((id) => visibleTreeRootIds.includes(id));

        if (!closestRootId) {
          return;
        }

        set(treeBlockHighlightState(closestRootId), highlight);
      };
    },
    [rootIds]
  );
};
