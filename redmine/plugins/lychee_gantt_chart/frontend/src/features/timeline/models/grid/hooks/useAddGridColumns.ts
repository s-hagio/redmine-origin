import { useRecoilCallback } from 'recoil';
import { Tick } from '../../tick';
import { gridRowsState } from '../states';
import { isDayGridRow, isUnitedGridRow } from '../helpers';
import { TimeDirection } from '../../timeAxis';
import { UnitedGridColumn } from '../types';
import { useBuildGridRows } from './useBuildGridRows';

type AddGridColumns = (newTicks: Tick[], direction: TimeDirection) => void;

export const useAddGridColumns = (): AddGridColumns => {
  const buildRows = useBuildGridRows();

  return useRecoilCallback(
    ({ set }) => {
      return (newTicks, direction) => {
        set(gridRowsState, (currentRows) => {
          return buildRows(newTicks).map((row, index) => {
            const currentRow = currentRows[index];
            const columns = [...currentRow.columns.map((column) => ({ ...column }))];

            if (isDayGridRow(row)) {
              switch (direction) {
                case TimeDirection.Forward:
                  columns.push(...row.columns);
                  break;
                case TimeDirection.Backward:
                  columns.unshift(...row.columns);
                  break;
              }
            }

            if (isUnitedGridRow(row)) {
              switch (direction) {
                case TimeDirection.Forward:
                  mergeUnitedColumnsForward(columns, row.columns);
                  break;
                case TimeDirection.Backward:
                  mergeUnitedColumnsBackward(columns, row.columns);
                  break;
              }
            }

            return { ...currentRow, columns };
          });
        });
      };
    },
    [buildRows]
  );
};

const mergeUnitedColumnsForward = (targetColumns: UnitedGridColumn[], sourceColumns: UnitedGridColumn[]) => {
  const targetColumnIndexes = new Map(targetColumns.map(({ id }, index) => [id, index]));

  sourceColumns.forEach((sourceColumn) => {
    const index = targetColumnIndexes.get(sourceColumn.id);

    if (index == null) {
      targetColumns.push(sourceColumn);
    } else {
      targetColumns[index].width += sourceColumn.width;
    }
  });
};

const mergeUnitedColumnsBackward = (targetColumns: UnitedGridColumn[], sourceColumns: UnitedGridColumn[]) => {
  const startLength = targetColumns.length;
  const targetColumnIndexes = new Map(targetColumns.map(({ id }, index) => [id, index]));

  sourceColumns.reverse().forEach((sourceColumn) => {
    const index = targetColumnIndexes.get(sourceColumn.id);

    if (index == null) {
      targetColumns.unshift(sourceColumn);
      return;
    }

    const fixedIndex = index + (targetColumns.length - startLength);

    targetColumns[fixedIndex].width += sourceColumn.width;
  });
};
