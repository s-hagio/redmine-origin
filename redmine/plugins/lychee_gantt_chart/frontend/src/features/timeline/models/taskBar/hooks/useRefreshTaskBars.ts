import { useRecoilCallback, useRecoilValue } from 'recoil';
import { buildTaskBarState } from '../functionalStates';
import { taskBarDependenciesState, taskBarMapState, taskBarParamsState, taskBarState } from '../states';
import { isTaskBarResourceIdentifier } from '../helpers';
import { TaskBar, TaskBarResourceIdentifier } from '../types';

export const useRefreshTaskBars = () => {
  const dependencies = useRecoilValue(taskBarDependenciesState);

  return useRecoilCallback(
    ({ snapshot, transact_UNSTABLE }) => {
      return () => {
        const map: Map<TaskBarResourceIdentifier, TaskBar | null> = new Map();

        dependencies.availableRows.forEach((row) => {
          if (!isTaskBarResourceIdentifier(row.id)) {
            return;
          }

          const params = snapshot.getLoadable(taskBarParamsState(row.id)).getValue();
          const buildTaskBar = snapshot.getLoadable(buildTaskBarState(row.id)).getValue();

          map.set(row.id, params ? buildTaskBar(params) : null);
        });

        transact_UNSTABLE(({ set }) => {
          set(taskBarMapState, map);

          map.forEach((taskBar, identifier) => {
            set(taskBarState(identifier), taskBar);
          });
        });
      };
    },
    [dependencies]
  );
};
