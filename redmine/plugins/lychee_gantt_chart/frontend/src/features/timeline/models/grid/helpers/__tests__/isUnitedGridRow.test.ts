import { isUnitedGridRow } from '../isUnitedGridRow';
import { GridType } from '../../types';

const unitedGridTypes = ['year', 'yearMonth', 'month', 'week'] as GridType[];
const otherTypes = (Object.values(GridType) as GridType[]).filter((type) => !unitedGridTypes.includes(type));

test('オブジェクトがUnitedGridRowならtrue', () => {
  expect.assertions(4);

  unitedGridTypes.forEach((type) => {
    expect(isUnitedGridRow({ type, columns: [] }), type).toBe(true);
  });
});

test('それ以外はfalse', () => {
  expect.assertions(otherTypes.length);

  otherTypes.forEach((type) => {
    expect(isUnitedGridRow({ type, columns: [] }), type).toBe(false);
  });
});
