import { useSetRecoilState } from 'recoil';
import { isNeedToCenteringState } from '../states';

export const useSetNeedToCentering = () => useSetRecoilState(isNeedToCenteringState);
