import { describe } from 'vitest';
import { renderRecoilHook } from '@/test-utils';
import { leafIssue } from '@/__fixtures__';
import { useIssueTaskBarDraggability } from '../useIssueTaskBarDraggability';
import { issueRelationCreationState } from '../../../issueRelation';

const useIsEditableFieldMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldRule', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIsEditableField: vi.fn().mockReturnValue(useIsEditableFieldMock),
}));

const issue = leafIssue;

beforeEach(() => {
  useIsEditableFieldMock.mockReturnValue(true);
});

test('チケットのタスクバーがドラッグ可能かどうかの情報をメモ化して返す', () => {
  const { result, rerender } = renderRecoilHook(() => useIssueTaskBarDraggability(issue));

  expect(result.current).toEqual({
    isDraggable: true,
    isRangeDraggable: true,
    isStartDateDraggable: true,
    isDueDateDraggable: true,
  });

  const previous = result.current;
  rerender();
  expect(result.current).toBe(previous);
});

test('「関連するチケット」の作成中はドラッグ不可', () => {
  const { result } = renderRecoilHook(() => useIssueTaskBarDraggability(issue), {
    initializeState: ({ set }) => {
      set(issueRelationCreationState, { type: 'from', sourceIssueId: 1, isLocked: false });
    },
  });

  expect(result.current).toEqual({
    isDraggable: false,
    isRangeDraggable: false,
    isStartDateDraggable: false,
    isDueDateDraggable: false,
  });
});

describe('startDateが編集不可な場合', () => {
  beforeEach(() => {
    useIsEditableFieldMock.mockImplementation((_context, fieldName) => fieldName !== 'startDate');
  });

  test('isRangeDraggableとisStartDateDraggableがfalse', () => {
    const { result } = renderRecoilHook(() => useIssueTaskBarDraggability(issue));

    expect(result.current).toEqual({
      isDraggable: true,
      isRangeDraggable: false,
      isStartDateDraggable: false,
      isDueDateDraggable: true,
    });
  });
});

describe('dueDateが編集不可な場合', () => {
  beforeEach(() => {
    useIsEditableFieldMock.mockImplementation((_context, fieldName) => fieldName !== 'dueDate');
  });

  test('isRangeDraggableとisDueDateDraggableがfalse', () => {
    const { result } = renderRecoilHook(() => useIssueTaskBarDraggability(issue));

    expect(result.current).toEqual({
      isDraggable: true,
      isRangeDraggable: false,
      isStartDateDraggable: true,
      isDueDateDraggable: false,
    });
  });
});
