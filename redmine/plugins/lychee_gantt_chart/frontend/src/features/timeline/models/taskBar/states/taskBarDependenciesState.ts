import { selector, waitForAll } from 'recoil';
import { availableRowsState } from '@/features/row';
import { buildTimeBarState } from '../../timeBar';

export const taskBarDependenciesState = selector({
  key: 'timeline/taskBarDependencies',
  get: ({ get }) => {
    return get(
      waitForAll({
        availableRows: availableRowsState,
        buildTimeBar: buildTimeBarState,
      })
    );
  },
});
