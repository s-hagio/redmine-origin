import { formatYearMonth } from '@/lib/date';
import { GridType } from '../types';
import { BuildUnitedColumn } from './types';

export const buildYearMonthColumn: BuildUnitedColumn = (groupedTicks, stepWidth) => {
  const firstTick = groupedTicks[0];
  const yearMonth = formatYearMonth(firstTick);

  return {
    id: `${GridType.YearMonth}-${yearMonth}`,
    type: GridType.YearMonth,
    label: yearMonth,
    width: groupedTicks.length * stepWidth,
  };
};
