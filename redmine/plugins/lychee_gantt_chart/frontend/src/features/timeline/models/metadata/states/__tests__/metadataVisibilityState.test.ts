import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { metadataVisibilityState } from '../metadataVisibilityState';
import { DEFAULT_VISIBILITY_COLLECTION, METADATA_VISIBILITY_STORED_KEY } from '../../constants';

test('default', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(metadataVisibilityState('title')));

  expect(result.current).toBe(DEFAULT_VISIBILITY_COLLECTION.title);
});

test('localStorageから復元', () => {
  vi.spyOn(localStorage, 'getItem').mockReturnValue(JSON.stringify({ status: 'hidden' }));

  const { result } = renderRecoilHook(() => useRecoilValue(metadataVisibilityState('status')));

  expect(result.current).toBe('hidden');

  expect(localStorage.getItem).toBeCalledWith(METADATA_VISIBILITY_STORED_KEY);
  expect(result.current).not.toBe(DEFAULT_VISIBILITY_COLLECTION.status);
});

test('変更時にlocalStorageへ保存', () => {
  const storedValue = { status: 'hidden' };

  vi.spyOn(localStorage, 'getItem').mockReturnValue(JSON.stringify(storedValue));
  vi.spyOn(localStorage, 'setItem');

  const { result } = renderRecoilHook(() => ({
    set: useSetRecoilState(metadataVisibilityState('assignedUser')),
  }));

  expect(localStorage.setItem).not.toBeCalled();

  act(() => {
    result.current.set('hidden');
  });

  expect(localStorage.setItem).lastCalledWith(
    METADATA_VISIBILITY_STORED_KEY,
    JSON.stringify({ ...storedValue, assignedUser: 'hidden' })
  );

  act(() => {
    result.current.set('visible');
  });

  expect(localStorage.setItem).lastCalledWith(
    METADATA_VISIBILITY_STORED_KEY,
    JSON.stringify({ ...storedValue, assignedUser: 'visible' })
  );
});
