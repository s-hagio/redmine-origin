import { IssueResourceIdentifier } from '@/models/coreResource';

export type TreeBlockRootID = IssueResourceIdentifier;

export type TreeBlock = {
  top: number;
  right: number;
  bottom: number;
  left: number;
};
