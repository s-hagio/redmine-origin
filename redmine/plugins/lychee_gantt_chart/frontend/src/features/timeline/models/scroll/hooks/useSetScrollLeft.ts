import { useSetRecoilState } from 'recoil';
import { scrollLeftState } from '../states';

export const useSetScrollLeft = () => useSetRecoilState(scrollLeftState);
