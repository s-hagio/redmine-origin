import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { addVisibleDaysState } from '../addVisibleDaysState';
import { timeAxisOptionsState } from '../../states';

const renderForTesting = (includeNonWorkingDays: boolean) => {
  return renderRecoilHook(() => useRecoilValue(addVisibleDaysState), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({
        ...current,
        nonWorkingDays: ['2023-09-12', '2023-09-13'],
      }));

      set(timeAxisOptionsState, (current) => ({ ...current, includeNonWorkingDays }));
    },
  });
};

describe('休業日を含まない', () => {
  test('可視日だけを対象に指定した日数を追加した日付を返す', () => {
    const { result } = renderForTesting(false);

    expect(result.current('2023-09-11', 5)).toEqual(new Date(2023, 8, 18));
  });

  test('負の値で日数を指定した場合はsubtract', () => {
    const { result } = renderForTesting(false);

    expect(result.current('2023-09-15', -5)).toEqual(new Date(2023, 8, 8));
  });
});

describe('休業日を含む', () => {
  test('そのまま日数を追加した日付を返す', () => {
    const { result } = renderForTesting(true);

    expect(result.current('2023-09-11', 5)).toEqual(new Date(2023, 8, 16));
  });
});
