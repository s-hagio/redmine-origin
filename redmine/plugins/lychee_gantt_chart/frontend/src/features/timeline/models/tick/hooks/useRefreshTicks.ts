import { useRecoilCallback, useRecoilValue } from 'recoil';
import { toDate } from '@/lib/date';
import { timeAxisOptionsState } from '../../timeAxis';
import { ticksState } from '../states';
import { useBuildTicks } from './useBuildTicks';

type RefreshTicks = (sizeOfDirection: number) => void;

export const useRefreshTicks = (): RefreshTicks => {
  const { date } = useRecoilValue(timeAxisOptionsState);
  const buildTicks = useBuildTicks();

  return useRecoilCallback(
    ({ set }) => {
      return (sizePerDirection) => {
        const centerDate = toDate(date || new Date());
        const ticks = buildTicks(centerDate, { sizePerDirection });

        set(ticksState, ticks);
      };
    },
    [date, buildTicks]
  );
};
