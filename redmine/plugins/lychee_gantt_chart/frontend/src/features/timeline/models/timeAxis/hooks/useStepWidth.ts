import { useRecoilValue } from 'recoil';
import { stepWidthState } from '../states';

export const useStepWidth = () => useRecoilValue(stepWidthState);
