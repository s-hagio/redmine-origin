import { selectorFamily } from 'recoil';
import { TimeBar, buildTimeBarState } from '../../timeBar';
import { todayTickEndPositionState } from '../../tick';
import { TaskBar, TaskBarBoundingRect, TaskBarParams, TaskBarResourceIdentifier } from '../types';
import { buildTaskBarBoundingRectState } from './buildTaskBarBoundingRectState';

export type BuildTaskBar = (params: TaskBarParams, additionalBoundingRect?: TaskBarBoundingRect) => TaskBar;

export const buildTaskBarState = selectorFamily<BuildTaskBar, TaskBarResourceIdentifier>({
  key: 'timeline/buildTaskBar',
  get: (identifier) => {
    return ({ get }) => {
      const buildTimeBar = get(buildTimeBarState);
      const buildTaskBarBoundingRect = get(buildTaskBarBoundingRectState(identifier));
      const todayTickEndPosition = get(todayTickEndPositionState);

      return ({ resourceType, startDate, endDate, progressRate }) => {
        const timeBar = buildTimeBar(startDate, endDate);
        const boundingRect = buildTaskBarBoundingRect();
        const lateWidth = calcLateWidth(timeBar, todayTickEndPosition);
        const doneWidth = timeBar.width * ((progressRate ?? 0) / 100);

        return { ...timeBar, ...boundingRect, resourceType, lateWidth, doneWidth };
      };
    };
  },
});

const calcLateWidth = (timeBar: TimeBar, todayTickEndPosition: number): number => {
  if (timeBar.left > todayTickEndPosition) {
    return 0;
  }

  if (timeBar.right < todayTickEndPosition) {
    return timeBar.width;
  }

  return timeBar.width - (timeBar.right - todayTickEndPosition);
};
