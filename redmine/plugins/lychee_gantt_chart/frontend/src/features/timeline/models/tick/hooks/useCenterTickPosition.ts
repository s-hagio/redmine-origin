import { useRecoilValue } from 'recoil';
import { timeAxisOptionsState } from '../../timeAxis';
import { usePositionOf } from './usePositionOf';

export const useCenterTickPosition = () => {
  const { date } = useRecoilValue(timeAxisOptionsState);
  const positionOf = usePositionOf();

  return positionOf(date || new Date());
};
