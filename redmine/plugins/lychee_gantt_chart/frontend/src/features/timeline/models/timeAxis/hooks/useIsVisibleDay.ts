import { useRecoilValue } from 'recoil';
import { isVisibleDayState } from '../functionalStates';

export const useIsVisibleDay = () => useRecoilValue(isVisibleDayState);
