import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { toDate } from '@/lib/date';
import { timeAxisOptionsState } from '../../../timeAxis';
import { ticksState } from '../../states';
import { useRefreshTicks } from '../useRefreshTicks';
import { useBuildTicks } from '../useBuildTicks';

vi.mock('../useBuildTicks', () => ({
  useBuildTicks: vi.fn(),
}));

const ticks = ['2022-07-01', '2022-07-02', '2022-07-03'].map((dateString) => toDate(dateString));
const buildTicksMock = vi.fn().mockReturnValue(ticks);
vi.mocked(useBuildTicks).mockReturnValue(buildTicksMock);

const render = () => {
  return renderRecoilHook(() => ({
    refresh: useRefreshTicks(),
    setOptions: useSetRecoilState(timeAxisOptionsState),
    ticks: useRecoilValue(ticksState),
  }));
};

test('コンテナ要素の幅と基準日を元にTickリストを更新', () => {
  const { result } = render();
  const baseDate = '2022-01-02';

  act(() => {
    result.current.setOptions((current) => ({ ...current, date: baseDate }));
  });

  expect(result.current.ticks).toEqual([]);

  act(() => {
    result.current.refresh(100);
  });

  expect(buildTicksMock).toHaveBeenLastCalledWith(toDate(baseDate), { sizePerDirection: 100 });
  expect(result.current.ticks).toBe(ticks);
});

test('基準日が設定されていない場合は当日を基準にTickリストを更新', () => {
  const { result } = render();
  const today = new Date();
  today.setHours(0, 0, 0, 0);

  act(() => {
    result.current.setOptions((current) => ({ ...current, date: null }));
  });

  expect(result.current.ticks).toEqual([]);

  act(() => {
    result.current.refresh(200);
  });

  expect(buildTicksMock).toHaveBeenLastCalledWith(today, { sizePerDirection: 200 });
  expect(result.current.ticks).toBe(ticks);
});
