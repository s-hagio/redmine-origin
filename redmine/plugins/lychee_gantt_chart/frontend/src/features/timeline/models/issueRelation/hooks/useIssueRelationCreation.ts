import { useRecoilValue } from 'recoil';
import { issueRelationCreationState } from '../states';

export const useIssueRelationCreation = () => useRecoilValue(issueRelationCreationState);
