import { selector } from 'recoil';
import { DateArgument, formatISODate, isBeforeDay, toDate } from '@/lib/date';
import { calcVisibleDaysState, isVisibleDayState } from '../../timeAxis';
import { tickIdentifiersState, ticksState } from '../states';

export type IndexOf = (arg: DateArgument) => number;

export const indexOfState = selector<IndexOf>({
  key: 'timeline/indexOf',
  get: ({ get }) => {
    const isVisibleDay = get(isVisibleDayState);
    const calcVisibleDays = get(calcVisibleDaysState);

    const ticks = get(ticksState);
    const tickIdentifiers = get(tickIdentifiersState);

    return (arg) => {
      if (!ticks.length || !isVisibleDay(arg)) {
        return Number.NaN;
      }

      const date = toDate(arg);
      const index = tickIdentifiers.indexOf(formatISODate(date));

      if (index !== -1) {
        return index;
      }

      if (isBeforeDay(date, ticks[0])) {
        return -(calcVisibleDays(date, ticks[0]) - 1);
      }

      const lastIndex = ticks.length - 1;

      return lastIndex + calcVisibleDays(ticks[lastIndex], date) - 1;
    };
  },
});
