import { useRecoilValue } from 'recoil';
import { addVisibleDaysState } from '../functionalStates';

export const useAddVisibleDays = () => useRecoilValue(addVisibleDaysState);
