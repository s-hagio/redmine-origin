import { selectorFamily } from 'recoil';
import { getTaskBarRelativeBoundingRect, getTaskBarResourceTypeFromIdentifier } from '../helpers';
import { TaskBarBoundingRect, TaskBarResourceIdentifier } from '../types';
import { additionalTaskBarBoundingRectsState } from '../states';

export type BuildTaskBarBoundingRect = () => TaskBarBoundingRect;

export const buildTaskBarBoundingRectState = selectorFamily<
  BuildTaskBarBoundingRect,
  TaskBarResourceIdentifier
>({
  key: 'timeline/buildTaskBarBoundingRect',
  get: (identifier) => {
    return ({ get }) => {
      const additionalBoundingRects = get(additionalTaskBarBoundingRectsState(identifier));

      return () => {
        const resourceType = getTaskBarResourceTypeFromIdentifier(identifier);
        const bounds = { ...getTaskBarRelativeBoundingRect(resourceType) };

        return additionalBoundingRects.reduce((memo, bounds) => {
          memo.top += bounds.top;
          memo.height += bounds.height;

          return memo;
        }, bounds);
      };
    };
  },
});
