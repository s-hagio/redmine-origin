import { describe } from 'vitest';
import { formatISODate, toDate } from '@/lib/date';
import { buildDayAttributes } from '../buildDayAttributes';
import { helpers as baseHelpers } from './__fixtures__';

const nonWorkingDays = new Set(['2022-07-02', '2022-07-04']);

describe('休業日表示: ON', () => {
  const helpers = { ...baseHelpers };

  vi.spyOn(helpers, 'isNonWorkingDay').mockImplementation((arg) => nonWorkingDays.has(formatISODate(arg)));
  vi.spyOn(helpers, 'isVisibleDay').mockReturnValue(true);

  test('Tickの前後が休業日', () => {
    expect(buildDayAttributes(toDate('2022-07-03'), helpers)).toStrictEqual({
      isNonWorkingDay: false,
      isBeforeInvisibleDay: false,
      isAfterInvisibleDay: false,
    });
  });

  test('Tickの前日が休業日', () => {
    expect(buildDayAttributes(toDate('2022-07-05'), helpers)).toStrictEqual({
      isNonWorkingDay: false,
      isBeforeInvisibleDay: false,
      isAfterInvisibleDay: false,
    });
  });

  test('Tickの翌日が休業日', () => {
    expect(buildDayAttributes(toDate('2022-07-01'), helpers)).toStrictEqual({
      isNonWorkingDay: false,
      isBeforeInvisibleDay: false,
      isAfterInvisibleDay: false,
    });
  });

  test('Tickが休業日', () => {
    expect(buildDayAttributes(toDate('2022-07-02'), helpers)).toStrictEqual({
      isNonWorkingDay: true,
      isBeforeInvisibleDay: false,
      isAfterInvisibleDay: false,
    });
  });

  test('Tickの前後が稼働日', () => {
    expect(buildDayAttributes(toDate('2022-07-20'), helpers)).toStrictEqual({
      isNonWorkingDay: false,
      isBeforeInvisibleDay: false,
      isAfterInvisibleDay: false,
    });
  });
});

describe('休業日表示: OFF', () => {
  const helpers = { ...baseHelpers };

  vi.spyOn(helpers, 'isNonWorkingDay').mockImplementation((arg) => nonWorkingDays.has(formatISODate(arg)));
  vi.spyOn(helpers, 'isVisibleDay').mockImplementation((arg) => !helpers.isNonWorkingDay(arg));

  test('Tickの前後が休業日', () => {
    expect(buildDayAttributes(toDate('2022-07-03'), helpers)).toStrictEqual({
      isNonWorkingDay: false,
      isBeforeInvisibleDay: true,
      isAfterInvisibleDay: true,
    });
  });

  test('Tickの前日が休業日', () => {
    expect(buildDayAttributes(toDate('2022-07-05'), helpers)).toStrictEqual({
      isNonWorkingDay: false,
      isBeforeInvisibleDay: false,
      isAfterInvisibleDay: true,
    });
  });

  test('Tickの翌日が休業日', () => {
    expect(buildDayAttributes(toDate('2022-07-01'), helpers)).toStrictEqual({
      isNonWorkingDay: false,
      isBeforeInvisibleDay: true,
      isAfterInvisibleDay: false,
    });
  });

  test('Tickの前後が稼働日', () => {
    expect(buildDayAttributes(toDate('2022-07-20'), helpers)).toStrictEqual({
      isNonWorkingDay: false,
      isBeforeInvisibleDay: false,
      isAfterInvisibleDay: false,
    });
  });
});
