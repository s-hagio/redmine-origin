import { useSetRecoilState } from 'recoil';
import { taskBarState } from '../states';
import { TaskBarResourceIdentifier } from '../types';

export const useSetTaskBar = (identifier: TaskBarResourceIdentifier) => {
  return useSetRecoilState(taskBarState(identifier));
};
