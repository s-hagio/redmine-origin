import { useRecoilValue } from 'recoil';
import { treeBlockState } from '../states';
import { TreeBlockRootID } from '../types';

export const useTreeBlock = (id: TreeBlockRootID) => useRecoilValue(treeBlockState(id));
