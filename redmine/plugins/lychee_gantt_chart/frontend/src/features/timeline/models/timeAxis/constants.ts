import { TimeAxisType } from './types';

export const STORED_OPTIONS_KEY = 'lycheeGantt:timeAxisOptions';

export const DEFAULT_OPTIONS = {
  type: TimeAxisType.DayLarge,
  date: null,
  includeNonWorkingDays: true,
} as const;

export const timeAxisTypes: ReadonlyArray<TimeAxisType> = Object.values(TimeAxisType);
