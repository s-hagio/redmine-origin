import { MetadataVisibility, MetadataVisibilityCollection } from './types';

export const DEFAULT_VISIBILITY_COLLECTION: MetadataVisibilityCollection = {
  title: MetadataVisibility.VisibleOnlyWhenColumnIsHidden,
  status: MetadataVisibility.Visible,
  progressRate: MetadataVisibility.Visible,
  assignedUser: MetadataVisibility.VisibleOnlyWhenColumnIsHidden,
};

export const METADATA_VISIBILITY_STORED_KEY = 'lycheeGantt:metadataVisibility';
