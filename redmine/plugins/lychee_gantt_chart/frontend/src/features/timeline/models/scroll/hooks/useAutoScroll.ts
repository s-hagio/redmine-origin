import { useMemo, useRef } from 'react';
import { TimeDirection } from '../../timeAxis';
import { useSetScrollLeft } from './useSetScrollLeft';

type StartAutoScroll = () => void;
type StopAutoScroll = () => void;

type Result = {
  start: StartAutoScroll;
  stop: StopAutoScroll;
};

const SCROLL_SIZE = 10;

export const useAutoScroll = (direction: TimeDirection): Result => {
  const setScrollLeft = useSetScrollLeft();
  const autoScrollingIdRef = useRef<number | null>(null);

  return useMemo(() => {
    const requestScroll = () => {
      autoScrollingIdRef.current = window.requestAnimationFrame(() => {
        setScrollLeft((current) => current + SCROLL_SIZE * direction);
        requestScroll();
      });
    };

    const cancelScroll = () => {
      autoScrollingIdRef.current && window.cancelAnimationFrame(autoScrollingIdRef.current);
    };

    return {
      start: requestScroll,
      stop: cancelScroll,
    };
  }, [direction, setScrollLeft]);
};
