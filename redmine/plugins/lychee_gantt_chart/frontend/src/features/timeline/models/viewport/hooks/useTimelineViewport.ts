import { useRecoilValue } from 'recoil';
import { timelineViewportState } from '../states';

export const useTimelineViewport = () => useRecoilValue(timelineViewportState);
