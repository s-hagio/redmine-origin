import { useRecoilValue } from 'recoil';
import { mainGridRowState } from '../states';

export const useMainGridRow = () => useRecoilValue(mainGridRowState);
