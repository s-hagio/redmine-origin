import { Version } from '@/models/version';
import { TaskBarParams, TaskBarResourceType } from '../types';

type VersionTaskBarResource = Pick<Version, 'startDate' | 'dueDate' | 'completedPercent'>;

export const extractVersionBarParams = (version: VersionTaskBarResource | null): TaskBarParams | null => {
  if (!version || !version.startDate || !version.dueDate) {
    return null;
  }

  return {
    resourceType: TaskBarResourceType.Version,
    startDate: version.startDate,
    endDate: version.dueDate,
    progressRate: version.completedPercent ?? 0,
  };
};
