import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { getStepWidth } from '../../helpers';
import { stepWidthState, timeAxisOptionsState } from '../../states';
import { TimeAxisType } from '../../types';

vi.mock('../../helpers', async () => ({
  ...(await vi.importActual<object>('../../helpers')),
  getStepWidth: vi.fn().mockReturnValue(999),
}));

test('TimeAxisのタイプに適応するステップ幅を返す', () => {
  const { result } = renderRecoilHook(() => ({
    stepWidth: useRecoilValue(stepWidthState),
    setOptions: useSetRecoilState(timeAxisOptionsState),
  }));

  const type = TimeAxisType.MonthSmall;

  act(() => {
    result.current.setOptions((current) => ({ ...current, type }));
  });

  expect(result.current.stepWidth).toBe(999);
  expect(getStepWidth).toHaveBeenLastCalledWith(type);
});
