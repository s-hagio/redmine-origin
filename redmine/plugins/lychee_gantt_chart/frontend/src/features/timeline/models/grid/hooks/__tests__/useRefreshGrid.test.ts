import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { formatISODate, toDate } from '@/lib/date';
import { useRefreshGrid } from '../useRefreshGrid';
import { ticksState } from '../../../tick';
import { gridRowsState } from '../../states';

vi.mock('../useBuildGridRows', () => ({
  useBuildGridRows: vi.fn().mockReturnValue(vi.fn((ticks) => ticks.map(formatISODate))),
}));

test('ticksを使ってGridを更新', () => {
  const { result } = renderRecoilHook(
    () => ({
      refresh: useRefreshGrid(),
      gridRows: useRecoilValue(gridRowsState),
    }),
    {
      initializeState: ({ set }) => {
        set(ticksState, [toDate('2022-07-01'), toDate('2022-07-02')]);
      },
    }
  );

  expect(result.current.gridRows).toStrictEqual([]);

  act(() => {
    result.current.refresh();
  });

  expect(result.current.gridRows).toStrictEqual(['2022-07-01', '2022-07-02']);
});
