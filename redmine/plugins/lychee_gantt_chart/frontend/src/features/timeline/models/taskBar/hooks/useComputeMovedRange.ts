import { useCallback } from 'react';
import { MovedRangeValues } from '../types';
import { TimeBarRange } from '../../timeBar';
import { useComputeMovedDate } from './useComputeMovedDate';

export type ComputeMovedRange = (baseRange: TimeBarRange, movedValues: MovedRangeValues) => TimeBarRange;

export const useComputeMovedRange = (): ComputeMovedRange => {
  const computeMovedDate = useComputeMovedDate();

  return useCallback(
    (baseRange, movedValues) => {
      const range = {
        startDate: computeMovedDate(baseRange.startDate, movedValues.start ?? movedValues.both ?? 0),
        endDate: computeMovedDate(baseRange.endDate, movedValues.end ?? movedValues.both ?? 0),
      };

      if (range.startDate > range.endDate) {
        if (movedValues.start != null) {
          range.startDate = range.endDate;
        }

        if (movedValues.end != null) {
          range.endDate = range.startDate;
        }
      }

      return range;
    },
    [computeMovedDate]
  );
};
