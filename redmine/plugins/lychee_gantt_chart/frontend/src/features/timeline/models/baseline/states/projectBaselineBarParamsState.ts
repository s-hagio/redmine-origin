import { selectorFamily } from 'recoil';
import { ProjectResourceIdentifier } from '@/models/coreResource';
import { projectBaselineSnapshotState } from '@/models/baseline';
import { extractProjectBarParams, TaskBarParams } from '../../taskBar';

export const projectBaselineBarParamsState = selectorFamily<TaskBarParams | null, ProjectResourceIdentifier>({
  key: 'timeline/projectBaselineBarParams',
  get: (identifier) => {
    return ({ get }) => {
      return extractProjectBarParams(get(projectBaselineSnapshotState(identifier)));
    };
  },
});
