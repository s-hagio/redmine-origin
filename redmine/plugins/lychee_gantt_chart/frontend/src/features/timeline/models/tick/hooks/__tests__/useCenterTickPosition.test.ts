import { constSelector } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { toDate } from '@/lib/date';
import { settingsState } from '@/models/setting';
import { TimeAxisOptions, timeAxisOptionsState } from '../../../timeAxis';
import { useCenterTickPosition } from '../useCenterTickPosition';
import { ticksState } from '../../states';

const stepWidthStateMock = vi.hoisted(() => vi.fn());

vi.mock('../../../timeAxis', async (importOriginal) =>
  Object.defineProperties(await importOriginal<object>(), {
    stepWidthState: { get: stepWidthStateMock },
  })
);

stepWidthStateMock.mockReturnValue(constSelector(3));

const ticks = ['2023-03-01', '2023-03-02', '2023-03-03'] as const;
const nonWorkingDays = ['2023-03-04', '2023-03-05'] as const;

const renderWithTimeAxisDate = (date: TimeAxisOptions['date']) => {
  return renderRecoilHook(
    () => ({
      position: useCenterTickPosition(),
    }),
    {
      initializeState: ({ set }) => {
        set(
          ticksState,
          ticks.map((dateString) => toDate(dateString))
        );
        set(settingsState, (current) => ({ ...current, nonWorkingDays: [...nonWorkingDays] }));
        set(timeAxisOptionsState, (current) => ({
          ...current,
          date,
          includeNonWorkingDays: false,
        }));
      },
    }
  );
};

test('時間軸オプションで指定した日付の位置を返す', () => {
  const { result } = renderWithTimeAxisDate('2023-03-02');

  expect(result.current.position).toBe(3);
});

test('日付が未指定の場合は当日の位置を返す', () => {
  vi.useFakeTimers();
  vi.setSystemTime(new Date('2023-02-28'));

  const { result } = renderWithTimeAxisDate(null);

  expect(result.current.position).toBe(-3);
});

test('不可視日の場合は次の可視日の位置を返す', () => {
  const { result } = renderWithTimeAxisDate('2023-03-05');

  expect(result.current.position).toBe(9);
});
