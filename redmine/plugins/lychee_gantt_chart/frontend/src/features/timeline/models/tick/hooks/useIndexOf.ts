import { useRecoilValue } from 'recoil';
import { indexOfState } from '../functionalStates';

export const useIndexOf = () => useRecoilValue(indexOfState);
