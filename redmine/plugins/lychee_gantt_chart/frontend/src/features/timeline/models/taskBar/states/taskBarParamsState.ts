import { selectorFamily } from 'recoil';
import {
  getResourceIdFromIdentifier,
  isIssueResourceIdentifier,
  isProjectResourceIdentifier,
  isVersionResourceIdentifier,
} from '@/models/coreResource';
import { projectState } from '@/models/project';
import { versionState } from '@/models/version';
import { issueState } from '@/models/issue';
import { TaskBarParams, TaskBarResourceIdentifier } from '../types';
import { extractIssueBarParams, extractProjectBarParams, extractVersionBarParams } from '../helpers';

export const taskBarParamsState = selectorFamily<TaskBarParams | null, TaskBarResourceIdentifier>({
  key: 'timeline/taskBarParams',
  get: (rowId) => {
    return ({ get }) => {
      if (isProjectResourceIdentifier(rowId)) {
        return extractProjectBarParams(get(projectState(getResourceIdFromIdentifier(rowId))));
      } else if (isVersionResourceIdentifier(rowId)) {
        return extractVersionBarParams(get(versionState(getResourceIdFromIdentifier(rowId))));
      } else if (isIssueResourceIdentifier(rowId)) {
        return extractIssueBarParams(get(issueState(getResourceIdFromIdentifier(rowId))));
      }

      throw new Error(`Unknown resource identifier: ${rowId}`);
    };
  },
});
