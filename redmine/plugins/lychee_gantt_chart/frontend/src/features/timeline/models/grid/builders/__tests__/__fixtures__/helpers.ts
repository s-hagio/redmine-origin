import { BuildHelpers } from '../../types';

export const helpers: BuildHelpers = {
  isNonWorkingDay: () => false,
  isVisibleDay: () => true,
};
