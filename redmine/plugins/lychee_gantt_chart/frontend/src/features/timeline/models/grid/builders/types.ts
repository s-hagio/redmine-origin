import { IsNonWorkingDay } from '@/models/workingDay';
import { IsVisibleDay } from '../../timeAxis';
import { UnitedGridRow, DayGridRow, UnitedGridColumn, DayGridColumn } from '../types';
import { Tick } from '../../tick';

export type BuildHelpers = {
  isNonWorkingDay: IsNonWorkingDay;
  isVisibleDay: IsVisibleDay;
};

export type BuildUnitedRow = (ticks: Tick[], stepWidth: number, helpers: BuildHelpers) => UnitedGridRow;
export type BuildDayRow = (ticks: Tick[], stepWidth: number, helpers: BuildHelpers) => DayGridRow;

export type BuildRow = BuildUnitedRow | BuildDayRow;

export type BuildUnitedColumn = (
  groupedTicks: Tick[],
  stepWidth: number,
  helpers: BuildHelpers
) => UnitedGridColumn;

export type BuildDayColumn = (tick: Tick, stepWidth: number, helpers: BuildHelpers) => DayGridColumn;
