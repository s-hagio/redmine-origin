import { renderHook } from '@testing-library/react';
import { useGetTickByPosition } from '../useGetTickByPosition';

const useStepWidthMock = vi.hoisted(() => vi.fn());

vi.mock('../../../timeAxis', () => ({
  useStepWidth: useStepWidthMock,
}));

const tickAtMock = vi.hoisted(() => vi.fn());

vi.mock('../useTickAt', () => ({
  useTickAt: vi.fn().mockReturnValue(tickAtMock),
}));

const tick = new Date();

beforeEach(() => {
  useStepWidthMock.mockReturnValue(10);
  tickAtMock.mockReturnValue(tick);
});

test('指定位置のTickを返す', () => {
  const { result } = renderHook(() => useGetTickByPosition());

  expect(tickAtMock).not.toBeCalled();
  expect(result.current(205)).toBe(tick);
  expect(tickAtMock).toBeCalledWith(20);
});
