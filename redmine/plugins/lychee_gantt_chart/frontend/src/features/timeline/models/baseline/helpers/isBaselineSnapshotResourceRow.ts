import { Row } from '@/features/row';
import { BaselineSnapshotResourceRow } from '../types';
import { isBaselineSnapshotResourceIdentifier } from './isBaselineSnapshotResourceIdentifier';

export const isBaselineSnapshotResourceRow = (row: Row): row is BaselineSnapshotResourceRow => {
  return isBaselineSnapshotResourceIdentifier(row.id);
};
