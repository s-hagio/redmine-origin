import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { toDate } from '@/lib/date';
import { settingsState } from '@/models/setting';
import { ticksState } from '../../states';
import { timeAxisOptionsState } from '../../../timeAxis';
import { indexOfState } from '../indexOfState';

const renderForTesting = () => {
  return renderRecoilHook(() => useRecoilValue(indexOfState), {
    initializeState: ({ set }) => {
      set(
        ticksState,
        ['2022-07-08', '2022-07-09', '2022-07-10'].map((dateString) => toDate(dateString))
      );
      set(settingsState, (current) => ({ ...current, nonWorkingDays: ['2022-07-06', '2022-07-12'] }));
      set(timeAxisOptionsState, (current) => ({ ...current, includeNonWorkingDays: false }));
    },
  });
};

test('渡された日付のindexを返す', () => {
  const { result } = renderForTesting();

  expect(result.current('2022-07-08')).toBe(0);
  expect(result.current('2022-07-09')).toBe(1);
  expect(result.current(new Date(2022, 6, 10))).toBe(2);
});

test('範囲外の場合は不可視日を考慮したindexを返す', () => {
  const { result } = renderForTesting();

  expect(result.current('2022-07-04')).toBe(-3);
  expect(result.current(new Date(2022, 6, 13))).toBe(4);
});

test('不可視日を指定した場合はNumber.NaNを返す', () => {
  const { result } = renderForTesting();

  expect(result.current('2022-07-12')).toBe(Number.NaN);
});
