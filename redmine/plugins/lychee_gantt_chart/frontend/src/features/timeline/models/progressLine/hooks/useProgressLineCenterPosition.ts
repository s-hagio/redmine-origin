import { useRecoilValue } from 'recoil';
import { progressLineCenterPositionState } from '../states';

export const useProgressLineCenterPosition = () => useRecoilValue(progressLineCenterPositionState);
