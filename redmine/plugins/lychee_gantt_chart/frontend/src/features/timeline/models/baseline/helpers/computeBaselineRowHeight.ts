import { BaselineSnapshotResourceIdentifier } from '../types';
import { getBaselineBarRelativeBoundingRect } from './getBaselineBarRelativeBoundingRect';

export const computeBaselineRowHeight = (identifier: BaselineSnapshotResourceIdentifier): number => {
  const bounds = getBaselineBarRelativeBoundingRect(identifier);

  return bounds.top + bounds.height;
};
