import { useSetRecoilState } from 'recoil';
import { timelineViewportState } from '../states';

export const useSetTimelineViewport = () => useSetRecoilState(timelineViewportState);
