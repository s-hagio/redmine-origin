import { formatYearMonth } from '@/lib/date';
import { GridType } from '../types';
import { BuildUnitedColumn } from './types';

export const buildMonthColumn: BuildUnitedColumn = (groupedTicks, stepWidth) => {
  const firstTick = groupedTicks[0];

  return {
    id: `${GridType.Month}-${formatYearMonth(firstTick)}`,
    type: GridType.Month,
    label: `${firstTick.getMonth() + 1}`,
    width: groupedTicks.length * stepWidth,
  };
};
