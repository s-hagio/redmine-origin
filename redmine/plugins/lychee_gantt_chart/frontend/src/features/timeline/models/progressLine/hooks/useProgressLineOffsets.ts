import { useRecoilValue } from 'recoil';
import { progressLineOffsetsState } from '../states';

export const useProgressLineOffsets = () => useRecoilValue(progressLineOffsetsState);
