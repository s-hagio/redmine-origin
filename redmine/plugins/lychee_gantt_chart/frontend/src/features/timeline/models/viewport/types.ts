export type Viewport = {
  width: number;
  height: number;
  offsetTop: number;
  offsetLeft: number;
};
