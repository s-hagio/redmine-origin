import { useMemo } from 'react';
import { FieldContext, useIsEditableField } from '@/models/fieldRule';
import { useIssueRelationCreation } from '../../issueRelation';

export type IssueTaskBarDraggability = {
  isDraggable: boolean;
  isRangeDraggable: boolean;
  isStartDateDraggable: boolean;
  isDueDateDraggable: boolean;
};

export const useIssueTaskBarDraggability = (fieldContext: FieldContext): IssueTaskBarDraggability => {
  const isEditableField = useIsEditableField();
  const issueRelationCreation = useIssueRelationCreation();

  return useMemo(() => {
    const isControllable = !issueRelationCreation;
    const isStartDateDraggable = isControllable && isEditableField(fieldContext, 'startDate');
    const isDueDateDraggable = isControllable && isEditableField(fieldContext, 'dueDate');

    return {
      isDraggable: isStartDateDraggable || isDueDateDraggable,
      isRangeDraggable: isStartDateDraggable && isDueDateDraggable,
      isStartDateDraggable,
      isDueDateDraggable,
    };
  }, [fieldContext, isEditableField, issueRelationCreation]);
};
