import { useRecoilState } from 'recoil';
import { metadataVisibilityState } from '../states';
import { MetadataType } from '../types';

export const useMetadataVisibilityState = (type: MetadataType) => {
  return useRecoilState(metadataVisibilityState(type));
};
