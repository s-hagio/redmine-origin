import { useRecoilValue } from 'recoil';
import { isNeedToCenteringState } from '../states';

export const useIsNeedToCentering = () => useRecoilValue(isNeedToCenteringState);
