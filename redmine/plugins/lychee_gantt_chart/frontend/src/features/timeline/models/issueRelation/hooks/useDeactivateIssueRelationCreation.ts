import { useRecoilCallback } from 'recoil';
import { issueRelationCreationState } from '../states';

export const useDeactivateIssueRelationCreation = () => {
  return useRecoilCallback(({ set }) => {
    return () => {
      set(issueRelationCreationState, null);
    };
  });
};
