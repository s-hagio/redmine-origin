import { atom } from 'recoil';
import { TaskBar, TaskBarResourceIdentifier } from '../types';

export const taskBarMapState = atom<ReadonlyMap<TaskBarResourceIdentifier, TaskBar | null>>({
  key: 'timeline/taskBarMap',
  default: new Map(),
});
