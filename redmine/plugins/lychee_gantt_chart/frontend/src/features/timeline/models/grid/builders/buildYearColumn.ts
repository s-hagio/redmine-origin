import { GridType } from '../types';
import { BuildUnitedColumn } from './types';

export const buildYearColumn: BuildUnitedColumn = (groupedTicks, stepWidth) => {
  const firstTick = groupedTicks[0];

  return {
    id: `${GridType.Year}-${firstTick.getFullYear()}`,
    type: GridType.Year,
    label: `${firstTick.getFullYear()}`,
    width: groupedTicks.length * stepWidth,
  };
};
