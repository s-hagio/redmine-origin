import { useRecoilValue } from 'recoil';
import { ColumnName } from '@/models/query';
import { useIsColumnVisible } from '@/features/dataTable';
import { MetadataType, MetadataVisibility } from '../types';
import { metadataVisibilityState } from '../states';

const columnNameMap: Record<MetadataType, ColumnName> = {
  [MetadataType.Title]: 'subject',
  [MetadataType.Status]: 'status',
  [MetadataType.ProgressRate]: 'done_ratio',
  [MetadataType.AssignedUser]: 'assigned_to',
};

export const useMetadataVisible = (type: MetadataType): boolean => {
  const visibility = useRecoilValue(metadataVisibilityState(type));
  const isColumnVisible = useIsColumnVisible();

  switch (visibility) {
    case MetadataVisibility.Hidden:
      return false;
    case MetadataVisibility.Visible:
      return true;
    case MetadataVisibility.VisibleOnlyWhenColumnIsHidden:
      return !isColumnVisible(columnNameMap[type]);
  }
};
