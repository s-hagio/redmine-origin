import { useRecoilCallback } from 'recoil';
import { selectedTimeRangeState } from '../states';
import { SelectedTimeRange } from '../types';

type Options = {
  unlock?: boolean;
};

export const useSelectTimeRange = () => {
  return useRecoilCallback(({ set }) => {
    return (newValue: SelectedTimeRange | null, options: Options = {}) => {
      set(selectedTimeRangeState, (current) => {
        if (current?.lock && !options.unlock && !newValue) {
          return current;
        }

        return newValue;
      });
    };
  }, []);
};
