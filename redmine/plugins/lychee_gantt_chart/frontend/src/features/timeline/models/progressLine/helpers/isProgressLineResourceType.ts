import { TaskBarResourceType } from '../../taskBar';

const targetResourceTypes: Set<string> = new Set([TaskBarResourceType.Version, TaskBarResourceType.Issue]);

export const isProgressLineResourceType = (resourceType: string): boolean => {
  return targetResourceTypes.has(resourceType);
};
