export type TimelineOptions = {
  showRelationLine: boolean;
  showProgressLine: boolean;
};
