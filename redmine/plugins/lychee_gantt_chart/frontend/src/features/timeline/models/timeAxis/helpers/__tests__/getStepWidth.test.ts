import { getStepWidth } from '../getStepWidth';
import { TimeAxisType } from '../../types';

test('タイプごとのステップ幅を返す', () => {
  expect(getStepWidth(TimeAxisType.MonthSmall)).toBe(1);
  expect(getStepWidth(TimeAxisType.MonthLarge)).toBe(2);
  expect(getStepWidth(TimeAxisType.Week)).toBe(4);
  expect(getStepWidth(TimeAxisType.DaySmall)).toBe(8);
  expect(getStepWidth(TimeAxisType.DayLarge)).toBe(16);
});
