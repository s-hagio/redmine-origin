import { ISODateString } from '@/lib/date';

export type Tick = Date;
export type TickIdentifier = ISODateString;
