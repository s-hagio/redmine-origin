import { selector } from 'recoil';
import { stepWidthState } from '../../timeAxis';
import { ticksState } from './ticksState';

export const totalTickWidthState = selector<number>({
  key: 'timeline/totalTickWidth',
  get: ({ get }) => {
    const ticks = get(ticksState);
    const stepWidth = get(stepWidthState);

    return ticks.length * stepWidth;
  },
});
