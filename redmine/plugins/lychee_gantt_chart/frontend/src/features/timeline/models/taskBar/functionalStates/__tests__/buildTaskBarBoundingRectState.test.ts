import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { getTaskBarRelativeBoundingRect } from '../../helpers';
import { TaskBarResourceIdentifier, TaskBarResourceType } from '../../types';
import { additionalTaskBarBoundingRectState } from '../../states';
import { buildTaskBarBoundingRectState } from '../buildTaskBarBoundingRectState';

type TestCase = {
  resourceType: TaskBarResourceType;
  identifier: TaskBarResourceIdentifier;
};

test.each<TestCase>([
  { resourceType: TaskBarResourceType.Project, identifier: 'project-1' },
  { resourceType: TaskBarResourceType.Version, identifier: 'version-1-2' },
  { resourceType: TaskBarResourceType.Issue, identifier: 'issue-123' },
])(
  '$resourceTypeのRelativeBoundingRectに追加のBoundingRectを適用して返す',
  ({ resourceType, identifier }) => {
    const { result } = renderRecoilHook(() => useRecoilValue(buildTaskBarBoundingRectState(identifier)), {
      initializeState: ({ set }) => {
        set(additionalTaskBarBoundingRectState({ identifier, adderName: 'a' }), { top: 10, height: 50 });
        set(additionalTaskBarBoundingRectState({ identifier, adderName: 'b' }), { top: 21, height: 20 });
      },
    });

    const relativeBounds = getTaskBarRelativeBoundingRect(resourceType);

    expect(result.current()).toEqual({
      top: relativeBounds.top + 10 + 21,
      height: relativeBounds.height + 50 + 20,
    });
  }
);
