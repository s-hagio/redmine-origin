import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { toDate } from '@/lib/date';
import { timeAxisOptionsState, TimeAxisType, TimeDirection } from '../../../timeAxis';
import { gridRowsState } from '../../states';
import { useAddGridColumns } from '../useAddGridColumns';
import { useGridRows } from '../useGridRows';

const baseDayGridRow = {
  width: 16,
  isNonWorkingDay: false,
  isBeforeInvisibleDay: false,
  isAfterInvisibleDay: false,
} as const;

test('Tick配列を元に指定方向へグリッド列を追加する', () => {
  const { result } = renderRecoilHook(
    () => ({
      addGridColumns: useAddGridColumns(),
      gridRows: useGridRows(),
    }),
    {
      initializeState: ({ set }) => {
        set(timeAxisOptionsState, (current) => ({ ...current, type: TimeAxisType.DayLarge }));
        set(gridRowsState, [
          {
            type: 'yearMonth',
            columns: [
              { id: 'yearMonth-2023-2', type: 'yearMonth', label: '2023-2', width: 16 },
              { id: 'yearMonth-2023-3', type: 'yearMonth', label: '2023-3', width: 16 },
            ],
          },
          {
            type: 'day',
            columns: [
              { id: 'day-2023-02-28', type: 'day', label: '28', ...baseDayGridRow },
              { id: 'day-2023-03-01', type: 'day', label: '1', ...baseDayGridRow },
            ],
          },
        ]);
      },
    }
  );

  act(() => {
    result.current.addGridColumns(
      ['2023-01-31', '2023-02-27'].map((dateString) => toDate(dateString)),
      TimeDirection.Backward
    );
    result.current.addGridColumns(
      ['2023-03-02', '2023-03-03'].map((dateString) => toDate(dateString)),
      TimeDirection.Forward
    );
  });

  expect(result.current.gridRows).toStrictEqual([
    {
      type: 'yearMonth',
      columns: [
        { id: 'yearMonth-2023-1', type: 'yearMonth', label: '2023-1', width: 16 },
        { id: 'yearMonth-2023-2', type: 'yearMonth', label: '2023-2', width: 32 },
        { id: 'yearMonth-2023-3', type: 'yearMonth', label: '2023-3', width: 48 },
      ],
    },
    {
      type: 'day',
      columns: [
        { id: 'day-2023-01-31', type: 'day', label: '31', ...baseDayGridRow },
        { id: 'day-2023-02-27', type: 'day', label: '27', ...baseDayGridRow },
        { id: 'day-2023-02-28', type: 'day', label: '28', ...baseDayGridRow },
        { id: 'day-2023-03-01', type: 'day', label: '1', ...baseDayGridRow },
        { id: 'day-2023-03-02', type: 'day', label: '2', ...baseDayGridRow },
        { id: 'day-2023-03-03', type: 'day', label: '3', ...baseDayGridRow },
      ],
    },
  ]);
});
