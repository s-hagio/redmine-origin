import { atomFamily } from 'recoil';
import { TaskBar, TaskBarResourceIdentifier } from '../types';

export const taskBarState = atomFamily<TaskBar | null, TaskBarResourceIdentifier>({
  key: 'timeline/taskBar',
  default: null,
});
