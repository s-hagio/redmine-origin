import { useRecoilValue } from 'recoil';
import { buildTimeBarState } from '../functionalStates';

export const useBuildTimeBar = () => useRecoilValue(buildTimeBarState);
