import { ParseKeys } from 'i18next';

export type GridColumnID = string;

export const GridType = {
  Year: 'year',
  YearMonth: 'yearMonth',
  Month: 'month',
  Week: 'week',
  Day: 'day',
  WeekDay: 'weekDay',
} as const;

export type GridType = (typeof GridType)[keyof typeof GridType];

export type DayGridType = 'day' | 'weekDay';
export type UnitedGridType = Omit<GridType, DayGridType>;

type LabelledColumn =
  | {
      labelPath: ParseKeys;
      label?: undefined;
    }
  | {
      labelPath?: undefined;
      label: string | null;
    };

export type UnitedGridColumn = {
  id: GridColumnID;
  type: UnitedGridType;
  width: number;
} & LabelledColumn;

export type DayGridColumn = {
  id: GridColumnID;
  type: DayGridType;
  width: number;
  isNonWorkingDay: boolean;
  isBeforeInvisibleDay: boolean;
  isAfterInvisibleDay: boolean;
} & LabelledColumn;

export type UnitedGridRow = {
  type: GridType;
  columns: UnitedGridColumn[];
};

export type DayGridRow = {
  type: GridType;
  columns: DayGridColumn[];
};

export type GridRow = UnitedGridRow | DayGridRow;
