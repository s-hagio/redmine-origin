import { useRecoilValue } from 'recoil';
import { TreeBlockRootID } from '../types';
import { treeBlockHighlightState } from '../states';

export const useTreeBlockHighlight = (id: TreeBlockRootID) => {
  return useRecoilValue(treeBlockHighlightState(id));
};
