import { selector } from 'recoil';
import { DateArgument } from '@/lib/date';
import { isNonWorkingDayState } from '@/models/workingDay';
import { TimeAxisType } from '../types';
import { timeAxisOptionsState } from '../states';

export type IsVisibleDay = (arg: DateArgument) => boolean;

export const isVisibleDayState = selector<IsVisibleDay>({
  key: 'timeline/isVisibleDay',
  get: ({ get }) => {
    const isNonWorkingDay = get(isNonWorkingDayState);
    const { type, includeNonWorkingDays } = get(timeAxisOptionsState);

    return (arg) => {
      return !isDayType(type) || includeNonWorkingDays || !isNonWorkingDay(arg);
    };
  },
});

const isDayType = (type: TimeAxisType) => {
  return type === TimeAxisType.DayLarge || type === TimeAxisType.DaySmall;
};
