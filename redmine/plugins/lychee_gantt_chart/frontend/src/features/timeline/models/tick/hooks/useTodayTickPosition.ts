import { useRecoilValue } from 'recoil';
import { todayTickPositionState } from '../states';

export const useTodayTickPosition = () => useRecoilValue(todayTickPositionState);
