import { useRecoilValue } from 'recoil';
import { selectedTimeRangeState } from '../states';

export const useSelectedTimeRange = () => useRecoilValue(selectedTimeRangeState);
