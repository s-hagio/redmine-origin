import { CoreResourceType } from '@/models/coreResource';
import { isTaskBarResourceType } from '../isTaskBarResourceType';
import { TaskBarResourceType } from '../../types';

test('TaskBarResourceのTypeならtrue', () => {
  expect(isTaskBarResourceType(TaskBarResourceType.Project)).toBe(true);
  expect(isTaskBarResourceType(TaskBarResourceType.Version)).toBe(true);
  expect(isTaskBarResourceType(TaskBarResourceType.Issue)).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isTaskBarResourceType(CoreResourceType.Group)).toBe(false);
  expect(isTaskBarResourceType('other')).toBe(false);
});
