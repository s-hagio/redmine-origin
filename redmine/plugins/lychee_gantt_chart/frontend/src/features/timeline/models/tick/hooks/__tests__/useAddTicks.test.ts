import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { formatISODate, toDate } from '@/lib/date';
import { settingsState } from '@/models/setting';
import { useAddTicks } from '../useAddTicks';
import { timeAxisOptionsState, TimeDirection } from '../../../timeAxis';
import { ticksState } from '../../states';

const initialTickDays = ['2023-03-01', '2023-03-02', '2023-03-03'] as const;
const nonWorkingDays = ['2023-02-27', '2023-03-05'] as const;

test('指定した方向にTickを追加して、追加したTickの配列を返す', () => {
  const { result } = renderRecoilHook(
    () => ({
      addTicks: useAddTicks(),
      ticks: useRecoilValue(ticksState),
    }),
    {
      initializeState: ({ set }) => {
        set(
          ticksState,
          initialTickDays.map((dateString) => toDate(dateString))
        );
        set(settingsState, (current) => ({ ...current, nonWorkingDays: [...nonWorkingDays] }));
        set(timeAxisOptionsState, (current) => ({ ...current, includeNonWorkingDays: false }));
      },
    }
  );

  const expectedTicksToBackward = ['2023-02-25', '2023-02-26', '2023-02-28'] as const;
  const expectedTicksToForward = ['2023-03-04', '2023-03-06'] as const;

  act(() => {
    const ticksAddedBackward = result.current.addTicks(3, TimeDirection.Backward);
    expect(ticksAddedBackward.map(formatISODate)).toStrictEqual(expectedTicksToBackward);

    const ticksAddedForward = result.current.addTicks(2, TimeDirection.Forward);
    expect(ticksAddedForward.map(formatISODate)).toStrictEqual(expectedTicksToForward);
  });

  expect(result.current.ticks.map(formatISODate)).toStrictEqual([
    ...expectedTicksToBackward,
    ...initialTickDays,
    ...expectedTicksToForward,
  ]);
});
