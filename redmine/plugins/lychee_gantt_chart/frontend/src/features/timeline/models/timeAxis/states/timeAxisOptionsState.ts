import { atom } from 'recoil';
import { persistJSON, restorePersistedJSON } from '@/utils/storage';
import { TimeAxisOptions } from '../types';
import { DEFAULT_OPTIONS, STORED_OPTIONS_KEY } from '../constants';

export const timeAxisOptionsState = atom<TimeAxisOptions>({
  key: 'timeline/timeAxisOptions',
  default: DEFAULT_OPTIONS,
  effects: [
    ({ setSelf, onSet }) => {
      setSelf((current) => ({
        ...DEFAULT_OPTIONS,
        ...current,
        ...restorePersistedJSON(STORED_OPTIONS_KEY),
      }));

      onSet((options) => {
        persistJSON(STORED_OPTIONS_KEY, options);
      });
    },
  ],
});
