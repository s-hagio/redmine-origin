import { selector } from 'recoil';
import { ensureVisibleDayState, TimeDirection } from '../../timeAxis';
import { positionOfState } from '../index';

export const todayTickPositionState = selector<number>({
  key: 'timeline/todayTickPosition',
  get: ({ get }) => {
    const ensureVisibleDay = get(ensureVisibleDayState);
    const positionOf = get(positionOfState);

    const date = ensureVisibleDay(new Date(), TimeDirection.Backward);

    return positionOf(date);
  },
});
