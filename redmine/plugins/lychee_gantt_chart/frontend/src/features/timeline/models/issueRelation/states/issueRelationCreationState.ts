import { atom } from 'recoil';
import { IssueRelationCreation } from '../types';

export const issueRelationCreationState = atom<IssueRelationCreation | null>({
  key: 'issueRelationCreation',
  default: null,
});
