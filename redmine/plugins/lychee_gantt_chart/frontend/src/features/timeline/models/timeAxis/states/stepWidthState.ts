import { selector } from 'recoil';
import { getStepWidth } from '../helpers';
import { timeAxisOptionsState } from './timeAxisOptionsState';

export const stepWidthState = selector<number>({
  key: 'timeline/stepWidth',
  get: ({ get }) => {
    const { type } = get(timeAxisOptionsState);

    return getStepWidth(type);
  },
});
