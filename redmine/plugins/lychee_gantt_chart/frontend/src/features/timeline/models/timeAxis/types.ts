import { ISODateString } from '@/lib/date';

export const TimeAxisType = {
  MonthSmall: 'monthSmall',
  MonthLarge: 'monthLarge',
  Week: 'week',
  DaySmall: 'daySmall',
  DayLarge: 'dayLarge',
} as const;

export type TimeAxisType = (typeof TimeAxisType)[keyof typeof TimeAxisType];

export type TimeAxisOptions = {
  type: TimeAxisType;
  date: ISODateString | null;
  includeNonWorkingDays: boolean;
};

export const TimeDirection = { Backward: -1, Forward: 1 };
export type TimeDirection = (typeof TimeDirection)[keyof typeof TimeDirection];
