import { useRecoilCallback, useRecoilValue } from 'recoil';
import { TimeDirection } from '../../timeAxis';
import { ticksState } from '../states';
import { Tick } from '../types';
import { useBuildTicks } from './useBuildTicks';

type AddTicks = (sizePerDirection: number, direction: TimeDirection) => Tick[];

export const useAddTicks = (): AddTicks => {
  const ticks = useRecoilValue(ticksState);
  const buildTicks = useBuildTicks();

  return useRecoilCallback(
    ({ set }) => {
      return (sizePerDirection, direction) => {
        const date = (direction === TimeDirection.Forward ? ticks.at(-1) : ticks[0]) as Tick;

        if (!date) {
          return [];
        }

        const newTicks = buildTicks(date, {
          sizePerDirection,
          directions: [direction],
          includeBaseDate: false,
        });

        set(ticksState, (currentTicks) => {
          if (direction === TimeDirection.Forward) {
            return [...currentTicks, ...newTicks];
          }
          return [...newTicks, ...currentTicks];
        });

        return newTicks;
      };
    },
    [buildTicks, ticks]
  );
};
