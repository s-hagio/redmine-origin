import { selectorFamily } from 'recoil';
import { rowBoundingRectState } from '@/features/row';
import { TaskBarResourceIdentifier, VerticalPosition } from '../types';
import { taskBarState } from './taskBarState';

export const absoluteTaskBarVerticalPositionState = selectorFamily<
  VerticalPosition,
  TaskBarResourceIdentifier
>({
  key: 'timeline/absoluteTaskBarVerticalPosition',
  get: (identifier) => {
    return ({ get }) => {
      const rowBounds = get(rowBoundingRectState(identifier));
      const taskBar = get(taskBarState(identifier));

      if (!rowBounds || !taskBar) {
        return { top: 0, middle: 0, bottom: 0 };
      }

      const top = rowBounds.top + taskBar.top;

      return {
        top,
        middle: top + taskBar.height / 2,
        bottom: top + taskBar.height,
      };
    };
  },
});
