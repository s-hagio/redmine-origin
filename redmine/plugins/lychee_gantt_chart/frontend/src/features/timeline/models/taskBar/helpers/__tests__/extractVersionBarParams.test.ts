import { Version } from '@/models/version';
import { versionSharedByTree } from '@/__fixtures__';
import { TaskBarResourceType } from '../../types';
import { extractVersionBarParams } from '../extractVersionBarParams';

const version: Version = {
  ...versionSharedByTree,
  startDate: '2022-01-01',
  dueDate: '2022-01-05',
  completedPercent: 25,
};

test('VersionからTaskBarParamsを抽出する', () => {
  expect(extractVersionBarParams(version)).toStrictEqual({
    resourceType: TaskBarResourceType.Version,
    startDate: version.startDate,
    endDate: version.dueDate,
    progressRate: version.completedPercent,
  });
});

test('TaskBarParamsを持たない場合はnullを返す', () => {
  expect(extractVersionBarParams({ ...version, startDate: null })).toBeNull();
  expect(extractVersionBarParams({ ...version, dueDate: null })).toBeNull();
  expect(extractVersionBarParams({ ...version, startDate: null, dueDate: null })).toBeNull();
});
