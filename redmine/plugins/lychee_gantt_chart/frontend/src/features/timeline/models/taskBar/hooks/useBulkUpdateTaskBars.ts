import { useRecoilCallback } from 'recoil';
import {
  getResourceIdFromIdentifier,
  isIssueResourceIdentifier,
  IssueResourceIdentifier,
} from '@/models/coreResource';
import { issueEntityState } from '@/models/issue';
import { useIsEditableField } from '@/models/fieldRule';
import { selectedRowIdsState } from '@/features/row';
import { MovedRangeValues } from '../types';
import { extractIssueBarParams } from '../helpers';
import { taskBarState } from '../states';
import { buildTaskBarState } from '../functionalStates';
import { useComputeMovedRange } from './useComputeMovedRange';

type UpdateSelectedTaskBars = (movedValues: MovedRangeValues) => void;

export const useBulkUpdateTaskBars = (
  draggingTaskBarIdentifier: IssueResourceIdentifier
): UpdateSelectedTaskBars => {
  const computeMovedRange = useComputeMovedRange();
  const isEditableField = useIsEditableField();

  return useRecoilCallback(
    ({ transact_UNSTABLE, snapshot }) => {
      return (movedValues) => {
        const selectedIssueRowIds = snapshot.getLoadable(selectedRowIdsState).getValue();
        const targetIdentifiers = selectedIssueRowIds.includes(draggingTaskBarIdentifier)
          ? selectedIssueRowIds.filter(isIssueResourceIdentifier)
          : [draggingTaskBarIdentifier];

        transact_UNSTABLE(({ get, set }) => {
          targetIdentifiers.forEach((identifier) => {
            const issue = get(issueEntityState(getResourceIdFromIdentifier(identifier)));
            const baseParams = extractIssueBarParams(issue);

            if (!issue || !baseParams) {
              return;
            }

            const params = { ...baseParams };
            const newRange = computeMovedRange(baseParams, movedValues);
            const buildTaskBar = snapshot.getLoadable(buildTaskBarState(identifier)).getValue();

            if (isEditableField(issue, 'startDate')) {
              params.startDate = newRange.startDate;
            }

            if (isEditableField(issue, 'dueDate')) {
              params.endDate = newRange.endDate;
            }

            if (params.startDate !== baseParams.startDate || params.endDate !== baseParams.endDate) {
              set(taskBarState(identifier), buildTaskBar(params));
            }
          });
        });
      };
    },
    [computeMovedRange, isEditableField, draggingTaskBarIdentifier]
  );
};
