import { TaskBarResourceIdentifier } from '../types';
import { isTaskBarResourceType } from './isTaskBarResourceType';

export const isTaskBarResourceIdentifier = (identifier: string): identifier is TaskBarResourceIdentifier => {
  const [resourceType] = identifier.split('-');

  return isTaskBarResourceType(resourceType);
};
