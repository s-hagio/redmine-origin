import { Project } from '@/models/project';
import { TaskBarParams, TaskBarResourceType } from '../types';

type ProjectTaskBarResource = Pick<Project, 'startDate' | 'dueDate' | 'completedPercent'>;

export const extractProjectBarParams = (project: ProjectTaskBarResource | null): TaskBarParams | null => {
  if (!project || !project.startDate || !project.dueDate) {
    return null;
  }

  return {
    resourceType: TaskBarResourceType.Project,
    startDate: project.startDate,
    endDate: project.dueDate,
    progressRate: project.completedPercent ?? 0,
  };
};
