import { useRecoilValue } from 'recoil';
import { IssueID } from '@/models/issue';
import { issueRelationCreationStatusState } from '../states';

export const useIssueRelationCreationStatus = (issueId: IssueID) => {
  return useRecoilValue(issueRelationCreationStatusState(issueId));
};
