import { selector } from 'recoil';
import { sortBy } from '@/utils/array';
import { addDays, DateArgument, isSameOrBeforeDay, toDate } from '@/lib/date';
import { isVisibleDayState } from './isVisibleDayState';

export type CalcVisibleDays = (fromDateArg: DateArgument, toDateArg: DateArgument) => number;

export const calcVisibleDaysState = selector<CalcVisibleDays>({
  key: 'timeline/calcVisibleDays',
  get: ({ get }) => {
    const isVisibleDay = get(isVisibleDayState);

    return (fromDateArg, toDateArg) => {
      const [start, end] = sortBy([toDate(fromDateArg), toDate(toDateArg)], (date) => date.getTime());

      let current = start;
      let days = 0;

      while (isSameOrBeforeDay(current, end)) {
        if (isVisibleDay(current)) {
          ++days;
        }

        current = addDays(current, 1);
      }

      return days;
    };
  },
});
