import { atomFamily } from 'recoil';
import { persistJSON, restorePersistedJSON } from '@/utils/storage';
import { MetadataType, MetadataVisibility } from '../types';
import { DEFAULT_VISIBILITY_COLLECTION, METADATA_VISIBILITY_STORED_KEY } from '../constants';

export const metadataVisibilityState = atomFamily<MetadataVisibility, MetadataType>({
  key: 'timeline/metadataVisibility',
  default: (type) => {
    const defaults = {
      ...DEFAULT_VISIBILITY_COLLECTION,
      ...restorePersistedJSON(METADATA_VISIBILITY_STORED_KEY, {}),
    };

    return defaults[type];
  },
  effects: (type) => [
    ({ onSet }) => {
      onSet((newValue) => {
        persistJSON(METADATA_VISIBILITY_STORED_KEY, {
          ...restorePersistedJSON(METADATA_VISIBILITY_STORED_KEY, {}),
          [type]: newValue,
        });
      });
    },
  ],
});
