import { selector } from 'recoil';
import { DateArgument } from '@/lib/date';
import { ensureVisibleDayState, stepWidthState, TimeDirection } from '../../timeAxis';
import { indexOfState } from './indexOfState';

type Options = {
  directionForEnsure?: TimeDirection;
};

type PositionOf = (arg: DateArgument, options?: Options) => number;

export const positionOfState = selector<PositionOf>({
  key: 'timeline/positionOf',
  get: ({ get }) => {
    const ensureVisibleDay = get(ensureVisibleDayState);
    const indexOf = get(indexOfState);
    const stepWidth = get(stepWidthState);

    return (arg, options = {}) => {
      const date = ensureVisibleDay(arg, options.directionForEnsure ?? TimeDirection.Forward);

      return indexOf(date) * stepWidth;
    };
  },
});
