import { useRecoilValue } from 'recoil';
import { visibleIssueRelationsState } from '../states';

export const useVisibleIssueRelations = () => useRecoilValue(visibleIssueRelationsState);
