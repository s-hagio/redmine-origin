import { useRecoilValue } from 'recoil';
import { visibleIssueTreeRootRowIdsState } from '@/features/row';

export const useVisibleTreeBlockIds = () => useRecoilValue(visibleIssueTreeRootRowIdsState);
