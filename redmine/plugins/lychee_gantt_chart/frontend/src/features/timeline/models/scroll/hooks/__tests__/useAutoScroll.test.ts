import { useRecoilValue } from 'recoil';
import { beforeEach } from 'vitest';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { useAutoScroll } from '../useAutoScroll';
import { TimeDirection } from '../../../timeAxis';
import { scrollLeftState } from '../../states';

const requestAnimationFrameMock = vi.fn((cb) => window.setTimeout(cb, 1));
const cancelAnimationFrameMock = vi.fn((id) => window.clearTimeout(id));

beforeEach(() => {
  vi.useFakeTimers();
  vi.spyOn(window, 'requestAnimationFrame').mockImplementation(requestAnimationFrameMock);
  vi.spyOn(window, 'cancelAnimationFrame').mockImplementation(cancelAnimationFrameMock);
});

beforeEach(() => {
  requestAnimationFrameMock.mockRestore();
  cancelAnimationFrameMock.mockRestore();
});

test('AutoScrollの開始／停止ヘルパーをMemo化して返す', () => {
  const { result, rerender } = renderRecoilHook(() => useAutoScroll(TimeDirection.Backward));
  const previous = result.current;

  rerender();

  expect(result.current).toBe(previous);
});

test('startで開始したAutoScrollをstopで停止できる', () => {
  const { result } = renderRecoilHook(() => ({
    autoScroll: useAutoScroll(TimeDirection.Backward),
    scrollLeft: useRecoilValue(scrollLeftState),
  }));

  act(() => vi.advanceTimersToNextTimer());
  expect(result.current.scrollLeft).toBe(0);

  result.current.autoScroll.start();

  expect(result.current.scrollLeft).toBe(0);

  act(() => vi.advanceTimersToNextTimer());
  expect(result.current.scrollLeft).toBe(-10);

  act(() => vi.advanceTimersToNextTimer());
  expect(result.current.scrollLeft).toBe(-20);

  result.current.autoScroll.stop();

  act(() => vi.advanceTimersToNextTimer());
  expect(result.current.scrollLeft).toBe(-20);
});

test('Forwardの場合は正の値でスクロールする', () => {
  const { result } = renderRecoilHook(() => ({
    autoScroll: useAutoScroll(TimeDirection.Forward),
    scrollLeft: useRecoilValue(scrollLeftState),
  }));

  result.current.autoScroll.start();

  act(() => vi.advanceTimersToNextTimer());
  expect(result.current.scrollLeft).toBe(10);

  act(() => vi.advanceTimersToNextTimer());
  expect(result.current.scrollLeft).toBe(20);
});
