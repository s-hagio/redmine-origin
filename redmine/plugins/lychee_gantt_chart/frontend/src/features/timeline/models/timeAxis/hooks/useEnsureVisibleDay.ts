import { useRecoilValue } from 'recoil';
import { ensureVisibleDayState } from '../functionalStates';

export const useEnsureVisibleDay = () => useRecoilValue(ensureVisibleDayState);
