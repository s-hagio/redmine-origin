import { renderRecoilHook } from '@/test-utils';
import { formatISODate } from '@/lib/date';
import { settingsState } from '@/models/setting';
import { useBuildTicks } from '../useBuildTicks';
import { timeAxisOptionsState, TimeDirection } from '../../../timeAxis';

const baseDate = new Date(2022, 7, 10);

test('基準日＋前後に指定サイズ分のTickを構築', () => {
  const { result } = renderRecoilHook(() => useBuildTicks());

  expect(result.current(baseDate, { sizePerDirection: 4 }).map(formatISODate)).toEqual([
    '2022-08-06',
    '2022-08-07',
    '2022-08-08',
    '2022-08-09',
    '2022-08-10',
    '2022-08-11',
    '2022-08-12',
    '2022-08-13',
    '2022-08-14',
  ]);
});

test('directionsで指定した方向のみTickを構築', () => {
  const { result } = renderRecoilHook(() => useBuildTicks());

  const options = {
    directions: [TimeDirection.Forward],
    sizePerDirection: 5,
  };

  expect(result.current(baseDate, options).map(formatISODate)).toEqual([
    '2022-08-10',
    '2022-08-11',
    '2022-08-12',
    '2022-08-13',
    '2022-08-14',
    '2022-08-15',
  ]);
});

test('基準日を含めて構築するかどうかを指定可能', () => {
  const { result } = renderRecoilHook(() => useBuildTicks());

  const options = {
    sizePerDirection: 1,
    includeBaseDate: false,
  };

  expect(result.current(baseDate, options).map(formatISODate)).toEqual(['2022-08-09', '2022-08-11']);
});

test('不可視日をスキップして構築', () => {
  const { result } = renderRecoilHook(
    () => ({
      build: useBuildTicks(),
    }),
    {
      initializeState: ({ set }) => {
        set(settingsState, (current) => ({
          ...current,
          nonWorkingDays: ['2022-08-08', '2022-08-10', '2022-08-12'],
        }));

        set(timeAxisOptionsState, (current) => ({
          ...current,
          includeNonWorkingDays: false,
        }));
      },
    }
  );

  expect(result.current.build(baseDate, { sizePerDirection: 4 }).map(formatISODate)).toEqual([
    '2022-08-04',
    '2022-08-05',
    '2022-08-06',
    '2022-08-07',
    '2022-08-09',
    '2022-08-11',
    '2022-08-13',
    '2022-08-14',
    '2022-08-15',
  ]);
});
