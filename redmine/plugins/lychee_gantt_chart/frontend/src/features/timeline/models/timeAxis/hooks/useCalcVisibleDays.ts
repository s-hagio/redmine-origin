import { useRecoilValue } from 'recoil';
import { calcVisibleDaysState } from '../functionalStates';

export const useCalcVisibleDays = () => useRecoilValue(calcVisibleDaysState);
