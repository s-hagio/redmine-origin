import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { issueRelationCreationState } from '../../states';
import { useDeactivateIssueRelationCreation } from '../useDeactivateIssueRelationCreation';
import { useIssueRelationCreation } from '../useIssueRelationCreation';

test('Deactivate', () => {
  const { result } = renderRecoilHook(
    () => ({
      deactivate: useDeactivateIssueRelationCreation(),
      issueRelationCreation: useIssueRelationCreation(),
    }),
    {
      initializeState: ({ set }) => {
        set(issueRelationCreationState, {
          type: 'to',
          sourceIssueId: 123,
          isLocked: false,
        });
      },
    }
  );

  act(() => result.current.deactivate());

  expect(result.current.issueRelationCreation).toBeNull();
});
