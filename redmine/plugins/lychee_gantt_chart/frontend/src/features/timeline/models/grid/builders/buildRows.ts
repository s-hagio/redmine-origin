import { groupBy } from '@/utils/array';
import { getYear, formatYearMonth, getISODay, formatISODate } from '@/lib/date';
import { TimeAxisType } from '../../timeAxis';
import { Tick } from '../../tick';
import { GridType, GridRow } from '../types';
import {
  BuildUnitedRow,
  BuildUnitedColumn,
  BuildDayRow,
  BuildDayColumn,
  BuildRow,
  BuildHelpers,
} from './types';
import { buildYearColumn } from './buildYearColumn';
import { buildYearMonthColumn } from './buildYearMonthColumn';
import { buildMonthColumn } from './buildMonthColumn';
import { buildWeekColumn } from './buildWeekColumn';
import { buildDayColumn } from './buildDayColumn';
import { buildWeekDayColumn } from './buildWeekDayColumn';

export const buildRows = (
  ticks: Tick[],
  timeAxisType: TimeAxisType,
  stepWidth: number,
  helpers: BuildHelpers
): GridRow[] => {
  return gridRowTypesByTimeAxisType[timeAxisType].map((gridType) => {
    const buildRow = buildersByGridType[gridType];

    return buildRow(ticks, stepWidth, helpers);
  });
};

const createBuildUnitedRow = (
  type: GridType,
  groupValueResolver: (tick: Tick) => string | number,
  buildColumn: BuildUnitedColumn
): BuildUnitedRow => {
  return (ticks, ...args) => {
    const ticksByGroupValue = groupBy(ticks, groupValueResolver);

    return {
      type,
      columns: Object.keys(ticksByGroupValue).map((groupValue) =>
        buildColumn(ticksByGroupValue[groupValue], ...args)
      ),
    };
  };
};

const createBuildDayRow = (type: GridType, buildColumn: BuildDayColumn): BuildDayRow => {
  return (ticks, ...args) => ({
    type,
    columns: ticks.map((tick) => buildColumn(tick, ...args)),
  });
};

const gridRowTypesByTimeAxisType: Record<TimeAxisType, GridType[]> = {
  [TimeAxisType.MonthSmall]: [GridType.Year, GridType.Month],
  [TimeAxisType.MonthLarge]: [GridType.Year, GridType.Month],
  [TimeAxisType.Week]: [GridType.YearMonth, GridType.Week],
  [TimeAxisType.DaySmall]: [GridType.YearMonth, GridType.Week, GridType.WeekDay],
  [TimeAxisType.DayLarge]: [GridType.YearMonth, GridType.Day],
};

const getWeekId = (date: Date) => {
  const monday = new Date(date);
  monday.setDate(monday.getDate() - getISODay(date) + 1);
  return formatISODate(monday);
};

const buildersByGridType: Record<GridType, BuildRow> = {
  [GridType.Year]: createBuildUnitedRow(GridType.Year, getYear, buildYearColumn),
  [GridType.YearMonth]: createBuildUnitedRow(GridType.YearMonth, formatYearMonth, buildYearMonthColumn),
  [GridType.Month]: createBuildUnitedRow(GridType.Month, formatYearMonth, buildMonthColumn),
  [GridType.Week]: createBuildUnitedRow(GridType.Week, getWeekId, buildWeekColumn),
  [GridType.Day]: createBuildDayRow(GridType.Day, buildDayColumn),
  [GridType.WeekDay]: createBuildDayRow(GridType.WeekDay, buildWeekDayColumn),
};
