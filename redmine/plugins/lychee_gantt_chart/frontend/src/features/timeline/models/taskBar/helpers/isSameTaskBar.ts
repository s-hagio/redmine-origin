import { TaskBar } from '../types';

export const isSameTaskBar = (aTaskBar: TaskBar | null, bTaskBar: TaskBar | null): boolean => {
  if (aTaskBar === null && bTaskBar === null) {
    return true;
  }

  if (aTaskBar === null || bTaskBar === null) {
    return false;
  }

  return (
    aTaskBar.resourceType === bTaskBar.resourceType &&
    aTaskBar.top === bTaskBar.top &&
    aTaskBar.right === bTaskBar.right &&
    aTaskBar.left === bTaskBar.left &&
    aTaskBar.width === bTaskBar.width &&
    aTaskBar.doneWidth === bTaskBar.doneWidth &&
    aTaskBar.lateWidth === bTaskBar.lateWidth &&
    aTaskBar.height === bTaskBar.height
  );
};
