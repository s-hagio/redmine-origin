import { toDate } from '@/lib/date';
import { buildRows } from '../buildRows';
import { TimeAxisType } from '../../../timeAxis';
import { buildMonthColumn } from '../buildMonthColumn';
import { buildYearColumn } from '../buildYearColumn';
import { buildYearMonthColumn } from '../buildYearMonthColumn';
import { buildWeekColumn } from '../buildWeekColumn';
import { buildWeekDayColumn } from '../buildWeekDayColumn';
import { buildDayColumn } from '../buildDayColumn';
import { helpers } from './__fixtures__';

const ticks = [
  toDate('2021-12-30'),
  toDate('2021-12-31'),
  toDate('2022-01-01'),
  toDate('2022-01-02'),
  toDate('2022-01-03'),
  toDate('2022-01-04'),
  toDate('2022-01-05'),
  toDate('2022-01-06'),
  toDate('2022-01-07'),
  toDate('2022-01-08'),
  toDate('2022-01-09'),
];

const stepWidth = 1;

test('月別（小）のGridRowの配列を構築', () => {
  expect(buildRows(ticks, TimeAxisType.MonthSmall, stepWidth, helpers)).toStrictEqual([
    {
      type: 'year',
      columns: [
        buildYearColumn(ticks.slice(0, 2), stepWidth, helpers),
        buildYearColumn(ticks.slice(2), stepWidth, helpers),
      ],
    },
    {
      type: 'month',
      columns: [
        buildMonthColumn(ticks.slice(0, 2), stepWidth, helpers),
        buildMonthColumn(ticks.slice(2), stepWidth, helpers),
      ],
    },
  ]);
});

test('月別（大）のGridRowの配列を構築', () => {
  expect(buildRows(ticks, TimeAxisType.MonthLarge, stepWidth, helpers)).toStrictEqual([
    {
      type: 'year',
      columns: [
        buildYearColumn(ticks.slice(0, 2), stepWidth, helpers),
        buildYearColumn(ticks.slice(2), stepWidth, helpers),
      ],
    },
    {
      type: 'month',
      columns: [
        buildMonthColumn(ticks.slice(0, 2), stepWidth, helpers),
        buildMonthColumn(ticks.slice(2), stepWidth, helpers),
      ],
    },
  ]);
});

test('週別のGridRowの配列を構築', () => {
  expect(buildRows(ticks, TimeAxisType.Week, stepWidth, helpers)).toStrictEqual([
    {
      type: 'yearMonth',
      columns: [
        buildYearMonthColumn(ticks.slice(0, 2), stepWidth, helpers),
        buildYearMonthColumn(ticks.slice(2), stepWidth, helpers),
      ],
    },
    {
      type: 'week',
      columns: [
        buildWeekColumn(ticks.slice(0, 4), stepWidth, helpers),
        buildWeekColumn(ticks.slice(4), stepWidth, helpers),
      ],
    },
  ]);
});

test('日別（小）のGridRowの配列を構築', () => {
  expect(buildRows(ticks, TimeAxisType.DaySmall, stepWidth, helpers)).toStrictEqual([
    {
      type: 'yearMonth',
      columns: [
        buildYearMonthColumn(ticks.slice(0, 2), stepWidth, helpers),
        buildYearMonthColumn(ticks.slice(2), stepWidth, helpers),
      ],
    },
    {
      type: 'week',
      columns: [
        buildWeekColumn(ticks.slice(0, 4), stepWidth, helpers),
        buildWeekColumn(ticks.slice(4), stepWidth, helpers),
      ],
    },
    {
      type: 'weekDay',
      columns: ticks.map((tick) => buildWeekDayColumn(tick, stepWidth, helpers)),
    },
  ]);
});

test('日別（大）のGridRowの配列を構築', () => {
  expect(buildRows(ticks, TimeAxisType.DayLarge, stepWidth, helpers)).toStrictEqual([
    {
      type: 'yearMonth',
      columns: [
        buildYearMonthColumn(ticks.slice(0, 2), stepWidth, helpers),
        buildYearMonthColumn(ticks.slice(2), stepWidth, helpers),
      ],
    },
    {
      type: 'day',
      columns: ticks.map((tick) => buildDayColumn(tick, stepWidth, helpers)),
    },
  ]);
});
