import { atom } from 'recoil';

export const scrollLeftState = atom<number>({
  key: 'timeline/scrollLeft',
  default: 0,
});
