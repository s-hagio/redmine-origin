import { selector } from 'recoil';
import { formatISODate } from '@/lib/date';
import { TickIdentifier } from '../types';
import { ticksState } from './ticksState';

export const tickIdentifiersState = selector<TickIdentifier[]>({
  key: 'timeline/tickIdentifiers',
  get: ({ get }) => {
    const ticks = get(ticksState);

    return ticks.map(formatISODate);
  },
});
