import { renderRecoilHook } from '@/test-utils';
import { issueEntityState } from '@/models/issue';
import { leafIssue } from '@/__fixtures__';
import { useComputeIssueRelationDelay } from '../useComputeIssueRelationDelay';

const useIssueMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/issue', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIssue: useIssueMock,
}));

const calcWorkingDaysMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/workingDay', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useCalcWorkingDays: vi.fn().mockReturnValue(calcWorkingDaysMock),
}));

const issueRelation = { issueFromId: 1, issueToId: 2 };
const baseIssue = { ...leafIssue };

// ↓稼働日の計算結果自体をMockするのでいつでも良い
const getIssueFromDueDate = vi.fn().mockReturnValue('2021-01-01');
const getIssueToStartDate = vi.fn().mockReturnValue('2021-01-02');

const renderForTesting = () => {
  return renderRecoilHook(() => useComputeIssueRelationDelay(), {
    initializeState: ({ set }) => {
      set(issueEntityState(issueRelation.issueFromId), {
        ...baseIssue,
        id: issueRelation.issueFromId,
        dueDate: getIssueFromDueDate(),
      });

      set(issueEntityState(issueRelation.issueToId), {
        ...baseIssue,
        id: issueRelation.issueToId,
        startDate: getIssueToStartDate(),
      });
    },
  });
};

test('IssueRelationのチケット間の遅延日数を「それぞれの当日」を除いて計算', () => {
  calcWorkingDaysMock.mockReturnValue(10);

  const { result } = renderForTesting();

  expect(result.current(issueRelation), '遅延日数 - 2日分').toBe(8);
  expect(calcWorkingDaysMock).toBeCalledWith(getIssueFromDueDate(), getIssueToStartDate());
});

test('日数が負の値の場合はそのまま返す', () => {
  calcWorkingDaysMock.mockReturnValue(-3);

  const { result } = renderForTesting();

  expect(result.current(issueRelation)).toBe(-3);
});

test('issueFrom.dueDateがない場合は0', () => {
  getIssueFromDueDate.mockReturnValue(null);

  const { result } = renderForTesting();

  expect(result.current(issueRelation)).toBe(0);
});

test('issueTo.startDateがない場合は0', () => {
  getIssueToStartDate.mockReturnValue(null);

  const { result } = renderForTesting();

  expect(result.current(issueRelation)).toBe(0);
});
