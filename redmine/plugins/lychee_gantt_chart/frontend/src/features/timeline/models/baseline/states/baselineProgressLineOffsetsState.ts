import { selector } from 'recoil';
import { selectedBaselineSummaryState } from '@/models/baseline';
import { Row, rowBoundingRectState, visibleRowsState } from '@/features/row';
import { stepWidthState } from '../../timeAxis';
import { positionOfState } from '../../tick';
import { buildProgressLineOffset, isProgressLineResourceType, ProgressLineOffset } from '../../progressLine';
import { timelineOptionsState } from '../../options';
import { isBaselineSnapshotResourceRow } from '../helpers';
import { baselineBarMapState } from './baselineBarMapState';

export const baselineProgressLineOffsetsState = selector<ProgressLineOffset[]>({
  key: 'timeline/baselineProgressLineOffsets',
  get: ({ get }) => {
    const { showProgressLine } = get(timelineOptionsState);

    if (!showProgressLine) {
      return [];
    }

    const baselineSummary = get(selectedBaselineSummaryState);

    if (!baselineSummary) {
      return [];
    }

    const visibleRows = get(visibleRowsState);
    const positionOf = get(positionOfState);
    const stepWidth = get(stepWidthState);
    const baselineBarMap = get(baselineBarMapState);

    const todayLinePosition = positionOf(baselineSummary.snappedAt) + stepWidth;

    if (Number.isNaN(todayLinePosition)) {
      return [];
    }

    const buildProgressLineOffsetByRow = (row: Row): ProgressLineOffset | null => {
      if (!isProgressLineResourceType(row.resourceType) || !isBaselineSnapshotResourceRow(row)) {
        return null;
      }

      const rowBounds = get(rowBoundingRectState(row.id));

      if (!rowBounds) {
        return null;
      }

      const defaults = {
        top: rowBounds.top + rowBounds.height / 2,
        left: todayLinePosition,
      };

      const baselineBar = baselineBarMap.get(row.id);

      if (!baselineBar) {
        return defaults;
      }

      return buildProgressLineOffset(
        baselineBar,
        rowBounds,
        todayLinePosition,
        baselineBar.width === baselineBar.doneWidth
      );
    };

    return visibleRows.reduce<ProgressLineOffset[]>(
      (offsets, row) => {
        const offset = buildProgressLineOffsetByRow(row);

        if (offset) {
          offsets.push(offset);
        }

        return offsets;
      },
      [{ top: 0, left: todayLinePosition }]
    );
  },
});
