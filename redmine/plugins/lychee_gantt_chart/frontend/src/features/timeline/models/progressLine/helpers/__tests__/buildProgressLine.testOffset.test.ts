import { RowBoundingRect } from '@/features/row';
import { buildProgressLineOffset } from '../buildProgressLineOffset';
import { TaskBar, TaskBarResourceType } from '../../../taskBar';

const taskBar: TaskBar = {
  resourceType: TaskBarResourceType.Issue,
  top: 11,
  right: 133,
  left: 33,
  width: 100,
  height: 155,
  doneWidth: 0,
  lateWidth: 0,
};
const rowBounds: RowBoundingRect = { id: 'issue-1', top: 10, height: 150 };
const taskBarMiddlePosition = rowBounds.top + taskBar.top + taskBar.height / 2;
const todayLinePosition = 1234;

test('TaskBarがなければRowの位置とcenterPositionでイナズマ線を作成', () => {
  expect(buildProgressLineOffset(null, rowBounds, todayLinePosition, false)).toEqual({
    top: 85,
    left: todayLinePosition,
  });
});

describe('ProjectのTaskBar', () => {
  const projectTaskBar = { ...taskBar, resourceType: TaskBarResourceType.Project };

  test('基準位置固定', () => {
    expect(buildProgressLineOffset(projectTaskBar, rowBounds, todayLinePosition, false)).toEqual({
      top: taskBarMiddlePosition,
      left: todayLinePosition,
    });
  });
});

describe('両端が未来のタスク', () => {
  const centerPosition = taskBar.left - 1;

  test('完了しているならTaskBarの終端位置', () => {
    expect(buildProgressLineOffset(taskBar, rowBounds, centerPosition, true)).toEqual({
      top: taskBarMiddlePosition,
      left: taskBar.right,
    });
  });

  test('進行中なら進捗率に応じた位置', () => {
    const inProgressTaskBar = { ...taskBar, doneWidth: 30 };

    expect(buildProgressLineOffset(inProgressTaskBar, rowBounds, centerPosition, false)).toEqual({
      top: taskBarMiddlePosition,
      left: inProgressTaskBar.left + inProgressTaskBar.doneWidth,
    });
  });

  test('未着手なら基準位置', () => {
    const notStartedTaskBar = { ...taskBar, doneWidth: 0 };

    expect(buildProgressLineOffset(notStartedTaskBar, rowBounds, centerPosition, false)).toEqual({
      top: taskBarMiddlePosition,
      left: centerPosition,
    });
  });
});

describe('開始日が過去で終了日が未来のタスク', () => {
  const centerPosition = taskBar.right - 1;

  test('完了しているならTaskBarの終端位置', () => {
    expect(buildProgressLineOffset(taskBar, rowBounds, centerPosition, true)).toEqual({
      top: taskBarMiddlePosition,
      left: taskBar.right,
    });
  });

  test('進行中なら進捗率に応じた位置', () => {
    const inProgressTaskBar = { ...taskBar, doneWidth: 30 };

    expect(buildProgressLineOffset(inProgressTaskBar, rowBounds, centerPosition, false)).toEqual({
      top: taskBarMiddlePosition,
      left: inProgressTaskBar.left + inProgressTaskBar.doneWidth,
    });
  });

  test('未着手ならは開始日の位置', () => {
    const notStartedTaskBar = { ...taskBar, doneWidth: 0 };

    expect(buildProgressLineOffset(notStartedTaskBar, rowBounds, centerPosition, false)).toEqual({
      top: taskBarMiddlePosition,
      left: taskBar.left,
    });
  });
});

describe('過去のタスク', () => {
  const centerPosition = taskBar.right + 1;

  test('完了しているなら基準位置', () => {
    expect(buildProgressLineOffset(taskBar, rowBounds, centerPosition, true)).toEqual({
      top: taskBarMiddlePosition,
      left: centerPosition,
    });
  });

  test('完了していないなら進捗率に応じた位置', () => {
    expect(buildProgressLineOffset(taskBar, rowBounds, centerPosition, false)).toEqual({
      top: taskBarMiddlePosition,
      left: taskBar.left + taskBar.doneWidth,
    });
  });
});
