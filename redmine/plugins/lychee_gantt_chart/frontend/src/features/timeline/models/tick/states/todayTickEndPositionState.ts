import { selector } from 'recoil';
import { stepWidthState } from '../../timeAxis';
import { todayTickPositionState } from '../index';

export const todayTickEndPositionState = selector<number>({
  key: 'timeline/todayTickEndPosition',
  get: ({ get }) => {
    const stepWidth = get(stepWidthState);
    const todayTickPosition = get(todayTickPositionState);

    return todayTickPosition + stepWidth;
  },
});
