import { BaselineSnapshotResourceIdentifier } from '../types';
import {
  getTaskBarRelativeBoundingRect,
  getTaskBarResourceTypeFromIdentifier,
  TaskBarBoundingRect,
  TaskBarResourceType,
} from '../../taskBar';

const relativeBoundingRectsByType: Record<TaskBarResourceType, Partial<TaskBarBoundingRect>> = {
  project: { top: 10 },
  version: { top: 10 },
  issue: { top: 3 },
} as const;

export const getBaselineBarRelativeBoundingRect = (
  identifier: BaselineSnapshotResourceIdentifier
): TaskBarBoundingRect => {
  const resourceType = getTaskBarResourceTypeFromIdentifier(identifier);

  return {
    ...getTaskBarRelativeBoundingRect(resourceType),
    ...relativeBoundingRectsByType[resourceType],
  };
};
