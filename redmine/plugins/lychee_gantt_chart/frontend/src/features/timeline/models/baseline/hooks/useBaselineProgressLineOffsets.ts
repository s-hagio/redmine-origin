import { useRecoilValue } from 'recoil';
import { baselineProgressLineOffsetsState } from '../states';

export const useBaselineProgressLineOffsets = () => useRecoilValue(baselineProgressLineOffsetsState);
