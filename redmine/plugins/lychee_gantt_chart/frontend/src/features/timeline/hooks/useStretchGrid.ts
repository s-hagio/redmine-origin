import { useCallback } from 'react';
import { useAddTicks } from '../models/tick';
import { useAddGridColumns } from '../models/grid';
import { TimeDirection, useStepWidth } from '../models/timeAxis';

type ReturnObject = {
  stretchedWidth: number;
  direction: TimeDirection | null;
};

export const useStretchGrid = <ScrollableElement extends HTMLElement = HTMLDivElement>() => {
  const addTicks = useAddTicks();
  const addGridColumns = useAddGridColumns();
  const stepWidth = useStepWidth();

  return useCallback(
    (el: ScrollableElement): ReturnObject => {
      const direction = getDirection(el);

      if (!direction) {
        return { stretchedWidth: 0, direction };
      }

      const newTicks = addTicks(Math.round((el.clientWidth * 2) / stepWidth), direction);
      addGridColumns(newTicks, direction);

      return {
        stretchedWidth: newTicks.length * stepWidth,
        direction,
      };
    },
    [addGridColumns, addTicks, stepWidth]
  );
};

const ADD_TICK_THRESHOLD = 200;

export const getDirection = (el: HTMLElement): TimeDirection | null => {
  if (el.scrollLeft <= ADD_TICK_THRESHOLD) {
    return TimeDirection.Backward;
  } else if (el.scrollWidth - (el.scrollLeft + el.clientWidth) <= ADD_TICK_THRESHOLD) {
    return TimeDirection.Forward;
  }

  return null;
};
