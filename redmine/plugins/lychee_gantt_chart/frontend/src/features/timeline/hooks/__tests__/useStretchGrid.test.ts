import { renderHook } from '@testing-library/react';
import { useAddTicks } from '../../models/tick';
import { useAddGridColumns } from '../../models/grid';
import { useStretchGrid } from '../useStretchGrid';
import { TimeDirection } from '../../models/timeAxis';

vi.mock('../../models/tick', () => ({
  useAddTicks: vi.fn(),
}));

vi.mock('../../models/grid', () => ({
  useAddGridColumns: vi.fn(),
}));

vi.mock('../../models/timeAxis', async () => ({
  ...(await vi.importActual<object>('../../models/timeAxis')),
  useStepWidth: vi.fn().mockReturnValue(2),
}));

const addTicksMock = vi.fn();
const addGridColumnsMock = vi.fn();

beforeEach(() => {
  vi.mocked(useAddTicks).mockReturnValue(addTicksMock);
  vi.mocked(useAddGridColumns).mockReturnValue(addGridColumnsMock);
});

const createElementMock = () => {
  const el = new HTMLDivElement();
  Object.defineProperty(el, 'clientWidth', { value: 500, writable: false });
  Object.defineProperty(el, 'scrollWidth', { value: 2000, writable: false });
  return el;
};

test('スクロール量がしきい値を超えていたらTickを生成する', () => {
  const { result } = renderHook(() => ({
    execute: useStretchGrid(),
  }));

  const el = createElementMock();

  // センタリング
  el.scrollLeft = el.scrollWidth / 2 - el.clientWidth / 2;

  expect(result.current.execute(el)).toStrictEqual({ stretchedWidth: 0, direction: null });
  expect(addTicksMock).not.toBeCalled();
  expect(addGridColumnsMock).not.toBeCalled();

  // しきい値まで右側（前方）にスクロール
  el.scrollLeft = el.scrollWidth - el.clientWidth - 200;

  const forwardStretchedTicks = [new Date(2023, 2, 13), new Date(2023, 2, 14)] as const;
  addTicksMock.mockReturnValueOnce(forwardStretchedTicks);

  expect(result.current.execute(el)).toStrictEqual({ stretchedWidth: 4, direction: TimeDirection.Forward });
  expect(addTicksMock).toBeCalledWith(500, TimeDirection.Forward);
  expect(addGridColumnsMock).toBeCalledWith(forwardStretchedTicks, TimeDirection.Forward);

  // しきい値まで左側（後方）にスクロール
  el.scrollLeft = 200;

  const backwardStretchedTicks = [new Date(2023, 2, 13), new Date(2023, 2, 14)] as const;
  addTicksMock.mockReturnValueOnce(backwardStretchedTicks);

  expect(result.current.execute(el)).toStrictEqual({ stretchedWidth: 4, direction: TimeDirection.Backward });
  expect(addTicksMock).toBeCalledWith(500, TimeDirection.Backward);
  expect(addGridColumnsMock).toBeCalledWith(backwardStretchedTicks, TimeDirection.Backward);
});
