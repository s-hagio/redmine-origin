import { atom } from 'recoil';
import { IdentifiableCoreResource } from '@/models/coreResource';

export const lastLoadedResourcesState = atom<IdentifiableCoreResource[]>({
  key: 'resourceLoader/lastLoadedResources',
  default: [],
});
