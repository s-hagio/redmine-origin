import { atomFamily } from 'recoil';
import { IdentifiableCoreResource } from '@/models/coreResource';
import { LoadStatus } from '../types';

export const loadStatusState = atomFamily<LoadStatus, IdentifiableCoreResource>({
  key: 'resourceLoader/loadStatus',
  default: LoadStatus.Pending,
});
