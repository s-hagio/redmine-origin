import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { createClient } from '@/api';
import { issueEntityState } from '@/models/issue';
import { rootIssue, parentIssue } from '@/__fixtures__';
import { useLoadIssues } from '../useLoadIssues';

vi.mock('@/api', () => ({
  createClient: vi.fn().mockReturnValue({
    issues: { $get: vi.fn() },
  }),
}));

const [issue1, issue2] = [rootIssue, parentIssue];

test('ID配列を元にプロジェクトデータを取得し個別の状態を更新', async () => {
  const client = createClient();
  vi.mocked(client.issues.$get).mockResolvedValueOnce([issue1, issue2]);

  const { result } = renderRecoilHook(() => ({
    loadIssues: useLoadIssues(),
    issue1: useRecoilValue(issueEntityState(issue1.id)),
    issue2: useRecoilValue(issueEntityState(issue2.id)),
  }));

  await act(() => result.current.loadIssues([1, 2, 3]));

  expect(client.issues.$get).lastCalledWith({ query: { ids: '1,2,3' } });
  expect(result.current.issue1).toStrictEqual(issue1);
  expect(result.current.issue2).toStrictEqual(issue2);
});
