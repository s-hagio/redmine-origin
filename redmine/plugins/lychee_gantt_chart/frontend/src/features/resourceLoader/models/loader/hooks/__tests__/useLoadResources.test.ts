import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { CoreResourceType } from '@/models/coreResource';
import {
  parentProject,
  mainProject,
  versionSharedByNone,
  versionSharedByHierarchy,
  rootIssue,
  parentIssue,
  childIssue,
} from '@/__fixtures__';
import { lastLoadedResourcesState } from '../../states';
import { useLoadResources } from '../useLoadResources';
import { useLoadProjects } from '../useLoadProjects';
import { useLoadVersions } from '../useLoadVersions';
import { useLoadIssues } from '../useLoadIssues';

vi.mock('../useLoadProjects', () => ({
  useLoadProjects: vi.fn().mockReturnValue(vi.fn().mockResolvedValue(null)),
}));
vi.mock('../useLoadVersions', () => ({
  useLoadVersions: vi.fn().mockReturnValue(vi.fn().mockResolvedValue(null)),
}));
vi.mock('../useLoadIssues', () => ({
  useLoadIssues: vi.fn().mockReturnValue(vi.fn().mockResolvedValue(null)),
}));

const loadableProjects = [parentProject, mainProject].map(({ id }) => ({
  type: CoreResourceType.Project,
  id,
}));
const loadableVersions = [versionSharedByNone, versionSharedByHierarchy].map(({ id }) => ({
  type: CoreResourceType.Version,
  id,
}));
const loadableIssues = [rootIssue, parentIssue, childIssue].map(({ id }) => ({
  type: CoreResourceType.Issue,
  id,
}));

test('RowResourceTypeごとにリソースをロード', async () => {
  const { result } = renderRecoilHook(() => ({
    loadResources: useLoadResources(),
    loadProjects: useLoadProjects(),
    loadVersions: useLoadVersions(),
    loadIssues: useLoadIssues(),
  }));

  await act(() =>
    result.current.loadResources([...loadableProjects, ...loadableVersions, ...loadableIssues])
  );

  expect(result.current.loadProjects).lastCalledWith(loadableProjects.map(({ id }) => id));
  expect(result.current.loadVersions).lastCalledWith(loadableVersions.map(({ id }) => id));
  expect(result.current.loadIssues).lastCalledWith(loadableIssues.map(({ id }) => id));
});

test('ロード中／ロード完了のリソースはロードしない', async () => {
  const { result } = renderRecoilHook(() => ({
    loadResources: useLoadResources(),
    loadIssues: useLoadIssues(),
  }));

  const [issueRow1, issueRow2, issueRow3] = loadableIssues;

  await act(async () => {
    // awaitを付けずにロード中を維持
    const firstCallPromise = result.current.loadResources([issueRow1]);
    expect(result.current.loadIssues).lastCalledWith([issueRow1.id]);

    await result.current.loadResources([issueRow1, issueRow2]);
    expect(result.current.loadIssues).lastCalledWith([issueRow2.id]);

    // 最初のロードを完了
    await firstCallPromise;

    await result.current.loadResources([issueRow1, issueRow2, issueRow3]);
    expect(result.current.loadIssues).lastCalledWith([issueRow3.id]);
  });
});

test('ロードに成功した行をlastLoadedRowsStateにセット', async () => {
  const { result } = renderRecoilHook(() => ({
    loadResources: useLoadResources(),
    loadIssues: useLoadIssues(),
    lastLoadedRows: useRecoilValue(lastLoadedResourcesState),
  }));

  // チケットのロードだけ失敗
  vi.mocked(result.current.loadIssues).mockRejectedValueOnce(null);

  await act(() =>
    result.current.loadResources([...loadableProjects, ...loadableVersions, ...loadableIssues])
  );

  expect(result.current.lastLoadedRows).toStrictEqual([...loadableProjects, ...loadableVersions]);
});
