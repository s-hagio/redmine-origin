import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { createClient } from '@/api';
import { versionState } from '@/models/version';
import { versionSharedByNone, versionSharedByHierarchy } from '@/__fixtures__';
import { useLoadVersions } from '../useLoadVersions';

vi.mock('@/api', () => ({
  createClient: vi.fn().mockReturnValue({
    versions: { $get: vi.fn() },
  }),
}));

const [version1, version2] = [versionSharedByNone, versionSharedByHierarchy];

test('ID配列を元にプロジェクトデータを取得し個別の状態を更新', async () => {
  const client = createClient();
  vi.mocked(client.versions.$get).mockResolvedValueOnce([version1, version2]);

  const { result } = renderRecoilHook(() => ({
    loadVersions: useLoadVersions(),
    version1: useRecoilValue(versionState(versionSharedByNone.id)),
    version2: useRecoilValue(versionState(versionSharedByHierarchy.id)),
  }));

  await act(() => result.current.loadVersions([1, 2, 3]));

  expect(client.versions.$get).lastCalledWith({ query: { ids: '1,2,3' } });
  expect(result.current.version1).toStrictEqual(version1);
  expect(result.current.version2).toStrictEqual(version2);
});
