import { useRecoilValue } from 'recoil';
import { lastLoadedResourcesState } from '../states';

export const useLastLoadedResources = () => useRecoilValue(lastLoadedResourcesState);
