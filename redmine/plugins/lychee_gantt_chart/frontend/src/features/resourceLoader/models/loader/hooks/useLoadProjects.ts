import { useRecoilCallback } from 'recoil';
import { createClient } from '@/api';
import { projectState } from '@/models/project';
import { LoadResource } from '../types';

export const useLoadProjects = (): LoadResource => {
  return useRecoilCallback(({ transact_UNSTABLE }) => {
    return async (ids) => {
      const client = createClient();
      const projects = await client.projects.$get({ query: { ids: ids.join(',') } });

      transact_UNSTABLE(({ set }) => {
        projects.forEach((project) => {
          set(projectState(project.id), (current) => ({ ...current, ...project }));
        });
      });
    };
  });
};
