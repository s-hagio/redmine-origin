import { useRecoilCallback } from 'recoil';
import { createClient } from '@/api';
import { versionState } from '@/models/version';
import { LoadResource } from '../types';

export const useLoadVersions = (): LoadResource => {
  return useRecoilCallback(({ transact_UNSTABLE }) => {
    return async (ids) => {
      const client = createClient();
      const versions = await client.versions.$get({ query: { ids: ids.join(',') } });

      transact_UNSTABLE(({ set }) => {
        versions.forEach((version) => {
          set(versionState(version.id), (current) => ({ ...current, ...version }));
        });
      });
    };
  });
};
