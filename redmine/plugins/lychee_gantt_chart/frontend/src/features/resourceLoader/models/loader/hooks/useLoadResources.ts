import { useRecoilCallback } from 'recoil';
import { groupBy } from '@/utils/array';
import { IdentifiableCoreResource as Resource, CoreResourceType } from '@/models/coreResource';
import { LoadStatus, LoadResource } from '../types';
import { loadStatusState, lastLoadedResourcesState } from '../states';
import { useLoadIssues } from './useLoadIssues';
import { useLoadProjects } from './useLoadProjects';
import { useLoadVersions } from './useLoadVersions';

const useLoadResourceMap = (): ReadonlyMap<CoreResourceType, LoadResource> => {
  return new Map([
    [CoreResourceType.Project, useLoadProjects()],
    [CoreResourceType.Version, useLoadVersions()],
    [CoreResourceType.Issue, useLoadIssues()],
  ]);
};

type LoadResources = (resources: Resource[]) => Promise<void>;
type Loader = () => Promise<void>;

const getLoadStatusId = ({ type, id }: Resource) => ({ type, id });

export const useLoadResources = (): LoadResources => {
  const loadResourceMap = useLoadResourceMap();

  return useRecoilCallback(
    ({ snapshot, set, transact_UNSTABLE }) => {
      return async (resources) => {
        const resourcesByType = groupBy(resources, 'type');
        const loadedResources: Resource[] = [];
        const loaders: Loader[] = [];

        const updateLoadStatus = (pendingResources: Resource[], status: LoadStatus) => {
          transact_UNSTABLE(({ set }) => {
            pendingResources.forEach((resource) => set(loadStatusState(getLoadStatusId(resource)), status));
          });
        };

        loadResourceMap.forEach((load, type) => {
          const pendingResources = (resourcesByType[type] || []).filter((resource) => {
            const status = snapshot.getLoadable(loadStatusState(getLoadStatusId(resource))).getValue();

            return status === LoadStatus.Pending || status === LoadStatus.Failed;
          });

          if (!pendingResources?.length) {
            return;
          }

          loaders.push(async () => {
            try {
              updateLoadStatus(pendingResources, LoadStatus.Loading);

              await load(pendingResources.map(({ id }) => id));
              loadedResources.push(...pendingResources);

              updateLoadStatus(pendingResources, LoadStatus.Loaded);
            } catch (e) {
              updateLoadStatus(pendingResources, LoadStatus.Failed);
            }
          });
        });

        await Promise.all(loaders.map((load) => load()));

        set(lastLoadedResourcesState, loadedResources);
      };
    },
    [loadResourceMap]
  );
};
