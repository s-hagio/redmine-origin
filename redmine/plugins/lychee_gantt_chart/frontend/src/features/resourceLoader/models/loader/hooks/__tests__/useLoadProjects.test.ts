import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { createClient } from '@/api';
import { projectState } from '@/models/project';
import { rootProject, mainProject } from '@/__fixtures__';
import { useLoadProjects } from '../useLoadProjects';

vi.mock('@/api', () => ({
  createClient: vi.fn().mockReturnValue({
    projects: { $get: vi.fn() },
  }),
}));

const [project1, project2] = [rootProject, mainProject];

test('ID配列を元にプロジェクトデータを取得し個別の状態を更新', async () => {
  const client = createClient();
  vi.mocked(client.projects.$get).mockResolvedValueOnce([project1, project2]);

  const { result } = renderRecoilHook(() => ({
    loadProjects: useLoadProjects(),
    project1: useRecoilValue(projectState(project1.id)),
    project2: useRecoilValue(projectState(project2.id)),
  }));

  await act(() => result.current.loadProjects([1, 2, 3]));

  expect(client.projects.$get).lastCalledWith({ query: { ids: '1,2,3' } });
  expect(result.current.project1).toStrictEqual(project1);
  expect(result.current.project2).toStrictEqual(project2);
});
