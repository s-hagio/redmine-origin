export const LoadStatus = {
  Pending: 'pending',
  Loading: 'loading',
  Loaded: 'loaded',
  Failed: 'failed',
} as const;

export type LoadStatus = typeof LoadStatus[keyof typeof LoadStatus];

export type LoadResource = <T = number | string>(ids: T[]) => Promise<void>;
