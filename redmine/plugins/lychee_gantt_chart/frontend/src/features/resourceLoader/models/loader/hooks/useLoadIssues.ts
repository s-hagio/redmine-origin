import { useRecoilCallback } from 'recoil';
import { createClient } from '@/api';
import { issueEntityState } from '@/models/issue';
import { LoadResource } from '../types';

export const useLoadIssues = (): LoadResource => {
  return useRecoilCallback(({ transact_UNSTABLE }) => {
    return async (ids) => {
      const client = createClient();
      const issues = await client.issues.$get({ query: { ids: ids.join(',') } });

      transact_UNSTABLE(({ set }) => {
        issues.forEach((issue) => {
          set(issueEntityState(issue.id), (current) => ({ ...current, ...issue }));
        });
      });
    };
  });
};
