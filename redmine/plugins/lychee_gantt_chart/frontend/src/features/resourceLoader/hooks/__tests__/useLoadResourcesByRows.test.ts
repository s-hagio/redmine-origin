import { act, renderHook } from '@testing-library/react';
import { toIdentifiableCoreResource } from '@/features/row';
import { mainProjectRow, mainProjectVersionRow, rootIssueRow, newStatusGroupRow } from '@/__fixtures__';
import { useLoadResources } from '../../models/loader';
import { useLoadResourcesByRows } from '../useLoadResourcesByRows';

vi.mock('../../models/loader/hooks/useLoadResources', () => ({
  useLoadResources: vi.fn().mockReturnValue(vi.fn()),
}));

const loadableRows = [mainProjectRow, mainProjectVersionRow, rootIssueRow];
const notLoadableRows = [newStatusGroupRow];

test('読み込み可能なRowを所定の形式を変換してloadResourcesへ渡す', () => {
  const { result } = renderHook(() => ({
    loadResourcesByRows: useLoadResourcesByRows(),
    loadResources: useLoadResources(),
  }));

  act(() => {
    result.current.loadResourcesByRows([...loadableRows, ...notLoadableRows]);
  });

  expect(result.current.loadResources).toBeCalledWith([
    toIdentifiableCoreResource(mainProjectRow),
    toIdentifiableCoreResource(mainProjectVersionRow),
    toIdentifiableCoreResource(rootIssueRow),
  ]);
});
