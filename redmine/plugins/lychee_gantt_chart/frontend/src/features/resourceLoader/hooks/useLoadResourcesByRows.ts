import { useCallback } from 'react';
import { RowResourceType, Row, toIdentifiableCoreResource, CoreResourceRow } from '@/features/row';
import { useLoadResources } from '../models/loader';

type LoadResourcesByRows = (rows: Row[]) => void;

export const useLoadResourcesByRows = (): LoadResourcesByRows => {
  const loadResources = useLoadResources();

  return useCallback(
    (rows) => {
      loadResources(rows.filter(isLoadableResourceRow).map(toIdentifiableCoreResource));
    },
    [loadResources]
  );
};

const loadableResourceRowType = new Set<string>([
  RowResourceType.Project,
  RowResourceType.Version,
  RowResourceType.Issue,
]);

const isLoadableResourceRow = (row: Row): row is CoreResourceRow => {
  return loadableResourceRowType.has(row.resourceType);
};
