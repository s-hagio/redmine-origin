import * as React from 'react';
import { render } from '@testing-library/react';
import { useVisibleRows } from '@/features/row';
import { rootIssueRow, rootProjectRow, rootProjectVersionRow, versionIssueRow } from '@/__fixtures__';
import { useLoadResourcesByRows } from '../../hooks/useLoadResourcesByRows';
import RowResourceLoader from '../RowResourceLoader';

vi.mock('@/features/row', async () => ({
  ...(await vi.importActual<object>('@/features/row')),
  useVisibleRows: vi.fn().mockReturnValue([]),
}));

vi.mock('../../hooks/useLoadResourcesByRows', () => ({
  useLoadResourcesByRows: vi.fn(),
}));

const rows1 = [rootProjectRow, rootIssueRow];
const rows2 = [rootProjectRow, rootProjectVersionRow, versionIssueRow];

test('Render', () => {
  vi.mocked(useVisibleRows).mockReturnValue(rows1);

  const loadResourcesByRowsMock = vi.fn();
  vi.mocked(useLoadResourcesByRows).mockReturnValue(loadResourcesByRowsMock);

  const { rerender, container } = render(<RowResourceLoader />);
  expect(container.textContent).toBe('');

  expect(loadResourcesByRowsMock).toHaveBeenLastCalledWith(rows1);

  loadResourcesByRowsMock.mockReset();
  rerender(<RowResourceLoader />);

  expect(loadResourcesByRowsMock).not.toHaveBeenCalled();

  vi.mocked(useVisibleRows).mockReturnValue(rows2);

  loadResourcesByRowsMock.mockReset();
  rerender(<RowResourceLoader />);

  expect(loadResourcesByRowsMock).toHaveBeenLastCalledWith(rows2);
});
