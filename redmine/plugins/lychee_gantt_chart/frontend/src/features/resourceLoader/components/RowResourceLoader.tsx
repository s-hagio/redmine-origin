import * as React from 'react';
import { useVisibleRows } from '@/features/row';
import { useLoadResourcesByRows } from '../hooks/useLoadResourcesByRows';

const RowResourceLoader: React.FC = () => {
  const rows = useVisibleRows();
  const loadResources = useLoadResourcesByRows();

  React.useLayoutEffect(() => {
    loadResources(rows);
  }, [rows, loadResources]);

  return null;
};

export default RowResourceLoader;
