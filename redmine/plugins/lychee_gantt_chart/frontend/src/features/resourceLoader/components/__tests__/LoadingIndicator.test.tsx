import * as React from 'react';
import { renderWithTranslation } from '@/test-utils';
import LoadingIndicator from '../LoadingIndicator';

test('Render', () => {
  const result = renderWithTranslation(<LoadingIndicator />);

  expect(result.container).toMatchSnapshot();
});
