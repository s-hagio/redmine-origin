import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { getZIndex } from '@/styles';

const LoadingIndicator: React.FC = () => {
  const { t } = useTranslation();

  return (
    <div className={containerStyle}>
      <div className={messageBoxStyle}>{t('label.loading')}</div>
    </div>
  );
};

export default LoadingIndicator;

const containerStyle = css`
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  z-index: ${getZIndex('resourceLoader/LoadingIndicator')};
`;

const messageBoxStyle = css`
  display: inline-flex;
  padding: 0.5rem 1rem;
  border-radius: 5px;
  background: #000;
  color: #fff;
`;
