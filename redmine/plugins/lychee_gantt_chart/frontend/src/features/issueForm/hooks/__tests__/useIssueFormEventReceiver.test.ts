import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { IssueFormEventType } from '../../types';
import { eventHandlerRegistrationsState } from '../../states';
import { useIssueFormEventReceiver } from '../useIssueFormEventReceiver';

test('指定イベントタイプのハンドラーをすべて実行', () => {
  const registration1 = { adderName: 'test1', handler: vi.fn() };
  const registration2 = { adderName: 'test2', handler: vi.fn() };
  const registration3 = { adderName: 'test3', handler: vi.fn() };

  const { result } = renderRecoilHook(() => useIssueFormEventReceiver(IssueFormEventType.BeforeSubmit), {
    initializeState: ({ set }) => {
      set(eventHandlerRegistrationsState(IssueFormEventType.BeforeSubmit), [registration1, registration2]);
      set(eventHandlerRegistrationsState(IssueFormEventType.SuccessSave), [registration3]);
    },
  });

  const event = new CustomEvent('lifのイベント名', {
    detail: {
      issueId: 123,
      originalParam: { subject: 'foo' },
    },
  });

  expect(registration1.handler).not.toBeCalled();
  expect(registration2.handler).not.toBeCalled();
  expect(registration3.handler).not.toBeCalled();

  act(() => result.current(event));

  expect(registration1.handler).toBeCalledWith(event);
  expect(registration2.handler).toBeCalledWith(event);
  expect(registration3.handler).not.toBeCalled();
});
