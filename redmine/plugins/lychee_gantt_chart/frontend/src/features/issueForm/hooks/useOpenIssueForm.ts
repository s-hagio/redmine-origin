import { useCallback } from 'react';
import { APIError, isAPIError } from '@/api';
import { IssueParams } from '@/models/issue';
import { IssueFormMeta, IssueFormParams } from '../types';
import { openIssueForm } from '../helpers';

type Options = {
  meta?: IssueFormMeta;
};

export const useOpenIssueForm = () => {
  return useCallback((params?: IssueParams, error?: APIError | unknown, options: Options = {}) => {
    const normalizedParams: IssueFormParams = { ...params, _meta: options.meta };

    if (isAPIError(error)) {
      return openIssueForm(normalizedParams, error);
    }

    return openIssueForm(normalizedParams);
  }, []);
};
