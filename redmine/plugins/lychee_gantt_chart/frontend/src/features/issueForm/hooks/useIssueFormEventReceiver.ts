import { useRecoilCallback } from 'recoil';
import { IssueFormEventType, IssueFormEventHandler } from '../types';
import { eventHandlerRegistrationsState } from '../states';

export const useIssueFormEventReceiver = (eventType: IssueFormEventType): IssueFormEventHandler => {
  return useRecoilCallback(
    ({ snapshot }) => {
      return (event) => {
        snapshot
          .getLoadable(eventHandlerRegistrationsState(eventType))
          .valueOrThrow()
          .forEach(({ handler }) => handler(event));
      };
    },
    [eventType]
  );
};
