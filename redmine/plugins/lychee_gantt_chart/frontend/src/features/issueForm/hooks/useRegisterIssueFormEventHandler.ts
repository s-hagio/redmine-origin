import { useRecoilCallback } from 'recoil';
import { eventHandlerRegistrationsState } from '../states';
import { IssueFormEventType, IssueFormEventHandlerRegistration } from '../types';

type AddIssueFormEventHandler = (
  eventType: IssueFormEventType,
  adderName: IssueFormEventHandlerRegistration['adderName'],
  handler: IssueFormEventHandlerRegistration['handler']
) => void;

export const useRegisterIssueFormEventHandler = (): AddIssueFormEventHandler => {
  return useRecoilCallback(({ set }) => {
    return (eventType, adderName, handler) => {
      set(eventHandlerRegistrationsState(eventType), (current) => {
        const newRegistrations = [...current];
        const index = newRegistrations.findIndex((registration) => registration.adderName === adderName);

        const newRegistration = { adderName, handler };

        if (index === -1) {
          newRegistrations.push(newRegistration);
        } else {
          newRegistrations[index] = newRegistration;
        }

        return newRegistrations;
      });
    };
  });
};
