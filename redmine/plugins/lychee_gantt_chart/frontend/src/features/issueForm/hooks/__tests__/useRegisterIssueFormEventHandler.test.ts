import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { IssueFormEventType } from '../../types';
import { eventHandlerRegistrationsState } from '../../states';
import { useRegisterIssueFormEventHandler } from '../useRegisterIssueFormEventHandler';

test('adderNameごとにユニークなイベントハンドラーを登録できる', () => {
  const { result } = renderRecoilHook(() => ({
    register: useRegisterIssueFormEventHandler(),
    registrations: useRecoilValue(eventHandlerRegistrationsState(IssueFormEventType.SuccessSave)),
  }));

  const adderName = 'test';
  const handler = vi.fn();

  act(() => result.current.register(IssueFormEventType.SuccessSave, adderName, handler));
  expect(result.current.registrations).toEqual([{ adderName, handler }]);

  act(() => result.current.register(IssueFormEventType.SuccessSave, adderName, handler));
  expect(result.current.registrations).toEqual([{ adderName, handler }]);

  const newHandler = vi.fn();
  act(() => result.current.register(IssueFormEventType.SuccessSave, adderName, newHandler));
  expect(result.current.registrations).toEqual([{ adderName, handler: newHandler }]);

  act(() => result.current.register(IssueFormEventType.SuccessSave, 'another', handler));

  expect(result.current.registrations).toEqual([
    { adderName, handler: newHandler },
    { adderName: 'another', handler },
  ]);
});

test('複数のイベントタイプに対して登録できる', () => {
  const { result } = renderRecoilHook(() => ({
    register: useRegisterIssueFormEventHandler(),
    beforeSubmitRegistrations: useRecoilValue(
      eventHandlerRegistrationsState(IssueFormEventType.BeforeSubmit)
    ),
    successSaveRegistrations: useRecoilValue(eventHandlerRegistrationsState(IssueFormEventType.SuccessSave)),
  }));

  const registration1 = { adderName: 'test1', handler: vi.fn() };
  const registration2 = { adderName: 'test2', handler: vi.fn() };
  const registration3 = { adderName: 'test3', handler: vi.fn() };

  act(() => {
    result.current.register(IssueFormEventType.BeforeSubmit, registration1.adderName, registration1.handler);
    result.current.register(IssueFormEventType.SuccessSave, registration2.adderName, registration2.handler);
    result.current.register(IssueFormEventType.SuccessSave, registration3.adderName, registration3.handler);
  });

  expect(result.current.beforeSubmitRegistrations).toEqual([registration1]);
  expect(result.current.successSaveRegistrations).toEqual([registration2, registration3]);
});
