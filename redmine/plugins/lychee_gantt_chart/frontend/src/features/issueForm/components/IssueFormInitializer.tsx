import * as React from 'react';
import { useEffect } from 'react';
import { useIssueFormEventReceiver } from '../hooks';
import { IssueFormEventType } from '../types';

const IssueFormInitializer: React.FC = () => {
  const handleBeforeSubmit = useIssueFormEventReceiver(IssueFormEventType.BeforeSubmit);
  const handleSaveSuccess = useIssueFormEventReceiver(IssueFormEventType.SuccessSave);

  useEffect(() => {
    const setupEventHandlers = () => {
      window.lif.onBeforeSubmit(handleBeforeSubmit);
      window.lif.onSuccessSave(handleSaveSuccess);
    };

    if (window.lif) {
      setupEventHandlers();
    } else {
      document.addEventListener('DOMContentLoaded', setupEventHandlers);
    }
  }, [handleBeforeSubmit, handleSaveSuccess]);

  return null;
};

export default React.memo(IssueFormInitializer);
