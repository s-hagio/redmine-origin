import * as React from 'react';
import { act, render } from '@testing-library/react';
import { IssueFormEventType } from '../../types';
import IssueFormInitializer from '../IssueFormInitializer';

const useIssueFormEventReceiverMock = vi.hoisted(() => vi.fn());

vi.mock('../../hooks', () => ({
  useIssueFormEventReceiver: useIssueFormEventReceiverMock,
}));

const receiverMap = {
  [IssueFormEventType.BeforeSubmit]: vi.fn(),
  [IssueFormEventType.SuccessSave]: vi.fn(),
};

beforeEach(() => {
  vi.spyOn(window.lif, 'onBeforeSubmit');
  vi.spyOn(window.lif, 'onSuccessSave');

  useIssueFormEventReceiverMock.mockImplementation((eventType: IssueFormEventType) => receiverMap[eventType]);
});

test('イベントタイプ別にIssueFormEventReceiverをセットする', () => {
  act(() => render(<IssueFormInitializer />));

  expect(window.lif.onBeforeSubmit).toBeCalledWith(receiverMap[IssueFormEventType.BeforeSubmit]);
  expect(window.lif.onSuccessSave).toBeCalledWith(receiverMap[IssueFormEventType.SuccessSave]);
});
