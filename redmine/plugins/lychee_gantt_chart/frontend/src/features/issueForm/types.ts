import { Issue, IssueID, IssueParams } from '@/models/issue';

export type IssueFormMeta = Record<string, unknown>;

export type IssueViewParams = Pick<Issue, 'id'>;
export type IssueFormParams<T = IssueFormMeta> = IssueParams & {
  _meta?: T;
};

export type IssueFormOpenParams = IssueViewParams | IssueParams;

export type IssueFormEvent<T extends object = IssueFormMeta> = CustomEvent<{
  issueId: IssueID;
  originalParam: IssueFormParams<T>;
}>;

export type IssueFormEventHandler<T extends object = IssueFormMeta> = (event: IssueFormEvent<T>) => void;

export type LycheeIssueForm = {
  open: (params: IssueFormOpenParams, error?: Error) => Promise<void>;
  onBeforeSubmit: (handler: IssueFormEventHandler) => void;
  onSuccessSave: (handler: IssueFormEventHandler) => void;
};

export const IssueFormEventType = {
  BeforeSubmit: 'beforeSubmit',
  SuccessSave: 'successSave',
} as const;
export type IssueFormEventType = (typeof IssueFormEventType)[keyof typeof IssueFormEventType];

export type IssueFormEventHandlerRegistration = {
  adderName: string;
  handler: IssueFormEventHandler;
};

declare global {
  interface Window {
    lif: LycheeIssueForm;
  }
}
