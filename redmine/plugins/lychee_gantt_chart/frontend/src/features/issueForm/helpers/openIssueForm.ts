import { LycheeIssueForm } from '../types';

export const openIssueForm: LycheeIssueForm['open'] = (...args) => {
  return window.lif.open(...args);
};
