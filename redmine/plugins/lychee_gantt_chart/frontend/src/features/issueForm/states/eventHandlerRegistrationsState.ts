import { atomFamily } from 'recoil';
import { IssueFormEventType, IssueFormEventHandlerRegistration } from '../types';

export const eventHandlerRegistrationsState = atomFamily<
  IssueFormEventHandlerRegistration[],
  IssueFormEventType
>({
  key: 'issueForm/eventHandlerRegistrations',
  default: () => [],
});
