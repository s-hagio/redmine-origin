import { useRecoilState } from 'recoil';
import { sortOrderState } from '../states';
import { SortCriteriaIndex } from '../types';

export const useSortOrderState = (index: SortCriteriaIndex) => useRecoilState(sortOrderState(index));
