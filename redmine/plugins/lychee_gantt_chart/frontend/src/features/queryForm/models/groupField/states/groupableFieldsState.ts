import { selector } from 'recoil';
import { FieldDefinition, fieldDefinitionMapState } from '@/models/fieldDefinition';

export const groupableFieldsState = selector<FieldDefinition[]>({
  key: 'queryForm/groupableFields',
  get: ({ get }) => {
    const fieldDefinitionMap = get(fieldDefinitionMapState);
    const map = new Map<string, FieldDefinition>();

    fieldDefinitionMap.forEach((fieldDefinition) => {
      if (fieldDefinition.groupable) {
        map.set(fieldDefinition.name, fieldDefinition);
      }
    });

    return [...map.values()];
  },
});
