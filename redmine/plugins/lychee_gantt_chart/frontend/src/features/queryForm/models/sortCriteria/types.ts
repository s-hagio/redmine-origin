import { SortCriteria } from '@/models/query';

export type SortCriteriaIndex = keyof SortCriteria;
