import { useRecoilValue } from 'recoil';
import { filterGroupsState } from '../states';

export const useFilterGroups = () => useRecoilValue(filterGroupsState);
