import {
  QueryFilter,
  QueryFilterGroupLabel,
  QueryFilterOperator,
  QueryFilterValue,
} from '@/models/queryFilter';

export type FilterGroupLabel = QueryFilterGroupLabel | null;

export type FilterGroup = {
  label: FilterGroupLabel;
  filters: QueryFilter[];
};

export type QueryFilterCondition = {
  operator: QueryFilterOperator;
  value: QueryFilterValue;
  multiple: boolean;
  disabled: boolean;
};
