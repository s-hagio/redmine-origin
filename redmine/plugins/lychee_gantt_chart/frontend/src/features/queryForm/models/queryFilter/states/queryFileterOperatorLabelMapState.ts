import { selector } from 'recoil';
import { settingsState } from '@/models/setting';

export const queryFilterOperatorLabelMapState = selector({
  key: 'queryForm/queryFilterOperatorLabelMap',
  get: ({ get }) => {
    const { queryFilterOperatorLabelMap } = get(settingsState);

    return queryFilterOperatorLabelMap;
  },
});
