export const FilterValueType = {
  Single: 'single',
  Range: 'range',
  Days: 'days',
  IssueId: 'issueId',
  ProjectId: 'projectId',
  None: 'none',
} as const;

export type FilterValueType = (typeof FilterValueType)[keyof typeof FilterValueType];
