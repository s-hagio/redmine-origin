import { selector } from 'recoil';
import { settingsState } from '@/models/setting';

export const queryFilterOperatorsMapState = selector({
  key: 'queryForm/queryFilterOperatorsMap',
  get: ({ get }) => {
    const { queryFilterOperatorsMap } = get(settingsState);

    return queryFilterOperatorsMap;
  },
});
