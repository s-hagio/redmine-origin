import { useRecoilState } from 'recoil';
import { optionalQueryState } from '../states';

export const useOptionalQueryState = () => useRecoilState(optionalQueryState);
