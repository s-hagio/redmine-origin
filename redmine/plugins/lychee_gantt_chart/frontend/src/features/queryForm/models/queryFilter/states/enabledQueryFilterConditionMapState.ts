import { selector } from 'recoil';
import { QueryFilterName } from '@/models/queryFilter';
import { QueryFilterCondition } from '../types';
import { selectedFilterNamesState } from './selectedFilterNamesState';
import { queryFilterConditionState } from './queryFilterConditionState';

type EnabledQueryFilterConditionMap = ReadonlyMap<QueryFilterName, QueryFilterCondition>;

export const enabledQueryFilterConditionMapState = selector<EnabledQueryFilterConditionMap>({
  key: 'queryForm/enabledQueryFilterConditionMap',
  get: ({ get }) => {
    const fieldNames = get(selectedFilterNamesState);

    return new Map(
      fieldNames
        .map((name) => {
          const condition = get(queryFilterConditionState(name));

          if (condition.disabled) {
            return;
          }

          return [name, condition];
        })
        .filter(Boolean) as [QueryFilterName, QueryFilterCondition][]
    );
  },
});
