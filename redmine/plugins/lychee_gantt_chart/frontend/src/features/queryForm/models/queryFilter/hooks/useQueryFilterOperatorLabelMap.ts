import { useRecoilValue } from 'recoil';
import { queryFilterOperatorLabelMapState } from '../states';

export const useQueryFilterOperatorLabelMap = () => useRecoilValue(queryFilterOperatorLabelMapState);
