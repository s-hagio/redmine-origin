import { useRecoilCallback } from 'recoil';
import { IssueQuery, useUpdateIssueQueryParams } from '@/models/query';
import { enabledQueryFilterConditionMapState, optionalQueryState } from '../states';
import { sortCriteriaState } from '../../sortCriteria';

export const useReflectIssueQuery = () => {
  const updateIssueQueryParams = useUpdateIssueQueryParams();

  return useRecoilCallback(
    ({ snapshot }) => {
      return () => {
        const enabledConditions = snapshot.getLoadable(enabledQueryFilterConditionMapState).getValue();
        const optionalQuery = snapshot.getLoadable(optionalQueryState).getValue();
        const sortCriteria = snapshot.getLoadable(sortCriteriaState).getValue();

        const query: Pick<IssueQuery, 'f' | 'op' | 'v' | 'sort'> = {
          f: [],
          op: {},
          v: {},
          sort: sortCriteria,
        };

        enabledConditions.forEach((condition, name) => {
          query.f.push(name);
          query.op[name] = condition.operator;
          query.v[name] = condition.value;
        });

        updateIssueQueryParams({ ...query, ...optionalQuery });
      };
    },
    [updateIssueQueryParams]
  );
};
