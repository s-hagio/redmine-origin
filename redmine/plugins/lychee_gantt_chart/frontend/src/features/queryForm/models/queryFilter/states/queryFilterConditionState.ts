import { selectorFamily } from 'recoil';
import { QueryFilterName } from '@/models/queryFilter';
import { QueryFilterCondition } from '../types';
import { queryFilterConditionEntityState } from './queryFilterConditionEntityState';

export const queryFilterConditionState = selectorFamily<QueryFilterCondition, QueryFilterName>({
  key: 'queryForm/queryFilterCondition',
  get: (name) => {
    return ({ get }) => {
      const entity = get(queryFilterConditionEntityState(name));

      return {
        ...entity,
        value: entity.multiple ? entity.value : entity.value.slice(0, 1),
      };
    };
  },
  set: (name) => {
    return ({ set }, newValue) => {
      set(queryFilterConditionEntityState(name), newValue);
    };
  },
});
