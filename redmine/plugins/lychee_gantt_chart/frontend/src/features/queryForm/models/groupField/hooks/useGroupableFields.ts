import { useRecoilValue } from 'recoil';
import { groupableFieldsState } from '../states';

export const useGroupableFields = () => useRecoilValue(groupableFieldsState);
