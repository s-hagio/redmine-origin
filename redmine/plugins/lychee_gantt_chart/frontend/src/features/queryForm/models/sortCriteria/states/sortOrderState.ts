import { atomFamily } from 'recoil';
import { SortOrder } from '@/models/query';
import { SortCriteriaIndex } from '../types';

export const sortOrderState = atomFamily<SortOrder, SortCriteriaIndex>({
  key: 'queryForm/sortOrder',
  default: ['', ''],
});
