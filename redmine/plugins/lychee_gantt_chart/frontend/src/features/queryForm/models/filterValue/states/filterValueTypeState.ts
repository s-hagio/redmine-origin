import { selectorFamily } from 'recoil';
import {
  availableQueryFilterState,
  QueryFilterName,
  QueryFilterOperator,
  QueryFilterType,
} from '@/models/queryFilter';
import { queryFilterConditionState } from '../../queryFilter';
import { FilterValueType } from '../types';

type TypedOperatorSet = Readonly<{
  valueType: FilterValueType;
  operators: QueryFilterOperator[];
}>;

const stringValueOperatorSet: TypedOperatorSet = {
  valueType: FilterValueType.Single,
  operators: ['~', '*~', '=', '!~', '!', '^', '$'],
};

const listValueOperatorSet: TypedOperatorSet = {
  valueType: FilterValueType.Single,
  operators: ['=', '!', 'ev', '!ev', 'cf']
};

const singleValueOperatorSet: TypedOperatorSet = {
  valueType: FilterValueType.Single,
  operators: ['=', '>=', '<='],
};

const rangeValueOperatorSet: TypedOperatorSet = {
  valueType: FilterValueType.Range,
  operators: ['><'],
};

const daysValueOperatorSet: TypedOperatorSet = {
  valueType: FilterValueType.Days,
  operators: ['<t+', '>t+', '><t+', 't+', '>t-', '<t-', '><t-', 't-'],
};

const issueIdOperatorSet: TypedOperatorSet = {
  valueType: FilterValueType.IssueId,
  operators: ['=', '!'],
};

const projectIdOperatorSet: TypedOperatorSet = {
  valueType: FilterValueType.ProjectId,
  operators: ['=p', '=!p', '!p'],
};

const treeValueOperatorSet: TypedOperatorSet = {
  valueType: FilterValueType.Single,
  operators: ['=', '~'],
};

const typedOperatorsByFilterType: Readonly<Record<QueryFilterType, ReadonlyArray<TypedOperatorSet>>> = {
  [QueryFilterType.List]: [listValueOperatorSet],
  [QueryFilterType.ListWithHistory]: [listValueOperatorSet],
  [QueryFilterType.ListStatus]: [listValueOperatorSet],
  [QueryFilterType.ListOptional]: [listValueOperatorSet],
  [QueryFilterType.ListOptionalWithHistory]: [listValueOperatorSet],
  [QueryFilterType.ListSubprojects]: [listValueOperatorSet],
  [QueryFilterType.Date]: [singleValueOperatorSet, rangeValueOperatorSet, daysValueOperatorSet],
  [QueryFilterType.DatePast]: [singleValueOperatorSet, rangeValueOperatorSet, daysValueOperatorSet],
  [QueryFilterType.String]: [stringValueOperatorSet],
  [QueryFilterType.Text]: [stringValueOperatorSet],
  [QueryFilterType.Integer]: [singleValueOperatorSet, rangeValueOperatorSet],
  [QueryFilterType.Float]: [singleValueOperatorSet, rangeValueOperatorSet],
  [QueryFilterType.Relation]: [issueIdOperatorSet, projectIdOperatorSet],
  [QueryFilterType.Tree]: [treeValueOperatorSet],
} as const;

export const filterValueTypeState = selectorFamily<FilterValueType, QueryFilterName>({
  key: 'queryForm/filterValueType',
  get: (name) => {
    return ({ get }) => {
      const { type } = get(availableQueryFilterState(name));
      const { operator } = get(queryFilterConditionState(name));

      const typedOperator = typedOperatorsByFilterType[type]?.find(({ operators }) =>
        operators.includes(operator)
      );

      return typedOperator?.valueType ?? FilterValueType.None;
    };
  },
});
