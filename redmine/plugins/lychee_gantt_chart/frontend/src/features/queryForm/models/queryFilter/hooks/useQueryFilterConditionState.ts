import { useRecoilState } from 'recoil';
import { QueryFilterName } from '@/models/queryFilter';
import { queryFilterConditionState } from '../states';

export const useQueryFilterConditionState = (name: QueryFilterName) => {
  return useRecoilState(queryFilterConditionState(name));
};
