import { atom } from 'recoil';
import { IssueQuery } from '@/models/query';

export const optionalQueryState = atom<Pick<IssueQuery, 'groupBy' | 'includeAncestorIssues'>>({
  key: 'queryForm/optionalQuery',
});
