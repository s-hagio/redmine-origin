import { useRecoilCallback } from 'recoil';
import { issueQueryState, SortOrder } from '@/models/query';
import { sortOrderState } from '../states';
import { SortCriteriaIndex } from '../types';

export const useResetLocalSortCriteria = () => {
  return useRecoilCallback(({ set, snapshot }) => {
    return () => {
      const { sort } = snapshot.getLoadable(issueQueryState).getValue();

      const sortCriteriaIndexes: SortCriteriaIndex[] = [0, 1, 2];

      sortCriteriaIndexes.forEach((index) => {
        const sortOrder: SortOrder = sort[index] ?? ['', ''];

        set(sortOrderState(index), sortOrder);
      });
    };
  });
};
