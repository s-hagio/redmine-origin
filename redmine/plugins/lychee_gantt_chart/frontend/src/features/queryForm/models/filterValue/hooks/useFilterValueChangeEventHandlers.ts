import { ChangeEvent, useMemo } from 'react';
import { QueryFilterName } from '@/models/queryFilter';
import { useQueryFilterConditionState } from '../../queryFilter';

export const useFilterValueChangeEventHandlers = <T extends HTMLInputElement | HTMLSelectElement>(
  name: QueryFilterName
) => {
  const [, setCondition] = useQueryFilterConditionState(name);

  return useMemo(
    () => ({
      handleValueChange: ({ currentTarget }: ChangeEvent<T>) => {
        setCondition((current) => ({
          ...current,
          value: [currentTarget.value],
        }));
      },
      handleFirstValueChange: ({ currentTarget }: ChangeEvent<T>) => {
        setCondition(({ value, ...rest }) => ({
          ...rest,
          value: [currentTarget.value, value[1]],
        }));
      },
      handleSecondValueChange: ({ currentTarget }: ChangeEvent<T>) => {
        setCondition(({ value, ...rest }) => ({
          ...rest,
          value: [value[0], currentTarget.value],
        }));
      },
    }),
    [setCondition]
  );
};
