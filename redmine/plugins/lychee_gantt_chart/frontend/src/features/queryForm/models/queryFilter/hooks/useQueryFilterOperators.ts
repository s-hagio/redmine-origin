import { useRecoilValue } from 'recoil';
import { QueryFilterType } from '@/models/queryFilter';
import { queryFilterOperatorsMapState } from '../states';

export const useQueryFilterOperators = (filterType: QueryFilterType) => {
  const map = useRecoilValue(queryFilterOperatorsMapState);

  return map[filterType];
};
