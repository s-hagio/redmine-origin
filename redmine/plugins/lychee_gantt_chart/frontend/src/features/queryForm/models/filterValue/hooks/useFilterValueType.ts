import { useRecoilValue } from 'recoil';
import { QueryFilterName } from '@/models/queryFilter';
import { filterValueTypeState } from '../states';

export const useFilterValueType = (name: QueryFilterName) => useRecoilValue(filterValueTypeState(name));
