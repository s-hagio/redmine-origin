import { useRecoilState } from 'recoil';
import { QueryFilterName } from '@/models/queryFilter';
import { filterSelectedState } from '../states';

export const useFilterSelectedState = (name: QueryFilterName) => useRecoilState(filterSelectedState(name));
