import { useRecoilValue } from 'recoil';
import { selectedFilterNamesState } from '../states';

export const useSelectedFilterNames = () => useRecoilValue(selectedFilterNamesState);
