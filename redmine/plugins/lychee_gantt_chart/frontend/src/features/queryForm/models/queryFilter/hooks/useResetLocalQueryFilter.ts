import { useRecoilCallback } from 'recoil';
import { issueQueryState } from '@/models/query';
import { normalizeQueryFilterValue } from '@/models/queryFilter';
import { optionalQueryState, queryFilterConditionState, selectedFilterNamesState } from '../states';

export const useResetLocalQueryFilter = () => {
  return useRecoilCallback(({ set, snapshot }) => {
    return () => {
      const {
        f: fieldNames,
        op: operatorsByName,
        v: valuesByName,
        groupBy,
        includeAncestorIssues,
      } = snapshot.getLoadable(issueQueryState).getValue();

      set(selectedFilterNamesState, fieldNames);

      fieldNames.forEach((name) => {
        set(queryFilterConditionState(name), (current) => {
          const value = normalizeQueryFilterValue(valuesByName[name]);

          return {
            ...current,
            operator: operatorsByName[name],
            value,
            multiple: value.length > 1,
          };
        });
      });

      set(optionalQueryState, { groupBy, includeAncestorIssues });
    };
  }, []);
};
