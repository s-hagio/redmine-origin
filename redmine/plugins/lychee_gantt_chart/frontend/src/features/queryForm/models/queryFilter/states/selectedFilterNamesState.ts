import { atom } from 'recoil';
import { QueryFilterName } from '@/models/queryFilter';

export const selectedFilterNamesState = atom<QueryFilterName[]>({
  key: 'queryForm/selectedFilterNames',
});
