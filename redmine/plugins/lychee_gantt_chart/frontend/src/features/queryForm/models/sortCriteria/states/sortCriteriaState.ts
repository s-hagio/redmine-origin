import { selector } from 'recoil';
import { SortCriteria } from '@/models/query';
import { sortOrderState } from './sortOrderState';

export const sortCriteriaState = selector<SortCriteria>({
  key: 'queryForm/sortCriteria',
  get: ({ get }) => {
    const sortCriteria: SortCriteria = {};

    ([0, 1, 2] as (0 | 1 | 2)[]).forEach((index) => {
      const sortOrder = get(sortOrderState(index));

      if (sortOrder && sortOrder[0] && sortOrder[1]) {
        sortCriteria[index] = sortOrder;
      }
    });

    return sortCriteria;
  },
});
