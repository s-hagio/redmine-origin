import { selectorFamily } from 'recoil';
import { QueryFilterName } from '@/models/queryFilter';
import { selectedFilterNamesState } from './selectedFilterNamesState';
import { queryFilterConditionState } from './queryFilterConditionState';

export const filterSelectedState = selectorFamily<boolean, QueryFilterName>({
  key: 'queryForm/filterSelected',
  get: (name) => {
    return ({ get }) => {
      const selectedFieldNames = get(selectedFilterNamesState);

      return selectedFieldNames.includes(name);
    };
  },
  set: (name) => {
    return ({ set, reset }, selected) => {
      set(selectedFilterNamesState, (currentFieldNames) => {
        if (!selected) {
          return currentFieldNames.filter((fieldName) => fieldName !== name);
        }

        return [...currentFieldNames, name];
      });

      reset(queryFilterConditionState(name));
    };
  },
});
