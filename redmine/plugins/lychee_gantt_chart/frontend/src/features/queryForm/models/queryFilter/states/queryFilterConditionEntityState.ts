import { atomFamily, selectorFamily } from 'recoil';
import { availableQueryFilterState, QueryFilterName } from '@/models/queryFilter';
import { QueryFilterCondition } from '../types';
import { queryFilterOperatorsMapState } from './queryFilterOperatorsMapState';

export const queryFilterConditionEntityState = atomFamily<QueryFilterCondition, QueryFilterName>({
  key: 'queryForm/queryFilterConditionEntity',
  default: selectorFamily({
    key: 'queryForm/queryFilterConditionEntity/default',
    get: (name) => {
      return ({ get }) => {
        const queryFilterOperatorMap = get(queryFilterOperatorsMapState);
        const filter = get(availableQueryFilterState(name));

        return {
          operator: queryFilterOperatorMap[filter.type][0],
          value: [''],
          multiple: false,
          disabled: false,
        };
      };
    },
  }),
});
