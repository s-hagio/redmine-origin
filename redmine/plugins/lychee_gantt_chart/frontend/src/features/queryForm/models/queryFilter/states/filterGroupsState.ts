import { selector } from 'recoil';
import { QueryFilter } from '@/models/queryFilter';
import { settingsState } from '@/models/setting';
import { FilterGroup, FilterGroupLabel } from '../types';

export const filterGroupsState = selector<FilterGroup[]>({
  key: 'queryForm/filterGroups',
  get: ({ get }) => {
    const { availableQueryFilters } = get(settingsState);

    const map = new Map<FilterGroupLabel, QueryFilter[]>([[null, []]]);

    availableQueryFilters.forEach((filter) => {
      map.set(filter.groupLabel, [...(map.get(filter.groupLabel) ?? []), filter]);
    });

    return [...map.keys()].map((label) => ({
      label,
      filters: map.get(label) ?? [],
    }));
  },
});
