import { renderHook } from '@testing-library/react';
import { QUERY_FORM_DIALOG_ID } from '../../constants';
import { useOpenQueryForm } from '../useOpenQueryForm';

const openDialogMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/dialog', () => ({
  useOpenDialog: vi.fn((id) => (id === QUERY_FORM_DIALOG_ID ? openDialogMock : null)),
}));

const resetLocalQueryFilterMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/queryFilter', () => ({
  useResetLocalQueryFilter: vi.fn().mockReturnValue(resetLocalQueryFilterMock),
}));

const resetLocalSortCriteriaMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/sortCriteria', () => ({
  useResetLocalSortCriteria: vi.fn().mockReturnValue(resetLocalSortCriteriaMock),
}));

test('ローカルstateのリセットを実行してからQueryFormを開く', () => {
  const { result } = renderHook(() => useOpenQueryForm());

  expect(resetLocalQueryFilterMock).not.toBeCalled();
  expect(resetLocalSortCriteriaMock).not.toBeCalled();
  expect(openDialogMock).not.toBeCalled();

  result.current();

  expect(resetLocalQueryFilterMock).toBeCalled();
  expect(resetLocalSortCriteriaMock).toBeCalled();
  expect(openDialogMock).toBeCalled();
});
