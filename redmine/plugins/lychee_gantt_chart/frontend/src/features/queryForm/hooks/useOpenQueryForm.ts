import { useCallback } from 'react';
import { useOpenDialog } from '@/features/dialog';
import { QUERY_FORM_DIALOG_ID } from '../constants';
import { useResetLocalQueryFilter } from '../models/queryFilter';
import { useResetLocalSortCriteria } from '../models/sortCriteria';

export const useOpenQueryForm = () => {
  const openDialog = useOpenDialog(QUERY_FORM_DIALOG_ID);
  const resetLocalQueryFilter = useResetLocalQueryFilter();
  const resetLocalSortCriteria = useResetLocalSortCriteria();

  return useCallback(() => {
    resetLocalQueryFilter();
    resetLocalSortCriteria();
    openDialog();
  }, [openDialog, resetLocalQueryFilter, resetLocalSortCriteria]);
};
