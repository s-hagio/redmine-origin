import { DialogID } from '@/features/dialog';

export const QUERY_FORM_DIALOG_ID: DialogID = 'queryForm/mainDialog';
export const FILTER_SELECT_DIALOG_ID: DialogID = 'queryForm/filterSelectDialog';

export const BLANK_VALUE = [''];
