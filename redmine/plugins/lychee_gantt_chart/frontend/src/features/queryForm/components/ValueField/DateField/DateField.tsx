import * as React from 'react';
import { QueryFilterValue } from '@/models/queryFilter';
import { useQueryFilterConditionState } from '../../../models/queryFilter';
import { TypedFieldProps } from '../types';
import { BLANK_VALUE } from '../../../constants';
import { FilterValueType, useFilterValueType } from '../../../models/filterValue';
import DateRangeFirstField from './DateRangeFirstField';
import DateRangeField from './DateRangeField';
import DaysField from './DaysField';

type Props = TypedFieldProps;

const DateField: React.FC<Props> = ({ name }) => {
  const [{ value }, setCondition] = useQueryFilterConditionState(name);

  const filterValueType = useFilterValueType(name);
  const prevFilterValueTypeRef = React.useRef<FilterValueType>(filterValueType);

  React.useEffect(() => {
    const getNormalizedValue = (value: QueryFilterValue): QueryFilterValue => {
      const prevWasDaysType = prevFilterValueTypeRef.current === FilterValueType.Days;

      if (filterValueType === FilterValueType.Single) {
        return prevWasDaysType ? BLANK_VALUE : value;
      }

      if (filterValueType === FilterValueType.Range) {
        return prevWasDaysType ? BLANK_VALUE : value;
      }

      if (filterValueType === FilterValueType.Days) {
        return !prevWasDaysType ? BLANK_VALUE : value;
      }

      return value;
    };

    setCondition(({ value, ...current }) => ({ ...current, value: getNormalizedValue(value) }));
    prevFilterValueTypeRef.current = filterValueType;
  }, [filterValueType, setCondition]);

  switch (filterValueType) {
    case FilterValueType.Single:
      return <DateRangeFirstField name={name} value={value} />;
    case FilterValueType.Range:
      return <DateRangeField name={name} value={value} />;
    case FilterValueType.Days:
      return <DaysField name={name} value={value} />;
    default:
      return null;
  }
};

export default React.memo(DateField);
