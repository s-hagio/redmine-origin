import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { useClearIssueQuery } from '@/models/query';
import { useDialog } from '@/features/dialog';
import { NakedButton } from '@/components/Button';
import { Icon, RedmineIcon } from '@/components/Icon';
import { getZIndex, linkColor } from '@/styles';
import { QUERY_FORM_DIALOG_ID } from '../constants';
import { useReflectIssueQuery } from '../models/queryFilter';
import { FilterFieldTable } from './FilterField';
import { FilterSelectContainer } from './FilterSelect';
import { GroupColumnSelect } from './GroupColumnSelect';
import { IncludeAncestorIssuesField } from './IncludeAncestorIssuesField';
import { SortCriteriaSelect } from './SortCriteriaSelect';

const QueryForm: React.FC = () => {
  const { t } = useTranslation();

  const [Dialog, { close }] = useDialog(QUERY_FORM_DIALOG_ID);

  const reflectIssueQuery = useReflectIssueQuery();
  const handleApplyButtonClick = React.useCallback(() => {
    close();
    reflectIssueQuery();
  }, [close, reflectIssueQuery]);

  const clearIssueQuery = useClearIssueQuery();
  const handleClearButtonClick = React.useCallback(() => {
    close();
    clearIssueQuery();
  }, [close, clearIssueQuery]);

  return (
    <Dialog className={style} alwaysRenderContainer disableCloseOnOutsideClick>
      {({ isVisible }) => (
        <React.Suspense fallback={null}>
          <div className={toolbarStyle}>
            <FilterSelectContainer />

            <NakedButton onClick={close}>
              <Icon name="close" size={32} />
            </NakedButton>
          </div>

          {isVisible && (
            <div className={bodyStyle}>
              <FilterFieldTable />
              <GroupColumnSelect />
              <IncludeAncestorIssuesField />
              <SortCriteriaSelect />
            </div>
          )}

          <ul className={footerButtonListStyle}>
            <li>
              <NakedButton onClick={handleApplyButtonClick}>
                <RedmineIcon name="check" width={15} />
                {t('label.apply')}
              </NakedButton>
            </li>

            <li>
              <NakedButton onClick={handleClearButtonClick}>
                <RedmineIcon name="reload" width={16} />
                {t('label.clear')}
              </NakedButton>
            </li>
          </ul>
        </React.Suspense>
      )}
    </Dialog>
  );
};

export default React.memo(QueryForm);

export const style = css`
  position: fixed;
  top: 0;
  left: -100%;
  min-width: 650px;
  max-width: 100vw;
  height: 100vh;
  padding: 1rem 1.5rem 5rem;
  background: #fff;
  z-index: ${getZIndex('queryForm/QueryForm')};
  overflow: auto;
  box-shadow: rgba(0, 0, 0, 0.2) 3px 0 8px;
  transition: left 0.15s ease-out;

  &[data-visible='true'] {
    left: 0;
  }

  @media screen and (max-width: 899px) {
    top: 64px; // Redmine #header height
  }
`;

const toolbarStyle = css`
  display: flex;
  align-items: center;
  margin-bottom: 1rem;

  button {
    margin-left: auto;
  }
`;

const bodyStyle = css`
  display: flex;
  flex-direction: column;
  row-gap: 1.5rem;
`;

const footerButtonListStyle = css`
  display: flex;
  column-gap: 0.85em;
  align-items: center;
  list-style: none;
  margin: 1.5rem 0 0;
  padding: 0;

  button {
    color: ${linkColor};
    &:hover {
      text-decoration: underline;
    }
  }

  img {
    margin-right: 2px;
  }
`;
