import * as React from 'react';
import { css } from '@linaria/core';
import { QueryFilterName, useAvailableQueryFilter } from '@/models/queryFilter';
import { useQueryFilterConditionState } from '../../models/queryFilter';
import OperatorField from '../OperatorField';
import ValueField from '../ValueField';

type Props = {
  name: QueryFilterName;
};

const FilterFieldRow: React.FC<Props> = ({ name }) => {
  const { label, type } = useAvailableQueryFilter(name);
  const [{ disabled }, setCondition] = useQueryFilterConditionState(name);

  const toggleEnabled = React.useCallback(() => {
    setCondition((current) => ({ ...current, disabled: !current.disabled }));
  }, [setCondition]);

  return (
    <tr className={style} data-disabled={disabled}>
      <td>
        <label className={labelStyle}>
          <input type="checkbox" checked={!disabled} onChange={toggleEnabled} />
          {label}
        </label>
      </td>

      <td>
        {!disabled && (
          <React.Suspense fallback={null}>
            <OperatorField name={name} type={type} />
          </React.Suspense>
        )}
      </td>

      <td>
        {!disabled && (
          <React.Suspense fallback={null}>
            <ValueField name={name} type={type} />
          </React.Suspense>
        )}
      </td>
    </tr>
  );
};

export default React.memo(FilterFieldRow);

const style = css`
  td {
    padding-right: 1em;
  }

  td:nth-of-type(1) {
    width: 200px;
  }
`;

const labelStyle = css`
  display: flex;
  align-items: center;

  input[type='checkbox'] {
    margin-right: 0.35em;
  }
`;
