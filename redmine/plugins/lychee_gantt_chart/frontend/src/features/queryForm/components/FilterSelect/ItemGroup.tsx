import * as React from 'react';
import { css } from '@linaria/core';
import { FilterGroup } from '../../models/queryFilter';
import Item from './Item';

type Props = FilterGroup;

const ItemGroup: React.FC<Props> = ({ label, filters }) => {
  return (
    <div className={style}>
      {label && <div className={labelStyle}>{label}</div>}

      <ul>
        {filters.map((filter) => (
          <li key={filter.name}>
            <Item {...filter} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ItemGroup;

const style = css`
  :not(:first-of-type) {
    margin-top: 0.75rem;
  }

  ul {
    margin: 0;
    padding: 0;
    list-style: none;
    white-space: nowrap;
  }
`;

const labelStyle = css`
  font-weight: bold;
`;
