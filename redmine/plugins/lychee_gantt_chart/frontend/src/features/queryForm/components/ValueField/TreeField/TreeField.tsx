import * as React from 'react';
import { IssueIdFieldInput } from '@/features/field';
import { useQueryFilterConditionState } from '../../../models/queryFilter';
import { TypedFieldProps } from '../types';
import { BLANK_VALUE } from '../../../constants';
import {
  FilterValueType,
  useFilterValueChangeEventHandlers,
  useFilterValueType,
} from '../../../models/filterValue';

type Props = TypedFieldProps;

const TreeField: React.FC<Props> = ({ name }) => {
  const { handleValueChange } = useFilterValueChangeEventHandlers(name);
  const [{ value }, setCondition] = useQueryFilterConditionState(name);

  const filterValueType = useFilterValueType(name);
  const isWithoutInput = filterValueType === FilterValueType.None;

  React.useEffect(() => {
    if (isWithoutInput) {
      setCondition((current) => ({ ...current, value: BLANK_VALUE }));
    }
  }, [isWithoutInput, setCondition]);

  if (isWithoutInput) {
    return null;
  }

  return <IssueIdFieldInput value={value[0]} onChange={handleValueChange} required />;
};

export default React.memo(TreeField);
