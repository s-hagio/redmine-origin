import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { useOptionalQueryState } from '../../models/queryFilter';
import { useGroupableFields } from '../../models/groupField';

const GroupColumnSelect: React.FC = () => {
  const { t } = useTranslation();
  const [{ groupBy }, setOptionalQuery] = useOptionalQueryState();
  const groupableFields = useGroupableFields();

  const handleChange = React.useCallback(
    ({ target }: React.ChangeEvent<HTMLSelectElement>) => {
      setOptionalQuery((current) => ({
        ...current,
        groupBy: target.selectedOptions[0].value || null,
      }));
    },
    [setOptionalQuery]
  );

  return (
    <div className={style}>
      {t('field.groupBy')}

      <select value={groupBy ?? ''} onChange={handleChange}>
        <option value="" />

        {groupableFields.map(({ name, label }) => (
          <option key={name} value={name}>
            {label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default React.memo(GroupColumnSelect);

const style = css`
  display: flex;
  align-items: center;
  select {
    margin-left: 0.5rem;
  }
`;
