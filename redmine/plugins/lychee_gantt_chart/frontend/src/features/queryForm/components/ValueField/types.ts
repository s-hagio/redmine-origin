import { QueryFilterName } from '@/models/queryFilter';

export type TypedFieldProps = {
  name: QueryFilterName;
};
