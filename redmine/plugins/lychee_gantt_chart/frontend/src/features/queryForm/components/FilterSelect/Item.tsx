import * as React from 'react';
import { css } from '@linaria/core';
import { QueryFilter } from '@/models/queryFilter';
import { useFilterSelectedState } from '../../models/queryFilter';

type Props = QueryFilter;

const Item: React.FC<Props> = ({ name, label }) => {
  const [isSelected, setSelected] = useFilterSelectedState(name);

  const toggleSelected = React.useCallback(() => {
    setSelected((selected) => !selected);
  }, [setSelected]);

  return (
    <label className={style}>
      <input type="checkbox" value={name} checked={isSelected} onChange={toggleSelected} />
      {label}
    </label>
  );
};

export default Item;

const style = css`
  display: flex;
  align-items: center;

  input {
    margin-right: 0.25rem;
  }
`;
