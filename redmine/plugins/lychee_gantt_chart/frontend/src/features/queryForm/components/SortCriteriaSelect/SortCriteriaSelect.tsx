import * as React from 'react';
import { css } from '@linaria/core';
import SortOrderSelect from './SortOrderSelect';

const SortCriteriaSelect: React.FC = () => {
  return (
    <div className={style}>
      <SortOrderSelect index={0} />
      <SortOrderSelect index={1} />
      <SortOrderSelect index={2} />
    </div>
  );
};

export default React.memo(SortCriteriaSelect);

const style = css``;
