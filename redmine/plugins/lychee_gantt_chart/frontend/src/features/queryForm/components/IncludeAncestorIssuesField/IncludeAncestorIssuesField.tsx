import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { useOptionalQueryState } from '../../models/queryFilter';

const IncludeAncestorIssuesField: React.FC = () => {
  const { t } = useTranslation();
  const [{ includeAncestorIssues }, setOptionalQuery] = useOptionalQueryState();

  const toggle = React.useCallback(() => {
    setOptionalQuery((current) => ({
      ...current,
      includeAncestorIssues: !current.includeAncestorIssues,
    }));
  }, [setOptionalQuery]);

  return (
    <div className={style}>
      <label>
        <input type="checkbox" checked={includeAncestorIssues} onChange={toggle} />
        {t('label.includeAncestorIssues')}
      </label>
    </div>
  );
};

export default React.memo(IncludeAncestorIssuesField);

const style = css`
  label {
    display: flex;
    align-items: center;
    column-gap: 4px;
  }
`;
