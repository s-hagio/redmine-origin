import * as React from 'react';
import { render } from '@testing-library/react';
import { QueryFilterOption } from '@/models/queryFilter';
import OptionList from '../OptionList';

const options: QueryFilterOption[] = [
  ['label1', 'value1'],
  ['label2', 'value2'],
];

test('Render', () => {
  const { container } = render(<OptionList options={options} />);

  expect(container).toMatchSnapshot();
});
