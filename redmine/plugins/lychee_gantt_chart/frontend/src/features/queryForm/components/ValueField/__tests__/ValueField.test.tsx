import * as React from 'react';
import { render } from '@testing-library/react';
import { QueryFilterType } from '@/models/queryFilter';
import ValueField from '../ValueField';

vi.mock('../ListField', () => ({ default: vi.fn((props) => `ListField: ${JSON.stringify(props)}`) }));
vi.mock('../DateField', () => ({ default: vi.fn((props) => `DateField: ${JSON.stringify(props)}`) }));
vi.mock('../StringField', () => ({ default: vi.fn((props) => `StringField: ${JSON.stringify(props)}`) }));
vi.mock('../NumberField', () => ({ default: vi.fn((props) => `NumberField: ${JSON.stringify(props)}`) }));
vi.mock('../RelationField', () => ({ default: vi.fn((props) => `RelationField: ${JSON.stringify(props)}`) }));
vi.mock('../TreeField', () => ({ default: vi.fn((props) => `TreeField: ${JSON.stringify(props)}`) }));

test.each(Object.values(QueryFilterType))('Render: %s', (type) => {
  const { container } = render(<ValueField type={type} name="name!!" />);

  expect(container).toMatchSnapshot();
});
