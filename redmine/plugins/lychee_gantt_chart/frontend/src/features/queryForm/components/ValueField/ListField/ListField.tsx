import * as React from 'react';
import { css } from '@linaria/core';
import { QueryFilterOption, QueryFilterOptionGroupLabel, useQueryFilterOptions } from '@/models/queryFilter';
import { NakedButton } from '@/components/Button';
import { RedmineIcon } from '@/components/Icon';
import { useQueryFilterConditionState } from '../../../models/queryFilter';
import { FilterValueType, useFilterValueType } from '../../../models/filterValue';
import { TypedFieldProps } from '../types';
import OptionList from '../OptionList';

type Props = TypedFieldProps;

const ListField: React.FC<Props> = ({ name }) => {
  const [{ value, multiple }, setCondition] = useQueryFilterConditionState(name);
  const options = useQueryFilterOptions(name);

  const filterValueType = useFilterValueType(name);
  const isWithoutInput = filterValueType === FilterValueType.None;

  const groupedOptions = React.useMemo(() => {
    if (!options || isWithoutInput) {
      return null;
    }

    const map = options.reduce(
      (memo, option) => {
        const normalizedOption: QueryFilterOption = Array.isArray(option) ? option : [option, option];
        const groupName = normalizedOption[2] ?? null;
        const groupedOptions = memo.get(groupName) ?? [];

        groupedOptions.push(normalizedOption);
        memo.set(groupName, groupedOptions);

        return memo;
      },
      new Map<QueryFilterOptionGroupLabel | null, QueryFilterOption[]>([[null, []]])
    );

    return [...map];
  }, [isWithoutInput, options]);

  const handleChange = React.useCallback(
    ({ target }: React.ChangeEvent<HTMLSelectElement>) => {
      setCondition((current) => ({
        ...current,
        value: [...target.selectedOptions].map(({ value }) => value),
      }));
    },
    [setCondition]
  );

  const toggleMultiple = React.useCallback(() => {
    setCondition((current) => ({ ...current, multiple: !current.multiple }));
  }, [setCondition]);

  React.useEffect(() => {
    setCondition(({ value, ...current }) => {
      const firstOption = (options || [])[0] ?? [''];

      return {
        ...current,
        value: value.filter(Boolean).length ? value : [firstOption ? firstOption[1] : ''],
      };
    });
  }, [options, setCondition]);

  if (!groupedOptions) {
    return null;
  }

  return (
    <div className={style}>
      <select value={multiple ? value : value[0]} multiple={multiple} onChange={handleChange} required>
        {groupedOptions.map(([groupLabel, options]) =>
          groupLabel != null ? (
            <optgroup key={groupLabel} label={groupLabel}>
              <OptionList options={options} />
            </optgroup>
          ) : (
            <OptionList key={groupLabel} options={options} />
          )
        )}
      </select>

      <NakedButton type="button" onClick={toggleMultiple}>
        <RedmineIcon name={multiple ? 'toggleMinus' : 'togglePlus'} />
      </NakedButton>
    </div>
  );
};

export default React.memo(ListField);

const style = css`
  display: flex;
  align-items: center;

  select {
    margin-right: 2px;

    &[multiple] {
      height: 10em;
    }
  }
`;
