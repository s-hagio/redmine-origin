import * as React from 'react';
import { render } from '@testing-library/react';
import SortCriteriaSelect from '../SortCriteriaSelect';

vi.mock('../SortOrderSelect', () => ({
  default: vi.fn((props) => `SortOrderSelect: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<SortCriteriaSelect />);

  expect(container).toMatchSnapshot();
});
