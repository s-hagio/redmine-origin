import * as React from 'react';
import { QueryFilterName, QueryFilterValue } from '@/models/queryFilter';
import { DateFieldInput } from '@/features/field';
import { useFilterValueChangeEventHandlers } from '../../../models/filterValue';

type Props = {
  name: QueryFilterName;
  value: QueryFilterValue;
};

const DateRangeFirstField: React.FC<Props> = ({ name, value }) => {
  const { handleFirstValueChange } = useFilterValueChangeEventHandlers(name);

  return <DateFieldInput value={value[0]} onChange={handleFirstValueChange} />;
};

export default DateRangeFirstField;
