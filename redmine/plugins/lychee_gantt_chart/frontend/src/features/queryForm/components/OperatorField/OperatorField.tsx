import * as React from 'react';
import { css } from '@linaria/core';
import { QueryFilterName, QueryFilterOperator, QueryFilterType } from '@/models/queryFilter';
import {
  useQueryFilterOperators,
  useQueryFilterConditionState,
  useQueryFilterOperatorLabelMap,
} from '../../models/queryFilter';

type Props = {
  name: QueryFilterName;
  type: QueryFilterType;
};

const OperatorField: React.FC<Props> = ({ name, type }) => {
  const operators = useQueryFilterOperators(type);
  const operatorLabelMap = useQueryFilterOperatorLabelMap();
  const [{ operator }, setCondition] = useQueryFilterConditionState(name);

  const handleChange = React.useCallback(
    ({ target }: React.ChangeEvent<HTMLSelectElement>) => {
      setCondition((current) => ({
        ...current,
        operator: target.selectedOptions[0].value as QueryFilterOperator,
      }));
    },
    [setCondition]
  );

  return (
    <select className={style} value={operator} onChange={handleChange}>
      {operators.map((op) => (
        <option key={op} value={op}>
          {operatorLabelMap[op]}
        </option>
      ))}
    </select>
  );
};

export default React.memo(OperatorField);

const style = css`
  max-width: 100%;
`;
