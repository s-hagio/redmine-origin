import * as React from 'react';
import { css } from '@linaria/core';
import { QueryFilterName, QueryFilterValue } from '@/models/queryFilter';
import { DateFieldInput } from '@/features/field';
import { useFilterValueChangeEventHandlers } from '../../../models/filterValue';

type Props = {
  name: QueryFilterName;
  value: QueryFilterValue;
};

const DateRangeField: React.FC<Props> = ({ name, value }) => {
  const { handleFirstValueChange, handleSecondValueChange } = useFilterValueChangeEventHandlers(name);

  return (
    <>
      <DateFieldInput value={value[0]} onChange={handleFirstValueChange} />
      <span className={symbolStyle}>~</span>
      <DateFieldInput value={value[1]} min={value[0]} onChange={handleSecondValueChange} />
    </>
  );
};

export default DateRangeField;

const symbolStyle = css`
  margin: 0 0.25em;
`;
