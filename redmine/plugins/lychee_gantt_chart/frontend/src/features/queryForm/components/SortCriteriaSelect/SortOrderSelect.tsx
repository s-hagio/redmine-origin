import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { SortDirection, sortDirections } from '@/models/query';
import { useSortableFields } from '@/features/sort';
import { SortCriteriaIndex, useSortOrderState } from '../../models/sortCriteria';

type Props = {
  index: SortCriteriaIndex;
};

const isSortDirection = (direction: string): direction is SortDirection => {
  return (sortDirections as ReadonlyArray<string>).includes(direction);
};

const SortOrderSelect: React.FC<Props> = ({ index }) => {
  const { t } = useTranslation();

  const sortableFields = useSortableFields();
  const [[column, direction], setSortOrder] = useSortOrderState(index);

  const handleColumnChange = React.useCallback(
    ({ target }: React.ChangeEvent<HTMLSelectElement>) => {
      setSortOrder(([, direction]) => [target.value, direction]);
    },
    [setSortOrder]
  );

  const handleDirectionChange = React.useCallback(
    ({ target }: React.ChangeEvent<HTMLSelectElement>) => {
      setSortOrder(([column]) => [column, isSortDirection(target.value) ? target.value : '']);
    },
    [setSortOrder]
  );

  return (
    <div className={style}>
      <select value={column} onChange={handleColumnChange}>
        <option value="" />

        {sortableFields.map(({ name, label }) => (
          <option key={name} value={name}>
            {label}
          </option>
        ))}
      </select>

      <select value={direction} onChange={handleDirectionChange}>
        <option value="" />

        {sortDirections.map((direction) => (
          <option key={direction} value={direction}>
            {t(`label.sortDirection.${direction}`)}
          </option>
        ))}
      </select>
    </div>
  );
};

export default SortOrderSelect;

const style = css`
  display: flex;
  align-items: center;
  gap: 10px;
`;
