import * as React from 'react';
import { css } from '@linaria/core';
import { StringFieldInput } from '@/features/field';
import { useQueryFilterConditionState } from '../../../models/queryFilter';
import {
  FilterValueType,
  useFilterValueType,
  useFilterValueChangeEventHandlers,
} from '../../../models/filterValue';
import { BLANK_VALUE } from '../../../constants';
import { TypedFieldProps } from '../types';

type Props = TypedFieldProps;

const StringField: React.FC<Props> = ({ name }) => {
  const { handleValueChange } = useFilterValueChangeEventHandlers(name);
  const [{ value }, setCondition] = useQueryFilterConditionState(name);

  const filterValueType = useFilterValueType(name);
  const isWithoutInput = filterValueType === FilterValueType.None;

  React.useEffect(() => {
    if (isWithoutInput) {
      setCondition((current) => ({ ...current, value: BLANK_VALUE }));
    }
  }, [isWithoutInput, setCondition]);

  if (isWithoutInput) {
    return null;
  }

  return <StringFieldInput className={style} value={value[0]} onChange={handleValueChange} required />;
};

export default React.memo(StringField);

const style = css`
  width: 20em;
`;
