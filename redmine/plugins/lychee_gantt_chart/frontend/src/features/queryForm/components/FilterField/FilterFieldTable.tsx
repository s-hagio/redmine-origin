import * as React from 'react';
import { css } from '@linaria/core';
import { useSelectedFilterNames } from '../../models/queryFilter';
import FilterFieldRow from './FilterFieldRow';

const FilterFieldTable: React.FC = () => {
  const filterNames = useSelectedFilterNames();

  return (
    <table className={style}>
      <tbody>
        {filterNames.map((name) => (
          <FilterFieldRow key={name} name={name} />
        ))}
      </tbody>
    </table>
  );
};

export default FilterFieldTable;

const style = css``;
