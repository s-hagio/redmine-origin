import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { Dialog, useOpenDialog } from '@/features/dialog';
import { SecondaryButton } from '@/components/Button';
import { Icon } from '@/components/Icon';
import { borderColor } from '@/styles';
import { FILTER_SELECT_DIALOG_ID } from '../../constants';
import FilterSelect from './FilterSelect';

const FilterSelectContainer: React.FC = () => {
  const { t } = useTranslation();
  const open = useOpenDialog(FILTER_SELECT_DIALOG_ID);

  return (
    <div className={style}>
      <SecondaryButton type="button" onClick={open}>
        <Icon name="add" />
        {t('label.selectFilter')}
      </SecondaryButton>

      <Dialog id={FILTER_SELECT_DIALOG_ID} className={dialogStyle}>
        <FilterSelect />
      </Dialog>
    </div>
  );
};

export default React.memo(FilterSelectContainer);

const style = css`
  position: relative;
  margin-right: 1rem;
`;

const dialogStyle = css`
  position: absolute;
  left: 0;
  max-height: calc(100vh - 5rem);
  padding: 0.5rem;
  border: 1px solid ${borderColor};
  background: #fff;
  z-index: 2;
  box-shadow: rgba(0, 0, 0, 0.15) 1px 1px 8px;
  overflow-y: scroll;
`;
