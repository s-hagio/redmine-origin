import * as React from 'react';
import { QueryFilterName, QueryFilterType } from '@/models/queryFilter';
import { TypedFieldProps } from './types';
import ListField from './ListField';
import TreeField from './TreeField';
import DateField from './DateField';
import StringField from './StringField';
import NumberField from './NumberField';
import RelationField from './RelationField';

type Props = {
  name: QueryFilterName;
  type: QueryFilterType;
};

const ComponentsByType: Record<QueryFilterType, React.FC<TypedFieldProps>> = {
  [QueryFilterType.List]: ListField,
  [QueryFilterType.ListWithHistory]: ListField,
  [QueryFilterType.ListStatus]: ListField,
  [QueryFilterType.ListOptional]: ListField,
  [QueryFilterType.ListOptionalWithHistory]: ListField,
  [QueryFilterType.ListSubprojects]: ListField,
  [QueryFilterType.Date]: DateField,
  [QueryFilterType.DatePast]: DateField,
  [QueryFilterType.String]: StringField,
  [QueryFilterType.Text]: StringField,
  [QueryFilterType.Integer]: NumberField,
  [QueryFilterType.Float]: NumberField,
  [QueryFilterType.Relation]: RelationField,
  [QueryFilterType.Tree]: TreeField,
} as const;

const ValueField: React.FC<Props> = ({ name, type }) => {
  return React.createElement(ComponentsByType[type], { name })
};

export default React.memo(ValueField);
