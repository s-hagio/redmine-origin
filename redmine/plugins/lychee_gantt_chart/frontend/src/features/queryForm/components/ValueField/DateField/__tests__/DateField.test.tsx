import * as React from 'react';
import { RecoilRoot } from 'recoil';
import { renderWithTranslation } from '@/test-utils';
import { QueryFilterOperator, QueryFilterType } from '@/models/queryFilter';
import { settingsState } from '@/models/setting';
import { QueryFilterCondition, queryFilterConditionState } from '../../../../models/queryFilter';
import DateField from '../DateField';

vi.mock('../DateRangeFirstField', () => ({
  default: vi.fn((props) => `DateRangeFirstField: ${JSON.stringify(props)}`),
}));
vi.mock('../DateRangeField', () => ({
  default: vi.fn((props) => `DateRangeField: ${JSON.stringify(props)}`),
}));
vi.mock('../DaysField', () => ({
  default: vi.fn((props) => `DaysField: ${JSON.stringify(props)}`),
}));

const condition: QueryFilterCondition = {
  operator: '=',
  value: ['value1', 'value2'],
  multiple: true,
  disabled: false,
};

test.each<QueryFilterOperator>([
  '=',
  '>=',
  '<=',
  '><',
  '<t+',
  '>t+',
  '><t+',
  't+',
  'nd',
  't',
  'ld',
  'nw',
  'w',
  'lw',
  'l2w',
  'nm',
  'm',
  'lm',
  'y',
  '>t-',
  '<t-',
  '><t-',
  't-',
  '!*',
  '*',
])('Render: %s', (operator) => {
  const { container } = renderWithTranslation(
    <RecoilRoot
      initializeState={({ set }) => {
        set(settingsState, (current) => ({
          ...current,
          availableQueryFilters: [
            {
              name: 'start_date',
              type: QueryFilterType.Date,
              label: '開始日',
              groupLabel: '日付',
              remote: false,
              values: null,
            },
          ],
        }));

        set(queryFilterConditionState('start_date'), { ...condition, operator });
      }}
    >
      <DateField name="start_date" />
    </RecoilRoot>
  );

  expect(container).toMatchSnapshot();
});

test.todo('operatorの変更時にvalueを正規化する');
