import * as React from 'react';
import { useRecoilValue } from 'recoil';
import { fireEvent } from '@testing-library/react';
import { renderWithRecoil, renderWithRecoilHook } from '@/test-utils';
import { SortOrder } from '@/models/query';
import { TranslationProvider } from '@/providers/translationProvider';
import SortOrderSelect from '../SortOrderSelect';
import { sortOrderState } from '../../../models/sortCriteria';

const useSortableFieldsMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/sort', () => ({
  useSortableFields: useSortableFieldsMock,
}));

const sortableFields = [
  { name: 'id', label: '#' },
  { name: 'start_date', label: '開始日' },
];

beforeEach(() => {
  useSortableFieldsMock.mockReturnValue(sortableFields);
});

test('Render', () => {
  const { container } = renderWithRecoil(
    <TranslationProvider lang="ja">
      <SortOrderSelect index={0} />
    </TranslationProvider>
  );

  expect(container).toMatchSnapshot();
});

test('SortOrderの条件にマッチしている選択肢を選択状態にする', () => {
  const { container } = renderWithRecoil(
    <TranslationProvider lang="ja">
      <SortOrderSelect index={1} />
    </TranslationProvider>,
    {
      initializeState: ({ set }) => {
        set(sortOrderState(1), ['start_date', 'desc'] as SortOrder);
      },
    }
  );

  const [columnSelect, directionSelect] = container.querySelectorAll('select');

  expect(columnSelect.value).toBe('start_date');
  expect(directionSelect.value).toBe('desc');
});

test('各種selectでSortOrderの条件を変更する', () => {
  const { result, container } = renderWithRecoilHook(
    <SortOrderSelect index={0} />,
    () => ({
      sortOrder: useRecoilValue(sortOrderState(0)),
    }),
    {
      initializeState: ({ set }) => {
        set(sortOrderState(0), ['', ''] as SortOrder);
      },
    }
  );
  const [columnSelect, directionSelect] = container.querySelectorAll('select');

  expect(result.current.sortOrder).toEqual(['', '']);

  fireEvent.change(columnSelect, { target: { value: 'id' } });
  expect(result.current.sortOrder).toEqual(['id', '']);

  fireEvent.change(directionSelect, { target: { value: 'desc' } });
  expect(result.current.sortOrder).toEqual(['id', 'desc']);

  fireEvent.change(directionSelect, { target: { value: 'asc' } });
  expect(result.current.sortOrder).toEqual(['id', 'asc']);

  fireEvent.change(columnSelect, { target: { value: '' } });
  expect(result.current.sortOrder).toEqual(['', 'asc']);
});
