import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { QueryFilterName, QueryFilterValue } from '@/models/queryFilter';
import { NumberFieldInput } from '@/features/field';
import { useFilterValueChangeEventHandlers } from '../../../models/filterValue';

type Props = {
  name: QueryFilterName;
  value: QueryFilterValue;
};

const DaysField: React.FC<Props> = ({ name, value }) => {
  const { t } = useTranslation();
  const { handleValueChange } = useFilterValueChangeEventHandlers(name);

  return (
    <>
      <NumberFieldInput
        className={inputStyle}
        value={value[0]}
        min="0"
        onChange={handleValueChange}
        required
      />
      {t('label.dayPlural')}
    </>
  );
};

export default DaysField;

const inputStyle = css`
  width: 5em;
  margin-right: 0.25em;
`;
