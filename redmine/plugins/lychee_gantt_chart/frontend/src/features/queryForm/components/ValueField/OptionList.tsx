import * as React from 'react';
import { QueryFilterOption } from '@/models/queryFilter';

type Props = {
  options: QueryFilterOption[];
};

const OptionList: React.FC<Props> = ({ options }) => {
  return (
    <>
      {options.map(([label, value]) => (
        <option key={value} value={value}>
          {label}
        </option>
      ))}
    </>
  );
};

export default React.memo(OptionList);
