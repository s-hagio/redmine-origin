import * as React from 'react';
import { IssueIdFieldInput } from '@/features/field';
import { TypedFieldProps } from '../types';
import { useQueryFilterConditionState } from '../../../models/queryFilter';
import {
  FilterValueType,
  useFilterValueType,
  useFilterValueChangeEventHandlers,
} from '../../../models/filterValue';
import { BLANK_VALUE } from '../../../constants';
import ListField from '../ListField';

type Props = TypedFieldProps;

const RelationField: React.FC<Props> = ({ name }) => {
  const { handleValueChange } = useFilterValueChangeEventHandlers(name);
  const [{ operator, value }, setCondition] = useQueryFilterConditionState(name);

  const filterValueType = useFilterValueType(name);
  const prevFilterValueTypeRef = React.useRef<FilterValueType>(filterValueType);

  React.useEffect(() => {
    if (prevFilterValueTypeRef.current !== filterValueType) {
      setCondition((current) => ({ ...current, value: BLANK_VALUE }));
      prevFilterValueTypeRef.current = filterValueType;
    }
  }, [filterValueType, operator, setCondition]);

  if (filterValueType === FilterValueType.IssueId) {
    return <IssueIdFieldInput value={value[0]} onChange={handleValueChange} required />;
  }

  if (filterValueType === FilterValueType.ProjectId) {
    return <ListField name={name} />;
  }

  return null;
};

export default React.memo(RelationField);
