import * as React from 'react';
import { css } from '@linaria/core';
import { useFilterGroups } from '../../models/queryFilter';
import ItemGroup from './ItemGroup';

const FilterSelect: React.FC = () => {
  const filterGroups = useFilterGroups();

  return (
    <div className={style}>
      {filterGroups.map((group) => (
        <ItemGroup key={group.label} {...group} />
      ))}
    </div>
  );
};

export default FilterSelect;

const style = css``;
