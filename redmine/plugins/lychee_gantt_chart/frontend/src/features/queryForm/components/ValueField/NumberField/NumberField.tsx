import * as React from 'react';
import { css } from '@linaria/core';
import { QueryFilterValue } from '@/models/queryFilter';
import { NumberFieldInput } from '@/features/field';
import { useQueryFilterConditionState } from '../../../models/queryFilter';
import { TypedFieldProps } from '../types';
import { BLANK_VALUE } from '../../../constants';
import {
  FilterValueType,
  useFilterValueType,
  useFilterValueChangeEventHandlers,
} from '../../../models/filterValue';

type Props = TypedFieldProps;

const NumberField: React.FC<Props> = ({ name }) => {
  const { handleFirstValueChange, handleSecondValueChange } = useFilterValueChangeEventHandlers(name);
  const [{ value }, setCondition] = useQueryFilterConditionState(name);

  const filterValueType = useFilterValueType(name);

  const isSingle = filterValueType === FilterValueType.Single;
  const isRange = filterValueType === FilterValueType.Range;

  React.useEffect(() => {
    const getNormalizedValue = (value: QueryFilterValue): QueryFilterValue => {
      if (isSingle) {
        return value.slice(0, 1);
      }

      if (isRange) {
        return value;
      }

      return BLANK_VALUE;
    };

    setCondition(({ value, ...current }) => ({ ...current, value: getNormalizedValue(value) }));
  }, [isSingle, isRange, setCondition]);

  return (
    <>
      {(isSingle || isRange) && (
        <NumberFieldInput
          className={inputStyle}
          value={value[0]}
          onChange={handleFirstValueChange}
          required
        />
      )}
      {isRange && (
        <>
          <span className={symbolStyle}>~</span>
          <NumberFieldInput
            className={inputStyle}
            value={value[1]}
            onChange={handleSecondValueChange}
            required
          />
        </>
      )}
    </>
  );
};

export default React.memo(NumberField);

const inputStyle = css`
  width: 10em;
`;

const symbolStyle = css`
  margin: 0 0.25em;
`;
