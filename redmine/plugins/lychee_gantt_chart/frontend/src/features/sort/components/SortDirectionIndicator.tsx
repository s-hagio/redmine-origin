import * as React from 'react';
import { ColumnName } from '@/models/query';
import { Icon } from '@/components/Icon';
import { useSortStatus } from '../models/sortStatus';

type Props = {
  name: ColumnName;
};

const SortDirectionIndicator: React.FC<Props> = ({ name }) => {
  const sortStatus = useSortStatus(name);

  if (!sortStatus.isSelected) {
    return null;
  }

  const deg = sortStatus.direction === 'asc' ? 180 : 0;

  return <Icon name="chevronDown" deg={deg} />;
};

export default SortDirectionIndicator;
