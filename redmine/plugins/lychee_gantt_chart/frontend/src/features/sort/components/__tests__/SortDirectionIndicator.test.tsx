import * as React from 'react';
import { render } from '@testing-library/react';
import { beforeEach } from 'vitest';
import SortDirectionIndicator from '../SortDirectionIndicator';

const useSortStatusMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/sortStatus', () => ({
  useSortStatus: useSortStatusMock,
}));

const name = 'start_date';
const ui = <SortDirectionIndicator name={name} />;

beforeEach(() => {
  useSortStatusMock.mockReturnValue({ isSortable: true, isSelected: true, direction: 'asc' });
});

describe('isSortable: false', () => {
  beforeEach(() => {
    useSortStatusMock.mockReturnValue({ isSortable: false });
  });

  test('何もレンダリングしない', () => {
    const { container } = render(ui);

    expect(container.innerHTML).toBe('');
    expect(useSortStatusMock).toBeCalledWith(name);
  });
});

describe('isSortable: true', () => {
  describe('isSelected: false', () => {
    beforeEach(() => {
      useSortStatusMock.mockReturnValue({ isSortable: true, isSelected: false });
    });

    test('何もレンダリングしない', () => {
      const { container } = render(ui);

      expect(container.innerHTML).toBe('');
      expect(useSortStatusMock).toBeCalledWith(name);
    });
  });

  describe('isSelected: true, direction: "asc"', () => {
    beforeEach(() => {
      useSortStatusMock.mockReturnValue({ isSortable: true, isSelected: true, direction: 'asc' });
    });

    test('Render', () => {
      const { container } = render(ui);

      expect(container).toMatchSnapshot();
      expect(useSortStatusMock).toBeCalledWith(name);
    });
  });

  describe('isSelected: true, direction: "desc"', () => {
    beforeEach(() => {
      useSortStatusMock.mockReturnValue({ isSortable: true, isSelected: true, direction: 'desc' });
    });

    test('Render', () => {
      const { container } = render(ui);

      expect(container).toMatchSnapshot();
      expect(useSortStatusMock).toBeCalledWith(name);
    });
  });
});
