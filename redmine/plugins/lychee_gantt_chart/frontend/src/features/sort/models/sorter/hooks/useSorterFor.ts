import { useRecoilValue } from 'recoil';
import { sorterForState } from '../functionalStates';

export const useSorterFor = () => useRecoilValue(sorterForState);
