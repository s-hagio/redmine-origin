import { useRecoilValue } from 'recoil';
import { sortableFieldMapState } from '../states';

export const useSortableFieldMap = () => useRecoilValue(sortableFieldMapState);
