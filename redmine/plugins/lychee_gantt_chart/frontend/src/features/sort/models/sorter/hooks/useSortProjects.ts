import { useRecoilValue } from 'recoil';
import { sortProjectsState } from '../functionalStates';

export const useSortProjects = () => useRecoilValue(sortProjectsState);
