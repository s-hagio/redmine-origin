import { selector } from 'recoil';
import { destructiveSortBy } from '@/utils/array';
import { coreResourceCollectionState } from '@/models/coreResource';
import { IssueID } from '@/models/issue';
import { SortIssues } from '../types';

export const sortIssuesState = selector<SortIssues>({
  key: 'sort/sortIssues',
  get: ({ get }) => {
    const { issues } = get(coreResourceCollectionState);
    const sortedIssueIds: IssueID[] = issues.map(({ id }) => id);

    return (issues) => {
      return destructiveSortBy(issues, (issue) => sortedIssueIds.indexOf(issue.id));
    };
  },
});
