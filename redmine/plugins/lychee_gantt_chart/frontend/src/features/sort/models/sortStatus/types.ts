import { SortDirection } from '@/models/query';

export type SortStatus = {
  isSortable: boolean;
  isSelected: boolean;
  direction: SortDirection | null;
};
