import { selector } from 'recoil';
import { CoreResourceType } from '@/models/coreResource';
import { SorterFor, SorterMap } from '../types';
import { sortProjectsState } from './sortProjectsState';
import { sortVersionsState } from './sortVersionsState';
import { sortIssuesState } from './sortIssuesState';
import { sortGroupsState } from './sortGroupsState';

export const sorterForState = selector<SorterFor>({
  key: 'sort/sorterFor',
  get: ({ get }) => {
    const sorterStateMap: SorterMap = {
      [CoreResourceType.Project]: get(sortProjectsState),
      [CoreResourceType.Version]: get(sortVersionsState),
      [CoreResourceType.Issue]: get(sortIssuesState),
      [CoreResourceType.Group]: get(sortGroupsState),
    } as const;

    return (resourceType) => {
      return sorterStateMap[resourceType];
    };
  },
});
