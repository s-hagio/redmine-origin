import { selectorFamily } from 'recoil';
import { ColumnName, issueQueryState } from '@/models/query';
import { sortableFieldMapState } from '../../sortableField';
import { SortStatus } from '../types';

export const sortStatusState = selectorFamily<SortStatus, ColumnName>({
  key: 'sort/sortStatus',
  get: (name) => {
    return ({ get }) => {
      const sortableFieldMap = get(sortableFieldMapState);
      const sortableField = sortableFieldMap.get(name);

      if (!sortableField) {
        return {
          isSortable: false,
          isSelected: false,
          direction: null,
        };
      }

      const { sort: sortCriteria } = get(issueQueryState);

      const sortOrder = Object.values(sortCriteria).find(([sortColumnName]) => {
        return sortColumnName === name;
      });

      return {
        isSortable: true,
        isSelected: !!sortOrder,
        direction: (sortOrder ?? [])[1] || (sortableField.defaultDesc ? 'desc' : 'asc'),
      };
    };
  },
});
