import { constSelector, useRecoilValue } from 'recoil';
import { beforeEach } from 'vitest';
import { renderRecoilHook } from '@/test-utils';
import {
  idFieldDefinition,
  parentIssueFieldDefinition,
  priorityFieldDefinition,
  projectFieldDefinition,
} from '@/__fixtures__';
import { sortableFieldsState } from '../sortableFieldsState';

const fieldDefinitionMapStateMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldDefinition', async (importOriginal) =>
  Object.defineProperties(await importOriginal<object>(), {
    fieldDefinitionMapState: { get: fieldDefinitionMapStateMock },
  })
);

beforeEach(() => {
  fieldDefinitionMapStateMock.mockReturnValue(
    constSelector(
      new Map([
        [idFieldDefinition.name, { ...idFieldDefinition, sortable: true }],
        [projectFieldDefinition.name, { ...projectFieldDefinition, sortable: true }],
        [priorityFieldDefinition.name, { ...priorityFieldDefinition, sortable: { defaultDesc: true } }],
        [parentIssueFieldDefinition.name, parentIssueFieldDefinition],
      ])
    )
  );
});

test('ソート可能なフィールド情報を配列で返す', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(sortableFieldsState));

  expect(result.current).toEqual([
    {
      name: idFieldDefinition.name,
      label: idFieldDefinition.label,
    },
    {
      name: projectFieldDefinition.name,
      label: projectFieldDefinition.label,
    },
    {
      name: priorityFieldDefinition.name,
      label: priorityFieldDefinition.label,
      defaultDesc: true,
    },
  ]);
});
