import { useRecoilValue } from 'recoil';
import { sortGroupsState } from '../functionalStates';

export const useSortGroups = () => useRecoilValue(sortGroupsState);
