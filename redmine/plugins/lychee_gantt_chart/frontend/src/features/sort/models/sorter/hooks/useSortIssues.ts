import { useRecoilValue } from 'recoil';
import { sortIssuesState } from '../functionalStates';

export const useSortIssues = () => useRecoilValue(sortIssuesState);
