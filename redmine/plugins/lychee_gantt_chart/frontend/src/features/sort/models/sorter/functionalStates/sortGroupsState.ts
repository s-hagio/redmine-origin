import { selector } from 'recoil';
import { destructiveSortBy } from '@/utils/array';
import { issueQueryState } from '@/models/query';
import { groupsState } from '@/models/group';
import { SortGroups } from '../types';

export const sortGroupsState = selector({
  key: 'sort/sortGroups',
  get: ({ getCallback }) => {
    return getCallback(({ snapshot }) => {
      const sortGroups: SortGroups = (groups) => {
        const storedGroups = snapshot.getLoadable(groupsState).getValue();
        const { sort, groupBy } = snapshot.getLoadable(issueQueryState).getValue();
        const sortedGroupIds =
          (sort[0] ?? [])[0] === groupBy ? (storedGroups ?? []).map(({ id }) => id) : null;

        if (sortedGroupIds) {
          return destructiveSortBy(groups, (group) => sortedGroupIds.indexOf(group.id));
        }

        return destructiveSortBy(groups, 'value');
      };

      return sortGroups;
    });
  },
});
