import { useRecoilValue } from 'recoil';
import { ColumnName } from '@/models/query';
import { sortStatusState } from '../states';

export const useSortStatus = (name: ColumnName) => useRecoilValue(sortStatusState(name));
