import { CoreResourceIdentifier, CoreResourceType } from '@/models/coreResource';
import { Project } from '@/models/project';
import { Version } from '@/models/version';
import { Issue } from '@/models/issue';
import { Group } from '@/models/group';

export type Options = {
  destructive?: boolean;
  subtreeRootId?: CoreResourceIdentifier | null;
};

export type SortableProject = Project;
export type SortableVersion = Version;
export type SortableIssue = Issue;
export type SortableGroup = Group;

export type SortProjects = <T extends SortableProject>(projects: T[], options?: Options) => T[];
export type SortVersions = <T extends SortableVersion>(versions: T[], options?: Options) => T[];
export type SortIssues = <T extends SortableIssue>(issues: T[], options?: Options) => T[];
export type SortGroups = <T extends SortableGroup>(groups: T[], options?: Options) => T[];

export type SorterMap = {
  [CoreResourceType.Project]: SortProjects;
  [CoreResourceType.Version]: SortVersions;
  [CoreResourceType.Issue]: SortIssues;
  [CoreResourceType.Group]: SortGroups;
};

export type SorterFor = <T extends keyof SorterMap>(resourceType: T) => SorterMap[T];
