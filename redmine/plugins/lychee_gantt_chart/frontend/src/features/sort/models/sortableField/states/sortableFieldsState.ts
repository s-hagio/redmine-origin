import { selector } from 'recoil';
import { isPlainObject } from '@/utils/object';
import { fieldDefinitionMapState } from '@/models/fieldDefinition';
import { SortableField } from '../types';

export const sortableFieldsState = selector<SortableField[]>({
  key: 'sort/sortableFields',
  get: ({ get }) => {
    const fieldDefinitionMap = get(fieldDefinitionMapState);

    const insertedNames: Set<string> = new Set();
    const sortableFields: SortableField[] = [];

    fieldDefinitionMap.forEach((field) => {
      if (!field?.sortable || insertedNames.has(field.name)) {
        return;
      }

      insertedNames.add(field.name);

      sortableFields.push({
        name: field.name,
        label: field.label,
        ...(isPlainObject(field.sortable) ? field.sortable : {}),
      });
    });

    return sortableFields;
  },
});
