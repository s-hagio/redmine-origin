import { ColumnName } from '@/models/query';

export type SortableField = {
  name: ColumnName;
  label: string;
  defaultDesc?: boolean;
};

export type SortableFieldMap = ReadonlyMap<ColumnName, SortableField>;
