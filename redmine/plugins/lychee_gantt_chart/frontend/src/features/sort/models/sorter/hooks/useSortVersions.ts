import { useRecoilValue } from 'recoil';
import { sortVersionsState } from '../functionalStates';

export const useSortVersions = () => useRecoilValue(sortVersionsState);
