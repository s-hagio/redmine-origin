import { selector } from 'recoil';
import { destructiveSortBy } from '@/utils/array';
import { coreResourceCollectionState } from '@/models/coreResource';
import { SortVersions } from '../types';

export const sortVersionsState = selector<SortVersions>({
  key: 'sort/sortVersions',
  get: ({ get }) => {
    const { versions } = get(coreResourceCollectionState);
    const sortedVersionIds = versions.map(({ id }) => id);

    return (versions) => {
      return destructiveSortBy(versions, (version) => sortedVersionIds.indexOf(version.id));
    };
  },
});
