import { selector } from 'recoil';
import { destructiveSortBy } from '@/utils/array';
import { coreResourceCollectionState } from '@/models/coreResource';
import { SortProjects } from '../types';

export const sortProjectsState = selector<SortProjects>({
  key: 'sort/sortProjects',
  get: ({ get }) => {
    const { projects } = get(coreResourceCollectionState);
    const sortedProjectIds = projects.map(({ id }) => id);

    return (projects) => {
      return destructiveSortBy(projects, (project) => sortedProjectIds.indexOf(project.id));
    };
  },
});
