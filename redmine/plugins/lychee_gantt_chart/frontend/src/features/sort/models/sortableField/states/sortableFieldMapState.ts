import { selector } from 'recoil';
import { SortableFieldMap } from '../types';
import { sortableFieldsState } from './sortableFieldsState';

export const sortableFieldMapState = selector<SortableFieldMap>({
  key: 'sort/sortableFieldMap',
  get: ({ get }) => {
    const sortableFields = get(sortableFieldsState);

    return new Map(sortableFields.map((field) => [field.name, field]));
  },
});
