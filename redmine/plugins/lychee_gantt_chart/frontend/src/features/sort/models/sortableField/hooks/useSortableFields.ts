import { useRecoilValue } from 'recoil';
import { sortableFieldsState } from '../states';

export const useSortableFields = () => useRecoilValue(sortableFieldsState);
