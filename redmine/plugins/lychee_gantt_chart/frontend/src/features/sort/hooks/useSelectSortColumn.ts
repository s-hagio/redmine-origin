import { useRecoilCallback } from 'recoil';
import {
  issueQueryState,
  sortDirections,
  ColumnName,
  SortOrder,
  useUpdateIssueQueryParams,
} from '@/models/query';
import { useSortableFieldMap } from '../models/sortableField';

export const useSelectSortColumn = () => {
  const sortableFieldMap = useSortableFieldMap();
  const updateIssueQueryParams = useUpdateIssueQueryParams();

  return useRecoilCallback(
    ({ snapshot }) => {
      return (columnName: ColumnName) => {
        const isDefaultDesc = sortableFieldMap.get(columnName)?.defaultDesc ?? false;
        const defaultDirection = isDefaultDesc ? 'desc' : 'asc';

        const { sort: currentSortCriteria } = snapshot.getLoadable(issueQueryState).getValue();
        const [firstColumnName, firstDirection] = currentSortCriteria[0] ?? [];

        const direction = (() => {
          if (firstColumnName !== columnName) {
            return defaultDirection;
          }

          // Directionを反転
          return sortDirections[sortDirections.indexOf(firstDirection || defaultDirection) ^ 1];
        })();

        updateIssueQueryParams({
          sort: {
            0: [columnName, direction] as SortOrder,
          },
        });
      };
    },
    [sortableFieldMap, updateIssueQueryParams]
  );
};
