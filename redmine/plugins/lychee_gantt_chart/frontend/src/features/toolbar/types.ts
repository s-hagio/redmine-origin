import { FunctionComponent } from 'react';

export type ToolbarItemComponent = FunctionComponent;
