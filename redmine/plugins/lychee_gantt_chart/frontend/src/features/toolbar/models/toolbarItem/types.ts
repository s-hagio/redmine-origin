import { RecoilValue } from 'recoil';
import { PermissionName } from '@/models/permission';
import { Mode } from '@/models/mode';
import { ToolbarItemComponent } from '../../types';

export type ToolbarItemID = string;

export type ToolbarItem = Readonly<{
  id: ToolbarItemID;
  Component: ToolbarItemComponent;
  modes?: Mode[];
  permissions?: PermissionName[];
  availabilityStates?: RecoilValue<boolean>[];
  align?: 'left' | 'right';
  separator?: 'left' | 'right' | 'both';
}>;
