import { constSelector, useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { Mode, modeState } from '@/models/mode';
import { currentUserState } from '@/models/currentUser';
import { user1 } from '@/__fixtures__';
import { availableToolbarItemsState } from '../availableToolbarItemsState';
import { visibleToolbarItemsState } from '../visibleToolbarItemsState';
import { ToolbarItem } from '../../types';

const Component = vi.fn();
const trueState = constSelector(true);
const falseState = constSelector(false);

const toolbarItems: ToolbarItem[] = [
  { id: 'mode-default', Component, modes: [Mode.Default] },
  { id: 'mode-simulation', Component, modes: [Mode.Simulation] },
  { id: 'perm-manage', Component, permissions: ['manage_lychee_gantt'] },
  { id: 'perm-none', Component, permissions: [] },
  { id: 'available-true', Component, availabilityStates: [trueState] },
  { id: 'available-false', Component, availabilityStates: [falseState] },
  {
    id: 'default-manage-true',
    Component,
    modes: [Mode.Default],
    permissions: ['manage_lychee_gantt'],
    availabilityStates: [trueState],
  },
  {
    id: 'default-noPermit-true',
    Component,
    modes: [Mode.Default],
    permissions: [],
    availabilityStates: [trueState],
  },
  {
    id: 'default-noPermit-false',
    Component,
    modes: [Mode.Default],
    permissions: [],
    availabilityStates: [falseState],
  },
  {
    id: 'simulation-manage-true',
    Component,
    modes: [Mode.Simulation],
    permissions: ['manage_lychee_gantt'],
    availabilityStates: [trueState],
  },
  {
    id: 'simulation-noPermit-true',
    Component,
    modes: [Mode.Simulation],
    permissions: [],
    availabilityStates: [trueState],
  },
  {
    id: 'simulation-noPermit-false',
    Component,
    modes: [Mode.Simulation],
    permissions: [],
    availabilityStates: [falseState],
  },
];

const renderForTesting = () => {
  return renderRecoilHook(
    () => ({
      visibleItems: useRecoilValue(visibleToolbarItemsState),
      setMode: useSetRecoilState(modeState),
    }),
    {
      initializeState: ({ set }) => {
        set(currentUserState, {
          ...user1,
          isAdmin: false,
          permissionsByProjectId: { 1: ['manage_lychee_gantt'] },
          roleIdsByProjectId: {},
        });
        set(modeState, Mode.Default);
        set(availableToolbarItemsState, toolbarItems);
      },
    }
  );
};

test('条件にマッチしたItemだけの配列', () => {
  const { result } = renderForTesting();

  expect(result.current.visibleItems.map(({ id }) => id)).toEqual([
    'mode-default',
    'perm-manage',
    'available-true',
    'default-manage-true',
  ]);

  act(() => result.current.setMode(Mode.Simulation));

  expect(result.current.visibleItems.map(({ id }) => id)).toEqual([
    'mode-simulation',
    'perm-manage',
    'available-true',
    'simulation-manage-true',
  ]);
});
