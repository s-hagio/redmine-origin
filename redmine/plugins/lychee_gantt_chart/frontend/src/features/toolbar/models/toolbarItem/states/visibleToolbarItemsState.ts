import { selector } from 'recoil';
import { modeState } from '@/models/mode';
import { isAllowedToState } from '@/models/currentUser';
import { ToolbarItem } from '../types';
import { availableToolbarItemsState } from './availableToolbarItemsState';

export const visibleToolbarItemsState = selector<ToolbarItem[]>({
  key: 'toolbar/visibleToolbarItems',
  get: ({ get }) => {
    const mode = get(modeState);
    const isAllowedTo = get(isAllowedToState);
    const items = get(availableToolbarItemsState);

    return items.filter(
      (item) =>
        (!item.modes || item.modes.includes(mode)) &&
        (!item.permissions || item.permissions.some((name) => isAllowedTo(name, null, { global: true }))) &&
        (!item.availabilityStates || item.availabilityStates.every((state) => get(state)))
    );
  },
});
