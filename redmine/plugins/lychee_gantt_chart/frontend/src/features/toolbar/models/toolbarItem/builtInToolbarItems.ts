import { queryFormToolbarItem, newIssueToolbarItem, baselineToolbarItem } from '../../components/items';
import { ToolbarItem } from './types';

export const builtInToolbarItems: ReadonlyArray<ToolbarItem> = [
  queryFormToolbarItem,
  newIssueToolbarItem,
  baselineToolbarItem,
] as const;
