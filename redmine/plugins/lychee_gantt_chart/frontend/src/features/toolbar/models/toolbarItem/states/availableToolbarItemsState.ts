import { atom } from 'recoil';
import { builtInToolbarItems } from '../builtInToolbarItems';
import { ToolbarItem } from '../types';

export const availableToolbarItemsState = atom<ToolbarItem[]>({
  key: 'toolbar/availableToolbarItems',
  default: [...builtInToolbarItems],
});
