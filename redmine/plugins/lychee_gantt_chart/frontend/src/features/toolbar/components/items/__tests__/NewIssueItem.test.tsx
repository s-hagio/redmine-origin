import * as React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithTranslation } from '@/test-utils';
import NewIssueItem from '../NewIssueItem';

const isAllowedToMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/currentUser', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useIsAllowedTo: vi.fn().mockReturnValue(isAllowedToMock),
}));

const openIssueFormMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/issueForm', () => ({
  useOpenIssueForm: vi.fn().mockReturnValue(openIssueFormMock),
}));

vi.mock('@/features/issueCreation', () => ({
  IssueCreationHotkeyInfo: vi.fn((props) => `IssueCreationHotkeyInfo: ${JSON.stringify(props)}`),
}));

const useMainProjectMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/project', () => ({
  useMainProject: useMainProjectMock,
}));

beforeEach(() => {
  isAllowedToMock.mockReturnValue(true);
});

test('Render', () => {
  const { container } = renderWithTranslation(<NewIssueItem />);

  expect(container).toMatchSnapshot();
});

test('クリックでopenIssueFormを呼び出す', () => {
  const { getByRole } = renderWithTranslation(<NewIssueItem />);
  const el = getByRole('button');

  expect(openIssueFormMock).not.toBeCalled();
  fireEvent.click(el);
  expect(openIssueFormMock).toBeCalledWith({ projectId: undefined });
});

test('メインプロジェクトがある場合はprojectIdを渡す', () => {
  useMainProjectMock.mockReturnValue({ id: 123 });

  const { getByRole } = renderWithTranslation(<NewIssueItem />);
  const el = getByRole('button');

  fireEvent.click(el);
  expect(openIssueFormMock).toBeCalledWith({ projectId: 123 });
});

test('チケット作成権限が一切ない場合は表示しない', () => {
  isAllowedToMock.mockReturnValue(false);

  const { container } = renderWithTranslation(<NewIssueItem />);

  expect(container.innerHTML).toBe('');
});
