import * as React from 'react';
import { renderWithRecoil } from '@/test-utils';
import Toolbar from '../Toolbar';
import { queryFormToolbarItem } from '../items';

const useVisibleToolbarItemsMock = vi.hoisted(() => vi.fn());

vi.mock('../../models/toolbarItem', () => ({
  useVisibleToolbarItems: useVisibleToolbarItemsMock,
}));

beforeEach(() => {
  useVisibleToolbarItemsMock.mockReturnValue([queryFormToolbarItem]);
});

test('Render', () => {
  const { container } = renderWithRecoil(<Toolbar />);

  expect(container).toMatchSnapshot();
});
