import * as React from 'react';
import { css } from '@linaria/core';
import { useTranslation } from 'react-i18next';
import { ParseKeys } from 'i18next';
import { Icon, IconProps } from '@/components/Icon';
import { NakedButton } from '@/components/Button';

type Props = Omit<JSX.IntrinsicElements['button'], 'className' | 'onClick'> & {
  labelPath: ParseKeys;
  icon?: IconProps;
  onClick: JSX.IntrinsicElements['button']['onClick'];
};

const ButtonItem: React.FC<Props> = ({ labelPath, icon, onClick, ...props }) => {
  const { t } = useTranslation();

  return (
    <NakedButton className={style} onClick={onClick} {...props}>
      {icon && <Icon {...icon} />}
      {t(labelPath)}
    </NakedButton>
  );
};

export default React.memo(ButtonItem);

const style = css`
  display: inline-flex;
  align-items: center;
  height: 24px;
  padding: 0;
  border: none;
  background: transparent;
  cursor: pointer;

  :disabled {
    opacity: 0.3;
  }
`;
