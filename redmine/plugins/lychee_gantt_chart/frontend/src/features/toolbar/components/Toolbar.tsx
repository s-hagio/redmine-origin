import * as React from 'react';
import { css } from '@linaria/core';
import { groupBy } from '@/utils/array';
import { useVisibleToolbarItems } from '../models/toolbarItem';

const Toolbar: React.FC = () => {
  const toolbarItems = useVisibleToolbarItems();

  const toolbarItemsByAlign = React.useMemo(
    () => groupBy(toolbarItems, (item) => item.align ?? 'left'),
    [toolbarItems]
  );

  return (
    <div className={style}>
      {['left', 'right'].map(
        (align) =>
          toolbarItemsByAlign[align] && (
            <ul key={align} className={listStyle} data-align={align}>
              {toolbarItemsByAlign[align].map((item) => (
                <li key={item.id} data-separator={item.separator}>
                  <item.Component />
                </li>
              ))}
            </ul>
          )
      )}
    </div>
  );
};

export default React.memo(Toolbar);

export const style = css`
  display: flex;

  [data-align='right'] {
    margin-left: auto;
  }
`;

const listStyle = css`
  display: flex;
  align-items: center;
  margin: 0;
  padding: 0;
  list-style: none;

  > li {
    margin-left: 10px;
    font-size: 0.75rem;

    &:first-of-type {
      margin-left: 0;
    }
  }
`;
