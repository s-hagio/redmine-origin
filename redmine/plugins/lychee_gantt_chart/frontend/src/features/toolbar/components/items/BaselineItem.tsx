import * as React from 'react';
import { hasMainProjectState } from '@/models/setting';
import { BaselineMenu } from '@/features/baselineForm';
import { ToolbarItem } from '../../models/toolbarItem';
import { ToolbarItemComponent } from '../../types';
import Item from '../Item';

const BaselineItem: ToolbarItemComponent = () => {
  return (
    <Item icon={{ name: 'baseline' }}>
      <BaselineMenu />
    </Item>
  );
};

export default BaselineItem;

export const toolbarItem: ToolbarItem = {
  id: 'baseline',
  Component: BaselineItem,
  permissions: ['view_lychee_gantt_baselines'],
  separator: 'both',
  align: 'right',
  availabilityStates: [hasMainProjectState],
};
