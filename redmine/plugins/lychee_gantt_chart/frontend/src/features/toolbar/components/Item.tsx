import * as React from 'react';
import { css, cx } from '@linaria/core';
import { Icon, IconProps } from '@/components/Icon';

type Props = JSX.IntrinsicElements['div'] & {
  icon?: IconProps;
};

const Item: React.FC<Props> = ({ className, icon, children, ...props }) => {
  return (
    <div className={cx(style, className)} {...props}>
      {icon && <Icon {...icon} />}
      {children}
    </div>
  );
};

export default React.memo(Item);

const style = css`
  display: inline-flex;
  align-items: center;
  height: 24px;
`;
