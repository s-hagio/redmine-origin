import * as React from 'react';
import { render } from '@testing-library/react';
import Item from '../Item';

test('Render', () => {
  const { container } = render(
    <Item className="abc" icon={{ name: 'filter' }}>
      CHILD!
    </Item>
  );

  expect(container).toMatchSnapshot();
});
