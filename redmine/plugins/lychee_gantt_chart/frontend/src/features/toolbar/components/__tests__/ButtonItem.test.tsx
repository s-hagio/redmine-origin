import * as React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithTranslation } from '@/test-utils';
import ButtonItem from '../ButtonItem';

const onClickSpy = vi.fn();

const renderForTesting = () => {
  return renderWithTranslation(
    <ButtonItem labelPath="label.query" icon={{ name: 'filter' }} onClick={onClickSpy} />
  );
};

test('Render', () => {
  const { container } = renderForTesting();

  expect(container).toMatchSnapshot();
});

test('Render', () => {
  const { getByRole } = renderForTesting();

  fireEvent.click(getByRole('button'));

  expect(onClickSpy).toBeCalled();
});
