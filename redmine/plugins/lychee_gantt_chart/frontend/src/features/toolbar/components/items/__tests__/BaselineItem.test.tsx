import * as React from 'react';
import { render } from '@testing-library/react';
import BaselineItem from '../BaselineItem';

vi.mock('@/features/baselineForm', () => ({
  BaselineMenu: vi.fn((props) => `BaselineMenu: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<BaselineItem />);

  expect(container).toMatchSnapshot();
});
