import * as React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithTranslation } from '@/test-utils';
import QueryFormItem from '../QueryFormItem';

const openQueryFormMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/queryForm', () => ({
  useOpenQueryForm: vi.fn().mockReturnValue(openQueryFormMock),
}));

test('Render', () => {
  const { container } = renderWithTranslation(<QueryFormItem />);

  expect(container).toMatchSnapshot();
});

test('クリックでopenQueryFormを呼び出す', () => {
  const { container } = renderWithTranslation(<QueryFormItem />);
  const el = container.firstChild as HTMLElement;

  fireEvent.click(el);

  expect(openQueryFormMock).toBeCalled();
});
