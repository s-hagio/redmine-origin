import * as React from 'react';
import { useMainProject } from '@/models/project';
import { useIsAllowedTo } from '@/models/currentUser';
import { useOpenIssueForm } from '@/features/issueForm';
import { IssueCreationHotkeyInfo } from '@/features/issueCreation';
import { ToolbarItem } from '../../models/toolbarItem';
import ButtonItem from '../ButtonItem';
import Item from '../Item';

const NewIssueItem: React.FC = () => {
  const isAllowedTo = useIsAllowedTo();
  const openIssueFrom = useOpenIssueForm();
  const { id: projectId } = useMainProject() || {};

  const handleClick = React.useCallback(() => {
    openIssueFrom({ projectId });
  }, [openIssueFrom, projectId]);

  if (!isAllowedTo('add_issues', null, { global: true })) {
    return null;
  }

  return (
    <Item>
      <ButtonItem icon={{ name: 'add' }} labelPath="label.newIssue" onClick={handleClick} />
      <IssueCreationHotkeyInfo />
    </Item>
  );
};

export default NewIssueItem;

export const toolbarItem: ToolbarItem = {
  id: 'newIssue',
  Component: NewIssueItem,
};
