import * as React from 'react';
import { useOpenQueryForm } from '@/features/queryForm';
import { ToolbarItem } from '../../models/toolbarItem';
import { ToolbarItemComponent } from '../../types';
import ButtonItem from '../ButtonItem';

const QueryFormItem: ToolbarItemComponent = () => {
  const openQueryForm = useOpenQueryForm();

  return <ButtonItem labelPath="label.query" icon={{ name: 'filter' }} onClick={openQueryForm} />;
};

export default QueryFormItem;

export const toolbarItem: ToolbarItem = {
  id: 'queryForm',
  Component: QueryFormItem,
};
