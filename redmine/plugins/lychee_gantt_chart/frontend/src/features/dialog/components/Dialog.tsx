import * as React from 'react';
import { css, cx } from '@linaria/core';
import { getZIndex } from '@/styles';
import { useCloseDialog } from '../hooks';
import { useDialogVisibleState } from '../models/visibility';
import { DialogID, CloseDialog } from '../types';

type RenderContentProps = {
  isVisible: boolean;
  close: CloseDialog;
};
type RenderContent = (props: RenderContentProps) => React.ReactNode;

type Props = Omit<JSX.IntrinsicElements['div'], 'id' | 'children'> & {
  id: DialogID;
  children: React.ReactNode | RenderContent;
  alwaysRenderContainer?: boolean;
  disableCloseOnOutsideClick?: boolean;
};

export type DialogElement = HTMLDivElement | null;

const Dialog = React.forwardRef<DialogElement, Props>(function Dialog(
  { id, className, children, alwaysRenderContainer, disableCloseOnOutsideClick, ...props },
  ref
) {
  const elementRef = React.useRef<DialogElement>(null);
  React.useImperativeHandle<DialogElement, DialogElement>(ref, () => elementRef.current);

  const [isVisible] = useDialogVisibleState(id);
  const close = useCloseDialog(id);

  const handleDocumentClick = React.useCallback(
    (event: MouseEvent) => {
      if (
        document.activeElement?.tagName !== 'INPUT' &&
        elementRef.current &&
        !event.composedPath().includes(elementRef.current)
      ) {
        close();
      }
    },
    [close]
  );

  const canRender = alwaysRenderContainer || isVisible;

  React.useEffect(() => {
    if (!canRender || disableCloseOnOutsideClick) {
      return;
    }

    const register = () => document.addEventListener('click', handleDocumentClick, { passive: true });
    const unregister = () => document.removeEventListener('click', handleDocumentClick);

    const delayTimerId = window.setTimeout(register);

    return () => {
      window.clearTimeout(delayTimerId);
      unregister();
    };
  }, [isVisible, handleDocumentClick, canRender, disableCloseOnOutsideClick]);

  if (!canRender) {
    return null;
  }

  return (
    <div ref={elementRef} className={cx(style, className)} {...props} data-visible={isVisible}>
      {typeof children === 'function' ? children({ isVisible, close }) : isVisible && children}
    </div>
  );
});

export default Dialog;

const style = css`
  position: absolute;
  z-index: ${getZIndex('dialog/Dialog')};
`;
