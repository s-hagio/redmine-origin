import * as React from 'react';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act, fireEvent } from '@testing-library/react';
import { renderWithRecoil, renderWithRecoilHook } from '@/test-utils';
import Dialog from '../Dialog';
import { dialogVisibleState } from '../../models/visibility';

const id = 'dialog_id_999';

test('Render', () => {
  const { result, container } = renderWithRecoilHook(<Dialog id={id}>CHILD!</Dialog>, () => ({
    setVisible: useSetRecoilState(dialogVisibleState(id)),
  }));

  expect(container.textContent).toBe('');

  act(() => result.current.setVisible(true));

  expect(container).toMatchSnapshot();
});

test('Dialogの可視状態に関わらず常にレンダリングするオプション', () => {
  const { container } = renderWithRecoil(
    <Dialog id={id} alwaysRenderContainer>
      CHILD!
    </Dialog>
  );

  expect(container).toMatchSnapshot();
});

test('childrenが関数の場合はisVisibleとclose関数を渡す', () => {
  const { result, container, getByRole } = renderWithRecoilHook(
    <Dialog id={id} alwaysRenderContainer>
      {({ isVisible, close }) => (
        <button onClick={() => close()}>{isVisible ? 'VISIBLE!' : 'INVISIBLE!'}</button>
      )}
    </Dialog>,
    () => ({
      setVisible: useSetRecoilState(dialogVisibleState(id)),
      isVisible: useRecoilValue(dialogVisibleState(id)),
    })
  );

  expect(container.textContent).toBe('INVISIBLE!');
  expect(result.current.isVisible).toBe(false);

  act(() => result.current.setVisible(true));

  expect(container.textContent).toBe('VISIBLE!');
  expect(result.current.isVisible).toBe(true);

  fireEvent.click(getByRole('button'));

  expect(container.textContent).toBe('INVISIBLE!');
  expect(result.current.isVisible).toBe(false);
});

test('ダイアログ以外の部分をクリックしたら閉じる', () => {
  vi.useFakeTimers();

  const { container, getByTestId } = renderWithRecoil(
    <Dialog data-testid={id} id={id}>
      CHILD!
    </Dialog>,
    {
      initializeState: ({ set }) => {
        set(dialogVisibleState(id), true);
      },
    }
  );

  // 表示直後のクリック
  fireEvent.click(container);
  expect(container.textContent).toBe('CHILD!');

  // useEffect内でclickイベントリスナがセットされる
  vi.advanceTimersToNextTimer();

  fireEvent.click(getByTestId(id));
  expect(container.textContent).toBe('CHILD!');

  fireEvent.click(container);
  expect(container.textContent).toBe('');
});

test('activeElementがINPUTの場合は閉じない', () => {
  const { container } = renderWithRecoil(
    <Dialog data-testid={id} id={id}>
      CHILD!
    </Dialog>,
    {
      initializeState: ({ set }) => {
        set(dialogVisibleState(id), true);
      },
    }
  );

  const input = document.createElement('input');
  document.body.appendChild(input);
  input.focus();

  expect(container.textContent).toBe('CHILD!');
  fireEvent.click(container);
  expect(container.textContent).toBe('CHILD!');
});

test('disableCloseOnOutsideClick: trueでダイアログ外クリックで閉じる機能を無効化', () => {
  const { container } = renderWithRecoil(
    <Dialog data-testid={id} id={id} disableCloseOnOutsideClick>
      CHILD!
    </Dialog>,
    {
      initializeState: ({ set }) => {
        set(dialogVisibleState(id), true);
      },
    }
  );

  fireEvent.click(container);
  expect(container.textContent).toBe('CHILD!');
});
