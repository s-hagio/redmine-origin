import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { useOpenDialog } from '../useOpenDialog';
import { dialogVisibleState } from '../../models/visibility';

test('Dialogを表示する関数を返す', () => {
  const id = 'dialog_id_123';

  const { result } = renderRecoilHook(() => ({
    open: useOpenDialog(id),
    isVisible: useRecoilValue(dialogVisibleState(id)),
  }));

  expect(result.current.isVisible).toBe(false);
  act(() => result.current.open());
  expect(result.current.isVisible).toBe(true);
});
