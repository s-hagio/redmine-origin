import * as React from 'react';
import { useMemo } from 'react';
import Dialog, { DialogElement } from '../components/Dialog';
import { DialogID } from '../types';
import { useOpenDialog } from './useOpenDialog';
import { useCloseDialog } from './useCloseDialog';

type Props = Omit<React.ComponentPropsWithRef<typeof Dialog>, 'id'>;

type ReturnObject = [
  React.ForwardRefExoticComponent<Props>,
  {
    open: () => void;
    close: () => void;
  },
];

export const useDialog = (id: DialogID): ReturnObject => {
  const open = useOpenDialog(id);
  const close = useCloseDialog(id);

  const IdentifiedDialog = useMemo(() => {
    return React.forwardRef<DialogElement, Props>(function IdentifiedDialog({ children, ...props }, ref) {
      return (
        <Dialog ref={ref} id={id} {...props}>
          {children}
        </Dialog>
      );
    });
  }, [id]);

  return [IdentifiedDialog, { open, close }];
};
