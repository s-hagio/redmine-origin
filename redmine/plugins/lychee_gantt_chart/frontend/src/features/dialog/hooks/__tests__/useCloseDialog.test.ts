import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { useCloseDialog } from '../useCloseDialog';
import { dialogVisibleState } from '../../models/visibility';

test('Dialogを非表示にする関数を返す', () => {
  const id = 'dialog_id_123';

  const { result } = renderRecoilHook(
    () => ({
      open: useCloseDialog(id),
      isVisible: useRecoilValue(dialogVisibleState(id)),
    }),
    {
      initializeState: ({ set }) => {
        set(dialogVisibleState(id), true);
      },
    }
  );

  expect(result.current.isVisible).toBe(true);
  act(() => result.current.open());
  expect(result.current.isVisible).toBe(false);
});
