import { useCallback } from 'react';
import { useDialogVisibleState } from '../models/visibility';
import { DialogID, CloseDialog } from '../types';

export const useCloseDialog = (id: DialogID): CloseDialog => {
  const [, setVisible] = useDialogVisibleState(id);

  return useCallback(() => {
    setVisible(false);
  }, [setVisible]);
};
