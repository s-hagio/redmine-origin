import * as React from 'react';
import { render, renderHook } from '@testing-library/react';
import { useDialog } from '../useDialog';

const openDialogMock = vi.hoisted(() => vi.fn());

vi.mock('../useOpenDialog', () => ({
  useOpenDialog: vi.fn().mockReturnValue(openDialogMock),
}));

const closeDialogMock = vi.hoisted(() => vi.fn());

vi.mock('../useCloseDialog', () => ({
  useCloseDialog: vi.fn().mockReturnValue(closeDialogMock),
}));

const DialogMock = vi.hoisted(() => vi.fn());

vi.mock('../../components/Dialog', () => ({
  default: vi.fn(({ children, ...props }) => {
    DialogMock(props);
    return React.createElement('dialog-mock', null, children);
  }),
}));

test('IDが割り当てられたDialogコンポーネントとopen/close用のヘルパー関数を返す', () => {
  const { result } = renderHook(() => useDialog('test'));

  const [IdentifiedDialog, helpers] = result.current;

  expect(helpers.open).toBe(openDialogMock);
  expect(helpers.close).toBe(closeDialogMock);

  const { container } = render(<IdentifiedDialog>CHILD!</IdentifiedDialog>);

  expect(container).toMatchSnapshot();
});
