import { useCallback } from 'react';
import { useDialogVisibleState } from '../models/visibility';
import { DialogID, OpenDialog } from '../types';

export const useOpenDialog = (id: DialogID): OpenDialog => {
  const [, setVisible] = useDialogVisibleState(id);

  return useCallback(() => {
    setVisible(true);
  }, [setVisible]);
};
