export type DialogID = string;

export type OpenDialog = () => void;
export type CloseDialog = () => void;
