import { atomFamily } from 'recoil';
import { DialogID } from '../../../types';

export const dialogVisibleState = atomFamily<boolean, DialogID>({
  key: 'dialog/dialogVisible',
  default: false,
});
