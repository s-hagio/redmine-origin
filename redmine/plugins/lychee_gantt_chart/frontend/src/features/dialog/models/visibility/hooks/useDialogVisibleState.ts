import { useRecoilState } from 'recoil';
import { dialogVisibleState } from '../states';
import { DialogID } from '../../../types';

export const useDialogVisibleState = (id: DialogID) => useRecoilState(dialogVisibleState(id));
