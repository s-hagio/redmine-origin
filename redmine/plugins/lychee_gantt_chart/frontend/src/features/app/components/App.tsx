import * as React from 'react';
import { css } from '@linaria/core';
import { IssueFormInitializer } from '@/features/issueForm';
import { ResourceControllerInitializer } from '@/features/resourceController';
import { Gantt } from '@/features/gantt';
import { FieldDependencyLoader } from '@/features/field';
import { LoadingIndicator } from '@/features/resourceLoader';
import { QueryForm } from '@/features/queryForm';
import { NotificationContainer } from '@/features/notification';
import { BaselineSnapshotLoader } from '@/features/baselineForm';
import UserInteractionRefresher from './UserInteractionRefresher';
import CoreResourceLoader from './CoreResourceLoader';

import './global.css';

const App: React.FC = () => {
  return (
    <div className={style}>
      <IssueFormInitializer />
      <ResourceControllerInitializer />
      <UserInteractionRefresher />

      <QueryForm />
      <NotificationContainer />

      <React.Suspense>
        <Gantt />
      </React.Suspense>

      <React.Suspense>
        <FieldDependencyLoader />
      </React.Suspense>

      <React.Suspense>
        <BaselineSnapshotLoader />
      </React.Suspense>

      <React.Suspense fallback={<LoadingIndicator />}>
        <CoreResourceLoader />
      </React.Suspense>
    </div>
  );
};

export default App;

const style = css`
  * {
    box-sizing: border-box;
  }

  padding-top: 15px;
`;
