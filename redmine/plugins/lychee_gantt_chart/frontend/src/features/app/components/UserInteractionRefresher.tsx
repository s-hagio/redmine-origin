import * as React from 'react';
import { useRefreshCurrentMousePosition } from '@/models/userInteraction';

const UserInteractionRefresher: React.FC = () => {
  const refreshCurrentMousePosition = useRefreshCurrentMousePosition();

  React.useEffect(() => {
    const handleMouseMove = (event: MouseEvent) => {
      refreshCurrentMousePosition(event);
    };

    document.addEventListener('mousemove', handleMouseMove);

    return () => {
      document.removeEventListener('mousemove', handleMouseMove);
    };
  }, [refreshCurrentMousePosition]);

  return null;
};

export default React.memo(UserInteractionRefresher);
