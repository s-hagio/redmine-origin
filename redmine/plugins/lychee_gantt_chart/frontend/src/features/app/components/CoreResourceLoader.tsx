import * as React from 'react';
import { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useRefreshCoreResources, useCoreResourceCollectionLoadable } from '@/models/coreResource';
import { useHandleNotifyFromLoadable } from '@/features/notification';
import { useOpenQueryForm } from '@/features/queryForm';

const CoreResourceLoader: React.FC = () => {
  const { t } = useTranslation();

  const openQueryForm = useOpenQueryForm();
  const previousErrorRef = useRef<Error | null>(null);

  const refresh = useRefreshCoreResources();
  const loadable = useCoreResourceCollectionLoadable();

  useHandleNotifyFromLoadable(loadable, { title: t('label.queryError') });

  React.useEffect(() => {
    if (loadable.state === 'hasError') {
      const error = loadable.errorMaybe();

      if (error !== previousErrorRef.current) {
        previousErrorRef.current = error ?? null;
        openQueryForm();
      }

      return;
    }

    const collection = loadable.valueMaybe();

    if (collection) {
      refresh(collection);
    }
  }, [loadable, openQueryForm, refresh]);

  return null;
};

export default CoreResourceLoader;
