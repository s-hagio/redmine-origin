import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import UserInteractionRefresher from '../UserInteractionRefresher';

const refreshMousePositionMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/userInteraction', () => ({
  useRefreshCurrentMousePosition: vi.fn().mockReturnValue(refreshMousePositionMock),
}));

test('documentのmouseMoveでcurrentMousePositionをrefresh', () => {
  render(<UserInteractionRefresher />);

  const event = new MouseEvent('mousemove', { clientX: 100, clientY: 200 });
  fireEvent(document, event);

  expect(refreshMousePositionMock).toBeCalledWith(event);
});
