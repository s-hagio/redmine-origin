import * as React from 'react';
import { render } from '@testing-library/react';
import App from '../App';

vi.mock('@/features/issueForm', () => ({
  IssueFormInitializer: vi.fn((props) => `IssueFormInitializer: ${JSON.stringify(props)}`),
}));

vi.mock('@/features/resourceController', () => ({
  ResourceControllerInitializer: vi.fn((props) => `ResourceControllerInitializer: ${JSON.stringify(props)}`),
}));

vi.mock('@/features/gantt', () => ({
  Gantt: vi.fn((props) => `Gantt: ${JSON.stringify(props)}`),
}));

vi.mock('@/features/field', () => ({
  FieldDependencyLoader: vi.fn((props) => `FieldDependencyLoader: ${JSON.stringify(props)}`),
}));

vi.mock('@/features/resourceLoader', () => ({
  LoadingIndicator: vi.fn((props) => `LoadingIndicator: ${JSON.stringify(props)}`),
}));

vi.mock('@/features/queryForm', () => ({
  QueryForm: vi.fn((props) => `QueryForm: ${JSON.stringify(props)}`),
}));

vi.mock('@/features/notification', () => ({
  NotificationContainer: vi.fn((props) => `NotificationContainer: ${JSON.stringify(props)}`),
}));

vi.mock('@/features/baselineForm', () => ({
  BaselineSnapshotLoader: vi.fn((props) => `BaselineSnapshotLoader: ${JSON.stringify(props)}`),
}));

vi.mock('../UserInteractionRefresher', () => ({
  default: vi.fn((props) => `UserInteractionRefresher: ${JSON.stringify(props)}`),
}));

vi.mock('../CoreResourceLoader', () => ({
  default: vi.fn((props) => `CoreResourceLoader: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const result = render(<App />);

  expect(result.container).toMatchSnapshot();
});
