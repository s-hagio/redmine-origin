import * as React from 'react';
import { RecoilLoadable } from 'recoil';
import { AxiosError } from 'axios';
import { renderWithRecoil } from '@/test-utils';
import CoreResourceLoader from '../CoreResourceLoader';

const useCoreResourceCollectionLoadableMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/coreResource', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useCoreResourceCollectionLoadable: useCoreResourceCollectionLoadableMock,
}));

const useHandleNotifyFromLoadableMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/notification', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useHandleNotifyFromLoadable: useHandleNotifyFromLoadableMock,
}));

const openQueryFormMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/queryForm', () => ({
  useOpenQueryForm: vi.fn().mockReturnValue(openQueryFormMock),
}));

beforeEach(() => {
  useCoreResourceCollectionLoadableMock.mockReturnValue(RecoilLoadable.loading());
});

test.todo('CoreResourceCollectionの読み込みと更新');
test.todo('クエリエラーが発生したらQueryFormを開く');
test.todo('クエリエラー発生後にQueryFormを開くのは同じエラーで一度のみ');

test('loadableのエラーハンドリング', () => {
  const loadable = RecoilLoadable.error(new AxiosError('error'));
  useCoreResourceCollectionLoadableMock.mockReturnValue(loadable);

  renderWithRecoil(<CoreResourceLoader />);

  expect(useHandleNotifyFromLoadableMock).toBeCalledWith(loadable, { title: 'Query Error' });
});
