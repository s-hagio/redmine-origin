import { useCallback } from 'react';
import { isNewIssueId } from '@/models/issue';
import { IssueFormEventHandler } from '@/features/issueForm';
import { useRefreshIssue } from '../models/issue';

export const useHandleIssueUpdateFromIssueForm = (): IssueFormEventHandler => {
  const refreshIssue = useRefreshIssue();

  return useCallback(
    (event) => {
      if (!isNewIssueId(event.detail.originalParam.id)) {
        refreshIssue(event.detail.issueId);
      }
    },
    [refreshIssue]
  );
};
