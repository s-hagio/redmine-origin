import { useRecoilCallback } from 'recoil';
import { BaselineParams, baselineSummariesState } from '@/models/baseline';
import { useSaveBaseline } from './useSaveBaseline';

export const useUpdateBaseline = () => {
  const saveBaseline = useSaveBaseline();

  return useRecoilCallback(
    ({ set }) => {
      return async (params: BaselineParams) => {
        const response = await saveBaseline(params);

        set(baselineSummariesState, (current) => {
          const index = current.findIndex((baseline) => baseline.id === params.id);

          if (index === -1) {
            return current;
          }

          const newBaselines = [...current];
          newBaselines[index] = response;

          return newBaselines;
        });

        return response;
      };
    },
    [saveBaseline]
  );
};
