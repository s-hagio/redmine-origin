import { useRecoilCallback } from 'recoil';
import { useSetCoreResourceEntities } from '@/models/coreResource';
import {
  issueEntityState,
  IssueParams,
  IssueWithRelatedResources,
  useCreateIssueTemporaryChangeCallbacks,
} from '@/models/issue';
import { IssueFormMeta, useOpenIssueForm } from '@/features/issueForm';
import { saveIssueByParams } from '../helpers';
import { useNotifyIssueError } from './useNotifyIssueError';

type Options = Partial<{
  meta: IssueFormMeta;
  shouldOpenFormOnError: boolean;
  alwaysNotifyOnError: boolean;
}>;

type SaveIssue = (params: IssueParams, options?: Options) => Promise<IssueWithRelatedResources>;

export const useSaveIssue = (): SaveIssue => {
  const notifyIssueError = useNotifyIssueError();
  const openIssueForm = useOpenIssueForm();

  const setCoreResourceEntities = useSetCoreResourceEntities();
  const createTemporaryChangeCallbacks = useCreateIssueTemporaryChangeCallbacks();

  return useRecoilCallback(
    ({ set }) => {
      return async (params, options) => {
        const { setTemporaryChange, removeTemporaryChange } = createTemporaryChangeCallbacks(params.id);

        setTemporaryChange(params);

        try {
          const response = await saveIssueByParams(params);
          const { issue, relatedResources } = response;

          set(issueEntityState(issue.id), issue);
          setCoreResourceEntities(relatedResources);

          return response;
        } catch (error) {
          if (options?.shouldOpenFormOnError) {
            openIssueForm(params, error, options);
          }

          if (options?.alwaysNotifyOnError || !options?.shouldOpenFormOnError) {
            notifyIssueError(error, params.id);
          }

          throw error;
        } finally {
          removeTemporaryChange();
        }
      };
    },
    [createTemporaryChangeCallbacks, notifyIssueError, openIssueForm, setCoreResourceEntities]
  );
};
