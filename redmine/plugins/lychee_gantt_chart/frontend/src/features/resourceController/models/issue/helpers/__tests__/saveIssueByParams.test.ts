import { saveIssueByParams } from '../saveIssueByParams';

const issuesApiMock = vi.hoisted(() => ({
  post: vi.fn().mockResolvedValue({ subject: 'Created issue!!!!' }),
  put: vi.fn().mockResolvedValue({ subject: 'Updated issue!!!!' }),
}));

vi.mock('@/api', () => ({
  createClient: vi.fn(() => ({
    issues: {
      $post: issuesApiMock.post,
      _id: vi.fn().mockReturnValue({
        $put: issuesApiMock.put,
      }),
    },
  })),
}));

test('paramsにidが含まれていない: 作成リクエスト', async () => {
  const params = { subject: 'test' };
  const result = await saveIssueByParams(params);

  expect(issuesApiMock.post).toBeCalledWith({ body: { issue: params } });
  expect(result).toEqual({ subject: 'Created issue!!!!' });
});

test('paramsにidが含まれている: 更新リクエスト', async () => {
  const params = { id: 1, subject: 'test' };
  const result = await saveIssueByParams(params);

  expect(issuesApiMock.put).toBeCalledWith({ body: { issue: params } });
  expect(result).toEqual({ subject: 'Updated issue!!!!' });
});
