import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { IssueID } from '@/models/issue';
import { useNotifyAPIError } from '@/features/notification';

type NotifyIssueError = (error: unknown, issueId?: IssueID) => void;

export const useNotifyIssueError = (): NotifyIssueError => {
  const { t } = useTranslation();
  const notifyAPIError = useNotifyAPIError();

  return useCallback(
    (error, issueId) => {
      const title = issueId ? `${t('label.issue')} #${issueId}` : t('label.newIssue');

      notifyAPIError(error, { title });
    },
    [notifyAPIError, t]
  );
};
