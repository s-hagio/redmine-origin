import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { createClient } from '@/api';
import { useSetCoreResourceEntities } from '@/models/coreResource';
import { BatchIssueParams } from '@/models/issue';
import { NotificationType, useNotify } from '@/features/notification';

type BulkUpdateIssues = (paramsList: BatchIssueParams[]) => Promise<void>;

export const useBulkUpdateIssues = (): BulkUpdateIssues => {
  const { t } = useTranslation();
  const notify = useNotify();
  const setCoreResourceEntities = useSetCoreResourceEntities();

  return useCallback(
    async (paramsList) => {
      // TODO: Apply temporary changes

      const client = createClient();
      const response = await client.issues.bulk_update.$put({
        body: {
          issues: paramsList,
        },
      });

      setCoreResourceEntities(response);

      if (!response.errors.length) {
        return;
      }

      const errorMessages = response.errors
        .map(({ id, errors }) => [`#${id}`, ...errors].join('\n'))
        .join('\n');

      notify(NotificationType.Error, errorMessages, {
        title: t('message.failedToSaveIssues', {
          total: paramsList.length,
          count: response.errors.length,
        }),
      });
    },
    [notify, setCoreResourceEntities, t]
  );
};
