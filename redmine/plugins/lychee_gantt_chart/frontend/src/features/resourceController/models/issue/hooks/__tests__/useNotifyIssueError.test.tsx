import * as React from 'react';
import { t } from 'i18next';
import { act, renderHook } from '@testing-library/react';
import { AxiosError } from 'axios';
import { TranslationProvider } from '@/providers/translationProvider';
import { useNotifyIssueError } from '../useNotifyIssueError';

const notifyErrorMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/notification', () => ({
  useNotifyAPIError: vi.fn().mockReturnValue(notifyErrorMock),
}));

test('IssueのAPIエラーを通知する', () => {
  const { result } = renderHook(() => useNotifyIssueError(), {
    wrapper: ({ children }) => <TranslationProvider lang="ja">{children}</TranslationProvider>,
  });

  const error = new AxiosError('error');
  const issueId = 123;

  act(() => result.current(error, issueId));

  expect(notifyErrorMock).toBeCalledWith(error, { title: `チケット #${issueId}` });
});

test('新規チケットの場合はIDを表示しない', () => {
  const { result } = renderHook(() => useNotifyIssueError(), {
    wrapper: ({ children }) => <TranslationProvider lang="ja">{children}</TranslationProvider>,
  });

  const error = new AxiosError('error');

  act(() => result.current(error));

  expect(notifyErrorMock).toBeCalledWith(error, { title: t('label.newIssue') });
});
