import { useRecoilValue } from 'recoil';
import { act, waitFor } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { baselineSummariesState, selectedBaselineIdState } from '@/models/baseline';
import { useDeleteBaseline } from '../useDeleteBaseline';

const createClientMock = vi.hoisted(() => vi.fn());

vi.mock('@/api', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  createClient: createClientMock,
}));

const apiDeleteMock = vi.fn().mockResolvedValue({});
const apiMocks = {
  baselines: {
    _id: vi.fn().mockReturnValue({ $delete: apiDeleteMock }),
  },
};

createClientMock.mockReturnValue(apiMocks);

const notifyAPIErrorMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/notification', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useNotifyAPIError: vi.fn().mockReturnValue(notifyAPIErrorMock),
}));

test('指定IDのBaselineを削除する', async () => {
  const { result } = renderRecoilHook(
    () => ({
      delete: useDeleteBaseline(),
      baselineSummaries: useRecoilValue(baselineSummariesState),
      selectedBaselineId: useRecoilValue(selectedBaselineIdState),
    }),
    {
      initializeState: ({ set }) => {
        set(baselineSummariesState, [
          { id: 1, name: 'test1', snappedAt: '2021-01-01T00:00:00.000Z' },
          { id: 2, name: 'test2', snappedAt: '2021-01-01T00:00:00.000Z' },
          { id: 3, name: 'test3', snappedAt: '2021-01-01T00:00:00.000Z' },
        ]);

        set(selectedBaselineIdState, 2);
      },
    }
  );

  act(() => {
    result.current.delete(2);
  });

  expect(apiMocks.baselines._id).toBeCalledWith(2);
  expect(apiDeleteMock).toBeCalled();

  await waitFor(() => {
    expect(result.current.baselineSummaries[1].isDeleting).toBe(true);
    expect(result.current.selectedBaselineId).toBe(null);
  });

  expect(result.current.baselineSummaries).toEqual([
    { id: 1, name: 'test1', snappedAt: '2021-01-01T00:00:00.000Z' },
    { id: 3, name: 'test3', snappedAt: '2021-01-01T00:00:00.000Z' },
  ]);
});

describe('APIエラーが発生した場合', () => {
  const error = new Error('test');

  beforeEach(() => {
    apiDeleteMock.mockRejectedValue(error);
  });

  test('例外を再び投げる', async () => {
    const { result } = renderRecoilHook(() => useDeleteBaseline());

    await expect(() => act(() => result.current(1))).rejects.toThrow(error);
  });

  test('エラーを通知する', async () => {
    const { result } = renderRecoilHook(() => useDeleteBaseline());

    await act(async () => {
      try {
        await result.current(1);
      } catch (e) {
        // do nothing
      }
    });

    expect(notifyAPIErrorMock).toBeCalledWith(error);
  });

  test('削除中フラグを外す', async () => {
    const { result } = renderRecoilHook(
      () => ({
        delete: useDeleteBaseline(),
        baselineSummaries: useRecoilValue(baselineSummariesState),
      }),
      {
        initializeState: ({ set }) => {
          set(baselineSummariesState, [
            { id: 1, name: 'test1', snappedAt: '2021-01-01T00:00:00.000Z', isDeleting: true },
          ]);
        },
      }
    );

    await act(async () => {
      try {
        await result.current.delete(1);
      } catch (e) {
        // do nothing
      }
    });

    expect(result.current.baselineSummaries[0].isDeleting).toBe(false);
  });
});
