import { renderHook } from '@testing-library/react';
import { useSaveIssue } from '../useSaveIssue';
import { useUpdateIssue } from '../useUpdateIssue';

const saveIssueMock = vi.hoisted(() => vi.fn());

vi.mock('../useSaveIssue', () => ({
  useSaveIssue: vi.fn(() => saveIssueMock),
}));

beforeEach(() => {
  vi.mocked(useSaveIssue).mockReturnValue(saveIssueMock);
});

test('idとparamsをmergeしてsaveIssueに渡す', () => {
  const id = 123;
  const params = { subject: 'fooooooooooooo' };
  const { result } = renderHook(() => ({
    update: useUpdateIssue(id),
  }));

  expect(saveIssueMock).not.toBeCalled();

  result.current.update(params);

  expect(saveIssueMock).toBeCalledWith({ id, ...params });
});
