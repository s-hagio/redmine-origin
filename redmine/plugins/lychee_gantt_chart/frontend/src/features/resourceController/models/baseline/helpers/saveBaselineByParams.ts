import { createClient } from '@/api';
import { BaselineParams } from '@/models/baseline';

export const saveBaselineByParams = async (params: BaselineParams) => {
  const client = createClient();
  const baseline = { ...params };

  if (params.id) {
    delete baseline.id;
    return client.baselines._id(params.id).$put({ body: { baseline } });
  }

  return client.baselines.$post({ body: { baseline } });
};
