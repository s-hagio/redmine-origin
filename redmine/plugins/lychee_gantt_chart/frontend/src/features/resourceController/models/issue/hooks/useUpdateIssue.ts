import { useCallback } from 'react';
import { IssueID, IssueParams, IssueWithRelatedResources } from '@/models/issue';
import { useSaveIssue } from './useSaveIssue';

type UpdateIssue = (params: IssueParams) => Promise<IssueWithRelatedResources>;

export const useUpdateIssue = (id: IssueID): UpdateIssue => {
  const saveIssue = useSaveIssue();

  return useCallback(async (params) => saveIssue({ id, ...params }), [id, saveIssue]);
};
