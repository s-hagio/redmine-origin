import { act, renderHook } from '@testing-library/react';
import { AxiosError } from 'axios';
import { useNotifyIssueRelationError } from '../useNotifyIssueRelationError';

const notifyErrorMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/notification', () => ({
  useNotifyAPIError: vi.fn().mockReturnValue(notifyErrorMock),
}));

test('IssueRelationのAPIエラーを通知する', () => {
  const { result } = renderHook(() => useNotifyIssueRelationError());
  const error = new AxiosError('error');

  act(() => result.current(error));

  expect(notifyErrorMock).toBeCalledWith(error);
});
