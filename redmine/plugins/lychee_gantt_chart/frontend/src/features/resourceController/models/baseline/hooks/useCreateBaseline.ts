import { useRecoilCallback } from 'recoil';
import { BaselineParams, baselineSummariesState } from '@/models/baseline';
import { useSettings } from '@/models/setting';
import { useSaveBaseline } from './useSaveBaseline';

export const useCreateBaseline = () => {
  const { mainProject } = useSettings();
  const saveBaseline = useSaveBaseline();

  return useRecoilCallback(
    ({ set }) => {
      return async (params: BaselineParams) => {
        if (!mainProject) {
          throw new Error('Main project is not set');
        }

        const response = await saveBaseline({
          ...params,
          projectId: mainProject.id,
        });

        set(baselineSummariesState, (current) => [response, ...current]);

        return response;
      };
    },
    [mainProject, saveBaseline]
  );
};
