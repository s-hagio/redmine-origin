import { BaselineParams } from '@/models/baseline';
import { saveBaselineByParams } from '../saveBaselineByParams';

const createClientMock = vi.hoisted(() => vi.fn());

vi.mock('@/api', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  createClient: createClientMock,
}));

const apiPutMock = vi.fn().mockResolvedValue({});
const apiMocks = {
  baselines: {
    $post: vi.fn().mockResolvedValue({}),
    _id: vi.fn().mockReturnValue({ $put: apiPutMock }),
  },
};

createClientMock.mockReturnValue(apiMocks);

test('ID付きparamsを渡した場合は更新リクエストを送る', async () => {
  const params: BaselineParams = { id: 123, name: 'New Name!!' };

  await saveBaselineByParams(params);

  expect(apiMocks.baselines._id).toBeCalledWith(params.id);
  expect(apiPutMock).toBeCalledWith({ body: { baseline: { name: params.name } } });
});

test('IDなしparamsを渡した場合は新規作成リクエストを送る', async () => {
  const params: BaselineParams = { name: 'New Name!!' };

  await saveBaselineByParams(params);

  expect(apiMocks.baselines.$post).toBeCalledWith({ body: { baseline: params } });
});
