import { useRecoilCallback } from 'recoil';
import { createClient } from '@/api';
import { useSetCoreResourceEntities } from '@/models/coreResource';
import {
  storedIssueRelationsState,
  provisionalIssueRelationsState,
  IssueRelationParams,
  ProvisionalIssueRelation,
} from '@/models/issueRelation';
import { useNotifyIssueRelationError } from './useNotifyIssueRelationError';

type CreateIssueRelation = (issueRelationParams: IssueRelationParams) => Promise<void>;

let idCounter = 0;

export const useCreateIssueRelation = (): CreateIssueRelation => {
  const notifyIssueRelationError = useNotifyIssueRelationError();
  const setCoreResourceEntities = useSetCoreResourceEntities();

  return useRecoilCallback(
    ({ set }) => {
      return async (issueRelationParams) => {
        const provisionalIssueRelation: ProvisionalIssueRelation = {
          ...issueRelationParams,
          id: `provisional-${++idCounter}`,
        };

        set(provisionalIssueRelationsState, (current) => [...current, provisionalIssueRelation]);

        try {
          const { issueRelation, relatedResources } = await createClient().issue_relations.$post({
            body: { issueRelation: issueRelationParams },
          });

          set(storedIssueRelationsState, (current) => [...current, issueRelation]);
          setCoreResourceEntities(relatedResources);
        } catch (error) {
          notifyIssueRelationError(error);
          throw error;
        } finally {
          set(provisionalIssueRelationsState, (current) =>
            current.filter(({ id }) => id !== provisionalIssueRelation.id)
          );
        }
      };
    },
    [setCoreResourceEntities, notifyIssueRelationError]
  );
};
