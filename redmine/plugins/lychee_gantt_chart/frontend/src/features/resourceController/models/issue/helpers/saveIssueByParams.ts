import { createClient } from '@/api';
import { IssueParams, IssueWithRelatedResources } from '@/models/issue';

export const saveIssueByParams = (params: IssueParams): Promise<IssueWithRelatedResources> => {
  const client = createClient();
  const request = params.id ? client.issues._id(params.id).$put : client.issues.$post;

  return request({ body: { issue: params } });
};
