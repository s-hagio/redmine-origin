import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { AxiosError } from 'axios';
import { renderRecoilHook } from '@/test-utils';
import { issueEntityState } from '@/models/issue';
import { mainProject, otherTreeIssue, rootIssue, versionSharedByNone } from '@/__fixtures__';
import { useSaveIssue } from '../useSaveIssue';

const setCoreResourceEntitiesMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/coreResource/hooks/useSetCoreResourceEntities', () => ({
  useSetCoreResourceEntities: vi.fn().mockReturnValue(setCoreResourceEntitiesMock),
}));

const temporaryChangeCallbacksMock = vi.hoisted(() => ({
  setTemporaryChange: vi.fn(),
  removeTemporaryChange: vi.fn(),
}));

vi.mock('@/models/issue', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useCreateIssueTemporaryChangeCallbacks: vi.fn(() => () => temporaryChangeCallbacksMock),
}));

const openIssueFormMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/issueForm', () => ({
  useOpenIssueForm: vi.fn().mockReturnValue(openIssueFormMock),
}));

const saveIssueByParamsMock = vi.hoisted(() => vi.fn());

vi.mock('../../helpers', () => ({
  saveIssueByParams: saveIssueByParamsMock,
}));

const notifyIssueErrorMock = vi.hoisted(() => vi.fn());

vi.mock('../useNotifyIssueError', () => ({
  useNotifyIssueError: vi.fn().mockReturnValue(notifyIssueErrorMock),
}));

const issue = rootIssue;

const relatedResources = {
  projects: [mainProject],
  versions: [versionSharedByNone],
  issues: [otherTreeIssue],
};

const params = { id: issue.id, subject: 'foooooo!', startDate: '2040-01-03' };

beforeEach(() => {
  saveIssueByParamsMock.mockResolvedValue({ issue, relatedResources });
});

test('チケットを保存して返ってきたJSONで各stateを更新', async () => {
  const { result } = renderRecoilHook(() => ({
    save: useSaveIssue(),
    issue: useRecoilValue(issueEntityState(issue.id)),
  }));

  expect(saveIssueByParamsMock).not.toBeCalled();
  expect(result.current.issue).toBeNull();
  expect(setCoreResourceEntitiesMock).not.toBeCalled();

  await act(() => result.current.save(params));

  expect(saveIssueByParamsMock).toBeCalledWith(params);
  expect(result.current.issue).toStrictEqual(issue);
  expect(setCoreResourceEntitiesMock).toBeCalledWith(relatedResources);
});

test('APIリクエスト前にTemporaryChangeをセットしてレスポンスを受け取ったあとに削除する', async () => {
  const { result } = renderRecoilHook(() => useSaveIssue());

  expect(temporaryChangeCallbacksMock.setTemporaryChange).not.toBeCalled();
  expect(temporaryChangeCallbacksMock.removeTemporaryChange).not.toBeCalled();

  const promise = act(() => result.current(params));

  expect(temporaryChangeCallbacksMock.setTemporaryChange).toBeCalledWith(params);
  expect(temporaryChangeCallbacksMock.removeTemporaryChange).not.toBeCalled();

  await promise;

  expect(temporaryChangeCallbacksMock.removeTemporaryChange).toBeCalled();
});

describe('APIリクエストが失敗した時', () => {
  const error = new AxiosError('error');

  beforeEach(() => {
    saveIssueByParamsMock.mockRejectedValue(error);
  });

  test('エラーの通知', async () => {
    const { result } = renderRecoilHook(() => useSaveIssue());

    expect(notifyIssueErrorMock).not.toBeCalled();

    try {
      await result.current(params);
    } catch (error) {
      // noop
    }

    expect(notifyIssueErrorMock).toBeCalledWith(error, params.id);
  });

  test('IssueFormを開く場合はエラー通知をしない', async () => {
    const { result } = renderRecoilHook(() => useSaveIssue());

    expect(notifyIssueErrorMock).not.toBeCalled();

    try {
      await result.current(params, { shouldOpenFormOnError: true });
    } catch (error) {
      // noop
    }

    expect(notifyIssueErrorMock).not.toBeCalled();
  });

  test('IssueFormを開く場合でもalwaysNotifyOnErrorがtrueならエラー通知をする', async () => {
    const { result } = renderRecoilHook(() => useSaveIssue());

    expect(notifyIssueErrorMock).not.toBeCalled();

    try {
      await result.current(params, { shouldOpenFormOnError: true, alwaysNotifyOnError: true });
    } catch (error) {
      // noop
    }

    expect(notifyIssueErrorMock).toBeCalledWith(error, params.id);
  });

  test('オプションで指定されていればIssueFormを開く', async () => {
    const { result } = renderRecoilHook(() => useSaveIssue());

    expect(openIssueFormMock).not.toBeCalled();

    try {
      await result.current(params, { meta: { foo: 1 } });
    } catch (error) {
      // noop
    }

    expect(openIssueFormMock).not.toBeCalled();

    const formOpenOptions = {
      meta: { foo: 1 },
      shouldOpenFormOnError: true,
    };

    try {
      await result.current(params, formOpenOptions);
    } catch (error) {
      // noop
    }

    expect(openIssueFormMock).toBeCalledWith(params, error, formOpenOptions);
  });

  test('Errorを再throw', async () => {
    const { result } = renderRecoilHook(() => useSaveIssue());

    await expect(() => result.current(params)).rejects.toThrow(error);
  });

  test('TemporaryChangeを削除', async () => {
    const { result } = renderRecoilHook(() => useSaveIssue());

    try {
      await result.current(params);
    } catch (error) {
      // noop
    }

    expect(temporaryChangeCallbacksMock.removeTemporaryChange).toBeCalled();
  });
});
