import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { useIssue } from '@/models/issue';
import { leafIssue, rootIssue, rootProject, versionSharedByTree } from '@/__fixtures__';
import { useRefreshIssue } from '../useRefreshIssue';

const createClientMock = vi.hoisted(() => vi.fn());

vi.mock('@/api', () => ({
  createClient: createClientMock,
}));

const setCoreResourceEntitiesMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/coreResource/hooks/useSetCoreResourceEntities', () => ({
  useSetCoreResourceEntities: vi.fn().mockReturnValue(setCoreResourceEntitiesMock),
}));

const issue = leafIssue;
const updatedIssue = { ...issue, subject: issue.subject + ' updated!' };

const relatedResources = {
  projects: [rootProject],
  versions: [versionSharedByTree],
  issues: [rootIssue],
};

const apiMock = {
  issues: {
    _id: vi.fn().mockReturnValue({
      $get: vi.fn().mockResolvedValue({
        issue: updatedIssue,
        relatedResources,
      }),
    }),
  },
};
const getIssueMock = vi.hoisted(() => vi.fn());

beforeEach(() => {
  createClientMock.mockReturnValue(apiMock);
});

test('チケットと関連リソースの情報を取得して状態を更新する', async () => {
  const { result } = renderRecoilHook(() => ({
    refresh: useRefreshIssue(),
    issue: useIssue(issue.id),
  }));

  expect(getIssueMock).not.toBeCalled();
  expect(setCoreResourceEntitiesMock).not.toBeCalled();

  await act(() => result.current.refresh(issue.id));

  expect(apiMock.issues._id).toBeCalledWith(issue.id);
  expect(result.current.issue).toEqual(updatedIssue);
  expect(setCoreResourceEntitiesMock).toBeCalledWith(relatedResources);
});
