import { useCallback } from 'react';
import { BaselineParams, BaselineSummary } from '@/models/baseline';
import { useNotifyAPIError } from '@/features/notification';
import { saveBaselineByParams } from '../helpers';

type SaveBaseline = (params: BaselineParams) => Promise<BaselineSummary>;

export const useSaveBaseline = (): SaveBaseline => {
  const notifyAPIError = useNotifyAPIError();

  return useCallback(
    async (params) => {
      try {
        return await saveBaselineByParams(params);
      } catch (e) {
        notifyAPIError(e);
        throw e;
      }
    },
    [notifyAPIError]
  );
};
