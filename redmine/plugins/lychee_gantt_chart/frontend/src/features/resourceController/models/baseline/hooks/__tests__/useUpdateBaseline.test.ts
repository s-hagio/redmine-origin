import { useRecoilValue } from 'recoil';
import { act, waitFor } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { baselineSummariesState, BaselineSummary } from '@/models/baseline';
import { useUpdateBaseline } from '../useUpdateBaseline';

const saveBaselineMock = vi.hoisted(() => vi.fn());

vi.mock('../useSaveBaseline', () => ({
  useSaveBaseline: vi.fn().mockReturnValue(saveBaselineMock),
}));

test('saveBaselineを実行し、戻り値で既存のBaselineSummaryを更新する', async () => {
  const baselineSummary: BaselineSummary = {
    id: 111,
    name: 'My Baseline!',
    snappedAt: '2024-01-24T13:20:58.676Z',
  };

  const { result } = renderRecoilHook(
    () => ({
      update: useUpdateBaseline(),
      baselineSummaries: useRecoilValue(baselineSummariesState),
    }),
    {
      initializeState: ({ set }) => {
        set(baselineSummariesState, [baselineSummary]);
      },
    }
  );

  const params = { id: baselineSummary.id, name: 'Super Baseline' };
  const response = { ...baselineSummary, label: 'Super Baseline', snappedAt: '2024-01-25T13:22:23.567Z' };

  saveBaselineMock.mockResolvedValue(response);

  act(() => {
    result.current.update(params);
  });

  expect(saveBaselineMock).toBeCalledWith(params);

  await waitFor(() => {
    expect(result.current.baselineSummaries).toEqual([response]);
  });
});
