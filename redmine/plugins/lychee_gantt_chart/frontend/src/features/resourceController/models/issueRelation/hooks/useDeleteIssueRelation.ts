import { useRecoilCallback } from 'recoil';
import { createClient } from '@/api';
import { IssueRelationID, storedIssueRelationsState } from '@/models/issueRelation';
import { useNotifyIssueRelationError } from './useNotifyIssueRelationError';

type DeleteIssueRelation = (id: IssueRelationID) => Promise<void>;

export const useDeleteIssueRelation = (): DeleteIssueRelation => {
  const notifyIssueRelationError = useNotifyIssueRelationError();

  return useRecoilCallback(
    ({ set }) => {
      return async (id) => {
        const setDeleting = (isDeleting: boolean) => {
          set(storedIssueRelationsState, (issueRelations) => {
            const index = issueRelations.findIndex((issueRelation) => issueRelation.id === id);

            const newIssueRelations = [...issueRelations];
            newIssueRelations[index] = { ...issueRelations[index], isDeleting };

            return newIssueRelations;
          });
        };

        setDeleting(true);

        try {
          await createClient().issue_relations._id(id).$delete();

          set(storedIssueRelationsState, (issueRelations) =>
            issueRelations.filter((issueRelation) => issueRelation.id !== id)
          );
        } catch (error) {
          setDeleting(false);
          notifyIssueRelationError(error);
        }
      };
    },
    [notifyIssueRelationError]
  );
};
