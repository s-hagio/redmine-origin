import { useRecoilCallback } from 'recoil';
import { createClient } from '@/api';
import { useSetCoreResourceEntities } from '@/models/coreResource';
import { IssueID, IssueWithRelatedResources, issueEntityState } from '@/models/issue';

type RefreshIssue = (id: IssueID) => Promise<IssueWithRelatedResources>;

export const useRefreshIssue = (): RefreshIssue => {
  const setCoreResourceEntities = useSetCoreResourceEntities();

  return useRecoilCallback(
    ({ set }) => {
      return async (id) => {
        const response = await createClient().issues._id(id).$get();
        const { issue, relatedResources } = response;

        set(issueEntityState(issue.id), issue);
        setCoreResourceEntities(relatedResources);

        return response;
      };
    },
    [setCoreResourceEntities]
  );
};
