import { useRecoilCallback } from 'recoil';
import { createClient } from '@/api';
import { BaselineID, baselineSummariesState, selectedBaselineIdState } from '@/models/baseline';
import { useNotifyAPIError } from '@/features/notification';

export const useDeleteBaseline = () => {
  const notifyAPIError = useNotifyAPIError();

  return useRecoilCallback(
    ({ set }) => {
      return async (id: BaselineID) => {
        const setDeleting = (isDeleting: boolean) => {
          set(baselineSummariesState, (baselines) => {
            const newBaselines = [...baselines];
            const index = baselines.findIndex((baseline) => baseline.id === id);

            newBaselines[index] = { ...newBaselines[index], isDeleting };

            return newBaselines;
          });
        };

        setDeleting(true);
        set(selectedBaselineIdState, (currentId) => (currentId === id ? null : currentId));

        try {
          await createClient().baselines._id(id).$delete();

          set(baselineSummariesState, (baselines) => baselines.filter((baseline) => baseline.id !== id));
        } catch (e) {
          notifyAPIError(e);
          setDeleting(false);
          throw e;
        }
      };
    },
    [notifyAPIError]
  );
};
