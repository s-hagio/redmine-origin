import { useRecoilValue } from 'recoil';
import { act, waitFor } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { baselineSummariesState, BaselineSummary } from '@/models/baseline';
import { useCreateBaseline } from '../useCreateBaseline';

const saveBaselineMock = vi.hoisted(() => vi.fn());

vi.mock('../useSaveBaseline', () => ({
  useSaveBaseline: vi.fn().mockReturnValue(saveBaselineMock),
}));

test('projectIdを付与してsaveBaselineを実行し、戻り値をbaselineSummariesStateに追加', async () => {
  const otherBaselineSummary: BaselineSummary = {
    id: 1,
    name: 'Baseline!',
    snappedAt: '2024-01-24T13:20:58.676Z',
  };
  const { result } = renderRecoilHook(
    () => ({
      create: useCreateBaseline(),
      baselineSummaries: useRecoilValue(baselineSummariesState),
    }),
    {
      initializeState: ({ set }) => {
        set(settingsState, (current) => ({
          ...current,
          mainProject: { id: 123, identifier: 'pj-123' },
        }));
        set(baselineSummariesState, [otherBaselineSummary]);
      },
    }
  );
  const params = { name: 'Super Baseline' };
  const response = { id: 2, ...params, label: 'Super Baseline', snappedAt: '2024-01-24T13:22:23.567Z' };

  saveBaselineMock.mockResolvedValue(response);

  act(() => {
    result.current.create(params);
  });

  expect(saveBaselineMock).toBeCalledWith({ ...params, projectId: 123 });

  await waitFor(() => {
    expect(result.current.baselineSummaries).toEqual([response, otherBaselineSummary]);
  });
});

test('mainProjectが設定されていない場合、エラーをthrowする', () => {
  const { result } = renderRecoilHook(
    () => ({
      create: useCreateBaseline(),
    }),
    {
      initializeState: ({ set }) => {
        set(settingsState, (current) => ({
          ...current,
          mainProject: undefined,
        }));
      },
    }
  );

  expect(() => result.current.create({ name: 'Super Baseline' })).rejects.toThrow('Main project is not set');
});
