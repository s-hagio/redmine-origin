import { renderRecoilHook } from '@/test-utils';
import { useSaveBaseline } from '../useSaveBaseline';

const notifyAPIErrorMock = vi.hoisted(() => vi.fn());

vi.mock('@/features/notification', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useNotifyAPIError: vi.fn().mockReturnValue(notifyAPIErrorMock),
}));

const saveBaselineByParamsMock = vi.hoisted(() => vi.fn());

vi.mock('../../helpers', () => ({
  saveBaselineByParams: saveBaselineByParamsMock,
}));

test('paramsに応じた保存処理を実行する', async () => {
  const { result } = renderRecoilHook(() => useSaveBaseline());
  const params = { name: 'test' };
  const response = { id: 1, name: 'test' };

  saveBaselineByParamsMock.mockResolvedValue(response);

  expect(await result.current(params)).toBe(response);
  expect(saveBaselineByParamsMock).toBeCalledWith(params);
});

test('エラー発生時にはエラーを通知して例外を投げる', async () => {
  const { result } = renderRecoilHook(() => useSaveBaseline());
  const params = { id: 1, name: 'test' };

  const error = new Error('Error!!!');
  saveBaselineByParamsMock.mockRejectedValue(error);

  await expect(() => result.current(params)).rejects.toThrow(error);
  expect(notifyAPIErrorMock).toBeCalledWith(error);
});
