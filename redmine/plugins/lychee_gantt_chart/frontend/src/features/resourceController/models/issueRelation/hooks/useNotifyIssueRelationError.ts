import { useCallback } from 'react';
import { useNotifyAPIError } from '@/features/notification';

type NotifyIssueRelationError = (error: unknown) => void;

export const useNotifyIssueRelationError = (): NotifyIssueRelationError => {
  const notifyAPIError = useNotifyAPIError();

  return useCallback(
    (error) => {
      notifyAPIError(error);
    },
    [notifyAPIError]
  );
};
