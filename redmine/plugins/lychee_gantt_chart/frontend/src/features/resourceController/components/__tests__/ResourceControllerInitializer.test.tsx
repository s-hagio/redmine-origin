import * as React from 'react';
import { render } from '@testing-library/react';
import ResourceControllerInitializer from '../ResourceControllerInitializer';

vi.mock('../IssueFormEventHandlerRegisterer', () => ({
  default: vi.fn((props) => `IssueFormEventHandlerRegisterer: ${JSON.stringify(props)}`),
}));

test('Render', () => {
  const { container } = render(<ResourceControllerInitializer />);

  expect(container).toMatchSnapshot();
});
