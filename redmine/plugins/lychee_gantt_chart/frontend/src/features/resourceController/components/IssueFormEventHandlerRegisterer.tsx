import * as React from 'react';
import { IssueFormEventType, useRegisterIssueFormEventHandler } from '@/features/issueForm';
import { FEATURE_NAME } from '../constants';
import { useHandleIssueUpdateFromIssueForm } from '../hooks';

const IssueFormEventHandlerRegisterer: React.FC = () => {
  const registerIssueFormEventHandler = useRegisterIssueFormEventHandler();
  const handleIssueUpdateByIssueForm = useHandleIssueUpdateFromIssueForm();

  React.useEffect(() => {
    registerIssueFormEventHandler(IssueFormEventType.SuccessSave, FEATURE_NAME, handleIssueUpdateByIssueForm);
  }, [handleIssueUpdateByIssueForm, registerIssueFormEventHandler]);

  return null;
};

export default React.memo(IssueFormEventHandlerRegisterer);
