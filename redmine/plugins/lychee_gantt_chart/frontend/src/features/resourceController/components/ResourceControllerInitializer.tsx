import * as React from 'react';
import IssueFormEventHandlerRegisterer from './IssueFormEventHandlerRegisterer';

const ResourceControllerInitializer: React.FC = () => {
  return (
    <>
      <IssueFormEventHandlerRegisterer />
    </>
  );
};

export default React.memo(ResourceControllerInitializer);
