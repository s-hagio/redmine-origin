export const borderColor = '#ccc';
export const lightBorderColor = '#e6e6e6';

export const linkColor = '#0d679f';
export const primaryColor = '#0d679f';
export const secondaryColor = '#577585';

export const disabledTextColor = '#999';
export const errorTextColor = '#d9534f';

// Dialog
export const dialogBorderColor = '#767676';

// export const selectedBorderColor = 'rgba(13,103,159,0.71)';
export const selectedBorderColor = '#5caad5';
export const selectedTransparentBorderColor = 'rgba(50, 151, 204, 0.8)';
export const selectedBackgroundColor = '#ebf4fd';
export const selectedTransparentBackgroundColor = 'rgba(123, 186, 242, 0.15)';
