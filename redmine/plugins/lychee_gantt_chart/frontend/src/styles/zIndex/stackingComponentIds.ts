export const stackingComponentIds = [
  'timeline/ScrollButton',
  'gantt/Header',
  'dialog/Dialog',
  'queryForm/QueryForm',
  'issueCreation/IssueCreationPreparer',
  'dataTable/EditableCell',
  'tooltip/Tooltip',
  'resourceLoader/LoadingIndicator',
  'notification/NotificationContainer',
] as const;
