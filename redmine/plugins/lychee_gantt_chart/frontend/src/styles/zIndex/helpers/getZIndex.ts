import { UNIT_VALUE } from '../constants';
import { StackingComponentID } from '../types';
import { stackingComponentIds } from '../stackingComponentIds';

export const getZIndex = (id: StackingComponentID): number => {
  const index = stackingComponentIds.indexOf(id);

  return (index + 1) * UNIT_VALUE;
};
