import { getFrontZIndex } from '../getFrontZIndex';
import { stackingComponentIds } from '../../stackingComponentIds';

const firstStackingComponentId = stackingComponentIds[0];

test('指定StackingComponentの前面にくるz-indexを返す', () => {
  expect(getFrontZIndex(firstStackingComponentId)).toBe(110);
});
