import { StackingComponentID } from '../types';
import { INTERRUPT_VALUE } from '../constants';
import { getZIndex } from './getZIndex';

export const getBackZIndex = (id: StackingComponentID): number => {
  return getZIndex(id) - INTERRUPT_VALUE;
};
