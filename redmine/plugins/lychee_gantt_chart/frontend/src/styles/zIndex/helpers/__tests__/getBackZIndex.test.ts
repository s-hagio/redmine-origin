import { getBackZIndex } from '../getBackZIndex';
import { stackingComponentIds } from '../../stackingComponentIds';

const firstStackingComponentId = stackingComponentIds[0];

test('指定StackingComponentの後面にくるz-indexを返す', () => {
  expect(getBackZIndex(firstStackingComponentId)).toBe(90);
});
