import { getZIndex } from '../getZIndex';
import { stackingComponentIds } from '../../stackingComponentIds';

const firstStackingComponentId = stackingComponentIds[0];

test('IDに応じたz-indexを返す', () => {
  expect(getZIndex(firstStackingComponentId)).toBe(100);
});
