import { stackingComponentIds } from './stackingComponentIds';

export type StackingComponentID = typeof stackingComponentIds[number];
