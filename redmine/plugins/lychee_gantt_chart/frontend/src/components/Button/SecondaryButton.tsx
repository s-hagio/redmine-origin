import * as React from 'react';
import { css, cx } from '@linaria/core';
import { borderColor, linkColor } from '@/styles';
import { DEFAULT_TYPE } from './constants';
import NakedButton from './NakedButton';

type Props = JSX.IntrinsicElements['button'];

const SecondaryButton: React.FC<Props> = ({ type, className, ...props }) => {
  return <NakedButton type={type || DEFAULT_TYPE} className={cx(style, className)} {...props} />;
};

export default React.memo(SecondaryButton);

const style = css`
  padding: 0.35em 0.5em;
  border: 1px solid ${borderColor};
  border-radius: 3px;
  background: #fff;
  line-height: 1;
  color: ${linkColor};
`;
