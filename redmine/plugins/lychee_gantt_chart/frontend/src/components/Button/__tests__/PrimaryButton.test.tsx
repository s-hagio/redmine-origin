import * as React from 'react';
import { render } from '@testing-library/react';
import PrimaryButton from '../PrimaryButton';

test('Render', () => {
  const result = render(<PrimaryButton>LABEL!</PrimaryButton>);

  expect(result.container).toMatchSnapshot();
});

test('Render with props', () => {
  const result = render(
    <PrimaryButton type="submit" className="test-class">
      LABEL!
    </PrimaryButton>
  );

  expect(result.container).toMatchSnapshot();
});
