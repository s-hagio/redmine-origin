import * as React from 'react';
import { render } from '@testing-library/react';
import SecondaryButton from '../SecondaryButton';

test('Render', () => {
  const result = render(<SecondaryButton>LABEL!</SecondaryButton>);

  expect(result.container).toMatchSnapshot();
});

test('Render with props', () => {
  const result = render(
    <SecondaryButton type="submit" className="test-class">
      LABEL!
    </SecondaryButton>
  );

  expect(result.container).toMatchSnapshot();
});
