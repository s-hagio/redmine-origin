import * as React from 'react';
import { render } from '@testing-library/react';
import NakedButton from '../NakedButton';

test('Render', () => {
  const result = render(<NakedButton>LABEL!</NakedButton>);

  expect(result.container).toMatchSnapshot();
});

test('Render with props', () => {
  const result = render(
    <NakedButton type="submit" className="test-class">
      LABEL!
    </NakedButton>
  );

  expect(result.container).toMatchSnapshot();
});
