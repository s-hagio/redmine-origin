import * as React from 'react';
import { css, cx } from '@linaria/core';
import { DEFAULT_TYPE } from './constants';

type Props = JSX.IntrinsicElements['button'];

const NakedButton: React.FC<Props> = ({ type, className, ...props }) => {
  return <button type={type || DEFAULT_TYPE} className={cx(style, className)} {...props} />;
};

export default React.memo(NakedButton);

const style = css`
  padding: 0;
  border: none;
  background: transparent;
  word-break: keep-all;

  &[type='submit'] {
    height: auto;
  }

  &[type='button'],
  &[type='submit'] {
    display: inline-flex;
    align-items: center;
    cursor: pointer;
  }
`;
