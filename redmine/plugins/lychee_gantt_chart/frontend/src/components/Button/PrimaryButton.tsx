import * as React from 'react';
import { css, cx } from '@linaria/core';
import { DEFAULT_TYPE } from './constants';
import NakedButton from './NakedButton';

type Props = JSX.IntrinsicElements['button'];

const PrimaryButton: React.FC<Props> = ({ type, className, ...props }) => {
  return <NakedButton type={type || DEFAULT_TYPE} className={cx(style, className)} {...props} />;
};

export default React.memo(PrimaryButton);

const style = css`
  &,
  &[type='submit'] {
    height: 24px;
    padding: 0.35em 0.75em;
    border-radius: 3px;
    background-color: #666;
    color: #fff;
  }
`;
