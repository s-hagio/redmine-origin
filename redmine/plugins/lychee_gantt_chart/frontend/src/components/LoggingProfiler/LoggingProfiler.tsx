import * as React from 'react';

type Props = Omit<React.ProfilerProps, 'onRender'>;

const VERY_SLOW_THRESHOLD = 300;
const SLOW_THRESHOLD = 100;

const VERY_SLOW_COLOR = '#f00';
const SLOW_COLOR = '#d78c00';
const NORMAL_COLOR = '#000';

const LoggingProfiler: React.FC<Props> = ({ children, ...props }) => {
  const onRender = React.useCallback<React.ProfilerOnRenderCallback>((id, phase, actualDuration) => {
    const isVerySlow = actualDuration >= VERY_SLOW_THRESHOLD;
    const isSlow = actualDuration >= SLOW_THRESHOLD;

    const durationColor = isVerySlow ? VERY_SLOW_COLOR : isSlow ? SLOW_COLOR : NORMAL_COLOR;

    console.info(
      `%c[Profiler]%c ${phase}\t${id}\t%c[${actualDuration.toFixed(2)}ms]`,
      'color: #00f; font-weight: bold',
      '',
      `color: ${durationColor}; font-weight: bold`
    );
  }, []);

  return (
    <React.Profiler {...props} onRender={onRender}>
      {children}
    </React.Profiler>
  );
};

export default LoggingProfiler;
