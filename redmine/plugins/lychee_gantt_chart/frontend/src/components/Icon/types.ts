export const IconNameMap = {
  chevronLeft: 'chevron_left',
  chevronRight: 'chevron_right',
  chevronDown: 'expand_more',

  doubleArrowLeft: 'keyboard_double_arrow_left',
  doubleArrowRight: 'keyboard_double_arrow_right',

  close: 'close',
  start: 'start',
  tune: 'tune',
  add: 'add',
  edit: 'edit',
  delete: 'delete',
  remove: 'remove',
  selectAll: 'select_all',
  zoomOut: 'zoom_out',
  zoomIn: 'zoom_in',
  filter: 'filter_alt',
  moreHoriz: 'more_horiz',
  check: 'check',
  error: 'error',
  warning: 'warning',
  info: 'info',

  calendar: 'calendar_today',
  verticalAlignCenter: 'vertical_align_center',

  dragIndicator: 'drag_indicator',

  baseline: 'view_timeline',
} as const;

export type IconName = keyof typeof IconNameMap;

export const RedmineIconNameMap = {
  project: 'projects',
  version: 'package',
  issue: 'ticket',
  issueGo: 'ticket_go',
  togglePlus: 'bullet_toggle_plus',
  toggleMinus: 'bullet_toggle_minus',
  check: 'toggle_check',
  reload: 'reload',
  save: 'save',
} as const;

export type RedmineIconName = keyof typeof RedmineIconNameMap;
