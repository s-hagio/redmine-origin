import * as React from 'react';
import { css, cx } from '@linaria/core';
import { IconNameMap } from './types';
import type { IconName } from './types';

export type Props = React.ComponentProps<'span'> & {
  name: IconName;
  size?: number;
  deg?: number;
};

const DEFAULT_SIZE = 18;

const Icon: React.FC<Props> = ({ name, size, deg, className, ...props }) => {
  const iconStyles: React.CSSProperties = {};

  if (size != null && size !== DEFAULT_SIZE) {
    iconStyles.fontSize = `${size}px`;
  }

  if (deg != null) {
    iconStyles.transform = `rotate(${deg}deg)`;
  }

  return (
    <span className={cx(style, className)} {...props}>
      <span className="material-symbols-outlined" style={iconStyles}>
        {IconNameMap[name]}
      </span>
    </span>
  );
};

export default React.memo(Icon);

export const style = css`
  display: inline-flex;
  align-items: center;

  .material-symbols-outlined {
    font-size: ${DEFAULT_SIZE}px;
  }
`;
