import * as React from 'react';
import { render } from '@testing-library/react';
import { Settings, useSettings } from '@/models/setting';
import RedmineIcon from '../RedmineIcon';

vi.mock('@/models/setting', async () => ({
  ...(await vi.importActual<object>('@/models/setting')),
  useSettings: vi.fn(),
}));

beforeEach(() => {
  vi.mocked(useSettings).mockReturnValue({ baseURL: '/path/to/base_url' } as Settings);
});

test('Render', () => {
  const result = render(<RedmineIcon name="issue" />);

  expect(result.container).toMatchSnapshot();
});

test('Render with optional props', () => {
  const result = render(<RedmineIcon name="version" alt="バージョン" width={123} height={456} />);

  expect(result.container).toMatchSnapshot();
});
