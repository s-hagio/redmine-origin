import * as React from 'react';
import { render } from '@testing-library/react';
import Icon from '../Icon';

test('Render', () => {
  const result = render(<Icon name="chevronRight" />);

  expect(result.container).toMatchSnapshot();
});

test('Render with optional props', () => {
  const result = render(<Icon name="chevronRight" size={333} deg={-90} />);

  expect(result.container).toMatchSnapshot();
});
