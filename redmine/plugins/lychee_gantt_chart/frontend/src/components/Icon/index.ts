export * from './types';

export { default as Icon, style as iconStyle } from './Icon';
export type { Props as IconProps } from './Icon';

export { default as RedmineIcon } from './RedmineIcon';
export type { Props as RedmineIconProps } from './RedmineIcon';
