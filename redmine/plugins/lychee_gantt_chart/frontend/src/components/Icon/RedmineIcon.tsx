import * as React from 'react';
import { cx } from '@linaria/core';
import { useSettings } from '@/models/setting';
import { style } from './Icon';
import { RedmineIconNameMap } from './types';
import type { RedmineIconName } from './types';

export type Props = React.ComponentProps<'span'> & {
  name: RedmineIconName;
  alt?: string;
  width?: number;
  height?: number;
};

const DEFAULT_WIDTH = 16;

const RedmineIcon: React.FC<Props> = ({ name, alt, width, height, className, ...props }) => {
  const { baseURL } = useSettings();

  return (
    <span className={cx(style, className)} {...props}>
      <img
        src={`${baseURL}/images/${RedmineIconNameMap[name]}.png`}
        alt={alt ?? ''}
        width={width ?? DEFAULT_WIDTH}
        height={height}
      />
    </span>
  );
};

export default React.memo(RedmineIcon);
