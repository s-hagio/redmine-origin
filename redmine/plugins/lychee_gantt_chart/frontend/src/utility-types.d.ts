declare type KeysByType<O, Type> = NonNullable<keyof PickByType<O, Type>>;

declare type PickByType<O, Type> = Pick<
  O,
  {
    [K in keyof O]: O[K] extends Type ? K : never;
  }[keyof O]
>;
