export type CustomFieldID = number;
export type CustomFieldName = `cf_${CustomFieldID}`;

export const CustomFieldFormat = {
  Enumeration: 'enumeration',
  String: 'string',
  Version: 'version',
  Attachment: 'attachment',
  User: 'user',
  List: 'list',
  Link: 'link',
  Float: 'float',
  Int: 'int',
  Date: 'date',
  Bool: 'bool',
  Text: 'text',
} as const;

export type CustomFieldFormat = (typeof CustomFieldFormat)[keyof typeof CustomFieldFormat];

export type CustomField = {
  id: CustomFieldID;
  name: CustomFieldName;
  label: string;
  format: CustomFieldFormat;
  groupable: boolean;
  sortable: boolean;
  minLength: number | null;
  maxLength: number | null;
  isFullTextFormatting: boolean;
  isMultiple: boolean;
  isBulkEditSupported: boolean;
  userRoleIds: number[] | null;
  isForAll: boolean;
  projectIds: number[];
  trackerIds: number[];
};

export type CustomFieldValue<T = string | number | boolean | null> = T | T[];
