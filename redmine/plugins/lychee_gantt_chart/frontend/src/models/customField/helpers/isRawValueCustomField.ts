import { CustomField } from '../types';

const rawValueFormats: ReadonlyArray<string> = ['int', 'float', 'date', 'bool'] as const;
const textFormats: ReadonlyArray<string> = ['string', 'text'] as const;

export const isRawValueCustomField = (customField: CustomField): boolean => {
  return (
    rawValueFormats.includes(customField.format) ||
    (textFormats.includes(customField.format) && !customField.isFullTextFormatting)
  );
};
