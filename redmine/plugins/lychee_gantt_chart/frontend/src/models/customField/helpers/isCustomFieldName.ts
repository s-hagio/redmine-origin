import { CustomFieldName } from '../types';

export const isCustomFieldName = (fieldName: string): fieldName is CustomFieldName => {
  return fieldName.startsWith('cf_');
};
