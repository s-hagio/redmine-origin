import {
  stringCustomField,
  intCustomField,
  floatCustomField,
  boolCustomField,
  textCustomField,
  formattableStringCustomField,
  formattableTextCustomField,
  enumerationCustomField,
  versionCustomField,
  attachmentCustomField,
  userCustomField,
  listCustomField,
  linkCustomField,
  dateCustomField,
} from '@/__fixtures__';
import { isRawValueCustomField } from '../isRawValueCustomField';

test('サーバー側でフォーマットしないカスタムフィールドの場合、trueを返す', () => {
  expect(isRawValueCustomField(stringCustomField)).toBe(true);
  expect(isRawValueCustomField(intCustomField)).toBe(true);
  expect(isRawValueCustomField(floatCustomField)).toBe(true);
  expect(isRawValueCustomField(dateCustomField)).toBe(true);
  expect(isRawValueCustomField(boolCustomField)).toBe(true);
  expect(isRawValueCustomField(textCustomField)).toBe(true);
});

test('それ以外はfalseを返す', () => {
  expect(isRawValueCustomField(enumerationCustomField)).toBe(false);
  expect(isRawValueCustomField(versionCustomField)).toBe(false);
  expect(isRawValueCustomField(attachmentCustomField)).toBe(false);
  expect(isRawValueCustomField(userCustomField)).toBe(false);
  expect(isRawValueCustomField(listCustomField)).toBe(false);
  expect(isRawValueCustomField(linkCustomField)).toBe(false);
  expect(isRawValueCustomField(formattableStringCustomField)).toBe(false);
  expect(isRawValueCustomField(formattableTextCustomField)).toBe(false);
});
