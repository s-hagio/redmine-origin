import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { enumerationCustomField, textCustomField } from '@/__fixtures__';
import { customFieldsState } from '../customFieldsState';
import { customFieldMapState } from '../customFieldMapState';

test('idとnameをキーにしたCustomFieldのMapを返す', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(customFieldMapState), {
    initializeState: ({ set }) => {
      set(customFieldsState, [textCustomField, enumerationCustomField]);
    },
  });

  expect(result.current.get(textCustomField.id)).toBe(textCustomField);
  expect(result.current.get(textCustomField.name)).toBe(textCustomField);
  expect(result.current.get(enumerationCustomField.id)).toBe(enumerationCustomField);
  expect(result.current.get(enumerationCustomField.name)).toBe(enumerationCustomField);
});
