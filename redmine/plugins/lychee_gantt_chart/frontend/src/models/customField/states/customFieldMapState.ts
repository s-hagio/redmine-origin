import { selector } from 'recoil';
import { CustomField, CustomFieldID, CustomFieldName } from '../types';
import { customFieldsState } from './customFieldsState';

export const customFieldMapState = selector<ReadonlyMap<CustomFieldID | CustomFieldName, CustomField>>({
  key: 'customFieldMap',
  get: ({ get }) => {
    const customFields = get(customFieldsState);
    const map = new Map();

    customFields.forEach((customField) => {
      map.set(customField.id, customField);
      map.set(customField.name, customField);
    });

    return map;
  },
});
