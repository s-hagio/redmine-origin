import { atom, selector } from 'recoil';
import { createClient } from '@/api';
import { CustomField } from '../types';

export const customFieldsState = atom<CustomField[]>({
  key: 'customFields',
  default: selector({
    key: 'customFields/default',
    get: () => {
      return createClient().custom_fields.$get();
    },
  }),
});
