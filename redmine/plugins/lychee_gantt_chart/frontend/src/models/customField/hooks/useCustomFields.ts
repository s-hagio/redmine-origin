import { useRecoilValue } from 'recoil';
import { customFieldsState } from '../states';

export const useCustomFields = () => useRecoilValue(customFieldsState);
