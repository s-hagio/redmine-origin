import { useRecoilValue } from 'recoil';
import { customFieldMapState } from '../states';

export const useCustomFieldMap = () => useRecoilValue(customFieldMapState);
