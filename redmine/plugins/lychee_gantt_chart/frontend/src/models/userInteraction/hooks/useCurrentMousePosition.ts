import { useRecoilValue } from 'recoil';
import { currentMousePositionState } from '../states';

export const useCurrentMousePosition = () => useRecoilValue(currentMousePositionState);
