import { useRecoilCallback } from 'recoil';
import { currentMousePositionState } from '../states';

export const useRefreshCurrentMousePosition = () => {
  return useRecoilCallback(({ set }) => {
    return (event: MouseEvent) => {
      set(currentMousePositionState, {
        x: event.clientX,
        y: event.clientY,
      });
    };
  });
};
