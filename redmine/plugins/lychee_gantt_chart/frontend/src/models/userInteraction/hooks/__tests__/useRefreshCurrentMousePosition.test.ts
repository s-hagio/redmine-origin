import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { currentMousePositionState } from '../../states';
import { useRefreshCurrentMousePosition } from '../useRefreshCurrentMousePosition';

test('MouseEventからcurrentMousePositionStateを更新する', () => {
  const { result } = renderRecoilHook(() => ({
    refresh: useRefreshCurrentMousePosition(),
    currentMousePosition: useRecoilValue(currentMousePositionState),
  }));

  expect(result.current.currentMousePosition).toStrictEqual({ x: -1, y: -1 });

  act(() => result.current.refresh({ clientX: 100, clientY: 200 } as MouseEvent));

  expect(result.current.currentMousePosition).toStrictEqual({ x: 100, y: 200 });
});
