import { atom } from 'recoil';
import { MousePosition } from '../types';

export const currentMousePositionState = atom<MousePosition>({
  key: 'userInteraction/currentMousePosition',
  default: { x: -1, y: -1 },
});
