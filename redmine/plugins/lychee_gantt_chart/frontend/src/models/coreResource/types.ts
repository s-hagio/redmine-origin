import { ProjectID, ProjectCore } from '@/models/project';
import { VersionID, VersionCore } from '@/models/version';
import { IssueID, IssueCore } from '@/models/issue';
import { GroupID } from '@/models/group';

export type CoreResourceCollection = {
  projects: ProjectCore[];
  versions: VersionCore[];
  issues: IssueCore[];
};

export const CoreResourceType = {
  Project: 'project',
  Version: 'version',
  Issue: 'issue',
  Group: 'group',
} as const;

export type CoreResourceType = (typeof CoreResourceType)[keyof typeof CoreResourceType];

export type ProjectResourceIdentifier = `project-${ProjectID}`;
export type VersionResourceIdentifier = `version-${ProjectID}-${VersionID}`;
export type IssueResourceIdentifier = `issue-${IssueID}`;
export type GroupResourceIdentifier = `group-${GroupID}`;

export type CoreResourceIdentifier =
  | ProjectResourceIdentifier
  | VersionResourceIdentifier
  | IssueResourceIdentifier
  | GroupResourceIdentifier;

export type CoreResourceID = ProjectID | VersionID | IssueID | GroupID;

export type IdentifiableCoreResource = {
  type: CoreResourceType;
  id: CoreResourceID;
};
