import { selector, waitForAll } from 'recoil';
import { createClient } from '@/api';
import { associatedResourceCollectionState } from '@/models/associatedResource';
import { customFieldsState } from '@/models/customField';
import { issueQueryParamsState } from '@/models/query';
import { CoreResourceCollection } from '../types';

export const syncedCoreResourceCollectionWithDependenciesState = selector<CoreResourceCollection>({
  key: 'syncedCoreResourceCollectionWithDependencies',
  get: ({ get }) => {
    const [collection] = get(
      waitForAll([syncedCoreResourceCollectionState, associatedResourceCollectionState, customFieldsState])
    );

    return collection;
  },
});

const syncedCoreResourceCollectionState = selector({
  key: 'syncedCoreResourceCollection',
  get: async ({ get }) => {
    const query = get(issueQueryParamsState);

    return createClient().core_resources.$get({ query });
  },
});
