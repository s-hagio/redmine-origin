import { atom } from 'recoil';
import { CoreResourceCollection } from '../types';

export const coreResourceCollectionState = atom<CoreResourceCollection>({
  key: 'coreResourceCollection',
  default: {
    projects: [],
    versions: [],
    issues: [],
  },
});
