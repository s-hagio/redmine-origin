import { useRecoilTransaction_UNSTABLE } from 'recoil';
import { Issue, issueEntityState } from '@/models/issue';
import { coreResourceCollectionState } from '../states';

type AddIssue = (newIssue: Issue) => void;

export const useAddIssue = (): AddIssue => {
  return useRecoilTransaction_UNSTABLE(({ set }) => {
    return (newIssue) => {
      set(coreResourceCollectionState, (collection) => {
        if (collection.issues.some(({ id }) => id === newIssue.id)) {
          return collection;
        }

        return {
          ...collection,
          issues: [...collection.issues, newIssue],
        };
      });

      set(issueEntityState(newIssue.id), newIssue);
    };
  });
};
