import { useRecoilTransaction_UNSTABLE } from 'recoil';
import { Project, projectState } from '@/models/project';
import { Version, versionState } from '@/models/version';
import { Issue, issueEntityState } from '@/models/issue';
import { coreResourceCollectionState } from '../states';

type CoreResourceEntities = {
  projects?: Project[];
  versions?: Version[];
  issues?: Issue[];
};

type SetCoreResourceEntities = (resourceEntities: CoreResourceEntities) => void;

export const useSetCoreResourceEntities = (): SetCoreResourceEntities => {
  return useRecoilTransaction_UNSTABLE(({ get, set }) => {
    return (resourceEntities) => {
      const collection = get(coreResourceCollectionState);

      const newProjects = [...collection.projects];
      const newVersions = [...collection.versions];
      const newIssues = [...collection.issues];

      (resourceEntities.projects ?? []).forEach((project) => {
        const index = newProjects.findIndex(({ id }) => id === project.id);

        if (index !== -1) {
          newProjects[index] = { ...newProjects[index], ...project };
        }

        set(projectState(project.id), (current) => ({ ...current, ...project }));
      });

      (resourceEntities.versions ?? []).forEach((version) => {
        const index = newVersions.findIndex(({ id }) => id === version.id);

        if (index !== -1) {
          newVersions[index] = { ...newVersions[index], ...version };
        }

        set(versionState(version.id), (current) => ({ ...current, ...version }));
      });

      (resourceEntities.issues ?? []).forEach((issue) => {
        const index = newIssues.findIndex(({ id }) => id === issue.id);

        if (index !== -1) {
          newIssues[index] = { ...newIssues[index], ...issue };
        }

        set(issueEntityState(issue.id), (current) => ({ ...current, ...issue }));
      });

      set(coreResourceCollectionState, {
        projects: newProjects,
        versions: newVersions,
        issues: newIssues,
      });
    };
  }, []);
};
