import { useRecoilCallback } from 'recoil';
import { CoreResourceCollection } from '../types';
import { coreResourceCollectionState } from '../states';
import { useSetCoreResourceEntities } from './useSetCoreResourceEntities';

type RefreshCoreResources = (collection: CoreResourceCollection) => void;

export const useRefreshCoreResources = (): RefreshCoreResources => {
  const setCoreResourceEntities = useSetCoreResourceEntities();

  return useRecoilCallback(
    ({ set }) => {
      return (collection) => {
        set(coreResourceCollectionState, collection);
        setCoreResourceEntities(collection);
      };
    },
    [setCoreResourceEntities]
  );
};
