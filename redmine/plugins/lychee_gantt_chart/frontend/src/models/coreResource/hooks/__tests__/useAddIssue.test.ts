import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { issueEntityState } from '@/models/issue';
import { rootIssue } from '@/__fixtures__';
import { coreResourceCollectionState } from '../../states';
import { useAddIssue } from '../useAddIssue';

const issue = rootIssue;

test('新しいIssueを追加する', () => {
  const { result } = renderRecoilHook(
    () => ({
      add: useAddIssue(),
      collection: useRecoilValue(coreResourceCollectionState),
      entity: useRecoilValue(issueEntityState(issue.id)),
    }),
    {
      initializeState: ({ set }) => {
        set(coreResourceCollectionState, { projects: [], versions: [], issues: [] });
      },
    }
  );

  expect(result.current.collection.issues).toHaveLength(0);
  expect(result.current.entity).toBeNull();

  act(() => result.current.add(issue));

  expect(result.current.collection.issues).toEqual([issue]);
  expect(result.current.entity).toEqual(issue);
});
