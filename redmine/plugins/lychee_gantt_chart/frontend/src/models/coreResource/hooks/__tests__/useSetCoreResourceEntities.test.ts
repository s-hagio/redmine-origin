import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { projectState } from '@/models/project';
import { versionState } from '@/models/version';
import { issueEntityState } from '@/models/issue';
import { mainProject, rootIssue, versionSharedByNone } from '@/__fixtures__';
import { coreResourceCollectionState } from '../../states';
import { useSetCoreResourceEntities } from '../useSetCoreResourceEntities';

test('CoreResourceCollectionを元に個別にCoreResourceの各stateを更新する', () => {
  const { result } = renderRecoilHook(() => ({
    set: useSetCoreResourceEntities(),
    project: useRecoilValue(projectState(mainProject.id)),
    version: useRecoilValue(versionState(versionSharedByNone.id)),
    issue: useRecoilValue(issueEntityState(rootIssue.id)),
  }));

  expect(result.current.project).toBeNull();
  expect(result.current.version).toBeNull();
  expect(result.current.issue).toBeNull();

  act(() => {
    result.current.set({
      projects: [mainProject],
      versions: [versionSharedByNone],
      issues: [rootIssue],
    });
  });

  expect(result.current.project).toStrictEqual(mainProject);
  expect(result.current.version).toStrictEqual(versionSharedByNone);
  expect(result.current.issue).toStrictEqual(rootIssue);
});

test('既にリソースデータが存在する場合はマージする', () => {
  const { result } = renderRecoilHook(
    () => ({
      set: useSetCoreResourceEntities(),
      collection: useRecoilValue(coreResourceCollectionState),
    }),
    {
      initializeState: ({ set }) => {
        set(coreResourceCollectionState, {
          projects: [mainProject],
          versions: [versionSharedByNone],
          issues: [rootIssue],
        });
      },
    }
  );

  const newResourceEntities = {
    projects: [{ ...mainProject, name: 'new project name' }],
    versions: [{ ...versionSharedByNone, name: 'new version name' }],
    issues: [{ ...rootIssue, subject: 'new issue subject' }],
  };

  act(() => result.current.set(newResourceEntities));

  expect(result.current.collection).toStrictEqual(newResourceEntities);
});
