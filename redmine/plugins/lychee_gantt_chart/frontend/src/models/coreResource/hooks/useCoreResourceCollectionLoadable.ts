import { useLoadRecoilValue } from '@/lib/hooks';
import { syncedCoreResourceCollectionWithDependenciesState } from '../states';

export const useCoreResourceCollectionLoadable = () => {
  return useLoadRecoilValue(syncedCoreResourceCollectionWithDependenciesState);
};
