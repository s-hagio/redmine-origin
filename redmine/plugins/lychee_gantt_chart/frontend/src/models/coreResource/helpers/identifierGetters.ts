import { ProjectID } from '@/models/project';
import { VersionID } from '@/models/version';
import { IssueID } from '@/models/issue';
import { GroupID } from '@/models/group';
import {
  CoreResourceType,
  ProjectResourceIdentifier,
  VersionResourceIdentifier,
  IssueResourceIdentifier,
  GroupResourceIdentifier,
} from '../types';

export const getProjectResourceIdentifier = (projectId: ProjectID): ProjectResourceIdentifier => {
  return `${CoreResourceType.Project}-${projectId}`;
};

export const getVersionResourceIdentifier = (
  projectId: ProjectID,
  versionId: VersionID
): VersionResourceIdentifier => {
  return `${CoreResourceType.Version}-${projectId}-${versionId}`;
};

export const getIssueResourceIdentifier = (id: IssueID): IssueResourceIdentifier => {
  return `${CoreResourceType.Issue}-${id}`;
};

export const getGroupResourceIdentifier = (id: GroupID): GroupResourceIdentifier => {
  return `${CoreResourceType.Group}-${id}`;
};
