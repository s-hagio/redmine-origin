import { CoreResourceType, IssueResourceIdentifier } from '../types';
import { isIdentifierOf } from './isIdentifierOf';

export const isIssueResourceIdentifier = (identifier: unknown): identifier is IssueResourceIdentifier => {
  return isIdentifierOf(identifier, CoreResourceType.Issue);
};
