import { CoreResourceType, ProjectResourceIdentifier } from '../types';
import { isIdentifierOf } from './isIdentifierOf';

export const isProjectResourceIdentifier = (identifier: unknown): identifier is ProjectResourceIdentifier => {
  return isIdentifierOf(identifier, CoreResourceType.Project);
};
