import { ProjectID } from '@/models/project';
import { VersionID } from '@/models/version';
import { IssueID } from '@/models/issue';
import { GroupID } from '@/models/group';
import {
  ProjectResourceIdentifier,
  VersionResourceIdentifier,
  IssueResourceIdentifier,
  CoreResourceIdentifier,
  CoreResourceID,
  GroupResourceIdentifier,
} from '../types';
import { isGroupResourceIdentifier } from './isGroupResourceIdentifier';

function getResourceIdFromIdentifier(identifier: ProjectResourceIdentifier): ProjectID;
function getResourceIdFromIdentifier(identifier: VersionResourceIdentifier): VersionID;
function getResourceIdFromIdentifier(identifier: IssueResourceIdentifier): IssueID;
function getResourceIdFromIdentifier(identifier: GroupResourceIdentifier): GroupID;

function getResourceIdFromIdentifier(identifier: CoreResourceIdentifier): CoreResourceID {
  const parts = identifier.split('-');

  if (isGroupResourceIdentifier(identifier)) {
    return parts[parts.length - 1];
  }

  return +parts[parts.length - 1];
}

export { getResourceIdFromIdentifier };
