import { CoreResourceType, GroupResourceIdentifier } from '../types';
import { isIdentifierOf } from './isIdentifierOf';

export const isGroupResourceIdentifier = (identifier: unknown): identifier is GroupResourceIdentifier => {
  return isIdentifierOf(identifier, CoreResourceType.Group);
};
