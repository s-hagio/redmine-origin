import {
  projectResourceIdentifier,
  versionResourceIdentifier,
  issueResourceIdentifier,
  groupResourceIdentifier,
} from '@/__fixtures__';
import { isVersionResourceIdentifier } from '../isVersionResourceIdentifier';

test('Versionの識別子ならtrue', () => {
  expect(isVersionResourceIdentifier(versionResourceIdentifier)).toBe(true);
});

test('それ以外ならfalse', () => {
  expect(isVersionResourceIdentifier(projectResourceIdentifier)).toBe(false);
  expect(isVersionResourceIdentifier(issueResourceIdentifier)).toBe(false);
  expect(isVersionResourceIdentifier(groupResourceIdentifier)).toBe(false);
});
