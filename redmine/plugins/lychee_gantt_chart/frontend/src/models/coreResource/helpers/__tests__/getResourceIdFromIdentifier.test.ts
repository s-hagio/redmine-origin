import { getResourceIdFromIdentifier } from '../getResourceIdFromIdentifier';

test('ProjectResourceIdentifier to ProjectID', () => {
  expect(getResourceIdFromIdentifier('project-1')).toBe(1);
});

test('VersionResourceIdentifier to VersionID', () => {
  expect(getResourceIdFromIdentifier('version-1-2')).toBe(2);
});

test('IssueResourceIdentifier to IssueID', () => {
  expect(getResourceIdFromIdentifier('issue-1')).toBe(1);
});

test('GroupResourceIdentifier to GroupID', () => {
  expect(getResourceIdFromIdentifier('group-status-new')).toBe('new');
});
