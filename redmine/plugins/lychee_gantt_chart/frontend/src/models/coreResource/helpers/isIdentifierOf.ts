import { CoreResourceType } from '../types';

export const isIdentifierOf = (identifier: unknown, resourceType: CoreResourceType) => {
  const [type] = (identifier + '').split('-');

  return type === resourceType;
};
