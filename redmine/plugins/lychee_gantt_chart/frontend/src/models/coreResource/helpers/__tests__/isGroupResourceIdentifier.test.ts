import {
  projectResourceIdentifier,
  versionResourceIdentifier,
  issueResourceIdentifier,
  groupResourceIdentifier,
} from '@/__fixtures__';
import { isGroupResourceIdentifier } from '../isGroupResourceIdentifier';

test('Groupの識別子ならtrue', () => {
  expect(isGroupResourceIdentifier(groupResourceIdentifier)).toBe(true);
});

test('それ以外ならfalse', () => {
  expect(isGroupResourceIdentifier(projectResourceIdentifier)).toBe(false);
  expect(isGroupResourceIdentifier(versionResourceIdentifier)).toBe(false);
  expect(isGroupResourceIdentifier(issueResourceIdentifier)).toBe(false);
});
