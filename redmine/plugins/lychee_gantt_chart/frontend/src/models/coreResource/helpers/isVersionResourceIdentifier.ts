import { CoreResourceType, VersionResourceIdentifier } from '../types';
import { isIdentifierOf } from './isIdentifierOf';

export const isVersionResourceIdentifier = (identifier: unknown): identifier is VersionResourceIdentifier => {
  return isIdentifierOf(identifier, CoreResourceType.Version);
};
