import {
  getProjectResourceIdentifier,
  getVersionResourceIdentifier,
  getIssueResourceIdentifier,
  getGroupResourceIdentifier,
} from '../identifierGetters';

test('getProjectResourceIdentifier', () => {
  expect(getProjectResourceIdentifier(1)).toBe('project-1');
});

test('getVersionResourceIdentifier', () => {
  expect(getVersionResourceIdentifier(1, 2)).toBe('version-1-2');
});

test('getIssueResourceIdentifier', () => {
  expect(getIssueResourceIdentifier(1)).toBe('issue-1');
});

test('getGroupResourceIdentifier', () => {
  expect(getGroupResourceIdentifier('status-1')).toBe('group-status-1');
});
