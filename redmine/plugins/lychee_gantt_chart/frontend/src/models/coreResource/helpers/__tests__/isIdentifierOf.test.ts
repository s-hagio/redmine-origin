import { isIdentifierOf } from '../isIdentifierOf';
import { CoreResourceType } from '../../types';
import {
  getProjectResourceIdentifier,
  getVersionResourceIdentifier,
  getIssueResourceIdentifier,
  getGroupResourceIdentifier,
} from '../identifierGetters';

const projectIdentifier = getProjectResourceIdentifier(1);
const versionIdentifier = getVersionResourceIdentifier(1, 2);
const issueIdentifier = getIssueResourceIdentifier(1);
const groupIdentifier = getGroupResourceIdentifier('status-new');

test('識別子と指定リソースタイプが一致したらtrue', () => {
  expect(isIdentifierOf(projectIdentifier, CoreResourceType.Project)).toBe(true);
  expect(isIdentifierOf(versionIdentifier, CoreResourceType.Version)).toBe(true);
  expect(isIdentifierOf(issueIdentifier, CoreResourceType.Issue)).toBe(true);
  expect(isIdentifierOf(groupIdentifier, CoreResourceType.Group)).toBe(true);
});

test('それ以外の組み合わせならfalse', () => {
  expect(isIdentifierOf(projectIdentifier, CoreResourceType.Version)).toBe(false);
  expect(isIdentifierOf(projectIdentifier, CoreResourceType.Issue)).toBe(false);
  expect(isIdentifierOf(projectIdentifier, CoreResourceType.Group)).toBe(false);

  expect(isIdentifierOf(versionIdentifier, CoreResourceType.Project)).toBe(false);
  expect(isIdentifierOf(versionIdentifier, CoreResourceType.Issue)).toBe(false);
  expect(isIdentifierOf(versionIdentifier, CoreResourceType.Group)).toBe(false);

  expect(isIdentifierOf(issueIdentifier, CoreResourceType.Project)).toBe(false);
  expect(isIdentifierOf(issueIdentifier, CoreResourceType.Version)).toBe(false);
  expect(isIdentifierOf(issueIdentifier, CoreResourceType.Group)).toBe(false);

  expect(isIdentifierOf(groupIdentifier, CoreResourceType.Project)).toBe(false);
  expect(isIdentifierOf(groupIdentifier, CoreResourceType.Version)).toBe(false);
  expect(isIdentifierOf(groupIdentifier, CoreResourceType.Issue)).toBe(false);
});
