import {
  projectResourceIdentifier,
  versionResourceIdentifier,
  issueResourceIdentifier,
  groupResourceIdentifier,
} from '@/__fixtures__';
import { isIssueResourceIdentifier } from '../isIssueResourceIdentifier';

test('Issueの識別子ならtrue', () => {
  expect(isIssueResourceIdentifier(issueResourceIdentifier)).toBe(true);
});

test('それ以外ならfalse', () => {
  expect(isIssueResourceIdentifier(projectResourceIdentifier)).toBe(false);
  expect(isIssueResourceIdentifier(versionResourceIdentifier)).toBe(false);
  expect(isIssueResourceIdentifier(groupResourceIdentifier)).toBe(false);
});
