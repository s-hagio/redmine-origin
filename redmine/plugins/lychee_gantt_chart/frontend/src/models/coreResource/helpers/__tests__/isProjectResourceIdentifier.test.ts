import {
  projectResourceIdentifier,
  versionResourceIdentifier,
  issueResourceIdentifier,
  groupResourceIdentifier,
} from '@/__fixtures__';
import { isProjectResourceIdentifier } from '../isProjectResourceIdentifier';

test('Projectの識別子ならtrue', () => {
  expect(isProjectResourceIdentifier(projectResourceIdentifier)).toBe(true);
});

test('それ以外ならfalse', () => {
  expect(isProjectResourceIdentifier(versionResourceIdentifier)).toBe(false);
  expect(isProjectResourceIdentifier(issueResourceIdentifier)).toBe(false);
  expect(isProjectResourceIdentifier(groupResourceIdentifier)).toBe(false);
});
