import { useRecoilValue } from 'recoil';
import { availableQueryFilterState } from '../states';
import { QueryFilterName } from '../types';

export const useAvailableQueryFilter = (name: QueryFilterName) => {
  return useRecoilValue(availableQueryFilterState(name));
};
