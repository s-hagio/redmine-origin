import { useRecoilValue } from 'recoil';
import { queryFilterOptionsState } from '../states';
import { QueryFilterName } from '../types';

export const useQueryFilterOptions = (name: QueryFilterName) => useRecoilValue(queryFilterOptionsState(name));
