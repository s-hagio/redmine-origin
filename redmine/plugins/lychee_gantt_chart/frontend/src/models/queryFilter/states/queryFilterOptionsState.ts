import { selectorFamily } from 'recoil';
import { createClient } from '@/api';
import { settingsState } from '@/models/setting';
import { QueryFilterName, QueryFilterOption, QueryFilterOptionValue } from '../types';
import { availableQueryFilterState } from './availableQueryFilterState';

export const queryFilterOptionsState = selectorFamily<
  (QueryFilterOption | QueryFilterOptionValue)[] | null,
  QueryFilterName
>({
  key: 'queryFilterOptions',
  get: (name) => {
    return async ({ get }) => {
      const { mainProject } = get(settingsState);
      const filter = get(availableQueryFilterState(name));

      if (!filter.remote) {
        return filter.values ?? null;
      }

      return createClient().queries.filter.$get({
        query: {
          name,
          projectId: mainProject?.id,
        },
      });
    };
  },
});
