import { selectorFamily } from 'recoil';
import { settingsState } from '@/models/setting';
import { QueryFilter, QueryFilterName } from '../types';

export const availableQueryFilterState = selectorFamily<QueryFilter, QueryFilterName>({
  key: 'availableQueryFilter',
  get: (name) => {
    return ({ get }) => {
      const { availableQueryFilters } = get(settingsState);

      const filter = availableQueryFilters.find((filter) => filter.name === name);

      if (!filter) {
        throw new Error(`Query filter "${name}" is not found.`);
      }

      return filter;
    };
  },
});
