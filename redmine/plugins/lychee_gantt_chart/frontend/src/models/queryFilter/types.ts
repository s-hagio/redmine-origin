import { ProjectID } from '@/models/project';

export type QueryFilterName = string;

export const QueryFilterType = {
  List: 'list',
  ListWithHistory: 'listWithHistory',
  ListStatus: 'listStatus',
  ListOptional: 'listOptional',
  ListOptionalWithHistory: 'listOptionalWithHistory',
  ListSubprojects: 'listSubprojects',
  Date: 'date',
  DatePast: 'datePast',
  String: 'string',
  Text: 'text',
  Integer: 'integer',
  Float: 'float',
  Relation: 'relation',
  Tree: 'tree',
} as const;

export type QueryFilterType = (typeof QueryFilterType)[keyof typeof QueryFilterType];

export type QueryFilterOperator =
  | '='
  | '!'
  | 'o'
  | 'c'
  | '!*'
  | '*'
  | '>='
  | '<='
  | '><'
  | '<t+'
  | '>t+'
  | '><t+'
  | 't+'
  | 'nd'
  | 't'
  | 'ld'
  | 'nw'
  | 'w'
  | 'lw'
  | 'l2w'
  | 'nm'
  | 'm'
  | 'lm'
  | 'y'
  | '>t-'
  | '<t-'
  | '><t-'
  | 't-'
  | '~'
  | '!~'
  | '*~'
  | '^'
  | '$'
  | '=p'
  | '=!p'
  | '!p'
  | '*o'
  | '!o'
  | 'ev'
  | '!ev'
  | 'cf';

export type QueryFilterOperatorLabelMap = Record<QueryFilterOperator, string>;
export type QueryFilterOperatorsMap = Record<QueryFilterType, QueryFilterOperator[]>;

export type QueryFilterValue = string[];

export type QueryFilterOptionLabel = string;
export type QueryFilterOptionValue = string;
export type QueryFilterOptionGroupLabel = string;
export type QueryFilterOption = [
  QueryFilterOptionLabel,
  QueryFilterOptionValue,
  QueryFilterOptionGroupLabel?,
];

export type QueryFilterGroupLabel = string;

export type QueryFilter = {
  name: QueryFilterName;
  type: QueryFilterType;
  label: string;
  groupLabel: QueryFilterGroupLabel | null;
  values: QueryFilterOption[] | null;
  remote: boolean;
};

export type QueryFilterValuesParams = {
  name: QueryFilterName;
  projectId?: ProjectID;
};
