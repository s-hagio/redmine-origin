import { QueryFilterValue } from '../types';

export const normalizeQueryFilterValue = (value: unknown): QueryFilterValue => {
  if (Array.isArray(value)) {
    return value.map((v) => (v ?? '') + '');
  }

  return [''];
};
