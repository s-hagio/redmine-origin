import { selector } from 'recoil';
import { settingsState } from '@/models/setting';

export const nonWorkingWeekDaysState = selector<Set<number>>({
  key: 'nonWorkingWeekDays',
  get: ({ get }) => {
    const { nonWorkingWeekDays } = get(settingsState);

    return new Set(nonWorkingWeekDays);
  },
});
