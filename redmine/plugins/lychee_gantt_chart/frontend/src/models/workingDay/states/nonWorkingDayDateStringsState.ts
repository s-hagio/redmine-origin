import { selector } from 'recoil';
import { ISODateString } from '@/lib/date';
import { settingsState } from '@/models/setting';

export const nonWorkingDayDateStringsState = selector<Set<ISODateString>>({
  key: 'nonWorkingDayDateStrings',
  get: ({ get }) => {
    const { nonWorkingDays } = get(settingsState);

    return new Set(nonWorkingDays);
  },
});
