import { useRecoilValue } from 'recoil';
import { isNonWorkingDayState } from '../functionalStates';

export const useIsNonWorkingDay = () => useRecoilValue(isNonWorkingDayState);
