import { useRecoilValue } from 'recoil';
import { calcWorkingDaysState } from '../functionalStates';

export const useCalcWorkingDays = () => useRecoilValue(calcWorkingDaysState);
