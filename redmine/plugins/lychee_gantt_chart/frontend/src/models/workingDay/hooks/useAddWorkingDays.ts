import { useRecoilValue } from 'recoil';
import { addWorkingDaysState } from '../functionalStates';

export const useAddWorkingDays = () => useRecoilValue(addWorkingDaysState);
