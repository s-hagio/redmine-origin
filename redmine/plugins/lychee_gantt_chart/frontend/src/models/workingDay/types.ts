import { DateArgument } from '@/lib/date';

export type IsNonWorkingDay = (arg: DateArgument) => boolean;
export type CalcWorkingDays = (from: DateArgument, to: DateArgument) => number;
export type AddWorkingDays = (arg: DateArgument, amount: number) => Date;
