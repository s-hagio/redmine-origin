import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { formatISODate, toDate } from '@/lib/date';
import { settingsState } from '@/models/setting';
import { addWorkingDaysState } from '../addWorkingDaysState';

test('日付に任意の稼働日数を加算する', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(addWorkingDaysState), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({
        ...current,
        nonWorkingDays: ['2222-03-23', '2222-03-24'],
      }));
    },
  });

  const startDate = toDate('2222-03-22');
  const days = 3;

  expect(formatISODate(result.current(startDate, days))).toBe('2222-03-27');
});
