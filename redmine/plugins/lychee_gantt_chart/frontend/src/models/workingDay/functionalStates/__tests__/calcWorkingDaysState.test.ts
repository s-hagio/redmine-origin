import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { calcWorkingDaysState } from '../calcWorkingDaysState';

const renderForTesting = () => {
  return renderRecoilHook(() => useRecoilValue(calcWorkingDaysState), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({ ...current, nonWorkingDays: ['2022-04-02', '2022-04-03'] }));
    },
  });
};

const from = new Date(2022, 3, 1);
const to = new Date(2022, 3, 5);

test('日付間の稼働日を返す', () => {
  const { result } = renderForTesting();

  expect(result.current(from, to)).toBe(3);
});

test('from, toの順序が逆でも正しく稼働日を返す', () => {
  const { result } = renderForTesting();

  expect(result.current(to, from)).toBe(-3);
});
