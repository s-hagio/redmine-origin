import { selector } from 'recoil';
import { formatISODate, getISODay, toDate } from '@/lib/date';
import { IsNonWorkingDay } from '../types';
import { nonWorkingDayDateStringsState } from '../states/nonWorkingDayDateStringsState';
import { nonWorkingWeekDaysState } from '../states/nonWorkingWeekDaysState';

export const isNonWorkingDayState = selector<IsNonWorkingDay>({
  key: 'isNonWorkingDay',
  get: ({ get }) => {
    const nonWorkingDayDateStrings = get(nonWorkingDayDateStringsState);
    const nonWorkingWeekDays = get(nonWorkingWeekDaysState);

    return (arg) => {
      const date = toDate(arg);

      return nonWorkingDayDateStrings.has(formatISODate(date)) || nonWorkingWeekDays.has(getISODay(date));
    };
  },
});
