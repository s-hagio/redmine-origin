import { selector } from 'recoil';
import { addDays, isBeforeDay, isSameOrBeforeDay, toDate } from '@/lib/date';
import { CalcWorkingDays } from '../types';
import { isNonWorkingDayState } from './isNonWorkingDayState';

export const calcWorkingDaysState = selector<CalcWorkingDays>({
  key: 'calcWorkingDays',
  get: ({ get }) => {
    const isNonWorkingDay = get(isNonWorkingDayState);

    return (from, to) => {
      const reverse = isBeforeDay(to, from);
      const endDay = toDate(reverse ? from : to);

      let current = toDate(reverse ? to : from);
      let days = 0;

      while (isSameOrBeforeDay(current, endDay)) {
        if (!isNonWorkingDay(current)) {
          ++days;
        }

        current = addDays(current, 1);
      }

      return reverse ? -days : days;
    };
  },
});
