import { selector } from 'recoil';
import { toDate } from '@/lib/date';
import { AddWorkingDays } from '../types';
import { isNonWorkingDayState } from './isNonWorkingDayState';

export const addWorkingDaysState = selector<AddWorkingDays>({
  key: 'addWorkingDays',
  get: ({ get }) => {
    const isNonWorkingDay = get(isNonWorkingDayState);

    return (arg, days) => {
      const date = toDate(arg);
      let remainingDays = days;

      while (remainingDays > 0) {
        date.setDate(date.getDate() + 1);

        if (!isNonWorkingDay(date)) {
          --remainingDays;
        }
      }

      return date;
    };
  },
});
