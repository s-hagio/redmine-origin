import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { isNonWorkingDayState } from '../isNonWorkingDayState';

test('「休業日（曜日）」または「会社休日（日付）」にマッチするならtrue', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(isNonWorkingDayState), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({
        ...current,
        nonWorkingDays: ['2022-03-09'],
        nonWorkingWeekDays: [6, 7], // 土、日
      }));
    },
  });

  expect(result.current('2022-03-06')).toBe(true); // 日曜日
  expect(result.current('2022-03-07')).toBe(false);
  expect(result.current('2022-03-08')).toBe(false);
  expect(result.current('2022-03-09')).toBe(true); // 会社休日
  expect(result.current('2022-03-10')).toBe(false);
  expect(result.current('2022-03-11')).toBe(false);
  expect(result.current('2022-03-12')).toBe(true); // 土曜日
});
