import { selector } from 'recoil';
import { BaselineSummary } from '../types';
import { selectedBaselineIdState } from './selectedBaselineIdState';
import { visibleBaselineSummariesState } from './visibleBaselineSummariesState';

export const selectedBaselineSummaryState = selector<BaselineSummary | null>({
  key: 'selectedBaselineSummary',
  get: ({ get }) => {
    const id = get(selectedBaselineIdState);

    if (!id) {
      return null;
    }

    const summaries = get(visibleBaselineSummariesState);

    return summaries.find((summary) => summary.id === id) || null;
  },
});
