import { atom } from 'recoil';
import { persistJSON, restorePersistedJSON } from '@/utils/storage';
import { BaselineID } from '../types';

const SELECTED_BASELINE_STORED_KEY = 'lycheeGantt:selectedBaselineId';

export const selectedBaselineIdState = atom<BaselineID | null>({
  key: 'selectedBaselineId',
  default: null,
  effects: [
    ({ setSelf, onSet }) => {
      setSelf(restorePersistedJSON(SELECTED_BASELINE_STORED_KEY, null));
      onSet((newValue) => persistJSON(SELECTED_BASELINE_STORED_KEY, newValue));
    },
  ],
});
