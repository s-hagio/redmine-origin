import { selector } from 'recoil';
import { BaselineSummary } from '../types';
import { baselineSummariesState } from './baselineSummariesState';

export const visibleBaselineSummariesState = selector<BaselineSummary[]>({
  key: 'visibleBaselineSummaries',
  get: ({ get }) => {
    const baselineSummaries = get(baselineSummariesState);

    return baselineSummaries.filter((baseline) => !baseline.isDeleting);
  },
});
