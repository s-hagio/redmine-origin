import { selectorFamily } from 'recoil';
import { ProjectResourceIdentifier } from '@/models/coreResource';
import { ProjectSnapshot } from '../types';
import { baselineSnapshotMapState } from './baselineSnapshotMapState';

export const projectBaselineSnapshotState = selectorFamily<ProjectSnapshot | null, ProjectResourceIdentifier>(
  {
    key: 'projectBaselineSnapshotState',
    get: (identifier) => {
      return ({ get }) => {
        const map = get(baselineSnapshotMapState);

        return (map.get(identifier) as ProjectSnapshot) ?? null;
      };
    },
  }
);
