import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { baselineSummary1 } from '@/__fixtures__';
import { Baseline, BaselineSnapshot, BaselineSnapshotIdentifier } from '../../types';
import { selectedBaselineState } from '../selectedBaselineState';
import { baselineSnapshotMapState } from '../baselineSnapshotMapState';

const baseline: Baseline = {
  ...baselineSummary1,
  projects: [{ id: 111, startDate: '2021-01-01', dueDate: '2021-01-30' }],
  versions: [{ id: 222, startDate: '2021-01-03', dueDate: '2021-01-20', completedPercent: 25 }],
  issues: [
    { id: 1, startDate: '2021-01-05', dueDate: '2021-01-09', doneRatio: 0, status: 'open' },
    { id: 2, startDate: '2021-01-08', dueDate: '2021-01-12', doneRatio: 10, status: 'open' },
  ],
};

test('ベースラインスナップショットをリソース識別子でMap化する', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(baselineSnapshotMapState), {
    initializeState: ({ set }) => {
      set(selectedBaselineState, baseline);
    },
  });

  expect(result.current).toEqual(
    new Map<BaselineSnapshotIdentifier, BaselineSnapshot>([
      [`project-111`, baseline.projects[0]],
      [`version-222`, baseline.versions[0]],
      [`issue-1`, baseline.issues[0]],
      [`issue-2`, baseline.issues[1]],
    ])
  );
});
