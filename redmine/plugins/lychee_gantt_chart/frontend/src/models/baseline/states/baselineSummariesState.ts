import { atom, selector } from 'recoil';
import { createClient } from '@/api';
import { settingsState } from '@/models/setting';
import { BaselineSummary } from '../types';

export const baselineSummariesState = atom<BaselineSummary[]>({
  key: 'baselineSummaries',
  default: selector({
    key: 'baselineSummaries/default',
    get: ({ get }) => {
      const { mainProject } = get(settingsState);

      if (!mainProject) {
        return [];
      }

      return createClient().baselines.$get({ query: { projectId: mainProject.id } });
    },
  }),
});
