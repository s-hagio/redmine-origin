import { selectorFamily } from 'recoil';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { IssueSnapshot } from '../types';
import { baselineSnapshotMapState } from './baselineSnapshotMapState';

export const issueBaselineSnapshotState = selectorFamily<IssueSnapshot | null, IssueResourceIdentifier>({
  key: 'issueBaselineSnapshot',
  get: (identifier) => {
    return ({ get }) => {
      const map = get(baselineSnapshotMapState);

      return (map.get(identifier) as IssueSnapshot) ?? null;
    };
  },
});
