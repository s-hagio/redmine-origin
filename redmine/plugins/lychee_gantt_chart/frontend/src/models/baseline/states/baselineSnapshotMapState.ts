import { selector } from 'recoil';
import { CoreResourceType } from '@/models/coreResource';
import { BaselineSnapshotMap } from '../types';
import { selectedBaselineState } from './selectedBaselineState';

const snapshotResourceKeyTypeMap = new Map<'projects' | 'versions' | 'issues', CoreResourceType>([
  ['projects', 'project'],
  ['versions', 'version'],
  ['issues', 'issue'],
]);

export const baselineSnapshotMapState = selector<BaselineSnapshotMap>({
  key: 'baselineSnapshotMap',
  get: ({ get }) => {
    const baseline = get(selectedBaselineState);
    const map = new Map();

    if (baseline) {
      snapshotResourceKeyTypeMap.forEach((resourceType, key) => {
        baseline[key].forEach((snapshot) => {
          map.set(`${resourceType}-${snapshot.id}`, snapshot);
        });
      });
    }

    return map;
  },
});
