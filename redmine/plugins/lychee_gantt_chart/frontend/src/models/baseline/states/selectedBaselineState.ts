import { atom } from 'recoil';
import { Baseline } from '../types';

export const selectedBaselineState = atom<Baseline | null>({
  key: 'selectedBaseline',
  default: null,
});
