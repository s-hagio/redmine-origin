import { selectorFamily } from 'recoil';
import { createClient } from '@/api';
import { Baseline, BaselineID } from '../types';

export const baselineState = selectorFamily<Baseline | null, BaselineID | null>({
  key: 'baseline',
  get: (id) => {
    return async () => {
      if (!id) {
        return null;
      }

      return await createClient().baselines._id(id).$get();
    };
  },
  cachePolicy_UNSTABLE: {
    eviction: 'most-recent',
  },
});
