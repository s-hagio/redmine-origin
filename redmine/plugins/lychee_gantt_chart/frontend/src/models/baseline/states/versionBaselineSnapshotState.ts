import { selectorFamily } from 'recoil';
import { getResourceIdFromIdentifier, VersionResourceIdentifier } from '@/models/coreResource';
import { VersionSnapshot } from '../types';
import { baselineSnapshotMapState } from './baselineSnapshotMapState';

export const versionBaselineSnapshotState = selectorFamily<VersionSnapshot | null, VersionResourceIdentifier>(
  {
    key: 'versionBaselineSnapshot',
    get: (identifier) => {
      return ({ get }) => {
        const map = get(baselineSnapshotMapState);
        const versionId = getResourceIdFromIdentifier(identifier);

        return (map.get(`version-${versionId}`) as VersionSnapshot) ?? null;
      };
    },
  }
);
