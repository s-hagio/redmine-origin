import { Project, ProjectID } from '@/models/project';
import { Version } from '@/models/version';
import { Issue } from '@/models/issue';

export type BaselineID = number;

export type ProjectSnapshot = Pick<Project, 'id' | 'startDate' | 'dueDate'>;

export type VersionSnapshot = Pick<Version, 'id' | 'startDate' | 'dueDate' | 'completedPercent'>;

export type IssueSnapshot = Pick<Issue, 'id' | 'startDate' | 'dueDate' | 'doneRatio'> & {
  status: string;
};

export type BaselineSnapshot = ProjectSnapshot | VersionSnapshot | IssueSnapshot;
export type BaselineSnapshotResourceType = 'project' | 'version' | 'issue';
export type BaselineSnapshotIdentifier = `${BaselineSnapshotResourceType}-${BaselineSnapshot['id']}`;
export type BaselineSnapshotMap = Map<BaselineSnapshotIdentifier, BaselineSnapshot>;

export type BaselineSummary = {
  id: BaselineID;
  name: string | null;
  snappedAt: string;
  isDeleting?: boolean;
};

export type Baseline = BaselineSummary & {
  projects: ProjectSnapshot[];
  versions: VersionSnapshot[];
  issues: IssueSnapshot[];
};

export type BaselineParams = Pick<Baseline, 'name'> & {
  id?: BaselineID;
  projectId?: ProjectID;
};
