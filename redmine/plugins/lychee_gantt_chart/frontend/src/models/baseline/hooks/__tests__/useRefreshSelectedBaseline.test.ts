import { constSelector, useRecoilValue, useSetRecoilState } from 'recoil';
import { beforeEach } from 'vitest';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { baseline1 } from '@/__fixtures__';
import { selectedBaselineIdState, selectedBaselineState } from '../../states';
import { useRefreshSelectedBaseline } from '../useRefreshSelectedBaseline';

const baselineStateMock = vi.hoisted(() => vi.fn());
const nullSelector = constSelector(null);
const baseline1Selector = constSelector(baseline1);

vi.mock('../../states/baselineState', () => ({
  baselineState: baselineStateMock,
}));

beforeEach(() => {
  baselineStateMock.mockImplementation((id) => {
    if (id === baseline1.id) {
      return baseline1Selector;
    }
    return nullSelector;
  });
});

test('選択されたベースラインをbaselineStateから取得してselectedBaselineStateにセットする', async () => {
  const { result, rerender } = renderRecoilHook(() => ({
    refresh: useRefreshSelectedBaseline(),
    selectedBaseline: useRecoilValue(selectedBaselineState),
    setSelectedBaselineId: useSetRecoilState(selectedBaselineIdState),
  }));

  expect(result.current.selectedBaseline).toBe(null);

  act(() => result.current.setSelectedBaselineId(baseline1.id));
  rerender();
  act(() => result.current.refresh());

  expect(result.current.selectedBaseline).toBe(baseline1);

  act(() => result.current.setSelectedBaselineId(null));
  rerender();
  act(() => result.current.refresh());

  expect(result.current.selectedBaseline).toBe(null);
});
