import { useRecoilCallback, useRecoilValue } from 'recoil';
import { baselineState, selectedBaselineIdState, selectedBaselineState } from '../states';

export const useRefreshSelectedBaseline = () => {
  const id = useRecoilValue(selectedBaselineIdState);
  const loadedSelectedBaseline = useRecoilValue(baselineState(id));

  return useRecoilCallback(
    ({ set }) => {
      return () => {
        set(selectedBaselineState, loadedSelectedBaseline);
      };
    },
    [loadedSelectedBaseline]
  );
};
