import { useRecoilValue } from 'recoil';
import { visibleBaselineSummariesState } from '../states';

export const useBaselineSummaries = () => useRecoilValue(visibleBaselineSummariesState);
