import { useCallback } from 'react';
import { useFormatDate, useFormatTime } from '@/models/dateFormat';
import { BaselineSummary } from '../types';

export const useFormatBaselineLabel = () => {
  const formatDate = useFormatDate();
  const formatTime = useFormatTime();

  return useCallback(
    ({ name, snappedAt }: BaselineSummary) => {
      if (name) {
        return name;
      }

      return `${formatDate(snappedAt, { omitCurrentYear: false })} ${formatTime(snappedAt)}`;
    },
    [formatDate, formatTime]
  );
};
