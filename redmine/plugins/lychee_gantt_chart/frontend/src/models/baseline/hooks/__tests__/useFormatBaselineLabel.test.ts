import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { baselineSummary1 } from '@/__fixtures__';
import { useFormatBaselineLabel } from '../useFormatBaselineLabel';

const renderForTesting = () => {
  return renderRecoilHook(() => useFormatBaselineLabel(), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({ ...current, dateFormat: 'yyyy/MM/dd', timeFormat: 'HH:mm' }));
    },
  });
};

test('nameがある場合はnameを返す', () => {
  const name = 'Name!!!';
  const { result } = renderForTesting();

  expect(result.current({ ...baselineSummary1, name })).toBe(name);
});

test('nameがない場合はsnappedAtをフォーマットして返す', () => {
  const { result } = renderForTesting();

  expect(
    result.current({
      ...baselineSummary1,
      name: null,
      snappedAt: '2020-01-23T04:05:06',
    })
  ).toBe('2020/01/23 04:05');
});

test('snappedAtをフォーマットする場合は当年を省略しない', () => {
  const { result } = renderForTesting();
  const currentYear = new Date().getFullYear();

  expect(
    result.current({
      ...baselineSummary1,
      name: null,
      snappedAt: `${currentYear}-12-31T14:55:06`,
    })
  ).toBe(`${currentYear}/12/31 14:55`);
});
