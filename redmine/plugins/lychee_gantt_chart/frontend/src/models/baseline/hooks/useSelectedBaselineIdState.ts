import { useRecoilState } from 'recoil';
import { selectedBaselineIdState } from '../states';

export const useSelectedBaselineIdState = () => useRecoilState(selectedBaselineIdState);
