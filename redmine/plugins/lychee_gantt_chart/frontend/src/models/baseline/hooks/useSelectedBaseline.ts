import { useRecoilValue } from 'recoil';
import { selectedBaselineState } from '../states';

export const useSelectedBaseline = () => useRecoilValue(selectedBaselineState);
