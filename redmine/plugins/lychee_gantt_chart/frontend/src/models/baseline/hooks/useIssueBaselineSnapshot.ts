import { useRecoilValue } from 'recoil';
import { IssueResourceIdentifier } from '@/models/coreResource';
import { issueBaselineSnapshotState } from '../states';

export const useIssueBaselineSnapshot = (identifier: IssueResourceIdentifier) => {
  return useRecoilValue(issueBaselineSnapshotState(identifier));
};
