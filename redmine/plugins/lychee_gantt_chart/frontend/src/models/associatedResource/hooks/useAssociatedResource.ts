import { useRecoilValue } from 'recoil';
import {
  AssociatedResourceType,
  AssociatedResourceID,
  AssociatedProject,
  AssociatedTracker,
  AssociatedPrincipal,
  AssociatedIssueStatus,
  AssociatedIssuePriority,
  AssociatedIssueCategory,
} from '../types';
import { indexedAssociatedResourceCollectionState } from '../states';

type AssociatedResource =
  | AssociatedProject
  | AssociatedTracker
  | AssociatedPrincipal
  | AssociatedIssueStatus
  | AssociatedIssuePriority
  | AssociatedIssueCategory;

type UseAssociatedResource = {
  (resourceType: 'project', id: AssociatedResourceID): AssociatedProject;
  (resourceType: 'tracker', id: AssociatedResourceID): AssociatedTracker;
  (resourceType: 'principal', id: AssociatedResourceID): AssociatedPrincipal;
  (resourceType: 'issueStatus', id: AssociatedResourceID): AssociatedIssueStatus;
  (resourceType: 'issuePriority', id: AssociatedResourceID): AssociatedIssuePriority;
  (resourceType: 'issueCategory', id: AssociatedResourceID): AssociatedIssueCategory;
  (resourceType: AssociatedResourceType, id: AssociatedResourceID): AssociatedResource;
};

export const useAssociatedResource: UseAssociatedResource = (
  type: string,
  id: AssociatedResourceID
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): any => {
  const collection = useRecoilValue(indexedAssociatedResourceCollectionState);

  switch (type) {
    case AssociatedResourceType.Project:
      return collection.projectsById[id];

    case AssociatedResourceType.Version:
      return collection.versionsById[id];

    case AssociatedResourceType.Tracker:
      return collection.trackersById[id];

    case AssociatedResourceType.Principal:
      return collection.principalsById[id];

    case AssociatedResourceType.IssueStatus:
      return collection.issueStatusesById[id];

    case AssociatedResourceType.IssuePriority:
      return collection.issuePrioritiesById[id];

    case AssociatedResourceType.IssueCategory:
      return collection.issueCategoriesById[id];
  }
};
