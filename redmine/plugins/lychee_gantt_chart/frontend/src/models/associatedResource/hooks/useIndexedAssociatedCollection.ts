import { useRecoilValue } from 'recoil';
import { indexedAssociatedResourceCollectionState } from '../states';

export const useIndexedAssociatedCollection = () => useRecoilValue(indexedAssociatedResourceCollectionState);
