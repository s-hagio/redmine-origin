import { renderRecoilHook } from '@/test-utils';
import { associatedResourceCollection } from '@/__fixtures__';
import { AssociatedResourceType, AssociatedResourceTypeKeyMap } from '../../types';
import { associatedResourceCollectionState } from '../../states';
import { useAssociatedResources } from '../useAssociatedResources';

const renderForTesting = (type: AssociatedResourceType) => {
  return renderRecoilHook(() => useAssociatedResources(type), {
    initializeState: ({ set }) => {
      set(associatedResourceCollectionState, associatedResourceCollection);
    },
  });
};

const associatedResourceTypes = Object.keys(AssociatedResourceType) as AssociatedResourceType[];

test.each(associatedResourceTypes)('%sの配列を返す', (type) => {
  const { result } = renderForTesting(type);
  const collectionKey = AssociatedResourceTypeKeyMap[type];

  expect(result.current).toEqual(associatedResourceCollection[collectionKey]);
});
