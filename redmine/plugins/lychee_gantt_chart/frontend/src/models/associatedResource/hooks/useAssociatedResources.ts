import { useRecoilValue } from 'recoil';
import {
  AssociatedProject,
  AssociatedVersion,
  AssociatedTracker,
  AssociatedPrincipal,
  AssociatedIssueStatus,
  AssociatedIssuePriority,
  AssociatedIssueCategory,
  AssociatedResourceType,
  AssociatedResourceTypeKeyMap,
} from '../types';
import { associatedResourceCollectionState } from '../states';

export const useAssociatedResources = <
  T extends AssociatedResourceType,
  R = T extends 'project'
    ? AssociatedProject
    : T extends 'version'
    ? AssociatedVersion
    : T extends 'tracker'
    ? AssociatedTracker
    : T extends 'principal'
    ? AssociatedPrincipal
    : T extends 'issueStatus'
    ? AssociatedIssueStatus
    : T extends 'issuePriority'
    ? AssociatedIssuePriority
    : T extends 'issueCategory'
    ? AssociatedIssueCategory
    : never,
>(
  type: T
): R[] => {
  const collection = useRecoilValue(associatedResourceCollectionState);

  return collection[AssociatedResourceTypeKeyMap[type]] as R[];
};
