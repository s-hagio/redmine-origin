import { renderRecoilHook } from '@/test-utils';
import {
  featureTracker,
  user2,
  group2,
  closedIssueStatus,
  highIssuePriority,
  issueCategory2,
  associatedResourceCollection,
  associatedMainProject,
  associatedVersionSharedByTree,
} from '@/__fixtures__';
import { AssociatedResourceType, AssociatedResourceID } from '../../types';
import { associatedResourceCollectionState } from '../../states';
import { useAssociatedResource } from '../useAssociatedResource';

const renderAssociatedResourceHook = (type: AssociatedResourceType, id: AssociatedResourceID) => {
  return renderRecoilHook(() => useAssociatedResource(type, id), {
    initializeState: ({ set }) => {
      set(associatedResourceCollectionState, associatedResourceCollection);
    },
  });
};

test('Project', () => {
  const { result } = renderAssociatedResourceHook(AssociatedResourceType.Project, associatedMainProject.id);

  expect(result.current).toBe(associatedMainProject);
});

test('Version', () => {
  const { result } = renderAssociatedResourceHook(
    AssociatedResourceType.Version,
    associatedVersionSharedByTree.id
  );

  expect(result.current).toBe(associatedVersionSharedByTree);
});

test('Tracker', () => {
  const { result } = renderAssociatedResourceHook(AssociatedResourceType.Tracker, featureTracker.id);

  expect(result.current).toBe(featureTracker);
});

test('User', () => {
  const { result } = renderAssociatedResourceHook(AssociatedResourceType.Principal, user2.id);

  expect(result.current).toBe(user2);
});

test('Group', () => {
  const { result } = renderAssociatedResourceHook(AssociatedResourceType.Principal, group2.id);

  expect(result.current).toBe(group2);
});

test('Issue Status', () => {
  const { result } = renderAssociatedResourceHook(AssociatedResourceType.IssueStatus, closedIssueStatus.id);

  expect(result.current).toBe(closedIssueStatus);
});

test('Issue Priority', () => {
  const { result } = renderAssociatedResourceHook(AssociatedResourceType.IssuePriority, highIssuePriority.id);

  expect(result.current).toBe(highIssuePriority);
});

test('Issue Category', () => {
  const { result } = renderAssociatedResourceHook(AssociatedResourceType.IssueCategory, issueCategory2.id);

  expect(result.current).toBe(issueCategory2);
});
