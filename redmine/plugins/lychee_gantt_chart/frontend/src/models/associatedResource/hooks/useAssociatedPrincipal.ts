import { useRecoilValue } from 'recoil';
import { associatedPrincipalState } from '../states';
import { OptionalAssociatedResourceID } from '../types';

export const useAssociatedPrincipal = (id: OptionalAssociatedResourceID) => {
  return useRecoilValue(associatedPrincipalState(id));
};
