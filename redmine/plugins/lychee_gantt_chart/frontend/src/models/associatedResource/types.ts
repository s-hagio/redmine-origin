import { ProjectID } from '@/models/project';
import { VersionSharing } from '@/models/version';
import { CoreFieldName } from '@/models/fieldDefinition';

export type AssociatedResourceID = number;
export type OptionalAssociatedResourceID = AssociatedResourceID | null;

type AssociatedBaseResource = {
  id: AssociatedResourceID;
  name: string;
};

export type AssociatedProject = AssociatedBaseResource & {
  trackerIds: number[];
  rootId: number;
  lft: number;
  rgt: number;
};

export type AssociatedVersion = AssociatedBaseResource & {
  projectId: ProjectID;
  sharing: VersionSharing;
  isLocked: boolean;
  isClosed: boolean;
  projectTreeValue: {
    rootId: ProjectID;
    lft: number;
    rgt: number;
  };
};

export type AssociatedTracker = AssociatedBaseResource & {
  defaultStatusId: number;
  disabledCoreFields: CoreFieldName[];
};

export type AssociatedPrincipal = AssociatedBaseResource & {
  isGroup: boolean;
  isLocked: boolean;
  position: number;
  avatar?: string;
};

export type AssociatedIssueStatus = AssociatedBaseResource & {
  isClosed: boolean;
  color: string;
};

export type AssociatedIssuePriority = AssociatedBaseResource & {
  isActive: boolean;
  isDefault: boolean;
};

export type AssociatedIssueCategory = AssociatedBaseResource & {
  projectId: ProjectID;
};

export type AssociatedResourceCollection = {
  projects: AssociatedProject[];
  versions: AssociatedVersion[];
  trackers: AssociatedTracker[];
  principals: AssociatedPrincipal[];
  issueStatuses: AssociatedIssueStatus[];
  issuePriorities: AssociatedIssuePriority[];
  issueCategories: AssociatedIssueCategory[];
};

export type AssociatedResource = AssociatedResourceCollection[keyof AssociatedResourceCollection][number];

export type IndexedAssociatedResourceCollection = {
  projectsById: Record<AssociatedProject['id'], AssociatedProject>;
  versionsById: Record<AssociatedVersion['id'], AssociatedVersion>;
  trackersById: Record<AssociatedTracker['id'], AssociatedTracker>;
  principalsById: Record<AssociatedPrincipal['id'], AssociatedPrincipal>;
  issueStatusesById: Record<AssociatedIssueStatus['id'], AssociatedIssueStatus>;
  issuePrioritiesById: Record<AssociatedIssuePriority['id'], AssociatedIssuePriority>;
  issueCategoriesById: Record<AssociatedIssueCategory['id'], AssociatedIssueCategory>;
};

export const AssociatedResourceType = {
  Project: 'project',
  Version: 'version',
  Tracker: 'tracker',
  Principal: 'principal',
  IssueStatus: 'issueStatus',
  IssuePriority: 'issuePriority',
  IssueCategory: 'issueCategory',
} as const;

export type AssociatedResourceCollectionKey = keyof AssociatedResourceCollection;

export const AssociatedResourceTypeKeyMap: Record<AssociatedResourceType, AssociatedResourceCollectionKey> = {
  [AssociatedResourceType.Project]: 'projects',
  [AssociatedResourceType.Version]: 'versions',
  [AssociatedResourceType.Tracker]: 'trackers',
  [AssociatedResourceType.Principal]: 'principals',
  [AssociatedResourceType.IssueStatus]: 'issueStatuses',
  [AssociatedResourceType.IssuePriority]: 'issuePriorities',
  [AssociatedResourceType.IssueCategory]: 'issueCategories',
} as const;

export type AssociatedResourceType = (typeof AssociatedResourceType)[keyof typeof AssociatedResourceType];
