import { selector } from 'recoil';
import { indexBy } from '@/utils/array';
import { IndexedAssociatedResourceCollection } from '../types';
import { associatedResourceCollectionState } from './associatedResourceCollectionState';

export const indexedAssociatedResourceCollectionState = selector<IndexedAssociatedResourceCollection>({
  key: 'indexedAssociatedResourceCollection',
  get: ({ get }) => {
    const collection = get(associatedResourceCollectionState);

    return {
      projectsById: indexBy(collection.projects, 'id'),
      versionsById: indexBy(collection.versions, 'id'),
      trackersById: indexBy(collection.trackers, 'id'),
      principalsById: indexBy(collection.principals, 'id'),
      issueStatusesById: indexBy(collection.issueStatuses, 'id'),
      issuePrioritiesById: indexBy(collection.issuePriorities, 'id'),
      issueCategoriesById: indexBy(collection.issueCategories, 'id'),
    };
  },
});
