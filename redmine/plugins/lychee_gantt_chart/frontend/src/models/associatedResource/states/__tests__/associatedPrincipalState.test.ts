import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { associatedResourceCollection, user1 } from '@/__fixtures__';
import { associatedPrincipalState } from '../associatedPrincipalState';
import { associatedResourceCollectionState } from '../associatedResourceCollectionState';

test('IDに対応するAssociatedPrincipalを返す', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(associatedPrincipalState(user1.id)), {
    initializeState: ({ set }) => {
      set(associatedResourceCollectionState, associatedResourceCollection);
    },
  });

  expect(result.current).toBe(user1);
});

test('idがnullの場合はnullを返す', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(associatedPrincipalState(null)), {
    initializeState: ({ set }) => {
      set(associatedResourceCollectionState, associatedResourceCollection);
    },
  });

  expect(result.current).toBe(null);
});
