import { atom, selector } from 'recoil';
import { createClient } from '@/api';
import { AssociatedResourceCollection } from '../types';

export const associatedResourceCollectionState = atom<AssociatedResourceCollection>({
  key: 'associatedResourceCollection',
  default: selector({
    key: 'associatedResourceCollection/default',
    get: async () => {
      return createClient().associated_resources.$get();
    },
  }),
});
