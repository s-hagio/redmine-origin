import { selectorFamily } from 'recoil';
import { OptionalAssociatedResourceID, AssociatedPrincipal } from '../types';
import { indexedAssociatedResourceCollectionState } from './indexedAssociatedResourceCollectionState';

export const associatedPrincipalState = selectorFamily<
  AssociatedPrincipal | null,
  OptionalAssociatedResourceID
>({
  key: 'associatedPrincipal',
  get: (id) => {
    return ({ get }) => {
      const { principalsById } = get(indexedAssociatedResourceCollectionState);

      if (!id) {
        return null;
      }

      return principalsById[id];
    };
  },
});
