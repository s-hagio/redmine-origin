import type { MutableSnapshot } from 'recoil';
import { CurrentUser, currentUserState } from '@/models/currentUser';
import { Settings, settingsState } from '@/models/setting';

export const createStateInitializer = (currentUser: CurrentUser, settings: Settings) => {
  return ({ set }: MutableSnapshot) => {
    set(currentUserState, currentUser);
    set(settingsState, settings);
  };
};
