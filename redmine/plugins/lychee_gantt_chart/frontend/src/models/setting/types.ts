import { ISODateString } from '@/lib/date';
import { Project } from '@/models/project';
import { QueryFilter, QueryFilterOperatorLabelMap, QueryFilterOperatorsMap } from '@/models/queryFilter';
import { ColumnName } from '@/models/query';

type ParentValueCalculation = 'derived' | 'independent';

export type Settings = {
  lang: string;
  baseURL: string;
  mainProject?: Pick<Project, 'id' | 'identifier'>;
  dateFormat: string;
  timeFormat: string;
  nonWorkingDays: ISODateString[];
  nonWorkingWeekDays: number[];
  crossProjectIssueRelations: boolean;
  crossProjectSubtasks: '' | 'system' | 'tree' | 'hierarchy' | 'descendants';
  issueDoneRatio: 'issue_field' | 'issue_status';
  parentIssueDates: ParentValueCalculation;
  parentIssuePriority: ParentValueCalculation;
  parentIssueDoneRatio: ParentValueCalculation;
  availableColumnNames: ColumnName[];
  queryFilterOperatorLabelMap: QueryFilterOperatorLabelMap;
  queryFilterOperatorsMap: QueryFilterOperatorsMap;
  availableQueryFilters: QueryFilter[];
};
