import { selector } from 'recoil';
import { settingsState } from './settingsState';

export const hasMainProjectState = selector<boolean>({
  key: 'hasMainProject',
  get: ({ get }) => {
    const settings = get(settingsState);

    return !!settings.mainProject;
  },
});
