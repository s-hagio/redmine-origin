import { atom } from 'recoil';
import { QueryFilterOperatorLabelMap, QueryFilterOperatorsMap } from '@/models/queryFilter';
import { Settings } from '../types';

export const settingsState = atom<Settings>({
  key: 'settings',
  default: {
    lang: 'en',
    baseURL: '',
    dateFormat: '',
    timeFormat: '',
    nonWorkingDays: [],
    nonWorkingWeekDays: [],
    crossProjectIssueRelations: false,
    crossProjectSubtasks: 'tree',
    issueDoneRatio: 'issue_field',
    parentIssueDates: 'derived',
    parentIssueDoneRatio: 'derived',
    parentIssuePriority: 'derived',
    availableColumnNames: [],
    queryFilterOperatorLabelMap: {} as QueryFilterOperatorLabelMap,
    queryFilterOperatorsMap: {} as QueryFilterOperatorsMap,
    availableQueryFilters: [],
  },
});
