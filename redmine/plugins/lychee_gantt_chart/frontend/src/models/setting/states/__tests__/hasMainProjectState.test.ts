import { useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '../settingsState';
import { hasMainProjectState } from '../hasMainProjectState';

test('settingsStateにmainProjectがある場合はtrueを返す', () => {
  const { result } = renderRecoilHook(() => ({
    hasMainProject: useRecoilValue(hasMainProjectState),
    setSettings: useSetRecoilState(settingsState),
  }));

  act(() => {
    result.current.setSettings((current) => ({
      ...current,
      mainProject: { id: 1, identifier: 'mainProject' },
    }));
  });

  expect(result.current.hasMainProject).toBe(true);

  act(() => {
    result.current.setSettings((current) => ({
      ...current,
      mainProject: undefined,
    }));
  });

  expect(result.current.hasMainProject).toBe(false);
});
