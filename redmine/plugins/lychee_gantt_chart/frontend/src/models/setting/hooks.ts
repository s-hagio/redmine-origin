import { useRecoilValue } from 'recoil';
import { settingsState } from './states';

export const useSettings = () => useRecoilValue(settingsState);
