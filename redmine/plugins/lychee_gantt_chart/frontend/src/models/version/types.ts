import { ISODateString } from '@/lib/date';
import { ProjectID } from '@/models/project';

export type VersionID = number;

export const VersionStatus = {
  Open: 'open',
  Locked: 'locked',
  Closed: 'closed',
} as const;

export type VersionStatus = typeof VersionStatus[keyof typeof VersionStatus];

export const VersionSharing = {
  None: 'none',
  Descendants: 'descendants',
  Hierarchy: 'hierarchy',
  Tree: 'tree',
  System: 'system',
} as const;

export type VersionSharing = typeof VersionSharing[keyof typeof VersionSharing];

export type VersionCore = {
  id: VersionID;
  projectId: ProjectID;
  name: string;
  status: VersionStatus;
  sharing: VersionSharing;
  isClosed: boolean;
  projectTreeValue: {
    rootId: ProjectID;
    lft: number;
    rgt: number;
  };
};

export type Version = VersionCore & {
  startDate?: ISODateString | null;
  dueDate?: ISODateString | null;
  completedPercent?: number;
  openIssuesCount?: number;
};
