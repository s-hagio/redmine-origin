import {
  parentProject,
  versionSharedByNone,
  versionSharedByDescendants,
  versionSharedByHierarchy,
  versionSharedByTree,
} from '@/__fixtures__';
import { selectVersionsByProject } from '../../helpers';

const versions = [
  versionSharedByNone,
  versionSharedByDescendants,
  versionSharedByHierarchy,
  versionSharedByTree,
];

test('共有されているバージョンだけを選択して返す', () => {
  expect(selectVersionsByProject(versions, parentProject)).toStrictEqual([
    versionSharedByHierarchy,
    versionSharedByTree,
  ]);
});
