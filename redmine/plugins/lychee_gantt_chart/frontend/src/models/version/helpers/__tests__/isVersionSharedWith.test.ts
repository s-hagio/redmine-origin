import {
  parentProject,
  mainProject,
  grandchildProject,
  childProject2,
  otherTreeProject,
  versionSharedByNone,
  versionSharedByDescendants,
  versionSharedBySystem,
  grandchildProjectVersionSharedByHierarchy,
  grandchildProjectVersionSharedByTree,
} from '@/__fixtures__';
import { isVersionSharedWith } from '../isVersionSharedWith';

test('sharing: none', () => {
  expect(isVersionSharedWith(versionSharedByNone, parentProject)).toBe(false);
  expect(isVersionSharedWith(versionSharedByNone, mainProject)).toBe(true);
  expect(isVersionSharedWith(versionSharedByNone, grandchildProject)).toBe(false);
  expect(isVersionSharedWith(versionSharedByNone, otherTreeProject)).toBe(false);
});

test('sharing: descendants', () => {
  expect(isVersionSharedWith(versionSharedByDescendants, parentProject)).toBe(false);
  expect(isVersionSharedWith(versionSharedByDescendants, mainProject)).toBe(true);
  expect(isVersionSharedWith(versionSharedByDescendants, grandchildProject)).toBe(true);
  expect(isVersionSharedWith(versionSharedByDescendants, otherTreeProject)).toBe(false);
});

test('sharing: hierarchy', () => {
  expect(isVersionSharedWith(grandchildProjectVersionSharedByHierarchy, parentProject)).toBe(true);
  expect(isVersionSharedWith(grandchildProjectVersionSharedByHierarchy, mainProject)).toBe(true);
  expect(isVersionSharedWith(grandchildProjectVersionSharedByHierarchy, grandchildProject)).toBe(true);
  expect(isVersionSharedWith(grandchildProjectVersionSharedByHierarchy, childProject2)).toBe(false);
  expect(isVersionSharedWith(grandchildProjectVersionSharedByHierarchy, otherTreeProject)).toBe(false);
});

test('sharing: tree', () => {
  expect(isVersionSharedWith(grandchildProjectVersionSharedByTree, parentProject)).toBe(true);
  expect(isVersionSharedWith(grandchildProjectVersionSharedByTree, mainProject)).toBe(true);
  expect(isVersionSharedWith(grandchildProjectVersionSharedByTree, grandchildProject)).toBe(true);
  expect(isVersionSharedWith(grandchildProjectVersionSharedByTree, childProject2)).toBe(true);
  expect(isVersionSharedWith(grandchildProjectVersionSharedByTree, otherTreeProject)).toBe(false);
});

test('sharing: system', () => {
  expect(isVersionSharedWith(versionSharedBySystem, parentProject)).toBe(true);
  expect(isVersionSharedWith(versionSharedBySystem, mainProject)).toBe(true);
  expect(isVersionSharedWith(versionSharedBySystem, grandchildProject)).toBe(true);
  expect(isVersionSharedWith(versionSharedBySystem, childProject2)).toBe(true);
  expect(isVersionSharedWith(versionSharedBySystem, otherTreeProject)).toBe(true);
});
