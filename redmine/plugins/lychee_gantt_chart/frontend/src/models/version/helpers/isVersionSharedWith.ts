import { isDescendantOf, isSameHierarchy, isSameTree } from '@/lib/nestedSet';
import { Project } from '@/models/project';
import { Version, VersionSharing } from '../types';

type PartialVersion = Pick<Version, 'projectId' | 'sharing' | 'projectTreeValue'>;
type PartialProject = Pick<Project, 'id' | 'rootId' | 'lft' | 'rgt'>;

export const isVersionSharedWith = (version: PartialVersion, project: PartialProject): boolean => {
  return (
    version.projectId === project.id ||
    version.sharing === VersionSharing.System ||
    (version.sharing === VersionSharing.Descendants && isDescendantOf(project, version.projectTreeValue)) ||
    (version.sharing === VersionSharing.Hierarchy && isSameHierarchy(project, version.projectTreeValue)) ||
    (version.sharing === VersionSharing.Tree && isSameTree(project, version.projectTreeValue))
  );
};
