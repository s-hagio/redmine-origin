import { Project } from '@/models/project';
import { Version } from '../types';
import { isVersionSharedWith } from './isVersionSharedWith';

export const selectVersionsByProject = (versions: Version[], project: Project): Version[] => {
  return versions.filter((version) => isVersionSharedWith(version, project));
};
