import { renderRecoilHook } from '@/test-utils';
import { settingsState, Settings } from '@/models/setting';
import { versionSharedByNone } from '@/__fixtures__';
import { useVersionURL } from '../useVersionURL';

test('バージョンIDを元にバージョンのURLを返す', () => {
  const baseURL = '/path/to/redmine';

  const { result } = renderRecoilHook(() => useVersionURL(versionSharedByNone.id), {
    initializeState: ({ set }) => {
      set(settingsState, { baseURL } as Settings);
    },
  });

  expect(result.current).toBe(`${baseURL}/versions/${versionSharedByNone.id}`);
});
