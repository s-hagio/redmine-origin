import { VersionID } from '../types';
import { useVersion } from './useVersion';

export const useVersionOrThrow = (id: VersionID) => {
  const version = useVersion(id);

  if (!version) {
    throw new Error(`Version ${id} not found`);
  }

  return version;
};
