import { useRecoilValue } from 'recoil';
import { VersionID } from '../types';
import { versionState } from '../states';

export const useVersion = (id: VersionID) => useRecoilValue(versionState(id));
