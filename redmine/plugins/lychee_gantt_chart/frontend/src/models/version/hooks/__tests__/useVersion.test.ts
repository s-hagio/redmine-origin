import { renderRecoilHook } from '@/test-utils';
import { versionSharedByNone } from '@/__fixtures__';
import { versionState } from '../../states';
import { useVersion } from '../useVersion';

test('IDに応じたバージョンの詳細ステートを返す', () => {
  const { result } = renderRecoilHook(() => useVersion(versionSharedByNone.id), {
    initializeState: ({ set }) => {
      set(versionState(versionSharedByNone.id), versionSharedByNone);
    },
  });

  expect(result.current).toBe(versionSharedByNone);
});
