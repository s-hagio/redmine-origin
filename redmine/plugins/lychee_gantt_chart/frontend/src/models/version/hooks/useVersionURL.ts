import { useRecoilValue } from 'recoil';
import { settingsState } from '@/models/setting';
import { VersionID } from '../types';

export const useVersionURL = (id: VersionID) => {
  const { baseURL } = useRecoilValue(settingsState);

  return `${baseURL}/versions/${id}`;
};
