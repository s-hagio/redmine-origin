import { atomFamily } from 'recoil';
import { Version, VersionID } from '../types';

export const versionState = atomFamily<Version | null, VersionID>({
  key: 'version',
  default: null,
});