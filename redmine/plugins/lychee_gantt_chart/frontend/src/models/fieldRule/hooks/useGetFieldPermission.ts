import { useCallback } from 'react';
import { CombinedFieldPermission, useFieldPermissionsMap } from '@/models/workflowRule';
import { FieldName } from '@/models/fieldDefinition';
import { FieldContext } from '../types';

type GetFieldPermission = (context: FieldContext, fieldName: FieldName) => CombinedFieldPermission | null;

export const useGetFieldPermission = (): GetFieldPermission => {
  const fieldPermissionsMap = useFieldPermissionsMap();

  return useCallback(
    (context, fieldName) => {
      return (
        fieldPermissionsMap.get(context.projectId)?.find((fieldRule) => {
          return (
            fieldRule.trackerId === context.trackerId &&
            fieldRule.statusId === context.statusId &&
            fieldRule.fieldName === fieldName
          );
        }) ?? null
      );
    },
    [fieldPermissionsMap]
  );
};
