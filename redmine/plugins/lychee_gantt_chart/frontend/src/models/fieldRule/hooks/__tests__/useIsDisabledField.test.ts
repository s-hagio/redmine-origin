import { renderRecoilHook } from '@/test-utils';
import { customFieldsState } from '@/models/customField';
import { associatedResourceCollectionState } from '@/models/associatedResource';
import { CoreFieldName } from '@/models/fieldDefinition';
import {
  associatedResourceCollection,
  bugTracker,
  textCustomField,
  rootIssue,
  mainProject,
  featureTracker,
  childProject1,
  rootProject,
  stringCustomField,
} from '@/__fixtures__';
import { FieldContext } from '../../types';
import { useIsDisabledField } from '../useIsDisabledField';

const renderForTesting = () => {
  return renderRecoilHook(() => useIsDisabledField(), {
    initializeState: ({ set }) => {
      const disabledCoreFieldNames: CoreFieldName[] = ['assignedToId'];

      set(associatedResourceCollectionState, {
        ...associatedResourceCollection,
        trackers: [{ ...bugTracker, disabledCoreFields: disabledCoreFieldNames }],
      });

      set(customFieldsState, [
        {
          ...textCustomField,
          isForAll: false,
          projectIds: [mainProject.id],
          trackerIds: [bugTracker.id],
        },
        {
          ...stringCustomField,
          isForAll: true,
          projectIds: [mainProject.id],
          trackerIds: [bugTracker.id],
        },
      ]);
    },
  });
};

const context: FieldContext = {
  ...rootIssue,
  projectId: mainProject.id,
  trackerId: bugTracker.id,
};

describe('コアフィールド', () => {
  test('無効化されていればtrue', () => {
    const { result } = renderForTesting();

    expect(result.current(context, 'assignedToId')).toBe(true);
    expect(result.current(context, 'subject')).toBe(false);
  });
});

describe('カスタムフィールド', () => {
  test('有効パターンならfalse', () => {
    const { result } = renderForTesting();

    expect(result.current(context, textCustomField.name)).toBe(false);
    expect(result.current(context, stringCustomField.name)).toBe(false);
  });

  test('指定プロジェクトが無効ならtrue', () => {
    const { result } = renderForTesting();

    expect(result.current({ ...context, projectId: childProject1.id }, textCustomField.name)).toBe(true);
  });

  test('isForAllが有効なら指定プロジェクトは関係なくfalse', () => {
    const { result } = renderForTesting();

    expect(result.current({ ...context, projectId: rootProject.id }, stringCustomField.name)).toBe(false);
  });

  test('指定トラッカーで無効ならtrue', () => {
    const { result } = renderForTesting();

    expect(result.current({ ...context, trackerId: featureTracker.id }, textCustomField.name)).toBe(true);
  });
});
