import { renderHook } from '@testing-library/react';
import { leafIssue } from '@/__fixtures__';
import { FieldContext } from '../../types';
import { useIsEditableIssue } from '../useIsEditableIssue';

const isAllowedToMock = vi.hoisted(() => vi.fn());
const currentUserId = vi.hoisted(() => 123);

vi.mock('@/models/currentUser', () => ({
  useIsAllowedTo: vi.fn().mockReturnValue(isAllowedToMock),
  useCurrentUser: vi.fn().mockReturnValue({ id: currentUserId }),
}));

const baseContext: FieldContext = { ...leafIssue };

const renderForTesting = () => {
  return renderHook(() => useIsEditableIssue());
};

beforeEach(() => {
  isAllowedToMock.mockReturnValue(false);
});

describe('context.idが未指定', () => {
  const context: FieldContext = { ...baseContext };
  delete context.id;

  test('add_issues権限があればtrue', () => {
    const { result } = renderForTesting();

    expect(result.current(context)).toBe(false);

    isAllowedToMock.mockImplementation((permissionName) => permissionName === 'add_issues');

    expect(result.current(context)).toBe(true);
  });
});

describe('context.idが新規チケットID', () => {
  const context: FieldContext = { ...baseContext, id: 'provisional-999' };

  test('add_issues権限があればtrue', () => {
    const { result } = renderForTesting();

    expect(result.current(context)).toBe(false);

    isAllowedToMock.mockImplementation((permissionName) => permissionName === 'add_issues');

    expect(result.current(context)).toBe(true);
  });
});

describe('context.idが既存チケットID', () => {
  const context: FieldContext = { ...baseContext, id: 999 };

  test('edit_issues権限があればtrue', () => {
    const { result } = renderForTesting();

    expect(result.current(context)).toBe(false);

    isAllowedToMock.mockImplementation((permissionName) => permissionName === 'edit_issues');

    expect(result.current(context)).toBe(true);
  });

  test('context.author_idがcurrentUser.idと同一かつedit_own_issues権限があればtrue', () => {
    const ownedContext: FieldContext = { ...context, authorId: currentUserId };
    const { result } = renderForTesting();

    expect(result.current(ownedContext)).toBe(false);

    isAllowedToMock.mockImplementation((permissionName) => permissionName === 'edit_own_issues');

    expect(result.current(ownedContext)).toBe(true);
  });
});
