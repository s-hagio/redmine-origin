import { useCallback } from 'react';
import { useIndexedAssociatedCollection } from '@/models/associatedResource';
import { isCustomFieldName, useCustomFieldMap } from '@/models/customField';
import { FieldRuleChecker } from '../types';

export const useIsDisabledField = (): FieldRuleChecker => {
  const { trackersById } = useIndexedAssociatedCollection();
  const customFieldMap = useCustomFieldMap();

  return useCallback(
    (context, fieldName) => {
      if (isCustomFieldName(fieldName)) {
        const customField = customFieldMap.get(fieldName);

        return (
          !customField ||
          (!customField.isForAll && !customField.projectIds.includes(context.projectId)) ||
          !customField.trackerIds.includes(context.trackerId)
        );
      }

      return !!trackersById[context.trackerId]?.disabledCoreFields.includes(fieldName);
    },
    [trackersById, customFieldMap]
  );
};
