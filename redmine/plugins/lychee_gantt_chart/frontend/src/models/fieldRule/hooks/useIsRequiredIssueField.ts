import { useCallback } from 'react';
import { IssueID, useIssueOrThrow } from '@/models/issue';
import { IsRequiredIssueField } from '../types';
import { useIsRequiredField } from './useIsRequiredField';

export const useIsRequiredIssueField = (issueId: IssueID): IsRequiredIssueField => {
  const isRequiredField = useIsRequiredField();
  const issue = useIssueOrThrow(issueId);

  return useCallback((fieldName) => isRequiredField(issue, fieldName), [isRequiredField, issue]);
};
