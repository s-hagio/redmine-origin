import { useCallback } from 'react';
import { useCurrentUser, useIsAllowedTo } from '@/models/currentUser';
import { isNewIssueId } from '@/models/issue';
import { SpecificFieldRuleChecker } from '../types';

export const useIsEditableIssue = (): SpecificFieldRuleChecker => {
  const currentUser = useCurrentUser();
  const isAllowedTo = useIsAllowedTo();

  return useCallback(
    (context) => {
      if (isNewIssueId(context.id)) {
        return isAllowedTo('add_issues', context);
      }

      return (
        isAllowedTo('edit_issues', context) ||
        (currentUser.id === context.authorId && isAllowedTo('edit_own_issues', context))
      );
    },
    [currentUser, isAllowedTo]
  );
};
