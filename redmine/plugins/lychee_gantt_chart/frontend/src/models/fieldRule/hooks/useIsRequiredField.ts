import { useCallback } from 'react';
import { useFieldDefinitionMap } from '@/models/fieldDefinition';
import { useSettings } from '@/models/setting';
import { FieldRuleChecker } from '../types';
import { useGetFieldPermission } from './useGetFieldPermission';

export const useIsRequiredField = (): FieldRuleChecker => {
  const { issueDoneRatio } = useSettings();
  const getFieldPermission = useGetFieldPermission();
  const fieldDefinitionMap = useFieldDefinitionMap();

  return useCallback(
    (context, fieldName) => {
      return (
        fieldDefinitionMap.get(fieldName)?.required ||
        (issueDoneRatio === 'issue_field' && fieldName === 'doneRatio') ||
        getFieldPermission(context, fieldName)?.rule === 'required'
      );
    },
    [fieldDefinitionMap, issueDoneRatio, getFieldPermission]
  );
};
