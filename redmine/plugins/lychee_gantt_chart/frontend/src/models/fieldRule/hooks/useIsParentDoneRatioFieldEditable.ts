import { useCallback } from 'react';
import { isParentNode } from '@/lib/nestedSet';
import { useSettings } from '@/models/setting';
import { SpecificFieldRuleChecker } from '../types';

export const useIsParentDoneRatioFieldEditable = (): SpecificFieldRuleChecker => {
  const { parentIssueDoneRatio } = useSettings();

  return useCallback(
    (context) => !isParentNode(context) || parentIssueDoneRatio === 'independent',
    [parentIssueDoneRatio]
  );
};
