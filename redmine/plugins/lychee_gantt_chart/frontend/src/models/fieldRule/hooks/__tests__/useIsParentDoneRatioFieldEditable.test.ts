import { renderHook } from '@testing-library/react';
import { describe } from 'vitest';
import { grandchildIssue, rootIssue } from '@/__fixtures__';
import { useIsParentDoneRatioFieldEditable } from '../useIsParentDoneRatioFieldEditable';

const useSettingsMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/setting', () => ({
  useSettings: useSettingsMock,
}));

const parentIssue = rootIssue;
const leafIssue = grandchildIssue;
const render = () => renderHook(() => useIsParentDoneRatioFieldEditable());

describe('親チケットの値の算出方法: 進捗率', () => {
  describe('「子チケットの値から算出」の場合', () => {
    beforeEach(() => {
      useSettingsMock.mockReturnValue({ parentIssueDoneRatio: 'derived' });
    });

    test('親チケットならfalse', () => {
      const { result } = render();

      expect(result.current(parentIssue)).toBe(false);
    });

    test('末端チケットならtrue', () => {
      const { result } = render();

      expect(result.current(leafIssue)).toBe(true);
    });
  });

  describe('「子チケットから独立」の場合', () => {
    beforeEach(() => {
      useSettingsMock.mockReturnValue({ parentIssueDoneRatio: 'independent' });
    });

    test('どちらもtrue', () => {
      const { result } = render();

      expect(result.current(parentIssue)).toBe(true);
      expect(result.current(leafIssue)).toBe(true);
    });
  });
});
