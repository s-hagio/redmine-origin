import { beforeEach } from 'vitest';
import { renderHook } from '@testing-library/react';
import { CombinedFieldPermission } from '@/models/workflowRule';
import { rootIssue } from '@/__fixtures__';
import { useGetFieldPermission } from '../useGetFieldPermission';

const useFieldPermissionsMapMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/workflowRule', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useFieldPermissionsMap: useFieldPermissionsMapMock,
}));

const startDateFieldRule: CombinedFieldPermission = {
  trackerId: rootIssue.trackerId,
  statusId: rootIssue.statusId,
  fieldName: 'startDate',
  rule: 'readonly',
};

beforeEach(() => {
  useFieldPermissionsMapMock.mockReturnValue(new Map([[rootIssue.projectId, [startDateFieldRule]]]));
});

test('contextとfieldNameにマッチするRuleを返す', () => {
  const { result } = renderHook(() => useGetFieldPermission());

  expect(result.current(rootIssue, 'startDate')).toStrictEqual(startDateFieldRule);
});

test('マッチしない場合はnullを返す', () => {
  const { result } = renderHook(() => useGetFieldPermission());

  expect(result.current({ ...rootIssue, projectId: rootIssue.projectId + 1 }, 'startDate')).toBeNull();
  expect(result.current({ ...rootIssue, trackerId: rootIssue.trackerId + 1 }, 'startDate')).toBeNull();
  expect(result.current({ ...rootIssue, statusId: rootIssue.statusId + 1 }, 'startDate')).toBeNull();
  expect(result.current(rootIssue, 'description')).toBeNull();
});
