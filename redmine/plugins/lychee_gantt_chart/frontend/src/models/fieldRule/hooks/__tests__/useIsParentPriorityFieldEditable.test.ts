import { renderHook } from '@testing-library/react';
import { describe } from 'vitest';
import { grandchildIssue, rootIssue } from '@/__fixtures__';
import { useIsParentPriorityFieldEditable } from '../useIsParentPriorityFieldEditable';

const useSettingsMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/setting', () => ({
  useSettings: useSettingsMock,
}));

const parentIssue = rootIssue;
const leafIssue = grandchildIssue;
const render = () => renderHook(() => useIsParentPriorityFieldEditable());

describe('親チケットの値の算出方法: 優先度', () => {
  describe('「子チケットの値から算出」の場合', () => {
    beforeEach(() => {
      useSettingsMock.mockReturnValue({ parentIssuePriority: 'derived' });
    });

    test('親チケットならfalse', () => {
      const { result } = render();

      expect(result.current(parentIssue)).toBe(false);
    });

    test('末端チケットならtrue', () => {
      const { result } = render();

      expect(result.current(leafIssue)).toBe(true);
    });
  });

  describe('「子チケットから独立」の場合', () => {
    beforeEach(() => {
      useSettingsMock.mockReturnValue({ parentIssuePriority: 'independent' });
    });

    test('どちらもtrue', () => {
      const { result } = render();

      expect(result.current(parentIssue)).toBe(true);
      expect(result.current(leafIssue)).toBe(true);
    });
  });
});
