import { useCallback } from 'react';
import { isParentNode } from '@/lib/nestedSet';
import { useSettings } from '@/models/setting';
import { SpecificFieldRuleChecker } from '../types';

export const useIsParentPriorityFieldEditable = (): SpecificFieldRuleChecker => {
  const { parentIssuePriority } = useSettings();

  return useCallback(
    (context) => !isParentNode(context) || parentIssuePriority === 'independent',
    [parentIssuePriority]
  );
};
