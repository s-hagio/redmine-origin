import { useCallback } from 'react';
import { isParentNode } from '@/lib/nestedSet';
import { useSettings } from '@/models/setting';
import { SpecificFieldRuleChecker } from '../types';

export const useIsParentDateFieldEditable = (): SpecificFieldRuleChecker => {
  const { parentIssueDates } = useSettings();

  return useCallback(
    (context) => !isParentNode(context) || parentIssueDates === 'independent',
    [parentIssueDates]
  );
};
