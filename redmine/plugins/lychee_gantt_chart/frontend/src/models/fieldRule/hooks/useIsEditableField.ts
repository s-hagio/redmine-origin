import { useCallback } from 'react';
import { FieldRuleChecker } from '../types';
import { useIsEditableIssue } from './useIsEditableIssue';
import { useIsDisabledField } from './useIsDisabledField';
import { useIsReadonlyField } from './useIsReadonlyField';
import { useIsParentDoneRatioFieldEditable } from './useIsParentDoneRatioFieldEditable';
import { useIsParentDateFieldEditable } from './useIsParentDateFieldEditable';
import { useIsParentPriorityFieldEditable } from './useIsParentPriorityFieldEditable';

export const useIsEditableField = (): FieldRuleChecker => {
  const isEditableIssue = useIsEditableIssue();
  const isDisabledField = useIsDisabledField();
  const isReadonlyField = useIsReadonlyField();
  const isParentDoneRatioFieldEditable = useIsParentDoneRatioFieldEditable();
  const isParentDateFieldEditable = useIsParentDateFieldEditable();
  const isParentPriorityFieldEditable = useIsParentPriorityFieldEditable();

  return useCallback(
    (context, fieldName) => {
      if (
        !isEditableIssue(context) ||
        isDisabledField(context, fieldName) ||
        isReadonlyField(context, fieldName)
      ) {
        return false;
      }

      switch (fieldName) {
        case 'doneRatio':
          return isParentDoneRatioFieldEditable(context);
        case 'startDate':
        case 'dueDate':
          return isParentDateFieldEditable(context);
        case 'priorityId':
          return isParentPriorityFieldEditable(context);
      }

      return true;
    },
    [
      isEditableIssue,
      isDisabledField,
      isReadonlyField,
      isParentDoneRatioFieldEditable,
      isParentDateFieldEditable,
      isParentPriorityFieldEditable,
    ]
  );
};
