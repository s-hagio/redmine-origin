import { renderHook } from '@testing-library/react';
import { rootIssue, startDateFieldDefinition } from '@/__fixtures__';
import { useIsRequiredField } from '../useIsRequiredField';

const useSettingsMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/setting', () => ({
  useSettings: useSettingsMock,
}));

const useFieldDefinitionMapMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldDefinition', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useFieldDefinitionMap: useFieldDefinitionMapMock.mockReturnValue(new Map()),
}));

const getFieldPermissionMock = vi.hoisted(() => vi.fn());

vi.mock('../useGetFieldPermission', () => ({
  useGetFieldPermission: vi.fn().mockReturnValue(getFieldPermissionMock),
}));

const issue = rootIssue;

beforeEach(() => {
  useSettingsMock.mockReturnValue({ issueDoneRatio: 'issue_field' });
});

test('requiredが適用される場合はtrueを返す', () => {
  getFieldPermissionMock.mockReturnValue({ rule: 'required' });

  const { result } = renderHook(() => useIsRequiredField());

  expect(result.current(issue, 'description')).toBe(true);
});

test('readonlyが適用される場合はfalseを返す', () => {
  getFieldPermissionMock.mockReturnValue({ rule: 'readonly' });

  const { result } = renderHook(() => useIsRequiredField());

  expect(result.current(issue, 'startDate')).toBe(false);
});

test('ルールが適用されていない場合はfalseを返す', () => {
  getFieldPermissionMock.mockReturnValue(null);

  const { result } = renderHook(() => useIsRequiredField());

  expect(result.current(issue, 'startDate')).toBe(false);
});

test.each([
  { issueDoneRatio: 'issue_field', expectedValue: true },
  { issueDoneRatio: 'issue_status', expectedValue: false },
])(
  'doneRatioはissueDoneRatioが"$issueDoneRatio"の場合は$expectedValueを返す',
  ({ issueDoneRatio, expectedValue }) => {
    useSettingsMock.mockReturnValue({ issueDoneRatio });

    const { result } = renderHook(() => useIsRequiredField());

    expect(result.current(issue, 'doneRatio')).toBe(expectedValue);
  }
);

test('requiredがtrueのフィールドは必須', () => {
  getFieldPermissionMock.mockReturnValue(null);

  const map = new Map();
  useFieldDefinitionMapMock.mockReturnValueOnce(map);

  const { result } = renderHook(() => useIsRequiredField());

  map.set('startDate', { ...startDateFieldDefinition, required: true });
  expect(result.current(issue, 'startDate')).toBe(true);

  map.set('startDate', { ...startDateFieldDefinition, required: false });
  expect(result.current(issue, 'startDate')).toBe(false);
});
