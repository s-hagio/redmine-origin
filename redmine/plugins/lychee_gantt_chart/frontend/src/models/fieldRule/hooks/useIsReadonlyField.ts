import { useCallback } from 'react';
import { useSettings } from '@/models/setting';
import { useFieldDefinitionMap } from '@/models/fieldDefinition';
import { FieldRuleChecker } from '../types';
import { useGetFieldPermission } from './useGetFieldPermission';

export const useIsReadonlyField = (): FieldRuleChecker => {
  const { issueDoneRatio } = useSettings();
  const getFieldPermission = useGetFieldPermission();
  const fieldMap = useFieldDefinitionMap();

  return useCallback(
    (context, fieldName) => {
      return (
        fieldMap.get(fieldName)?.readonly ||
        (issueDoneRatio === 'issue_status' && fieldName === 'doneRatio') ||
        getFieldPermission(context, fieldName)?.rule === 'readonly'
      );
    },
    [fieldMap, issueDoneRatio, getFieldPermission]
  );
};
