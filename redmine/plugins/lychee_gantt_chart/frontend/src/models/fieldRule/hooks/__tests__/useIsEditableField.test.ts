import { renderHook } from '@testing-library/react';
import { rootIssue } from '@/__fixtures__';
import { useIsEditableField } from '../useIsEditableField';

const isEditableIssueMock = vi.hoisted(() => vi.fn());
const isDisabledIssueFieldMock = vi.hoisted(() => vi.fn());
const isReadonlyIssueFieldMock = vi.hoisted(() => vi.fn());
const isParentDoneRatioFieldEditableMock = vi.hoisted(() => vi.fn());
const isParentDateFieldEditableMock = vi.hoisted(() => vi.fn());
const isParentPriorityFieldEditableMock = vi.hoisted(() => vi.fn());

vi.mock('../useIsEditableIssue', () => ({
  useIsEditableIssue: vi.fn().mockReturnValue(isEditableIssueMock),
}));

vi.mock('../useIsDisabledField', () => ({
  useIsDisabledField: vi.fn().mockReturnValue(isDisabledIssueFieldMock),
}));

vi.mock('../useIsReadonlyField', () => ({
  useIsReadonlyField: vi.fn().mockReturnValue(isReadonlyIssueFieldMock),
}));

vi.mock('../useIsParentDoneRatioFieldEditable', () => ({
  useIsParentDoneRatioFieldEditable: vi.fn().mockReturnValue(isParentDoneRatioFieldEditableMock),
}));

vi.mock('../useIsParentDateFieldEditable', () => ({
  useIsParentDateFieldEditable: vi.fn().mockReturnValue(isParentDateFieldEditableMock),
}));

vi.mock('../useIsParentPriorityFieldEditable', () => ({
  useIsParentPriorityFieldEditable: vi.fn().mockReturnValue(isParentPriorityFieldEditableMock),
}));

beforeEach(() => {
  isEditableIssueMock.mockReturnValue(true);
  isDisabledIssueFieldMock.mockReturnValue(false);
  isReadonlyIssueFieldMock.mockReturnValue(false);
  isParentDoneRatioFieldEditableMock.mockReturnValue(true);
  isParentDateFieldEditableMock.mockReturnValue(true);
  isParentPriorityFieldEditableMock.mockReturnValue(true);
});

const issue = rootIssue;
const render = () => renderHook(() => useIsEditableField());

test('編集可能な場合はtrueを返す', () => {
  const { result } = render();

  expect(result.current(issue, 'subject')).toBe(true);
  expect(result.current(issue, 'doneRatio')).toBe(true);
  expect(result.current(issue, 'startDate')).toBe(true);
  expect(result.current(issue, 'dueDate')).toBe(true);
  expect(result.current(issue, 'priorityId')).toBe(true);
});

test.each([
  ['編集権限が無い場合', () => isEditableIssueMock.mockReturnValue(false)],
  ['disabledの場合', () => isDisabledIssueFieldMock.mockReturnValue(true)],
  ['readonlyの場合', () => isReadonlyIssueFieldMock.mockReturnValue(true)],
])('%sは常にfalse', (_message, setMockReturnValue) => {
  setMockReturnValue();

  const { result } = render();

  expect(result.current(issue, 'subject')).toBe(false);
  expect(result.current(issue, 'doneRatio')).toBe(false);
  expect(result.current(issue, 'startDate')).toBe(false);
  expect(result.current(issue, 'dueDate')).toBe(false);
  expect(result.current(issue, 'priorityId')).toBe(false);
});

test('親の進捗率フィールドが編集不能判定ならfalse', () => {
  isParentDoneRatioFieldEditableMock.mockReturnValue(false);

  const { result } = render();

  expect(result.current(issue, 'subject')).toBe(true);
  expect(result.current(issue, 'doneRatio')).toBe(false);
  expect(result.current(issue, 'startDate')).toBe(true);
  expect(result.current(issue, 'dueDate')).toBe(true);
  expect(result.current(issue, 'priorityId')).toBe(true);
});

test('親の日付フィールドが編集不能判定ならfalse', () => {
  isParentDateFieldEditableMock.mockReturnValue(false);

  const { result } = render();

  expect(result.current(issue, 'subject')).toBe(true);
  expect(result.current(issue, 'doneRatio')).toBe(true);
  expect(result.current(issue, 'startDate')).toBe(false);
  expect(result.current(issue, 'dueDate')).toBe(false);
  expect(result.current(issue, 'priorityId')).toBe(true);
});

test('親の優先度フィールドが編集不能判定ならfalse', () => {
  isParentPriorityFieldEditableMock.mockReturnValue(false);

  const { result } = render();

  expect(result.current(issue, 'subject')).toBe(true);
  expect(result.current(issue, 'doneRatio')).toBe(true);
  expect(result.current(issue, 'startDate')).toBe(true);
  expect(result.current(issue, 'dueDate')).toBe(true);
  expect(result.current(issue, 'priorityId')).toBe(false);
});
