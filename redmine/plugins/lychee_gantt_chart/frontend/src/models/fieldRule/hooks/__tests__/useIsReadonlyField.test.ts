import { renderHook } from '@testing-library/react';
import { rootIssue, startDateFieldDefinition } from '@/__fixtures__';
import { useIsReadonlyField } from '../useIsReadonlyField';

const useSettingsMock = vi.hoisted(() => vi.fn().mockReturnValue({ issueDoneRatio: 'issue_field' }));

vi.mock('@/models/setting', () => ({
  useSettings: useSettingsMock,
}));

const getFieldPermissionMock = vi.hoisted(() => vi.fn());

vi.mock('../useGetFieldPermission', () => ({
  useGetFieldPermission: vi.fn().mockReturnValue(getFieldPermissionMock),
}));

const useFieldDefinitionMapMock = vi.hoisted(() => vi.fn().mockReturnValue(new Map()));

vi.mock('@/models/fieldDefinition', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  useFieldDefinitionMap: useFieldDefinitionMapMock,
}));

const issue = rootIssue;

test('readonlyが適用される場合はtrueを返す', () => {
  getFieldPermissionMock.mockReturnValue({ rule: 'readonly' });

  const { result } = renderHook(() => useIsReadonlyField());

  expect(result.current(issue, 'description')).toBe(true);
});

test('requiredが適用される場合はfalseを返す', () => {
  getFieldPermissionMock.mockReturnValue({ rule: 'required' });

  const { result } = renderHook(() => useIsReadonlyField());

  expect(result.current(issue, 'startDate')).toBe(false);
});

test('ルールが適用されていない場合はfalseを返す', () => {
  getFieldPermissionMock.mockReturnValue(null);

  const { result } = renderHook(() => useIsReadonlyField());

  expect(result.current(issue, 'startDate')).toBe(false);
});

describe('進捗率の算出方法が「チケットのフィールドを使用」の場合', () => {
  beforeEach(() => {
    useSettingsMock.mockReturnValue({ issueDoneRatio: 'issue_field' });
  });

  test('doneRatioは読み取り専用ではない', () => {
    const { result } = renderHook(() => useIsReadonlyField());

    expect(result.current(issue, 'doneRatio')).toBe(false);
  });
});

describe('進捗率の算出方法が「チケットのステータスに連動」の場合', () => {
  beforeEach(() => {
    useSettingsMock.mockReturnValue({ issueDoneRatio: 'issue_status' });
  });

  test('doneRatioは読み取り専用', () => {
    const { result } = renderHook(() => useIsReadonlyField());

    expect(result.current(issue, 'doneRatio')).toBe(true);
  });
});

test('readonlyがtrueのフィールドは読み取り専用', () => {
  const map = new Map();
  useFieldDefinitionMapMock.mockReturnValueOnce(map);

  const { result } = renderHook(() => useIsReadonlyField());

  map.set('startDate', { ...startDateFieldDefinition, readonly: true });
  expect(result.current(issue, 'startDate')).toBe(true);

  map.set('startDate', { ...startDateFieldDefinition, readonly: false });
  expect(result.current(issue, 'startDate')).toBe(false);
});
