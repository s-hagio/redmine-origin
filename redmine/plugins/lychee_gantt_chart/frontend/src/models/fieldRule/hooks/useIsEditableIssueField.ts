import { useCallback } from 'react';
import { IssueID, useIssueOrThrow } from '@/models/issue';
import { IsEditableIssueField } from '../types';
import { useIsEditableField } from './useIsEditableField';

export const useIsEditableIssueField = (issueId: IssueID): IsEditableIssueField => {
  const isEditableField = useIsEditableField();
  const issue = useIssueOrThrow(issueId);

  return useCallback((fieldName) => isEditableField(issue, fieldName), [isEditableField, issue]);
};
