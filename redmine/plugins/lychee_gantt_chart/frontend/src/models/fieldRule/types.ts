import { Node } from '@/lib/nestedSet';
import { FieldName } from '@/models/fieldDefinition';
import { AssociatedPrincipal } from '@/models/associatedResource';
import { ProjectID } from '@/models/project';
import { IssueID } from '@/models/issue';

export type FieldContext = Node & {
  id?: IssueID;
  projectId: ProjectID;
  trackerId: number;
  statusId: number;
  authorId?: AssociatedPrincipal['id'];
};

export type FieldRuleChecker = (context: FieldContext, fieldName: FieldName) => boolean;
export type SpecificFieldRuleChecker = (context: FieldContext) => boolean;

export type IsEditableIssueField = (fieldName: FieldName) => boolean;
export type IsRequiredIssueField = (fieldName: FieldName) => boolean;
