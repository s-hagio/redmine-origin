import { pick } from '@/utils/object';
import { Issue } from '@/models/issue';
import { FieldContext } from '../types';

export const extractIssueFieldContext = (issue: Issue): FieldContext => {
  return pick(issue, ['id', 'projectId', 'trackerId', 'statusId', 'authorId', 'rootId', 'lft', 'rgt']);
};
