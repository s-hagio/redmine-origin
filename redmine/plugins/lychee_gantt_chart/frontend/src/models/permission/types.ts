import { ProjectID } from '@/models/project';

export type PermissionName =
  | 'manage_lychee_gantt'
  | 'add_issues'
  | 'edit_issues'
  | 'edit_own_issues'
  | 'copy_issues'
  | 'manage_issue_relations'
  | 'manage_subtasks'
  | 'set_issues_private'
  | 'set_own_issues_private'
  | 'add_issue_notes'
  | 'edit_issue_notes'
  | 'edit_own_issue_notes'
  | 'set_notes_private'
  | 'delete_issues'
  | 'manage_categories'
  | 'view_time_entries'
  | 'log_time'
  | 'log_time_for_other_users'
  | 'manage_versions'
  | 'manage_public_queries'
  | 'save_queries'
  | 'view_lychee_gantt_baselines'
  | 'manage_lychee_gantt_baselines'
  | 'view_lychee_gantt_milestones'
  | 'manage_lychee_gantt_milestones';

export type PermissionContextualProjectID = ProjectID | null | undefined;
export type PermissionContext = { projectId: PermissionContextualProjectID } | null | undefined;
