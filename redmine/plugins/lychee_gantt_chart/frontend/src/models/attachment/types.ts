export type Attachment = {
  id: number;
  filename: string;
  url: string;
};
