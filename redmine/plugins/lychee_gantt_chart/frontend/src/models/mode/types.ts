export const Mode = {
  Default: 'default',
  Simulation: 'simulation',
} as const;

export type Mode = typeof Mode[keyof typeof Mode];
