import { useRecoilValue } from 'recoil';
import { isAllowedInMainProjectState } from '../functionalStates';

export const useIsAllowedInMainProject = () => useRecoilValue(isAllowedInMainProjectState);
