import { useRecoilValue } from 'recoil';
import { currentUserState } from '../states';

export const useCurrentUser = () => useRecoilValue(currentUserState);
