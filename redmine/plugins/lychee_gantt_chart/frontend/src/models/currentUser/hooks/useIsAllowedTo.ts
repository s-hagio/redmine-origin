import { useRecoilValue } from 'recoil';
import { isAllowedToState } from '../functionalStates';

export const useIsAllowedTo = () => useRecoilValue(isAllowedToState);
