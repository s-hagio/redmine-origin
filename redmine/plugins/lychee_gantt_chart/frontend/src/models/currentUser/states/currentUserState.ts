import { atom } from 'recoil';
import { CurrentUser } from '../types';

export const currentUserState = atom<CurrentUser>({
  key: 'currentUser',
});
