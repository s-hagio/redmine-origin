import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { currentUserState } from '../../states';
import { isAllowedToState } from '../isAllowedToState';

test('adminユーザーの時は常にtrueを返す', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(isAllowedToState), {
    initializeState: ({ set }) => {
      set(currentUserState, {
        id: 123,
        isAdmin: true,
        permissionsByProjectId: {},
        roleIdsByProjectId: {},
      });
    },
  });

  expect(result.current('manage_versions', { projectId: 1 })).toBe(true);
  expect(result.current('log_time', { projectId: 2 })).toBe(true);
  expect(result.current('manage_issue_relations', null, { global: true })).toBe(true);
});

describe('一般ユーザー', () => {
  const renderForTesting = () => {
    return renderRecoilHook(() => useRecoilValue(isAllowedToState), {
      initializeState: ({ set }) => {
        set(currentUserState, {
          id: 123,
          isAdmin: false,
          permissionsByProjectId: {
            1: ['manage_lychee_gantt', 'manage_versions'],
            2: ['manage_lychee_gantt', 'add_issues', 'view_lychee_gantt_milestones'],
            3: ['add_issues', 'manage_versions', 'log_time', 'view_lychee_gantt_milestones'],
          },
          roleIdsByProjectId: {},
        });
      },
    });
  };

  test('操作系の権限はLGCの操作権限と対象権限を持っていればtrue', () => {
    const { result } = renderForTesting();

    expect(result.current('manage_versions', { projectId: 1 })).toBe(true);
    expect(result.current('manage_versions', { projectId: 2 })).toBe(false);
    expect(result.current('manage_versions', { projectId: 3 })).toBe(false);
  });

  test('閲覧系の権限は対象権限を持っていればtrue', () => {
    const { result } = renderForTesting();

    expect(result.current('view_lychee_gantt_milestones', { projectId: 1 })).toBe(false);
    expect(result.current('view_lychee_gantt_milestones', { projectId: 2 })).toBe(true);
    expect(result.current('view_lychee_gantt_milestones', { projectId: 3 })).toBe(true);
  });

  describe('globalオプションがtrueの時', () => {
    test('いずれかのプロジェクトで権限チェックが通ればtrue', () => {
      const { result } = renderForTesting();

      expect(result.current('manage_versions', null, { global: true })).toBe(true);
      expect(result.current('view_lychee_gantt_milestones', null, { global: true })).toBe(true);
      expect(result.current('log_time', null, { global: true })).toBe(false);
    });
  });
});
