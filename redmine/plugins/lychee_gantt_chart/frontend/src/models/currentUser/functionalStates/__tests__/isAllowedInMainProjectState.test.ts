import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { Settings, settingsState } from '@/models/setting';
import { mainProject, otherTreeProject } from '@/__fixtures__';
import { currentUserState } from '../../states';
import { isAllowedInMainProjectState } from '../isAllowedInMainProjectState';

const renderWithMainProject = (project?: Settings['mainProject']) => {
  return renderRecoilHook(() => useRecoilValue(isAllowedInMainProjectState), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({ ...current, mainProject: project }));

      set(currentUserState, {
        id: 1,
        isAdmin: false,
        permissionsByProjectId: {
          [mainProject.id]: ['manage_lychee_gantt', 'add_issues'],
          [otherTreeProject.id]: ['manage_lychee_gantt', 'manage_versions'],
        },
        roleIdsByProjectId: {},
      });
    },
  });
};

test('メインプロジェクト内で権限があればtrueを返す', () => {
  const { result } = renderWithMainProject(mainProject);

  expect(result.current('add_issues')).toBe(true);
  expect(result.current('manage_versions')).toBe(false);
});

test('メインプロジェクトがなければ常にfalseを返す', () => {
  const { result } = renderWithMainProject();

  expect(result.current('add_issues')).toBe(false);
  expect(result.current('manage_versions')).toBe(false);
});
