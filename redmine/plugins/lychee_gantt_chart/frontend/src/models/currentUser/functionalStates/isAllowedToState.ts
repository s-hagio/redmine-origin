import { selector } from 'recoil';
import { get as getValue } from '@/utils/object';
import { isProjectId } from '@/models/project';
import { IsAllowedTo } from '../types';
import { currentUserState } from '../states';

export const isAllowedToState = selector<IsAllowedTo>({
  key: 'isAllowedTo',
  get: ({ get }) => {
    const user = get(currentUserState);

    return (name, context, options = {}) => {
      if (user.isAdmin) {
        return true;
      }

      const projectId = getValue(context, 'projectId');

      if (!projectId && !options.global) {
        throw new Error('context argument must be a PermissionContext.');
      }

      const targetPermissions = isProjectId(projectId)
        ? [user.permissionsByProjectId[projectId] ?? []]
        : Object.values(user.permissionsByProjectId);

      return targetPermissions.some((permissions) => {
        return (
          (name.startsWith('view_') || permissions.includes('manage_lychee_gantt')) &&
          permissions.includes(name)
        );
      });
    };
  },
});
