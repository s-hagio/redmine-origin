import { selector } from 'recoil';
import { settingsState } from '@/models/setting';
import { IsAllowedInMainProject } from '../types';
import { isAllowedToState } from './isAllowedToState';

export const isAllowedInMainProjectState = selector<IsAllowedInMainProject>({
  key: 'isAllowedInMainProject',
  get: ({ get }) => {
    const { mainProject } = get(settingsState);
    const isAllowedTo = get(isAllowedToState);

    return (name, options) => {
      return !!mainProject && isAllowedTo(name, { projectId: mainProject.id }, { ...options, global: false });
    };
  },
});
