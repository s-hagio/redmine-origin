import { PermissionName, PermissionContext } from '@/models/permission';
import { ProjectID } from '@/models/project';

export type UserID = number;
export type RoleID = number;

export type CurrentUser = {
  id: UserID;
  isAdmin: boolean;
  permissionsByProjectId: Record<ProjectID, PermissionName[]>;
  roleIdsByProjectId: Record<ProjectID, RoleID[]>;
};

type PermissionCheckerOptions = {
  global?: boolean;
};

export type IsAllowedTo = (
  name: PermissionName,
  context: PermissionContext,
  options?: PermissionCheckerOptions
) => boolean;

export type IsAllowedInMainProject = (name: PermissionName, options?: PermissionCheckerOptions) => boolean;
