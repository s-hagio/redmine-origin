import { useRecoilValue } from 'recoil';
import { IssueRelationID } from '../types';
import { isDeletableIssueRelationState } from '../states';

export const useIsDeletableIssueRelation = (id: IssueRelationID | null) => {
  return useRecoilValue(isDeletableIssueRelationState(id));
};
