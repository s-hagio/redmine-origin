import { useRecoilValue } from 'recoil';
import { IssueID } from '@/models/issue';
import { isIssueRelationManageableState } from '../states';

export const useIsIssueRelationManageable = (issueId: IssueID) => {
  return useRecoilValue(isIssueRelationManageableState(issueId));
};
