import { useRecoilCallback } from 'recoil';
import { useLoadRecoilValue } from '@/lib/hooks';
import { storedIssueRelationsState, syncedIssueRelationsState } from '../states';

export const useRefreshIssueRelations = () => {
  const loadable = useLoadRecoilValue(syncedIssueRelationsState);

  return useRecoilCallback(
    ({ set }) => {
      return () => {
        const issueRelations = loadable.valueMaybe();

        if (issueRelations) {
          set(storedIssueRelationsState, issueRelations);
        }
      };
    },
    [loadable]
  );
};
