import { selectorFamily } from 'recoil';
import { IssueRelationID } from '../types';
import { issueRelationState } from './issueRelationState';
import { isIssueRelationManageableState } from './isIssueRelationManageableState';

export const isDeletableIssueRelationState = selectorFamily<boolean, IssueRelationID | null>({
  key: 'isDeletableIssueRelation',
  get: (id) => {
    return ({ get }) => {
      if (!id) {
        return false;
      }

      const { issueFromId, issueToId } = get(issueRelationState(id));

      return (
        get(isIssueRelationManageableState(issueFromId)) || get(isIssueRelationManageableState(issueToId))
      );
    };
  },
});
