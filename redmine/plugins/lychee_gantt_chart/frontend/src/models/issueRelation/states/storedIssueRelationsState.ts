import { atom } from 'recoil';
import { StoredIssueRelation } from '../types';

export const storedIssueRelationsState = atom<StoredIssueRelation[]>({
  key: 'storedIssueRelations',
  default: [],
});
