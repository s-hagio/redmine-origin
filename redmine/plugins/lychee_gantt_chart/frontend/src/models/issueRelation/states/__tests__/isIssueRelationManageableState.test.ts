import { selector, useRecoilRefresher_UNSTABLE, useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { issueEntityState } from '@/models/issue';
import { leafIssue } from '@/__fixtures__';
import { isIssueRelationManageableState } from '../isIssueRelationManageableState';

const isAllowedToMock = vi.fn();
const isAllowedToStateMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/currentUser', async (importOriginal) =>
  Object.defineProperties(await importOriginal(), {
    isAllowedToState: { get: isAllowedToStateMock },
  })
);

isAllowedToStateMock.mockReturnValue(
  selector({
    key: 'isAllowedTo/mock',
    get: () => isAllowedToMock,
  })
);

describe('対象チケットが存在する', () => {
  const issue = { ...leafIssue };

  const renderForTesting = () => {
    const renderResult = renderRecoilHook(
      () => ({
        isManageable: useRecoilValue(isIssueRelationManageableState(issue.id)),
        refresh: useRecoilRefresher_UNSTABLE(isIssueRelationManageableState(issue.id)),
      }),
      {
        initializeState: ({ set }) => {
          set(issueEntityState(issue.id), issue);
        },
      }
    );

    act(() => {
      renderResult.result.current.refresh(); // selectorを再評価するため更新
    });

    return renderResult;
  };

  test('manage_issue_relations権限がある場合はtrue', () => {
    isAllowedToMock.mockImplementation(
      (permName, context) => permName === 'manage_issue_relations' && context?.id === issue.id
    );

    const { result } = renderForTesting();

    expect(result.current.isManageable).toBe(true);
  });

  test('manage_issue_relations権限がない場合はfalse', () => {
    isAllowedToMock.mockReturnValue(false);

    const { result } = renderForTesting();

    expect(result.current.isManageable).toBe(false);
  });
});

test('対象チケットが存在しない場合はfalse', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(isIssueRelationManageableState(-1)));

  expect(result.current).toBe(false);
});
