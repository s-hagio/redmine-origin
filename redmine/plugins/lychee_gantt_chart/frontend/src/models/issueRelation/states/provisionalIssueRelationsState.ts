import { atom } from 'recoil';
import { ProvisionalIssueRelation } from '../types';

export const provisionalIssueRelationsState = atom<ProvisionalIssueRelation[]>({
  key: 'provisionalIssueRelations',
  default: [],
});
