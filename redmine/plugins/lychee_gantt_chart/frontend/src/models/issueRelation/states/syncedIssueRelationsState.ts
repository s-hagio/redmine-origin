import { selector } from 'recoil';
import { createClient } from '@/api';
import { issueQueryParamsState } from '@/models/query';

export const syncedIssueRelationsState = selector({
  key: 'syncedIssueRelations',
  get: async ({ get }) => {
    const query = get(issueQueryParamsState);

    return await createClient().issue_relations.$get({ query });
  },
});
