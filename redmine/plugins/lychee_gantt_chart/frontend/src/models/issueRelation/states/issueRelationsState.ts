import { selector } from 'recoil';
import { IssueRelation } from '../types';
import { storedIssueRelationsState } from './storedIssueRelationsState';
import { provisionalIssueRelationsState } from './provisionalIssueRelationsState';

export const issueRelationsState = selector<IssueRelation[]>({
  key: 'issueRelations',
  get: ({ get }) => {
    const storedIssueRelations = get(storedIssueRelationsState);
    const provisionalIssueRelations = get(provisionalIssueRelationsState);

    return [...storedIssueRelations, ...provisionalIssueRelations];
  },
});
