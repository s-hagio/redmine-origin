import { selector } from 'recoil';
import { IssueRelation, IssueRelationID } from '../types';
import { issueRelationsState } from './issueRelationsState';

type IssueRelationMap = ReadonlyMap<IssueRelationID, IssueRelation>;

export const issueRelationMapState = selector<IssueRelationMap>({
  key: 'issueRelationMap',
  get: ({ get }) => {
    const issueRelations = get(issueRelationsState);

    return new Map(issueRelations.map((issueRelation) => [issueRelation.id, issueRelation]));
  },
});
