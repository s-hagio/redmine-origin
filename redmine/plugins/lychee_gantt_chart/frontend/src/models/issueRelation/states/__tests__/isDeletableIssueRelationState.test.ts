import { atomFamily, MutableSnapshot, useRecoilRefresher_UNSTABLE, useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { StoredIssueRelation, RelationType, IssueRelationID } from '../../types';
import { storedIssueRelationsState } from '../storedIssueRelationsState';
import { isDeletableIssueRelationState } from '../isDeletableIssueRelationState';

const isIssueRelationManageableStateMock = vi.hoisted(() => vi.fn());
const issueRelationManageableMockState = atomFamily<boolean, IssueRelationID>({
  key: 'isIssueRelationManageable/mock',
  default: false,
});
isIssueRelationManageableStateMock.mockReturnValue(issueRelationManageableMockState);

vi.mock('../isIssueRelationManageableState', () =>
  Object.defineProperties({}, { isIssueRelationManageableState: { get: isIssueRelationManageableStateMock } })
);

const issueFromId = 101;
const issueToId = 102;
const issueRelation: StoredIssueRelation = {
  id: 1,
  relationType: RelationType.Precedes,
  issueFromId,
  issueToId,
};

const renderForTesting = (initializeState?: (mutableSnapshot: MutableSnapshot) => void) => {
  const renderResult = renderRecoilHook(
    () => ({
      isDeletable: useRecoilValue(isDeletableIssueRelationState(issueRelation.id)),
      refresh: useRecoilRefresher_UNSTABLE(isDeletableIssueRelationState(issueRelation.id)),
    }),
    {
      initializeState: (mutableSnapshot) => {
        const { set, reset } = mutableSnapshot;

        set(storedIssueRelationsState, [issueRelation]);
        [issueFromId, issueToId].forEach((id) => reset(issueRelationManageableMockState(id)));

        initializeState?.(mutableSnapshot);
      },
    }
  );

  act(() => renderResult.result.current.refresh());

  return renderResult;
};

describe('対象チケットのいずれかでIssueRelation管理権限がある', () => {
  test.each([issueFromId, issueToId])('%i', (issueId) => {
    const { result } = renderForTesting(({ set }) => {
      set(issueRelationManageableMockState(issueId), true);
    });

    expect(result.current.isDeletable).toBe(true);
  });
});

test('IssueRelation管理権限がない場合はfalse', () => {
  const { result } = renderForTesting();

  expect(result.current.isDeletable).toBe(false);
});

test('IDがnullなら常にfalse', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(isDeletableIssueRelationState(null)));

  expect(result.current).toBe(false);
});
