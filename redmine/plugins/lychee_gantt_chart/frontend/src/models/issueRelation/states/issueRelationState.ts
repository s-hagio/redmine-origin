import { selectorFamily } from 'recoil';
import { IssueRelation, IssueRelationID } from '../types';
import { issueRelationMapState } from './issueRelationMapState';

export const issueRelationState = selectorFamily<IssueRelation, IssueRelationID>({
  key: 'issueRelation',
  get: (id) => {
    return ({ get }) => {
      const map = get(issueRelationMapState);
      const issueRelation = map.get(id);

      if (!issueRelation) {
        throw new Error(`IssueRelation not found: ${id}`);
      }

      return issueRelation;
    };
  },
});
