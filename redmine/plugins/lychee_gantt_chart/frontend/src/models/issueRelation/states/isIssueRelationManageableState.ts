import { selectorFamily } from 'recoil';
import { IssueID, issueState } from '@/models/issue';
import { isAllowedToState } from '@/models/currentUser';

export const isIssueRelationManageableState = selectorFamily<boolean, IssueID>({
  key: 'isIssueRelationManageable',
  get: (issueId) => {
    return ({ get }) => {
      const isAllowedTo = get(isAllowedToState);
      const issue = get(issueState(issueId));

      return !!issue && isAllowedTo('manage_issue_relations', issue);
    };
  },
});
