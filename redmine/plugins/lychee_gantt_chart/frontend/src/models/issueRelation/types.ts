import { Issue, IssueID, IssueRelatedResources } from '@/models/issue';

export type StoredIssueRelationID = number;
export type ProvisionalIssueRelationID = `provisional-${number}`;
export type IssueRelationID = StoredIssueRelationID | ProvisionalIssueRelationID;

export const RelationType = {
  Relates: 'relates',
  Duplicates: 'duplicates',
  Duplicated: 'duplicated',
  Blocks: 'blocks',
  Blocked: 'blocked',
  Precedes: 'precedes',
  Follows: 'follows',
  CopiedTo: 'copied_to',
  CopiedFrom: 'copied_from',
  Draft: 'draft',
} as const;

export type RelationType = (typeof RelationType)[keyof typeof RelationType];

export type BaseIssueRelation = {
  relationType: RelationType;
  issueFromId: IssueID;
  issueToId: IssueID;
  delay?: number;
  isDeleting?: boolean;
};

export type StoredIssueRelation = {
  id: StoredIssueRelationID;
} & BaseIssueRelation;

export type ProvisionalIssueRelation = {
  id: ProvisionalIssueRelationID;
} & BaseIssueRelation;

export type IssueRelation = StoredIssueRelation | ProvisionalIssueRelation;

export type IssueRelationWithRelatedResources = {
  issueRelation: StoredIssueRelation;
  issueFrom: Issue;
  issueTo: Issue;
  relatedResources: IssueRelatedResources;
};

export const IssueRelationParamKeys = ['relationType', 'issueFromId', 'issueToId', 'delay'] as const;
export type IssueRelationParamKeys = (typeof IssueRelationParamKeys)[number];
export type IssueRelationParams = Pick<BaseIssueRelation, IssueRelationParamKeys>;
