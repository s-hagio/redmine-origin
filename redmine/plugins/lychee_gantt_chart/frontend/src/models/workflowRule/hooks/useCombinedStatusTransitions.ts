import { useRecoilValue } from 'recoil';
import { combinedStatusTransitionsState } from '../states';

export const useCombinedStatusTransitions = () => useRecoilValue(combinedStatusTransitionsState);
