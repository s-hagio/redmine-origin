import { useRecoilValue } from 'recoil';
import { workflowRulesCollectionState } from '../states';

export const useWorkflowRulesCollection = () => useRecoilValue(workflowRulesCollectionState);
