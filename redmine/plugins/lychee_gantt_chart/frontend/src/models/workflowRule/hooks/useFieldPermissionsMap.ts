import { useRecoilValue } from 'recoil';
import { combinedFieldPermissionsMapState } from '../states';

export const useFieldPermissionsMap = () => useRecoilValue(combinedFieldPermissionsMapState);
