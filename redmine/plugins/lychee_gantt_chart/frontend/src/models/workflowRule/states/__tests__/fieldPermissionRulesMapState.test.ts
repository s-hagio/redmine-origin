import { atom, constSelector, useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { CurrentUser, currentUserState } from '@/models/currentUser';
import { combinedFieldPermissionsMapState } from '../combinedFieldPermissionsMapState';

vi.mock('@/models/currentUser', async (importOriginal) => ({
  ...(await importOriginal<object>()),
  currentUserState: atom<Pick<CurrentUser, 'isAdmin' | 'roleIdsByProjectId'>>({
    key: 'currentUser/mock',
    default: {
      isAdmin: false,
      roleIdsByProjectId: {
        101: [1, 2, 3],
        102: [1],
        103: [3, 4],
      },
    },
  }),
}));

vi.mock('../workflowRulesCollectionState', () => ({
  workflowRulesCollectionState: constSelector({
    fieldPermissions: [
      { roleId: 1, trackerId: 1, statusId: 1, fieldName: 'subject', rule: 'readonly' },
      { roleId: 2, trackerId: 1, statusId: 1, fieldName: 'subject', rule: 'required' },
      { roleId: 3, trackerId: 1, statusId: 1, fieldName: 'subject', rule: 'readonly' },
      { roleId: 1, trackerId: 1, statusId: 1, fieldName: 'startDate', rule: 'readonly' },
      { roleId: 1, trackerId: 1, statusId: 2, fieldName: 'startDate', rule: 'required' },
      { roleId: 3, trackerId: 1, statusId: 1, fieldName: 'description', rule: 'readonly' },
    ],
  }),
}));

test('プロジェクト(ID)ごとに有効なFieldPermissionRule配列のMapを返す', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(combinedFieldPermissionsMapState));

  expect(result.current).toStrictEqual(
    new Map([
      [101, [{ trackerId: 1, statusId: 1, fieldName: 'subject', rule: 'required' }]],
      [
        102,
        [
          { trackerId: 1, statusId: 1, fieldName: 'subject', rule: 'readonly' },
          { trackerId: 1, statusId: 1, fieldName: 'startDate', rule: 'readonly' },
          { trackerId: 1, statusId: 2, fieldName: 'startDate', rule: 'required' },
        ],
      ],
      [103, []],
    ])
  );
});

test('システム管理者の場合は空のMapを返す', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(combinedFieldPermissionsMapState), {
    initializeState: ({ set }) => {
      set(currentUserState, {
        id: 1,
        isAdmin: true,
        permissionsByProjectId: {},
        roleIdsByProjectId: {},
      });
    },
  });

  expect(result.current).toStrictEqual(new Map());
});
