import { selector } from 'recoil';
import { CombinedStatusTransition } from '../types';
import { workflowRulesCollectionState } from './workflowRulesCollectionState';

export const combinedStatusTransitionsState = selector<CombinedStatusTransition[]>({
  key: 'combinedStatusTransitionsState',
  get: ({ get }) => {
    const { combinedStatusTransitions } = get(workflowRulesCollectionState);

    return combinedStatusTransitions;
  },
});
