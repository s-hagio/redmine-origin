import { selector } from 'recoil';
import { createClient } from '@/api';
import { WorkflowRulesCollection } from '../types';

export const workflowRulesCollectionState = selector<WorkflowRulesCollection>({
  key: 'workflowRulesCollection',
  get: async () => {
    return createClient().workflow_rules.$get();
  },
});
