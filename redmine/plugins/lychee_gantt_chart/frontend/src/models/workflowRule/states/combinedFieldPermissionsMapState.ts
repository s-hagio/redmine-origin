import { selector } from 'recoil';
import { groupBy } from '@/utils/array';
import { omit } from '@/utils/object';
import { currentUserState } from '@/models/currentUser';
import { ProjectID } from '@/models/project';
import { CombinedFieldPermission } from '../types';
import { workflowRulesCollectionState } from './workflowRulesCollectionState';

type CombinedFieldPermissionsMap = ReadonlyMap<ProjectID, ReadonlyArray<CombinedFieldPermission>>;

export const combinedFieldPermissionsMapState = selector<CombinedFieldPermissionsMap>({
  key: 'fieldPermissionRulesMap',
  get: ({ get }) => {
    const { isAdmin, roleIdsByProjectId } = get(currentUserState);
    const map = new Map<ProjectID, CombinedFieldPermission[]>();

    if (isAdmin) {
      return map;
    }

    const { fieldPermissions } = get(workflowRulesCollectionState);
    const fieldPermissionRulesByRoleId = groupBy(fieldPermissions, 'roleId');

    for (const projectId in roleIdsByProjectId) {
      const roleIds = roleIdsByProjectId[projectId];

      const scopedRulesList = Object.values(
        groupBy(
          roleIds.flatMap((roleId) => fieldPermissionRulesByRoleId[roleId] ?? []),
          (rule) => [rule.trackerId, rule.statusId, rule.fieldName].join('/')
        )
      );

      const combinedFieldPermissions: CombinedFieldPermission[] = [];

      scopedRulesList.forEach((scopedRules) => {
        // 割り当てられたロールとルールの数が異なる場合、ルールは適用されない
        if (scopedRules.length !== roleIds.length) {
          return;
        }

        const ruleTypes = [...new Set(scopedRules.map(({ rule }) => rule))];

        combinedFieldPermissions.push({
          ...omit(scopedRules[0], ['roleId']),
          rule: ruleTypes.length === 1 ? ruleTypes[0] : 'required',
        });
      });

      map.set(Number(projectId), combinedFieldPermissions);
    }

    return map;
  },
});
