import { RoleID } from '@/models/currentUser';
import { AssociatedIssueStatus, AssociatedTracker } from '@/models/associatedResource';
import { FieldName } from '@/models/fieldDefinition';

export type FieldPermission = {
  roleId: RoleID;
  trackerId: AssociatedTracker['id'];
  statusId: AssociatedIssueStatus['id'];
  fieldName: FieldName;
  rule: 'readonly' | 'required';
};

export type CombinedFieldPermission = Omit<FieldPermission, 'roleId'>;

export type CombinedStatusTransition = {
  roleId: RoleID;
  trackerId: AssociatedTracker['id'];
  oldStatusId: AssociatedIssueStatus['id'];
  newStatusIds: AssociatedIssueStatus['id'][];
  isForAssignee: boolean;
  isForAuthor: boolean;
};

export type WorkflowRulesCollection = {
  fieldPermissions: FieldPermission[];
  combinedStatusTransitions: CombinedStatusTransition[];
};
