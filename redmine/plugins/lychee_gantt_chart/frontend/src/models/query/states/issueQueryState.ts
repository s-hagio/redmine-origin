import { atom } from 'recoil';
import { persistJSON, restorePersistedJSON } from '@/utils/storage';
import { IssueQuery } from '../types';
import { defaultIssueQueryState } from './defaultIssueQueryState';

const ISSUE_QUERY_STORED_KEY = 'lycheeGantt:issueQuery';

export const issueQueryState = atom<IssueQuery>({
  key: 'issueQuery',
  effects: [
    ({ setSelf, getLoadable, trigger }) => {
      if (trigger !== 'get') {
        return;
      }

      const defaultQuery = getLoadable(defaultIssueQueryState).getValue();
      const storedQuery = restorePersistedJSON<IssueQuery>(ISSUE_QUERY_STORED_KEY);
      const query = { ...defaultQuery };

      if (storedQuery?.projectId === query.projectId) {
        Object.assign(query, storedQuery);
      }

      setSelf(query);
    },
    ({ onSet }) => {
      onSet((query) => {
        persistJSON(ISSUE_QUERY_STORED_KEY, query);
      });
    },
  ],
});
