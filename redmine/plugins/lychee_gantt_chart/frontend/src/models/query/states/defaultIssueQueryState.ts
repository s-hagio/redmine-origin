import { selector } from 'recoil';
import { settingsState } from '@/models/setting';
import { IssueQuery } from '../types';
import { DEFAULT_QUERY } from '../constants';

export const defaultIssueQueryState = selector<IssueQuery>({
  key: 'defaultIssueQuery',
  get: ({ get }) => {
    const settings = get(settingsState);

    const query = { ...DEFAULT_QUERY };
    const projectId = settings.mainProject?.id ?? null;

    if (projectId) {
      Object.assign(query, { projectId });
    }

    return query;
  },
});
