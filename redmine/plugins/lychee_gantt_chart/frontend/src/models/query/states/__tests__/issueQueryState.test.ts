import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { Settings, settingsState } from '@/models/setting';
import { issueQueryState } from '../../states';
import { DEFAULT_QUERY } from '../../constants';

test('デフォルト', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(issueQueryState));

  expect(result.current).toStrictEqual(DEFAULT_QUERY);
});

test('settings.mainProjectがある場合はprojectIdをセット', () => {
  const mainProject = { id: 1, identifier: 'a' };

  const { result } = renderRecoilHook(() => useRecoilValue(issueQueryState), {
    initializeState: ({ set }) => {
      set(settingsState, { mainProject } as Settings);
    },
  });

  expect(result.current).toStrictEqual({ ...DEFAULT_QUERY, projectId: mainProject.id });
});
