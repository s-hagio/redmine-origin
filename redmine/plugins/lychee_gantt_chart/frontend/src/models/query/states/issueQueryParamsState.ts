import { atom, DefaultValue } from 'recoil';
import { convertIssueQueryToParams } from '../helpers';
import { IssueQueryParams } from '../types';
import { issueQueryState } from './issueQueryState';

export const issueQueryParamsState = atom<IssueQueryParams>({
  key: 'issueQueryParams',
  effects: [
    ({ setSelf, getLoadable, trigger }) => {
      if (trigger !== 'get') {
        return;
      }

      setSelf((current) => {
        if (current instanceof DefaultValue) {
          return convertIssueQueryToParams(getLoadable(issueQueryState).getValue());
        }
        return current;
      });
    },
  ],
});
