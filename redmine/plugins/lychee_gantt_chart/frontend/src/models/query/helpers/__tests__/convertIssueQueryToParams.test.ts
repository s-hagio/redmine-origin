import { omit } from '@/utils/object';
import { convertIssueQueryToParams } from '../convertIssueQueryToParams';
import { IssueQuery } from '../../types';

const issueQuery: IssueQuery = {
  projectId: 123,
  f: ['id', 'subject'],
  op: { id: '=', subject: '~' },
  v: { id: ['1'], subject: ['test'] },
  sort: { 0: ['id', 'asc'] },
  c: ['id', 'subject'],
  groupBy: null,
  includeAncestorIssues: false,
};

test('IssueQueryをIssueQueryParamsに変換', () => {
  expect(convertIssueQueryToParams({ ...issueQuery, includeAncestorIssues: true })).toEqual({
    ...omit(issueQuery, ['c']),
    includeAncestorIssues: 1,
  });
});

test('フィルタが一つもない場合はダミーを追加', () => {
  const noFilterIssueQuery: IssueQuery = {
    ...issueQuery,
    f: [],
  };

  expect(convertIssueQueryToParams(noFilterIssueQuery)).toEqual(
    expect.objectContaining({
      f: ['_'],
    })
  );
});
