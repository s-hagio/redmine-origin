import { pick } from '@/utils/object';
import { IssueQuery, IssueQueryParams } from '../types';

export const convertIssueQueryToParams = (query: IssueQuery): IssueQueryParams => {
  if (query.id) {
    return {
      id: query.id,
      projectId: query.projectId,
    };
  }

  const params: IssueQueryParams = {
    projectId: query.projectId,
    ...pick(query, ['f', 'op', 'v', 'sort', 'groupBy']),
    includeAncestorIssues: query.includeAncestorIssues ? 1 : 0,
  };

  if (params.f.length === 0) {
    params.f = ['_'];
  }

  return params;
};
