import { useRecoilCallback } from 'recoil';
import { IssueQuery } from '../types';
import { convertIssueQueryToParams } from '../helpers';
import { issueQueryParamsState, issueQueryState } from '../states';

type UpdateIssueQueryParams = (partialQuery: Partial<IssueQuery>) => void;

export const useUpdateIssueQueryParams = (): UpdateIssueQueryParams => {
  return useRecoilCallback(({ set, snapshot }) => {
    return (partialQuery) => {
      const query = snapshot.getLoadable(issueQueryState).getValue();
      const newQuery = { ...query, ...partialQuery };

      set(issueQueryState, newQuery);
      set(issueQueryParamsState, convertIssueQueryToParams(newQuery));
    };
  }, []);
};
