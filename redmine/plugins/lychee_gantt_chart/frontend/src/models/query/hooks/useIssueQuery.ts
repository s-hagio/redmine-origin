import { useRecoilValue } from 'recoil';
import { issueQueryState } from '../states';

export const useIssueQuery = () => useRecoilValue(issueQueryState);
