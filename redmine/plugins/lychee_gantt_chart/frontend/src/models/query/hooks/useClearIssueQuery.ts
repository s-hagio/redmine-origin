import { useRecoilCallback } from 'recoil';
import { useUpdateIssueQueryParams } from '../hooks';
import { defaultIssueQueryState } from '../states';

export const useClearIssueQuery = () => {
  const updateIssueQueryParams = useUpdateIssueQueryParams();

  return useRecoilCallback(
    ({ snapshot }) => {
      return () => {
        updateIssueQueryParams(snapshot.getLoadable(defaultIssueQueryState).getValue());
      };
    },
    [updateIssueQueryParams]
  );
};
