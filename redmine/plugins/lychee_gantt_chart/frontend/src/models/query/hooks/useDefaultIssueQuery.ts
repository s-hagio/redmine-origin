import { useRecoilValue } from 'recoil';
import { defaultIssueQueryState } from '../states';

export const useDefaultIssueQuery = () => useRecoilValue(defaultIssueQueryState);
