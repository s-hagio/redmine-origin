import { QueryFilterOperator, QueryFilterValue } from '@/models/queryFilter';
import { CustomFieldName } from '@/models/customField';

export type IssueQueryID = number;

export type ColumnName = string | CustomFieldName;

export type SortDirection = 'asc' | 'desc' | '';
export type SortOrder = [ColumnName, SortDirection];

export type GroupBy = ColumnName | null;

export type SortCriteria = {
  0?: SortOrder;
  1?: SortOrder;
  2?: SortOrder;
};

export type IssueQuery = {
  id?: IssueQueryID;
  projectId?: number;
  f: ColumnName[];
  op: Record<ColumnName, QueryFilterOperator>;
  v: Record<ColumnName, QueryFilterValue>;
  c: ColumnName[];
  groupBy: GroupBy;
  sort: SortCriteria;
  includeAncestorIssues: boolean;
};

export type BooleanQueryParamKey = KeysByType<IssueQuery, boolean>;
export type BooleanQueryParams = Record<BooleanQueryParamKey, 0 | 1>;

export type IssueQueryParams =
  | {
      id: IssueQueryID;
      projectId?: number;
    }
  | (Pick<IssueQuery, 'projectId' | 'f' | 'op' | 'v' | 'sort'> & BooleanQueryParams);
