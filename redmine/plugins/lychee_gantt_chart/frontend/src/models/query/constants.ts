import { IssueQuery } from './types';

export const DEFAULT_QUERY: Readonly<IssueQuery> = {
  f: ['status_id'],
  op: { status_id: 'o' },
  v: {},
  c: ['subject', 'status', 'assigned_to', 'start_date', 'due_date'],
  sort: {
    0: ['start_date', 'asc'],
  },
  groupBy: null,
  includeAncestorIssues: false,
};

export const sortDirections = ['asc', 'desc'] as const;
