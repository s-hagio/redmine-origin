import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { createStateInitializer } from '@/models';
import { currentUserState, CurrentUser } from '@/models/currentUser';
import { settingsState, Settings } from '@/models/setting';

describe('createStateInitializer', () => {
  test('初期状態をセットする関数を返す', () => {
    const currentUser: CurrentUser = {
      isAdmin: true,
      permissionsByProjectId: null,
    };

    const settings = {
      mainProject: { id: 111, identifier: 'foo' },
    } as Settings;

    const initializeState = createStateInitializer(currentUser, settings);
    const { result } = renderRecoilHook(
      () => ({
        currentUser: useRecoilValue(currentUserState),
        settings: useRecoilValue(settingsState),
      }),
      { initializeState }
    );

    expect(result.current).toStrictEqual({
      currentUser,
      settings,
    });
  });
});
