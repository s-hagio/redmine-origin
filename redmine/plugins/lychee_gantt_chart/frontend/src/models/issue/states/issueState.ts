import { selectorFamily } from 'recoil';
import { omit } from '@/utils/object';
import { Issue, IssueID } from '../types';
import { issueEntityState } from './issueEntityState';
import { issueTemporaryChangesState } from './issueTemporaryChangesState';

export const issueState = selectorFamily<Issue | null, IssueID>({
  key: 'issue',
  get: (id) => {
    return ({ get }) => {
      const entity = get(issueEntityState(id));
      const temporaryChanges = get(issueTemporaryChangesState(id));

      if (!entity) {
        return null;
      }

      const temporaryChangedAttributes = temporaryChanges.reduce<Partial<Issue>>(
        (memo, { attributes }) => ({
          ...memo,
          ...attributes,
          customFieldValues: {
            ...memo.customFieldValues,
            ...attributes.customFieldValues,
          },
        }),
        {}
      );

      return {
        ...entity,
        ...omit(temporaryChangedAttributes, ['customFieldValues']),
        customFieldValues: {
          ...entity.customFieldValues,
          ...temporaryChangedAttributes.customFieldValues,
        },
      };
    };
  },
});
