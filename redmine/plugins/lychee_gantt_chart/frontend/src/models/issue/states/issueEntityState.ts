import { atomFamily } from 'recoil';
import { Issue, IssueID } from '../types';

export const issueEntityState = atomFamily<Issue | null, IssueID>({
  key: 'issueEntity',
  default: null,
});
