import { atomFamily } from 'recoil';
import { IssueID, IssueTemporaryChange } from '../types';

export const issueTemporaryChangesState = atomFamily<IssueTemporaryChange[], IssueID>({
  key: 'issueTemporaryChanges',
  default: [],
});
