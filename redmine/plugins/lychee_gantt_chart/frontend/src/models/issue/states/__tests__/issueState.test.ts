import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { floatCustomField, parentIssue, rootIssue, stringCustomField, textCustomField } from '@/__fixtures__';
import { IssueTemporaryChange } from '../../types';
import { issueEntityState } from '../issueEntityState';
import { issueState } from '../issueState';
import { issueTemporaryChangesState } from '../issueTemporaryChangesState';

test('issueEntityとissueTemporaryChangesを合成', () => {
  const temporaryChanges: IssueTemporaryChange[] = [
    { id: 1, attributes: { subject: 'foooooo!!!!' } },
    { id: 2, attributes: { startDate: '2023-09-11' } },
  ];

  const { result } = renderRecoilHook(() => useRecoilValue(issueState(rootIssue.id)), {
    initializeState: ({ set }) => {
      set(issueEntityState(rootIssue.id), rootIssue);
      set(issueTemporaryChangesState(rootIssue.id), temporaryChanges);

      // 別チケのissueTemporaryChangesは無視される
      set(issueTemporaryChangesState(parentIssue.id), [
        { id: 123, attributes: { description: 'parent issue!' } },
      ]);
    },
  });

  expect(result.current).toEqual({
    ...rootIssue,
    subject: temporaryChanges[0].attributes.subject,
    startDate: temporaryChanges[1].attributes.startDate,
    customFieldValues: {},
  });
});

test('ネストしている値（customFieldValues）はマージして合成', () => {
  const temporaryChanges: IssueTemporaryChange[] = [
    { id: 1, attributes: { customFieldValues: { [stringCustomField.id]: 'string changed!' } } },
    { id: 2, attributes: { customFieldValues: { [textCustomField.id]: 'text added!' } } },
  ];

  const { result } = renderRecoilHook(() => useRecoilValue(issueState(rootIssue.id)), {
    initializeState: ({ set }) => {
      set(issueEntityState(rootIssue.id), {
        ...rootIssue,
        customFieldValues: {
          [stringCustomField.id]: 'string!',
          [floatCustomField.id]: 1.23,
        },
      });

      set(issueTemporaryChangesState(rootIssue.id), temporaryChanges);
    },
  });

  expect(result.current).toEqual({
    ...rootIssue,
    customFieldValues: {
      [stringCustomField.id]: 'string changed!',
      [floatCustomField.id]: 1.23,
      [textCustomField.id]: 'text added!',
    },
  });
});
