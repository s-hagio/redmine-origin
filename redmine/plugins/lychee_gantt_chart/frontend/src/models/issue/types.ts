import { ISODateString } from '@/lib/date';
import { ProjectID, Project } from '@/models/project';
import { VersionID, Version } from '@/models/version';
import { CustomFieldID, CustomFieldValue } from '@/models/customField';
import { Attachment } from '@/models/attachment';

export type StoredIssueID = number;
export type ProvisionalIssueID = `provisional-${number}`;

export type IssueID = StoredIssueID | ProvisionalIssueID;

export type IssueCore = {
  id: IssueID;
  projectId: ProjectID;
  fixedVersionId: VersionID | null;
  trackerId: number;
  statusId: number;
  categoryId: number | null;
  priorityId: number;
  assignedToId: number | null;
  authorId: number;
  subject: string;
  startDate: ISODateString | null;
  dueDate: ISODateString | null;
  doneRatio: number | null;
  estimatedHours: number | null;
  remainingEstimate?: number | null;
  parentId: IssueID | null;
  rootId: IssueID;
  lft: number;
  rgt: number;
  isClosed: boolean;
  isPrivate: boolean;
};

export type InlineIssueRelation = {
  id: number;
  label: string;
  otherIssue: {
    id: IssueID;
    subject: string;
  };
};

export type Issue = IssueCore & {
  parentIssueSubject?: string;
  lastUpdatedById?: number | null;
  totalEstimatedHours?: number;
  totalRemainingEstimate?: number | null;
  spentHours?: number | null;
  totalSpentHours?: number | null;
  description?: string;
  formattedDescription?: string;
  relations?: InlineIssueRelation[];
  attachments?: Attachment[];
  customFieldValues?: Record<CustomFieldID, CustomFieldValue>;
  formattedCustomFieldValues?: Record<CustomFieldID, string>;
};

export type IssueParams = Partial<
  Pick<
    Issue,
    | 'id'
    | 'projectId'
    | 'fixedVersionId'
    | 'trackerId'
    | 'statusId'
    | 'categoryId'
    | 'priorityId'
    | 'assignedToId'
    | 'subject'
    | 'startDate'
    | 'dueDate'
    | 'doneRatio'
    | 'estimatedHours'
    | 'parentId'
    | 'description'
    | 'customFieldValues'
  >
>;

export type BatchIssueParams = { id: IssueID } & IssueParams;
export type BatchIssueError = { id: IssueID; errors: string[] };

export type IssueRelatedResources = {
  projects: Project[];
  versions: Version[];
  issues: Issue[];
};

export type IssueWithRelatedResources = {
  issue: Issue;
  relatedResources: IssueRelatedResources;
};

export type IssueTemporaryChangeID = number;

export type IssueTemporaryChange = {
  id: IssueTemporaryChangeID;
  attributes: Partial<Issue>;
};

export type SetIssueTemporaryChange = (attributes: IssueTemporaryChange['attributes']) => void;
export type RemoveIssueTemporaryChange = () => void;

export type IssueTemporaryChangeCallbacks = {
  setTemporaryChange: SetIssueTemporaryChange;
  removeTemporaryChange: RemoveIssueTemporaryChange;
};
