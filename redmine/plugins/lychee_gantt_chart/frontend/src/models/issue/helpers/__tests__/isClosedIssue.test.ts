import { Issue } from '../../types';
import { isClosedIssue } from '../isClosedIssue';

test('isClosedがtrueまたは進捗率が100%ならtrue', () => {
  expect(isClosedIssue({ isClosed: true, doneRatio: 0 } as Issue)).toBe(true);
  expect(isClosedIssue({ isClosed: false, doneRatio: 100 } as Issue)).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isClosedIssue({ isClosed: false, doneRatio: 0 } as Issue)).toBe(false);
  expect(isClosedIssue({ isClosed: false, doneRatio: 50 } as Issue)).toBe(false);
});
