import { Issue } from '../types';

export const isClosedIssue = (issue: Issue | null): boolean => {
  return !!issue && (issue.isClosed || issue.doneRatio === 100);
};
