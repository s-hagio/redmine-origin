export const isNewIssueId = (id: unknown): boolean => {
  return !id || `${id}`.startsWith(`provisional-`);
};
