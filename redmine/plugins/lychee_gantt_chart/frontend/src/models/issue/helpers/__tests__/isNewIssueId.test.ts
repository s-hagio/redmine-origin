import { isNewIssueId } from '../isNewIssueId';

test('IDが空ならtrue', () => {
  expect(isNewIssueId('')).toBe(true);
});

test('IDが"provisional-"で始まるならtrue', () => {
  expect(isNewIssueId('provisional-1')).toBe(true);
});

test('それ以外はfalse', () => {
  expect(isNewIssueId(1)).toBe(false);
  expect(isNewIssueId('1')).toBe(false);
});
