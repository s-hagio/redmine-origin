import { useRecoilValue } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { issueTemporaryChangesState } from '../../states';
import { useCreateIssueTemporaryChangeCallbacks } from '../useCreateIssueTemporaryChangeCallbacks';

const issueId = 123;

const renderForTesting = () => {
  return renderRecoilHook(() => ({
    temporaryChanges: useRecoilValue(issueTemporaryChangesState(issueId)),
    createCallbacks: useCreateIssueTemporaryChangeCallbacks(),
  }));
};

describe('setTemporaryChange', () => {
  test('新しいTemporaryChangeをセットする', () => {
    const { result } = renderForTesting();
    const { setTemporaryChange } = result.current.createCallbacks(issueId);

    act(() => setTemporaryChange({ subject: 'test' }));

    expect(result.current.temporaryChanges[0].attributes).toEqual({ subject: 'test' });
  });

  test('複数回実行した場合は追加したTemporaryChangeを上書きする', () => {
    const { result } = renderForTesting();
    const { setTemporaryChange } = result.current.createCallbacks(issueId);

    act(() => setTemporaryChange({ subject: 'the first' }));

    const id = result.current.temporaryChanges[0].id;

    expect.assertions(6);

    [{ subject: 'test' }, { startDate: '2023-09-11' }, { description: 'waaai!' }].forEach((attributes) => {
      act(() => setTemporaryChange(attributes));

      expect(result.current.temporaryChanges).toHaveLength(1);
      expect(result.current.temporaryChanges[0]).toEqual({ id, attributes });
    });
  });
});

describe('removeTemporaryChange', () => {
  test('追加したTemporaryChangeを削除する', () => {
    const { result } = renderForTesting();
    const { setTemporaryChange, removeTemporaryChange } = result.current.createCallbacks(issueId);

    expect(result.current.temporaryChanges).toHaveLength(0);

    act(() => setTemporaryChange({ subject: 'test' }));

    expect(result.current.temporaryChanges).toHaveLength(1);

    act(() => removeTemporaryChange());

    expect(result.current.temporaryChanges).toHaveLength(0);
  });
});
