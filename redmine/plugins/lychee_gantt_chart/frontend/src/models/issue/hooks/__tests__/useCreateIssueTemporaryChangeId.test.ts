import { renderHook } from '@testing-library/react';
import { useCreateIssueTemporaryChangeId } from '../useCreateIssueTemporaryChangeId';

test('実行するたびに新しいIDを返す', () => {
  const { result } = renderHook(() => useCreateIssueTemporaryChangeId());

  expect(result.current()).toBe(1);
  expect(result.current()).toBe(2);
  expect(result.current()).toBe(3);
});
