import { useSettings } from '@/models/setting';
import { IssueID } from '../types';

export const useIssueURL = (id: IssueID) => {
  const { baseURL } = useSettings();

  return `${baseURL}/issues/${id}`;
};
