import { useCallback } from 'react';
import { IssueTemporaryChangeID } from '../types';

type CreateIssueTemporaryChangeId = () => IssueTemporaryChangeID;

let idCounter = 0;

export const useCreateIssueTemporaryChangeId = (): CreateIssueTemporaryChangeId => {
  return useCallback(() => ++idCounter, []);
};
