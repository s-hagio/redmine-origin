import { useRecoilValue } from 'recoil';
import { issueState } from '../states';
import { IssueID } from '../types';

export const useIssue = (id: IssueID) => useRecoilValue(issueState(id));
