import { renderRecoilHook } from '@/test-utils';
import { settingsState, Settings } from '@/models/setting';
import { rootIssue } from '@/__fixtures__';
import { useIssueURL } from '../useIssueURL';

test('チケットIDを元にチケットのURLを返す', () => {
  const baseURL = '/path/to/redmine';

  const { result } = renderRecoilHook(() => useIssueURL(rootIssue.id), {
    initializeState: ({ set }) => {
      set(settingsState, { baseURL } as Settings);
    },
  });

  expect(result.current).toBe(`${baseURL}/issues/${rootIssue.id}`);
});
