import { Issue } from '../types';
import { useIssue } from './useIssue';

export const useIssueOrThrow = (id: Issue['id']): Issue => {
  const issue = useIssue(id);

  if (!issue) {
    throw new Error(`Issue ${id} not found`);
  }

  return issue;
};
