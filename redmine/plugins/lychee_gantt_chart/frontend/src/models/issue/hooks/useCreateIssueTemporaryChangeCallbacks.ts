import { useRecoilCallback } from 'recoil';
import { issueTemporaryChangesState } from '../states';
import {
  IssueID,
  IssueTemporaryChangeID,
  IssueTemporaryChange,
  IssueTemporaryChangeCallbacks,
} from '../types';
import { useCreateIssueTemporaryChangeId } from './useCreateIssueTemporaryChangeId';

type Options = {
  temporaryChangeId?: IssueTemporaryChangeID;
};

type CreateIssueTemporaryChangeCallbacks = (
  issueId?: IssueID | null,
  options?: Options
) => IssueTemporaryChangeCallbacks;

export const useCreateIssueTemporaryChangeCallbacks = (): CreateIssueTemporaryChangeCallbacks => {
  const createIssueTemporaryChangeId = useCreateIssueTemporaryChangeId();

  return useRecoilCallback(
    ({ set }) => {
      return (issueId, options = {}) => {
        const id = options.temporaryChangeId ?? createIssueTemporaryChangeId();

        if (!issueId) {
          return {
            setTemporaryChange: () => {},
            removeTemporaryChange: () => {},
          };
        }

        const setTemporaryChange = (attributes: IssueTemporaryChange['attributes']) => {
          const temporaryChange = { id, attributes };

          set(issueTemporaryChangesState(issueId), (current) => {
            const newChanges = [...current];
            const index = newChanges.findIndex((change) => change.id === id);

            if (index !== -1) {
              newChanges[index] = temporaryChange;
            } else {
              newChanges.push(temporaryChange);
            }

            return newChanges;
          });
        };

        const removeTemporaryChange = () => {
          set(issueTemporaryChangesState(issueId), (current) => {
            const newChanges = [...current];
            const index = newChanges.findIndex((change) => change.id === id);

            if (index !== -1) {
              newChanges.splice(index, 1);
            }

            return newChanges;
          });
        };

        return {
          setTemporaryChange,
          removeTemporaryChange,
        };
      };
    },
    [createIssueTemporaryChangeId]
  );
};
