import { renderRecoilHook } from '@/test-utils';
import { rootIssue } from '@/__fixtures__';
import { issueEntityState } from '../../states';
import { useIssue } from '../useIssue';

test('IDに応じたチケットの詳細ステートを返す', () => {
  const { result } = renderRecoilHook(() => useIssue(rootIssue.id), {
    initializeState: ({ set }) => {
      set(issueEntityState(rootIssue.id), rootIssue);
    },
  });

  expect(result.current).toStrictEqual(rootIssue);
});
