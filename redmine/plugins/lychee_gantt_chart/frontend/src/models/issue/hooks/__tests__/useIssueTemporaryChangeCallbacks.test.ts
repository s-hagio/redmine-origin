import { renderHook } from '@testing-library/react';
import { IssueTemporaryChangeCallbacks } from '../../types';
import { useIssueTemporaryChangeCallbacks } from '../useIssueTemporaryChangeCallbacks';

const callbacksMock = vi.hoisted<IssueTemporaryChangeCallbacks>(() => ({
  setTemporaryChange: vi.fn(),
  removeTemporaryChange: vi.fn(),
}));
const createIssueTemporaryChangeCallbacksMock = vi.fn().mockReturnValueOnce(callbacksMock);

vi.mock('../useCreateIssueTemporaryChangeCallbacks', () => ({
  useCreateIssueTemporaryChangeCallbacks: vi.fn(() => createIssueTemporaryChangeCallbacksMock),
}));

test('指定チケットIDのCallbacksを作成してメモ化したものを返す', () => {
  const { result, rerender } = renderHook(() => useIssueTemporaryChangeCallbacks(1));

  expect(result.current).toBe(callbacksMock);

  rerender();

  expect(result.current).toBe(callbacksMock);
});
