import { useMemo } from 'react';
import { IssueID, IssueTemporaryChangeCallbacks } from '../types';
import { useCreateIssueTemporaryChangeCallbacks } from './useCreateIssueTemporaryChangeCallbacks';

export const useIssueTemporaryChangeCallbacks = (issueId: IssueID): IssueTemporaryChangeCallbacks => {
  const createIssueTemporaryChangeCallbacks = useCreateIssueTemporaryChangeCallbacks();

  return useMemo(() => {
    return createIssueTemporaryChangeCallbacks(issueId);
  }, [issueId, createIssueTemporaryChangeCallbacks]);
};
