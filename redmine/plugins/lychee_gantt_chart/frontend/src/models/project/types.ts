import { ISODateString } from '@/lib/date';

export type ProjectID = number;
export type ProjectIdentifier = string;

export type ProjectCore = {
  id: ProjectID;
  identifier: ProjectIdentifier;
  name: string;
  rootId: ProjectID;
  lft: number;
  rgt: number;
};

export type Project = ProjectCore & {
  startDate?: ISODateString | null;
  dueDate?: ISODateString | null;
  completedPercent?: number;
};
