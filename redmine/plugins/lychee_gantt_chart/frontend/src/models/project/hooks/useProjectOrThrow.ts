import { Project } from '../types';
import { useProject } from './useProject';

export const useProjectOrThrow = (id: Project['id']): Project => {
  const project = useProject(id);

  if (!project) {
    throw new Error(`Project with id ${id} not found`);
  }

  return project;
};
