import { renderRecoilHook } from '@/test-utils';
import { mainProject } from '@/__fixtures__';
import { projectState } from '../../states';
import { useProject } from '../useProject';

test('IDに応じたプロジェクトの詳細ステートを返す', () => {
  const { result } = renderRecoilHook(() => useProject(mainProject.id), {
    initializeState: ({ set }) => {
      set(projectState(mainProject.id), mainProject);
    },
  });

  expect(result.current).toBe(mainProject);
});
