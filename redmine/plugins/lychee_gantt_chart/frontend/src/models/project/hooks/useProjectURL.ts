import { useRecoilValue } from 'recoil';
import { settingsState } from '@/models/setting';
import { ProjectIdentifier } from '../types';

export const useProjectURL = (identifier: ProjectIdentifier) => {
  const { baseURL } = useRecoilValue(settingsState);

  return `${baseURL}/projects/${identifier}`;
};
