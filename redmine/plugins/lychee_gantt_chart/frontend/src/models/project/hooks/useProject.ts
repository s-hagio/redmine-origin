import { useRecoilValue } from 'recoil';
import { ProjectID } from '../types';
import { projectState } from '../states';

export const useProject = (id: ProjectID) => useRecoilValue(projectState(id));
