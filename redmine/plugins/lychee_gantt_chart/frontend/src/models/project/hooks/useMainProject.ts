import { useRecoilValue } from 'recoil';
import { mainProjectState } from '../states';

export const useMainProject = () => useRecoilValue(mainProjectState);
