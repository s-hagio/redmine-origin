import { renderRecoilHook } from '@/test-utils';
import { settingsState, Settings } from '@/models/setting';
import { mainProject } from '@/__fixtures__';
import { useProjectURL } from '../useProjectURL';

test('プロジェクト識別子を元にプロジェクトのURLを返す', () => {
  const baseURL = '/path/to/redmine';

  const { result } = renderRecoilHook(() => useProjectURL(mainProject.identifier), {
    initializeState: ({ set }) => {
      set(settingsState, { baseURL } as Settings);
    },
  });

  expect(result.current).toBe(`${baseURL}/projects/${mainProject.identifier}`);
});
