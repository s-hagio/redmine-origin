import { selector } from 'recoil';
import { settingsState } from '@/models/setting';
import { Project } from '../types';
import { projectState } from './projectState';

export const mainProjectState = selector<Project | null>({
  key: 'mainProject',
  get: ({ get }) => {
    const { mainProject } = get(settingsState);

    if (mainProject) {
      return get(projectState(mainProject.id));
    }

    return null;
  },
});
