import { atomFamily } from 'recoil';
import { Project, ProjectID } from '../types';

export const projectState = atomFamily<Project | null, ProjectID>({
  key: 'project',
  default: null,
});
