import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { childProject1, mainProject, rootProject } from '@/__fixtures__';
import { ProjectID } from '../../types';
import { projectState } from '../projectState';
import { mainProjectState } from '../mainProjectState';

const renderForTesting = (mainProjectId: ProjectID | null) => {
  return renderRecoilHook(() => useRecoilValue(mainProjectState), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({
        ...current,
        mainProject: mainProjectId ? { id: mainProjectId, identifier: mainProject.identifier } : void 0,
      }));
      set(projectState(rootProject.id), rootProject);
      set(projectState(mainProject.id), mainProject);
      set(projectState(childProject1.id), childProject1);
    },
  });
};

test('メインプロジェクトを返す', () => {
  const { result } = renderForTesting(mainProject.id);

  expect(result.current).toEqual(mainProject);
});

test('メインプロジェクトIDが指定されていない場合はnullを返す', () => {
  const { result } = renderForTesting(null);

  expect(result.current).toBeNull();
});
