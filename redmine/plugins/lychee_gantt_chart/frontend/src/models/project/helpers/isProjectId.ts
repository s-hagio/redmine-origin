import { ProjectID } from '../types';

export const isProjectId = (arg: unknown): arg is ProjectID => {
  return typeof arg === 'number';
};
