import { getGroupId } from '../getGroupId';

test('カラム名とグループ値からグループIDを取得', () => {
  expect(getGroupId('status', 1)).toBe('status-1');
  expect(getGroupId('created_at', '2020-07-25T07:05:40.000Z')).toBe('created_at-2020-07-25T07:05:40.000Z');
  expect(getGroupId('is_private', true)).toBe('is_private-true');
});
