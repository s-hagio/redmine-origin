import { formatISODate } from '@/lib/date';
import {
  createdOnFieldDefinition,
  rootIssue,
  stringCustomField,
  stringCustomFieldDefinition,
  trackerFieldDefinition,
} from '@/__fixtures__';
import { getGroupValue } from '../getGroupValue';

const issue = {
  ...rootIssue,
  trackerId: 123,
  createdOn: '2024-01-23T23:12:12Z',
  customFieldValues: {
    [stringCustomField.id]: null,
  },
};

test('IssueからGroupValueを取得する', () => {
  expect(getGroupValue(issue, trackerFieldDefinition)).toBe(issue.trackerId);
});

test('DateTime型の場合はDate形式に変換する', () => {
  expect(getGroupValue(issue, createdOnFieldDefinition)).toBe(formatISODate(issue.createdOn));
});

test('nullの場合は空文字列を返す', () => {
  expect(getGroupValue(issue, stringCustomFieldDefinition)).toBe('');
});
