import { GroupValue } from '../types';

export const getGroupId = (columnName: string, value: GroupValue): string => {
  return `${columnName}-${value}`;
};
