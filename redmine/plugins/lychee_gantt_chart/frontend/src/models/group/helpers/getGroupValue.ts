import { get as getValue } from '@/utils/object';
import { formatISODate } from '@/lib/date';
import { FieldDefinition, FieldFormat } from '@/models/fieldDefinition';
import { Issue } from '@/models/issue';
import { GroupValue } from '../types';

const BLANK_VALUE = '';

export const getGroupValue = (issue: Issue, field: FieldDefinition): GroupValue => {
  const value = getValue<GroupValue>(issue, field.valuePath);

  if (value && field.format === FieldFormat.DateTime) {
    return formatISODate(value + '');
  }

  return value ?? BLANK_VALUE;
};
