import { useRecoilValue } from 'recoil';
import { groupFieldState } from '../states';

export const useGroupField = () => useRecoilValue(groupFieldState);
