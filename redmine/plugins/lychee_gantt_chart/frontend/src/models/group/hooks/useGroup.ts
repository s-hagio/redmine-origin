import { useRecoilValue } from 'recoil';
import { groupState } from '../states';
import { GroupID } from '../types';

export const useGroup = (id: GroupID) => useRecoilValue(groupState(id));
