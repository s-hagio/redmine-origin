import { GroupID } from '../types';
import { useGroup } from './useGroup';

export const useGroupOrThrow = (id: GroupID) => {
  const group = useGroup(id);

  if (!group) {
    throw new Error(`Group not found: ${id}`);
  }

  return group;
};
