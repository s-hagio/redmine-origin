import { IssueID } from '@/models/issue';

export type GroupID = string;
export type GroupValue = string | number | boolean | null;
export type GroupContent = string | number | boolean | null;

export type Group = {
  id: GroupID;
  value: GroupValue;
  content: GroupContent;
  issueIds: IssueID[];
};
