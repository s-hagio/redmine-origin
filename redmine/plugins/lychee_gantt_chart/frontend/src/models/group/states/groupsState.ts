import { selector } from 'recoil';
import { get as getValue } from '@/utils/object';
import { coreResourceCollectionState } from '@/models/coreResource';
import { Issue, issueState } from '@/models/issue';
import { getGroupValue, getGroupId } from '../helpers';
import { Group, GroupValue } from '../types';
import { groupFieldState } from './groupFieldState';

export const groupsState = selector<Group[] | null>({
  key: 'groups',
  get: ({ get }) => {
    const field = get(groupFieldState);

    if (!field) {
      return null;
    }

    const { issues } = get(coreResourceCollectionState);

    const groupedIssuesMap = new Map<GroupValue, Issue[]>();

    issues.forEach((issue) => {
      const groupIssue = get(issueState(issue.id)) ?? issue;

      const groupValue = getGroupValue(groupIssue, field);
      const groupedIssues = groupedIssuesMap.get(groupValue) || [];

      groupedIssues.push(groupIssue);
      groupedIssuesMap.set(groupValue, groupedIssues);
    });

    const groups: Group[] = [];

    groupedIssuesMap.forEach((issues, value) => {
      groups.push({
        id: getGroupId(field.name, value),
        value: value,
        content: field.contentPath ? getValue(issues[0], field.contentPath) : value,
        issueIds: issues.map(({ id }) => id),
      });
    });

    return groups;
  },
});
