import { selector, useRecoilValue, useSetRecoilState } from 'recoil';
import { act } from '@testing-library/react';
import { renderRecoilHook } from '@/test-utils';
import { issueQueryState } from '@/models/query';
import {
  intCustomFieldDefinition,
  issueSubjectFieldDefinition,
  stringCustomFieldDefinition,
  trackerFieldDefinition,
} from '@/__fixtures__';
import { groupFieldState } from '../groupFieldState';

const fieldDefinitionMapStateMock = vi.hoisted(() => vi.fn());

vi.mock('@/models/fieldDefinition', async (importOriginal) =>
  Object.defineProperties(await importOriginal(), {
    fieldDefinitionMapState: { get: fieldDefinitionMapStateMock },
  })
);

const fieldDefinitions = [trackerFieldDefinition, issueSubjectFieldDefinition, intCustomFieldDefinition];

fieldDefinitionMapStateMock.mockReturnValue(
  selector({
    key: 'fieldDefinitionMap/mock',
    get: () => new Map(fieldDefinitions.map((field) => [field.name, field])),
  })
);

const renderForTesting = () => {
  return renderRecoilHook(() => ({
    fieldGroup: useRecoilValue(groupFieldState),
    setIssueQuery: useSetRecoilState(issueQueryState),
  }));
};

test('クエリで指定されたフィールドの定義を返す', () => {
  const { result } = renderForTesting();

  act(() => {
    result.current.setIssueQuery((current) => ({ ...current, groupBy: trackerFieldDefinition.name }));
  });

  expect(result.current.fieldGroup).toBe(trackerFieldDefinition);
});

test('グループ化できないフィールドはnull', () => {
  const { result } = renderForTesting();

  act(() => {
    result.current.setIssueQuery((current) => ({ ...current, groupBy: stringCustomFieldDefinition.name }));
  });

  expect(result.current.fieldGroup).toBeNull();
});
