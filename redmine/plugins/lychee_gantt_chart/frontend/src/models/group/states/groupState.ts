import { selectorFamily } from 'recoil';
import { Group, GroupID } from '../types';
import { groupMapState } from './groupMapState';

export const groupState = selectorFamily<Group | null, GroupID>({
  key: 'group',
  get: (id) => {
    return ({ get }) => {
      const groupMap = get(groupMapState);
      const group = groupMap.get(id);

      return group ?? null;
    };
  },
});
