import { selector } from 'recoil';
import { Group, GroupID } from '../types';
import { groupsState } from './groupsState';

export const groupMapState = selector<ReadonlyMap<GroupID, Group>>({
  key: 'groupMap',
  get: ({ get }) => {
    const groups = get(groupsState);

    return new Map(groups?.map((group) => [group.id, group]));
  },
});
