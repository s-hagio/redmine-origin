import { selector } from 'recoil';
import { FieldDefinition, fieldDefinitionMapState } from '@/models/fieldDefinition';
import { issueQueryState } from '@/models/query';

export const groupFieldState = selector<FieldDefinition | null>({
  key: 'groupField',
  get: ({ get }) => {
    const { groupBy } = get(issueQueryState);
    const fieldDefinitionMap = get(fieldDefinitionMapState);

    const field = groupBy ? fieldDefinitionMap.get(groupBy) : null;

    return field?.groupable ? field : null;
  },
});
