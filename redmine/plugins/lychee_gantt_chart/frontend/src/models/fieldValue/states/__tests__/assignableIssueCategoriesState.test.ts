import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { associatedResourceCollectionState } from '@/models/associatedResource';
import {
  associatedResourceCollection,
  issueCategory1,
  issueCategory2,
  issueCategory3,
  rootProject,
  parentProject,
  mainProject,
} from '@/__fixtures__';
import { assignableIssueCategoriesState } from '../assignableIssueCategoriesState';

const mainProjectCategories = [
  { ...issueCategory1, projectId: mainProject.id },
  { ...issueCategory2, projectId: mainProject.id },
];

const otherProjectCategories = [
  { ...issueCategory3, projectId: rootProject.id },
  { ...issueCategory3, projectId: parentProject.id },
];

test('指定プロジェクトのカテゴリ配列を返す', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(assignableIssueCategoriesState(mainProject.id)), {
    initializeState: ({ set }) => {
      set(associatedResourceCollectionState, {
        ...associatedResourceCollection,
        issueCategories: [...mainProjectCategories, ...otherProjectCategories],
      });
    },
  });

  expect(result.current).toEqual(mainProjectCategories);
});
