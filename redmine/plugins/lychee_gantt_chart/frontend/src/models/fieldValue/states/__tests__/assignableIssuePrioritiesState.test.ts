import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { associatedResourceCollectionState } from '@/models/associatedResource';
import { associatedResourceCollection, activeIssuePriorities, inactiveIssuePriorities } from '@/__fixtures__';
import { assignableIssuePrioritiesState } from '../assignableIssuePrioritiesState';

test('指定プロジェクトのカテゴリ配列を返す', () => {
  const { result } = renderRecoilHook(() => useRecoilValue(assignableIssuePrioritiesState), {
    initializeState: ({ set }) => {
      set(associatedResourceCollectionState, {
        ...associatedResourceCollection,
        issuePriorities: [...activeIssuePriorities, ...inactiveIssuePriorities],
      });
    },
  });

  expect(result.current).toEqual(activeIssuePriorities);
});
