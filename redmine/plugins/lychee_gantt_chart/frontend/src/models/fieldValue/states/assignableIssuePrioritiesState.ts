import { selector } from 'recoil';
import { AssociatedIssuePriority, associatedResourceCollectionState } from '@/models/associatedResource';

export const assignableIssuePrioritiesState = selector<AssociatedIssuePriority[]>({
  key: 'assignableIssuePrioritiesState',
  get: ({ get }) => {
    const { issuePriorities } = get(associatedResourceCollectionState);

    return issuePriorities.filter((priority) => priority.isActive);
  },
});
