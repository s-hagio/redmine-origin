import { selectorFamily } from 'recoil';
import { createClient } from '@/api';
import { ProjectID } from '@/models/project';
import { FieldValueCollection } from '../types';

export const fieldValueCollectionState = selectorFamily<FieldValueCollection, ProjectID>({
  key: 'fieldRuleCollection',
  get: (projectId) => {
    return () => {
      return createClient().projects._id(projectId).field_values.$get();
    };
  },
});
