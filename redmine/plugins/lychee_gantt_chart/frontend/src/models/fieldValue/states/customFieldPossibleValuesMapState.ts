import { selectorFamily } from 'recoil';
import { CustomFieldID } from '@/models/customField';
import { ProjectID } from '@/models/project';
import { AssignableCustomField } from '../types';
import { fieldValueCollectionState } from './fieldValueCollectionState';

export const customFieldPossibleValuesMapState = selectorFamily<
  ReadonlyMap<CustomFieldID, AssignableCustomField>,
  ProjectID
>({
  key: 'customFieldPossibleValuesMap',
  get: (projectId) => {
    return ({ get }) => {
      const { customFieldPossibleValues } = get(fieldValueCollectionState(projectId));

      return new Map(
        customFieldPossibleValues.map((valueObject) => [valueObject.customFieldId, valueObject])
      );
    };
  },
});
