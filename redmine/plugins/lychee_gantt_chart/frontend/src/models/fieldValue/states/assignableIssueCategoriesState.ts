import { selectorFamily } from 'recoil';
import { associatedResourceCollectionState } from '@/models/associatedResource';
import { ProjectID } from '@/models/project';

export const assignableIssueCategoriesState = selectorFamily({
  key: 'assignableIssueCategories',
  get: (projectId: ProjectID) => {
    return ({ get }) => {
      const { issueCategories } = get(associatedResourceCollectionState);

      return issueCategories.filter((category) => category.projectId === projectId);
    };
  },
});
