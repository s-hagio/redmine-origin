import { CustomFieldID } from '@/models/customField';
import { AssociatedPrincipal, AssociatedTracker } from '@/models/associatedResource';

export type FieldValueCollection = {
  allowedTargetTrackers: AssignableTracker[];
  assignableUsers: AssignableUser[];
  customFieldPossibleValues: AssignableCustomField[];
};

export type AssignableTracker = Pick<AssociatedTracker, 'id' | 'name'>;

export type AssignableUser = Pick<AssociatedPrincipal, 'id' | 'name'> & {
  trackerIds: AssignableTracker['id'][];
};

export type AssignableCustomFieldValue = {
  label?: string;
  value: string | number | boolean | null;
};

export type AssignableCustomField = {
  customFieldId: CustomFieldID;
  trackerIds: AssignableTracker['id'][];
  values: AssignableCustomFieldValue[];
};
