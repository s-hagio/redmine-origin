import { useMemo } from 'react';
import { sortBy } from '@/utils/array';
import { isDescendantOf } from '@/lib/nestedSet';
import { useIsAllowedTo } from '@/models/currentUser';
import {
  AssociatedProject,
  AssociatedResourceType,
  useAssociatedResources,
} from '@/models/associatedResource';

type AssignableProject = Pick<AssociatedProject, 'id' | 'name'> & {
  depth: number;
};

export const useAssignableProjects = (): AssignableProject[] => {
  const isAllowedTo = useIsAllowedTo();
  const projects = useAssociatedResources(AssociatedResourceType.Project);

  return useMemo(() => {
    const assignableProjects = projects.filter((project) => {
      return project.trackerIds.length && isAllowedTo('add_issues', { projectId: project.id });
    });

    const ancestors: AssociatedProject[] = [];

    return sortBy(assignableProjects, 'lft').map((project) => {
      while (ancestors.length && !isDescendantOf(project, ancestors[ancestors.length - 1])) {
        ancestors.pop();
      }

      ancestors.push(project);

      return {
        id: project.id,
        name: project.name,
        depth: ancestors.length - 1,
      };
    });
  }, [isAllowedTo, projects]);
};
