import { ProjectID } from '@/models/project';
import {
  AssociatedResourceType,
  useAssociatedResource,
  useAssociatedResources,
} from '@/models/associatedResource';

export const useAssignableTrackers = (projectId: ProjectID) => {
  const trackers = useAssociatedResources(AssociatedResourceType.Tracker);
  const project = useAssociatedResource(AssociatedResourceType.Project, projectId);

  return trackers.filter((tracker) => project.trackerIds.includes(tracker.id));
};
