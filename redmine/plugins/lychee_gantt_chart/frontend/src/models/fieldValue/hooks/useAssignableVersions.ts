import { AssociatedResourceType, useAssociatedResources } from '@/models/associatedResource';
import { ProjectID, useProjectOrThrow } from '@/models/project';
import { isVersionSharedWith } from '@/models/version';

export const useAssignableVersions = (projectId: ProjectID) => {
  const project = useProjectOrThrow(projectId);
  const versions = useAssociatedResources(AssociatedResourceType.Version);

  return versions.filter((version) => {
    return !version.isLocked && !version.isClosed && isVersionSharedWith(version, project);
  });
};
