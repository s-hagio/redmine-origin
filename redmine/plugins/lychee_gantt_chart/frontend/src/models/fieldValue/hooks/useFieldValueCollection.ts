import { useRecoilValue } from 'recoil';
import { ProjectID } from '@/models/project';
import { fieldValueCollectionState } from '../states';

export const useFieldValueCollection = (projectId: ProjectID) => {
  return useRecoilValue(fieldValueCollectionState(projectId));
};
