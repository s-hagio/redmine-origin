import { useRecoilValue } from 'recoil';
import { assignableIssuePrioritiesState } from '../states';

export const useAssignableIssuePriorities = () => {
  return useRecoilValue(assignableIssuePrioritiesState);
};
