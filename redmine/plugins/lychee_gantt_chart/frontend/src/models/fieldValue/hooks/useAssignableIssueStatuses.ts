import { useMemo } from 'react';
import { AssociatedResourceType, useAssociatedResources } from '@/models/associatedResource';
import { useCombinedStatusTransitions } from '@/models/workflowRule';
import { useCurrentUser } from '@/models/currentUser';
import { Issue } from '@/models/issue';

type PartialIssue = Pick<Issue, 'id' | 'projectId' | 'trackerId' | 'statusId' | 'authorId' | 'assignedToId'>;

export const useAssignableIssueStatuses = (issue: PartialIssue) => {
  const combinedStatusTransitions = useCombinedStatusTransitions();
  const currentUser = useCurrentUser();
  const statuses = useAssociatedResources(AssociatedResourceType.IssueStatus);

  return useMemo(() => {
    const userRoleIds = currentUser.roleIdsByProjectId[issue.projectId];

    const assignableStatusIds = new Set(
      combinedStatusTransitions
        .filter((transition) => {
          if (transition.trackerId !== issue.trackerId || transition.oldStatusId !== issue.statusId) {
            return false;
          }

          return (
            currentUser.isAdmin ||
            userRoleIds.includes(transition.roleId) ||
            (transition.isForAuthor && issue.authorId === currentUser.id) ||
            (transition.isForAssignee && issue.assignedToId === currentUser.id)
          );
        })
        .flatMap(({ newStatusIds }) => newStatusIds)
    );

    assignableStatusIds.add(issue.statusId);

    return statuses.filter(({ id }) => assignableStatusIds.has(id));
  }, [
    combinedStatusTransitions,
    currentUser.id,
    currentUser.isAdmin,
    currentUser.roleIdsByProjectId,
    statuses,
    issue.projectId,
    issue.trackerId,
    issue.statusId,
    issue.authorId,
    issue.assignedToId,
  ]);
};
