import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { destructiveSortBy } from '@/utils/array';
import { Issue } from '@/models/issue';
import {
  AssociatedResourceType,
  AssociatedPrincipal,
  AssociatedTracker,
  useAssociatedResources,
} from '@/models/associatedResource';
import { useCurrentUser } from '@/models/currentUser';
import { AssignableUser } from '../types';
import { useAssignableProjectUsers } from './useAssignableProjectUsers';

type PartialIssue = Pick<Issue, 'projectId' | 'trackerId' | 'authorId' | 'assignedToId'>;

export const useAssignableIssueUsers = (issue: PartialIssue) => {
  const { t } = useTranslation();

  const currentUser = useCurrentUser();
  const activeTrackers = useAssociatedResources(AssociatedResourceType.Tracker);
  const activeUsers = useAssociatedResources(AssociatedResourceType.Principal);
  const projectUsers = useAssignableProjectUsers(issue.projectId, issue.trackerId);

  return useMemo(() => {
    const assignableUsers = [...projectUsers];
    const allTrackerIds = activeTrackers.map(({ id }) => id);

    if (!assignableUsers.some(({ id }) => issue.authorId === id)) {
      pushUserIfExists(assignableUsers, activeUsers, issue.authorId, allTrackerIds);
    }

    if (issue.assignedToId && !assignableUsers.some(({ id }) => issue.assignedToId === id)) {
      pushUserIfExists(assignableUsers, activeUsers, issue.assignedToId, allTrackerIds);
    }

    destructiveSortBy(assignableUsers, 'position');

    if (assignableUsers.some(({ id }) => id === currentUser.id)) {
      assignableUsers.unshift({
        id: currentUser.id,
        name: `<< ${t('label.me')} >>`,
        trackerIds: allTrackerIds,
      });
    }

    return assignableUsers;
  }, [projectUsers, activeTrackers, issue.assignedToId, issue.authorId, activeUsers, currentUser.id, t]);
};

const pushUserIfExists = (
  arr: AssignableUser[],
  users: AssociatedPrincipal[],
  userId: AssociatedPrincipal['id'],
  trackerIds: AssociatedTracker['id'][]
) => {
  const user = users.find(({ id }) => userId === id);

  if (user) {
    arr.push({ ...user, trackerIds });
  }
};
