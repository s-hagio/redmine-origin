import { useRecoilValue } from 'recoil';
import { ProjectID } from '@/models/project';
import { assignableIssueCategoriesState } from '../states';

export const useAssignableIssueCategories = (projectId: ProjectID) => {
  return useRecoilValue(assignableIssueCategoriesState(projectId));
};
