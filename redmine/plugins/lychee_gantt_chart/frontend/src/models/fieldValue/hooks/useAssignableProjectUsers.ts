import { AssociatedTracker } from '@/models/associatedResource';
import { ProjectID } from '@/models/project';
import { useFieldValueCollection } from './useFieldValueCollection';

export const useAssignableProjectUsers = (projectId: ProjectID, trackerId: AssociatedTracker['id']) => {
  const { assignableUsers } = useFieldValueCollection(projectId);

  return assignableUsers.filter((user) => user.trackerIds.includes(trackerId));
};
