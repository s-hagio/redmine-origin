import { useRecoilValue } from 'recoil';
import { ProjectID } from '@/models/project';
import { CustomFieldID } from '@/models/customField';
import { AssociatedTracker } from '@/models/associatedResource';
import { customFieldPossibleValuesMapState } from '../states';
import { AssignableCustomFieldValue } from '../types';

export const useAssignableCustomFieldValues = (
  projectId: ProjectID,
  customFieldId: CustomFieldID,
  trackerId: AssociatedTracker['id']
): AssignableCustomFieldValue[] => {
  const customFieldPossibleValuesMap = useRecoilValue(customFieldPossibleValuesMapState(projectId));
  const valueObject = customFieldPossibleValuesMap.get(customFieldId);

  if (valueObject?.trackerIds.includes(trackerId)) {
    return valueObject.values;
  }

  return [];
};
