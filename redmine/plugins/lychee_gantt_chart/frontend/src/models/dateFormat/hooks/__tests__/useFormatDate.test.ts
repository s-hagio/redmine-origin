import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { useFormatDate } from '../useFormatDate';

const render = () => {
  return renderRecoilHook(() => useFormatDate(), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({ ...current, dateFormat: 'yyyy-MM-dd' }));
    },
  });
};

const currentYear = new Date().getFullYear();

test('日付を表示形式でフォーマット', () => {
  const { result } = render();

  expect(result.current(new Date(currentYear - 1, 1, 23))).toBe(`${currentYear - 1}-02-23`);
});

test('当年の場合は年を省略', () => {
  const { result } = render();

  expect(result.current(new Date(currentYear, 1, 23))).toBe(`02-23`);
});

test('omitCurrentYearオプションがfalseの場合は年を省略しない', () => {
  const { result } = render();

  expect(result.current(new Date(currentYear, 1, 23), { omitCurrentYear: false })).toBe(
    `${currentYear}-02-23`
  );
});
