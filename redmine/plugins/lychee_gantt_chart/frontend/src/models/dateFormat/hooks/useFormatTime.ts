import { useCallback } from 'react';
import { DateArgument, format, toDate } from '@/lib/date';
import { useTimeFormat } from './useTimeFormat';

type FormatTime = (arg: DateArgument) => string;

export const useFormatTime = (): FormatTime => {
  const timeFormat = useTimeFormat();

  return useCallback(
    (arg) => {
      const date = toDate(arg, { preserveTime: true });

      return format(date, timeFormat);
    },
    [timeFormat]
  );
};
