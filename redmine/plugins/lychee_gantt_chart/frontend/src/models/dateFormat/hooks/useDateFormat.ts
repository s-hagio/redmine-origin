import { useRecoilValue } from 'recoil';
import { dateFormatState } from '../states';

export const useDateFormat = () => useRecoilValue(dateFormatState);
