import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { useFormatTime } from '../useFormatTime';

test('時間を表示形式でフォーマット', () => {
  const { result } = renderRecoilHook(() => useFormatTime(), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({ ...current, timeFormat: '%I:%M %p' }));
    },
  });

  expect(result.current(new Date(2023, 12, 25, 12, 34, 59))).toBe('12:34 PM');
  expect(result.current('2023-01-02T00:12:34.555')).toBe('12:12 AM');
});

test('24時間表記', () => {
  const { result } = renderRecoilHook(() => useFormatTime(), {
    initializeState: ({ set }) => {
      set(settingsState, (current) => ({ ...current, timeFormat: '%H:%M' }));
    },
  });

  expect(result.current(new Date(2023, 12, 25, 15, 28, 59))).toBe('15:28');
  expect(result.current('2023-01-02T00:22:34.555')).toBe('00:22');
});
