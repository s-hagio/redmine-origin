import { useRecoilValue } from 'recoil';
import { timeFormatState } from '../states';

export const useTimeFormat = () => useRecoilValue(timeFormatState);
