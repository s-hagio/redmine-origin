import { useCallback } from 'react';
import { useRecoilValue } from 'recoil';
import { DateArgument, toDate, format } from '@/lib/date';
import { dateFormatState, yearOmittedDateFormatState } from '../states';

type Options = {
  omitCurrentYear?: boolean;
};
type FormatDate = (arg: DateArgument, options?: Options) => string;

const defaultOptions: Options = {
  omitCurrentYear: true,
};

export const useFormatDate = (): FormatDate => {
  const dateFormat = useRecoilValue(dateFormatState);
  const yearOmittedDateFormat = useRecoilValue(yearOmittedDateFormatState);

  return useCallback(
    (arg, options) => {
      const opts = { ...defaultOptions, ...options };
      const date = toDate(arg);

      if (opts.omitCurrentYear && isCurrentYear(date)) {
        return format(date, yearOmittedDateFormat);
      }

      return format(date, dateFormat);
    },
    [dateFormat, yearOmittedDateFormat]
  );
};

const isCurrentYear = (date: Date) => {
  return date.getFullYear() === new Date().getFullYear();
};
