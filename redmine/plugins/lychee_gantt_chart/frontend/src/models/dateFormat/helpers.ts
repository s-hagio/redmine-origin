export const removeYearDateFormatString = (dateFormat: string) => {
  return dateFormat.replace(/[^a-zA-Z]*yyyy[^a-zA-Z]*/, '');
};
