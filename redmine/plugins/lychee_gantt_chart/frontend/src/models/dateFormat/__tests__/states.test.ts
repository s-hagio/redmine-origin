import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { settingsState } from '@/models/setting';
import { removeYearDateFormatString } from '../helpers';
import { dateFormatState, yearOmittedDateFormatState } from '../states';

vi.mock('../helpers', async () => ({
  ...(await vi.importActual<object>('../helpers')),
  removeYearDateFormatString: vi.fn(),
}));

describe('dateFormatState', () => {
  test('Rubyのフォーマット文字列をそれぞれ変換', () => {
    const { result } = renderRecoilHook(() => useRecoilValue(dateFormatState), {
      initializeState: ({ set }) => {
        set(settingsState, (current) => ({ ...current, dateFormat: '%Y-%m-%d-%b-%B' }));
      },
    });

    expect(result.current).toBe('yyyy-MM-dd-MMM-MMMM');
  });
});

describe('yearOmittedDateFormatState', () => {
  test('dateFormatStateをremoveYearDateFormatString()に渡して戻り値を返す', () => {
    const dateFormat = 'abc';
    const returnValue = '123';

    vi.mocked(removeYearDateFormatString).mockReturnValue(returnValue);

    const { result } = renderRecoilHook(() => useRecoilValue(yearOmittedDateFormatState), {
      initializeState: ({ set }) => {
        set(settingsState, (current) => ({ ...current, dateFormat: dateFormat }));
      },
    });

    expect(removeYearDateFormatString).toBeCalledWith(dateFormat);
    expect(result.current).toBe(returnValue);
  });
});
