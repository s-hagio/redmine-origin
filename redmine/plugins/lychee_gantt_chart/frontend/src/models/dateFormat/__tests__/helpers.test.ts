import { removeYearDateFormatString } from '../helpers';

describe('removeYearDateFormatString', () => {
  test('年フォーマットの文字列前後の英字以外の文字列を削除する', () => {
    expect(removeYearDateFormatString('yyyy-MM-dd')).toBe('MM-dd');
    expect(removeYearDateFormatString('dd.MM.yyyy')).toBe('dd.MM');
    expect(removeYearDateFormatString('dd-MM-yyyy')).toBe('dd-MM');
    expect(removeYearDateFormatString('MM/dd/yyyy')).toBe('MM/dd');
    expect(removeYearDateFormatString('MMMM dd, yyyy')).toBe('MMMM dd');
  });
});
