import { selector } from 'recoil';
import { removeYearDateFormatString } from '../helpers';
import { dateFormatState } from './dateFormatState';

export const yearOmittedDateFormatState = selector<string>({
  key: 'yearOmittedDateFormat',
  get: ({ get }) => {
    const dateFormat = get(dateFormatState);

    return removeYearDateFormatString(dateFormat);
  },
});
