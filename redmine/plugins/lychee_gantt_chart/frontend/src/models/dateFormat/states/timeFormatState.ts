import { selector } from 'recoil';
import { settingsState } from '@/models/setting';

const convertableFormats = new Map([
  ['%H', 'HH'],
  ['%I', 'hh'],
  ['%M', 'mm'],
  ['%p', 'aa'],
]);

export const timeFormatState = selector({
  key: 'timeFormat',
  get: ({ get }) => {
    const { timeFormat } = get(settingsState);

    return timeFormat.replace(/%[a-zA-Z]/g, (str) => convertableFormats.get(str) ?? str);
  },
});
