import { selector } from 'recoil';
import { settingsState } from '@/models/setting';

const convertableFormats = new Map([
  ['%Y', 'yyyy'],
  ['%m', 'MM'],
  ['%d', 'dd'],
  ['%b', 'MMM'],
  ['%B', 'MMMM'],
]);

export const dateFormatState = selector<string>({
  key: 'dateFormat',
  get: ({ get }) => {
    const { dateFormat } = get(settingsState);

    return dateFormat.replace(/%[a-zA-Z]/g, (str) => convertableFormats.get(str) ?? str);
  },
});
