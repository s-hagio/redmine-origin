import { CustomFieldID, CustomFieldName, CustomFieldFormat, CustomField } from '@/models/customField';
import { ColumnName } from '@/models/query';

export type CoreFieldName =
  | 'projectId'
  | 'trackerId'
  | 'statusId'
  | 'subject'
  | 'priorityId'
  | 'isPrivate'
  | 'assignedToId'
  | 'categoryId'
  | 'fixedVersionId'
  | 'parentId'
  | 'startDate'
  | 'dueDate'
  | 'estimatedHours'
  | 'spentHours'
  | 'doneRatio'
  | 'description'
  | 'remainingEstimate';

export type FieldName = CoreFieldName | CustomFieldName;

export const FieldFormat = {
  Html: 'html',
  PlainText: 'plainText',
  FormattableText: 'formattableText',
  String: 'string',
  Text: 'text',
  Int: 'int',
  Float: 'float',
  FixedFloat: 'fixedFloat',
  Percent: 'percent',
  Date: 'date',
  DateTime: 'dateTime',
  Bool: 'bool',
  Relations: 'relations',
  Attachments: 'attachments',
  Id: 'id',
  ParentIssueId: 'parentIssueId',
  Project: 'project',
  Version: 'version',
  Tracker: 'tracker',
  Principal: 'principal',
  AssignedTo: 'assignedTo',
  IssueStatus: 'issueStatus',
  IssueStatusWithColor: 'issueStatusWithColor',
  IssuePriority: 'issuePriority',
  IssueCategory: 'issueCategory',
  DoneRatio: 'doneRatio',
  Color: 'color',
} as const;

export type FieldFormat = (typeof FieldFormat)[keyof typeof FieldFormat];

type SortableDefinition =
  | boolean
  | {
      defaultDesc?: boolean;
    };

type BaseFieldDefinition = {
  name: ColumnName;
  fieldName?: FieldName;
  label: string;
  valuePath: string;
  contentPath?: string;
  sortable?: SortableDefinition;
  groupable?: boolean;
  readonly?: boolean;
  required?: boolean;
};

export type CoreFieldDefinition = BaseFieldDefinition & {
  format: FieldFormat;
};

export type CustomFieldDefinition = BaseFieldDefinition &
  Pick<
    CustomField,
    'minLength' | 'maxLength' | 'isFullTextFormatting' | 'isMultiple' | 'isBulkEditSupported' | 'sortable'
  > & {
    customFieldId: CustomFieldID;
    format: CustomFieldFormat;
  };

export type FieldDefinition = CoreFieldDefinition | CustomFieldDefinition;
