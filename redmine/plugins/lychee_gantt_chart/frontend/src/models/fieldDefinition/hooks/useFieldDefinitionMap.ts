import { useRecoilValue } from 'recoil';
import { fieldDefinitionMapState } from '../states';

export const useFieldDefinitionMap = () => useRecoilValue(fieldDefinitionMapState);
