import * as React from 'react';
import { useRecoilValue } from 'recoil';
import { renderRecoilHook } from '@/test-utils';
import { customFieldsState } from '@/models/customField';
import { TranslationProvider } from '@/providers/translationProvider';
import { customFields, formattableStringCustomField, formattableTextCustomField } from '@/__fixtures__';
import { fieldDefinitionMapState } from '../fieldDefinitionMapState';

test('フィールド定義のMapを返す', () => {
  const { result } = renderRecoilHook(
    () => useRecoilValue(fieldDefinitionMapState),
    {
      initializeState: ({ set }) => {
        set(customFieldsState, [
          ...customFields.values(),
          formattableStringCustomField,
          formattableTextCustomField,
        ]);
      },
    },
    {
      wrapper: ({ children }) => <TranslationProvider lang="ja">{children}</TranslationProvider>,
    }
  );

  expect(result.current).toMatchSnapshot();
});
