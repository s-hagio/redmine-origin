import { selector } from 'recoil';
import { t } from 'i18next';
import { pick } from '@/utils/object';
import { customFieldsState, isRawValueCustomField } from '@/models/customField';
import { coreFieldDefinitionTemplates } from '../templates';
import { CoreFieldDefinition, FieldDefinition } from '../types';

export const fieldDefinitionMapState = selector<ReadonlyMap<string, FieldDefinition>>({
  key: 'fieldDefinitionMap',
  get: ({ get }) => {
    const customFields = get(customFieldsState);

    const fieldDefinitions: FieldDefinition[] = coreFieldDefinitionTemplates.map<CoreFieldDefinition>(
      ({ labelPath, label, ...template }) => ({
        ...template,
        label: labelPath != null ? t(labelPath) : label,
      })
    );

    customFields.forEach((customField) => {
      const field: FieldDefinition = {
        ...pick(customField, [
          'name',
          'label',
          'format',
          'sortable',
          'groupable',
          'minLength',
          'maxLength',
          'isFullTextFormatting',
          'isMultiple',
          'isBulkEditSupported',
        ]),
        fieldName: customField.name,
        customFieldId: customField.id,
        format: customField.format,
        valuePath: `customFieldValues.${customField.id}`,
        contentPath: `formattedCustomFieldValues.${customField.id}`,
      };

      if (isRawValueCustomField(customField)) {
        delete field.contentPath;
      }

      fieldDefinitions.push(field);
    });

    return new Map(
      fieldDefinitions.flatMap((field) => {
        const pairs: [string, FieldDefinition][] = [[field.name, field]];

        if (field.fieldName) {
          pairs.push([field.fieldName, field]);
        }

        return pairs;
      })
    );
  },
});
