import {
  descriptionFieldDefinition,
  idFieldDefinition,
  parentSubjectFieldDefinition,
  stringCustomFieldDefinition,
} from '@/__fixtures__';
import { isCustomFieldDefinition } from '../isCustomFieldDefinition';

test('CustomFieldDefinitionならtrue', () => {
  expect(isCustomFieldDefinition(stringCustomFieldDefinition)).toBe(true);
});

test('それ以外ならfalse', () => {
  expect(isCustomFieldDefinition(idFieldDefinition)).toBe(false);
  expect(isCustomFieldDefinition(parentSubjectFieldDefinition)).toBe(false);
  expect(isCustomFieldDefinition(descriptionFieldDefinition)).toBe(false);
});
