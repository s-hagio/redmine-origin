import { CustomFieldDefinition, FieldDefinition } from '../types';

export const isCustomFieldDefinition = (
  fieldDefinition: FieldDefinition
): fieldDefinition is CustomFieldDefinition => {
  return (fieldDefinition as CustomFieldDefinition).customFieldId != null;
};
