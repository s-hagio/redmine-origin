import * as field from './field.json';
import * as label from './label.json';
import * as date from './date.json';
import * as timeline from './timeline.json';
import * as message from './message.json';
import * as error from './error.json';

export const translation = { field, label, date, timeline, message, error };
