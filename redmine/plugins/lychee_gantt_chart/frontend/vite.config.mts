/// <reference types="vitest" />

import { defineConfig, splitVendorChunkPlugin } from 'vite';
import tsconfigPaths from 'vite-tsconfig-paths';
import react from '@vitejs/plugin-react';
import linaria from '@linaria/vite';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    tsconfigPaths(),
    splitVendorChunkPlugin(),
    react(),
    linaria({
      sourceMap: process.env.NODE_ENV !== 'production',
      classNameSlug: (hash, _title, { name }) => `lgc-${name}-${hash}`,
      babelOptions: {
        presets: ['@babel/preset-typescript', '@babel/preset-react'],
        plugins: [['module-resolver', { alias: { '@': './src' } }]],
      },
    }),
  ],

  build: {
    outDir: '../assets/bundle',
    assetsDir: '',
    emptyOutDir: true,
    manifest: true,
    rollupOptions: {
      input: ['src/main.tsx'],
    },
  },

  server: {
    host: '0.0.0.0',
    port: 3001,
  },

  test: {
    globals: true,
    environment: 'happy-dom',
    watch: false,
    clearMocks: true,
    setupFiles: ['./vitest.setup.js'],
    env: {
      DEV: '',
    },
  },
});
