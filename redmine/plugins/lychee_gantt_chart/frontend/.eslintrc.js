module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:aspida/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'prettier',
  ],
  plugins: ['unused-imports', 'lychee-gantt'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'no-unused-vars': 'off',
    'lychee-gantt/import-path': 'error',
    '@typescript-eslint/no-unused-vars': ['error'],
    'react/prop-types': 'off',
    'react/jsx-curly-brace-presence': 'error',
    'react/self-closing-comp': [
      'error',
      {
        component: true,
        html: true,
      },
    ],
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': [
      'warn',
      {
        additionalHooks: '(useRecoilCallback|useRecoilTransaction_UNSTABLE)',
      },
    ],
    'import/order': [
      'error',
      {
        groups: ['builtin', 'external', 'internal', 'index', 'parent', 'sibling', 'type', 'unknown'],
        pathGroups: [
          { pattern: 'react', group: 'builtin', position: 'before' },
          { pattern: 'recoil', group: 'builtin', position: 'before' },
          { pattern: '@/utils/**', group: 'internal', position: 'before' },
          { pattern: '@/test-utils', group: 'internal', position: 'before' },
          { pattern: '@/lib/**', group: 'internal', position: 'before' },
          { pattern: '@/providers', group: 'internal', position: 'before' },
          { pattern: '@/api', group: 'internal', position: 'before' },
          { pattern: '@/models', group: 'internal', position: 'before' },
          { pattern: '@/models/**/*', group: 'internal', position: 'before' },
          { pattern: '@/features/**', group: 'internal', position: 'before' },
          { pattern: '@/**', group: 'internal', position: 'before' },
          { pattern: '@/**/*', group: 'internal' },
        ],
        pathGroupsExcludedImportTypes: ['react'],
      },
    ],
    'no-restricted-imports': [
      'error',
      {
        patterns: ['@/providers/*/*', '@/features/*/*', '@/models/*/*'],
      },
    ],
    'import/no-restricted-paths': [
      'error',
      {
        zones: [
          {
            target: '**/*',
            from: '**/*/models/**/states.ts',
            except: ['**/*/models/**/*'],
            message: 'Recoil states are only available in the Model layer.',
          },
        ],
      },
    ],
    'unused-imports/no-unused-imports': 'error',
  },
  settings: {
    react: {
      version: 'detect',
    },
    'import/resolver': {
      typescript: {},
    },
  },
};
