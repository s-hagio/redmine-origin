const path = require('path');

const isRestrictedFilePath = (path) => {
  return path.startsWith('models') || path.startsWith('features');
};

const getGroupRootName = (relativePath) => {
  return relativePath.split(path.sep).slice(0, 2).join(path.sep);
};

module.exports = {
  meta: {
    fixable: 'code',
  },

  create(context) {
    const absoluteRootPath = path.resolve(context.getCwd(), 'src');
    const absoluteSourcePath = context.getFilename();
    const rootRelativeSourcePath = path.relative(absoluteRootPath, context.getFilename());
    const sourceFileGroupRootName = getGroupRootName(rootRelativeSourcePath);

    if (!isRestrictedFilePath(rootRelativeSourcePath)) {
      return {};
    }

    return {
      ImportDeclaration(node) {
        const importPath = node.source.value;

        const isRootRelativePath = importPath.startsWith('@/');
        const isRelativePath = importPath.startsWith('.');

        // External packages
        if (!isRootRelativePath && !isRelativePath) {
          return;
        }

        const absoluteImportPath = isRootRelativePath
          ? importPath.replace(/^@/, absoluteRootPath)
          : path.resolve(absoluteSourcePath, `../${importPath}`);

        const rootRelativePath = path.relative(absoluteRootPath, absoluteImportPath);

        if (!isRestrictedFilePath(rootRelativePath)) {
          return;
        }

        const groupRootName = getGroupRootName(rootRelativePath);
        const isSameGroup = groupRootName === sourceFileGroupRootName;

        if (isRelativePath && !isSameGroup) {
          context.report({
            node,
            message: 'Relative import should be used only for files in the same group.',
            fix(fixer) {
              return fixer.replaceText(node.source, `'@/${groupRootName}'`);
            },
          });
        } else if (isRootRelativePath && isSameGroup) {
          context.report({
            node,
            message: 'Absolute import should be used only for files in different group.',
          });
        }
      },
    };
  },
};
