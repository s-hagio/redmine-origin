# frozen_string_literal: true

get 'lychee_gantt', to: 'lychee_gantt_chart#show'
# get 'lgc', to: redirect(path: '/lychee_gantt')

resources :projects do
  get 'lychee_gantt', to: 'lychee_gantt_chart#show'
  # get 'lgc', to: redirect(path: '/projects/%{project_id}/lychee_gantt')
end

defaults format: :json do
  namespace :lychee_gantt do
    namespace :api do
      resources :associated_resources, only: %i[index]
      resources :core_resources, only: %i[index]
      resources :custom_fields, only: %i[index]
      resources :workflow_rules, only: %i[index]
      resources :projects, only: %i[index] do
        resources :field_values, only: %i[index]
      end
      resources :versions, only: %i[index]
      resources :issues, only: %i[index create show update] do
        collection do
          put 'bulk_update', to: 'batch_issues#update'
        end
      end
      resources :issue_relations, only: %i[index create destroy]
      resources :baselines, only: %i[index create show update destroy]

      resources :queries, only: %i[] do
        get 'filter', on: :collection
      end
    end
  end
end
