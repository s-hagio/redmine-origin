# frozen_string_literal: true

namespace :lychee_gantt do
  desc 'Migrate from LGC v4'
  task migrate_from_v4: %w[
    lychee_gantt:migrate_from_v4:enable_project_modules
    lychee_gantt:migrate_from_v4:enable_default_project_module
    lychee_gantt:migrate_from_v4:allow_role_permissions
  ]

  namespace :migrate_from_v4 do
    v4_module_name = 'lgc'
    v5_module_name = 'lychee_gantt'

    basic_permission_map = {
      view_lgc: :view_lychee_gantt,
      manage_lgc: :manage_lychee_gantt,
    }.freeze

    baseline_permission_map = {
      view_lgc_planning_line: :view_lychee_gantt_baselines,
      manage_lgc_plans: :manage_lychee_gantt_baselines,
    }.freeze

    milestone_permission_map = {
      view_lgc_milestones: :view_lychee_gantt_milestones,
      manage_lgc_milestones: :manage_lychee_gantt_milestones,
    }.freeze

    desc 'Enable project modules'
    task enable_project_modules: :environment do
      EnabledModule.transaction do
        EnabledModule.where(name: v4_module_name).where.not(
          project_id: EnabledModule.where(name: v5_module_name).select(:project_id)
        ).find_each do |enabled_module|
          EnabledModule.create!(name: v5_module_name, project_id: enabled_module.project_id)
        end
      end
    end

    desc 'Enable default project module'
    task enable_default_project_module: :environment do
      if Setting.default_projects_modules.include?(v4_module_name)
        Setting.default_projects_modules = (Setting.default_projects_modules + [v5_module_name]).uniq
      end
    end

    def allow_permissions(permission_map)
      Role.find_each do |role|
        new_permissions = permission_map.select { |v4_perm| role.permissions.include?(v4_perm) }.values
        role.update!(permissions: (role.permissions + new_permissions).uniq)
      end
    end

    desc 'Allow role permissions'
    task allow_role_permissions: :environment do
      allow_permissions(
        basic_permission_map.merge(baseline_permission_map).merge(milestone_permission_map)
      )
    end

    desc 'Allow basic permissions'
    task allow_basic_permissions: :environment do
      allow_permissions(basic_permission_map)
    end

    desc 'Allow baseline permissions'
    task allow_baseline_permissions: :environment do
      allow_permissions(baseline_permission_map)
    end

    desc 'Allow milestone permissions'
    task allow_milestone_permissions: :environment do
      allow_permissions(milestone_permission_map)
    end

    desc 'Rollback migrated configurations from LGC v4'
    task rollback: %w[
      lychee_gantt:migrate_from_v4:rollback:disable_project_modules
      lychee_gantt:migrate_from_v4:rollback:disable_default_project_module
      lychee_gantt:migrate_from_v4:rollback:disallow_role_permissions
    ]

    namespace :rollback do
      desc 'Disable project modules'
      task disable_project_modules: :environment do
        EnabledModule.where(name: v5_module_name).delete_all
      end

      desc 'Disable default project module'
      task disable_default_project_module: :environment do
        Setting.default_projects_modules -= [v5_module_name]
      end

      def disallow_permissions(permission_map)
        Role.find_each { |role| role.update!(permissions: role.permissions - permission_map.values) }
      end

      desc 'Disallow role permissions'
      task disallow_role_permissions: :environment do
        disallow_permissions(
          basic_permission_map.merge(baseline_permission_map).merge(milestone_permission_map)
        )
      end

      desc 'Disallow basic permissions'
      task disallow_basic_permissions: :environment do
        disallow_permissions(basic_permission_map)
      end

      desc 'Disallow baseline permissions'
      task disallow_baseline_permissions: :environment do
        disallow_permissions(baseline_permission_map)
      end

      desc 'Disallow milestone permissions'
      task disallow_milestone_permissions: :environment do
        disallow_permissions(milestone_permission_map)
      end
    end
  end
end
