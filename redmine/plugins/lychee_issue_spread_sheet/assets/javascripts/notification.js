function issueSpreadSheetNotification(text){
  return new Noty({
    layout: 'topCenter',
    text: text,
    animation: {
      open: 'animated slideInDown',
      close: 'animated slideOutUp',
    },
    timeout: 5000,
    progressBar: false,
    callbacks: {
      onTemplate: function(){
        $(this.barDom).html(
          $('<div/>', { id: 'flash_notice', class: 'flash notice' }).html(this.options.text)
        );
      },
      onShow: function(){
        $('#noty_layout__topCenter').each(function(){
          $(this).css({
            'top': '0',
            'width': '99%',
            'max-width': '100%',
            'left': '5px',
            '-webkit-transform': 'none',
            'transform': 'none'
          });
        });
      }
    }
  });
}
