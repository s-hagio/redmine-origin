$(function(){
  var id, $active_element = null;
  var originValueClassName = 'originValue';
  var pr_links;

  $('.issues td').on('dblclick', function(){
    deleteEditTags($('.issues td:has(.' + originValueClassName + ')'));

    var $el = $(this);
    var fieldClasses = $el.attr('class').split(/\s+/);
    if(isDescription(fieldClasses)){
      var tr = $el.closest('tr');
      tr = tr.prev('.issue').length >= 1 ? tr.prev('.issue') : tr.prevUntil('.issue').prev('.issue');
      id = tr.children('td.id').text();
    }else{
      id = $el.closest('tr').children("td.id").text();
    }
    if(id){ allowedEditIssue($el); }
  });


  $(document).on('keydown', '#editable_issue_value', function(e) {
    // textbox の enter で form の submit を抑制する
    if (
      ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) &&
      $(this).is('input[type=text]') // textareaの場合は改行可能にしたい
    ){
      e.preventDefault();
      $(this).blur();
    }
  });

  function isDescription(fieldClasses){
    return fieldClasses.indexOf('description') >= 0 || fieldClasses.indexOf('block_column') >= 0
  }

  function allowedEditIssue($el){
    var fieldClass = $el.attr('class').split(/\s+/)[0];
    var cf_id = fieldClass.match(/^cf/) ? fieldClass.substring(3) : null;
    $.get(
      $('[data-allowed-edit-issue-spread-sheet-url]').data('allowed-edit-issue-spread-sheet-url'),
      { issue_id: id, field_class: fieldClass, cf_id: cf_id }
    ).done(function(obj) {
      if(obj.status == 'OK'){ switchBoxtype($el); }
    });
  }

  function switchBoxtype($element){
    var fieldClass = $element.attr('class');
    var textType = ['subject', 'estimated_hours'];
    var ignoredFields = [
        'checkbox hide-when-print', 'id', 'updated_on', 'spent_hours',
        'author', 'created_on', 'closed_on', 'relations',
        'total_estimated_hours', 'total_spent_hours', 'attachments',
        'last_updated_by'
        ];
    var $textBox = $('<input type="text" id="editable_issue_value"/>').val($element.text());
    if(fieldClass === 'done_ratio' && $('[data-setting-done-ratio]').data('setting-done-ratio') === 'issue_status' ) {
      return;
    };
    if($.inArray(fieldClass, ignoredFields) >= 0 || fieldClass.match(/^cf_\d+ attachment$/)){
      return;
    }else if($.inArray(fieldClass, textType) >= 0){
      if(fieldClass == 'subject'){
        pr_links = $element.contents().clone();
        pr_links.splice(0, 1);
      }
      createEditTag($element, $textBox);
      focusBlur($element, fieldClass);
    }else if(fieldClass.match(/^cf/)){
      switchCf($element, fieldClass)
    }else if(fieldClass.match(/date$/)){
      dateType($element, $textBox, fieldClass)
    }else if(fieldClass == 'parent'){
      var parentText = $('a',$element).text();
      var parentVal = parentText.substring(parentText.indexOf("#") + 1, parentText.length);
      createEditTag(
        $element,
        $('<input type="text" id="editable_issue_value"/>').val(parentVal)
      );
      focusBlur($element, fieldClass);
    }else if(fieldClass.indexOf('description') >= 0){
      textArea($element, fieldClass)
    }else{
      selectBox($element, fieldClass);
    }
  }

  function switchCf($element, fieldClass){
    var cf_id = fieldClass.substring(3, fieldClass.indexOf(" "));
    $.get(
      $('[data-issue-spread-sheet-url]').data('issue-spread-sheet-url'),
      { issue_id: id, field_class: fieldClass, cf_id: cf_id }
    ).done(function(obj) {
      createEditTag($element, obj.custom_field_tag);
      focusBlurWithCF($element, fieldClass, cf_id, obj.custom_field_tag_name)
    });
  }

  // ラジオボックス以外をクリックした時
  $(document).click(function(event){
    if($active_element !== null){
      if(!$(event.target).closest('.check_box_group').length){
        var fieldClass = $active_element.attr('class');
        if($active_element.find("input[type='checkbox']").length){
          var value = $active_element.find('input:checked').map(function() {
            return $(this).val();
          }).get();
        }else{
          var value = $active_element.find('input:checked').val();
        }
        var cf_id = fieldClass.substring(3, fieldClass.indexOf(" "));
        update($active_element, fieldClass, value, '', cf_id);

        $active_element = null;
        if($active_element.find("input[type='checkbox']").length){
          $(document).on('click', contextMenuClick);
        }
      }
    }
  });

  function deleteEditTags($elements){
    $elements.each(function(){
      deleteEditTag($(this));
    });
  }

  function deleteEditTag($element){
    $element.html($element.find('input.' + originValueClassName).val());
  }

  function createEditTag($element, editTag){
    var originValue = $element.html();
    $element.html(editTag)
            .append($('<input>',
                      {
                        type: 'hidden',
                        value: originValue,
                        class: originValueClassName
                      })
                    );
  }

  function selectBox($element, fieldClass){
    getFromDb(fieldClass, function(obj){
      var selectBox = $('<select/>').attr('id','editable_issue');
      var blankArray = ['assigned_to', 'category', 'fixed_version']
      if($.inArray(fieldClass, blankArray) >= 0){
        selectBox.append($('<option>'));
      }
      createSelectBox($element, selectBox, obj);
      focusBlur($element, fieldClass);
    });
  }

  function createSelectBox($element, selectBox, obj){
    $.each(obj.values, function (i, value) {
      selectBox.append($('<option>').html(value[0]).val(value[1]));
      selectBox.val(obj.value_ids[0]);
    });
    createEditTag($element, selectBox);
  }

  function getFromDb(fieldClass, donefunction){
    $.ajax({
      url: $('[data-issue-spread-sheet-url]').data('issue-spread-sheet-url'),
      type: 'GET',
      dataType: 'json',
      data : {
        issue_id: id,
        field_class: fieldClass.split(/\s+/)[0]
      }
    }).done(function(obj){
      if(obj.values){ donefunction(obj); }
    });
  }

  function textArea($element, fieldClass){
    getFromDb(fieldClass, function(obj){
      createEditTag(
        $element,
        $('<textarea cols="60" rows="10" id="editable_issue_value"/>').val(obj.values)
      )
      focusBlur($element, fieldClass);
    });
  }

  function formatDate(ui, text){
    return $.datepicker.formatDate(
      'yy-mm-dd',
      $.datepicker.parseDate(ui.datepicker('option', 'dateFormat'), text)
    )
  }

  function dateType($element, $textBox, fieldClass, cf_id){
    var datepickerOption = {
      onClose: function(dateText) {
        update($element, fieldClass, formatDate($textBox, dateText), '', cf_id);
      }
    }
    var format = $('[data-date-format]').data('date-format')
    if(!$.isEmptyObject(format)){ datepickerOption['dateFormat'] = format }

    $textBox.datepicker(datepickerOption);
    createEditTag($element, $textBox);
    $element.children().focus();
  }

  function focusBlur($element, fieldClass, cf_id){
    $element.children().focus().blur(function(){
      var value = $element.children().val();
      var text = $("#editable_issue option:selected").map(function() {
        return $(this).text();
      }).get().join(', ');
      update($element, fieldClass, value, text, cf_id);
    });
  }

  function focusBlurWithCF($element, fieldClass, cf_id, custom_field_tag_name){
    if(fieldClass.match(/^cf_\d+ date$/)){
      focusBlurWithDate($element, fieldClass, cf_id, custom_field_tag_name);
    }else if(fieldClass.match(/^cf_\d+ bool$/)){
      focusBlurWithBool($element, fieldClass, cf_id, custom_field_tag_name)
    }else if(fieldClass.match(/^cf_\d+ (list|enumeration|user|version)$/)){
      focusBlurWithList($element, fieldClass, cf_id, custom_field_tag_name)
    }else{
      focusBlur($element, fieldClass, cf_id);
      // fix for losing focus on click in long text CF
      $element.on('click', "[name='" + custom_field_tag_name + "']", function(e){
        if ($(this).is('textarea')){
          e.preventDefault();
          return false;
        }
      });
      $element.on('keydown', "[name='" + custom_field_tag_name + "']", function(e){
        if (((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) && $(this).is('input[type=text]')){
          e.preventDefault();
          $(this).blur();
        }
      });
    }
  }

  function focusBlurWithDate($element, fieldClass, cf_id, custom_field_tag_name){
    var showOnButton = $('.ui-datepicker-trigger');
    if(showOnButton.length){
      showOnButton.click();
      $('#ui-datepicker-div').css('z-index', '100');
      $element.on('change', "[name='" + custom_field_tag_name + "']", function(){
        var value = $(this).val();
        update($element, fieldClass, value, value, cf_id);
      });
    }else{
      $element.find("[name='" + custom_field_tag_name + "']").focus();
      focusBlur($element, fieldClass, cf_id);
    }
  }

  function focusBlurWithList($element, fieldClass, cf_id, custom_field_tag_name){
    switch ($element.find("[name='" + custom_field_tag_name + "']").attr('type')) {
      case 'radio':
        focusBlurWithRadio($element, fieldClass, cf_id, custom_field_tag_name);
        break;
      case 'checkbox':
        focusBlurWithCheckBox($element, fieldClass, cf_id, custom_field_tag_name);
        break;
      default:
        focusBlur($element, fieldClass, cf_id);
        break;
    }
  }

  function focusBlurWithBool($element, fieldClass, cf_id, custom_field_tag_name){
    selector = $element.find("[name='" + custom_field_tag_name + "']");
    type = selector.attr('type');
    if (type == 'hidden') { // boolean CF as checkbox
      type = selector.next().attr('type');
    }
    switch (type) {
      case 'radio':
        focusBlurWithRadio($element, fieldClass, cf_id, custom_field_tag_name);
        break;
      case 'checkbox':
        focusBlurWithSingleCheckBox($element, fieldClass, cf_id, custom_field_tag_name);
        break;
      default:
        focusBlur($element, fieldClass, cf_id);
        break;
    }
  }

  function focusBlurWithRadio($element, _fieldClass, _cf_id, custom_field_tag_name){
    $element.on('change', "[name='" + custom_field_tag_name + "']", function(){
      $active_element = $element;
    });
    $active_element = $element;
  }

  function focusBlurWithCheckBox($element, _fieldClass, _cf_id, custom_field_tag_name){
    $element.on('change', "[name='" + custom_field_tag_name + "']", function(){
      $active_element = $element;
    });
    $active_element = $element;
    $(document).off('click', contextMenuClick);
  }

  function focusBlurWithSingleCheckBox($element, fieldClass, cf_id, custom_field_tag_name){
    $element.on('change', "[name='" + custom_field_tag_name + "']", function(){
      var value = $(this).prop("checked") ? 1 : 0;
      update($element, fieldClass, value, '', cf_id);
    });
  }

  function update($element, fieldClass, value, text, cf_id){
    $.ajax({
      url: $('[data-issue-spread-sheet-url]').data('issue-spread-sheet-url'),
      type: 'PUT',
      data : {
        issue_id: id,
        project_id: $('[data-project-id]').data('project-id'),
        field_class: fieldClass.split(/\s+/)[0],
        value: value,
        text: text,
        cf_id: cf_id,
        visible_block_columns_caption: $('[data-visible-block-columns-caption]').data('visible-block-columns-caption')
      }
    }).done(function(obj){
      if(obj.status == 'OK'){
        // 値によってはobj.valueが空になることがある。その場合にも内容の書き換えを強制するために空文字を渡す。
        $element.html(obj.value || '');
        // 説明欄の更新時でobj.valueが空の時は、parentElementは削除する
        if(!obj.value && isDescription(fieldClass)){
          $element.parent().remove();
        }
        if(obj.issue.status){
          $element.siblings('.status').html(obj.issue.status);
        }
        if(fieldClass == 'subject' && pr_links.length > 0){
          $element.append(pr_links);
        }

        if(fieldClass == 'status' && obj.flash_notice) {
          var $actual_start_date_field = $('#issue-' + id + ' .actual_start_date');
          if($actual_start_date_field[0] && obj.issue.actual_start_date) {
              $actual_start_date_field.text(obj.issue.actual_start_date);
          }
          var $actual_due_date_field = $('#issue-' + id + ' .actual_due_date');
          if($actual_due_date_field[0] && obj.issue.actual_due_date) {
            $actual_due_date_field.text(obj.issue.actual_due_date);
          }
          issueSpreadSheetNotification(obj.flash_notice).show();
        }
      }else if(obj.status == 'Error'){
        errorMessage(obj.errors);
      }
    }).fail(function(_obj){
      deleteEditTag($element);
    });
  }

  function errorMessage(errors){
    $('<div id="errorExplanation" class="editable_issue_error"><ul><li>'+errors+'</li></li></ul></div>').insertAfter('h2');
    setTimeout(function(){
      $(".editable_issue_error").slideUp("slow");
    },  2000);
  }
});
