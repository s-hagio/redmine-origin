# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

resource :issue_spread_sheet, only: %i[show update], defaults: { format: :json } do
  collection do
    get 'allowed_edit', defaults: { format: :json }
  end
end
