$(function() {
  function colorIcon(color) {
    return $('<div>', {
      class: 'color-icon',
      style: 'background-color: ' + color,
    });
  }

  var colors = $('#issue-status-colors').data('colors');

  $('table.list tbody tr td.name').each(function(i) {
    $(this).prepend(colorIcon(colors[i]));
  });

  $('#issue-status-colors').remove();
});
