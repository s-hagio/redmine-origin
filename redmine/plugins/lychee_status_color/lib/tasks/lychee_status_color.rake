namespace :redmine do
  namespace :plugins do
    namespace :lychee_status_color do
      desc 'Set default colors to existing issue_statuses for Lychee Gantt Chart'
      task set_default_status_colors: :environment do
        ActiveRecord::Base.transaction do
          open_colors = LycheeIssueStatusColor::DEFAULT_OPEN_COLORS.cycle
          IssueStatus.sorted.includes(:lychee_issue_status_color).each do |status|
            color = if status.is_closed?
                      LycheeIssueStatusColor::DEFAULT_CLOSED_COLOR
                    else
                      open_colors.next
                    end

            if status.lychee_issue_status_color.present?
              status.lychee_issue_status_color.update!(color: color)
            else
              status.create_lychee_issue_status_color!(color: color)
            end
          end
        end
      end
    end
  end
end
