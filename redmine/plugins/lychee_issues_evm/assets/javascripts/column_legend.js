$(function() {
  const descriptions = $('#column-legend-descriptions').data('descriptions');
  const columnTitles = {};
  $('.query-columns option').each(function() {
    if (descriptions.hasOwnProperty(this.value)) {
      columnTitles[this.text] = descriptions[this.value];  // No id information in the th tags, so you have to use the label as key
      $(this).text(this.text + " (" + descriptions[this.value] + ")");
    };
  });

  $('table.issues th').each(function() {
    if (columnTitles.hasOwnProperty(this.textContent)) {
      $(this).attr('title', columnTitles[this.textContent]);
    };
  });
});
