var Lrm = (function($) {
  "use strict";

  var Lrm = {};

  Lrm.initialize = function() {
    $("[data-behaviour='select2']").select2({
      dropdownAutoWidth: true
    });

    $('.lrm-target-check').on('click', function() {
      $(this).closest('li').find('li input').prop('checked', $(this).prop('checked'));
    });
  };

  Lrm.addClickHandlerForTableRows = function(selector) {
    $(selector).on('click', function() {
      var chart = $("#chart-container").highcharts();
      var series = chart.series;
      var user_name = $(this).find("th").html();
      var tr = $(this);

      $.each(series, function(_, s) {
        if(s.name === user_name) {
          if(s.visible) {
            s.hide();
          } else {
            s.show();
          }
          tr.toggleClass('selected');
        }
      });
    });
  }

  // グラフをクリックしたときのイベントハンドラ
  // （テーブルのハイライト）
  Lrm.chartClickHandlerForTableHighlight = function() {
    var table = $('.lrm-table.workload');
    table.find('tr.selected').removeClass('selected');
    var index = this.userOptions.order;
    var rowspan = +table.find('.row-key').attr('rowspan');
    for (var i = 1; i <= rowspan; i++) {
      table.find('tbody tr:nth-child(' + (rowspan * index + i) + ')').addClass('selected');
    }
  }

  function deepCopy(object) {
    return JSON.parse(JSON.stringify(object));
  }

  // グラフをクリックしたときのイベントハンドラ
  // （クリックされたseriesを一番下に移動）
  // （部分グラフのハイライト）
  var originalChart, originalDatas; // scope: Lrm
  Lrm.chartClickHandlerForChartHighlight = function() {
    var index = Number(this.series.userOptions.order);

    if (originalChart === undefined) {
      // save chart for reset chart
      originalChart = $("#chart-container").highcharts();
      originalDatas = deepCopy(originalChart.series.map(function(elem) {
        return {
          data: elem.options.data,
          userOptions: elem.userOptions
        }
      }));
    }
    var sortedDatas = originalDatas.slice(0, index * 2);
    sortedDatas = sortedDatas.concat(originalDatas.slice(index * 2 + 1 + 1));
    sortedDatas.push(originalDatas[index * 2]);
    sortedDatas.push(originalDatas[index * 2 + 1]);

    // グラフ全体をweak色で塗りつぶす
    var chart = $("#chart-container").highcharts();
    chart.series.forEach(function(elem, i) {
      elem.setData(sortedDatas[i].data, false);
      elem.update({
        order: sortedDatas[i].userOptions.order,
        name: sortedDatas[i].userOptions.name,
        color: sortedDatas[i].userOptions.originalColor
      });
    });

    // 選択部分をstrong色で塗りつぶす
    [originalChart.series.length - 2, originalChart.series.length - 1].forEach(function(targetIndex) {
      chart.series[targetIndex].update({
        color: sortedDatas[targetIndex].userOptions.originalColorStrong,
      });
    });
  }

  function clearChart() {
    // グラフ全体をdefaul色で塗りつぶす
    if (originalChart === undefined) return;
    originalChart.series.forEach(function(elem, i) {
      elem.setData(originalDatas[i].data, false);
      elem.update({
        order: originalDatas[i].userOptions.order,
        name: originalDatas[i].userOptions.name,
        color: originalDatas[i].userOptions.originalColor
      });
    });
  }

  // グラフの背景をクリックしたときのイベントハンドラ
  // （部分グラフのハイライトリセット）
  Lrm.backgroundClickHandler = function() {
    clearChart();
    $(".lrm-table.workload tr").removeClass("selected");
  }

  Lrm.initializeStickyLrmTable = function(){
    watchUpDownScrollToFixSideScrollableHeaders();
    sideScrollWithFixedHeader();

    // テーブルが画面上部までスクロールされたときに日付ヘッダを固定するためにスクロールを監視
    function watchUpDownScrollToFixSideScrollableHeaders() {
      var $table = $(".lrm-table");
      if($table.offset()){
        var tablePosition = $table.offset().top;
      }
      var $headerRow = $(".lrm-table .header-row");    
      var fixed = false;
      $(window).on("scroll", function() {
        var aboveScreen = $(this).scrollTop() > tablePosition;
        if( aboveScreen && !fixed ) {
          $headerRow.addClass("top_fixed");
          $table.addClass("lrm-top-fixed-table");
          fixSideScrollableHeaders();
          fixLeftHeaderPosition(10); // #tab-mainからの相対positionから絶対positionへ
          fixed = true;
        } else if( !aboveScreen && fixed ) {
          $headerRow.removeClass("top_fixed");
          $table.removeClass("lrm-top-fixed-table");
          fixLeftHeaderPosition(-20); // 絶対positionから#tab-mainからの相対positionへ
          fixed = false;
        }
      });  
    }

    function fixLeftHeaderPosition(fixedPosition){
      var $leftHeaders = $(".lrm-table .header-row .left-header");
      $leftHeaders.each(function (idx, header) {
        $(header).css("left", $(header).offset().left + fixedPosition)
      });
    }

    function fixSideScrollableHeaders(){
      var $dateHeaders = $(".lrm-table .header-row .date");
      var $totalHeader = $(".lrm-table .header-row #row_total");
      // 時間表示部分のセル
      var $referenceCells = $(".lrm-table tbody tr:first-child td");
      // 横スクロールの移動を反映した状態で日付ヘッダと合計ヘッダが固定されるようにする
      $dateHeaders.each(function (idx, header) {
        fixHeaderPositionAndWidth($(header), $referenceCells.eq(idx));
      });
      // 合計セルはborderWidthを考慮しない
      $totalHeader.width($referenceCells.last().width());
      $totalHeader.css("left", $referenceCells.last().offset().left);
    }
  
    function fixHeaderPositionAndWidth(fixedCell, referenceCell){
      var borderWidth = 2;
      fixedCell.width(referenceCell.width() + borderWidth);
      // fixedCellはposition: fixedとなっていることを期待している
      fixedCell.css("left", referenceCell.offset().left);
    }
  
    // 表を横スクロールしたとき固定されたヘッダ（横スクロール可能なもの）も一緒に動くようにする
    function sideScrollWithFixedHeader() {
      var $scrollElement = $("#tab-main");
      var userAgent = window.navigator.userAgent.toLowerCase();
      var ie11 = userAgent.indexOf('trident') != -1;
  
      // 横スクロール可能なヘッダ - 上部固定前のエレメント
      var $sideScrollableHeaders;
      if(ie11){
        $sideScrollableHeaders = $(".lrm-table .header-row th");
      } else {
        // IE11以外は左ヘッダ（集計対象項目）は固定
        $sideScrollableHeaders = $(".lrm-table .header-row th:not(.left-header)");
      }
  
      // 上部固定前の位置を保持しておく
      var headerLeftPositions = [];
      $sideScrollableHeaders.each(function(idx, element){
        headerLeftPositions[idx] = $(element).offset().left;
      });
  
      $scrollElement.on("scroll", function() {
        var sideScrollQuantity = $scrollElement.scrollLeft();
        // 横スクロール可能なヘッダ - 上部固定後のエレメント
        var $topFixedSideScrollableHeaders;
        if(ie11) {
          $topFixedSideScrollableHeaders = $(".lrm-table .header-row.top_fixed th");
        } else {
          // IE11以外は左ヘッダ（集計対象項目）は固定
          $topFixedSideScrollableHeaders = $(".lrm-table .header-row.top_fixed th:not(.left-header)");
        }
        $topFixedSideScrollableHeaders.each(function(idx, element){
          // 元の位置からスクロールされた分だけ移動させる
          $(element).css("left", headerLeftPositions[idx] - sideScrollQuantity);
        });
      });
    }
  }
  
  return(Lrm);
}($));
