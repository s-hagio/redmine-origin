var LrmMonthrangepicker = (function($) {
  'use strict';

  var LrmMonthrangepicker = {};

  var INTERNAL_PICKER_INPUT_HTML = '<input type="text" style="margin: 0; border: 0; padding: 0; width: 0; visiblity: hidden;">';

  function determinePickerOptions(currentLanguage) {
    var pastYearRange = [moment().subtract(11, 'month').startOf('month'), moment().endOf('month')];
    var pastHalfYearRange = [moment().subtract(5, 'month').startOf('month'), moment().endOf('month')];
    var lastMonthRange = [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')];
    var thisMonthRange = [moment().startOf('month'), moment().endOf('month')];

    if (currentLanguage === 'ja') {
      return {
        locale: {
          format: 'YYYY-MM',
          applyLabel: '適用',
          cancelLabel: 'キャンセル',
          monthNames: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
        },
      };
    }

    return {
      locale: {
        format: 'YYYY-MM',
      },
    };
  }

  function installUndeterminedPeriodFeature($picker) {
    $picker.haveDeterminedPeriod = function() {
      return this.startDate.isValid() &&
        (!this.endDate || // 開始月だけクリックして終了月をクリックしていないとき
         this.endDate.isValid());
    };
    $picker.updateMonthsInView = function() {
      if (this.haveDeterminedPeriod()) {
        monthrangepicker.prototype.updateMonthsInView.apply(this, arguments);
        return;
      }

      this.leftCalendar.month = moment().startOf('year');
      this.rightCalendar.month = moment().add(1, 'year').endOf('year');
    };
    $picker.updateCalendars = function() {
      monthrangepicker.prototype.updateCalendars.apply(this, arguments);
      if (this.haveDeterminedPeriod()) {
        return;
      }

      this.container.
        find('.active, .start-date, .end-date, .in-range').
        removeClass(['active', 'start-date', 'end-date', 'in-range']);
    };

    // 月範囲未選択状態でも「適用」ボタンがクリックできる変更
    $picker.updateFormInputs = function() {
      if (this.haveDeterminedPeriod()) {
        monthrangepicker.prototype.updateFormInputs.apply(this, arguments);
        return;
      }

      this.container.find('button.applyBtn').removeAttr('disabled');
    };

    // 「適用」ボタンクリック時に月範囲未選択状態なら発火しない
    $picker.callbackOriginal = $picker.callback;
    $picker.callback = function() {
      if (!this.haveDeterminedPeriod()) {
        return;
      }

      this.callbackOriginal.apply(this, arguments);
    };
  }

  LrmMonthrangepicker.apply = function(currentLanguage, $showButton, $fromDate, $toDate) {
    $showButton.before(INTERNAL_PICKER_INPUT_HTML);
    var $internalPickerInput = $showButton.prev();
    $internalPickerInput.monthrangepicker(
      determinePickerOptions(currentLanguage),
      function(startDate, endDate) {
        $fromDate.val(startDate.format('YYYY-MM-DD'));
        $toDate.val(endDate.endOf('month').format('YYYY-MM-DD'));
      },
    );
    var $picker = $internalPickerInput.data('daterangepicker');
    installUndeterminedPeriodFeature($picker);
    $showButton.click(function() {
      $picker.setStartDate($fromDate.val());
      $picker.setEndDate($toDate.val());
      $picker.show();

      // カレンダーの下の開始月と終了月の表示を削除する対応
      $picker.container.find('.drp-selected').hide();
    });
  };

  return LrmMonthrangepicker;
}($));

// ujs化
$(function() {
  $('*[data-lrm-monthdatepicker-button]').each(function() {
    var $showButton = $(this);
    LrmMonthrangepicker.apply(
      $showButton.data('lrm-monthdatepicker-language'),
      $showButton,
      $($showButton.data('lrm-monthdatepicker-from-date')),
      $($showButton.data('lrm-monthdatepicker-to-date')),
    );
  });
});
