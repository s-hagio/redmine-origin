# frozen_string_literal: true

namespace :redmine do
  namespace :plugins do
    namespace :lrm do
      desc 'Generate sample data for LRM for development and demo.'
      task seed: :environment do
        load Rails.root.join('plugins', 'lrm', 'db', 'seeds', "#{Rails.env}.rb")
      end
    end
  end
end
