namespace :redmine do
  namespace :plugins do
    namespace :lrm do
      desc 'Migrate custom queries from v1.8 to v2.0'
      task migrate_custom_queries: :environment do
        module MigrateCustomQuery
          class ValidationError < StandardError; end
          class LrmQueriesPrincipal < ActiveRecord::Base
            self.table_name = :lrm_queries_principals
            belongs_to :principal, class_name: 'Principal'
          end
          ::LrmQuery.has_many(:lrm_queries_principals, foreign_key: :lrm_query_id,
                                                       class_name: 'MigrateCustomQuery::LrmQueriesPrincipal')

          class << self
            def lrm_queries_with_principals
              LrmQuery.where.not(name: LrmQuery::SESSION_NAME).includes(:lrm_queries_principals)
            end

            def filter_by_type(type)
              type == 'User' ? 'assigned_to_id' : 'member_of_group'
            end

            def errors
              @errors ||= ActiveModel::Errors.new(self)
            end
          end
        end

        MigrateCustomQuery.errors.clear # for spec

        # Validate
        MigrateCustomQuery.lrm_queries_with_principals.find_each do |lrm_query|
          Principal.where(id: lrm_query.lrm_queries_principals.pluck(:principal_id)).pluck(:type).uniq.each do |type|
            next MigrateCustomQuery.errors.add(lrm_query.name, '対象者もしくは対象グループが正しくありません') unless %w[User Group].include?(type)
            filter = MigrateCustomQuery.filter_by_type(type)
            if lrm_query.filters && lrm_query.filters[filter] && lrm_query.filters[filter][:operator] != '='
              MigrateCustomQuery.errors.add(lrm_query.name, "#{filter}が'等しい'ではありません")
            end
          end

          next if lrm_query.group_summary.nil?
          group_key = lrm_query.group_summary ? 'group' : 'user'
          if lrm_query.group_keys.exclude?(group_key) && lrm_query.group_keys.size >=3
            MigrateCustomQuery.errors.add(lrm_query.name, "集計対象項目は３つまで入力できます")
          end
        end

        if MigrateCustomQuery.errors.size > 0
          # Show validation errors
          puts "#{MigrateCustomQuery.errors.size} errors detected"
          MigrateCustomQuery.errors.messages.each do |query_name, messages|
            puts "#{query_name}:"
            messages.each { |message| puts " - #{message}" }
          end
          raise MigrateCustomQuery::ValidationError, "#{MigrateCustomQuery.errors.size} errors detected"
        else
          # Migrate custom queries
          ActiveRecord::Base.transaction do
            MigrateCustomQuery.lrm_queries_with_principals.find_each do |lrm_query|
              Principal.where(id: lrm_query.lrm_queries_principals.pluck(:principal_id)).group_by(&:type).each do |type, principals|
                filter = MigrateCustomQuery.filter_by_type(type)
                lrm_query.filters = {} unless lrm_query.filters
                lrm_query.filters = lrm_query.filters.with_indifferent_access
                lrm_query.filters[filter] = { operator: '=', values: [] } unless lrm_query.filters[filter]
                lrm_query.filters[filter][:values] = lrm_query.filters[filter][:values].concat(principals.map { |principal| principal.id.to_s }).uniq
              end

              unless lrm_query.group_summary.nil?
                lrm_query.group_keys = lrm_query.group_keys.push(lrm_query.group_summary ? :group : :user)
              end
              lrm_query.save!
            end
          end
          puts '全て移行できました'
        end
      end
    end
  end
end
