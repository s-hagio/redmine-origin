# frozen_string_literal: true

resources :resource_management, only: :index do
  collection do
    get :workload
    get :working_rate
    get :productivity
    get :oyakata_reports, controller: 'oyakata_reports'
  end
end

resources :lrm_queries, except: :index

get '/resource_management_settings', to: 'resource_management_settings#index', as: 'resource_management_settings'
get '/resource_management_settings/distributed_workloads', to: 'distributed_workloads#index', as: 'distributed_workloads'
put '/resource_management_settings/distributed_workloads', to: 'distributed_workloads#update', as: 'update_distributed_workloads'
get '/resource_management_settings/threshold_settings', to: 'threshold_settings#index', as: 'threshold_settings'
put '/resource_management_settings/threshold_settings', to: 'threshold_settings#update', as: 'update_threshold_settings'
get '/resource_management_settings/time_management', to: 'lrm_time_management_setting#show', as: 'lrm_time_management_setting'
put '/resource_management_settings/time_management', to: 'lrm_time_management_setting#update', as: 'update_lrm_time_management_setting'
