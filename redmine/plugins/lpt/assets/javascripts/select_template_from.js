function lychee_project_template_required_base_date() {
  var label = $("[name^='base_date']").closest('p').find('label').first();
  var required = label.find('.required');
  if ($('select#template_from').val() != '') {
    required.length ? required.show() : label.append('<span class="required"> *</span>');
  } else {
    required.hide();
  };
};

function lychee_project_template_display_base_date() {
  var is_based_start_date = $('input[name="base_date"]:checked').val() === 'start_date'
  $('#project_start_date').toggle(is_based_start_date);
  $('#project_due_date').toggle(!is_based_start_date);
  $("#project_start_date").prop("disabled", !is_based_start_date);
  $("#project_due_date").prop("disabled", is_based_start_date);
};

// multiple choices controls (list, KV) sending separate data for every choice
// combine them per control ID
function lychee_project_template_combine_values(data) {
  var combined_values = [];
  for (var i = 0; i < data.project_custom_fields.length; i++) {
    var id = data.project_custom_fields[i].custom_field_id;
    var value = data.project_custom_fields[i].value;
    if (combined_values[id]) {
      combined_values[id].push(value);
    } else {
      combined_values[id] = [value];
    };
  };
  return combined_values;
};

function lychee_project_template_selector_from_id(id, data) {
  var control_name = 'project[custom_field_values][' + id + ']';
  // multiple choices controls (list, KV) have additional []
  // in the end of name
  if (data.multiple_values[id]) {
    control_name = control_name + '[]';
  }
  control_name = ":visible[name='" + control_name + "']";
  return control_name;
};

// used after validation error
$(document).ready(function () {
  lychee_project_template_required_base_date();
  lychee_project_template_display_base_date();
});

// used after from template selection
$(document).on('change', 'select#template_from', function () {
    lychee_project_template_required_base_date();
    $.ajax({
        type: 'GET',
        url: $('#select_template_from_path').val(),
        data: {
            project_id: $(this).val()
        }
    }).done(function(data) {
        // 標準項目
        $('input#project_name').val(data.name);
        $('textarea#project_description').val(data.description);
        $('input#project_identifier').val(data.identifier);
        $('input#project_homepage').val(data.homepage);
        $('input#project_is_public').prop('checked', data.is_public);
        if($('select#project_parent_id').val().length === 0) {
          $('select#project_parent_id').val(data.parent_id);
        }
        $('input#project_inherit_members').prop('checked', data.inherit_members);
        // プロジェクトカスタムフィールド
        $('[id^=project_custom_field_values_]').each(function(index, element) {
          if ($(element).attr('type') !== 'checkbox') { // checkbox は hidden 属性の element が存在する。 checked="checked" で制御するため
            $(element).val('');
          }
        });

        var combined_values = lychee_project_template_combine_values(data);
        for (var i = 0; i < data.project_custom_fields.length; i++) {
          var project_cf_id = data.project_custom_fields[i].custom_field_id;
          var project_cf_control_selector = lychee_project_template_selector_from_id(project_cf_id, data);
          $(project_cf_control_selector).val(combined_values[project_cf_id]);
          $(project_cf_control_selector).trigger('change');
        }
        // モジュール
        $('input[id^=project_enabled_module_names_]').prop('checked', false);
        for (var i = 0; i < data.enabled_modules.length; i++) {
          $('input#project_enabled_module_names_' + data.enabled_modules[i].name).prop('checked', true);
        }
        // トラッカー
        $("[name='project[tracker_ids][]']").prop('checked', false);
        for (var i = 0; i < data.trackers.length; i++) {
          $("[name='project[tracker_ids][]'][value=" + data.trackers[i].id + "]").prop('checked', true);
        }
        // チケットカスタムフィールド
        $("[name='project[issue_custom_field_ids][]'][disabled!='disabled']").prop('checked', false);
        for (var i = 0; i < data.issue_custom_fields.length; i++) {
          $("[name='project[issue_custom_field_ids][]'][value=" + data.issue_custom_fields[i].id + "]").prop('checked', true);
        }
    });
});

$(document).on('change', 'select#project_parent_id', function () {
  $.ajax({
    type: 'GET',
    url: $('#change_project_parent_path').val(),
    data: { project_parent_id: $(this).val() }
  }).done(function(data) {
    var templateForm = $('select#template_from');
    var selectedTemplate = templateForm.val();
    templateForm.children('[value!=""]').remove();
    // 選択肢差し替え
    $.each(data, function() {
      var newOption = $('<option>').val(this.id).text(this.name);
      // 既に選択済みのテンプレートが差し替え後も存在している場合は初期選択状態とする
      if (selectedTemplate == this.id.toString()) {
        newOption.attr('selected', true);
      }
      templateForm.append(newOption);
    });
  });
});

$(document).on('change', 'input[name="base_date"]:checked', function () {
  lychee_project_template_display_base_date();
});
