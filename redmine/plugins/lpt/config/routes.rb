# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
get 'project/select_template_from', to: 'projects#select_template_from', as: 'select_template_from'
get 'project/change_project_parent', to: 'projects#change_project_parent', as: 'change_project_parent'
