group :development, :test do
  gem 'pry', '<= 0.12.2' if RUBY_VERSION < '2.4'
  gem 'pry-rails'
  gem 'pry-byebug', (RUBY_VERSION >= '2.4' ? '>= 0' : '~> 3.6')

  gem 'rspec-rails'
  gem 'factory_bot_rails'

  unless dependencies.detect { |d| d.name == 'rubocop' }
    gem 'rubocop', require: false
    gem 'rubocop-no_keyword_args', git: 'https://github.com/agileware-jp/rubocop-no_keyword_args.git', require: false
    gem 'rubocop-lychee', git: 'https://github.com/agileware-jp/rubocop-lychee.git', require: false
  end
end

group :test do
  dependencies.reject! { |i| %w[capybara nokogiri selenium-webdriver].include? i.name }
  gem 'capybara', '~> 3'
  gem 'selenium-webdriver', '~> 3'
  gem 'chromedriver-helper' unless ENV['CI']
  gem 'puma', '~> 3.7' if dependencies.none? { |i| i.name == 'puma' }
  gem 'database_cleaner'
  gem 'rspec_junit_formatter'
end
