$(function(){
  function findChildren($el){
    return $el.siblings("[data-parent-id='" + $el.data('principal-id') + "'], [data-parent-id='" + $el.data('member-id') + "']");
  }
  function showChildren($el){
    if(isExpanded($el.find('.tree-expander'))){
      findChildren($el).each(function(){ showChildren($(this)); });
    }
    $el.show();
  }
  function hideChildren($el){
    findChildren($el).each(function(){ hideChildren($(this)); });

    $el.hide();
  }
  function isExpanded($el){
    return $el.is('.tree-expander-expanded');
  }

  $('.controller-projects.action-settings').on('click', '.tree-expander', function(){
    var $this = $(this);
    var $parent = $this.parents('.organization, .member');

    if(isExpanded($this)){
      findChildren($parent).each(function(){ hideChildren($(this)); });

      $this.removeClass('tree-expander-expanded').addClass('tree-expander-collapsed');
    }else{
      findChildren($parent).each(function(){ showChildren($(this)); });

      $this.removeClass('tree-expander-collapsed').addClass('tree-expander-expanded');
    }
  })
})
