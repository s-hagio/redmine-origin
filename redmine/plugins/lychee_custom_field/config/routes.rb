resources :custom_field_categories, only: %i[index new edit create update destroy]

resources :required_custom_fields, only: :index do
  collection do
    patch 'bulk_update'
  end
end

resources :invisible_custom_field_enumerations, only: :index do
  collection do
    patch 'bulk_update'
  end
end

resources :custom_field_setting_state, only: [:index]

delete 'custom_field_setting_state/required/:id/',
  to: 'custom_field_setting_state#required_bulk_update',
  as: 'required_bulk_update'

delete 'custom_field_setting_state/invisible/:source_id/:id/',
  to: 'custom_field_setting_state#invisible_bulk_update',
  as: 'invisible_bulk_update'

put 'projects/:project_id/per_project_lists/:custom_field_id',
    to: 'per_project_lists#update_all',
    as: 'update_all_per_project_list'
put 'projects/:project_id/per_project_lists/:custom_field_id/add',
    to: 'per_project_lists#add',
    as: 'add_possible_value'
delete 'projects/:project_id/per_project_lists/:custom_field_id/delete/:index/',
    to: 'per_project_lists#destroy',
    as: 'destroy_possible_value'

put 'projects/:project_id/per_project_kv/:custom_field_id',
    to: 'per_project_kv#update_all',
    as: 'update_all_per_project_kv'
put 'projects/:project_id/per_project_kv/:custom_field_id/add',
    to: 'per_project_kv#add',
    as: 'add_possible_enumeration'
delete 'projects/:project_id/per_project_kv/:custom_field_id/delete/:index/',
    to: 'per_project_kv#destroy',
    as: 'destroy_possible_enumeration'

# Per project REST api
get 'projects/:project_id/custom_fields',
    to: 'per_project_api#index',
    as: 'index_per_project'
get 'projects/:project_id/custom_fields/:custom_field_id',
    to: 'per_project_api#show',
    as: 'show_per_project'
post 'projects/:project_id/custom_fields/:custom_field_id',
    to: 'per_project_api#create',
    as: 'create_per_project'
delete 'projects/:project_id/custom_fields/:custom_field_id',
    to: 'per_project_api#delete',
    as: 'delete_per_project'
put 'projects/:project_id/custom_fields/:custom_field_id/position/:position',
    to: 'per_project_api#replace',
    as: 'replace_per_project_value'
put 'projects/:project_id/custom_fields/:custom_field_id',
    to: 'per_project_api#add',
    as: 'add_per_project_value'
delete 'projects/:project_id/custom_fields/:custom_field_id/position/:position',
    to: 'per_project_api#destroy',
    as: 'delete_per_project_value'
