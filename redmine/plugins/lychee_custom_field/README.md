# Lychee Custom Field

'Smart' Custom Fields:

- grouping in categories with 'foldable' view in issues forms
- specific for given project values in list-type custom fields

## Per project value lists API

See [Per Project Lists API documentation](PER_PROJECT_LISTS_API.md) for more details.

## Environment

- Ruby 2.0.x
- Redmine 3.4.x
