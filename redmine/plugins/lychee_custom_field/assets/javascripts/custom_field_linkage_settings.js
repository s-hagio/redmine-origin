CustomFieldLinkageSettings = {}

CustomFieldLinkageSettings.formSelector = 'form.custom_field_linkage_setting';

CustomFieldLinkageSettings.setBeforeTabChangeConfirm = function(message) {
  $('div.tabs > ul > li > a').on('click', function() {
    if($(CustomFieldLinkageSettings.formSelector).hasClass('changed')) {
      return confirm(message);
    }
  });
}

$(document).on('change', CustomFieldLinkageSettings.formSelector, function() {
  $(this).addClass('changed');
});
