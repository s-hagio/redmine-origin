(function() {
  $(document).on('click', "input[name='add_possible_value']", function() {
    ajax_add_possible_value($(this).data('custom-field-id'));
    return false;
  });

  $(document).on('click', "input[name='add_possible_enumeration']", function() {
    ajax_add_possible_enumeration($(this).data('custom-field-id'));
    return false;
  });

  $(document).on('keypress', '[id^=text_add_possible_value_]', function(event) {
    if (event.which === 13) {
      ajax_add_possible_value($(this).next("input[name='add_possible_value']").data('custom-field-id'));
      return false;
    }
  });

  $(document).on('keypress', '[id^=text_add_possible_enumeration_]', function(event) {
    if (event.which === 13) {
      ajax_add_possible_enumeration($(this).next("input[name='add_possible_enumeration']").data('custom-field-id'));
      return false;
    }
  });

  function ajax_add_possible_value(custom_field_id) {
    $.ajax({
      type: 'PUT',
      url: $('#add_possible_value_path_' + custom_field_id).val(),
      data: {
        project_id: $('#project_id').val(),
        possible_value: $('#text_add_possible_value_' + custom_field_id).val()
      }
    });
  }

  function ajax_add_possible_enumeration(custom_field_id) {
    $.ajax({
      type: 'PUT',
      url: $('#add_possible_enumeration_path_' + custom_field_id).val(),
      data: {
        project_id: $('#project_id').val(),
        possible_enumeration: $('#text_add_possible_enumeration_' + custom_field_id).val()
      }
    });
  }
})();
