var lastSelectedRow, lastSelectedColumn;

function contextMenuClick(event) {
  event.stopPropagation();
  var target = $(event.target);
  contextMenuHide();
  if (!target.hasClass('hascontextmenu')) {
    return;
  }
  if (target.hasClass('rowmenu')) {
    lastSelectedRow = target.closest('.hascontextmenu').parent('tr');
    contextMenuToggleRowSelection(lastSelectedRow);
    lastSelectedColumn = undefined;
  }
  if (target.hasClass('columnmenu')) {
    var cell = target.closest('td');
    var cellIndex = cell[0].cellIndex;
    lastSelectedColumn = selectedColumnCells(cellIndex);
    contextMenuToggleColSelection(lastSelectedColumn);
    lastSelectedRow = undefined;
  }
  contextMenuShow(event);
}

function contextMenuInsideClick(event) {
  contextMenuHide();
  var target = $(event.target);
  var visibility = target.hasClass('visible') ? '0' : '1';
  if (lastSelectedRow) {
    $(lastSelectedRow)
      .children()
      .each(function () {
        changeVisibility(this, visibility);
      });
  }
  if (lastSelectedColumn) {
    lastSelectedColumn.forEach(function (cell) {
      changeVisibility(cell, visibility);
    });
  }
}

function contextMenuShow(event, target) {
  var mouse_x = event.pageX;
  var mouse_y = event.pageY;
  var mouse_y_c = event.clientY;
  var render_x = mouse_x;
  var render_y = mouse_y;
  var menu_width;
  var menu_height;
  var window_width;
  var window_height;
  var max_width;
  var max_height;

  $('#context-menu').css('left', render_x + 'px');
  $('#context-menu').css('top', render_y + 'px');
  $('#context-menu').html(`
    <ul>
      <li><a class="visible" rel="nofollow" href="#">${optionsForSelect.visible}</a></li>
      <li><a class="invisible" rel="nofollow" href="#">${optionsForSelect.invisible}</a></li>
    </ul>
  `);

  menu_width = $('#context-menu').width();
  menu_height = $('#context-menu').height();
  max_width = mouse_x + 2 * menu_width;
  max_height = mouse_y_c + menu_height;

  var ws = window_size();
  window_width = ws.width;
  window_height = ws.height;

  /* display the menu above and/or to the left of the click if needed */
  if (max_width > window_width) {
    render_x -= menu_width;
    $('#context-menu').addClass('reverse-x');
  } else {
    $('#context-menu').removeClass('reverse-x');
  }

  if (max_height > window_height) {
    render_y -= menu_height;
    $('#context-menu').addClass('reverse-y');
  } else {
    $('#context-menu').removeClass('reverse-y');
  }

  if (render_x <= 0) render_x = 1;
  if (render_y <= 0) render_y = 1;
  $('#context-menu').css('left', render_x + 'px');
  $('#context-menu').css('top', render_y + 'px');
  $('#context-menu').show();
}

function contextMenuHide() {
  $('#context-menu').hide();
  contextMenuUnselectAll();
}

function contextMenuCreate() {
  if ($('#context-menu').length < 1) {
    var menu = document.createElement('div');
    menu.setAttribute('id', 'context-menu');
    menu.setAttribute('style', 'display:none;');
    document.getElementById('content').appendChild(menu);
    $('#context-menu').click(contextMenuInsideClick);
  }
}

function contextMenuUnselectAll() {
  $('.hascontextmenu').each(function () {
    contextMenuRemoveSelection($(this).parent('tr'));
  });
  if (lastSelectedColumn) {
    contextMenuRemoveColSelection(lastSelectedColumn);
  }
}

function contextMenuRemoveSelection(tr) {
  tr.removeClass('context-menu-selection');
}

function contextMenuToggleRowSelection(tr) {
  if (tr.hasClass('context-menu-selection')) {
    contextMenuRemoveSelection(tr);
  } else {
    tr.addClass('context-menu-selection');
  }
}

function contextMenuToggleColSelection(cells) {
  cells.forEach(function (cell) {
    if ($(cell).hasClass('context-menu-selection')) {
      $(cell).removeClass('context-menu-selection');
    } else {
      $(cell).addClass('context-menu-selection');
    }
  });
}

function contextMenuRemoveColSelection(cells) {
  cells.forEach(function (cell) {
    if ($(cell).hasClass('context-menu-selection')) {
      $(cell).removeClass('context-menu-selection');
    }
  });
}

function contextMenuInit() {
  contextMenuCreate();
  contextMenuUnselectAll();
  $(document).on('click', '#content', contextMenuClick);
}

function window_size() {
  var w;
  var h;
  if (window.innerWidth) {
    w = window.innerWidth;
    h = window.innerHeight;
  } else if (document.documentElement) {
    w = document.documentElement.clientWidth;
    h = document.documentElement.clientHeight;
  } else {
    w = document.body.clientWidth;
    h = document.body.clientHeight;
  }
  return { width: w, height: h };
}

function changeVisibility(cell, visibility) {
  var selectBox = $(cell).children().first();
  $(selectBox).val(visibility);
}

function selectedColumnCells(cellIndex) {
  var rows = targetTable.find('tbody tr');
  var cellsArray = [];
  rows.each(function () {
    var cells = $(this).find(`td:nth-child(${cellIndex + 1})`);
    cellsArray.push(cells[0]);
  });
  return cellsArray;
}

$(document).ready(function () {
  contextMenuInit();
});
