const CATS_SESSION_KEY = 'lychee_custom_field/collapsed_cats/';

$(function() {
  $(document).on("click", ".collapsible legend", function() {
    toggleFieldset(this);
    toggleCookie($(this).parents('fieldset').first());
  });

  function toggleCookie(fieldset) {
    var keyName = CATS_SESSION_KEY + fieldset.attr('id');
    if (fieldset.hasClass('collapsed')) {
      Cookies.set(keyName, 'collapsed', {expires: 7, path: ''});
    } else {
      Cookies.set(keyName, 'opened', {expires: 7, path: ''});
    }
  }
}());
