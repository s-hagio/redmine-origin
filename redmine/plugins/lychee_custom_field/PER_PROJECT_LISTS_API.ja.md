# Lycheeカスタムフィールド「プロジェクトごとのリスト」API

Lycheeカスタムフィールドは、「プロジェクトごとの設定」がONになっているリスト型およびキー・バリュー リスト型のカスタムフィールドについて、「プロジェクトごとのリスト」の取得、追加、更新、削除を行うAPIを提供しています。

※カスタムフィールドの「プロジェクトごとの設定」をAPIから有効にすることはできません。あらかじめ「プロジェクトごとの設定」を有効にしてご利用ください。

## 概要

Redmine本体のREST APIと同様のインターフェースを持ちます。

APIの出力形式はJSONおよびXMLです。URLの末尾に`.json`または`.xml`を指定してください。

## 認証

以下の方法で認証情報を渡すことができます。

* `key`パラメータにAPIアクセスキーの値をセットする
* HTTPのBasic認証で、`username`にAPIアクセスキーの値をセットし、`password`に任意の値をセットする
* HTTPの`X-Redmine-API-Key`にAPIアクセスキーの値をセットする
* HTTPのBasic認証で、`username`にログインIDをセットし、`password`にログインパスワードをセットする

あらかじめ「管理＞設定＞API＞RESTによるWebサービスを有効にする」を有効にする必要があります。「APIアクセスキー」はユーザーごとに割り当てられており、「個人設定」ページの右カラムで確認することができます。

## 使用例

以下の例ではHTTPクライアントとして[HTTPie](https://httpie.org/)を使用しています。

- `{project_id}`は、操作対象のプロジェクトの「設定＞プロジェクト＞識別子」に表示されている値に置き換えてください。
- `{custom_field_id}`は、操作対象のカスタムフィールドの編集画面のURLの`/custom_fields/**/edit`の`**`に相当する値に置き換えてください。

### [取得] 指定したプロジェクトの「プロジェクトごとのリスト」が有効になったカスタムフィールドの一覧

```bash
> http GET localhost:3000/projects/{project_id}/custom_fields.json?key=1234
{
    "per_project_api": [
        {
            "custom_field": {
                "id": 1,
                "name": "ppr_list"
                "format": "list",
            },
            "project": {
                "id": 1,
                "identifier": "lcf-tests"
            }
        },
        ...
    ]
}
```

プロジェクトが存在しなければ404 Not Foundを返します。

### [取得] 指定したプロジェクトのカスタムフィールドの「プロジェクトごとのリスト」の一覧

「プロジェクトごとのリスト」の値とIDを返します。

```bash
> http localhost:3000/projects/{project_id}/custom_fields/{custom_field_id}.json?key=1234
{
    "per_project_api": {
        "project": {
            "id": 1,
            "identifier": "lcf-tests"
        },
        "custom_field": {
            "id": 1,
            "name": "ppr_list"
            "format": "list",
            "is_required": false,
            "default_value": "",
            "available_values": [
                { "name": "a", "position": 1 },
                { "name": "b", "position": 2 },
                { "name": "c", "position": 3 }
            ]
        }
    }
}
```

「プロジェクトごとのリスト」が存在しない場合は404 Not Foundを返します。

### [作成] 「プロジェクトごとのリスト」の初期値を作成

指定したプロジェクトのカスタムフィールドに「プロジェクトごとのリスト」が設定されていない場合、Redmine本体で設定された選択肢から初期値を作成します。

```bash
> http POST localhost:3000/projects/{project_id}/custom_fields/{custom_field_id}.json?key=1234
```

プロジェクトやカスタムフィールドが存在しない場合には404 Not Foundを返します。

### [作成] 「プロジェクトごとのリスト」に値を追加

> **possible_value** パラメータが必須です。

`possible_value`パラメータで指定した値が「プロジェクトごとのリスト」の末尾に追加されます。

```bash
> http PUT localhost:3000/projects/{project_id}/custom_fields/{custom_field_id}.json?key=1234 <<<'{ "possible_value" : "aaa" }'
```

プロジェクトやカスタムフィールドが存在しない場合、または「プロジェクトごとのリスト」に値が存在しない場合には404 Not Foundを返します。

`possible_value`が空の場合は以下のエラーを返します。

```bash
HTTP/1.1 422 Unprocessable Entity
...
    "errors": [
        "List item can not be empty."
    ]
```

### [更新] 指定した位置の「プロジェクトごとのリスト」の値を更新

> **possible_value** パラメータが必須です。

`{position}`番目の「プロジェクトごとのリスト」の値を更新します。

```bash
> http PUT localhost:3000/projects/{project_id}/custom_fields/{custom_field_id}/position/{position}.json?key=1234 <<<'{ "possible_value" : "bbb" }'
```

プロジェクトやカスタムフィールドが存在しない場合には404 Not Foundを返します。

パラメータに不備があった場合には以下のエラーを返します。

```bash
HTTP/1.1 422 Unprocessable Entity
...
    "errors": [
        "List item position is not in range."
    ]
...
    "errors": [
        "List item can not be empty."
    ]
```

### [削除] 指定した位置の「プロジェクトごとのリスト」の値を削除

`{position}`番目の「プロジェクトごとのリスト」の値を更新します。

```bash
> http DELETE localhost:3000/projects/{project_id}/custom_fields/{custom_field_id}/position/{position}.json?key=1234
...
```

すべての「プロジェクトごとのリスト」が削除されたカスタムフィールドには、Redmine本体で設定された選択肢が再設定されます。

プロジェクトやカスタムフィールドが存在しない場合には404 Not Foundを返します。

パラメータに不備があった場合には422 Unprocessable Entityを返します。

### [削除] 「プロジェクトごとのリスト」の値をすべて削除する

Redmine本体で設定された選択肢が再設定されます。

成功した場合には200 OKを返します。失敗した場合には404 Not Foundを返します。

```bash
> http DELETE localhost:3000/projects/{project_id}/custom_fields/{custom_field_id}.json?key=1234
HTTP/1.1 200 OK
...
HTTP/1.1 404 Not Found
```
