namespace :redmine do
  namespace :plugins do
    namespace :lychee_custom_field do
      desc 'Copy data from lychee_smart_custom_field'
      task copy_data_from_lychee_smart_custom_field: :environment do
        module LycheeSmartCustomField
          class CustomFieldCategory < ActiveRecord::Base
            self.table_name = :custom_field_categories
            has_many :custom_field_category_relations, dependent: :destroy
          end
          class CustomFieldCategoryRelation < ActiveRecord::Base
            self.table_name = :custom_field_category_relations
            belongs_to :custom_field
            belongs_to :custom_field_category
          end
          class RequiredCustomField < ActiveRecord::Base
            self.table_name = :required_custom_fields
          end
          class InvisibleCustomFieldEnumeration < ActiveRecord::Base
            self.table_name = :invisible_custom_field_enumerations
          end

          class << self
            def errors
              @errors ||= ActiveModel::Errors.new(self)
            end

            def save_with_error_messages(object)
              if object.new_record?
                object.save!
              else
                errors.add(:base, I18n.t('task.messages.already_exists', record: object.to_json))
              end
            end
          end
        end

        ActiveRecord::Base.transaction do
          %w[
            CustomFieldCategory
            RequiredCustomField
            InvisibleCustomFieldEnumeration
          ].each do |klass|
            next unless "LycheeSmartCustomField::#{klass}".constantize.table_exists?
            "LycheeSmartCustomField::#{klass}".constantize.find_each do |smart_custom_field|
              "Lychee::#{klass}".constantize.find_or_initialize_by(smart_custom_field.as_json.except('id')).tap do |object|
                LycheeSmartCustomField.save_with_error_messages(object)
                next unless smart_custom_field.is_a?(LycheeSmartCustomField::CustomFieldCategory)
                smart_custom_field.custom_field_category_relations.each do |cf_category_relation|
                  next if cf_category_relation.custom_field.blank?
                  LycheeSmartCustomField.save_with_error_messages(
                    Lychee::CustomFieldCategoryRelation.find_or_initialize_by(
                      lychee_custom_field_category_id: object.id,
                      custom_field: cf_category_relation.custom_field
                    )
                  )
                end
              end
            end
          end
        end

        if LycheeSmartCustomField.errors.count > 0
          puts "#{LycheeSmartCustomField.errors.count} errors detected"
          puts LycheeSmartCustomField.errors.full_messages
        else
          puts I18n.t('task.messages.successfully_saved')
        end
      end
    end
  end
end
