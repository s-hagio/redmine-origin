namespace :redmine do
  namespace :plugins do
    namespace :lychee_custom_field do
      namespace :remove_extra_data do
        desc 'CustomFieldEnumeration テーブルに存在しない custom_field_enumeration_id を持っている InvisibleCustomFieldEnumeration を削除する'
        task invisible_custom_field: :environment do
          custom_field_enumeration_ids = CustomFieldEnumeration.pluck(:id)
          source_custom_field_enumeration_ids = Lychee::InvisibleCustomFieldEnumeration.group(:source_custom_field_enumeration_id).pluck(:source_custom_field_enumeration_id)
          source_custom_field_enumeration_ids.each do |e_id|
            Lychee::InvisibleCustomFieldEnumeration.where(source_custom_field_enumeration_id: e_id).delete_all  unless custom_field_enumeration_ids.include?(e_id)
          end
          target_custom_field_enumeration_ids = Lychee::InvisibleCustomFieldEnumeration.group(:target_custom_field_enumeration_id).pluck(:target_custom_field_enumeration_id)
          target_custom_field_enumeration_ids.each do |e_id|
            Lychee::InvisibleCustomFieldEnumeration.where(target_custom_field_enumeration_id: e_id).delete_all  unless custom_field_enumeration_ids.include?(e_id)
          end
        end

        desc 'CustomField テーブルに存在しない custom_field_id を持っている RequiredCustomField を削除する'
        task required_custom_field: :environment do
          custom_field_ids = CustomField.pluck(:id)
          required_cf_ids = Lychee::RequiredCustomField.group(:custom_field_id).pluck(:custom_field_id)
          required_cf_ids.each do |cf_id|
            Lychee::RequiredCustomField.where(custom_field_id: cf_id).delete_all unless custom_field_ids.include?(cf_id)
          end
        end
      end
    end
  end
end
