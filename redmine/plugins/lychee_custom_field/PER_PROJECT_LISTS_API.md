# Per Project Lists API

> **Warning!** API will not create custom fields or make them 'smart', it only
> works with **already existing smart custom fields**.

This API provides access and basic CRUD operations (create, read, update, delete) for the per project *list* or *key-value* type of smart custom fields. The API supports both XML and JSON formats - add *.json* or *.xml* to the URLs.

## Authentication

To enable the API-style authentication, you have to check **Enable REST API** in *Administration -> Settings -> API.*
Then, authentication can be done in 2 different ways:

 * using your regular login/password via **HTTP Basic authentication**.
 * using your **API key** which is a handy way to avoid putting a password in a script.

The API key may be attached to each request in one of the following way:

 * passed in as a *"key"* parameter
 * passed in as a *username* with a random password via HTTP Basic authentication
 * passed in as a *"X-Redmine-API-Key"* HTTP header

You can find your **API key** on your account page ( */my/account* ) when logged in, on the right-hand pane of the default layout.

## Examples

Examples below are made with [HTTPie](https://httpie.org/).

### [READ] List all project-lists

```bash
> http localhost:3000/projects/1/custom_fields.json?key=1234
{
    "per_project_api": [
        {
            "custom_field": {
                "id": 1,
                "name": "ppr_list"
                "format": "list",
            },
            "project": {
                "id": 1,
                "identifier": "lcf-tests"
            }
        },
        ...
    ]
}
```

Returns 404 error if project is not found.

### [READ] Show specified per-project list possible values (with indexes)

Show values and indexes, which can be used for delete value on index operations.

* Existing list

```bash
> http localhost:3000/projects/1/custom_fields/1.json?key=1234
{
    "per_project_api": {
        "project": {
            "id": 1,
            "identifier": "lcf-tests"
        },
        "custom_field": {
            "id": 1,
            "name": "ppr_list"
            "format": "list",
            "is_required": false,
            "default_value": "",
            "available_values": [
                { "name": "a", "position": 1 },
                { "name": "b", "position": 2 },
                { "name": "c", "position": 3 }
            ]
        }
    }
}
```

Output is the exact same for the Key-Value type of fields:


```bash
> http localhost:3000/projects/1/custom_fields/2.json?key=1234
{
    "per_project_api": {
        "project": {
            "id": 1,
            "identifier": "lcf-tests"
        },
        "custom_field": {
            "id": 2,
            "name": "ppr_kv"
            "format": "enumeration",
            ...
            "available_values": [
                { "name": "aaa", "position": 1 },
                { "name": "bbb", "position": 2 }
            ]
        }
    }
}
```

* Non-existing list - 404 error, empty body

```bash
> http localhost:3000/projects/1/custom_fields.json?key=1234
HTTP/1.1 404 Not Found
...
```

### [CREATE] Create new project-related list

Project and custom field should **exist already**.
Will create list if not exist or return the existing one. Output format is the same as show API call above.
Return 404 with empty body if project or custom field does not exist.

```bash
> http POST localhost:3000/projects/1/custom_fields/1.json?key=1234
...
HTTP/1.1 404 Not Found
```

### [UPDATE] Add new value to a specified list

> **possible_value** parameter is required.

New value will be added to the **end of the list**.
If operation is successful output format is the same as show API call above.
Return 404 error if project or custom field does not exists.

```bash
> http PUT localhost:3000/projects/1/custom_fields/1.json?key=1234 <<<'{ "possible_value" : "aaa" }'
...
HTTP/1.1 404 Not Found
```

Wiil display an error on empty possible value:

```bash
HTTP/1.1 422 Unprocessable Entity
...
    "errors": [
        "List item can not be empty."
    ]
```

### [UPDATE] Update value at a specified position in the list

> **possible_value** parameter is required.

Will update element on position 2 in the list of available values.
If operation is successful output format is the same as show API call above.
Return 404 error if project or custom field does not exists.

```bash
> http PUT localhost:3000/projects/1/custom_fields/1/position/2.json?key=1234 <<<'{ "possible_value" : "bbb" }'
...
HTTP/1.1 404 Not Found
```

Wiil display an error on empty possible value or invalid position:

```bash
HTTP/1.1 422 Unprocessable Entity
...
    "errors": [
        "List item position is not in range."
    ]
...
    "errors": [
        "List item can not be empty."
    ]
```

### [DELETE] Remove value at a specified position in the list

Will delete element on position 2 in the list of available values:

```bash
> http DELETE localhost:3000/projects/1/custom_fields/1/position/2.json?key=1234
...
```

If operation is successful output format is the same as show API call above.
Return 404 error if project or custom field does not exists.

```bash
> http DELETE localhost:3000/projects/1/custom_fields/1/position/0.json?key=1234
...
HTTP/1.1 404 Not Found
```

Will display same error messages as for the update value at position API call above for invalid position or empty value.

### [DELETE] Remove the whole list

Return 200 code with empty body if the list exists, else 404 error.

```bash
> http DELETE localhost:3000/projects/1/custom_fields/1.json?key=1234
HTTP/1.1 200 OK
...
HTTP/1.1 404 Not Found
```
