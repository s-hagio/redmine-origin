!(function($) {
  'use strict';

  var rExtractErrorExplanation = /errorExplanation.+?>(.+)<[\\]?\/div/;

  /**
   * Lac.Relatable
   *
   * @constructor
   */
  function Relatable(options) {
    options = options || {};
    this.activeFromClass = options.activeFromClass || 'lac-relatable-from';
    this.activeToClass = options.activeToClass || 'lac-relatable-to';
  }

  /**
   * Alias of prototype
   */
  Relatable.fn = Relatable.prototype;

  /**
   * Start
   */
  Relatable.fn.start = function(selector) {
    var $doc = $(document);

    this.selector = selector || '.lac-chart-item';

    $doc.on('contextmenu', this.selector, $.proxy(this, 'onContextMenu'));

    $doc.on(
      'click',
      '.lac-relatable-types[data-edit="1"] li',
      $.proxy(this, 'onChooseRelationType')
    );
    $doc.on('click', '.lac-relatable-items a', function(event) {
      event.preventDefault();
    });
  };

  /**
   * On context menu
   *
   * @param {Event} event
   */
  Relatable.fn.onContextMenu = function(event) {
    event.preventDefault();
    this.activate(event.currentTarget);

    if (!this.activated) {
      return;
    }

    var issueQueryForm = $('<div class="lac-unrelated-issues"></div>');
    this.openModal(issueQueryForm, 900);
    Le.ajax({ url: Le.url('unrelatedIssues', {}) });
  };

  /**
   * On choose relation type
   *
   * @param {Event} event
   */
  Relatable.fn.onChooseRelationType = function(event) {
    if (event.target.tagName.toLowerCase() !== 'li') {
      return;
    }

    var delay,
      $el = $(event.currentTarget),
      relationType = $el.attr('data-type');

    if (!relationType) {
      this.deactivate();
      return;
    }

    if (relationType === 'child') {
      this.saveParental();
      return;
    }

    if (relationType === 'precedes') {
      delay = $el.find('input').val();
    }

    this.saveRelation(relationType, delay);
  };

  /**
   * Activate
   *
   * @param {HTMLElement} el
   */
  Relatable.fn.activate = function(el) {
    if (this.activated) {
      return;
    }

    var $el = $(el);

    if (!$el.data('relatable')) {
      return;
    }

    this.$from = $el;
    this.$from.addClass(this.activeFromClass);

    this.activated = true;
  };

  /**
   * Deactivate
   */
  Relatable.fn.deactivate = function() {
    if (!this.activated) {
      return;
    }

    if (this.$from) {
      this.$from.removeClass(this.activeFromClass);
      this.$from = null;
    }

    this.destroyToElement();
    this.closeModal();

    this.activated = false;
  };

  /**
   * Open modal
   * target: Element that should be put on target side of modal
   * width: overwrite width in pixels for modal
   */
  Relatable.fn.openModal = function(target, width) {
    var $content = Le.$template('relatable-modal', {});

    // Set which of the onChooseRelationType functions shall be used
    $content.filter('.lac-relatable-types').attr('data-edit', '1');

    $content
      .filter('.lac-relatable-items')
      .append(this.cloneItemForModal(this.$from), target);

    Lac.modal.content($content, width);
    Lac.modal.open();
    this.isSelecting = true;
  };

  /**
   * Clone issue and remove unnecessary element for relatable modal.
   */
  Relatable.fn.cloneItemForModal = function(el) {
    var clone = el.clone().removeAttr('data-relatable');
    clone.find('.lac-editable-toggle').remove();
    clone.find('.lac-create-issue').remove();
    return clone;
  };

  /**
   * Close modal
   */
  Relatable.fn.closeModal = function() {
    Lac.modal.content('');
    Lac.modal.close();
    this.isSelecting = false;
    this.destroyToElement();
  };

  /**
   * Save parental
   *
   * Lac.refresh
   */
  Relatable.fn.saveParental = function() {
    var data = {
      ids: [this.$to.data('id')],
      issue: {
        parent_issue_id: this.$from.data('id'),
      },
    };

    this.deactivate();
    Le.bulkUpdate(data)
      .done($.proxy(this, 'flashResultMessageOfRelation'))
      .done(Lac.refresh);
  };

  /**
   * Save
   *
   * @param {String} type
   * @param {Number} [delay]
   */
  Relatable.fn.saveRelation = function(type, delay) {
    this.save({
      url: Le.url('issueRelations', { issue_id: this.$from.data('id') }),
      data: {
        relation: {
          relation_type: type,
          issue_to_id: this.$to.data('id'),
          delay: delay,
        },
      },
    }).done($.proxy(this, 'flashResultMessageOfRelation'));
  };

  Relatable.fn.save = function(params) {
    if (!params.type) {
      params.type = 'POST';
    }

    this.deactivate();

    var deferred = Le.ajax(params);
    deferred.done(Lac.refresh);
    return deferred;
  };

  /**
   * Flash result message of Parental
   *
   * @param {String} html
   */
  Relatable.fn.flashResultMessageOfParental = function(html) {
    var $el = $($.parseHTML(html)).filter('#flash_error');

    if ($el.length) {
      Le.flash('error', $el.text());
    } else {
      Le.flash('notice', Lac.labels.message.parent_child_success);
    }
  };

  /**
   * Flash result message of Relation
   *
   * @param {String} js
   */
  Relatable.fn.flashResultMessageOfRelation = function(js) {
    var $el,
      error = rExtractErrorExplanation.exec(js);

    if (!error) {
      Le.flash('notice', Lac.labels.message.association_success);
      return;
    }

    $el = $(document.createElement('div'));
    $el.html(error[1].replace(/\\\//g, '/'));

    Le.flash('error', $el.find('li').text());
  };

  Relatable.fn.destroyToElement = function() {
    if (this.$to) {
      this.$to.removeClass(this.activeToClass);
      this.$to = null;
    }
  };

  Lac.Relatable = Relatable;
})(jQuery);
