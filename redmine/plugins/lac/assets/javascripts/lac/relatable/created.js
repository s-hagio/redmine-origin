!(function($) {
  'use strict';

  var rExtractErrorExplanation = /errorExplanation.+?>(.+)<[\\]?\/div/;

  /**
   * Lac.Relatable.Created
   *
   * @constructor
   */
  function Created(options) {
    options = options || {};
    this.activeFromClass = options.activeFromClass || 'lac-relatable-from';
  }

  /**
   * Alias of prototype
   */
  Created.fn = Created.prototype;

  /**
   * Add creation of relatable capability.
   *
   * @param {String} createSelector
   * @param {String} itemSelector
   */
  Created.fn.start = function(createSelector, itemSelector) {
    this.itemSelector = itemSelector;

    var $doc = $(document);

    $doc.on('click', createSelector, $.proxy(this, 'onCreateIssue'));
    $doc.on(
      'click',
      '.lac-relatable-types[data-create="1"] li',
      $.proxy(this, 'onChooseRelationType')
    );
  };

  Created.fn.onCreateIssue = function(event) {
    var $el = $(event.currentTarget).parents(this.itemSelector);

    this.$from = $el;
    this.$from.addClass(this.activeFromClass);
    this.openModal();
  };

  /**
   * Open modal
   */
  Created.fn.openModal = function() {
    var that = this,
      $content = Le.$template('relatable-modal', {});

    // Set which of the onChooseRelationType functions shall be used
    $content.filter('.lac-relatable-types').attr('data-create', '1');

    Le.ajax({
      url: Le.url('newIssue', { id: this.$from.data('id') }),
      success: function(json) {
        if (json.success) {
          that.$to = Le.$template('new-issue-editor', json);
          that.setEditorFieldVisibility(that.$to, json);

          $content
            .filter('.lac-relatable-items')
            .append(that.cloneItemForModal(that.$from), that.$to);
          var args = $.extend({}, datepickerOptions, {
            showWeek: false,
            beforeShow: function() {
              setTimeout(function() {
                $('#ui-datepicker-div').css('zIndex', 1100);
              }, 0);
            },
          });
          // From version 3.3 Redmine uses new HTML5 date input fields instead of the jQuery UI Datepicker
          if (Le.REDMINE_VERSION >= '3.3')
            that.$to.find('.lac-datePicker').datepickerFallback(args);
          else that.$to.find('.lac-datePicker').datepicker(args);
        } else {
          Le.flash('error', json.error);
        }
      },
    });

    Lac.modal.content($content);
    Lac.modal.open();
  };

  Created.fn.setEditorFieldVisibility = function(root_el, json) {
    for (var i = 0, item; (item = json.all_field_info[i]); i++) {
      var field_info = item.field_info;
      var el = root_el.find('#lac-chart-' + field_info.id);
      field_info.is_visible ? el.show() : el.hide();
    }
  };

  Created.fn.onTrackerChanged = function(select) {
    var that = this;

    Le.ajax({
      url: Le.url('newIssue'),
      data: {
        id: this.$from.data('id'),
        tracker_id: select.options[select.selectedIndex].value,
      },
      success: function(json) {
        if (json.success) {
          that.setEditorFieldVisibility(that.$to, json);
        }
      },
    });
  };

  /**
   * Clone issue and remove unnecessary element for relatable modal.
   */
  Created.fn.cloneItemForModal = function(el) {
    var clone = el.clone().removeAttr('data-relatable');
    clone.find('.lac-editable-toggle').remove();
    clone.find('.lac-create-issue').remove();
    return clone;
  };

  /**
   * Close modal
   */
  Created.fn.closeModal = function() {
    Lac.modal.content('');
    Lac.modal.close();

    this.$to = null;
    if (this.$from) {
      this.$from.removeClass(this.activeFromClass);
      this.$from = null;
    }
  };

  /**
   * On choose relation type
   *
   * @param {Event} event
   */
  Created.fn.onChooseRelationType = function(event) {
    if (event.target.tagName.toLowerCase() !== 'li') {
      return;
    }

    var that = this,
      $el = $(event.currentTarget),
      relationType = $el.attr('data-type');

    if (!relationType) {
      this.closeModal();
      return;
    }

    this.saveCreated().done(function(json) {
      if (json.errors) {
        Le.flash('error', json.errors);
      } else {
        that.$to.data('id', json.id);
        that
          .relationRequest($el, relationType)
          .done(Lac.refresh)
          .done(that.closeModal());
      }
    });
  };

  Created.fn.saveCreated = function() {
    var that = this,
      data = { id: this.$from.data('id'), issue: {} };

    $.each(this.$to.find('form').serializeArray(), function(i, object) {
      if (object.name.indexOf('[]') != -1) {
        var key = object.name.slice(0, -2);
        if (data.issue[key]) {
          data.issue[key].push(object.value);
        } else {
          data.issue[key] = [object.value];
        }
      } else {
        data.issue[object.name] = object.value;
      }
    });

    return Le.bulkUpdate(data, {
      type: 'POST',
      url: Le.url('createIssue'),
    });
  };

  Created.fn.relationRequest = function($el, relationType) {
    var delay;

    if (relationType === 'child') {
      return this.saveParental();
    }

    if (relationType === 'precedes') {
      delay = $el.find('input').val();
    }

    return this.saveRelation(relationType, delay);
  };

  /**
   * Save parental
   *
   * Lac.refresh
   */
  Created.fn.saveParental = function() {
    var data = {
      ids: [this.$to.data('id')],
      issue: {
        parent_issue_id: this.$from.data('id'),
      },
    };

    return Le.bulkUpdate(data).done(
      $.proxy(this, 'flashResultMessageOfRelation')
    );
  };

  /**
   * Save relation
   *
   * @param {String} type
   * @param {Number} [delay]
   */
  Created.fn.saveRelation = function(type, delay) {
    return Le.ajax({
      type: 'POST',
      url: Le.url('issueRelations', { issue_id: this.$from.data('id') }),
      data: {
        relation: {
          relation_type: type,
          issue_to_id: this.$to.data('id'),
          delay: delay,
        },
      },
    }).done($.proxy(this, 'flashResultMessageOfRelation'));
  };

  /**
   * Flash result message of Parental
   *
   * @param {String} html
   */
  Created.fn.flashResultMessageOfParental = function(html) {
    var $el = $($.parseHTML(html)).filter('#flash_error');

    if ($el.length) {
      Le.flash('error', $el.text());
    } else {
      Le.flash('notice', Lac.labels.message.parent_child_success);
    }
  };

  /**
   * Flash result message of Relation
   *
   * @param {String} js
   */
  Created.fn.flashResultMessageOfRelation = function(js) {
    var $el,
      error = rExtractErrorExplanation.exec(js);

    if (!error) {
      Le.flash('notice', Lac.labels.message.association_success);
      return;
    }

    $el = $(document.createElement('div'));
    $el.html(error[1].replace(/\\\//g, '/'));

    Le.flash('error', $el.find('li').text());
  };

  Lac.Relatable.Created = Created;
})(jQuery);
