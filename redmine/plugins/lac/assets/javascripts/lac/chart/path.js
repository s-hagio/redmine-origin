!function($) {
  'use strict';

  var ROOT_SELECTOR = '> ul > li > .lac-chart-item',
      CHILDREN_SELECTOR = '~ ul > li > .lac-chart-item',
      RELATIONS_SELECTOR = '~ .lac-chart-relations > .lac-chart-item',
      CHANGESET_SELECTOR = RELATIONS_SELECTOR + '[data-type="changeset"]',
      DEFAULT_COLOR = '#999';

  /**
   * Lac.Chart.Path
   *
   * @param {Object} options
   * @constructor
   */
  function Path(options) {
    options = options || Lac;
    this.$chart = options.$chart;
    this.$paper = options.$paper;
    this.paper = options.paper;
    this.rootSelector = options.rootSelector || ROOT_SELECTOR;
    this.childrenSelector = options.childrenSelector || CHILDREN_SELECTOR;
    this.changeSetSelector = options.changeSetSelector || CHANGESET_SELECTOR;
    this.relationsSelector = options.relationsSelector || RELATIONS_SELECTOR;
    this.curve = options.curve || 20;
  }

  /**
   * Alias of prototype
   */
  Path.fn = Path.prototype;

  /**
   * Mixin
   */
  $.extend(Path.fn, Lac.Events);

  /**
   * Reset the paper
   */
  Path.fn.reset = function() {
    this.$chart.css({height: 'auto'});
    this.$paper.hide();

    var $parent = this.$chart.parent(),
        rect = {
          width: $parent.prop('scrollWidth'),
          height: $parent.prop('scrollHeight')
        };

    this.$chart.height(rect.height);
    this.$paper.css(rect);
    this.$paper.show();
    this.paper.clear();
    this.paper.setSize(rect.width, rect.height);
  };

  /**
   * Draw all path
   */
  Path.fn.draw = function() {
    this.reset();
    this.drawChildren();
    this.drawChangeSet();
    this.drawRelations();
  };

  /**
   * Draw children
   *
   * @param {jQuery} [$parent]
   */
  Path.fn.drawChildren = function($parent) {
    if (!$parent) {
      $parent = this.$chart.find(this.rootSelector);
    }

    var that = this,
        curve = this.curve,
        fromRect = $parent.rect(),
        fromX = fromRect.left + 39,
        fromX2 = fromX + curve;

    $parent.find(this.childrenSelector).each(function() {
      var $child = $(this),
          rect = $child.rect(),
          y = rect.middle,
          y2 = y - curve,
          path = that.add([
            'M' + fromX + ',' + fromRect.bottom,
            'L' + fromX + ',' + y2,
            'M' + fromX + ',' + y2,
            'Q' + fromX + ',' + y + ' ' + fromX2 + ',' + y,
            'L' + rect.left + ',' + y
          ].join(' '));

      that.proxyEvents(path, {
        type: 'parental',
        id: $child.data('id')
      });

      that.drawChildren($child);
    });
  };

  /**
   * Draw change set
   */
  Path.fn.drawChangeSet = function() {
    var that = this,
        selector = this.changeSetSelector;

    this.$chart.find('.lac-chart-item').each(function() {
      var fromRect,
          $from = $(this),
          $changeSets = $from.find(selector);

      if (!$changeSets.length) {
        return;
      }

      fromRect = $from.rect();

      $changeSets.each(function() {
        var c = Path.makeCoordinate(fromRect, $(this).rect());
        that.add('M' + c.from.join(',') + ' L' + c.to.join(','));
      });
    });
  };

  /**
   * Draw relations
   */
  Path.fn.drawRelations = function() {
    var that = this,
        selector = this.relationsSelector;

    this.$chart.find('[data-rels]').each(function() {
      var $el = $(this),
          fromRect = $el.rect(),
          $targets = $el.find(selector);

      $.each($el.data('rels'), function(i, object) {
        var $target = $targets.filter('[data-id="' + object.id + '"]'),
            c = Path.makeCoordinate(fromRect, $target.rect()),
            options = { color: '#ACACAC' },
            path = that.add('M' + c.from.join(',') + ' L' + c.to.join(','), options),
            type = object.relation_type;

        path.attr('stroke-dasharray', '- ');

        if (type === 'precedes' || type === 'blocks') {
          that.addArrow(c, options);
        }

        if (type === 'follows' || type === 'blocked') {
          that.addArrow({
            from: c.to,
            to: c.from
          }, options);
        }

        that.proxyEvents(path, {
          type: object.relation_type,
          url: object.relation_url
        });
      });
    });
  };

  /**
   * Add the path
   *
   * @param {String} pathString
   * @param {Object} [options]
   * @return {Raphael.Path}
   */
  Path.fn.add = function(pathString, options) {
    if (!options) {
      options = {};
    }

    var path = this.paper.path(pathString);

    path.attr('stroke-width', 2);
    path.attr('stroke', options.color || DEFAULT_COLOR);

    return path;
  };

  /**
   * Arrow
   *
   * @param {{from: Array, to: Array}} coordinate
   * @param {Object} [options]
   */
  Path.fn.addArrow = function(coordinate, options) {
    var x1 = coordinate.from[0],
        x2 = coordinate.to[0],
        y1 = coordinate.from[1],
        y2 = coordinate.to[1],
        sizeX = 18,
        sizeY = 9,
        angle = (Math.atan2(x1-x2, y2-y1) / (2 * Math.PI)) * 360;

    if (!options) {
      options = {};
    }

    x2 += (x1 - x2) / 2.5;
    y2 += (y1 - y2) / 2.5;

    this.paper.path([
      'M' + x2 + ',' + y2,
      'L' + (x2 - sizeX) + ',' + (y2 - sizeY),
      'L' + (x2 - sizeX) + ',' + (y2 + sizeY),
      'L' + x2 + ',' + y2
    ].join(' ')).attr({
      fill: options.color || DEFAULT_COLOR,
      stroke: null
    }).rotate(90 + angle, x2, y2);
  };

  /**
   * Proxy events
   *
   * @param {Raphael.Path} path
   * @param {Object} relation
   */
  Path.fn.proxyEvents = function(path, relation) {
    var that = this;

    path.relation = relation;

    path.hover(function(event) {
      event.originalContext = this;
      that.trigger('mouseenter', event);
    }, function(event) {
      event.originalContext = this;
      that.trigger('mouseleave', event);
    });

    path.click(function(event) {
      event.originalContext = this;
      that.trigger('click', event);
    });
  };

  /**
   * Make path coordinate
   *
   * @param {Object} fromRect
   * @param {Object} toRect
   * @returns {{from: Array, to: Array}}
   */
  Path.makeCoordinate = function(fromRect, toRect) {
    var fromPath = [fromRect.left, fromRect.top],
        toPath = [toRect.left, toRect.top];

    if (fromRect.left === toRect.left) {
      fromPath[0] = fromRect.center;
      toPath[0] = toRect.center;

      if (fromRect.top < toRect.top) {
        fromPath[1] = fromRect.bottom;
      }

    } else {
      fromPath[1] = fromRect.middle;
      toPath[1] = toRect.middle;

      if (fromRect.left < toRect.left) {
        fromPath[0] = fromRect.right;

      } else if (fromRect.left > toRect.left) {
        toPath[0] = toRect.right;
      }
    }

    return {
      from: fromPath,
      to: toPath
    }
  };

  Lac.Chart.Path = Path;
}(jQuery);