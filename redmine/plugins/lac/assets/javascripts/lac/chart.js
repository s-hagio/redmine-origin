!(function($) {
  'use strict';

  /**
   * Lac.Chart
   *
   * @param {Object} [options]
   * @constructor
   */
  function Chart(options) {
    options = options || Lac;
    this.$chart = options.$chart;
    this.$paper = options.$paper;
    this.paper = options.paper;

    this.path = new this.constructor.Path(options);
    this.bindPathEvents();
  }

  /**
   * Alias of prototype
   * @type {Object}
   */
  Chart.fn = Chart.prototype;

  /**
   * Bind path events
   */
  Chart.fn.bindPathEvents = function() {
    this.path.on('mouseenter', $.proxy(this, 'onEnterPath'));
    this.path.on('mouseleave', $.proxy(this, 'onLeavePath'));
    this.path.on('click', $.proxy(this, 'onClickPath'));
  };

  /**
   * Load
   */
  Chart.fn.load = function() {
    this._loader = Le.ajax({
      url: Le.url('chart'),
      data: this.filterParams(),
    });
    return this._loader.done($.proxy(this, 'onLoaded'));
  };

  /**
   * Filter params
   *
   * @returns {Object}
   */
  Chart.fn.filterParams = function() {
    var $input = $('#query_form_content').find('input[type="checkbox"]'),
      params = {};

    $input.each(function() {
      if (this.checked) {
        params[this.name] = 1;
      }
    });

    $('#query_form_content')
      .find('select')
      .each(function() {
        params[this.name] = $(this).val();
      });

    return params;
  };

  /**
   * On loaded
   *
   * @param {Object} json
   */
  Chart.fn.onLoaded = function(json) {
    this.prepare(json);
    this.render();
  };

  /**
   * Prepare
   *
   * @param {Object} data
   */
  Chart.fn.prepare = function(data) {
    this.data = data;
    this.data._html_class = 'lac-current';
  };

  /**
   * Render
   */
  Chart.fn.render = function() {
    this.$chart.html(
      this.renderParent(this.data.parent, Le.template('chart', this.data))
    );
    this.drawRelationLine();
  };

  /**
   * Render parent
   *
   * @param {Object} data
   * @param {String|jQuery} childHTML
   * @return {String}
   */
  Chart.fn.renderParent = function(data, childHTML) {
    if (!data) {
      return childHTML;
    }

    var $el = Le.$template('chart', data),
      $ul = $el.find('li ul'),
      $child = $(
        typeof childHTML === 'string' ? $.parseHTML(childHTML) : childHTML
      );

    if ($ul.length) {
      $ul.prepend($child.find('> li'));
    } else {
      $el.find('> li').append($child);
    }

    return this.renderParent(data.parent, $el);
  };

  /**
   * Reset relation line
   */
  Chart.fn.drawRelationLine = function() {
    this.path.draw();
  };

  /**
   * Unlink parent
   *
   * @param {Object} data
   */
  Chart.fn.unlinkParent = function(data) {
    var deferred = Le.ajax({
      url: Le.url('bulkUpdate'),
      type: 'POST',
      data: {
        ids: [data.id],
        issue: {
          parent_issue_id: 'none',
        },
      },
    });

    deferred.done(this.flashMessageWithHTML);
    deferred.done(Lac.refresh);
  };

  /**
   * Unlink relation
   *
   * @param {Object} data
   */
  Chart.fn.unlinkRelation = function(data) {
    var deferred = Le.ajax({
      url: data.url,
      type: 'DELETE',
      dataType: 'script',
    });
    deferred.done(Lac.refresh);
  };

  /**
   * On mouse enter the path
   *
   * @param {MouseEvent} event
   */
  Chart.fn.onEnterPath = function(event) {
    var path = event.originalContext;
    this._originalDash = path.attr('stroke-dasharray');
    path.attr('stroke-width', 6);
    path.attr('stroke-dasharray', 'none');
  };

  /**
   * On mouse leave the path
   *
   * @param {MouseEvent} event
   */
  Chart.fn.onLeavePath = function(event) {
    var path = event.originalContext;
    path.attr('stroke-width', 2);
    path.attr('stroke-dasharray', this._originalDash);
  };

  /**
   * On click the path
   *
   * @param {MouseEvent} event
   */
  Chart.fn.onClickPath = function(event) {
    var path = event.originalContext;

    if (!confirm(Lac.labels.message.delete_path_confirm)) {
      return;
    }

    if (path.relation.type === 'parental') {
      this.unlinkParent(path.relation);
    } else {
      this.unlinkRelation(path.relation);
    }
  };

  /**
   * Flash message with HTML
   *
   * @param {String} html
   */
  Chart.fn.flashMessageWithHTML = function(html) {
    var $el = $($.parseHTML(html)).filter('#flash_error');

    if ($el.length) {
      Le.flash('error', $el.text());
    }
  };

  Lac.Chart = Chart;
})(jQuery);
