!function($) {
  'use strict';

  var Events = {},
      slice = Array.prototype.slice;

  /**
   * Add event listener
   *
   * @param name
   * @param listener
   * @param context
   */
  Events.on = function(name, listener, context) {
    if (!this._events) {
      this._events = {};
    }

    if (!this._events[name]) {
      this._events[name] = [];
    }

    this._events[name].push({
      listener: listener,
      context: context || this
    });
  };

  /**
   * Remove event listener
   *
   * @param {String} name
   * @param {Function} listener
   * @param {Object} [context]
   */
  Events.off = function(name, listener, context) {
    if (!this._events || !this._events[name]) {
      return;
    }

    if (!listener) {
      this._events[name] = [];
      return;
    }

    context = context || this;

    this._events[name] = $.map(this._events[name], function(object) {
      if (object.listener !== listener && object.context !== context) {
        return object;
      }
    });
  };

  /**
   * Once event listener
   *
   * @param {String} name
   * @param {Function} listener
   * @param {Object} [context]
   */
  Events.once = function(name, listener, context) {
    var that = this;

    this.on(name, function once() {
      listener.apply(this, arguments);
      that.off(name, once, context);
    }, context);
  };

  /**
   * Trigger event
   *
   * @param {String} name
   */
  Events.trigger = function(name) {
    var args = slice.call(arguments, 1);

    $.each(this._events[name], function() {
      this.listener.apply(this.context, args);
    });
  };

  Lac.Events = Events;
}(jQuery);