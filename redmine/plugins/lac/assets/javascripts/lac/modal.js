!function($) {
  'use strict';

  /**
   * Lac.Modal
   *
   * @param {HTMLElement} el
   * @constructor
   */
  function Modal(el) {
    this.$el = $(el);
    this.$content = this.$el.find('.lac-modal-content-body');
  }

  /**
   * Alias of prototype
   */
  Modal.fn = Modal.prototype;

  /**
   * Content
   *
   * @param {String|HTMLElement} html
   */
  Modal.fn.content = function(html, width) {
    if (width)
      this.$content.css("width", width);
    this.$content.html(html);
  };

  /**
   * Open
   */
  Modal.fn.open = function() {
    this.showButtons();
    this.$el.show();
  };

  /**
   * Close
   */
  Modal.fn.close = function() {
    this.$el.hide();
    this.$content.css("width", "");
  };

  Modal.fn.showButtons = function() {
    this.$el.find('li[data-type="child"]').show();
    this.$el.find('li[data-type="relates"]').show();
    this.$el.find('li[data-type="blocks"]').show();
    this.$el.find('li[data-type="precedes"]').show();
  }

  Modal.fn.hideButtons = function() {
    this.$el.find('li[data-type="child"]').hide();
    this.$el.find('li[data-type="relates"]').hide();
    this.$el.find('li[data-type="blocks"]').hide();
    this.$el.find('li[data-type="precedes"]').hide();
  }

  Lac.Modal = Modal;
}(jQuery);
