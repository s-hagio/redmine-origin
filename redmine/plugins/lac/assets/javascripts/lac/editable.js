!(function($) {
  'use strict';

  /**
   * Lac.Editable
   *
   * @param {HTMLElement} el
   * @constructor
   */
  function Editable(el) {
    this.$el = $(el);
    this.id = this.$el.data('id');
    this.editing = this.$el.attr('data-editing') === '1';
    this.$el.data(Editable.INSTANCE_KEY, this);
  }

  /**
   * Alias of Prototype
   */
  Editable.fn = Editable.prototype;

  /**
   * Set editing state
   *
   * @param {boolean} editing
   */
  Editable.fn.setEditing = function(editing) {
    this.editing = editing;
    this.$el.attr('data-editing', +this.editing);
  };

  /**
   * Re-Draw
   */
  Editable.fn.redraw = function() {
    Lac.chart.drawRelationLine();
  };

  /**
   * Toggle
   */
  Editable.fn.toggle = function() {
    if (this.editing) {
      this.exit();
    } else {
      this.start();
    }
  };

  /**
   * Start
   */
  Editable.fn.start = function() {
    if (this.editing) {
      return;
    }
    this.setEditing(true);
    this.load();
  };

  /**
   * Exit
   */
  Editable.fn.exit = function() {
    if (!this.editing) {
      return;
    }
    this.abort();
    this.setEditing(false);
    this.$item.show();
    this.$editor.remove();
    this.destroy();
  };

  /**
   * Load
   */
  Editable.fn.load = function() {
    this.loader = Le.ajax({
      url: Le.url('dataForEditor', { issue_id: this.id }),
    });
    this.loader.done($.proxy(this, 'render'));
  };

  /**
   * Render
   *
   * @param {Object} json
   */
  Editable.fn.render = function(json) {
    this.$item = this.$el.find('> a');
    this.$item.hide();
    this.$editor = Le.$template('chart-item-editor', json);
    this.$el.append(this.$editor);
    this.redraw();
    var args = $.extend({}, datepickerOptions, {
      showWeek: false,
      beforeShow: function() {
        setTimeout(function() {
          $('#ui-datepicker-div').css('zIndex', 100);
        }, 0);
      },
    });
    // From version 3.3 Redmine uses new HTML5 date input fields instead of the jQuery UI Datepicker
    if (Le.REDMINE_VERSION >= '3.3')
      this.$el.find('.lac-datePicker').datepickerFallback(args);
    else this.$el.find('.lac-datePicker').datepicker(args);
  };

  /**
   * Abort
   */
  Editable.fn.abort = function() {
    if (this.loader) {
      this.loader.abort();
      this.loader = null;
    }
  };

  /**
   * Save
   */
  Editable.fn.save = function() {
    var that = this,
      data = { ids: [this.id], issue: {} };

    $.each(this.$el.find('form').serializeArray(), function(i, object) {
      var value = object.value;

      if (value === '') {
        if (object.name.match(/^custom_field_values/)) {
          value = '__none__';
        } else {
          value = 'none';
        }
      }

      data.issue[object.name] = value;
    });

    Le.bulkUpdate(data).done(function() {
      Lac.refresh().done(function() {
        that.destroy();
        Le.flash('notice', Lac.labels.message.issues_update_success);
      });
    });
  };

  /**
   * Update binding elements
   */
  Editable.fn.updateBindingElements = function() {
    var key = 'data-editable-bind-name',
      $bindings = this.$el.find('[' + key + ']');

    this.$el.find('input,select').each(function() {
      var $binding = $bindings.filter('[' + key + '="' + this.name + '"]');

      if (this.tagName.toUpperCase() !== 'SELECT') {
        $binding.text(this.value);
        return;
      }

      var option = this.options[this.selectedIndex],
        $option = $(this).find('[value="' + option.value + '"]');

      $binding.text($option.eq(-1).text());
    });
  };

  /**
   * Destroy
   */
  Editable.fn.destroy = function() {
    this.constructor.destroy(this.$el);
    this.$el = null;
    this.id = null;
    this.loader = null;
    this.redraw();
  };

  /**
   * Instance key
   * @type {string}
   */
  Editable.INSTANCE_KEY = 'lac:instance';

  /**
   * Get editable
   *
   * @param {HTMLElement} el
   */
  Editable.get = function(el) {
    var $el = $(el),
      editable = $el.data(Editable.INSTANCE_KEY);

    if (!editable) {
      editable = new Editable(el);
      $el.data(Editable.INSTANCE_KEY, editable);
    }

    return editable;
  };

  /**
   * Destroy editable
   *
   * @param {HTMLElement} el
   */
  Editable.destroy = function(el) {
    $(el).removeData(Editable.INSTANCE_KEY);
  };

  /**
   * Lac editable
   *
   * @param {String} toggleSelector
   * @param {String} itemSelector
   * @returns {jQuery}
   */
  $.fn.lacEditable = function(toggleSelector, itemSelector) {
    this.on('click', toggleSelector, function(event) {
      var $el = $(event.currentTarget).parents(itemSelector);
      Editable.get($el).toggle();
    });

    this.on('submit', itemSelector + ' form', function(event) {
      event.preventDefault();
      var $el = $(event.currentTarget).parents(itemSelector);
      Editable.get($el).save();
    });

    return this;
  };

  Lac.Editable = Editable;
})(jQuery);
