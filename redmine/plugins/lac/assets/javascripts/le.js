var Le = (function($) {
  'use strict';

  var Le = {},
    rParams = /\:(\w+)/g,
    rTrim = /^[\n\s]+|[\n\s]+$/g;

  /**
   * URL
   *
   * @param {String} name
   * @param {Object} [params]
   * @returns {String}
   */
  Le.url = function(name, params) {
    params = params || {};
    return Le.url[name].replace(rParams, function($0, key) {
      return params[key] || null;
    });
  };

  /**
   * Define the URL
   *
   * @param {String} name
   * @param {String} url
   */
  Le.defineURL = function(name, url) {
    Le.url[name] = url;
  };

  /**
   * Template HTML
   *
   * @param {String} name
   * @param {String} [prefix="le"]
   * @return {String}
   */
  Le.templateHTML = function(name, prefix) {
    return $('#' + (prefix || 'le') + '-template-' + name).html();
  };

  /**
   * Template
   *
   * @param {String} name
   * @param {Object} params
   * @param {String} [prefix="le"]
   * @returns {String} Built HTML
   */
  Le.template = function(name, params, prefix) {
    var template = Le.template.cache[name];

    if (!template) {
      template = Handlebars.compile(Le.templateHTML(name, prefix));
      Le.template.cache[name] = template;
    }

    return template(params).replace(rTrim, '');
  };

  /**
   * Cache of template
   * @type {Object}
   */
  Le.template.cache = {};

  /**
   * jQuery Template
   *
   * @param {String} name
   * @param {Object} params
   * @param {String} [prefix="le"]
   * @returns {jQuery}
   */
  Le.$template = function(name, params, prefix) {
    return $($.parseHTML(Le.template(name, params, prefix)));
  };

  /**
   * Register the partial
   *
   * @param {String} name
   * @param {String} [prefix="le"]
   */
  Le.registerPartial = function(name, prefix) {
    Handlebars.registerPartial(name, Le.templateHTML(name, prefix));
  };

  /**
   * Ajax
   *
   * @param {Object} params
   * @param {Object} [options]
   * @returns {jQuery.Deferred}
   */
  Le.ajax = function(params, options) {
    Le.showOverlay();

    var deferred = $.ajax(params);
    options = options || {};

    if (!options.skipHandleError) {
      deferred.fail(Le.ajax.error);
    }

    deferred.always(Le.hideOverlay);

    return deferred;
  };

  /**
   * Ajax error handler
   *
   * @param jqXHR
   * @param textStatus
   * @param errorThrown
   */
  Le.ajax.error = function(jqXHR, textStatus, errorThrown) {
    Le.flash('error', textStatus + ': ' + errorThrown);
  };

  /**
   * Bulk update
   *
   * @param data
   * @param options
   * @returns {*}
   */
  Le.bulkUpdate = function(data, options) {
    if (!options) {
      options = {};
    }

    var deferred = $.Deferred(),
      jqXHR = Le.ajax({
        type: options.type || 'POST',
        url: options.url || Le.url('bulkUpdate'),
        data: data,
      });

    jqXHR.done(function(html) {
      var $el,
        $html = $($.parseHTML(html));

      if (($el = $html.filter('#flash_error')).length) {
        deferred.reject(this, 'Error', $el.html());
      } else if (($el = $html.find('#errorExplanation')).length) {
        deferred.reject(this, 'Error', $el.find('li').html());
      } else {
        deferred.resolve(html);
      }
    });

    jqXHR.fail(function(jqXHR, textStatus, errorThrown) {
      var $el,
        $html = $($.parseHTML(jqXHR.responseText));

      if (($el = $html.filter('#errorExplanation')).length) {
        deferred.reject(jqXHR, 'Error', $el.html());
      } else {
        deferred.reject(jqXHR, 'Error', errorThrown);
      }
    });

    if (!options.skipError) {
      deferred.fail(Le.ajax.error);
    }

    return deferred.promise();
  };

  /**
   * Ajax error handler
   *
   * @param jqXHR
   * @param textStatus
   * @param errorThrown
   */
  Le.ajax.error = function(jqXHR, textStatus, errorThrown) {
    Le.flash('error', textStatus + ': ' + errorThrown);
  };

  /**
   * Flash the message
   *
   * @param {String} type
   * @param {String} message
   */
  Le.flash = function(type, message) {
    var baseStyle,
      $message = $(
        $.parseHTML(
          Le.template('flash', {
            type: type,
            message: message,
          })
        )
      );

    $message.css({ opacity: 0 });
    $('body').append($message);

    baseStyle = {
      top: -$message.innerHeight(),
      opacity: 0,
    };

    $message.css(baseStyle);
    $message.animate({ top: 0, opacity: 1 }, 300);
    $message.delay(5000);
    $message.animate(baseStyle, 300, function() {
      $(this).remove();
    });
    $message.on('click', function() {
      $(this).dequeue();
    });
  };

  /**
   * Show the overlay
   */
  Le.showOverlay = function() {
    Le.$overlay.show();
  };

  /**
   * Hide the overlay
   */
  Le.hideOverlay = function() {
    Le.$overlay.hide();
  };

  $(function() {
    Le.$overlay = $(document.createElement('div'));
    Le.$overlay.css({
      position: 'fixed',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      zIndex: 5000,
      background: '#FFF',
      opacity: 0,
    });
    Le.hideOverlay();
    $('body').append(Le.$overlay);
  });

  return Le;
})(jQuery);
