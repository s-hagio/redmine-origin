!function($) {
  'use strict';

  /**
   * Get rect
   *
   * @returns {Object}
   */
  $.fn.rect = function() {
    var rect = this.position();
    rect.width = this.outerWidth();
    rect.height = this.outerHeight();
    rect.right = rect.left + rect.width;
    rect.bottom = rect.top + rect.height;
    rect.center = rect.right - (rect.width / 2);
    rect.middle = rect.bottom - (rect.height / 2);
    return rect;
  };

  /**
   * Drag scroll
   */
  $.fn.dragScroll = function() {
    var $parent = this.parent();
    $parent.kinetic({
      filterTarget: function(el) {
        return $(el).parents('form').length < 1;
      }
    });
    $parent.on('focus', function(event) {
      event.preventDefault();
    });
    return this;
  };

  /**
   * Popover
   */
  $.fn.popover = function() {
    var ignoreName,
        $doc = $(document);
    $doc.on('click', function() {
      $('[data-popover-name]').filter(function() {
        return $(this).attr('data-popover-name') !== ignoreName;
      }).hide();
      ignoreName = null;
    });
    $doc.on('click', '[data-popover-name],[data-popover-target]', function() {
      var $el = $(this);
      ignoreName = $el.attr('data-popover-name') || $el.attr('data-popover-target');
    });
    return this.on('click', '[data-popover-target]', function() {
      var name = $(this).attr('data-popover-target'),
          $popover = $('[data-popover-name="' + name + '"]');
      $popover.toggle();
    });
  };
}(jQuery);