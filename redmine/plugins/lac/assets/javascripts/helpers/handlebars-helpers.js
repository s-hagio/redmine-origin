Handlebars.registerHelper('hasRelations', function(options) {
  if (this.changesets || this.other_issues) {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});

Handlebars.registerHelper('unlessUndefined', function(value, options) {
  if (value === undefined) {
    return options.inverse(this);
  } else {
    return options.fn(this);
  }
});

Handlebars.registerHelper('stringify', function(data) {
  return JSON.stringify(data);
});
