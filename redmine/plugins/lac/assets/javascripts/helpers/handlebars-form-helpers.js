Handlebars.registerHelper('optionTags', function(options, selectedValue) {
  var option, selected,
      html = '',
      i = 0;

  while (option = options[i++]) {
    selected = option.value === selectedValue ? ' selected="selected"' : '';
    html += '<option value="' + option.value + '"' + selected + '>'
         + option.text
         + '</option>';
  }

  return new Handlebars.SafeString(html);
});

Handlebars.registerHelper('optionTagsWithRange', function(min, max, step, selectedValue) {
  var options = [],
      i = min;

  for (; i < max; i += step) {
    options.push({value: i, text: i});
  }

  return Handlebars.helpers.optionTags(options, selectedValue);
});

Handlebars.registerHelper('radioTags', function(fieldInfo) {
  var option,
      html = '';

  for (var i in fieldInfo.values) {
    option = fieldInfo.values[i];
    html += '<label><input type="radio" value="' + option.value + '" '
         + Handlebars.helpers.editorFieldFlag('checked', option.value === fieldInfo.value)
         + Handlebars.helpers.editorFieldAttribute('name', fieldInfo.field_name)
         + '> ' + option.text + '</label>';
  }

  return new Handlebars.SafeString(html);
});

Handlebars.registerHelper('checkboxTags', function(fieldInfo) {
  var option,
      html = '<input value="" type="hidden"'
           + Handlebars.helpers.editorFieldAttribute('name', fieldInfo.field_name)
           + '>';

  for (var i in fieldInfo.values) {
    option = fieldInfo.values[i];
    html += '<label><input type="checkbox" value="' + option.value + '" '
         + Handlebars.helpers.editorFieldFlag('checked', option.value === fieldInfo.value)
         + Handlebars.helpers.editorFieldAttribute('name', fieldInfo.field_name)
         + '> ' + option.text + '</label>';
  }

  return new Handlebars.SafeString(html);
});

Handlebars.registerHelper('newAttachmentTags', function(fieldInfo, display_mode) {
  return '<span class="add_attachment" style="display: ' + display_mode + ';">'
         + '<input type="file" name="' + fieldInfo.field_name + '[dummy][file]" '
         + 'class="file_selector " onchange="addInputFiles(this);" data-max-file-size="5242880" '
         + 'data-max-concurrent-uploads="2" data-upload-path="/uploads.js" '
         + 'data-param="' + fieldInfo.field_name + '" '
         + 'data-description="false" data-description-placeholder="Optional description">'
         + '(Maximum size: 5 MB)'
         + '</span>';
});

Handlebars.registerHelper('alreadyAttachedTags', function(fieldInfo) {
  return '<span id="attachments_p0">'
         + '<input type="text" class="filename" readonly="readonly" '
         + 'value="' + fieldInfo.filename + '" '
         + 'name="' + fieldInfo.field_name + '[p0][filename]">'
         + '<a class="icon-only icon-del" href="#" '
         + 'onclick="$(this).closest(\'.attachments_form\').find(\'.add_attachment\').show();'
         + '$(this).parent().remove(); return false;">delete</a>'
         + '<input type="hidden" '
         + 'value="' + fieldInfo.value + '" '
         + 'name="' + fieldInfo.field_name + '[p0][id]">'
         + '</span>';
});

Handlebars.registerHelper('fileTags', function(fieldInfo) {
  var display_mode = 'inline';
  var html = '<input type="hidden" value="" '
           + 'name="' + fieldInfo.field_name + '[blank]">';
      html += '<span class="attachments_form"><span class="attachments_fields">';
      if (fieldInfo.value && fieldInfo.value != "") {
        display_mode = 'none';
        html += Handlebars.helpers.alreadyAttachedTags(fieldInfo);
      }
      html += '</span>';
      html += Handlebars.helpers.newAttachmentTags(fieldInfo, display_mode);
      html += '</span>';
  return new Handlebars.SafeString(html);
});

Handlebars.registerHelper('editorFieldAttribute', function(name, value) {
  return value ? name + '="' + value + '" ' : '';
});

Handlebars.registerHelper('editorFieldFlag', function(name, value) {
  return value ? name + ' ' : '';
});


Handlebars.registerHelper('editorField', function(fieldInfo) {
  var html = '';

  if (fieldInfo.type == 'text') {
    html += '<input class="lac-text" type="text"'
         + Handlebars.helpers.editorFieldAttribute('name', fieldInfo.field_name)
         + Handlebars.helpers.editorFieldAttribute('value', fieldInfo.value)
         + '>';
  } else if (fieldInfo.type == 'select') {
    html += '<select '
         + Handlebars.helpers.editorFieldAttribute('name', fieldInfo.field_name)
         + Handlebars.helpers.editorFieldFlag('multiple', fieldInfo.multiple)
         + (fieldInfo.field_name == 'tracker_id' ? Handlebars.helpers.editorFieldAttribute('onchange', 'Lac.created.onTrackerChanged(this)') : '')
         + '>'
         + Handlebars.helpers.optionTags(fieldInfo.values, fieldInfo.value)
         + '</select>';
  } else if (fieldInfo.type == 'date') {
    html += '<input type="date"'
         + Handlebars.helpers.editorFieldAttribute('name', fieldInfo.field_name)
         + Handlebars.helpers.editorFieldAttribute('value', fieldInfo.value)
         + 'size="10" class="lac-datePicker">';
  } else if (fieldInfo.type == 'radio') {
    html += '<span class="check_box_group">'
         + Handlebars.helpers.radioTags(fieldInfo)
         + '</span>';
  } else if (fieldInfo.type == 'check_box') {
    html += '<span class="check_box_group">'
         + Handlebars.helpers.checkboxTags(fieldInfo)
         + '</span>';
  } else if (fieldInfo.type == 'file') {
    html += '<span class="lac-file">'
         + Handlebars.helpers.fileTags(fieldInfo)
         + '</span>';
  }
  return new Handlebars.SafeString(html);
});
