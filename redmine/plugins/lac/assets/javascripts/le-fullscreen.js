!(function($) {
  var SESSION_ID = 'le-fullscreen',
    ENABLED_CLASS = 'le-fullscreen';

  function fullscreen(mode) {
    mode = mode || sessionStorage.getItem(SESSION_ID);

    if (!mode) {
      return;
    }

    var isStartMode = mode === 'start',
      newMode = isStartMode ? 'exit' : 'start';

    sessionStorage.setItem(SESSION_ID, mode);

    $('html').toggleClass(ENABLED_CLASS, isStartMode);

    $('[data-fullscreen]').each(function() {
      var $el = $(this);
      $el.attr('data-fullscreen', newMode);
      $el.text($el.attr('data-fullscreen-' + newMode));
      $el.addClass('icon-fullscreen-' + newMode);
      $el.removeClass('icon-fullscreen-' + mode);
    });
  }

  $(document).on('click', '[data-fullscreen]', function(event) {
    event.preventDefault();
    var $el = $(event.currentTarget);
    fullscreen($el.attr('data-fullscreen'));
  });

  fullscreen();
})(jQuery);
