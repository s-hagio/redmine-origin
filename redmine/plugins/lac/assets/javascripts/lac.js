var Lac = (function($) {
  'use strict';

  var Lac = {};

  Lac.modulePermissions = {};

  Lac.permission = function(name, permission) {
    Lac.modulePermissions[name] = permission;
  };

  Lac.setLabels = function(labels) {
    Lac.labels = labels;
  };

  Lac.setupPartials = function() {
    Le.registerPartial('chart-item');
    Le.registerPartial('chart-unrelated-item');
    Le.registerPartial('chart-issue');
    Le.registerPartial('chart-changeset');
    Le.registerPartial('chart-other');
    Le.registerPartial('editor-field');
    Le.registerPartial('unrelated-issue-modal');
  };

  Lac.setupChartZoomer = function() {
    Lac.zoom = 3;
    Lac.$zoomer = $('.lac-chart-zoom');
    Lac.$zoomer.disableZoom = function() {
      this.find('[data-zoom]').each(function() {
        var $el = $(this),
          zoom = $el.data('zoom');
        $el.toggleClass(
          'lac-disabled',
          (zoom === 'in' && Lac.zoom <= 1) || (zoom === 'out' && Lac.zoom >= 3)
        );
      });
    };
    Lac.$zoomer.disableZoom();
    Lac.$zoomer.on('click', '[data-zoom]', function() {
      var $el = $(this),
        zoom = $(this).data('zoom');

      if ($el.hasClass('lac-disabled')) {
        return;
      }

      if (zoom === 'in') {
        --Lac.zoom;
      } else if (zoom === 'out') {
        ++Lac.zoom;
      }

      Lac.$zoomer.disableZoom();
      $('body').attr('data-chart-zoom', Lac.zoom);
      Lac.chart.drawRelationLine();
    });
  };

  Lac.setupModules = function() {
    Lac.$chart = $('#lac-chart');
    Lac.$chart.dragScroll('.lac-chart-item');
    Lac.$chart.lacEditable('.lac-editable-toggle', '.lac-chart-item');

    Lac.$paper = $('#lac-chart-relations');

    Lac.paper = new Raphael(Lac.$paper[0]);

    Lac.modal = new Lac.Modal('#lac-modal');
    Lac.chart = new Lac.Chart();

    if (Lac.modulePermissions.relatable) {
      Lac.relatable = new Lac.Relatable();
      Lac.relatable.start();

      Lac.created = new Lac.Relatable.Created();
      Lac.created.start('.lac-create-issue', '.lac-chart-item');
    }
  };

  Lac.setupPlugins = function() {
    $(document).popover();
  };

  Lac.start = function() {
    Lac.setupPartials();
    Lac.setupChartZoomer();
    Lac.setupModules();
    Lac.setupPlugins();
    Lac.refresh();
  };

  Lac.refresh = function() {
    return $.when(Lac.chart.load());
  };

  return Lac;
})(jQuery);
