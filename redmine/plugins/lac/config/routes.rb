# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

match 'issues/:id/associations',
      controller: 'association_chart',
      action: 'index',
      via: :get,
      as: 'issue_association_chart'

match 'lac/issues/:id/unrelated_issues',
      controller: 'lac_issues',
      action: 'unrelated_issues',
      via: %i[get post],
      as: 'lac_unrelated_issues'

get 'lac/issues/:id/data_for_editor',
    controller: 'association_chart',
    action: 'data_for_editor',
    as: 'lac_data_for_editor'

get 'lac/issues/:id/new',
    controller: 'lac_issues',
    action: 'new',
    as: 'lac_issue_new'

post 'lac/issues/:id/create',
     controller: 'lac_issues',
     action: 'create',
     as: 'lac_issue_create'

get 'lac/flash',
    controller: 'association_chart',
    action: 'flash_message',
    as: 'lac_flash_message'
