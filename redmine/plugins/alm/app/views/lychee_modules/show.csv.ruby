# Forces 'utf-8' to prevent garbled characters.
Redmine::Export::CSV.generate(encoding: 'utf-8') do |csv|
  csv << %w[login name mail group].map { |column| t("field_#{column}", default: column) }
  @lychee_module.user_ids.sort.each_slice(1000) do |user_ids|
    User.preload(:groups).where(id: user_ids).each do |user|
      groups = user.groups.pluck(:lastname).join(",")
      csv << [user.login, user.name, user.mail, groups]
    end
  end
end
