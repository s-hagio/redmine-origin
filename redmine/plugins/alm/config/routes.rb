# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

resource :lychee_license, only: :show do
  member do
    post :register
    put :register
  end
end

resources :lychee_users, only: %i[index show], controller: 'lychee_modules', param: 'module_name'
