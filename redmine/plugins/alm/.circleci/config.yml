version: 2.1

orbs:
  redmine-plugin: agileware-jp/redmine-plugin@3.3.0
  redmine:
    commands:
      install-rspec:
        steps:
          - run: bin/rails generate rspec:install
  rubocop:
    executors:
      rubocop-executor:
        parameters:
          ruby_version:
            description: version of Ruby
            type: string
            default: $RUBY_MAX_VERSION
        docker:
          - image: cimg/ruby:<<parameters.ruby_version>>
    commands:
      install-rubocop:
        steps:
          - run:
              name: Install Rubocop
              command: |
                set -e
                gem install rubocop -v '~> 0.93.1'
                gem install rubocop-rails
                gem install rubocop-rspec
                gem install rubocop-performance
      install-cop-from-github:
        parameters:
          url:
            description: URL of Git repository of custom cop
            type: string
        steps:
          - run:
              name: Install Cop from << parameters.url >>
              command: git clone << parameters.url >> /tmp/cop-repo && cd /tmp/cop-repo && rake install && cd / && rm -rf /tmp/cop-repo
      run-cops:
        parameters:
          rubocop_option:
            description: CLI option for rubocop
            type: string
            default: '--format simple'
        steps:
          - run:
              name: Rubocop
              command: rubocop << parameters.rubocop_option >>

jobs:
  rubocop:
    parameters:
      rubocop_files:
        description: files to check by rubocop
        type: string
    executor: rubocop/rubocop-executor
    steps:
      - checkout
      - rubocop/install-rubocop
      - rubocop/install-cop-from-github:
          url: https://github.com/agileware-jp/rubocop-lychee.git
      - rubocop/install-cop-from-github:
          url: https://github.com/agileware-jp/rubocop-no_keyword_args.git
      - rubocop/run-cops:
          rubocop_option: --fail-level E --display-only-fail-level-offenses -f s -c ~/project/.rubocop.yml << parameters.rubocop_files >>
  brakeman:
    docker:
      - image: presidentbeef/brakeman
    steps:
      - checkout
      - run: /usr/src/app/bin/brakeman --color
  rspec:
    parameters:
      redmine_version:
        type: string
      ruby_version:
        type: string
      db:
        type: enum
        enum: ['mysql', 'pg']
      db_version:
        type: string
    executor:
      name: redmine-plugin/ruby-<< parameters.db >>
      ruby_version: << parameters.ruby_version >>
      db_version: << parameters.db_version >>
    steps:
      - checkout
      - redmine-plugin/download-redmine:
          version: << parameters.redmine_version >>
      - redmine-plugin/install-self
      - redmine-plugin/generate-database_yml
      - redmine-plugin/bundle-install
      - redmine-plugin/migrate-without-plugins
      - redmine-plugin/rspec

default_context: &default_context
  context:
    - lychee-ci-environment

workflows:
  version: 2

  test:
    jobs:
      - rubocop:
          <<: *default_context
          name: 'rubocop'
          rubocop_files:  ~/project
      - brakeman
      - rspec:
          <<: *default_context
          name: RSpec on supported maximum versions with PostgreSQL
          redmine_version: $REDMINE_MAX_VERSION
          ruby_version: $RUBY_MAX_VERSION
          db: pg
          db_version: $POSTGRES_VERSION
      - rspec:
          <<: *default_context
          name: RSpec on supported minimum versions with MySQL
          redmine_version: $REDMINE_MIN_VERSION
          ruby_version: $RUBY_MIN_VERSION
          db: mysql
          db_version: $MYSQL_VERSION
