var LycheeSamlAuth = (function($){
  "use strict";

  var LycheeSamlAuth = {};

  LycheeSamlAuth.try_connection = function(event){
    var $this = $(event.target);
    var url = $this.attr('href');
    var id = $this.data('auth-source-saml-id');

    var $source_row = $('#auth-source-' + id);
    
    $.get(url).done(function(obj) {
      var $target_row = $source_row.next('.try-connection-auth-sources');

      if($target_row.length < 1){
        $source_row.after($('.try-connection-auth-sources').last().clone());
        $source_row.next('.try-connection-auth-sources').show();

        $target_row = $source_row.next('.try-connection-auth-sources');
      }else{
        $target_row.show();
      }

      $target_row.find('.request_url').text(obj.request_url);
      $target_row.find('.status_code').text(obj.status);
    });

    return false;
  }

  LycheeSamlAuth.add_form_event = function($form){
    $form.find('#auth_source_saml_auth_source_saml_info_type').on('change', function(){
      $("#auth_source_saml_form p:not('.all')").hide();
      $('#auth_source_saml_form p.' + $(this).val().split(/(?=[A-Z])/).join('_').toLowerCase().replace('_info', '')).show();
    }).change();

    $form.find('#auth_source_saml_onthefly_register').on('change', function(){
      var $required = $('#auth_source_saml_attribute_mapping_firstname, #auth_source_saml_attribute_mapping_lastname').prev().find('span.required');

      $(this).is(':checked') ? $required.show() : $required.hide();
    }).change();

    $form.on('submit', function(){
      $('#auth_source_saml_form p:hidden input').remove();
    })
  }

  return(LycheeSamlAuth);
}(jQuery));
