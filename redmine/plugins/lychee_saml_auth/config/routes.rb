resources :auth_source_saml do
  member do
    get 'try_connection'
    get 'auth_request'
  end
end

# Used by OmniAuth
# https://github.com/omniauth/omniauth#integrating-omniauth-into-your-application
get  '/auth/failure'            => 'account#login_with_saml_failure'
post '/auth/:provider/callback' => 'account#login_with_saml_callback'
