import type { Config } from 'jest';

const config: Config = {
  testPathIgnorePatterns: [
    '/node_modules/', // デフォルト
    '/src_calendar_mode/',
  ],
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  setupFilesAfterEnv: ['<rootDir>/src/setupTests.ts'],
  moduleNameMapper: {
    '\\.(s?css)$': '<rootDir>/src/__mocks__/empty.js',
    '\\.(png)$': '<rootDir>/src/__mocks__/file.js',
    '^@/(.+)': '<rootDir>/src/$1',
  },
};

export default config;
