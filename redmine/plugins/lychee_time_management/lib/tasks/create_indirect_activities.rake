# frozen_string_literal: true

desc <<~DESC
  Create work_plans about indirect activity for tomorrow.

  Available options:
    * activity   => id of indirect time_entry activity (defaults to the default activity)
    * start_hour => start hour of work_plan (defaults to 9)
    * start_min  => start minute of work_plan (defaults to 0)
    * hours      => hours of work_plan (defaults to 3)
    * users      => comma separated list of user/group ids whose work_plan should be created

  Example:
    rake redmine:plugins:lychee_time_management:create_indirect_activities start_hour=7 start_min=30 hours=2 users="1,23, 56" RAILS_ENV="production"
DESC

namespace :redmine do
  namespace :plugins do
    namespace :lychee_time_management do
      task create_indirect_activities: :environment do
        activity = if ENV['activity']
                     IndirectTimeEntryActivity.find(ENV['activity'])
                   else
                     IndirectTimeEntryActivity.default
                   end
        principals = Principal.where(id: ENV['users'].presence.to_s.split(','))
        factory = IndirectWorkPlanFactory.new(activity, principals)

        Time.use_zone('Tokyo') do
          factory.create_work_plans!(
            Time.zone.now.tomorrow.change(
              hour: ENV.fetch('start_hour', 9),
              min: ENV.fetch('start_min', 0)
            ),
            ENV.fetch('hours', 3).to_f
          )
        end
      end
    end
  end
end
