namespace :redmine do
  namespace :plugins do
    namespace :lychee_time_management do
      desc 'Fix work plans with different projects'
      task fix_work_plans_with_different_projects: :environment do
        # Work plans with different projects were set parent_id as nil.
        WorkPlan.owned_by('TimeEntryActivity').where(parent_id: nil).each do |direct_act_plan|
          issue_plan = WorkPlan.owned_by('Issue').contains(direct_act_plan)
                               .find_by(user: direct_act_plan.user)
          next if issue_plan.nil?
          if issue_plan.project.activities.exists?(direct_act_plan.owned_by.id)
            direct_act_plan.update_columns(
              project_id: issue_plan.project_id,
              parent_id: issue_plan.id
            )
          else
            direct_act_plan.destroy
          end
        end
      end

      desc 'Fix issue work plans with different projects'
      task fix_issue_work_plans_with_different_projects: :environment do
        ActiveRecord::Base.transaction do
          WorkPlan.owned_by('Issue').find_each do |issue_plan|
            issue_project = issue_plan.owned_by.project
            next if issue_plan.project_id == issue_project.id
            if issue_project.module_enabled?(:lychee_time_management)
              issue_plan.update_columns(project_id: issue_project.id)
              issue_plan.children.update_all(project_id: issue_project.id)
            else
              issue_plan.children.destroy_all
              issue_plan.destroy
            end
          end
        end
      end

      desc 'Set parent_id of work plans'
      task set_parent_id_of_work_plans: :environment do
        WorkPlan.owned_by('TimeEntryActivity').where(parent_id: nil).each do |direct_act_plan|
          parent_plan = WorkPlan.owned_by('Issue').contains(direct_act_plan).find_by(
            user: direct_act_plan.user,
            project: direct_act_plan.project
          )
          direct_act_plan.update_columns(parent_id: parent_plan.id) if parent_plan
        end
      end

      desc 'List invalid time entries in LTM'
      task list_invalid_ltm_time_entries: :environment do
        output = LycheeTimeManagement::Batch::InvalidLtmTimeEntryDetector.new.run
        if output.present?
          puts output
        else
          warn '修正が必要なデータはありません。'
        end
      end

      desc 'List ltm time entries with wrong spent on date in LTM'
      task list_ltm_time_entries_with_wrong_spent_on_date: :environment do
        output = LycheeTimeManagement::Batch::TimeEntrySpentOnChecker.run
        if output.present?
          puts output
        else
          warn '修正が必要なデータはありません。'
        end
      end

      desc 'Set work plans as converted'
      task set_work_plans_as_converted: :environment do
        LycheeTimeManagement::Batch::SetWorkPlanConverted.run
      end
    end
  end
end
