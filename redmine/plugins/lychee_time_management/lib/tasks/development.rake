namespace :redmine do
  namespace :plugins do
    namespace :lychee_time_management do
      desc 'Initialize records for development'
      task dev_seed: :environment do
        LycheeTimeManagement::DevelopmentSeed.initialize
      end
    end
  end
end
