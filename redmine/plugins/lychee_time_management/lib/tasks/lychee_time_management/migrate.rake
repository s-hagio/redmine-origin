# frozen_string_literal: true

namespace :redmine do
  namespace :plugins do
    namespace :lychee_time_management do
      namespace :migrate do
        desc 'Migrate spent_on, start_time and hours to start and end'
        task m005up: :environment do
          WorkPlan.reset_column_information

          # タイムゾーンは分からないのでJSTと仮定する
          WorkPlan.find_each do |plan|
            start = ActiveSupport::TimeZone['Asia/Tokyo'].local(
              plan.spent_on.year,
              plan.spent_on.month,
              plan.spent_on.day,
              plan.start_time.hour,
              plan.start_time.min,
              plan.start_time.sec
            )
            plan.update!(
              start: start,
              end: start.advance(hours: plan.hours)
            )
          end
        end

        desc 'Migrate start and end to spent_on, start_time and hours'
        task m005down: :environment do
          WorkPlan.reset_column_information

          # タイムゾーンは分からないのでJSTと仮定する
          WorkPlan.find_each do |plan|
            plan.update!(
              spent_on: plan.start.in_time_zone('Asia/Tokyo'),
              hours: (plan.end - plan.start) / 1.hour,
              start_time: plan.start
            )
          end
        end

        desc 'Remove WorkPlan without project_id'
        task m013down: :environment do
          WorkPlan.where(project_id: nil).destroy_all
        end

        desc 'Remove all IndirectAcitivityTimeEntry records'
        task m014down: :environment do
          # TRUNCATE is not supported by activerecord-sqlserver-adapter and SQLite3.
          ActiveRecord::Base.connection.execute('DELETE FROM indirect_activity_time_entries;')
        end

        desc 'Migrate time_entry_starts from ltm_time_entries'
        task m020up: :environment do
          time_entry_start_class = Class.new(ActiveRecord::Base) {
            def self.name
              'TimeEntryStart'
            end

            self.table_name = 'time_entry_starts'

            belongs_to :time_entry
          }
          time_entry_start_class.reset_column_information
          time_entry_start_class.where(start: nil).destroy_all

          indirect_time_entry_class = Class.new(ActiveRecord::Base) {
            def self.name
              'IndirectTimeEntry'
            end

            self.table_name = 'indirect_time_entries'

            belongs_to :project
            belongs_to :user
            belongs_to :activity, class_name: 'IndirectTimeEntryActivity'
          }
          indirect_time_entry_class.reset_column_information

          LycheeTimeManagement::Batch.migrate_to_ltm_time_entries(
            time_entry_start_class.joins(:time_entry),
            indirect_time_entry_class.all
          )
        end

        desc 'Convert indirect time entries for table mode'
        task m034up: :environment do
          ActiveRecord::Base.transaction do
            max_hours_per_day = Setting.timelog_max_hours_per_day
            Setting.timelog_max_hours_per_day = '0'
            LtmTimeEntry.where(owned_by_type: 'IndirectTimeEntryActivity')
                        .includes(:user, :owned_by)
                        .find_each do |ltm_time_entry|
              user = ltm_time_entry.user
              next if user.blank? || ltm_time_entry.owned_by.blank?

              TableMode::IndirectTimeEntry.create!(
                author: user,
                user: user,
                activity: ltm_time_entry.owned_by,
                spent_on: user.time_to_date(ltm_time_entry.start),
                hours: ltm_time_entry.send(:hours),
                comments: ltm_time_entry.comments
              )
            end
            Setting.timelog_max_hours_per_day = max_hours_per_day
          end
        end
      end
    end
  end
end
