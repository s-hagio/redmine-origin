namespace :redmine do
  namespace :plugins do
    namespace :lychee_time_management do
      desc 'Convert work plans into time entries'
      task work_plans_to_time_entries: :environment do
        logger = Logger.new('log/work_plans_to_time_entries.log', 'monthly')
        begin
          logger.info('Start work_plans_to_time_entries')
          LycheeTimeManagement::Batch::PlanToTimeEntry.new(Time.now.utc, logger).run
        rescue => e
          logger.error([e.message, e.backtrace].join("\n"))
          raise
        ensure
          logger.info('Finish work_plans_to_time_entries')
        end
      end
    end
  end
end
