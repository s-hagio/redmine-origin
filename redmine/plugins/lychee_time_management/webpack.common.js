const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  entry: {
    table: './src/entry_points/table.tsx',
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'assets', 'javascripts'),
  },
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        exclude: /(node_modules|bower_components)/,
        use: { loader: 'ts-loader' },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.png$/,
        type: 'asset/inline',
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json'],
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
  plugins: [
    new CleanWebpackPlugin(),
    new WebpackNotifierPlugin({
      title: 'Webpack [Lychee Time Management]',
      skipFirstNotification: true,
      alwaysNotify: true,
    }),
    new MomentLocalesPlugin({
      localesToKeep: ['ja'],
    }),
    // new BundleAnalyzerPlugin(),
  ],
  target: 'web',
};
