# lychee_time_management

## 開発

### 準備

必要なバージョンのNodeJSとnpmパッケージをインストール

```console
$ nodenv install --skip-existing $(cat .node-version) && npx -y yarn install &&
  (cd src_calendar_mode && nodenv install --skip-existing $(cat .node-version) && npx -y yarn install)
```

ホットリロードを動かすためにrails serverを次のように起動してください

```console
$ LTM_WEBPACK_SERVE_URL=http://127.0.0.1:8080 LTM_WEBPACK_CALENDAR_MODE_SERVE_URL=http://127.0.0.1:8081 bundle exec rails server
```

### yarn serverの起動

#### 表形式

```console
$ npx yarn server
```

#### カレンダー形式

```console
$ (cd src_calendar_mode && npx yarn server)
```
