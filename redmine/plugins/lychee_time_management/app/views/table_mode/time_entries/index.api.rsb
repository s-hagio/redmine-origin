api.array :time_entries do
  @time_entries.each do |time_entry|
    api.time_entry do
      api.id "#{time_entry.class.name}-#{time_entry.id}"
      api.activity_type activity_type(time_entry)
      api.activity(
        activity_id: time_entry.activity_id,
        project_id: time_entry.project_id,
        issue_id: time_entry.issue_id,
        indirect_activity_id: time_entry.activity_id,
      )
      api.date time_entry.spent_on
      api.hours time_entry.raw_hours
      api.comments time_entry.comments

      render_api_custom_values time_entry.try(:visible_custom_field_values) || [], api
    end
  end
end

api.array :activities do
  TimeEntryActivity.where(project_id: [nil, @projects.map(&:id)].flatten).each do |time_entry_activity|
    api.time_entry_activity do
      api.id time_entry_activity.id
      api.name time_entry_activity.name
      api.position time_entry_activity.position
    end
  end
end

api.array :indirect_activities do
  IndirectTimeEntryActivity.all.each do |time_entry_activity|
    api.time_entry_activity do
      api.id time_entry_activity.id
      api.name time_entry_activity.name
      api.active time_entry_activity.active
      api.position time_entry_activity.position
    end
  end
end

api.array :issues do
  @issues.each do |issue|
    render_issue_attributes(issue, @issues_by_id, api)
  end
end

api.array :projects do
  @projects.each do |project|
    render_project_attributes(project, @user, api)
  end
end

if @working_hours
  api.array :working_hours do
    @working_hours.each do |working_hour|
      api.working_hour do
        api.id working_hour.id
        api.date working_hour.spent_on
        api.hours working_hour.hours
      end
    end
  end
end

api.assigned_issue_ids @assigned_issues.ids
