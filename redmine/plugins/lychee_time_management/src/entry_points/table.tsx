import 'core-js/stable';
import { enableES5 } from 'immer';
import * as React from 'react';
import { createRoot } from 'react-dom/client';
import { RecoilRoot } from 'recoil';
import 'react-toastify/dist/ReactToastify.css';

import {
  timespanFormatState,
  TimespanFormat,
} from '@/recoil/timespanFormatState';
import {
  themeStyleState,
} from '@/recoil/themeStyleState';
import TableModeRoot from '@/components/TableMode/Root';
import { Labels } from '@/lib/tableMode/labels';
import { CurrentUser, TargetUser, Permission } from '@/lib/tableMode/types';
import { buildAttachmentApiHandler } from '@/lib/tableMode/attachmentApiHandler';
import { timeEntryDataStore } from '@/lib/tableMode/timeEntryDataStore';
import WorkDaysWorkMonthProvider, {
  ClosingDay,
  DateString,
  Weekday,
} from '@/lib/tableMode/workDaysWorkMonthProvider';

// IE 11対応
enableES5();

const rootElement = document.getElementById(
  'time-management-root'
) as HTMLElement;

function parseDatasetParameter<T>(element: HTMLElement, name: string): T {
  return JSON.parse(element.dataset[name] as string) as T;
}

const INJECTED_PARAMETERS = {
  currentUser: parseDatasetParameter<CurrentUser>(rootElement, 'currentUser'),
  targetUsers: parseDatasetParameter<TargetUser[]>(rootElement, 'targetUsers'),
  holidays: parseDatasetParameter<DateString[]>(rootElement, 'holidays'),
  closingDay: rootElement.dataset['closingDay'] as ClosingDay,
  labels: parseDatasetParameter<Labels>(rootElement, 'labels'),
  nonWorkingWeekdays: parseDatasetParameter<Weekday[]>(
    rootElement,
    'nonWorkingWeekdays'
  ),
  requiredFields: parseDatasetParameter<string[]>(
    rootElement,
    'requiredFields'
  ),
  rootPath: rootElement.dataset['rootPath'] as string,
  indirectTimeEntryPermissions: parseDatasetParameter<Permission[]>(
    rootElement,
    'indirectTimeEntryPermissions'
  ),
  requiredCustomFieldExists: parseDatasetParameter<boolean>(
    rootElement,
    'requiredCustomFieldExists'
  ),
  timespanFormat: parseDatasetParameter<TimespanFormat>(
    rootElement,
    'timespanFormat'
  ),
};

const workMonthProvider = new WorkDaysWorkMonthProvider(
  INJECTED_PARAMETERS.nonWorkingWeekdays,
  INJECTED_PARAMETERS.holidays,
  INJECTED_PARAMETERS.closingDay
);

const initialThemeStyle = {
  contentBackgroundColor: getComputedStyle(
    document.querySelector('#content')!
  ).getPropertyValue('background-color'),
};

const root = createRoot(rootElement);
root.render(
  <RecoilRoot
    initializeState={({ set }): void => {
      set(timespanFormatState, INJECTED_PARAMETERS.timespanFormat);
      set(themeStyleState, initialThemeStyle);
    }}
  >
    <TableModeRoot
      currentUser={INJECTED_PARAMETERS.currentUser}
      targetUsers={INJECTED_PARAMETERS.targetUsers}
      labels={INJECTED_PARAMETERS.labels}
      timeEntryDataStore={timeEntryDataStore}
      workMonthProvider={workMonthProvider}
      timeEntryRequiredFields={INJECTED_PARAMETERS.requiredFields}
      requiredCustomFieldExists={INJECTED_PARAMETERS.requiredCustomFieldExists}
      indirectTimeEntryPermissions={
        INJECTED_PARAMETERS.indirectTimeEntryPermissions
      }
      attachmentApi={buildAttachmentApiHandler(INJECTED_PARAMETERS.rootPath)}
    />
  </RecoilRoot>
);
