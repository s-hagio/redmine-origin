export const noNeedInputOpertor = [
  'o', // 未完了
  'c', // 完了
  '!*', // なし
  '*', // すべて
  't', // 今日
  'ld', // 昨日
  'w', // 今週
  'lw', // 先週
  'l2w', // 直近2週間
  'm', // 今月,
  'lm', // 先月,
  'y', // 今年,
  '*o', // 未完了のチケット"
  '!o', // なし または完了したチケット"
] as const;

export const needInputOperators = [
  '=', // 等しい
  '!', // 等しくない
  '>=', // 以上
  '<=', // 以下
  '~', // 含む",
  '!~', // 含まない",
  '=p', // 次のプロジェクト内のチケット",
  '=!p', // 次のプロジェクト外のチケット",
  '!p', // 次のプロジェクト内のチケットを除く",
] as const;

export const needRangeInputOperators = [
  '><', // 次の範囲内
] as const;

export const needNumberInputOperators = [
  '<t+', // 今日から○日後以前"
  '>t+', // 今日から○日後以降"
  '><t+', // 今後○日"
  't+', // 今日から○日後"
  '>t-', // 今日より○日前以降"
  '<t-', // 今日より○日前以前"
  '><t-', // 過去○日"
  't-', // ○日前"
] as const;

export type FilterType =
  | 'list'
  | 'list_status'
  | 'list_optional'
  | 'list_optional_with_history'
  | 'list_subprojects'
  | 'date'
  | 'date_past'
  | 'string'
  | 'text'
  | 'integer'
  | 'float'
  | 'relation'
  | 'tree';

export type OperatorType =
  | typeof noNeedInputOpertor[number]
  | typeof needInputOperators[number]
  | typeof needRangeInputOperators[number]
  | typeof needNumberInputOperators[number];

export default {};
