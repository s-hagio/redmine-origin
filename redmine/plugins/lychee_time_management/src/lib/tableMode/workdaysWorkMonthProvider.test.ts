import * as MockDate from 'mockdate';
import { WorkMonth } from '@/lib/tableMode/types';

import WorkDaysWorkMonthProvider from './workDaysWorkMonthProvider';

describe('WorkDaysWorkMonthProvider', () => {
  const build = (): WorkDaysWorkMonthProvider => {
    return new WorkDaysWorkMonthProvider([], [], 'end_of_month');
  };

  describe('getCurrent', () => {
    beforeAll(() => {
      MockDate.set(new Date('2021-02-09T12:00:00+09:00'));
    });

    afterAll(() => {
      MockDate.reset();
    });

    it('returns the current month', () => {
      const provider = build();
      expect(provider.getCurrent().month).toEqual('2021-02');
    });

    it('returns all the dates of the month', () => {
      const provider = build();
      const dates = provider.getCurrent().dates.map(date => date.date);

      expect(dates).toHaveLength(28);
      expect(dates[0]).toEqual('2021-02-01');
      expect(dates[1]).toEqual('2021-02-02');
      expect(dates[27]).toEqual('2021-02-28');
    });

    it('returns dates specified by closing day (today is before closing day)', () => {
      const provider = new WorkDaysWorkMonthProvider([], [], '15');
      const dates = provider.getCurrent().dates.map(date => date.date);

      expect(dates).toHaveLength(31);
      expect(dates[0]).toEqual('2021-01-16');
      expect(dates[1]).toEqual('2021-01-17');
      expect(dates[30]).toEqual('2021-02-15');
    });

    it('returns dates specified by closing day (today is after closing day)', () => {
      const provider = new WorkDaysWorkMonthProvider([], [], '5');
      const dates = provider.getCurrent().dates.map(date => date.date);

      expect(dates).toHaveLength(28);
      expect(dates[0]).toEqual('2021-02-06');
      expect(dates[1]).toEqual('2021-02-07');
      expect(dates[27]).toEqual('2021-03-05');
    });

    it('returns only today as today', () => {
      const provider = build();
      const dateTypes = provider.getCurrent().dates.map(date => date.type);

      expect(dateTypes[8]).toBe('today');
      expect(
        dateTypes.slice(0, 8).every(type => type !== 'today')
      ).toBeTruthy();
      expect(dateTypes.slice(9).every(type => type !== 'today')).toBeTruthy();
    });

    it('returns all past dates as past', () => {
      const provider = build();
      const pastValues = provider.getCurrent().dates.map(date => date.past);

      expect(pastValues.slice(0, 7).every(past => past)).toBeTruthy();
      expect(pastValues.slice(8).every(past => !past)).toBeTruthy();
    });

    it('returns non-working weekdays as holidays and non-working saturdays as saturdays', () => {
      const provider = new WorkDaysWorkMonthProvider(
        [0, 1, 6],
        [],
        'end_of_month'
      ); // Monday, Saturday, Sunday
      const dateTypes = provider.getCurrent().dates.map(date => date.type);

      expect(dateTypes.slice(0, 7)).toEqual([
        'holiday',
        'workDay',
        'workDay',
        'workDay',
        'workDay',
        'saturday',
        'holiday',
      ]);
    });

    it('returns working saturdays as work days', () => {
      const provider = new WorkDaysWorkMonthProvider([], [], 'end_of_month'); // Monday, Saturday, Sunday
      const dateTypes = provider.getCurrent().dates.map(date => date.type);

      expect(dateTypes[5]).toEqual('workDay');
      expect(dateTypes[12]).toEqual('workDay');
      expect(dateTypes[19]).toEqual('workDay');
      expect(dateTypes[26]).toEqual('workDay');
    });

    it('returns specified holidays as holidays', () => {
      const provider = new WorkDaysWorkMonthProvider(
        [],
        ['2021-02-03'],
        'end_of_month'
      );
      const dateTypes = provider.getCurrent().dates.map(date => date.type);

      expect(dateTypes[2]).toBe('holiday');
    });
  });

  describe('getNextMonth', () => {
    it('returns the next month', () => {
      const provider = build();
      const month: WorkMonth = {
        month: '2021-02',
        dates: [],
      };

      const nextMonth = provider.getNextMonth(month);

      expect(nextMonth.month).toBe('2021-03');
      expect(nextMonth.dates[0].date).toBe('2021-03-01');
      expect(nextMonth.dates[30].date).toBe('2021-03-31');
    });
  });

  describe('getPreviousMonth', () => {
    it('returns the previous month', () => {
      const provider = build();
      const month: WorkMonth = {
        month: '2021-02',
        dates: [],
      };

      const previousMonth = provider.getPreviousMonth(month);

      expect(previousMonth.month).toBe('2021-01');
      expect(previousMonth.dates[0].date).toBe('2021-01-01');
      expect(previousMonth.dates[30].date).toBe('2021-01-31');
    });
  });
});
