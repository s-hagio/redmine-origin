import { AttachmentApiHandler } from '@/components/RedmineCustomField';

export type Permission = 'edit' | 'create';

export type Activity = {
  id: number;
  name: string;
  active?: boolean;
  position: number;
};

export type AttachmentApiHandlerWithMemory = AttachmentApiHandler & {
  deleteUploadedAttachments: () => Promise<void>;
  clearMemory: () => void;
};

export type CurrentUser = { id: number; name: string; locale: string };
export type TargetUser = { id: number; name: string };

export type Issue = {
  id: number;
  projectId: number;
  trackerId: number;
  subject: string;
  status: string;
  estimatedHours: number | null;
  startDate: string | null;
  dueDate: string | null;
  url: string;
  parentId?: number;
};

export type Project = {
  id: number;
  name: string;
  url: string;
  lft: number;
  active: boolean;
  permissions: Permission[];
  activityIds: number[];
};

export type Tracker = {
  id: number;
  name: string;
};

export type TimeEntryId = string;

export type Entry = {
  date: string; // urn:iso:std:iso:8601 format
  hours: number;
};

export type WorkingHours = Entry & {
  id: number;
};

type BaseTimeEntry = Entry & {
  id: TimeEntryId;
  comments?: string;
  customFieldValues?: { [fieldId: number]: unknown };
};

export type IssueTimeEntry = BaseTimeEntry & {
  activityType: 'issue';
  activity: { issueId: number; activityId: number };
};

export type ProjectTimeEntry = BaseTimeEntry & {
  activityType: 'project';
  activity: { projectId: number; activityId: number };
};

export type IndirectTimeEntry = BaseTimeEntry & {
  activityType: 'indirect';
  activity: { indirectActivityId: number };
};

export type TimeEntry = IssueTimeEntry | ProjectTimeEntry | IndirectTimeEntry;

export type UpdatableTimeEntryAttributes = Pick<
  TimeEntry,
  'comments' | 'customFieldValues'
> & {
  hours: string;
};

export type IssueRowAppearance = {
  id: number;
  rowHeight: number;
  activityRowOffset: number;
};

export type RowCoordinates =
  | (Pick<IssueTimeEntry, 'activityType'> & {
      activity: IssueTimeEntry['activity'] & { projectId: number };
    })
  | Pick<ProjectTimeEntry, 'activity' | 'activityType'>
  | Pick<IndirectTimeEntry, 'activity' | 'activityType'>;

export type TimeEntryCoordinates = RowCoordinates & Pick<TimeEntry, 'date'>;

export type TimeEntryData = {
  activities: Activity[];
  indirectActivities: Activity[];
  issues: Issue[];
  projects: Project[];
  timeEntries: TimeEntry[];
  assignedIssueIds: number[];
  workingHours: WorkingHours[];
};

export type WorkDate = {
  /**
   * Format: `YYYY-MM-DD`
   */
  date: string;
  past?: boolean;
  type: 'workDay' | 'saturday' | 'holiday' | 'today';
};

export type WorkMonth = {
  /**
   * Format: `YYYY-MM`
   */
  month: string;
  dates: WorkDate[];
};
