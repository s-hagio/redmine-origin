import { css, FlattenSimpleInterpolation } from 'styled-components';

import { WorkDate } from './types';

type DateTypeColors = {
  holiday: string;
  saturday: string;
  today: string;
  workDay: string;
};

const DEFAULT_COLORS: DateTypeColors = {
  holiday: '#F3F5F6',
  saturday: '#F3F5F6',
  today: '#fcfce8',
  workDay: 'white',
};

const ISSUE_ROW_COLORS: DateTypeColors = {
  holiday: '#F3F5F6',
  saturday: '#F3F5F6',
  today: 'rgb(237, 246, 221)',
  workDay: 'rgb(239, 248, 233)',
};

const dateColor = (date: WorkDate, colors: DateTypeColors): string => {
  switch (date.type) {
    case 'holiday':
      return colors.holiday;
    case 'saturday':
      return colors.saturday;
    case 'today':
      return colors.today;
    default:
      return colors.workDay;
  }
};

type SharedComponentStyle<PropsType> = (
  props: PropsType
) => FlattenSimpleInterpolation;

export const backgroundColorOfDate: SharedComponentStyle<{
  workDate: WorkDate;
  issueRow?: boolean;
}> = ({ issueRow, workDate }) => css`
  background-color: ${dateColor(
    workDate,
    issueRow ? ISSUE_ROW_COLORS : DEFAULT_COLORS
  )};
  mix-blend-mode: multiply;
`;

export const TABLE_BORDER_STYLE = '1px solid #cccccc';
export const CELL_PADDING = 3;
export const HOURS_CELL_RIGHT_PADDING = 9;
export const LABEL_CELL_LEFT_PADDING = 6;

export const PROJECT_ROW_STYLE = `
  background-color: #e7f0fa;
  border-top-style: solid;
`;

export const TABLE_ROW_HOVER_STYLE = `background-color: rgba(85, 85, 85, 0.10)`;

export const TIME_ENTRY_CELL_WIDTH = 50;

export const TIME_ENTRY_CELL_STYLE = `
  width: ${TIME_ENTRY_CELL_WIDTH}px;
  min-width: ${TIME_ENTRY_CELL_WIDTH}px; // IE 11
`;

export const TIME_ENTRY_ROW_HEIGHT = 30;
export const TOP_EMPTY_AREA_ROW_HEIGHT = 40;
export const TABLE_HEADER_ROW_HEIGHT = 24;

export const CELL_RIGHT_MARGIN = `padding-right: 4px`;
