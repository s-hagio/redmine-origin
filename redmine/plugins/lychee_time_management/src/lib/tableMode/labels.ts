import * as React from 'react';

export type Labels = { [key: string]: string };

const LabelsContext = React.createContext<Labels>({});

export const LabelsProvider = LabelsContext.Provider;

type LabelGetter = (key: string) => string;

export const useLabels = (): LabelGetter => {
  const labels = React.useContext(LabelsContext);

  const l = React.useCallback((key: string): string => labels[key], [labels]);

  return l;
};
