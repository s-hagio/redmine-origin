import axios from 'axios';
import { camelizeKeys } from 'humps';
import {
  Attachment,
  AttachmentApiHandler,
} from '@/components/RedmineCustomField/types';

export const buildAttachmentApiHandler = (
  baseURL: string
): AttachmentApiHandler => {
  const csrfToken = (): string => {
    // csrf-token is not given if allow_forgery_protection is false.
    const node = document.getElementsByName('csrf-token')[0] as HTMLMetaElement;
    return node ? node.content : '';
  };

  const axiosInstance = axios.create({
    baseURL,
    withCredentials: true,
    headers: { 'X-CSRF-Token': csrfToken() },
  });

  return {
    createAttachment: async (file: File): Promise<Attachment> => {
      const formData = new FormData();
      formData.append('attachment[file]', file);

      const {
        data: { attachment },
      } = await axiosInstance.post(
        '/lychee_time_management/attachments.json',
        formData
      );
      return camelizeKeys(attachment) as Attachment;
    },

    deleteAttachment: async (id: number): Promise<void> => {
      await axiosInstance.delete(
        `/lychee_time_management/attachments/${id}.json`
      );
    },

    getAttachment: async (id: number): Promise<Attachment> => {
      const {
        data: { attachment },
      } = await axiosInstance.get(
        `/lychee_time_management/attachments/${id}.json`
      );

      return camelizeKeys(attachment) as Attachment;
    },
  };
};
