import { Map } from 'immutable';
import { sortIssueData } from './rowSorter';

describe('sortIssueData', () => {
  const issuesById = Map({
    1: {
      id: 1,
      subject: 'root-xxx',
      startDate: '2021-01-01',
      dueDate: '2021-01-01',
    },
    2: {
      id: 2,
      parentId: 1,
      subject: 'child-bbb',
      startDate: '2021-03-02',
      dueDate: '2021-03-02',
    },
    3: {
      id: 3,
      parentId: 1,
      subject: 'child-aaa',
      startDate: '2021-03-01',
      dueDate: '2021-03-03',
    },
    4: {
      id: 4,
      subject: 'root-aaa',
      startDate: '2021-02-01',
      dueDate: '',
    },
    5: {
      id: 5,
      parentId: 4,
      subject: 'child-bbb',
      startDate: '2021-02-03',
      dueDate: '2021-02-03',
    },
    6: {
      id: 6,
      parentId: 4,
      subject: 'child-aaa',
      startDate: '2021-02-02',
      dueDate: '2021-02-04',
    },
  }).mapKeys(key => Number(key));
  const issues = Array.from(issuesById.keys()).map(id => ({ id }));

  it('returns sorted array by subject', () => {
    const result = sortIssueData(issues, issuesById, 'subject');
    expect(result.map(({ id }) => id)).toEqual([4, 6, 5, 1, 3, 2]);
  });

  it('returns sorted array by startDate', () => {
    const result = sortIssueData(issues, issuesById, 'startDate');
    expect(result.map(({ id }) => id)).toEqual([1, 3, 2, 4, 6, 5]);
  });

  it('returns sorted array by dueDate', () => {
    const result = sortIssueData(issues, issuesById, 'dueDate');
    expect(result.map(({ id }) => id)).toEqual([4, 5, 6, 1, 2, 3]);
  });
});
