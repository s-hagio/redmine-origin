import axios from 'axios';
import * as qs from 'qs';
import { camelizeKeys, decamelizeKeys } from 'humps';
import {
  Issue,
  Project,
  Tracker,
  TimeEntry,
  TimeEntryData,
} from '@/lib/tableMode/types';
import { CustomField } from '@/components/RedmineCustomField';
import { TimeEntryDataStore } from '@/components/TableMode/Root';
import { Filter } from '@/components/TableMode/FilterTable';

const baseURL = (): string => {
  const node = document.getElementById('time-management-root');
  return node?.dataset['rootPath'] ?? '';
};

const csrfToken = (): string => {
  // csrf-token is not given if allow_forgery_protection is false.
  const node = document.getElementsByName('csrf-token')[0] as HTMLMetaElement;
  return node ? node.content : '';
};

const instance = axios.create({
  baseURL: baseURL(),
  withCredentials: true,
  headers: { 'X-CSRF-Token': csrfToken() },
});

async function processRequestErrors<T>(request: () => Promise<T>): Promise<T> {
  try {
    const result = await request();
    return result;
  } catch (axiosError: any) {
    const { response } = axiosError;
    // フロントで表示できる形に変換する
    if (response.status === 422) {
      throw { error: response.data.errors.join(',') };
    } else {
      throw { error: response.data.toString() };
    }
  }
}

export const timeEntryDataStore: TimeEntryDataStore = {
  getForMonth: async function (month, userId) {
    const { data } = await instance.get('/table_mode/time_entries.json', {
      params: decamelizeKeys({
        startDate: month.dates[0].date,
        endDate: month.dates[month.dates.length - 1].date,
        userId: userId,
      }),
    });

    return camelizeKeys(data) as TimeEntryData;
  },
  createTimeEntry: async (entry, userId) =>
    processRequestErrors(async (): Promise<TimeEntry> => {
      const { data: newEntry } = await instance.post(
        '/table_mode/time_entries.json',
        decamelizeKeys({
          ...entry,
          ...entry.activity,
          spentOn: entry.date,
          userId: userId,
        })
      );
      return camelizeKeys(newEntry) as TimeEntry;
    }),
  updateTimeEntry: async (id, attributes) =>
    processRequestErrors(async (): Promise<TimeEntry> => {
      const { data: updated } = await instance.put(
        `/table_mode/time_entries/${id}.json`,
        decamelizeKeys(attributes)
      );
      return camelizeKeys(updated) as TimeEntry;
    }),
  deleteTimeEntry: async (id) =>
    processRequestErrors(async (): Promise<void> => {
      await instance.delete(`/table_mode/time_entries/${id}.json`);
    }),
  getCustomFieldsForProject: async (projectId: number) => {
    const { data } = await instance.get('/table_mode/custom_fields.json', {
      params: decamelizeKeys({ projectId }),
    });

    return camelizeKeys(data.custom_fields) as CustomField[];
  },
  getProjectFilters: async () => {
    const {
      data: {
        grouped_filter_options: groupedFilterOptions,
        available_filters: availableFilters,
        operators_by_filter_type: operatorsByFilterType,
        operators_labels: operatorsLabels,
        queries: queries,
      },
    } = await instance.get(`/table_mode/project_filters.json`);
    return {
      groupedFilterOptions,
      availableFilters,
      operatorsByFilterType,
      operatorsLabels,
      queries,
    };
  },
  getFilterValues: async (type, name) => {
    // send HTML request for user login
    const { data: values } = await instance.get(
      `/queries/filter?type=${type}&name=${name}`
    );
    return {
      values,
    };
  },
  getProjects: async ({
    userId,
    queryId,
    filters,
    page,
  }: {
    userId: number;
    queryId: string;
    filters: Filter[];
    page: number;
  }) => {
    const f = filters.map(({ field }) => field);
    const op: { [field: string]: Filter['operator'] } = {};
    const v: { [field: string]: Filter['values'] } = {};

    filters.forEach((filter) => {
      op[filter.field] = filter.operator;
      v[filter.field] = filter.values;
    });

    const params = qs.stringify(
      { f, op, v, page, query_id: queryId, user_id: userId },
      { arrayFormat: 'brackets' }
    );
    const {
      data: { projects, queried_filters, total_count, offset, limit },
    } = await instance.get(`/table_mode/projects.json?${params}`);
    return {
      projects,
      queriedFilters: queried_filters,
      totalCount: total_count,
      offset,
      limit,
    };
  },
  getAvailableFilters: async () => {
    const {
      data: {
        grouped_filter_options: groupedFilterOptions,
        available_filters: availableFilters,
        operators_by_filter_type: operatorsByFilterType,
        operators_labels: operatorsLabels,
        queries: queries,
      },
    } = await instance.get(`/table_mode/filters.json`);
    return {
      groupedFilterOptions,
      availableFilters,
      operatorsByFilterType,
      operatorsLabels,
      queries,
    };
  },
  getFilteredIssues: async (
    filters: Filter[],
    page: number,
    queryId: string,
    userId: number
  ) => {
    const f = filters.map(({ field }) => field);
    const op: { [field: string]: Filter['operator'] } = {};
    const v: { [field: string]: Filter['values'] } = {};

    filters.forEach((filter) => {
      op[filter.field] = filter.operator;
      v[filter.field] = filter.values;
    });

    const params = qs.stringify(
      { f, op, v, page, query_id: queryId, user_id: userId },
      { arrayFormat: 'brackets' }
    );
    const {
      data: {
        queried_filters,
        issues,
        ancestor_issues,
        projects,
        trackers,
        total_count,
        offset,
        limit,
      },
    } = await instance.get(`/table_mode/issues.json?${params}`);

    return {
      queriedFilters: camelizeKeys(queried_filters) as Filter[],
      issues: camelizeKeys(issues) as Issue[],
      ancestorIssues: camelizeKeys(ancestor_issues) as Issue[],
      projects: camelizeKeys(projects) as Project[],
      trackers: camelizeKeys(trackers) as Tracker[],
      totalCount: total_count,
      offset,
      limit,
    };
  },
  getProjectActivities: async (
    projectIds: string,
  ) => {
    const params = qs.stringify({ project_ids: projectIds });
    const {
      data: {
        activities
      }
    } = await instance.get(`/table_mode/activities.json?${params}`);

    return {
      activities,
    };
  },
  getInheritProjects: async (
    userId: number,
  ) => {
    const params = qs.stringify({ user_id: userId });
    const {
      data: {
        projects,
        activities,
        project_activities,
      }
    } = await instance.get(`/table_mode/projects/inherit.json?${params}`);

    return {
      projects,
      activities,
      project_activities,
    };
  },
};
