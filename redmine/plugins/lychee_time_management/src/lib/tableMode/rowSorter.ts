import { List, Map } from 'immutable';

export const sortProjectData = <
  T extends { id: number },
  U extends { id: number; name: string; lft: number }
>(
  array: T[],
  mapById: Map<T['id'], U>
): T[] => {
  return [...array].sort((a, b) => {
    const left = mapById.get(a.id);
    const right = mapById.get(b.id);
    if (!left || !right) {
      return 0;
    }
    return left.lft - right.lft;
  });
};

export const sortActivities = <
  T extends { id: number },
  U extends { id: number; name: string; position: number }
>(
  array: T[],
  mapById: Map<T['id'], U>
): T[] => {
  return [...array].sort((a, b) => {
    const left = mapById.get(a.id);
    const right = mapById.get(b.id);
    if (!left || !right) {
      return 0;
    }
    return left.position - right.position;
  });
};

const compareStrings = (a: string, b: string): number => {
  const nameA = a.toUpperCase();
  const nameB = b.toUpperCase();
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }
  return 0;
};

export const availableSortKeys = ['subject', 'startDate', 'dueDate'] as const;
export type IssueSortKey = 'subject' | 'startDate' | 'dueDate';

const compareFunction = <
  T extends { id: number } & { [key in IssueSortKey]: string | null }
>(
  mapById: Map<T['id'], T>,
  sortKey: IssueSortKey
) => (a: T, b: T): number => {
  const left = mapById.get(a.id);
  const right = mapById.get(b.id);
  if (!left || !right) {
    return 0;
  }
  return compareStrings(left[sortKey] ?? '', right[sortKey] ?? '');
};

const sortedIds = <
  T extends { id: number; parentId?: number } & {
    [key in IssueSortKey]: string | null;
  }
>(
  parentId: number | undefined,
  mapById: Map<T['id'], T>,
  sortKey: IssueSortKey
): List<number> => {
  const ids = mapById
    .toList()
    .filter(issue => issue.parentId === parentId)
    .sort(compareFunction(mapById, sortKey));
  return ids.flatMap(({ id }) => sortedIds(id, mapById, sortKey).unshift(id));
};

export const sortIssueData = <
  T extends { id: number },
  U extends {
    id: number;
    parentId?: number;
  } & { [key in IssueSortKey]: string | null }
>(
  array: T[],
  mapById: Map<T['id'], U>,
  sortKey: IssueSortKey
): T[] => {
  const ids = sortedIds(undefined, mapById, sortKey);
  return [...array].sort((a, b) => ids.indexOf(a.id) - ids.indexOf(b.id));
};
