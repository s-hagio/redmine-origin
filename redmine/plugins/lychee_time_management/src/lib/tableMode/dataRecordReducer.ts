import * as React from 'react';

type DataRecord = { id: number };

type DataRecordStoreState<DataRecordType extends DataRecord> = {
  [id: number]: DataRecordType;
};

type DataRecordStoreActions<DataRecordType extends DataRecord> = {
  type: 'setAll';
  records: DataRecordType[];
};

export const createRecordReducer = function<
  DataRecordType extends DataRecord
>(): React.Reducer<
  DataRecordStoreState<DataRecordType>,
  DataRecordStoreActions<DataRecordType>
> {
  return (_state, action): DataRecordStoreState<DataRecordType> => {
    switch (action.type) {
      case 'setAll':
        const newState: DataRecordStoreState<DataRecordType> = {};
        action.records.forEach(record => (newState[record.id] = record));
        return newState;
    }
  };
};

export const allRecords = function<DataRecordType extends DataRecord>(
  state: DataRecordStoreState<DataRecordType>
): DataRecordType[] {
  return Object.values(state);
};
