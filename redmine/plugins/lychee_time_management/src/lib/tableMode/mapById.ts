import { Map } from 'immutable';

type ObjectWithId = { id: unknown };

export type MapById<T extends ObjectWithId> = Map<T['id'], T>;

export const buildMapById = function<T extends ObjectWithId>(
  items: T[]
): MapById<T> {
  return Map(items.map(item => [item.id, item]));
};
