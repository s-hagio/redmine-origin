import { Map } from 'immutable';

import { TimeEntry, TimeEntryId } from '@/lib/tableMode/types';

import { buildMapById } from './mapById';
import { TableDataAction } from './tableData';

export type TimeEntries = {
  records: Map<TimeEntryId, TimeEntry>;
  tableDataUpdate?: TableDataAction;
};

export const initTimeEntries = (): TimeEntries => ({
  records: Map(),
});

type TimeEntriesAction =
  | { type: 'init'; timeEntries: TimeEntry[] }
  | { type: 'set'; timeEntry: TimeEntry }
  | { type: 'remove'; id: TimeEntryId };

const tableDataUpdateForSet = (
  { records }: TimeEntries,
  timeEntry: TimeEntry
): TableDataAction => {
  if (records.has(timeEntry.id)) {
    const oldTimeEntry = records.get(timeEntry.id) as TimeEntry;
    const hoursDifference = timeEntry.hours - oldTimeEntry.hours;
    return {
      type: 'updateTimeEntryHoursBy',
      hoursDifference,
      timeEntry,
    };
  } else {
    return { type: 'addTimeEntry', timeEntry };
  }
};

export const timeEntriesReducer = (
  timeEntries: TimeEntries,
  action: TimeEntriesAction
): TimeEntries => {
  const { records } = timeEntries;
  switch (action.type) {
    case 'init':
      return { ...timeEntries, records: buildMapById(action.timeEntries) };
    case 'set':
      return {
        records: records.set(action.timeEntry.id, action.timeEntry),
        tableDataUpdate: tableDataUpdateForSet(timeEntries, action.timeEntry),
      };
    case 'remove':
      return {
        records: records.remove(action.id),
        tableDataUpdate: {
          type: 'removeTimeEntry',
          timeEntry: records.get(action.id) as TimeEntry,
        },
      };
  }
};
