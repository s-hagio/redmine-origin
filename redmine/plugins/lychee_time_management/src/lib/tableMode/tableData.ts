import produce, { Draft } from 'immer';
import {
  Entry,
  Issue,
  IndirectTimeEntry,
  IssueTimeEntry,
  ProjectTimeEntry,
  TimeEntry,
  TimeEntryId,
  WorkMonth,
  WorkingHours,
} from './types';

export type CellData = {
  sum: number;
  entries: TimeEntryId[];
};

export type ActivityRowData = {
  id: number;
  sum: number;
  cells: CellData[];
};

type WithSums = {
  sum: number;
  sumByDate: number[];
};

type WithActivityRows = {
  activityRows: ActivityRowData[];
};

export type IssueData = {
  id: number;
  projectId: number;
} & WithActivityRows &
  WithSums;

export type ProjectData = {
  id: number;
  dataForIssues: IssueData[];
} & WithActivityRows &
  WithSums;

export type IndirectActivities = WithActivityRows & WithSums;

type WithColumnIndex<T> = T & { columnIndex: number };

export type TableData = {
  dataForProjects: ProjectData[];
  indirectActivities: IndirectActivities;
  issueProjectIds: { [issueId: number]: number };
  columnIndexOf: (date: string) => number;
  sumWorkingHours: number;
  enableWorkingHours: boolean;
  workingHoursByDate: number[];
} & WithSums;

export const buildTableData = (): TableData => ({
  sum: 0,
  sumByDate: [],
  dataForProjects: [],
  indirectActivities: {
    activityRows: [],
    sum: 0,
    sumByDate: [],
  },
  issueProjectIds: {},
  columnIndexOf: (): number => 0,
  sumWorkingHours: 0,
  enableWorkingHours: false,
  workingHoursByDate: [],
});

type InitAction = {
  type: 'init';
  timeEntries: TimeEntry[];
  issues: Issue[];
  workMonth: WorkMonth;
  assignedIssueIds: number[];
  workingHours: WorkingHours[];
};

type ClearAction = {
  type: 'clear';
};

type AddTimeEntryAction = { type: 'addTimeEntry'; timeEntry: TimeEntry };

type AddIssueActivityAction = {
  type: 'addIssueActivity';
  activity: IssueTimeEntry['activity'];
};

type AddProjectActivityAction = {
  type: 'addProjectActivity';
  activity: ProjectTimeEntry['activity'];
};

type AddIndirectActivityAction = {
  type: 'addIndirectActivity';
  activity: IndirectTimeEntry['activity'];
};

type UpdateTimeEntryHoursByAction = {
  type: 'updateTimeEntryHoursBy';
  timeEntry: TimeEntry;
  hoursDifference: number;
};

type RemoveTimeEntryAction = {
  type: 'removeTimeEntry';
  timeEntry: TimeEntry;
};

type AddProjectRows = {
  type: 'addProjectRows';
  projectIds: number[];
};

type AddIssueRows = {
  type: 'addIssueRows';
  issueProjectIds: { [id: number]: number };
};

export type TableDataAction =
  | InitAction
  | ClearAction
  | AddTimeEntryAction
  | AddIssueActivityAction
  | AddProjectActivityAction
  | AddIndirectActivityAction
  | UpdateTimeEntryHoursByAction
  | RemoveTimeEntryAction
  | AddProjectRows
  | AddIssueRows;

const newSums = (): Array<number> => new Array(31).fill(0);

const emptyState = produce(
  (tableData: Draft<TableData>, { issues, workMonth }: InitAction) => {
    tableData.sum = 0;
    tableData.sumByDate = newSums();
    tableData.dataForProjects = [];
    tableData.indirectActivities = {
      activityRows: [],
      sum: 0,
      sumByDate: newSums(),
    };
    tableData.enableWorkingHours = false;
    tableData.workingHoursByDate = newSums();
    tableData.issueProjectIds = {};
    issues.forEach(
      (issue) => (tableData.issueProjectIds[issue.id] = issue.projectId)
    );
    const dateIndexes: { [date: string]: number } = {};
    workMonth.dates.forEach((date, index) => {
      dateIndexes[date.date] = index;
    });
    tableData.columnIndexOf = (date: string): number =>
      dateIndexes[date];
  }
);

const addColumnIndex = function <T extends Entry>(
  tableData: TableData,
  entry: T
): WithColumnIndex<T> {
  return {
    ...entry,
    columnIndex: tableData.columnIndexOf(entry.date),
  };
};

const projectIdOfIssue = (tableData: TableData, issueId: number): number =>
  tableData.issueProjectIds[issueId];

const findIndexOfId = (items: Array<{ id: number }>, id: number): number =>
  items.findIndex((item) => item.id === id);

const buildCell = (): CellData => ({ sum: 0, entries: [] });

const buildCells = (size: number): CellData[] =>
  new Array(size).fill(0).map(() => buildCell());

const buildRow = (id: number, columns: number): ActivityRowData => ({
  id,
  sum: 0,
  cells: buildCells(columns),
});

type ActivityRowIndex = { activityIndex: number };
type ProjectRowIndex = { projectIndex: number };
type IssueRowIndex = ProjectRowIndex & { issueIndex: number };
type IssueActivityRowIndex = IssueRowIndex & ActivityRowIndex;
type ProjectActivityRowIndex = ProjectRowIndex & ActivityRowIndex;
type IndirectActivityRowIndex = ActivityRowIndex;

const rowIndexOfProject = (
  tableData: TableData,
  projectId: number
): ProjectRowIndex => ({
  projectIndex: findIndexOfId(tableData.dataForProjects, projectId),
});

const projectDataAtIndex = (
  tableData: TableData,
  { projectIndex }: ProjectRowIndex
): ProjectData | undefined => tableData.dataForProjects[projectIndex];

const rowIndexOfIssue = (
  tableData: TableData,
  issueId: number
): IssueRowIndex => {
  const projectRowIndex = rowIndexOfProject(
    tableData,
    projectIdOfIssue(tableData, issueId)
  );
  const projectData = projectDataAtIndex(tableData, projectRowIndex);

  return {
    ...projectRowIndex,
    issueIndex: projectData
      ? findIndexOfId(projectData.dataForIssues, issueId)
      : -1,
  };
};

const issueDataAtIndex = (
  tableData: TableData,
  issueRowIndex: IssueRowIndex
): IssueData | undefined =>
  projectDataAtIndex(tableData, issueRowIndex)?.dataForIssues[
    issueRowIndex.issueIndex
  ];

const rowIndexOfIssueActivity = (
  tableData: TableData,
  { issueId, activityId }: IssueTimeEntry['activity']
): IssueActivityRowIndex => {
  const issueRowIndex = rowIndexOfIssue(tableData, issueId);
  const issueData = issueDataAtIndex(tableData, issueRowIndex);

  return {
    ...issueRowIndex,
    activityIndex: issueData
      ? findIndexOfId(issueData.activityRows, activityId)
      : -1,
  };
};

const issueActivityRowAtIndex = (
  tableData: TableData,
  issueActivityRowIndex: IssueActivityRowIndex
): ActivityRowData | undefined =>
  issueDataAtIndex(tableData, issueActivityRowIndex)?.activityRows[
    issueActivityRowIndex.activityIndex
  ];

const rowIndexOfProjectActivity = (
  tableData: TableData,
  { projectId, activityId }: ProjectTimeEntry['activity']
): ProjectActivityRowIndex => {
  const projectRowIndex = rowIndexOfProject(tableData, projectId);
  const projectData = projectDataAtIndex(tableData, projectRowIndex);

  return {
    ...projectRowIndex,
    activityIndex: projectData
      ? findIndexOfId(projectData.activityRows, activityId)
      : -1,
  };
};

const projectActivityRowAtIndex = (
  tableData: TableData,
  projectActivityRowIndex: ProjectActivityRowIndex
): ActivityRowData | undefined =>
  projectDataAtIndex(tableData, projectActivityRowIndex)?.activityRows[
    projectActivityRowIndex.activityIndex
  ];

const rowIndexOfIndirectActivity = (
  tableData: TableData,
  { indirectActivityId }: IndirectTimeEntry['activity']
): IndirectActivityRowIndex => ({
  activityIndex: findIndexOfId(
    tableData.indirectActivities.activityRows,
    indirectActivityId
  ),
});

const indirectActivityRowAtIndex = (
  tableData: TableData,
  indirectActivityRowIndex: IndirectActivityRowIndex
): ActivityRowData | undefined =>
  tableData.indirectActivities.activityRows[
    indirectActivityRowIndex.activityIndex
  ];

const addMissingProjectData = (
  tableData: TableData,
  projectId: number
): TableData => {
  const { projectIndex } = rowIndexOfProject(tableData, projectId);
  if (projectIndex !== -1) {
    return tableData;
  }

  return produce(tableData, (result) => {
    result.dataForProjects.push({
      id: projectId,
      sum: 0,
      sumByDate: newSums(),
      activityRows: [],
      dataForIssues: [],
    });
  });
};

const addMissingIssueData = (
  tableData: TableData,
  issueId: number
): TableData => {
  const projectId = projectIdOfIssue(tableData, issueId);
  const tableDataWithProjectData = addMissingProjectData(tableData, projectId);

  const issueRowIndex = rowIndexOfIssue(tableDataWithProjectData, issueId);

  if (issueRowIndex.issueIndex !== -1) {
    return tableDataWithProjectData;
  }

  return produce(tableDataWithProjectData, (result) => {
    const projectData = projectDataAtIndex(
      result,
      issueRowIndex
    ) as ProjectData;
    projectData.dataForIssues.push({
      id: issueId,
      projectId,
      sum: 0,
      sumByDate: newSums(),
      activityRows: [],
    });
  });
};

const addMissingIssueActivityRow = (
  tableData: TableData,
  activity: IssueTimeEntry['activity']
): TableData => {
  const tableDataWithIssueData = addMissingIssueData(
    tableData,
    activity.issueId
  );
  const issueActivityRowIndex = rowIndexOfIssueActivity(
    tableDataWithIssueData,
    activity
  );
  if (issueActivityRowIndex.activityIndex !== -1) {
    return tableDataWithIssueData;
  }

  return produce(tableDataWithIssueData, (result) => {
    const issueData = issueDataAtIndex(
      result,
      issueActivityRowIndex
    ) as IssueData;
    issueData.activityRows.push(
      buildRow(activity.activityId, issueData.sumByDate.length)
    );
  });
};

const addMissingProjectActivityRow = (
  tableData: TableData,
  activity: ProjectTimeEntry['activity']
): TableData => {
  const tableDataWithProjectData = addMissingProjectData(
    tableData,
    activity.projectId
  );
  const projectActivityRowIndex = rowIndexOfProjectActivity(
    tableDataWithProjectData,
    activity
  );
  if (projectActivityRowIndex.activityIndex !== -1) {
    return tableDataWithProjectData;
  }

  return produce(tableDataWithProjectData, (result) => {
    const projectData = projectDataAtIndex(
      result,
      projectActivityRowIndex
    ) as ProjectData;
    projectData.activityRows.push(
      buildRow(activity.activityId, projectData.sumByDate.length)
    );
  });
};

const addMissingIndirectActivityRow = (
  tableData: TableData,
  activity: IndirectTimeEntry['activity']
): TableData => {
  const indirectActivityRowIndex = rowIndexOfIndirectActivity(
    tableData,
    activity
  );

  if (indirectActivityRowIndex.activityIndex !== -1) {
    return tableData;
  }

  return produce(tableData, (result) => {
    result.indirectActivities.activityRows.push(
      buildRow(activity.indirectActivityId, tableData.sumByDate.length)
    );
  });
};

const addHoursToSums = (
  rowWithSums: Draft<WithSums>,
  columnIndex: number,
  hours: number
): void => {
  rowWithSums.sumByDate[columnIndex] += hours;
  rowWithSums.sum += hours;
};

const addTotalHours = (
  tableData: TableData,
  columnIndex: number,
  hours: number
): TableData =>
  produce(tableData, (result) => {
    addHoursToSums(result, columnIndex, hours);
  });

const addProjectHours = (
  tableData: TableData,
  projectRowIndex: ProjectRowIndex,
  columnIndex: number,
  hours: number
): TableData => {
  const updatedTableData = produce(tableData, (result) => {
    const projectData = projectDataAtIndex(
      result,
      projectRowIndex
    ) as ProjectData;
    addHoursToSums(projectData, columnIndex, hours);
  });

  return addTotalHours(updatedTableData, columnIndex, hours);
};

const addIssueActivityHours = (
  tableData: TableData,
  issueActivityRowIndex: IssueActivityRowIndex,
  columnIndex: number,
  hours: number
): TableData => {
  const updatedTableData = produce(tableData, (result) => {
    const row = issueActivityRowAtIndex(
      result,
      issueActivityRowIndex
    ) as ActivityRowData;
    row.cells[columnIndex].sum += hours;
    row.sum += hours;
    const issueData = issueDataAtIndex(
      result,
      issueActivityRowIndex
    ) as IssueData;
    addHoursToSums(issueData, columnIndex, hours);
  });

  return addProjectHours(
    updatedTableData,
    issueActivityRowIndex,
    columnIndex,
    hours
  );
};

const addProjectActivityHours = (
  tableData: TableData,
  projectActivityRowIndex: ProjectActivityRowIndex,
  columnIndex: number,
  hours: number
): TableData => {
  const updatedTableData = produce(tableData, (result) => {
    const row = projectActivityRowAtIndex(
      result,
      projectActivityRowIndex
    ) as ActivityRowData;
    row.cells[columnIndex].sum += hours;
    row.sum += hours;
  });

  return addProjectHours(
    updatedTableData,
    projectActivityRowIndex,
    columnIndex,
    hours
  );
};

const addIndirectActivityHours = (
  tableData: TableData,
  indirectActivityRowIndex: IndirectActivityRowIndex,
  columnIndex: number,
  hours: number
): TableData => {
  const updatedTableData = produce(tableData, (result) => {
    const row = indirectActivityRowAtIndex(
      result,
      indirectActivityRowIndex
    ) as ActivityRowData;
    row.cells[columnIndex].sum += hours;
    row.sum += hours;
    addHoursToSums(result.indirectActivities, columnIndex, hours);
  });

  return addTotalHours(updatedTableData, columnIndex, hours);
};

const addIssueTimeEntry = (
  tableData: TableData,
  timeEntry: WithColumnIndex<IssueTimeEntry>
): TableData => {
  let updatedTableData = addMissingIssueActivityRow(
    tableData,
    timeEntry.activity
  );

  const issueActivityRowIndex = rowIndexOfIssueActivity(
    updatedTableData,
    timeEntry.activity
  );

  updatedTableData = produce(updatedTableData, (result) => {
    const activityRow = issueActivityRowAtIndex(
      result,
      issueActivityRowIndex
    ) as ActivityRowData;
    activityRow.cells[timeEntry.columnIndex].entries.push(timeEntry.id);
  });

  return addIssueActivityHours(
    updatedTableData,
    issueActivityRowIndex,
    timeEntry.columnIndex,
    timeEntry.hours
  );
};

const addProjectTimeEntry = (
  tableData: TableData,
  timeEntry: WithColumnIndex<ProjectTimeEntry>
): TableData => {
  let updatedTableData = addMissingProjectActivityRow(
    tableData,
    timeEntry.activity
  );

  const projectActivityRowIndex = rowIndexOfProjectActivity(
    updatedTableData,
    timeEntry.activity
  );

  updatedTableData = produce(updatedTableData, (result) => {
    const activityRow = projectActivityRowAtIndex(
      result,
      projectActivityRowIndex
    ) as ActivityRowData;
    activityRow.cells[timeEntry.columnIndex].entries.push(timeEntry.id);
  });

  return addProjectActivityHours(
    updatedTableData,
    projectActivityRowIndex,
    timeEntry.columnIndex,
    timeEntry.hours
  );
};

const addIndirectTimeEntry = (
  tableData: TableData,
  timeEntry: WithColumnIndex<IndirectTimeEntry>
): TableData => {
  let updatedTableData = addMissingIndirectActivityRow(
    tableData,
    timeEntry.activity
  );

  const indirectActivityRowIndex = rowIndexOfIndirectActivity(
    updatedTableData,
    timeEntry.activity
  );

  updatedTableData = produce(updatedTableData, (result) => {
    const activityRow = indirectActivityRowAtIndex(
      result,
      indirectActivityRowIndex
    ) as ActivityRowData;
    activityRow.cells[timeEntry.columnIndex].entries.push(timeEntry.id);
  });

  return addIndirectActivityHours(
    updatedTableData,
    indirectActivityRowIndex,
    timeEntry.columnIndex,
    timeEntry.hours
  );
};

const addTimeEntry = (
  tableData: TableData,
  timeEntry: TimeEntry
): TableData => {
  const timeEntryWithColumnIndex = addColumnIndex(tableData, timeEntry);
  switch (timeEntryWithColumnIndex.activityType) {
    case 'issue':
      return addIssueTimeEntry(tableData, timeEntryWithColumnIndex);
    case 'project':
      return addProjectTimeEntry(tableData, timeEntryWithColumnIndex);
    case 'indirect':
      return addIndirectTimeEntry(tableData, timeEntryWithColumnIndex);
    default:
      return tableData;
  }
};

const addWorkingHours = (
  tableData: TableData,
  workingHours: WorkingHours
): TableData => {
  const workingHoursWithColumnIndex = addColumnIndex(tableData, workingHours);
  const updatedTableData = produce(tableData, (result) => {
    result.workingHoursByDate = [...tableData.workingHoursByDate];
    result.workingHoursByDate[workingHoursWithColumnIndex.columnIndex] = workingHours.hours;
    result.sumWorkingHours += workingHours.hours;
  });
  return updatedTableData;
};

const updateTimeEntryHours = (
  tableData: TableData,
  { timeEntry, hoursDifference }: UpdateTimeEntryHoursByAction
): TableData => {
  const timeEntryWithColumnIndex = addColumnIndex(tableData, timeEntry);
  const { columnIndex } = timeEntryWithColumnIndex;

  switch (timeEntryWithColumnIndex.activityType) {
    case 'issue':
      const issueActivityRowIndex = rowIndexOfIssueActivity(
        tableData,
        timeEntryWithColumnIndex.activity
      );
      return addIssueActivityHours(
        tableData,
        issueActivityRowIndex,
        columnIndex,
        hoursDifference
      );
    case 'project':
      const projectActivityRowIndex = rowIndexOfProjectActivity(
        tableData,
        timeEntryWithColumnIndex.activity
      );
      return addProjectActivityHours(
        tableData,
        projectActivityRowIndex,
        columnIndex,
        hoursDifference
      );
    case 'indirect':
      const indirectActivityRowIndex = rowIndexOfIndirectActivity(
        tableData,
        timeEntryWithColumnIndex.activity
      );
      return addIndirectActivityHours(
        tableData,
        indirectActivityRowIndex,
        columnIndex,
        hoursDifference
      );
  }
};

const removeIssueTimeEntry = (
  tableData: TableData,
  { activity, columnIndex, id, hours }: WithColumnIndex<IssueTimeEntry>
): TableData => {
  const issueActivityRowIndex = rowIndexOfIssueActivity(tableData, activity);

  const updatedTableData = produce(tableData, (result) => {
    const activityRow = issueActivityRowAtIndex(
      result,
      issueActivityRowIndex
    ) as ActivityRowData;
    const cell = activityRow.cells[columnIndex];
    cell.entries = cell.entries.filter((entryId) => entryId !== id);
  });

  return addIssueActivityHours(
    updatedTableData,
    issueActivityRowIndex,
    columnIndex,
    -hours
  );
};

const removeProjectTimeEntry = (
  tableData: TableData,
  { activity, columnIndex, id, hours }: WithColumnIndex<ProjectTimeEntry>
): TableData => {
  const projectActivityRowIndex = rowIndexOfProjectActivity(
    tableData,
    activity
  );

  const updatedTableData = produce(tableData, (result) => {
    const activityRow = projectActivityRowAtIndex(
      result,
      projectActivityRowIndex
    ) as ActivityRowData;
    const cell = activityRow.cells[columnIndex];
    cell.entries = cell.entries.filter((entryId) => entryId !== id);
  });

  return addProjectActivityHours(
    updatedTableData,
    projectActivityRowIndex,
    columnIndex,
    -hours
  );
};

const removeIndirectTimeEntry = (
  tableData: TableData,
  { activity, columnIndex, id, hours }: WithColumnIndex<IndirectTimeEntry>
): TableData => {
  const indirectActivityRowIndex = rowIndexOfIndirectActivity(
    tableData,
    activity
  );

  const updatedTableData = produce(tableData, (result) => {
    const activityRow = indirectActivityRowAtIndex(
      result,
      indirectActivityRowIndex
    ) as ActivityRowData;
    const cell = activityRow.cells[columnIndex];
    cell.entries = cell.entries.filter((entryId) => entryId !== id);
  });

  return addIndirectActivityHours(
    updatedTableData,
    indirectActivityRowIndex,
    columnIndex,
    -hours
  );
};

const removeTimeEntry = (
  tableData: TableData,
  timeEntry: TimeEntry
): TableData => {
  const timeEntryWithColumnIndex = addColumnIndex(tableData, timeEntry);

  switch (timeEntryWithColumnIndex.activityType) {
    case 'issue':
      return removeIssueTimeEntry(tableData, timeEntryWithColumnIndex);
    case 'project':
      return removeProjectTimeEntry(tableData, timeEntryWithColumnIndex);
    case 'indirect':
      return removeIndirectTimeEntry(tableData, timeEntryWithColumnIndex);
  }
};

const addProjectRows = (
  tableData: TableData,
  projectIds: number[]
): TableData => {
  let newData = tableData;
  projectIds.forEach((projectId) => {
    newData = addMissingProjectData(newData, projectId);
  });
  return newData;
};

const addIssueRows = (
  tableData: TableData,
  issueProjectIds: { [id: number]: number }
): TableData => {
  let newData = produce(tableData, (draft) => {
    draft.issueProjectIds = {
      ...draft.issueProjectIds,
      ...issueProjectIds,
    };
  });
  Object.keys(issueProjectIds).forEach((id) => {
    newData = addMissingIssueData(newData, Number(id));
  });
  return newData;
};

export const tableDataReducer = (
  tableData: TableData,
  action: TableDataAction
): TableData => {
  switch (action.type) {
    case 'clear':
      return buildTableData();
    case 'init':
      let result = emptyState(tableData, action);
      action.timeEntries.forEach((timeEntry) => {
        result = addTimeEntry(result, timeEntry);
      });
      action.assignedIssueIds.forEach((issueId) => {
        result = addMissingIssueData(result, issueId);
      });
      if (action.workingHours) {
        result = {...result, enableWorkingHours: true};
        action.workingHours.forEach((workingHours) => {
          result = addWorkingHours(result, workingHours);
        });
      }
      return result;
    case 'addTimeEntry':
      return addTimeEntry(tableData, action.timeEntry);
    case 'updateTimeEntryHoursBy':
      return updateTimeEntryHours(tableData, action);
    case 'removeTimeEntry':
      return removeTimeEntry(tableData, action.timeEntry);
    case 'addIssueActivity':
      return addMissingIssueActivityRow(tableData, action.activity);
    case 'addProjectActivity':
      return addMissingProjectActivityRow(tableData, action.activity);
    case 'addIndirectActivity':
      return addMissingIndirectActivityRow(tableData, action.activity);
    case 'addProjectRows': {
      return addProjectRows(tableData, action.projectIds);
    }
    case 'addIssueRows': {
      return addIssueRows(tableData, action.issueProjectIds);
    }
  }
};
