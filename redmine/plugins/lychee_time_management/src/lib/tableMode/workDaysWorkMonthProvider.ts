import * as moment from 'moment';

import { WorkMonthProvider } from '@/components/TableMode/Root';
import { WorkDate, WorkMonth } from './types';

/**
 * 0 = Sunday, 1 = Monday, ..., 6 = Saturday
 */
export type Weekday = 0 | 1 | 2 | 3 | 4 | 5 | 6;

/**
 * Format: `YYYY-MM-DD`
 */
export type DateString = string;

export type ClosingDay = 'end_of_month' | '5' | '10' | '15' | '20' | '25';

class WorkDaysWorkMonthProvider implements WorkMonthProvider {
  private today: moment.Moment;
  private nonWorkingWeekdays: Weekday[];
  private holidays: moment.Moment[];
  private closingDay: ClosingDay;

  // NonWorkingWeekdays
  constructor(
    nonWorkingWeekdays: Weekday[],
    holidays: DateString[],
    closingDay: ClosingDay
  ) {
    this.today = moment();
    this.nonWorkingWeekdays = nonWorkingWeekdays || [];
    this.holidays = holidays.map((date) => moment(date)) || [];
    this.closingDay = closingDay || 'end_of_month';
  }

  getCurrent(): WorkMonth {
    const today = moment().startOf('day');
    return this.buildMonth(
      today.isSameOrBefore(this.closingDayInSameMonth(today))
        ? today
        : today.add(1, 'month')
    );
  }

  getToday(): string {
    return this.today.format('YYYY-MM-DD');
  }

  getNextMonth(month: WorkMonth): WorkMonth {
    const nextMonth = moment(month.month).add(1, 'month');
    return this.buildMonth(nextMonth);
  }

  getPreviousMonth(month: WorkMonth): WorkMonth {
    const previousMonth = moment(month.month).subtract(1, 'month');
    return this.buildMonth(previousMonth);
  }

  private buildMonth(date: moment.Moment): WorkMonth {
    return {
      month: date.format('YYYY-MM'),
      dates: this.buildDates(date),
    };
  }

  private buildDates(date: moment.Moment): WorkDate[] {
    const result: WorkDate[] = [];

    const startDate = this.closingDayInSameMonth(
      moment(date).subtract(1, 'month')
    ).add(1, 'day');
    const endDate = this.closingDayInSameMonth(date);

    const nextDate = startDate;
    while (nextDate.isSameOrBefore(endDate, 'day')) {
      const workDate: WorkDate = {
        type: this.dateType(nextDate),
        date: nextDate.format('YYYY-MM-DD'),
      };
      if (nextDate.isBefore(this.today, 'day')) {
        workDate.past = true;
      }
      result.push(workDate);
      nextDate.add(1, 'day');
    }

    return result;
  }

  private closingDayInSameMonth(date: moment.Moment): moment.Moment {
    return this.closingDay === 'end_of_month'
      ? date.clone().endOf('month')
      : date.clone().date(Number(this.closingDay));
  }

  private dateType(date: moment.Moment): WorkDate['type'] {
    if (date.isSame(this.today, 'day')) {
      return 'today';
    }

    if (
      this.nonWorkingWeekdays.includes(date.day() as Weekday) ||
      this.holidays.some((holiday) => holiday.isSame(date, 'day'))
    ) {
      if (date.day() == 6) {
        return 'saturday';
      }

      return 'holiday';
    }

    return 'workDay';
  }
}

export default WorkDaysWorkMonthProvider;
