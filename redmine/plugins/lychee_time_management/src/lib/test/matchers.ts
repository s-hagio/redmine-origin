import { waitFor } from '@testing-library/react';

export const expectTextContents = (
  elements: HTMLElement[],
  textContents: (string | RegExp)[]
): void => {
  expect(elements).toHaveLength(textContents.length);
  elements.forEach((element, index) =>
    expect(element).toHaveTextContent(textContents[index])
  );
};

export const expectBackgroundColors = (
  elements: HTMLElement[],
  backgroundColors: string[]
): void => {
  expect(elements).toHaveLength(backgroundColors.length);
  elements.forEach((element, index) =>
    expect(element).toHaveStyle(`background-color: ${backgroundColors[index]}`)
  );
};

export const expectEmptyText = async (element: HTMLElement): Promise<void> =>
  await waitFor(() => {
    expect(element).not.toHaveTextContent(/.+/);
  });

export const expectNotFocusCell = (cell: HTMLElement) => {
  expect(cell.attributes.getNamedItem('tabIndex')?.value).toBe('-1');
};
export const expectFocusCell = (cell: HTMLElement) => {
  expect(cell.attributes.getNamedItem('tabIndex')?.value).toBe('0');
};
