import {
  BoundFunctions,
  ByRoleOptions,
  queries,
  screen,
  within,
} from '@testing-library/react';

export const withinElementOrScreen = (
  element?: HTMLElement
): BoundFunctions<typeof queries> => (element ? within(element) : screen);

type QueryOptions = ByRoleOptions & { withinElement?: HTMLElement };

export const buildElementQueries = (
  role: string,
  options?: ByRoleOptions
): [
  (queryOptions?: QueryOptions) => HTMLElement,
  (queryOptions?: QueryOptions) => HTMLElement | null
] => {
  return [
    (queryOptions): HTMLElement => {
      const { withinElement, ...overrideOptions } = queryOptions || {};

      return withinElementOrScreen(withinElement).getByRole(role, {
        ...options,
        ...overrideOptions,
      });
    },
    (queryOptions): HTMLElement | null => {
      const { withinElement, ...overrideOptions } = queryOptions || {};

      return withinElementOrScreen(withinElement).queryByRole(role, {
        ...options,
        ...overrideOptions,
      });
    },
  ];
};

// Workaround: menu's aria-label is ignored
// https://github.com/eps1lon/dom-accessibility-api/blob/main/sources/accessible-name-and-description.ts#L547-L552
// https://github.com/w3c/accname/issues/67
export const getMenuWithName = (name: string): HTMLElement =>
  screen.getByRole('menu', {
    name: (_, element) =>
      element.attributes.getNamedItem('aria-label')?.value === name,
  });
