import * as React from 'react';
import { RecoilValue, useRecoilValue } from 'recoil';

export function RecoilObserver<T>({
  node,
  onChange,
}: {
  node: RecoilValue<T>;
  onChange: (value: T) => void;
}) {
  const value = useRecoilValue(node);
  React.useEffect(() => onChange(value), [onChange, value]);
  return null;
}
