export { buildCustomField } from '@/components/RedmineCustomField/testHelpers';
import * as moment from 'moment';
import {
  Activity,
  CurrentUser,
  IndirectTimeEntry,
  Issue,
  IssueTimeEntry,
  Project,
  ProjectTimeEntry,
  WorkMonth,
} from '@/lib/tableMode/types';

const randomInt = (limit: number): number => Math.ceil(Math.random() * limit);

const toTwoDigits = (n: number): string => (n < 10 ? `0${n}` : n.toString());

let lastId = 0;

export const generateId = (): number => {
  lastId += 1;
  return lastId;
};

export const buildUser = (attributes?: Partial<CurrentUser>): CurrentUser => ({
  id: 1,
  name: '水口　崇',
  locale: 'ja',
  ...(attributes || {}),
});

export const buildActivity = (attributes?: Partial<Activity>): Activity => {
  const id = generateId();
  return {
    id,
    name: `Activity #${id}`,
    position: id,
    ...(attributes || {}),
  };
};

export const buildIssue = (attributes?: Partial<Issue>): Issue => {
  const id = generateId();
  return {
    id,
    projectId: randomInt(10000),
    trackerId: 1,
    subject: 'Issue ABC',
    status: 'Doing',
    estimatedHours: randomInt(8),
    startDate: null,
    dueDate: null,
    url: `https://someone.redmine.org/projects/abc/${id}`,
    ...(attributes || {}),
  };
};

export const buildProject = (attributes?: Partial<Project>): Project => {
  const id = generateId();
  return {
    id,
    name: `Project #${id}`,
    url: `https://someone.redmine.org/projects/${id}`,
    lft: 0,
    active: true,
    permissions: ['edit', 'create'],
    activityIds: [],
    ...(attributes || {}),
  };
};

export const buildIssueTimeEntry = (
  attributes?: Partial<IssueTimeEntry>
): IssueTimeEntry => ({
  id: `timeEntry-${generateId()}`,
  activityType: 'issue',
  activity: {
    issueId: randomInt(10000),
    activityId: randomInt(10000),
  },
  date: `2021-01-${toTwoDigits(randomInt(31))}`,
  hours: randomInt(8),
  ...(attributes || {}),
});

export const buildProjectTimeEntry = (
  attributes?: Partial<ProjectTimeEntry>
): ProjectTimeEntry => ({
  id: `timeEntry-${generateId()}`,
  activityType: 'project',
  activity: {
    projectId: randomInt(10000),
    activityId: randomInt(10000),
  },
  date: `2021-01-${toTwoDigits(randomInt(31))}`,
  hours: randomInt(8),
  ...(attributes || {}),
});

export const buildIndirectTimeEntry = (
  attributes?: Partial<IndirectTimeEntry>
): IndirectTimeEntry => ({
  id: `timeEntry-${generateId()}`,
  activityType: 'indirect',
  activity: {
    indirectActivityId: randomInt(10000),
  },
  date: `2021-01-${toTwoDigits(randomInt(31))}`,
  hours: randomInt(8),
  ...(attributes || {}),
});

export const buildWorkMonth = (attributes?: {
  month?: string;
  days?: number[];
}): WorkMonth => {
  const { month, days } = attributes || {};
  let currentMonth = month || '2021-01';
  let previousDay = 0;
  const dates: string[] = [];
  (days || new Array(31).fill(0).map((_, index) => index + 1)).forEach(day => {
    if (previousDay >= day) {
      currentMonth = moment(currentMonth)
        .add(1, 'month')
        .format('YYYY-MM');
    }
    previousDay = day;
    const date = `${currentMonth}-${toTwoDigits(day)}`;
    // new Date('2021-02-31').getDate() => 3の挙動でvalidかどうか判定
    if (new Date(date).getDate() === day) {
      dates.push(date);
    }
  });
  return {
    month: month || '2021-01',
    dates: dates.map(date => ({
      date,
      type: 'workDay',
    })),
  };
};
