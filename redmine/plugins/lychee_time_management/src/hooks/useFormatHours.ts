import { useRecoilValue } from 'recoil';
import {
  timespanFormatState,
  TIMESPAN_FORMAT,
  TimespanFormat,
} from '@/recoil/timespanFormatState';

const formatHours =
  (timespanFormat: TimespanFormat) =>
  (hours: number | null | undefined, options = { round: true }): string => {
    if (!hours || hours < 0) {
      return '';
    }

    if (timespanFormat == TIMESPAN_FORMAT.MINUTES) {
      const h = Math.floor(hours);
      const m = Math.round((hours - h) * 60);
      // 小数の誤差を補正(1:60のとき,2:00になるように)
      if (m >= 60) return `${h + 1}:00`;
      return `${h}:${String(m).padStart(2, '0')}`;
    }

    if (options.round) {
      return hours.toFixed(2);
    }

    return hours.toString();
  };

export const useFormatHours: () => (
  hours: number | null | undefined,
  options?: { round: boolean }
) => string = () => {
  const timespanFormat = useRecoilValue(timespanFormatState);
  return formatHours(timespanFormat);
};
