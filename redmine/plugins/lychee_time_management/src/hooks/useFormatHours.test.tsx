import { renderHook } from '@testing-library/react';
import { RecoilRoot, useRecoilValue } from 'recoil';
import { useFormatHours } from './useFormatHours';
import { TIMESPAN_FORMAT } from '@/recoil/timespanFormatState';

jest.mock('recoil', () => ({
  ...jest.requireActual('recoil'),
  useRecoilValue: jest.fn(),
}));

describe('useFormatHours', () => {
  describe('when in minutes format', () => {
    (useRecoilValue as jest.Mock).mockReturnValue(TIMESPAN_FORMAT.MINUTES);

    const { result } = renderHook(() => useFormatHours(), {
      wrapper: RecoilRoot,
    });
    const formatHours = result.current;

    it('returns total time in correct format', () => {
      //  0.08333333333333332 + 0.6666666666666665 + 0.25 + 0.25 + 0.5  + 0.25 => 1.9999999999999998
      // (0:05)                (0:40)               (0:15) (0:15) (0:30) (0:15)
      //  DBに循環素数の近似値を保存しているため、合計値に誤差が生じる
      const totalTime = 1.9999999999999998;
      expect(formatHours(totalTime)).toBe('2:00');
    });

    it('returns an empty string for negative hours', () => {
      // 小数点の誤算により、限りなく0に近い値が生じることがある
      const time = -3.469446951953614e-18;
      expect(formatHours(time)).toBe('');
    });
  });
});
