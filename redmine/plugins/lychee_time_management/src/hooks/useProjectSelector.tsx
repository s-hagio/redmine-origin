import PlusButton from '@/components/TableMode/atoms/PlusButton';
import { SelectProjectForm } from '@/components/TableMode/SelectProjectForm';
import { useLabels } from '@/lib/tableMode/labels';
import type { Project } from '@/lib/tableMode/types';
import * as React from 'react';
import * as ReactModal from 'react-modal';

type ProjectId = Project['id'];
export const useProjectSelector = (
  userId: number,
  stagedProjectIds: ProjectId[],
  mergeProjectsByIds: (projects: Project[]) => void,
  addProjectRows: (ids: ProjectId[]) => void,
  updateActivitiesById: (projectIds: number[], issueIds: number[]) => void,
  onClickProjectSelectorButton: () => void,
  onCloseModal: () => void
) => {
  const [open, setOpen] = React.useState(false);
  const l = useLabels();

  const renderButton = React.useCallback(
    (): React.ReactNode => (
      <PlusButton
        tabIndex={-1}
        filled={true}
        onClick={() => {
          setOpen(true);
          onClickProjectSelectorButton();
        }}
      >
        {l('button_add_project')}
      </PlusButton>
    ),
    [l, onClickProjectSelectorButton]
  );

  const onFetched = React.useCallback(
    (projects: Project[]) => {
      mergeProjectsByIds(projects);
    },
    [mergeProjectsByIds]
  );

  const renderModal = React.useCallback(
    (): React.ReactNode => (
      <ReactModal
        isOpen={open}
        contentLabel="プロジェクト選択"
        onRequestClose={(): void => {
          setOpen(false);
          onCloseModal();
        }}
        style={{
          overlay: { backgroundColor: 'rgba(0, 0, 0, 0.75)', zIndex: 3 },
          content: {
            top: 'calc(50% - 300px)',
            bottom: 'calc(50% - 300px)',
            padding: '20px 30px',
          },
        }}
      >
        <SelectProjectForm
          userId={userId}
          stagedProjectIds={stagedProjectIds}
          onFetched={onFetched}
          onAddProjects={addProjectRows}
          onClose={() => {
            setOpen(false);
            onCloseModal();
          }}
          updateActivitiesById={updateActivitiesById}
        />
      </ReactModal>
    ),
    [
      open,
      userId,
      stagedProjectIds,
      onFetched,
      addProjectRows,
      updateActivitiesById,
      onCloseModal,
    ]
  );

  return {
    renderModal,
    renderButton,
  };
};
