import * as React from 'react';
import { Paginator } from '@/components/TableMode/SelectIssueForm/Paginator';

export const initialPage = 1 as const;

type Paginator = {
  totalCount: number;
  offset: number;
  limit: number;
};
export type SetPaginator = React.Dispatch<React.SetStateAction<Paginator>>;

export const usePagination = (
  onChange: ({
    page,
    setPaginator,
  }: {
    page: number;
    setPaginator: SetPaginator;
  }) => void
) => {
  const [page, setPage] = React.useState<number>(initialPage);
  const [paginator, setPaginator] = React.useState<Paginator>({
    totalCount: 0,
    offset: 0,
    limit: 0,
  });
  const handleChange = React.useCallback(
    (page: number) => {
      setPage(page);
      onChange({ page, setPaginator });
    },
    [onChange, setPaginator]
  );

  const renderPaginator = React.useCallback(
    () => <Paginator {...paginator} onChange={handleChange} />,
    [paginator, handleChange]
  );

  return {
    page,
    setPage,
    paginator,
    setPaginator,
    renderPaginator,
  };
};
