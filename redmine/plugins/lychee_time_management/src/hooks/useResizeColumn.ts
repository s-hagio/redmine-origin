import * as React from 'react';
import { useSetRecoilState } from 'recoil';
import { cellWidths, MIN_CELL_WIDTHS } from '@/recoil/cellWidths';

export const useResizeColumn = (
  cellName?:
    | 'issueNameCellWidth'
    | 'issueStatusCellWidth'
    | 'issueActivitiesCellWidth'
) => {
  const ref = React.useRef<HTMLDivElement>(null);
  const [isDragging, setIsDragging] = React.useState(false);
  const setCellWidths = useSetRecoilState(cellWidths);

  const onMouseDown = React.useCallback(() => {
    setIsDragging(true);
  }, []);

  const mouseMove = React.useCallback(
    (e: MouseEvent) => {
      if (ref && ref.current && cellName) {
        const width = e.clientX - ref.current.offsetLeft;
        const newWidth =
          MIN_CELL_WIDTHS[cellName] <= width
            ? width
            : MIN_CELL_WIDTHS[cellName];

        setCellWidths((currVal) => ({
          ...currVal,
          ...{ [cellName]: newWidth },
        }));
      }
    },
    [cellName, setCellWidths]
  );

  React.useEffect(() => {
    const mouseUp = () => {
      setIsDragging(false);
      window.removeEventListener('mousemove', mouseMove);
      window.removeEventListener('mouseup', mouseUp);
    };

    if (isDragging) {
      window.addEventListener('mousemove', mouseMove);
      window.addEventListener('mouseup', mouseUp);
    }

    return () => {
      window.removeEventListener('mousemove', mouseMove);
      window.removeEventListener('mouseup', mouseUp);
    };
  }, [isDragging, mouseMove]);

  return { ref, onMouseDown };
};
