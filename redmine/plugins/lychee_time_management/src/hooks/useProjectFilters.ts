import * as React from 'react';
import { timeEntryDataStore } from '@/lib/tableMode/timeEntryDataStore';
import type {
  Query,
  AvailableFilters,
  OperatorsByFilterType,
  OperatorsLabels,
} from '@/components/TableMode/FilterTable';
import { GroupedFilterOptions } from '@/components/TableMode/FilterTable/types';

export const useProjectFilters = () => {
  const [queries, setQueries] = React.useState<Query[]>([]);
  const [groupedFilterOptions, setGroupedFilterOptions] =
    React.useState<GroupedFilterOptions>([]);
  const [availableFilters, setAvailableFilters] =
    React.useState<AvailableFilters>({});
  const [operatorsByFilterType, setOperatorsByFilterType] =
    React.useState<OperatorsByFilterType>({});
  const [operatorsLabels, setOperatorsLabels] = React.useState<OperatorsLabels>(
    {}
  );
  React.useEffect(() => {
    (async (): Promise<void> => {
      const {
        queries,
        groupedFilterOptions,
        availableFilters,
        operatorsByFilterType,
        operatorsLabels,
      } = await timeEntryDataStore.getProjectFilters();
      setQueries(queries);
      setGroupedFilterOptions(groupedFilterOptions);
      setAvailableFilters(availableFilters);
      setOperatorsByFilterType(operatorsByFilterType);
      setOperatorsLabels(operatorsLabels);
    })();
  }, []);

  return {
    queries,
    groupedFilterOptions,
    availableFilters,
    setAvailableFilters,
    operatorsByFilterType,
    operatorsLabels,
  };
};
