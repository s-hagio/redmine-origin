import * as React from 'react';
import styled from 'styled-components';
import { useResizeColumn } from '@/hooks/useResizeColumn';
import TableCell from '@/components/TableMode/atoms/TableCell';
import { TABLE_HEADER_ROW_HEIGHT } from '@/lib/tableMode/cellStyle';

export const ColumnHeader: React.FC<{
  cellName?:
    | 'issueNameCellWidth'
    | 'issueStatusCellWidth'
    | 'issueActivitiesCellWidth';
  className?: string;
  children?: React.ReactNode;
}> = ({ cellName, className, children }) => {
  const { ref, onMouseDown } = useResizeColumn(cellName);
  return (
    <TableCell
      ref={ref}
      role="columnheader"
      className={className}
      style={{ position: 'relative' }}
    >
      {children}
      {cellName && (
        <ResizeHandle onMouseDown={onMouseDown} data-testid="resize-handle" />
      )}
    </TableCell>
  );
};

const ResizeHandle = styled.div`
  height: ${TABLE_HEADER_ROW_HEIGHT}px;
  position: absolute;
  top: 0;
  right: 0;
  cursor: col-resize;
  width: 7px;
  border-right: 2px solid transparent;
`;
