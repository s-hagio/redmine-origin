import * as React from 'react';
import styled from 'styled-components';

import { TIME_ENTRY_ROW_HEIGHT } from '@/lib/tableMode/cellStyle';
import { useLabels } from '@/lib/tableMode/labels';
import { MapById } from '@/lib/tableMode/mapById';
import { IndirectActivities } from '@/lib/tableMode/tableData';
import { Activity } from '@/lib/tableMode/types';

import TableCell from './atoms/TableCell';
import TableRowGroup from './atoms/TableRowGroup';
import ActivityRowHeader from './ActivityRowHeader';
import AddActivityButton from './AddActivityButton';
import { sortActivities } from '@/lib/tableMode/rowSorter';
import HoverableRow from './HoverableRow';
import { useRecoilValue } from 'recoil';
import { rowHoverState } from '@/recoil/rowHoverState';

const RowHeader = styled(TableCell).attrs({ role: 'rowheader' })`
  border-left: none;
`;

export const IndirectActivitiesLabelCell = styled(RowHeader)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: ${TIME_ENTRY_ROW_HEIGHT}px;
  padding-left: 24px;
  font-size: 13px;
`;

export const IndirectActivitiesEmptyCellForActivities = styled(RowHeader)`
  height: ${TIME_ENTRY_ROW_HEIGHT}px;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding-right: 0px;
`;

export const IndirectActivitiesEmptyCellForSum = styled(RowHeader)`
  height: ${TIME_ENTRY_ROW_HEIGHT}px;
`;

type IndirectActivityRowHeadersProps = {
  indirectActivityData: IndirectActivities;
  indirectActivities: MapById<Activity>;
  onAddActivity: (activityId: number) => void;
};

const IndirectActivityRowHeaders: React.FC<IndirectActivityRowHeadersProps> = ({
  indirectActivityData,
  indirectActivities,
  onAddActivity,
}: IndirectActivityRowHeadersProps) => {
  const l = useLabels();
  const unusedActivities = React.useMemo(() => {
    const usedActivities = indirectActivityData.activityRows.map((row) =>
      indirectActivities.get(row.id)
    ) as Activity[];

    return indirectActivities
      .valueSeq()
      .toArray()
      .filter(
        (activity) => !usedActivities.includes(activity) && activity.active
      );
  }, [indirectActivities, indirectActivityData.activityRows]);

  const sortedIndirectActivities = React.useMemo(
    () => sortActivities(indirectActivityData.activityRows, indirectActivities),
    [indirectActivityData.activityRows, indirectActivities]
  );

  const firstIndirectActivityId = React.useMemo(
    () => sortedIndirectActivities[0]?.id,
    [sortedIndirectActivities]
  );

  const hoverState = useRecoilValue(rowHoverState);
  const isHovering = React.useMemo(() => hoverState.indirect, [hoverState]);

  const noActivityCell = [
    <HoverableRow key={Number.MIN_SAFE_INTEGER} indirect={true}>
      <IndirectActivitiesEmptyCellForActivities>
        <AddActivityButton
          isVisibleButton={isHovering}
          activitiesById={indirectActivities}
          activities={unusedActivities}
          onSelectActivity={onAddActivity}
          isLast={true}
        />
      </IndirectActivitiesEmptyCellForActivities>
      <IndirectActivitiesEmptyCellForSum />
    </HoverableRow>,
  ];

  return (
    <TableRowGroup
      aria-label="間接作業"
      height={
        Math.max(sortedIndirectActivities.length, 1) * TIME_ENTRY_ROW_HEIGHT
      }
      cellsBeforeRows={
        <HoverableRow indirect={true} activityId={firstIndirectActivityId}>
          <IndirectActivitiesLabelCell>
            <span>{l('label_indirect_activities')}</span>
          </IndirectActivitiesLabelCell>
        </HoverableRow>
      }
    >
      {sortedIndirectActivities.length === 0
        ? noActivityCell
        : sortedIndirectActivities.map((activityRow, i) => {
            const indirectActivity = indirectActivities.get(
              activityRow.id
            ) as Activity;
            const isLast = i === sortedIndirectActivities.length - 1;
            const isVisibleButton =
              hoverState.indirect && hoverState.activityId === activityRow.id;

            return (
              <HoverableRow
                key={indirectActivity.id}
                activityId={indirectActivity.id}
                indirect={true}
              >
                <ActivityRowHeader
                  activity={indirectActivity}
                  rowHeight={TIME_ENTRY_ROW_HEIGHT + (isLast ? -1 : 0)}
                  timeEntrySum={activityRow.sum}
                  addActivityButton={
                    <AddActivityButton
                      isVisibleButton={isVisibleButton}
                      activitiesById={indirectActivities}
                      activities={unusedActivities}
                      onSelectActivity={onAddActivity}
                      isLast={isLast}
                    />
                  }
                />
              </HoverableRow>
            );
          })}
    </TableRowGroup>
  );
};

export default IndirectActivityRowHeaders;
