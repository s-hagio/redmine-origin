import * as React from 'react';
import { RecoilRoot } from 'recoil';
import { render, screen } from '@testing-library/react';
import { buildActivity } from '@/lib/test/factories';
import { expectTextContents } from '@/lib/test/matchers';

import ActivityRowHeader from './ActivityRowHeader';

describe('ActivityRowHeader', () => {
  it('shows activity name, timeEntrySum', () => {
    const activity = buildActivity({ name: 'Tests' });

    render(
      <RecoilRoot>
        <ActivityRowHeader activity={activity} timeEntrySum={11} />
      </RecoilRoot>
    );

    const cells = screen.getAllByRole('rowheader');
    expectTextContents(cells, ['Tests', '11']);
  });
});
