import * as React from 'react';
import { TIME_ENTRY_ROW_HEIGHT } from '@/lib/tableMode/cellStyle';
import { ActivityRowData, IndirectActivities } from '@/lib/tableMode/tableData';
import {
  RowCoordinates,
  TimeEntryCoordinates,
  TimeEntryId,
  WorkMonth,
  Activity,
} from '@/lib/tableMode/types';
import { MapById } from '@/lib/tableMode/mapById';
import { sortActivities } from '@/lib/tableMode/rowSorter';

import TableRow from './atoms/TableRow';
import ActivityRow, { CellComponentType } from './ActivityRow';
import HoverableRow from './HoverableRow';

const buildCoordinates = (activityRow: ActivityRowData): RowCoordinates => ({
  activityType: 'indirect',
  activity: {
    indirectActivityId: activityRow.id,
  },
});

type IndirectActivityRowsProps = {
  indirectActivitiesById: MapById<Activity>;
  indirectActivities: IndirectActivities;
  workMonth: WorkMonth;
  CellComponent: CellComponentType;
  onEditMultipleTimeEntries: (
    ids: TimeEntryId[],
    cellElement: Element,
    coordinates: TimeEntryCoordinates
  ) => void;
};

const IndirectActivityRows: React.FC<IndirectActivityRowsProps> = ({
  indirectActivitiesById,
  indirectActivities,
  workMonth,
  CellComponent,
  onEditMultipleTimeEntries,
}: IndirectActivityRowsProps) => {
  const sortedIndirectActivities = React.useMemo(
    () =>
      sortActivities(indirectActivities.activityRows, indirectActivitiesById),
    [indirectActivities.activityRows, indirectActivitiesById]
  );

  if (indirectActivities.activityRows.length === 0) {
    return (
      <HoverableRow indirect={true}>
        <TableRow rowHeight={TIME_ENTRY_ROW_HEIGHT} />
      </HoverableRow>
    );
  }

  return (
    <>
      {sortedIndirectActivities.map((activityRow) => {
        const rowCoordinates = buildCoordinates(activityRow);

        return (
          <HoverableRow
            key={activityRow.id}
            indirect={true}
            activityId={activityRow.id}
          >
            <ActivityRow
              workMonth={workMonth}
              row={activityRow}
              rowCoordinates={rowCoordinates}
              CellComponent={CellComponent}
              onEditMultipleTimeEntries={(ids, element, date): void => {
                onEditMultipleTimeEntries(ids, element, {
                  ...rowCoordinates,
                  date,
                });
              }}
            />
          </HoverableRow>
        );
      })}
    </>
  );
};

export default IndirectActivityRows;
