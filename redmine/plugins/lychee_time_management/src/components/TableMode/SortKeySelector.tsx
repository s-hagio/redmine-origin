import * as React from 'react';
import { decamelize } from 'humps';
import { useLabels } from '@/lib/tableMode/labels';
import {
  availableSortKeys as sortKeys,
  IssueSortKey as SortKey,
} from '@/lib/tableMode/rowSorter';
import Selector from './atoms/Selector';

export { SortKey };

type Props = {
  value: SortKey;
  onChange: (key: SortKey) => void;
};

const localStorageKey = 'LTM_TABLE_MODE_ISSUE_SORT_KEY' as const;

export const SortKeySelector: React.FC<Props> = ({ value, onChange }) => {
  const l = useLabels();
  const handleChange = (value: unknown): void => {
    const currentValue = value as SortKey;
    if (sortKeys.includes(currentValue)) {
      localStorage.setItem(localStorageKey, currentValue);
      onChange(currentValue as SortKey);
    }
  };

  const options = React.useMemo(
    () =>
      sortKeys.map(
        (key) =>
          ({
            value: key,
            label: l(`field_${decamelize(key)}`),
          } as const)
      ),
    [l]
  );

  return (
    <Selector
      value={value}
      onChange={handleChange}
      label={l('label_sort')}
      options={options}
      selectorWidth={130}
    />
  );
};

export const initialKey = (): SortKey => {
  const storedKey = localStorage.getItem(localStorageKey) as SortKey | null;
  if (storedKey && sortKeys.includes(storedKey)) {
    return storedKey;
  }
  return 'subject';
};
