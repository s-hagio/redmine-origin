import * as React from 'react';
import styled from 'styled-components';
import { useLabels } from '@/lib/tableMode/labels';
import { useProjectFilters } from '@/hooks/useProjectFilters';
import {
  usePagination,
  initialPage,
  SetPaginator,
} from '@/hooks/usePagination';
import { timeEntryDataStore } from '@/lib/tableMode/timeEntryDataStore';
import Button from '@/components/TableMode/atoms/Button';
import LoadingOverlay from '@/components/TableMode/atoms/LoadingOverlay';
import { Table } from './Table';
import type { Project } from '@/lib/tableMode/types';
import {
  FilterTable as Filters,
  Filter,
} from '@/components/TableMode/FilterTable';

const Root = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;
const TableArea = styled.div`
  flex: 0 1 auto;
  overflow: auto;
  margin-bottom: 1.2em;
`;
const ButtonsArea = styled.div`
  display: inline;
  justify-content: space-between;
  margin-top: 1.2em;
  ${Button} {
    display: inline;
  }
`;

type Props = {
  userId: number;
  stagedProjectIds: number[];
  onFetched: (projects: Project[]) => void;
  onAddProjects: (ids: Project['id'][]) => void;
  onClose: () => void;
  updateActivitiesById: (projectIds: number[], issueIds: number[]) => void;
};

export const SelectProjectForm: React.FC<Props> = ({
  userId,
  stagedProjectIds,
  onFetched,
  onAddProjects,
  onClose,
  updateActivitiesById,
}) => {
  const l = useLabels();
  const [loading, setLoading] = React.useState(true);
  const [projects, setProjects] = React.useState<Project[]>([]);
  const [selectedIds, setSelectedIds] = React.useState<Project['id'][]>([]);
  const {
    queries,
    groupedFilterOptions,
    availableFilters,
    setAvailableFilters,
    operatorsByFilterType,
    operatorsLabels,
  } = useProjectFilters();
  const [queryId, setQueryId] = React.useState<string>('');
  const [appliedFilters, setAppliedFilters] = React.useState<Filter[]>([]);
  const [currentFilters, setCurrentFilters] = React.useState<Filter[]>([
    {
      field: 'status',
      operator: '=',
      values: ['1'],
      enable: true,
    },
  ]);

  const updateTable = React.useCallback(
    async ({
      page,
      queryId,
      filters,
      setPaginator,
    }: {
      page: number;
      queryId: string;
      filters: Filter[];
      setPaginator: SetPaginator;
    }) => {
      setLoading(true);
      const enableFilters = filters.filter((f) => f.enable);
      const {
        projects: tableProjects,
        queriedFilters,
        totalCount,
        offset,
        limit,
      } = await timeEntryDataStore.getProjects({
        userId,
        queryId,
        filters: enableFilters,
        page,
      });
      setCurrentFilters(queriedFilters);
      setAppliedFilters(queriedFilters);
      setPaginator({ totalCount, offset, limit });
      onFetched(tableProjects);
      setProjects(tableProjects);
      setLoading(false);
    },
    [onFetched, userId]
  );
  const { renderPaginator, page, setPage, setPaginator } = usePagination(
    ({ page, setPaginator }) => {
      updateTable({ page, queryId, filters: appliedFilters, setPaginator });
    }
  );

  const getProjectActivities = (projectIds: number[]) => {
    updateActivitiesById(projectIds, []);
  };

  const handleSubmit = React.useCallback(() => {
    onAddProjects(selectedIds);
    getProjectActivities(selectedIds);
    onClose();
  }, [onAddProjects, onClose, selectedIds]);

  React.useEffect(() => {
    updateTable({ page, queryId, filters: currentFilters, setPaginator });
  }, []);

  const handleQueryChange = React.useCallback(
    (e: React.ChangeEvent<HTMLSelectElement>): void => {
      const currentValue = e.currentTarget.value;

      setQueryId(currentValue);
      setSelectedIds([]);
      setPage(initialPage);
      updateTable({
        page: initialPage,
        queryId: currentValue,
        filters: [],
        setPaginator,
      });
    },
    [setPage, setPaginator, updateTable]
  );

  const handleFilterSubmit = React.useCallback(
    (e: React.SyntheticEvent<HTMLElement>): void => {
      e.preventDefault();
      setQueryId('');
      setSelectedIds([]);
      setPage(initialPage);
      updateTable({
        page: initialPage,
        queryId: '',
        filters: currentFilters,
        setPaginator,
      });
    },
    [currentFilters, setPage, setPaginator, updateTable]
  );

  return (
    <Root>
      <div>
        <h2>{l('label_select_projects')}</h2>
      </div>
      <div>
        <fieldset id="queries" className="collapsible">
          <legend>{l('label_query')}</legend>
          <select onChange={handleQueryChange} value={queryId}>
            <option value=""></option>
            {queries.map((query) => (
              <option key={query.id} value={query.id}>
                {query.name}
              </option>
            ))}
          </select>
        </fieldset>
      </div>
      <form>
        <Filters
          type="ProjectQuery"
          groupedFilterOptions={groupedFilterOptions}
          availableFilters={availableFilters}
          setAvailableFilters={setAvailableFilters}
          operatorsByFilterType={operatorsByFilterType}
          operatorsLabels={operatorsLabels}
          filters={currentFilters}
          onChange={(filters): void => setCurrentFilters(filters)}
        />
        <p className="buttons">
          <a
            href="#"
            onClick={handleFilterSubmit}
            className="icon icon-checked"
          >
            {l('button_apply')}
          </a>
        </p>
      </form>
      <TableArea>
        {loading && <LoadingOverlay />}
        <Table
          key={page}
          projects={projects}
          stagedProjectIds={stagedProjectIds}
          selectedIds={selectedIds}
          setSelectedIds={setSelectedIds}
        />
      </TableArea>
      {renderPaginator()}
      <ButtonsArea>
        <Button
          textColor="#6b92b4"
          borderColor="#6b92b4"
          onClick={handleSubmit}
        >
          {l('button_add_selected_projects')}
        </Button>
        &nbsp;
        <a onClick={(): void => onClose()} href="#">
          {l('button_cancel_selection')}
        </a>
      </ButtonsArea>
    </Root>
  );
};
