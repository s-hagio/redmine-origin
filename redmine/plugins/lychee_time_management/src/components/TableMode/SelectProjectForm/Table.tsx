import * as React from 'react';
import classNames from 'classnames';
import { useLabels } from '@/lib/tableMode/labels';
import type { Project } from '@/lib/tableMode/types';

type ProjectId = Project['id'];
type Props = {
  projects: Project[];
  stagedProjectIds: ProjectId[];
  selectedIds: ProjectId[];
  setSelectedIds: React.Dispatch<React.SetStateAction<ProjectId[]>>;
};

export const Table: React.FC<Props> = ({
  projects,
  stagedProjectIds,
  selectedIds,
  setSelectedIds,
}) => {
  const l = useLabels();
  const [checkAll, setCheckAll] = React.useState(false);

  React.useEffect(() => {
    if (checkAll) {
      setSelectedIds(projects.map(({ id }) => id));
      return;
    }
    setSelectedIds([]);
  }, [projects, setSelectedIds, checkAll]);

  const handleChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const id = Number(e.currentTarget.name);
    const index = selectedIds.indexOf(id);
    if (index === -1) {
      setSelectedIds((prev) => [...prev, id]);
      return;
    }
    const newSelectedIds = [...selectedIds];
    newSelectedIds.splice(index, 1);
    setSelectedIds(newSelectedIds);
  };

  return (
    <table className="list projects odd-even">
      <thead>
        <tr>
          <th className="checkbox">
            <input
              type="checkbox"
              checked={checkAll}
              onChange={(e): void => {
                setCheckAll(e.currentTarget.checked);
              }}
            />
          </th>
          <th className="project">{l('field_name')}</th>
        </tr>
      </thead>
      <tbody>
        {projects.map(({ id, name }, idx) => (
          <tr
            key={id}
            className={classNames('project', idx % 2 === 0 ? 'odd' : 'even')}
          >
            <td className="checkbox">
              <input
                type="checkbox"
                name={id.toString()}
                checked={
                  stagedProjectIds.includes(id) || selectedIds.includes(id)
                }
                onChange={handleChange}
                disabled={stagedProjectIds.includes(id)}
              />
            </td>
            <td className="name">{name}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};
