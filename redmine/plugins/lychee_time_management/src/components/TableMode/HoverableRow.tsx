import { TABLE_ROW_HOVER_STYLE } from '@/lib/tableMode/cellStyle';
import { rowHoverState } from '@/recoil/rowHoverState';
import * as React from 'react';
import { useRecoilState } from 'recoil';
import styled from 'styled-components';

const Root = styled.div<{ isHovering: boolean }>`
  display: flex;
  ${({ isHovering }) => isHovering && TABLE_ROW_HOVER_STYLE};
`;

type HoverableRowProps = {
  children: React.ReactNode;
  className?: string;
  projectId?: number;
  issueId?: number;
  activityId?: number;
  indirect?: true;
  height?: number;
};

const HoverableRow: React.FC<HoverableRowProps> = ({
  children,
  className,
  projectId,
  issueId,
  activityId,
  indirect,
}) => {
  const [rowHover, setRowHover] = useRecoilState(rowHoverState);
  const onMouseEnter = React.useCallback(() => {
    setRowHover({
      projectId,
      issueId,
      activityId,
      indirect,
    });
  }, [projectId, issueId, activityId, indirect, setRowHover]);

  const onMouseLeave = React.useCallback(() => {
    setRowHover({});
  }, [setRowHover]);

  const isHovering = React.useMemo(
    () =>
      rowHover.projectId === projectId &&
      rowHover.issueId === issueId &&
      rowHover.activityId === activityId &&
      rowHover.indirect === indirect,
    [rowHover, projectId, issueId, activityId, indirect]
  );

  return (
    <Root
      aria-label="ホバー可能"
      className={className}
      isHovering={isHovering}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
    >
      {children}
    </Root>
  );
};

const StyledHoverableRow = styled(HoverableRow)`
  height: ${(props) => (props.height ? `${props.height}px` : 'min-content')};
`;

export default StyledHoverableRow;
