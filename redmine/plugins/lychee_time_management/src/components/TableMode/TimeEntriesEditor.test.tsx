import * as React from 'react';
import { RecoilRoot } from 'recoil';
import { render, screen, waitFor, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { buildIssueTimeEntry, buildCustomField } from '@/lib/test/factories';
import { buildElementQueries } from '@/lib/test/queries';

import { CustomField } from '@/components/RedmineCustomField';
import { LabelsProvider } from '@/lib/tableMode/labels';
import {
  AttachmentApiHandlerWithMemory,
  TimeEntry,
} from '@/lib/tableMode/types';
import TimeEntriesEditor, { TimeEntriesEditorProps } from './TimeEntriesEditor';
import { expectTextContents } from '@/lib/test/matchers';

type CallBacks = jest.Mocked<
  Pick<
    TimeEntriesEditorProps,
    'onCreate' | 'onDelete' | 'onUpdate' | 'onCancel'
  > &
    AttachmentApiHandlerWithMemory
>;

describe('TimeEntriesEditor', () => {
  const renderWith = ({
    timeEntries,
    customFields,
    requiredFields,
  }: {
    timeEntries?: TimeEntry[];
    customFields?: CustomField[];
    requiredFields?: string[];
  }): CallBacks => {
    const callbacks: CallBacks = {
      onCreate: jest.fn(),
      onDelete: jest.fn(),
      onUpdate: jest.fn(),
      getAttachment: jest.fn(),
      createAttachment: jest.fn(),
      deleteAttachment: jest.fn(),
      deleteUploadedAttachments: jest.fn(),
      clearMemory: jest.fn(),
      onCancel: jest.fn(),
    };

    /* eslint-disable */
    const labels = {
      actionview_instancetag_blank_option: '選んでください',
      button_create: '作成',
      button_save: '保存',
      field_comments: 'コメント',
      field_hours: '時間',
      label_none: 'なし',
    };
    /* eslint-enable */

    render(
      <RecoilRoot>
        <LabelsProvider value={labels}>
          <TimeEntriesEditor
            timeEntries={timeEntries || []}
            requiredFields={requiredFields || []}
            customFields={customFields || []}
            attachmentApi={{
              createAttachment: callbacks.createAttachment,
              deleteAttachment: callbacks.deleteAttachment,
              getAttachment: callbacks.getAttachment,
              deleteUploadedAttachments: callbacks.deleteUploadedAttachments,
              clearMemory: callbacks.clearMemory,
            }}
            permissions={['edit', 'create']}
            {...callbacks}
          />
        </LabelsProvider>
      </RecoilRoot>
    );
    return callbacks;
  };

  const renderWithCustomField = (customField: CustomField): CallBacks =>
  renderWith({
    customFields: [customField],
  });

  it('shows all time entries and hours up to 2 decimal places', () => {
    renderWith({
      timeEntries: [
        buildIssueTimeEntry({ comments: 'Entry A', hours: 2.489 }),
        buildIssueTimeEntry({ comments: 'Entry B', hours: 3 }),
      ],
    });

    const entries = screen.getAllByRole('listitem');

    expect(entries).toHaveLength(2);
    expect(entries[0]).toHaveTextContent(/2\.49\s*Entry A/);
    expect(entries[1]).toHaveTextContent(/3\.00\s*Entry B/);
  });

  const [getHoursField] = buildElementQueries('textbox', { name: /時間/ });
  const [getCommentField] = buildElementQueries('textbox', {
    name: /コメント/,
  });
  const [getCreateButton] = buildElementQueries('button', { name: '作成' });
  const [getEditButton] = buildElementQueries('button', { name: '編集' });

  it('hours is required', async () => {
    const user = userEvent.setup();
    renderWith({});

    const hoursField = getHoursField();
    const createButton = getCreateButton();

    expect(hoursField).toBeRequired();
    expect(createButton).toBeDisabled();
    await user.type(hoursField, '4');
    expect(createButton).toBeEnabled();
  });

  it('hours must be a number', async () => {
    const user = userEvent.setup();
    renderWith({});

    const hoursField = getHoursField();
    const createButton = getCreateButton();

    await user.type(hoursField, 'abc');
    expect(createButton).toBeDisabled();
  });

  it('comments is optional when not specified as required', async () => {
    const user = userEvent.setup();
    renderWith({});

    const hoursField = getHoursField();
    const commentField = getCommentField();
    const createButton = getCreateButton();

    expect(commentField).not.toBeRequired();
    await user.type(hoursField, '1');
    expect(createButton).toBeEnabled();
  });

  it('comments is required when specified as required', async () => {
    const user = userEvent.setup();
    renderWith({ requiredFields: ['comments'] });

    const hoursField = getHoursField();
    const commentField = getCommentField();
    const createButton = getCreateButton();

    expect(commentField).toBeRequired();
    await user.type(hoursField, '1');
    expect(createButton).toBeDisabled();
    await user.type(commentField, 'comments');
    expect(createButton).toBeEnabled();
  });

  const getTimeEntryItem = (name: string): HTMLElement =>
    screen.getByRole('listitem', { name: new RegExp(name) });

  it('Bugfix: it can switch between several editable entries', async () => {
    const user = userEvent.setup();
    renderWith({
      timeEntries: [
        buildIssueTimeEntry({ comments: 'Entry A', hours: 2 }),
        buildIssueTimeEntry({ comments: 'Entry B', hours: 3 }),
      ],
    });

    await user.click(
      getEditButton({ withinElement: getTimeEntryItem('Entry A') })
    );
    await user.click(
      getEditButton({ withinElement: getTimeEntryItem('Entry B') })
    );

    await waitFor(() => {
      expect(getHoursField()).toHaveValue('3');
    });
  });

  it('Bugfix: not submit when type enter key after japanese input', async () => {
    const user = userEvent.setup();
    renderWith({});

    const hoursField = getHoursField();
    const commentField = getCommentField();

    await user.type(hoursField, '1');
    await user.type(commentField, 'こんにちは{enter}');

    expect(hoursField).toBeInTheDocument();
    expect(commentField).toBeInTheDocument();
  });

  it('Bugfix: not submit when type enter key after japanese input into a custom text field', async () => {
    /**
     * NOTE: textareaは、Enterを押してもsubmitされないため、
     * input type="text"のときのみ検証
     */

    const user = userEvent.setup();
    const customField = buildCustomField({
      fieldFormat: "string",
      name: "custom-field"
    })
    renderWithCustomField(customField)

    const customFieldElement = screen.getByRole('textbox', { name: 'custom-field' });

    const hoursField = getHoursField();
    const commentField = getCommentField();

    await user.type(hoursField, '1');
    await user.type(commentField, 'こんにちは{enter}');
    await user.type(customFieldElement, 'こんにちは{enter}');

    expect(hoursField).toBeInTheDocument();
    expect(commentField).toBeInTheDocument();
    expect(customFieldElement).toBeInTheDocument();
  });

  describe('Custom Fields', () => {
    const textInputCustomFields: CustomField[] = [
      buildCustomField({ fieldFormat: 'link' }),
      buildCustomField({ fieldFormat: 'string' }),
      buildCustomField({ fieldFormat: 'int' }),
      buildCustomField({ fieldFormat: 'float' }),
    ];

    it.each(textInputCustomFields)(
      'displays $fieldFormat custom fields as text field',
      (customField) => {
        renderWithCustomField(customField);

        expect(
          screen.getByRole('textbox', { name: new RegExp(customField.name) })
        ).toBeInTheDocument();
      }
    );

    it.each(textInputCustomFields)(
      'displays $fieldFormat custom fields with defaultValue as text field',
      (customField) => {
        renderWithCustomField({
          ...customField,
          defaultValue: 'This is default!',
        });

        expect(
          screen.getByRole('textbox', { name: new RegExp(customField.name) })
        ).toHaveValue('This is default!');
      }
    );

    // カスタムフィールドの追加前に入力されたTimeEntryの編集時
    it.each(textInputCustomFields)(
      'prefer empty value over defaultValue when edit TimeEntry on $fieldFormat custom fields',
      async (customField) => {
        const user = userEvent.setup();
        renderWith({
          customFields: [
            {
              ...customField,
              defaultValue: 'This is default!',
            },
          ],
          timeEntries: [
            buildIssueTimeEntry(), // customFieldValuesが空
          ],
        });
        await user.click(
          screen.getByRole('button', { name: '編集' })
        );

        expect(
          screen.getByRole('textbox', { name: new RegExp(customField.name) })
        ).toHaveValue('');
      }
    );

    it('displays date custom fields as date inputs', () => {
      const customField = buildCustomField({
        fieldFormat: 'date',
        defaultValue: '2023-03-27',
      });

      renderWithCustomField(customField);

      const dateField = screen.getByLabelText(new RegExp(customField.name));
      expect(dateField).toHaveAttribute('type', 'date');
      expect(dateField).toHaveValue('2023-03-27');
    });

    it('displays text custom fields as textarea', () => {
      const customField = buildCustomField({
        fieldFormat: 'text',
        defaultValue: 'This is:\n  default!',
      });

      renderWithCustomField(customField);

      const textBox = screen.getByRole('textbox', {
        name: new RegExp(customField.name),
      });

      expect(textBox).toContainHTML('<textarea');
      expect(textBox).toContainHTML('This is:\n  default!');
    });

    it('displays please select custom field select option from label', () => {
      const customField = buildCustomField({
        fieldFormat: 'list',
        isRequired: true,
        possibleValuesOptions: [
          ['Choice A', 'a'],
          ['Choice B', 'b'],
        ],
      });

      renderWithCustomField(customField);

      const selectBox = screen.getByRole('combobox', {
        name: new RegExp(customField.name),
      });
      const options = within(selectBox).getAllByRole('option');

      expectTextContents(options, [
        '--- 選んでください ---',
        'Choice A',
        'Choice B',
      ]);
    });

    it('displays empty custom field select option from label', () => {
      const customField = buildCustomField({
        fieldFormat: 'list',
        editTagStyle: 'check_box',
        possibleValuesOptions: [
          ['Choice A', 'a'],
          ['Choice B', 'b'],
        ],
      });

      renderWithCustomField(customField);

      const radioGroup = screen.getByRole('radiogroup', {
        name: new RegExp(customField.name),
      });
      const radioButtons = within(radioGroup).getAllByRole('radio');

      expectTextContents(
        radioButtons.map((element) => element.parentElement as HTMLElement),
        ['(なし)', 'Choice A', 'Choice B']
      );
      expect(radioButtons[0]).toHaveAttribute('checked');
    });

    it('displays list custom field with defaultValue', () => {
      const customField = buildCustomField({
        fieldFormat: 'list',
        editTagStyle: 'check_box',
        defaultValue: 'b',
        possibleValuesOptions: [
          ['Choice A', 'a'],
          ['Choice B', 'b'],
        ],
      });

      renderWithCustomField(customField);

      const radioGroup = screen.getByRole('radiogroup', {
        name: new RegExp(customField.name),
      });
      const radioButtons = within(radioGroup).getAllByRole('radio');

      expectTextContents(
        radioButtons.map((element) => element.parentElement as HTMLElement),
        ['(なし)', 'Choice A', 'Choice B']
      );
      expect(radioButtons[0]).not.toBeChecked();
      expect(radioButtons[2]).toHaveAttribute('checked');
    });

    // カスタムフィールドの追加前に入力されたTimeEntryの編集時
    it('prefer empty value over defaultValue when edit TimeEntry on list custom fields', async () => {
      const user = userEvent.setup();
      const customField = buildCustomField({ // TimeEntryのcustomFieldValuesが空
        fieldFormat: 'list',
        editTagStyle: 'check_box',
        defaultValue: 'b',
        possibleValuesOptions: [
          ['Choice A', 'a'],
          ['Choice B', 'b'],
        ],
      });
      renderWith({
        customFields: [customField],
        timeEntries: [buildIssueTimeEntry()],
      });
      await user.click(
        screen.getByRole('button', { name: '編集' })
      );

      waitFor(() => {
        const radioGroup = screen.getByRole('radiogroup', {
          name: new RegExp(customField.name),
        });
        const radioButtons = within(radioGroup).getAllByRole('radio');

        expectTextContents(
          radioButtons.map((element) => element.parentElement as HTMLElement),
          ['(なし)', 'Choice A', 'Choice B']
        );
        expect(radioButtons[0]).toHaveAttribute('checked');
        expect(radioButtons[2]).not.toBeChecked();
      });
    });

    it('does not submit custom field values if there are no custom fields', async () => {
      const user = userEvent.setup();
      const { onCreate } = renderWith({
        customFields: [],
      });

      await user.type(getHoursField(), '2');
      await user.type(getCommentField(), 'Comment');
      await user.click(getCreateButton());

      expect(onCreate).toHaveBeenCalledWith({
        hours: '2',
        comments: 'Comment',
      });
    });

    describe('attachment custom fields', () => {
      const getInput = (customField: CustomField): HTMLElement =>
        screen.getByLabelText(new RegExp(customField.name));

      const expectFileInput = (customField: CustomField): void =>
        expect(getInput(customField)).toContainHTML('type="file"');

      const getDeleteAttachmentButton = (
        customField: CustomField
      ): HTMLElement =>
        within(getInput(customField)).getByRole('button', { name: '削除' });

      const waitForAttachment = async (
        customField: CustomField
      ): Promise<void> =>
        waitFor(() => {
          expect(getDeleteAttachmentButton(customField)).toBeInTheDocument();
        });

      const waitForAttachmentToDisappear = async (
        customField: CustomField
      ): Promise<void> =>
        waitFor(() => {
          expect(
            within(getInput(customField)).queryByRole('button')
          ).not.toBeInTheDocument();
        });

      it('is displayed as file input when without value', () => {
        const customField = buildCustomField({
          fieldFormat: 'attachment',
        });

        renderWithCustomField(customField);

        expectFileInput(customField);
      });

      it('is displayed as text with delete button when with value', async () => {
        const user = userEvent.setup();
        const customField = buildCustomField({
          fieldFormat: 'attachment',
        });

        const { getAttachment } = renderWith({
          customFields: [customField],
          timeEntries: [
            buildIssueTimeEntry({
              comments: 'Entry',
              customFieldValues: { [customField.id]: '22' },
            }),
          ],
        });

        getAttachment.mockResolvedValue({
          id: 22,
          filename: 'uploaded.txt',
          token: '22.abc',
        });
        const timeEntryItem = screen.getByRole('listitem', { name: 'Entry' });
        await user.click(getEditButton({ withinElement: timeEntryItem }));
        await waitForAttachment(customField);

        expect(getAttachment).toHaveBeenCalledWith(22);
        expect(getInput(customField)).toHaveTextContent('uploaded.txt');
      });

      it('is creates an attachment when choosing a file', async () => {
        const user = userEvent.setup();
        const customField = buildCustomField({
          fieldFormat: 'attachment',
        });

        const { createAttachment } = renderWithCustomField(customField);

        createAttachment.mockResolvedValue({
          id: 23,
          filename: 'screenshot.png',
          token: '23.abc',
        });
        const field = getInput(customField);
        const file = new File(['content'], 'screenshot.png', {
          type: 'image/png',
        });
        await user.upload(field, file);

        expect(createAttachment).toHaveBeenCalledWith(file);
        await waitForAttachment(customField);
        expect(getInput(customField)).toHaveTextContent('screenshot.png');
      });

      it('for new time entries: it deletes attachment immediately when pressing delete button', async () => {
        const user = userEvent.setup();
        const customField = buildCustomField({
          fieldFormat: 'attachment',
        });

        const { createAttachment, deleteAttachment } = renderWith({
          customFields: [customField],
        });

        createAttachment.mockResolvedValue({
          id: 24,
          filename: 'screenshot.png',
          token: '24.abc',
        });
        const field = getInput(customField);
        const file = new File(['content'], 'screenshot.png', {
          type: 'image/png',
        });
        await user.upload(field, file);

        expect(createAttachment).toHaveBeenCalledWith(file);
        await waitForAttachment(customField);

        await user.click(getDeleteAttachmentButton(customField));
        await waitForAttachmentToDisappear(customField);
        expect(deleteAttachment).toHaveBeenCalledWith(24);
        expectFileInput(customField);
      });

      it('for existing time entries: it only empties the field value when pressing delete button', async () => {
        const user = userEvent.setup();
        const customField = buildCustomField({
          fieldFormat: 'attachment',
        });

        const { getAttachment, deleteAttachment } = renderWith({
          customFields: [customField],
          timeEntries: [
            buildIssueTimeEntry({
              comments: 'Entry',
              customFieldValues: { [customField.id]: '22' },
            }),
          ],
        });

        getAttachment.mockResolvedValue({
          id: 22,
          filename: 'uploaded.txt',
          token: '22.abc',
        });
        const timeEntryItem = screen.getByRole('listitem', { name: 'Entry' });
        await user.click(getEditButton({ withinElement: timeEntryItem }));
        await waitForAttachment(customField);
        await user.click(getDeleteAttachmentButton(customField));
        await waitForAttachmentToDisappear(customField);

        expect(deleteAttachment).not.toHaveBeenCalled();
        expectFileInput(customField);
      });
    });
  });
});
