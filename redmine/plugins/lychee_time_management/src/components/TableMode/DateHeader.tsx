import * as React from 'react';
import * as moment from 'moment';
import styled from 'styled-components';
import TableCell from './atoms/TableCell';

import {
  TABLE_HEADER_ROW_HEIGHT,
  TIME_ENTRY_CELL_STYLE,
} from '@/lib/tableMode/cellStyle';
import { WorkDate } from '@/lib/tableMode/types';

const Day = styled.div`
  font-size: 13px;
  color: #333333;
`;

const DoW = styled.div`
  font-size: 10px;
  color: #979797;
`;

const Root = styled(TableCell)<{ workDate: WorkDate }>`
  ${TIME_ENTRY_CELL_STYLE}

  div:not(:last-child) {
    border-bottom-style: none;
  }

  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: ${TABLE_HEADER_ROW_HEIGHT}px;
  gap: 2px;
`;

type DateHeaderProps = {
  workDate: WorkDate;
};

const DateHeader: React.FC<DateHeaderProps> = ({
  workDate,
}: DateHeaderProps) => {
  const dateMoment = moment(workDate.date);

  return (
    <Root role="columnheader" workDate={workDate}>
      <Day>{dateMoment.format('D')}</Day>
      <DoW>{dateMoment.format('dd').substring(0, 1)}</DoW>
    </Root>
  );
};

export default DateHeader;
