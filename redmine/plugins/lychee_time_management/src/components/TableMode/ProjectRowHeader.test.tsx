import * as React from 'react';
import { RecoilRoot } from 'recoil';
import { render, screen } from '@testing-library/react';

import { buildProject } from '@/lib/test/factories';
import { expectTextContents } from '@/lib/test/matchers';
import { buildMapById } from '@/lib/tableMode/mapById';
import type { Project } from '@/lib/tableMode/types';

import ProjectRowHeader from './ProjectRowHeader';

const renderWith = ({
  project,
  timeEntrySum,
}: {
  project: Project;
  timeEntrySum?: number;
}): void => {
  render(
    <RecoilRoot>
      <ProjectRowHeader
        project={project}
        timeEntrySum={timeEntrySum || 0}
        activitiesById={buildMapById([])}
        availableActivities={[]}
        onAddActivity={jest.fn()}
        allowInsertingRows
      />
    </RecoilRoot>
  );
};

describe('ProjectRowHeader', () => {
  it('shows project name, timeEntrySum', () => {
    const project = buildProject({ name: 'Project X' });

    renderWith({ project, timeEntrySum: 33 });

    const cells = screen.getAllByRole('rowheader');
    expectTextContents(cells, ['Project X', '33']);
  });

  it('shows a link to the project', () => {
    const project = buildProject({ name: 'Project X' });

    renderWith({ project });

    const projectLink = screen.getByRole('link');
    expect(projectLink).toHaveTextContent(/Project X/);
    expect(projectLink).toHaveAttribute('href', project.url);
  });
});
