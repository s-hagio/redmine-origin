import * as React from 'react';
import { ActivityRowData, IssueData } from '@/lib/tableMode/tableData';
import {
  RowCoordinates,
  TimeEntryCoordinates,
  TimeEntryId,
  WorkMonth,
  Activity,
} from '@/lib/tableMode/types';
import { MapById } from '@/lib/tableMode/mapById';
import { sortActivities } from '@/lib/tableMode/rowSorter';

import TableRow from './atoms/TableRow';

import ActivityRow, { CellComponentType } from './ActivityRow';
import HoverableRow from './HoverableRow';

const buildCoordinates = (
  issueData: IssueData,
  activityRow: ActivityRowData
): RowCoordinates => ({
  activityType: 'issue',
  activity: {
    issueId: issueData.id,
    activityId: activityRow.id,
    projectId: issueData.projectId,
  },
});

type IssueDataRowsProps = {
  issueData: IssueData;
  activitiesById: MapById<Activity>;
  rowHeight: number;
  activityRowOffset: number;
  workMonth: WorkMonth;
  CellComponent: CellComponentType;
  onEditMultipleTimeEntries: (
    ids: TimeEntryId[],
    cellElement: Element,
    coordinates: TimeEntryCoordinates
  ) => void;
};

const IssueDataRows: React.FC<IssueDataRowsProps> = ({
  issueData,
  activitiesById,
  rowHeight,
  activityRowOffset,
  workMonth,
  CellComponent,
  onEditMultipleTimeEntries,
}: IssueDataRowsProps) => {
  const sortedActivities = React.useMemo(
    () => sortActivities(issueData.activityRows, activitiesById),
    [issueData.activityRows, activitiesById]
  );

  if (issueData.activityRows.length === 0) {
    return (
      <HoverableRow projectId={issueData.projectId} issueId={issueData.id}>
        <TableRow rowHeight={rowHeight} />
      </HoverableRow>
    );
  }

  return (
    <>
      {sortedActivities.map((activityRow, idx) => {
        const rowCoordinates = buildCoordinates(issueData, activityRow);
        const isFirstActivity = idx === 0;
        const activityRowHeight = isFirstActivity
          ? rowHeight
          : rowHeight - activityRowOffset;

        return (
          <HoverableRow
            key={activityRow.id}
            projectId={issueData.projectId}
            issueId={issueData.id}
            activityId={activityRow.id}
          >
            <ActivityRow
              row={activityRow}
              workMonth={workMonth}
              height={activityRowHeight}
              activityRowOffset={isFirstActivity ? activityRowOffset : 0}
              CellComponent={CellComponent}
              rowCoordinates={rowCoordinates}
              onEditMultipleTimeEntries={(ids, element, date): void => {
                onEditMultipleTimeEntries(ids, element, {
                  ...rowCoordinates,
                  date,
                });
              }}
            />
          </HoverableRow>
        );
      })}
    </>
  );
};

export default IssueDataRows;
