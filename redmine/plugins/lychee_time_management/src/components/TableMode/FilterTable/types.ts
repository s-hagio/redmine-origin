export type AvailableFilter = {
  name: string;
  type: string;
  values: [string, string][] | null;
  remote: boolean;
};

export type AvailableFilters = {
  [field: string]: AvailableFilter | undefined;
};

type Option = [string, string];
export type GroupedFilterOptions = [string, Option[]][];
