import * as React from 'react';
import { useLabels } from '@/lib/tableMode/labels';
import { GroupedFilterOptions } from './types';

type Props = {
  groupedFilterOptions: GroupedFilterOptions;
  currentFilterNames: string[];
  onSelect: (name: string) => void;
};

export const AddFilter: React.FC<Props> = ({
  groupedFilterOptions,
  currentFilterNames,
  onSelect,
}) => {
  const l = useLabels();
  const [value, setValue] = React.useState('');

  const handleSelect = (e: React.ChangeEvent<HTMLSelectElement>): void => {
    setValue('');
    onSelect(e.currentTarget.value);
  };

  return (
    <div className="add-filter">
      <label>{l('label_filter_add')}</label>&nbsp;
      <select value={value} onChange={handleSelect}>
        <option />
        {groupedFilterOptions.map(([optgroup, options]) => {
          if (optgroup === null) {
            const [label, field] = options[0];
            return (
              <option
                key={field}
                value={field}
                disabled={currentFilterNames.includes(field)}
              >
                {label}
              </option>
            );
          }
          return (
            <optgroup key={optgroup} label={optgroup}>
              {options.map(([label, field]) => (
                <option
                  key={field}
                  value={field}
                  disabled={currentFilterNames.includes(field)}
                >
                  {label}
                </option>
              ))}
            </optgroup>
          );
        })}
      </select>
    </div>
  );
};
