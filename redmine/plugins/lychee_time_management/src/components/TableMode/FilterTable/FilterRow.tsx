import * as React from 'react';
import { FilterValues } from './FilterValues';
import { OperatorType } from '@/redmine/filters';
import { AvailableFilter } from './types';

export type Query = { id: number; name: string };
export type Filter = {
  field: string;
  operator: string;
  values?: string[];
  enable: boolean;
};
export type OperatorsByFilterType = { [type: string]: string[] | undefined };
export type OperatorsLabels = { [operator: string]: string | undefined };

type Props = Filter & {
  availableFilter: AvailableFilter;
  operatorsByFilterType: OperatorsByFilterType;
  operatorsLabels: OperatorsLabels;
  onChange: (filter: Filter) => void;
};

export const FilterRow: React.FC<Props> = ({
  availableFilter,
  field,
  operator,
  values,
  enable,
  operatorsByFilterType,
  operatorsLabels,
  onChange,
}) => (
  <tr className="filter">
    <td className="field" style={{ minWidth: 200 }}>
      <input
        type="checkbox"
        id={field}
        checked={enable}
        onChange={(e): void => {
          onChange({
            field,
            operator,
            values,
            enable: e.currentTarget.checked,
          });
        }}
      />
      <label htmlFor={field}>{availableFilter.name}</label>
    </td>
    {enable && (
      <FilterForm
        availableFilter={availableFilter}
        field={field}
        operator={operator}
        values={values}
        enable={enable}
        operatorsByFilterType={operatorsByFilterType}
        operatorsLabels={operatorsLabels}
        onChange={onChange}
      />
    )}
  </tr>
);

const FilterForm: React.FC<Omit<Props, 'fieldName'>> = ({
  availableFilter,
  field,
  operator,
  values,
  enable,
  operatorsByFilterType,
  operatorsLabels,
  onChange,
}) => {
  const operators = operatorsByFilterType[availableFilter.type] ?? ['='];

  const handleOperatorChange = (
    e: React.SyntheticEvent<HTMLSelectElement>
  ): void => {
    const operator = e.currentTarget.value;

    let newValues = values;
    if (
      values &&
      values[0] === '' &&
      availableFilter.values &&
      availableFilter.values[0]
    ) {
      newValues = [availableFilter.values[0][1]];
    }

    onChange({
      field,
      operator,
      values: newValues,
      enable,
    });
  };

  return (
    <>
      <td className="operator">
        <select value={operator} onChange={handleOperatorChange}>
          {operators.map(operator => (
            <option key={operator} value={operator}>
              {operatorsLabels[operator]}
            </option>
          ))}
        </select>
      </td>
      <td className="values">
        <FilterValues
          availableFilter={availableFilter}
          operator={operator as OperatorType}
          values={values}
          onChange={(values): void => {
            onChange({ field, operator, values, enable });
          }}
        />
      </td>
    </>
  );
};
