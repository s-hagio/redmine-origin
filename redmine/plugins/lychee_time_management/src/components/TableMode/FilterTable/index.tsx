import * as React from 'react';
import { useLabels } from '@/lib/tableMode/labels';
import { produce } from 'immer';
import { timeEntryDataStore } from '@/lib/tableMode/timeEntryDataStore';
import {
  FilterRow,
  Query,
  Filter,
  OperatorsByFilterType,
  OperatorsLabels,
} from './FilterRow';
import { AddFilter } from './AddFilter';
import { AvailableFilters, GroupedFilterOptions } from './types';

export {
  Query,
  Filter,
  AvailableFilters,
  OperatorsByFilterType,
  OperatorsLabels,
};

type Props = {
  type: 'ProjectQuery' | 'IssueQuery';
  groupedFilterOptions: GroupedFilterOptions;
  availableFilters: AvailableFilters;
  setAvailableFilters: React.Dispatch<React.SetStateAction<AvailableFilters>>;
  operatorsByFilterType: OperatorsByFilterType;
  operatorsLabels: OperatorsLabels;
  filters: Filter[];
  onChange: (filters: Filter[]) => void;
};

export const FilterTable: React.FC<Props> = ({
  type,
  groupedFilterOptions,
  availableFilters,
  setAvailableFilters,
  operatorsByFilterType,
  operatorsLabels,
  filters,
  onChange,
}) => {
  const l = useLabels();
  const handleFilterChange = (filter: Filter): void => {
    const newFilters = produce(filters, (draft) => {
      const index = draft.findIndex((f) => f.field == filter.field);
      if (index !== -1) {
        draft[index] = filter;
      }
    });
    onChange(newFilters);
  };

  const updateFilterValues = async (
    field: string
  ): Promise<[string, string][] | null> => {
    const availableFilter = availableFilters[field];
    if (!availableFilter) {
      return null;
    }
    const { remote } = availableFilter;
    if (!remote) {
      return null;
    }
    const { values } = await timeEntryDataStore.getFilterValues(type, field);
    const newFilters = produce(availableFilters, (draft) => {
      const filter = draft[field];
      if (filter) {
        filter.values = values;
      }
    });
    setAvailableFilters(newFilters);
    return values;
  };

  const handleAddFilter = async (field: string): Promise<void> => {
    const remoteValues = await updateFilterValues(field);
    const newFilters = produce(filters, (draft) => {
      const availableFilter = availableFilters[field];
      if (!availableFilter) {
        return;
      }
      const { type, values } = availableFilter;
      const operators = operatorsByFilterType[type] ?? ['='];
      const defaultValue =
        (values && values[0][1]) ?? (remoteValues && remoteValues[0][1]) ?? '';
      const filter = {
        field,
        operator: operators[0],
        values: [defaultValue],
        enable: true,
      };
      draft.push(filter);
    });
    onChange(newFilters);
  };


  const [filterVisible, setFilterVisible] = React.useState<boolean>(true);
  const filterClassName = (
    // TODO: icon-expended が icon-expanded に5系から修正された。
    // 現在は4系と5系をサポートしているので sorted-asc で回避する 5系のみになったら統一する
    `icon icon-${filterVisible ? 'sorted-asc' : 'collapsed'}`
  );

  const toggleFilter = () => {
    setFilterVisible(!filterVisible);
  };

  return (
    <fieldset id="filters" className="collapsible">
      <legend onClick={toggleFilter} className={filterClassName}>{l('label_filter_plural')}</legend>
      <div style={{ display: filterVisible ? "" : "none" }}>
        <table id="filters-table">
          <tbody>
            {filters.map(({ field, operator, values, enable }) => {
              const availableFilter = availableFilters[field];
              return (
                availableFilter && (
                  <FilterRow
                    key={field}
                    availableFilter={availableFilter}
                    field={field}
                    operator={operator}
                    values={values}
                    enable={enable}
                    operatorsByFilterType={operatorsByFilterType}
                    operatorsLabels={operatorsLabels}
                    onChange={handleFilterChange}
                  />
                )
              );
            })}
          </tbody>
        </table>
        <AddFilter
          groupedFilterOptions={groupedFilterOptions}
          currentFilterNames={filters.map((f) => f.field)}
          onSelect={handleAddFilter}
        />
      </div>
    </fieldset>
  );
};
