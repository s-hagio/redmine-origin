import * as React from 'react';
import classNames from 'classnames';

import {
  noNeedInputOpertor,
  needRangeInputOperators,
  needNumberInputOperators,
  OperatorType,
} from '@/redmine/filters';
import { useLabels } from '@/lib/tableMode/labels';
import { AvailableFilter } from './types';

type Props = {
  availableFilter: AvailableFilter;
  operator: OperatorType;
  values: string[] | undefined;
  onChange: (values: string[] | undefined) => void;
};

const noInputsOperators: OperatorType[] = [...noNeedInputOpertor];
const rangeInputsOperators: OperatorType[] = [...needRangeInputOperators];
const numberInputOperators: OperatorType[] = [...needNumberInputOperators];

export const FilterValues: React.FC<Props> = ({
  availableFilter,
  operator,
  values = ['', ''],
  onChange,
}) => {
  const l = useLabels();
  const [value, secondValue] = values;
  const [multiple, setMultiple] = React.useState(false);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    onChange([e.currentTarget.value]);
  };

  const handleSelectChange = (
    e: React.ChangeEvent<HTMLSelectElement>
  ): void => {
    const values = Array.from(
      e.currentTarget.selectedOptions,
      option => option.value
    );
    onChange(values);
  };

  if (noInputsOperators.includes(operator)) {
    return null;
  }

  if (rangeInputsOperators.includes(operator)) {
    return (
      <>
        <input
          type="date"
          value={value}
          onChange={(e): void => {
            onChange([e.currentTarget.value, secondValue]);
          }}
        />
        <input
          type="date"
          value={secondValue}
          onChange={(e): void => {
            onChange([value, e.currentTarget.value]);
          }}
        />
      </>
    );
  }

  if (numberInputOperators.includes(operator)) {
    return (
      <>
        <input size={3} value={value} onChange={handleChange} />
        <span>{l('label_day_plural')}</span>
      </>
    );
  }

  if (['text', 'date'].includes(availableFilter.type)) {
    return <input type={availableFilter.type} value={value} onChange={handleChange} />;
  }

  return (
    <>
      <select
        value={multiple ? values : value}
        multiple={multiple}
        onChange={handleSelectChange}
      >
        {(availableFilter.values ?? []).map(([label, value]) => (
          <option key={value} value={value}>
            {label}
          </option>
        ))}
      </select>
      <span
        className={classNames(
          'toggle-multiselect',
          'icon-only',
          multiple ? 'icon-toggle-minus' : 'icon-toggle-plus'
        )}
        onClick={(e): void => {
          e.preventDefault();
          setMultiple(!multiple);
        }}
      >
        +
      </span>
    </>
  );
};
