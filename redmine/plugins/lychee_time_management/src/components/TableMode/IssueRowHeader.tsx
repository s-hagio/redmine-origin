import * as React from 'react';
import styled from 'styled-components';

import { useFormatHours } from '@/hooks/useFormatHours';
import {
  CELL_PADDING,
  HOURS_CELL_RIGHT_PADDING,
  LABEL_CELL_LEFT_PADDING,
  TIME_ENTRY_ROW_HEIGHT,
} from '@/lib/tableMode/cellStyle';
import { MapById } from '@/lib/tableMode/mapById';
import { IssueData } from '@/lib/tableMode/tableData';
import { Issue } from '@/lib/tableMode/types';

/* import { lineHeightFromHeight } from './atoms/TableRow'; */
import TableRowGroup from './atoms/TableRowGroup';
import HoverableRow from './HoverableRow';

const ANCESTOR_SUBJECT_SEPARATOR = '>>';
const ANCESTOR_SUBJECT_SEPARATOR_LENGTH = 3;
const PARENT_SUBJECT_LINE_HEIGHT = 14;
const MAX_ANCESTOR_SUBJECT_LENGTH = 22;

const buildAncestorSubjects = (
  issueId: number,
  issuesById: MapById<Issue>
): string[] => {
  const issue = issuesById.get(issueId) as Issue;

  const ancestorSubjects = [];
  for (let currentIssue = issue; currentIssue.parentId; ) {
    currentIssue = issuesById.get(currentIssue.parentId) as Issue;
    ancestorSubjects.unshift(currentIssue.subject);
  }
  return ancestorSubjects;
};

const subjectsFittingInOneLine = (ancestorSubjects: string[]): string[] => {
  let tmpLength = 0;
  const lastSubjectIndexInOneLine = ancestorSubjects.findIndex(
    (subject, i) =>
      (tmpLength +=
        subject.length + (i === 0 ? 0 : ANCESTOR_SUBJECT_SEPARATOR_LENGTH)) >
      MAX_ANCESTOR_SUBJECT_LENGTH
  );
  return ancestorSubjects.slice(
    0,
    lastSubjectIndexInOneLine === -1
      ? ancestorSubjects.length
      : lastSubjectIndexInOneLine === 0
      ? 1
      : lastSubjectIndexInOneLine
  );
};

export const buildAncestorSubjectsPerLine = (
  issueId: number,
  issuesById: MapById<Issue>
): string[][] => {
  const ancestorSubjectsPerLine = [];
  for (
    const ancestorSubjects = buildAncestorSubjects(issueId, issuesById);
    ancestorSubjects.length > 0;

  ) {
    const nextLineSubjects = subjectsFittingInOneLine(ancestorSubjects);
    ancestorSubjectsPerLine.push(nextLineSubjects);
    ancestorSubjects.splice(0, nextLineSubjects.length);
  }
  return ancestorSubjectsPerLine;
};

export const rowHeight = (
  issueData: IssueData,
  issuesById: MapById<Issue>
): number => {
  const ancestorSubjectsPerLine = buildAncestorSubjectsPerLine(
    issueData.id,
    issuesById
  );
  const ancestorSubjectsHeight =
    PARENT_SUBJECT_LINE_HEIGHT * ancestorSubjectsPerLine.length;
  const ownHeight = TIME_ENTRY_ROW_HEIGHT;
  const rowHeightFromIssueSubjects = Math.floor(
    ancestorSubjectsHeight + ownHeight
  );
  return Math.max(TIME_ENTRY_ROW_HEIGHT, rowHeightFromIssueSubjects);
};

const Root = styled(TableRowGroup)`
  width: fit-content;
`;

const RowHeaderCell = styled.div.attrs(({ role }) => ({
  role: role ?? 'cell',
}))`
  text-align: left;
  box-sizing: border-box;
  display: inline-block;
  padding: ${CELL_PADDING}px;
  :first-child {
    border-left-width: 0;
  }
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`;

const RowHeader = styled(RowHeaderCell).attrs({ role: 'rowheader' })`
  height: ${TIME_ENTRY_ROW_HEIGHT}px;
`;

export const IssueNameCell = styled(RowHeader)`
  display: inline-flex;
  align-items: stretch;
  height: auto;
  padding-left: 0px;
`;

const RootForCellsBeforeRows = styled(HoverableRow)`
  align-items: center;
  padding-left: 24px;
`;

const TextAreaWithoutTextOverflow = styled.div.attrs({ role: 'presentation' })`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  font-size: 13px;
`;

const IssueNameArea = styled.div`
  flex-grow: 1;
  min-width: 0; // IE11
  flex-basis: 0px; // IE11
  display: flex;
  flex-direction: column;
  text-align: left;
  height: 100%;
  justify-content: center;
`;

const ParentIssueSubject = styled.div`
  overflow: hidden;
  font-size: 10px;
  color: #555555;
  line-height: ${PARENT_SUBJECT_LINE_HEIGHT}px;
`;

const AncestorIssueSubject = styled.span`
  &::after {
    content: '${ANCESTOR_SUBJECT_SEPARATOR}';
    display: inline-block;
    margin: 0 4px;
  }
`;

const AncestorIssueSubjectLine = styled.div`
  & {
    text-overflow: ellipsis;
    overflow: hidden;
  }

  &:last-child {
    ${AncestorIssueSubject}:last-child::after {
      display: none;
    }
  }
`;

export const IssueStatusCell = styled(RowHeader)`
  display: flex;
  align-items: center;
  padding-left: ${LABEL_CELL_LEFT_PADDING}px;
  span {
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

export const IssueEstimatedHoursCell = styled(RowHeader)`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding-right: ${HOURS_CELL_RIGHT_PADDING}px;
`;

export const IssueActivityEmptyCell = styled(RowHeader)`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 0px;
`;

export const IssueSumEmptyCell = styled(RowHeader)``;

type IssueRowHeaderProps = {
  issue: Issue;
  issues: MapById<Issue>;
  activityRowOffset: number;
  children?: React.ReactNode[];
  firstActivityId?: number;
};

const IssueRowHeader: React.FC<IssueRowHeaderProps> = ({
  issue,
  issues,
  activityRowOffset,
  children,
  firstActivityId,
}: IssueRowHeaderProps) => {
  const formatHours = useFormatHours();
  const height =
    TIME_ENTRY_ROW_HEIGHT * (children?.length || 1) + activityRowOffset;
  const ancestorSubjectsPerLine = buildAncestorSubjectsPerLine(
    issue.id,
    issues
  );

  return (
    <Root
      aria-label={issue.subject}
      height={height}
      cellsBeforeRows={
        <RootForCellsBeforeRows
          height={TIME_ENTRY_ROW_HEIGHT + activityRowOffset}
          projectId={issue.projectId}
          issueId={issue.id}
          activityId={firstActivityId}
        >
          <IssueNameCell>
            <IssueNameArea>
              <ParentIssueSubject
                title={ancestorSubjectsPerLine
                  .flat()
                  .join(` ${ANCESTOR_SUBJECT_SEPARATOR} `)}
              >
                {ancestorSubjectsPerLine.map((ary, i) => (
                  <AncestorIssueSubjectLine key={[issue.id, i].join('-')}>
                    {ary.map((ancestorSubject, j) => (
                      <AncestorIssueSubject key={[issue.id, i, j].join('-')}>
                        {ancestorSubject}
                      </AncestorIssueSubject>
                    ))}
                  </AncestorIssueSubjectLine>
                ))}
              </ParentIssueSubject>
              <TextAreaWithoutTextOverflow title={issue.subject}>
                <a href={issue.url} tabIndex={-1}>
                  #{issue.id}
                </a>
                &nbsp;{issue.subject}
              </TextAreaWithoutTextOverflow>
            </IssueNameArea>
          </IssueNameCell>
          <IssueStatusCell
            title={issue.status}
            style={{ marginTop: activityRowOffset }}
          >
            <span>{issue.status}</span>
          </IssueStatusCell>
          <IssueEstimatedHoursCell style={{ marginTop: activityRowOffset }}>
            {formatHours(issue.estimatedHours)}
          </IssueEstimatedHoursCell>
        </RootForCellsBeforeRows>
      }
    >
      {children}
    </Root>
  );
};

export default IssueRowHeader;
