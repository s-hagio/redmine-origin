import { useFormatHours } from '@/hooks/useFormatHours';
import {
  HOURS_CELL_RIGHT_PADDING,
  LABEL_CELL_LEFT_PADDING,
  TIME_ENTRY_ROW_HEIGHT,
} from '@/lib/tableMode/cellStyle';
import { Activity } from '@/lib/tableMode/types';
import * as React from 'react';
import styled from 'styled-components';

import TableCell from './atoms/TableCell';
import TableRow from './atoms/TableRow';

const Root = styled(TableRow)`
  width: fit-content;
`;

const RowHeader = styled(TableCell).attrs({ role: 'rowheader' })`
  border-left: none;
`;

export const ActivityNameCellWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const ActivityNameCell = styled(RowHeader)`
  flex-grow: 1;
  padding-left: ${LABEL_CELL_LEFT_PADDING}px;
`;

const ActivityRowSumCellWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const ActivityRowSumCell = styled(RowHeader)`
  text-align: right;
  padding-right: ${HOURS_CELL_RIGHT_PADDING}px;
`;

type ActivityRowHeaderProps = {
  activity: Activity;
  rowHeight?: number;
  activityRowOffset?: number;
  timeEntrySum: number;
  addActivityButton?: React.ReactElement;
};

const ActivityRowHeader: React.FC<ActivityRowHeaderProps> = ({
  activity,
  rowHeight,
  activityRowOffset = 0,
  timeEntrySum,
  addActivityButton,
}: ActivityRowHeaderProps) => {
  const formatTimeEntryHours = useFormatHours();

  return (
    <Root rowHeight={rowHeight || TIME_ENTRY_ROW_HEIGHT}>
      <ActivityNameCellWrapper>
        <ActivityNameCell
          title={activity?.name}
          style={{ marginTop: activityRowOffset }}
        >
          {activity?.name || '--'}
        </ActivityNameCell>
        {addActivityButton && (
          <div
            style={{
              marginTop: activityRowOffset,
              alignSelf: 'center',
            }}
          >
            {addActivityButton}
          </div>
        )}
      </ActivityNameCellWrapper>
      <ActivityRowSumCellWrapper>
        <ActivityRowSumCell style={{ marginTop: activityRowOffset }}>
          {formatTimeEntryHours(timeEntrySum)}
        </ActivityRowSumCell>
      </ActivityRowSumCellWrapper>
    </Root>
  );
};

export default ActivityRowHeader;
