import * as React from 'react';
import { MapById } from '@/lib/tableMode/mapById';
import { IssueData } from '@/lib/tableMode/tableData';
import { Activity, Issue } from '@/lib/tableMode/types';

import ActivityRowHeader from './ActivityRowHeader';
import IssueRowHeader, {
  IssueActivityEmptyCell,
  IssueSumEmptyCell,
} from './IssueRowHeader';
import { sortActivities } from '@/lib/tableMode/rowSorter';
import HoverableRow from './HoverableRow';
import AddActivityButton from './AddActivityButton';
import { useRecoilValue } from 'recoil';
import { rowHoverState } from '@/recoil/rowHoverState';

type IssueRowHeadersProps = {
  issue: Issue;
  issues: MapById<Issue>;
  issueData: IssueData;
  isLastIssue: boolean;
  activities: MapById<Activity>;
  rowHeight: number;
  activityRowOffset: number;
  onAddActivity: (activityId: number) => void;
  allowInsertingRows?: boolean;
};

const IssueRowHeaders: React.FC<IssueRowHeadersProps> = ({
  issue,
  issues,
  issueData,
  isLastIssue,
  activities,
  rowHeight,
  activityRowOffset,
  onAddActivity,
  allowInsertingRows = true,
}: IssueRowHeadersProps) => {
  React.useEffect(() => {
    if(issueData.activityRows.length > 0) return;
    const activeActivities = activities.valueSeq().toArray().filter((activity) => activity.active);
    if(activeActivities.length !== 1) return;

    onAddActivity(activeActivities[0].id);
  }, [issueData.activityRows, activities]);

  const unusedActivities = React.useMemo(() => {
    const usedActivities = issueData.activityRows.map((row) =>
      activities.get(row.id)
    ) as Activity[];

    return activities
      .valueSeq()
      .toArray()
      .filter(
        (activity) => !usedActivities.includes(activity) && activity.active
      );
  }, [activities, issueData.activityRows]);

  const sortedActivities = React.useMemo(
    () => sortActivities(issueData.activityRows, activities),
    [issueData.activityRows, activities]
  );

  const hoverState = useRecoilValue(rowHoverState);

  const noActivityCell = [
    <HoverableRow
      key={Number.MIN_SAFE_INTEGER}
      projectId={issueData.projectId}
      issueId={issueData.id}
    >
      <IssueActivityEmptyCell style={{ marginTop: activityRowOffset }}>
        {
          <AddActivityButton
            isVisibleButton={
              allowInsertingRows &&
              hoverState.projectId === issueData.projectId &&
              hoverState.issueId === issueData.id
            }
            activitiesById={activities}
            activities={unusedActivities}
            onSelectActivity={onAddActivity}
          />
        }
      </IssueActivityEmptyCell>
      <IssueSumEmptyCell />
    </HoverableRow>,
  ];

  return (
    <IssueRowHeader
      issue={issue}
      issues={issues}
      activityRowOffset={activityRowOffset}
      firstActivityId={sortedActivities[0]?.id}
    >
      {sortedActivities.length === 0
        ? noActivityCell
        : sortedActivities.map((activityRow, i) => {
            const activity = activities.get(activityRow.id) as Activity;
            const isFirstActivity = i === 0;
            const isLastActivity = i === sortedActivities.length - 1;
            const isHovering =
              hoverState.projectId === issueData.projectId &&
              hoverState.issueId === issueData.id &&
              hoverState.activityId === activityRow.id;

            return (
              <HoverableRow
                key={activityRow.id}
                projectId={issueData.projectId}
                issueId={issueData.id}
                activityId={activityRow.id}
              >
                <ActivityRowHeader
                  activity={activity}
                  rowHeight={
                    rowHeight +
                    (isLastIssue && (isFirstActivity || isLastActivity)
                      ? -1
                      : 0) -
                    (isFirstActivity ? 0 : activityRowOffset)
                  }
                  activityRowOffset={isFirstActivity ? activityRowOffset : 0}
                  timeEntrySum={activityRow.sum}
                  addActivityButton={
                    <AddActivityButton
                      isVisibleButton={allowInsertingRows && isHovering}
                      activitiesById={activities}
                      activities={unusedActivities}
                      onSelectActivity={onAddActivity}
                    />
                  }
                />
              </HoverableRow>
            );
          })}
    </IssueRowHeader>
  );
};

export default IssueRowHeaders;
