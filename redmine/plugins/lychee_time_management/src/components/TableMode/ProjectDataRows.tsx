import * as React from 'react';
import styled from 'styled-components';
import { useFormatHours } from '@/hooks/useFormatHours';
import {
  PROJECT_ROW_STYLE,
  TIME_ENTRY_CELL_STYLE,
  TIME_ENTRY_ROW_HEIGHT,
} from '@/lib/tableMode/cellStyle';
import { MapById } from '@/lib/tableMode/mapById';
import { ActivityRowData, ProjectData } from '@/lib/tableMode/tableData';
import {
  RowCoordinates,
  IssueRowAppearance,
  TimeEntryCoordinates,
  TimeEntryId,
  WorkMonth,
  Activity,
} from '@/lib/tableMode/types';
import { sortActivities } from '@/lib/tableMode/rowSorter';

import TableCell from './atoms/TableCell';
import TableRow, { rowHeightStyle } from './atoms/TableRow';
import ActivityRow, { CellComponentType } from './ActivityRow';
import IssueDataRows from './IssueDataRows';
import HoverableRow from './HoverableRow';

const ProjectSumRow = styled(TableRow)`
  ${PROJECT_ROW_STYLE}

  ${rowHeightStyle(TIME_ENTRY_ROW_HEIGHT)}
  mix-blend-mode: multiply;
`;

const ProjectSumCell = styled(TableCell).attrs({ role: 'cell' })`
  ${TIME_ENTRY_CELL_STYLE}
  text-align: right;
`;

const buildCoordinates = (
  projectData: ProjectData,
  activityRow: ActivityRowData
): RowCoordinates => ({
  activityType: 'project',
  activity: {
    projectId: projectData.id,
    activityId: activityRow.id,
  },
});

type ProjectDataRowsProps = {
  projectData: ProjectData;
  issueRowAppearances: MapById<IssueRowAppearance>;
  activitiesById: MapById<Activity>;
  workMonth: WorkMonth;
  CellComponent: CellComponentType;
  onEditMultipleTimeEntries: (
    ids: TimeEntryId[],
    cellElement: Element,
    coordinates: TimeEntryCoordinates
  ) => void;
};

const ProjectDataRows: React.FC<ProjectDataRowsProps> = ({
  projectData,
  issueRowAppearances,
  activitiesById,
  workMonth,
  CellComponent,
  onEditMultipleTimeEntries,
}: ProjectDataRowsProps) => {
  const formatTimeEntryHours = useFormatHours();
  const sortedActivities = React.useMemo(
    () => sortActivities(projectData.activityRows, activitiesById),
    [projectData.activityRows, activitiesById]
  );

  return (
    <>
      <HoverableRow projectId={projectData.id}>
        <ProjectSumRow>
          {workMonth.dates.map((workDate, index) => (
            <ProjectSumCell key={workDate.date}>
              {formatTimeEntryHours(projectData.sumByDate[index])}
            </ProjectSumCell>
          ))}
        </ProjectSumRow>
      </HoverableRow>
      {sortedActivities.map((activityRow) => {
        const rowCoordinates = buildCoordinates(projectData, activityRow);

        return (
          <HoverableRow
            key={activityRow.id}
            projectId={projectData.id}
            activityId={activityRow.id}
          >
            <ActivityRow
              workMonth={workMonth}
              row={activityRow}
              CellComponent={CellComponent}
              rowCoordinates={rowCoordinates}
              onEditMultipleTimeEntries={(ids, element, date): void => {
                onEditMultipleTimeEntries(ids, element, {
                  ...rowCoordinates,
                  date,
                });
              }}
            />
          </HoverableRow>
        );
      })}
      {projectData.dataForIssues.map((issueData) => {
        const issueRowAppearance = issueRowAppearances.get(
          issueData.id
        ) as IssueRowAppearance;
        return (
          <IssueDataRows
            key={issueData.id}
            issueData={issueData}
            activitiesById={activitiesById}
            rowHeight={issueRowAppearance.rowHeight}
            activityRowOffset={issueRowAppearance.activityRowOffset}
            CellComponent={CellComponent}
            onEditMultipleTimeEntries={onEditMultipleTimeEntries}
            workMonth={workMonth}
          />
        );
      })}
    </>
  );
};

export default ProjectDataRows;
