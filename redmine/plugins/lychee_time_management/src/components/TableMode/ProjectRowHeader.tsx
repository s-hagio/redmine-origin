import * as React from 'react';
import styled from 'styled-components';
import { useFormatHours } from '@/hooks/useFormatHours';
import {
  HOURS_CELL_RIGHT_PADDING,
  TIME_ENTRY_ROW_HEIGHT,
} from '@/lib/tableMode/cellStyle';
import { Activity, Project } from '@/lib/tableMode/types';

import TableCell from './atoms/TableCell';
import TableRow from './atoms/TableRow';
import AddActivityButton from './AddActivityButton';
import { MapById } from '@/lib/tableMode/mapById';
import HoverableRow from './HoverableRow';
import { useRecoilValue } from 'recoil';
import { rowHoverState } from '@/recoil/rowHoverState';

const Root = styled(TableRow)`
  border-top: none;
  width: fit-content;
`;

const RowHeader = styled(TableCell).attrs({ role: 'rowheader' })``;

export const ProjectNameCell = styled(RowHeader)`
  display: flex;
  justify-content: space-between;
  padding-left: 1em;
  text-align: left;
  padding-right: 0px;
  font-size: 13px;
  font-weight: bold;
  color: #116699;

  a {
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

export const ProjectRowSumCell = styled(RowHeader)`
  text-align: right;
  padding-right: ${HOURS_CELL_RIGHT_PADDING}px;
  height: ${TIME_ENTRY_ROW_HEIGHT}px;
`;

const RightFloatingArea = styled.div`
  height: 100%;
`;

type ProjectRowHeaderProps = {
  className?: string;
  project: Project;
  timeEntrySum: number;
  activitiesById: MapById<Activity>;
  availableActivities: Activity[];
  onAddActivity: (activityId: number) => void;
  allowInsertingRows: boolean;
};

const ProjectRowHeader: React.FC<ProjectRowHeaderProps> = ({
  className,
  project,
  timeEntrySum,
  activitiesById,
  availableActivities,
  onAddActivity,
  allowInsertingRows,
}: ProjectRowHeaderProps) => {
  const formatTimeEntryHours = useFormatHours();
  const hoverState = useRecoilValue(rowHoverState);
  const isHovering = React.useMemo(
    () =>
      hoverState.projectId === project.id &&
      hoverState.issueId == null &&
      hoverState.activityId == null,
    [hoverState, project]
  );

  return (
    <Root className={className} rowHeight={TIME_ENTRY_ROW_HEIGHT}>
      <HoverableRow projectId={project.id}>
        <ProjectNameCell role="rowheader">
          <a href={project.url} tabIndex={-1}>
            {project.name}
          </a>
          <RightFloatingArea>
            <AddActivityButton
              isVisibleButton={allowInsertingRows && isHovering}
              activitiesById={activitiesById}
              activities={availableActivities}
              onSelectActivity={onAddActivity}
            />
          </RightFloatingArea>
        </ProjectNameCell>
      </HoverableRow>
      <HoverableRow projectId={project.id}>
        <ProjectRowSumCell>
          {formatTimeEntryHours(timeEntrySum)}
        </ProjectRowSumCell>
      </HoverableRow>
    </Root>
  );
};

export default ProjectRowHeader;
