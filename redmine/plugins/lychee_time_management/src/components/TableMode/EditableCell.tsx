import * as React from 'react';
import styled, { css } from 'styled-components';

import { useFormatHours } from '@/hooks/useFormatHours';
import HoursInput from './atoms/HoursInput';
import TableCell from './atoms/TableCell';

const InputField = styled(HoursInput)`
  box-sizing: border-box;
  width: calc(100% - 0.2em);
  margin-left: 0.1em;
  margin-right: 0.1em;
`;

const focusedTableCellStyle = css<{ isFocused?: boolean }>`
  outlineoffset: -2;
  outline: ${({ isFocused }) => (isFocused ? '2px solid gray' : 'none')};
`;
const EditableTableCell = styled(TableCell).attrs<{ isFocused?: boolean }>(
  ({ isFocused }) => ({
    tabIndex: isFocused ? 0 : -1,
  })
)<{ isFocused?: boolean }>`
  text-align: right;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  outline: none;
  ${({ isFocused }) => isFocused && focusedTableCellStyle}
`;

type EditableCellProps = {
  edit: boolean;
  onBlur?: () => void;
  value?: number;
  className?: string;
  onClick?: React.MouseEventHandler<HTMLDivElement>;
  onContextMenu?: React.MouseEventHandler<HTMLDivElement>;
  onUpdate?: (
    value: string,
    afterUpdateFocusBehavior?: 'MOVE_DOWN' | 'MOVE_RIGHT'
  ) => Promise<boolean>;
  offset?: number;
  isFocused?: boolean;
};

const EditableCell: React.FC<EditableCellProps> = ({
  edit,
  onBlur,
  value,
  onClick,
  onContextMenu,
  onUpdate,
  className,
  offset = 0,
  isFocused,
}: EditableCellProps) => {
  const formatTimeEntryHours = useFormatHours();
  const processing = React.useRef(false);
  const inputRef = React.useRef<HTMLInputElement>(null);
  const [defaultValue, setDefaultValue] = React.useState(
    formatTimeEntryHours(value, { round: false })
  );
  React.useEffect(() => {
    processing.current = false;
    if (edit && inputRef.current) {
      inputRef.current.focus();
    }
  }, [edit]);
  const handleKeyDown = React.useCallback(
    async (event: React.KeyboardEvent<HTMLInputElement>) => {
      if (!onUpdate || !inputRef.current) return;

      if (event.key === 'Escape') {
        inputRef.current.value = defaultValue;
        inputRef.current.blur();
        return;
      }

      const afterUpdateBehaviorByKey = {
        Enter: 'MOVE_DOWN',
        Tab: 'MOVE_RIGHT',
      } as const;
      if (
        (event.key === 'Enter' || event.key === 'Tab') &&
        !processing.current
      ) {
        event.preventDefault();
        processing.current = true;
        try {
          if (
            await onUpdate(
              inputRef.current.value,
              afterUpdateBehaviorByKey[event.key]
            )
          ) {
            setDefaultValue(inputRef.current.value);
            onBlur?.();
          }
        } finally {
          processing.current = false;
        }
      }
    },
    [defaultValue, onBlur, onUpdate]
  );

  const handleBlur = React.useCallback(async () => {
    if (onUpdate && inputRef.current && !processing.current) {
      processing.current = true;
      try {
        await onUpdate(inputRef.current.value);
      } finally {
        processing.current = false;
      }
    }
    onBlur?.();
  }, [onUpdate, onBlur]);

  const cellRef = React.useRef<HTMLDivElement>(null);

  React.useEffect(() => {
    if (isFocused) {
      requestAnimationFrame(() => {
        cellRef?.current?.scrollIntoView({
          inline: 'nearest',
          block: 'nearest',
        });
      });
    }
  }, [isFocused]);

  return (
    <EditableTableCell
      ref={cellRef}
      className={className}
      role="cell"
      onClick={onClick}
      onContextMenu={onContextMenu}
      isFocused={isFocused}
    >
      {edit ? (
        <InputField
          onKeyDown={handleKeyDown}
          onBlur={handleBlur}
          ref={inputRef}
          defaultValue={formatTimeEntryHours(value, { round: false })}
          style={{ marginTop: offset, outline: 'none' }}
        />
      ) : (
        <span
          style={{
            display: 'inline-block',
            marginTop: offset,
            outline: 'none',
          }}
        >
          {formatTimeEntryHours(value)}
        </span>
      )}
    </EditableTableCell>
  );
};

export default EditableCell;
