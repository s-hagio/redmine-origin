import styled from 'styled-components';

const DEFAULT_BUTTON_COLOR = '#9b9b9b';
const DEFAULT_BUTTON_BORDER_COLOR = '#555';
const DEFAULT_BUTTON_BG_COLOR = 'white';

type ButtonProps = {
  textColor?: string;
  borderColor?: string;
  backgroundColor?: string;
};

const Button = styled.button<ButtonProps>`
  display: block;
  background-color: ${({ backgroundColor }): string =>
    backgroundColor || DEFAULT_BUTTON_BG_COLOR};
  color: ${({ textColor }): string => textColor || DEFAULT_BUTTON_COLOR};
  border-color: ${({ borderColor }): string =>
    borderColor || DEFAULT_BUTTON_BORDER_COLOR};
  font-weight: bold;
  cursor: pointer;
  border-radius: 4px;
  padding: 5px 10px;
  margin: 0;
  font-size: 11px;
  line-height: 1.1;
`;

export default Button;
