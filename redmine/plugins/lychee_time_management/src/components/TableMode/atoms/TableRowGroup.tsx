import * as React from 'react';
import styled from 'styled-components';

import {
  TABLE_BORDER_STYLE,
  TIME_ENTRY_ROW_HEIGHT,
} from '@/lib/tableMode/cellStyle';

import TableRow, { rowHeightStyle } from './TableRow';
import HoverableRow from '../HoverableRow';

const Root = styled.div.attrs({ role: 'rowgroup' })<{
  height?: number;
}>`
  display: flex;
  align-items: stretch;
  box-sizing: border-box;

  ${({ height }): string => rowHeightStyle(height || TIME_ENTRY_ROW_HEIGHT)}

  border-top: ${TABLE_BORDER_STYLE};
`;

export const RowContainerCell = styled.div`
  display: flex;
  flex-direction: column;
  box-sizing: border-box;

  :first-child {
    border-left-style: none; // Don't render left border when starting directly with rows, they already render left border
  }

  && ${HoverableRow} {
    ${TableRow} {
      border-top: none;
      border-bottom: ${TABLE_BORDER_STYLE};
    }

    :last-child {
      ${TableRow} {
        border-bottom: none;
      }
    }
  }
`;

type Props = {
  cellsBeforeRows?: React.ReactNode;
  className?: string;
  height?: number;
  children: React.ReactNode;
} & React.AriaAttributes;

const TableRowGroup: React.FC<Props> = ({
  cellsBeforeRows,
  children,
  className,
  height,
  ...ariaAttributes
}: React.PropsWithChildren<Props>) => (
  <Root height={height} className={className} {...ariaAttributes}>
    {cellsBeforeRows}
    <RowContainerCell>{children}</RowContainerCell>
  </Root>
);

export default TableRowGroup;
