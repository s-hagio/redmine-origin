import * as React from 'react';
import styled from 'styled-components';

import { useLabels } from '@/lib/tableMode/labels';

const Root = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10000;
`;

const Text = styled.div`
  font-size: 20px;
  padding: 20px;
  border-radius: 20px;
  color: rgb(100, 100, 100);
  background-color: rgba(200, 200, 200, 0.5);
`;

const LoadingOverlay: React.FC = () => {
  const l = useLabels();

  return (
    <Root>
      <Text>{l('label_loading')}</Text>
    </Root>
  );
};

export default LoadingOverlay;
