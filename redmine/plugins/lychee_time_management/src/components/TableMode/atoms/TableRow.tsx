import styled from 'styled-components';
import { CELL_PADDING, TABLE_BORDER_STYLE } from '@/lib/tableMode/cellStyle';
import TableCell from './TableCell';

export const lineHeightFromHeight = (height: number): number =>
  height - 2 * CELL_PADDING - 1; // 「-1」はborder-top分

export const rowHeightStyle = (height: number): string => `
  height: ${height}px;

  ${TableCell} {
    line-height: ${lineHeightFromHeight(height)}px;
  }
`;

type TableRowProps = {
  rowHeight?: number;
};

const TableRow = styled.div.attrs(({ role }) => ({
  role: role !== undefined ? role : 'row',
}))<TableRowProps>`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-items: stretch;
  box-sizing: border-box;
  border-top: ${TABLE_BORDER_STYLE};
  ${({ rowHeight }): string =>
    rowHeight
      ? `height: ${rowHeight}px;
         line-height: ${lineHeightFromHeight(rowHeight)}px;`
      : ''}

  ${TableCell} {
    line-height: inherit; // 「ステータス」列のline-height避け
  }
}
`;

export default TableRow;
