import * as React from 'react';

type HtmlInputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

type HoursInputProps = Omit<
  HtmlInputProps,
  'onChange' | 'value' | 'defaultValue'
> & {
  value?: string;
  defaultValue?: string;
  // valueが空文字の場合でも呼ばれる
  onChange?: (value: string) => void;
};

const TOO_LONG_FLOAT = /^(\d+\.\d{2})\d+$/;

const roundFloatStringIfNecessary = (value: string): string => {
  if (TOO_LONG_FLOAT.test(value)) {
    return Number(value).toFixed(2);
  }
  return value;
};

const truncateFloatStringIfNecessary = (value: string): string => {
  const match = value.match(TOO_LONG_FLOAT);
  if (match) {
    return match[1] as string;
  }

  return value;
};

const HoursInput = React.forwardRef<HTMLInputElement, HoursInputProps>(
  ({ value, onChange, defaultValue, ...inputProps }: HoursInputProps, ref) => {
    const [currentValue, setCurrentValue] = React.useState<string>(
      roundFloatStringIfNecessary(value || defaultValue || '')
    );

    React.useEffect(() => {
      if (value) {
        const truncatedValue = roundFloatStringIfNecessary(value);
        setCurrentValue(truncatedValue);
      }
    }, [value]);

    const handleChange = React.useCallback(
      (event: React.ChangeEvent<HTMLInputElement>) => {
        event.target.setAttribute('type', 'text');
        const truncatedInput = truncateFloatStringIfNecessary(
          event.target.value
        );
        setCurrentValue(truncatedInput);
        if (
          onChange &&
          (truncatedInput === '' || !isNaN(parseFloat(truncatedInput)))
        ) {
          onChange(truncatedInput);
        }
      },
      [onChange]
    );

    const handleFocus = (e: React.FocusEvent<HTMLInputElement>) => {
      e.target.setAttribute('type', 'url');
    };

    return (
      <input
        type={currentValue === '' ? 'url' : 'text'}
        title="" // ブラウザのデフォルト表示を消すため
        value={currentValue}
        onChange={handleChange}
        onFocus={handleFocus}
        {...inputProps}
        ref={ref}
      />
    );
  }
);

HoursInput.displayName = 'HoursInput';

export default HoursInput;
