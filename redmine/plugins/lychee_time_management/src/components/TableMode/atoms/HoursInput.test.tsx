import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as React from 'react';

import HoursInput from './HoursInput';

const setup = (jsx: JSX.Element) => {
  return {
    user: userEvent.setup(),
    ...render(jsx),
  };
};

describe('HoursInput', () => {
  it('emits entered number', async () => {
    const handleChange = jest.fn();
    const { user } = setup(<HoursInput onChange={handleChange} />);

    const input = screen.getByRole('textbox');
    await user.type(input, '22.5');

    expect(handleChange).toHaveBeenCalledWith('22.5');
  });

  it('only emits valid floats', async () => {
    const handleChange = jest.fn();
    const { user } = setup(<HoursInput onChange={handleChange} />);

    const input = screen.getByRole('textbox');
    await user.type(input, 'a');

    expect(handleChange).not.toHaveBeenCalled();
  });

  it('emits empty value via onChange', async () => {
    const handleChange = jest.fn();
    const { user } = setup(<HoursInput onChange={handleChange} />);

    const input = screen.getByRole('textbox');
    await user.type(input, '100');
    await user.clear(input);

    expect(handleChange).toHaveBeenCalledTimes(4);
    expect(handleChange).toHaveBeenLastCalledWith('');
  });

  it.each(['1.25', '1h15m', '1h', '15m', '1:15'])(
    'emits redmine valid time value via onChange',
    async (value) => {
      const handleChange = jest.fn();
      const { user } = setup(<HoursInput onChange={handleChange} />);

      const input = screen.getByRole('textbox');
      await user.type(input, value);

      expect(handleChange).toHaveBeenCalledWith(value);
    }
  );

  it.each([
    ['19.124', '19.12'],
    ['19.125', '19.13'],
  ])(
    'rounds the initial value to two decimal places (%s -> %s)',
    async (defaultValue, expectedInputValue) => {
      setup(<HoursInput defaultValue={defaultValue} />);
      const input = screen.getByRole('textbox');

      expect(input).toHaveValue(expectedInputValue);
    }
  );

  it('allows a maximum of 2 decimal digits', async () => {
    const handleChange = jest.fn();
    const { user } = setup(<HoursInput onChange={handleChange} />);

    const input = screen.getByRole('textbox');
    await user.type(input, '22.559');

    expect(handleChange).toHaveBeenCalledWith('22.55');
    expect(input).toHaveValue('22.55');
  });

  it('updates when value changes', () => {
    const { rerender } = setup(<HoursInput value="22.2" />);
    rerender(<HoursInput value="22.22" />);

    const input = screen.getByRole('textbox');
    expect(input).toHaveValue('22.22');
  });

  it('is empty by default', () => {
    setup(<HoursInput />);

    const input = screen.getByRole('textbox');
    expect(input).toHaveValue('');
  });

  it('can receive an empty value', () => {
    setup(<HoursInput value="" />);

    const input = screen.getByRole('textbox');
    expect(input).toHaveValue('');
  });
});
