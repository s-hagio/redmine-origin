import styled from 'styled-components';
import { CELL_PADDING, TABLE_BORDER_STYLE } from '@/lib/tableMode/cellStyle';

const TableCell = styled.div.attrs(({ role }) => ({
  role: role !== undefined ? role : 'cell',
}))`
  text-align: left;
  box-sizing: border-box;
  display: inline-block;
  padding: ${CELL_PADDING}px;
  border-left: ${TABLE_BORDER_STYLE};
  :first-child {
    border-left-width: 0;
  }
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  color: #555555;
  font-size: 12px;
`;

export default TableCell;
