import * as React from 'react';
import styled from 'styled-components';

const Root = styled.button`
  background: none;
  border: none;
  color: grey;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
`;

type ImageButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  src: string;
  height: number;
  width: number;
};

const ImageButton = React.forwardRef<HTMLButtonElement, ImageButtonProps>(
  ({ src, height, width, ...restProps }, ref) => {
    return (
      <Root {...restProps} ref={ref}>
        <img src={src} height={height} width={width} />
      </Root>
    );
  }
);

ImageButton.displayName = 'ImageButton';

export default ImageButton;
