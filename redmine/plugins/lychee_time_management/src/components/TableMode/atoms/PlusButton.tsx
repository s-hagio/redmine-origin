import * as React from 'react';
import styled from 'styled-components';
import addBlueImage from '@/assets/icn_add_blue@2x.png';
import addWhiteImage from '@/assets/icn_add_white@2x.png';

const DEFAULT_BUTTON_COLOR = '#116699';
const DEFAULT_BUTTON_BORDER_COLOR = '#116699';
const DEFAULT_BUTTON_BG_COLOR = '#EFEFEF';

const FILLED_BUTTON_COLOR = '#FFFFFF';
const FILLED_BUTTON_BG_COLOR = '#116699';

type PlusButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  filled?: boolean;
};

const Button = styled.button<PlusButtonProps>`
  display: flex;
  align-items: center;
  background-color: ${({ filled }): string =>
    filled ? FILLED_BUTTON_BG_COLOR : DEFAULT_BUTTON_BG_COLOR};
  color: ${({ filled }): string =>
    filled ? FILLED_BUTTON_COLOR : DEFAULT_BUTTON_COLOR};
  border-color: ${DEFAULT_BUTTON_BORDER_COLOR};
  cursor: pointer;
  border-radius: 4px;
  margin: 0;
  font-size: 11px;
  vertical-align: middle;
`;

const PlusButton: React.FC<PlusButtonProps> = ({
  children,
  filled = false,
  ...restProps
}) => {
  return (
    <Button filled={filled} {...restProps}>
      <img src={filled ? addWhiteImage : addBlueImage} height={18} width={18} />
      {children}
    </Button>
  );
};

export default PlusButton;
