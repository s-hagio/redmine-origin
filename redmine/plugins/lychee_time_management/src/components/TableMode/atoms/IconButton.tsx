import * as React from 'react';
import {
  FaAngleDoubleLeft,
  FaAngleDoubleRight,
  FaPlus,
  FaTrash,
} from 'react-icons/fa';
import styled from 'styled-components';

const Root = styled.button`
  background: none;
  border: none;
  padding: 2px;
  color: grey;
  cursor: pointer;
  height: unset;
  margin: unset;
  display: inline-block;
  line-height: 1;
`;

const ICONS = {
  next: FaAngleDoubleRight,
  plus: FaPlus,
  previous: FaAngleDoubleLeft,
  trash: FaTrash,
};

type IconButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  icon: keyof typeof ICONS;
};

const IconButtonButton = React.forwardRef<HTMLButtonElement, IconButtonProps>(
  ({ icon, ...otherProps }: IconButtonProps, ref) => {
    const IconComponent = ICONS[icon] as React.ComponentType;
    return (
      <Root {...otherProps} ref={ref}>
        <IconComponent />
      </Root>
    );
  }
);

IconButtonButton.displayName = 'IconButtonButton';

export default IconButtonButton;
