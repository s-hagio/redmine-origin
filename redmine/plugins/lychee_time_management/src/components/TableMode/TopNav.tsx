import LeftArrowImage from '@/assets/icn_chevron_left@2x.png';
import RightArrowImage from '@/assets/icn_chevron_right@2x.png';
import { SortKeySelector } from '@/components/TableMode/SortKeySelector';
import { useLabels } from '@/lib/tableMode/labels';
import { IssueSortKey } from '@/lib/tableMode/rowSorter';
import {
  CurrentUser,
  TargetUser,
  WorkDate,
  WorkMonth,
} from '@/lib/tableMode/types';
import { rowHeaderWidth as rowHeaderWidthState } from '@/recoil/cellWidths';
import * as React from 'react';
import { useRecoilValue } from 'recoil';
import styled from 'styled-components';
import Button from './atoms/Button';
import ImageButton from './atoms/ImageButton';
import { WorkMonthProvider } from './Root';
import TargetUserSelect from './TargetUserSelect';

const TodayButton = styled(Button)`
  border-radius: 6px;
  height: 36px;
  font-size: 15px;
  font-weight: normal;
  color: #000000;
  border: 1px #ebebeb solid;
`;

const LeftSelectorArea = styled.div`
  display: flex;
  align-items: center;
  gap: 20px;
`;

type TopNavProps = {
  workMonthProvider: WorkMonthProvider;
  onChangeWorkMonth: (workMonth: WorkMonth) => void;
  currentUser: CurrentUser;
  targetUserId: number;
  targetUsers: TargetUser[];
  onChangeTargetUserId: (id: number) => void;
  issueSortKey: IssueSortKey;
  onChageIssueSortKey: (issueSortKey: IssueSortKey) => void;
};

const formatDatesRange = (dates: WorkDate[]): string =>
  `${dates[0].date.replace(/-/g, '/')} 〜 ${dates[
    dates.length - 1
  ].date.replace(/-/g, '/')}`;

const TopNav: React.FC<TopNavProps> = ({
  workMonthProvider,
  onChangeWorkMonth,
  currentUser,
  targetUserId,
  targetUsers,
  onChangeTargetUserId,
  issueSortKey,
  onChageIssueSortKey,
}: TopNavProps) => {
  const rowHeaderWidth = useRecoilValue(rowHeaderWidthState);

  const l = useLabels();

  const [workMonth, setWorkMonth] = React.useState(
    workMonthProvider.getCurrent()
  );

  const goToPreviousMonth = React.useCallback(() => {
    const previousWorkMonth = workMonthProvider.getPreviousMonth(workMonth);
    setWorkMonth(previousWorkMonth);
    onChangeWorkMonth(previousWorkMonth);
  }, [onChangeWorkMonth, workMonth, workMonthProvider]);

  const goToNextMonth = React.useCallback(() => {
    const nextWorkMonth = workMonthProvider.getNextMonth(workMonth);
    setWorkMonth(nextWorkMonth);
    onChangeWorkMonth(nextWorkMonth);
  }, [onChangeWorkMonth, workMonth, workMonthProvider]);

  const goToCurrentMonth = React.useCallback(() => {
    const currentMonth = workMonthProvider.getCurrent();
    setWorkMonth(currentMonth);
    onChangeWorkMonth(currentMonth);
  }, [onChangeWorkMonth, workMonthProvider]);

  const TopNavContainer = styled.div`
    display: flex;
    flex: 1;
    justify-content: start;
    align-items: center;
    padding: 4px 4px;
  `;

  const HeaderLabel = styled.span`
    font-size: 20px;
  `;

  const MonthSelector = styled.nav.attrs({ ['aria-label']: '月の選択' })<{
    marginLeft: number;
  }>`
    display: flex;
    padding: 0px 10px;
    align-items: center;
  `;

  const MonthRangeLabel = styled.span`
    font-size: 15px;
    color: #333333;
    padding-top: 1px;
  `;

  return (
    <TopNavContainer>
      <HeaderLabel>{l('label_work_time')}</HeaderLabel>
      <MonthSelector marginLeft={rowHeaderWidth}>
        <ImageButton
          aria-label="前"
          src={LeftArrowImage}
          height={24}
          width={24}
          onClick={goToPreviousMonth}
        />
        <MonthRangeLabel>{formatDatesRange(workMonth.dates)}</MonthRangeLabel>
        <ImageButton
          aria-label="次"
          src={RightArrowImage}
          height={24}
          width={24}
          onClick={goToNextMonth}
        />
      </MonthSelector>
      <TodayButton onClick={goToCurrentMonth}>
        {l('button_capitalized_today')}
      </TodayButton>
      <div style={{ flexGrow: 1 }} />
      <LeftSelectorArea>
        <SortKeySelector value={issueSortKey} onChange={onChageIssueSortKey} />
        <TargetUserSelect
          currentUser={currentUser}
          targetUserId={targetUserId}
          targetUsers={targetUsers}
          onChangeTargetUserId={onChangeTargetUserId}
        />
      </LeftSelectorArea>
    </TopNavContainer>
  );
};

export default TopNav;
