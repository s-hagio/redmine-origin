import * as React from 'react';
import styled from 'styled-components';

import {
  CustomField,
  CustomFieldConfig,
  CustomFieldInput,
} from '@/components/RedmineCustomField';
import { useFormatHours } from '@/hooks/useFormatHours';
import { useLabels } from '@/lib/tableMode/labels';
import {
  AttachmentApiHandlerWithMemory,
  Permission,
  TimeEntry,
  TimeEntryId,
  UpdatableTimeEntryAttributes,
} from '@/lib/tableMode/types';

import ReactFocusLock from 'react-focus-lock';
import { match, P } from 'ts-pattern';
import { FieldFormat } from '../RedmineCustomField/types';
import HoursInput from './atoms/HoursInput';
import IconButton from './atoms/IconButton';

const SelectedTimeEntry = styled.span`
  font-weight: bold;
`;

const EntryName = styled.span`
  flex-grow: 1;
  a {
    cursor: pointer;
  }
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const EntryHours = styled.span`
  margin-right: 10px;
`;

const FormattedTimeEntry: React.FC<{ timeEntry: TimeEntry }> = ({
  timeEntry,
}) => {
  const formatTimeEntryHours = useFormatHours();
  return (
    <>
      <EntryHours>{formatTimeEntryHours(timeEntry.hours)}</EntryHours>
      {timeEntry.comments}
    </>
  );
};

const TimeEntryItem = styled.div.attrs({ role: 'listitem' })`
  display: flex;
  align-items: center;
`;

const RequiredAsterisk = styled.span`
  color: red;
`;

const TimeEntryCustomFieldInput = styled(CustomFieldInput)``;

const TimeEntryField = styled.div`
  & > * {
    display: inline-block;
  }
  & > label {
    vertical-align: top;
    width: 80px;
    padding-right: 10px;
    font-weight: bold;
    text-align: right;
  }
  & > ${TimeEntryCustomFieldInput}, & > input {
    width: calc(100% - 80px - 10px);
    box-sizing: border-box;
  }
`;

export type TimeEntriesEditorProps = {
  attachmentApi: AttachmentApiHandlerWithMemory;
  onCreate: (attributes: UpdatableTimeEntryAttributes) => void;
  onDelete: (id: TimeEntryId) => void;
  onUpdate: (
    id: TimeEntryId,
    attributes: Partial<UpdatableTimeEntryAttributes>
  ) => void;
  onCancel: () => void;
  requiredFields: string[];
  timeEntries: TimeEntry[];
  customFields: CustomField[];
  permissions: Permission[];
};

type FormData = {
  timeEntry?: TimeEntry;
  hours: string;
  comments: string;
  customFieldValues: { [id: number]: unknown };
};

const initFormData = (customFields: CustomField[]): FormData => {
  const result: FormData = {
    hours: '',
    comments: '',
    customFieldValues: {},
  };
  customFields.forEach((customField) => {
    result.customFieldValues[customField.id] = customField.multiple ? [customField.defaultValue] : customField.defaultValue;
  });
  return result;
};

const parseFormData = ({
  hours,
  comments,
  customFieldValues,
}: FormData): UpdatableTimeEntryAttributes => {
  const result: UpdatableTimeEntryAttributes = {
    hours,
    comments,
  };
  if (Object.keys(customFieldValues).length > 0) {
    result.customFieldValues = { ...customFieldValues };
  }

  return result;
};

const isValid = (
  { hours, comments }: FormData,
  requiredFields: string[]
): boolean => {
  if (hours === '' || isNaN(parseFloat(hours))) {
    return false;
  }

  if (requiredFields.includes('comments') && comments === '') {
    return false;
  }

  return true;
};

const Form: React.FC<{
  onSubmit: () => void;
  disabled: boolean;
  children: React.ReactNode;
}> = ({ onSubmit, disabled, children }) => {
  const processing = React.useRef(false);
  const handleSubmit = (e: React.FormEvent): void => {
    e.preventDefault();
    if (processing.current) {
      return;
    }
    processing.current = true;
    onSubmit();
  };

  React.useEffect(() => {
    processing.current = false;
  }, [disabled]);

  return (
    <form onSubmit={handleSubmit}>
      {children}
    </form>
  );
};

const TimeEntriesEditor: React.FC<TimeEntriesEditorProps> = ({
  attachmentApi,
  onCreate,
  onDelete,
  onUpdate,
  customFields,
  permissions,
  requiredFields,
  timeEntries,
  onCancel,
}: TimeEntriesEditorProps) => {
  const l = useLabels();
  const formatHours = useFormatHours();
  const [customFieldConfig, setCustomFieldConfig] =
    React.useState<CustomFieldConfig>({
      attachmentApi,
      // 新規レコードでファイル型カスタムフィールドにアップロードされた添付ファイルをすぐに削除しないとサーバーに残ってしまう
      deleteAttachmentsImmediately: true,
      labels: {
        none: l('label_none'),
        pleaseSelect: l('actionview_instancetag_blank_option'),
      },
    });
  const [formData, setFormData] = React.useState<FormData>(
    initFormData(customFields)
  );
  const [isJapaneseInput, setIsJapaneseInput] = React.useState<boolean>(false);

  const toggleIsJapaneseInput = React.useCallback(() => {
    setIsJapaneseInput((prev) => !prev);
  }, []);

  const requiredAsterisk = React.useMemo(
    () => <RequiredAsterisk>*</RequiredAsterisk>,
    []
  );

  const handleClickTimeEntry = React.useCallback(
    (timeEntry: TimeEntry) => {
      (async (): Promise<void> => {
        await attachmentApi.deleteUploadedAttachments();
        const customFieldValues = { ...(timeEntry.customFieldValues || {}) };
        customFields.forEach((customField) => {
          // カスタムフィールドの追加前に入力されたTimeEntryの編集時は
          // カスタムフィールドのデフォルト値を使わない
          customFieldValues[customField.id] ??= customField.multiple ? [] : '';
        });
        setFormData({
          timeEntry,
          hours: formatHours(timeEntry.hours, { round: false }),
          comments: timeEntry.comments || '',
          customFieldValues,
        });
        // 既存レコードを編集するときに、ファイル型カスタムフィールドの値が空で送ったら、添付ファイルが自動的に削除される
        // つまりフロントエンドからの削除リクエストは送らない方がいい
        setCustomFieldConfig({
          ...customFieldConfig,
          deleteAttachmentsImmediately: false,
        });
      })();
    },
    [attachmentApi, customFieldConfig, formatHours]
  );

  const handleSubmit = React.useCallback(() => {
    const attributes = parseFormData(formData);
    if (formData.timeEntry) {
      const { timeEntry } = formData;
      onUpdate(timeEntry.id, attributes);
    } else {
      onCreate(attributes);
    }
    attachmentApi.clearMemory();
  }, [attachmentApi, formData, onCreate, onUpdate]);

  const creatable = permissions.includes('create');
  const editable = permissions.includes('edit');

  const createOperationKeyDownHandler: (
    fieldFormat?: FieldFormat
  ) => React.KeyboardEventHandler<HTMLInputElement> =
    (fieldFormat) => (event) => {
      match([event.key, event.ctrlKey, fieldFormat])
        .with(['Enter', true, P.union('text', 'attachment')], () => {
          event.preventDefault();
          if (!isValid(formData, requiredFields)) {
            requestAnimationFrame(() => {
              // textareaの場合、Enterでform側のsubmit処理が動いてしまい、そちらにフォーカスを持っていかれるのでここで戻す
              if (fieldFormat === 'text') {
                (event.nativeEvent.target as HTMLInputElement).focus();
              }
            });
            return;
          }
          handleSubmit();
        })
        .with(['Enter', P._, P.not(P.union('text', 'attachment'))], () => {
          event.preventDefault();
          if (!isValid(formData, requiredFields) || isJapaneseInput) return;
          handleSubmit();
        })
        .with(['Escape', P._, P._], () => {
          event.preventDefault();
          onCancel();
        })
        .otherwise(() => ({}));
    };

  return (
    <div>
      <div role="list">
        {timeEntries.map((timeEntry) => (
          <TimeEntryItem key={timeEntry.id} aria-label={timeEntry.comments}>
            <EntryName title={timeEntry.comments}>
              {formData.timeEntry === timeEntry || !editable ? (
                <SelectedTimeEntry>
                  <FormattedTimeEntry timeEntry={timeEntry} />
                </SelectedTimeEntry>
              ) : (
                <a
                  role="button"
                  onClick={(): void => {
                    handleClickTimeEntry(timeEntry);
                  }}
                  aria-label="編集"
                >
                  <FormattedTimeEntry timeEntry={timeEntry} />
                </a>
              )}
            </EntryName>
            {editable && (
              <IconButton
                icon="trash"
                aria-label="削除"
                onClick={(): void => {
                  if (window.confirm(l('text_are_you_sure'))) {
                    onDelete(timeEntry.id);
                  }
                }}
              />
            )}
          </TimeEntryItem>
        ))}
      </div>
      <hr />
      {(!!formData.timeEntry || creatable) && (
        <ReactFocusLock>
          <Form
            disabled={!isValid(formData, requiredFields)}
            onSubmit={handleSubmit}
          >
            <TimeEntryField>
              <label id="hoursLabel">
                {l('field_hours')} {requiredAsterisk}
              </label>
              <HoursInput
                required
                aria-labelledby="hoursLabel"
                value={formData.hours}
                autoFocus={true}
                onKeyDown={createOperationKeyDownHandler()}
                onChange={(value): void => {
                  setFormData({
                    ...formData,
                    hours: value,
                  });
                }}
              />
            </TimeEntryField>
            <TimeEntryField>
              <label id="commentLabel">
                {l('field_comments')}
                {requiredFields.includes('comments') ? requiredAsterisk : null}
              </label>
              <input
                type="text"
                name="time_entry[comments]"
                aria-labelledby="commentLabel"
                value={formData.comments}
                required={requiredFields.includes('comments')}
                onKeyDown={createOperationKeyDownHandler()}
                onChange={(event): void => {
                  setFormData({
                    ...formData,
                    comments: event.target.value,
                  });
                }}
                onCompositionStart={toggleIsJapaneseInput}
                onCompositionEnd={toggleIsJapaneseInput}
              />
            </TimeEntryField>
            {customFields.map((customField) => {
              const labelId = `cf${customField.id}Label`;
              const fieldFormat = customField.fieldFormat;

              return (
                <TimeEntryField key={customField.id}>
                  <label id={labelId}>
                    {customField.name}
                    {customField.isRequired && requiredAsterisk}
                  </label>
                  <TimeEntryCustomFieldInput
                    aria-labelledby={labelId}
                    customField={customField}
                    config={customFieldConfig}
                    name={
                      // https://github.com/redmine/redmine/blob/5.0.5/app/views/timelog/_form.html.erb#L33
                      // https://github.com/redmine/redmine/blob/5.0.5/app/helpers/custom_fields_helper.rb#L71
                      `time_entry[custom_field_values][${customField.id}]`
                    }
                    value={formData.customFieldValues[customField.id]}
                    onKeyDown={createOperationKeyDownHandler(fieldFormat)}
                    onChange={(value): void => {
                      setFormData({
                        ...formData,
                        customFieldValues: {
                          ...formData.customFieldValues,
                          [customField.id]: value,
                        },
                      });
                    }}
                    onCompositionStart={toggleIsJapaneseInput}
                    onCompositionEnd={toggleIsJapaneseInput}
                  />
                </TimeEntryField>
              );
            })}
            <input
              type="submit"
              value={formData.timeEntry ? l('button_save') : l('button_create')}
              disabled={!isValid(formData, requiredFields)}
            />
          </Form>
        </ReactFocusLock>
      )}
    </div>
  );
};

export default TimeEntriesEditor;
