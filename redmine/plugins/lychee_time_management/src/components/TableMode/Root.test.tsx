import { act } from 'react-dom/test-utils';
import {
  AttachmentApiHandler,
  CustomField,
} from '@/components/RedmineCustomField';
import {
  Activity,
  CurrentUser,
  Issue,
  IssueTimeEntry,
  Permission,
  Project,
  TimeEntry,
  WorkDate,
  WorkMonth,
} from '@/lib/tableMode/types';
import {
  buildActivity,
  buildCustomField,
  buildIndirectTimeEntry,
  buildIssue,
  buildIssueTimeEntry,
  buildProject,
  buildProjectTimeEntry,
  buildUser,
  buildWorkMonth,
  generateId,
} from '@/lib/test/factories';
import {
  expectBackgroundColors,
  expectEmptyText,
  expectFocusCell,
  expectNotFocusCell,
  expectTextContents,
} from '@/lib/test/matchers';
import { buildElementQueries, getMenuWithName } from '@/lib/test/queries';
import {
  fireEvent,
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as React from 'react';
import { RecoilRoot } from 'recoil';

import { match } from 'ts-pattern';
import Root, { TimeEntryDataStore, WorkMonthProvider } from './Root';

type UserEvent = ReturnType<typeof userEvent.setup>;

const openContextMenu = (element: HTMLElement): void => {
  fireEvent.contextMenu(element);
};

/* eslint-disable */
const labels = {
  button_create: '作成',
  button_save: '保存',
  button_add_selected_issues: '選択したチケットを配置',
  button_cancel_selection: 'キャンセル',
  button_add_project: 'プロジェクト追加',
  button_add_task: 'タスク追加',
  button_take_over_project: '前月のプロジェクトを引き継ぐ',
  label_indirect_activities: '間接作業',
  label_loading: 'ロード中...',
  label_select_issues: 'チケット選択',
  label_query: 'カスタムクエリ',
  label_total: '合計',
  field_activity: '作業分類',
  field_comments: 'コメント',
  field_estimated_hours: '予定工数',
  field_hours: '時間',
  field_status: 'ステータス',
};
/* eslint-enable */

describe('Root', () => {
  jest.setTimeout(10000);

  const waitUntilLoaded = async (): Promise<void> => {
    if (screen.queryByText(/ロード中/)) {
      await waitForElementToBeRemoved(() => screen.queryByText(/ロード中/));
    }
  };

  const setupAttachmentApiMock = () => {
    return {
      createAttachment: jest.fn(),
      deleteAttachment: jest.fn(),
      getAttachment: jest.fn(),
    };
  };

  const setupWorkMonthProviderMock = (
    workMonth?: WorkMonth,
    dateInMonth = 1
  ) => {
    const currentMonth = workMonth || buildWorkMonth();
    return {
      getCurrent: jest.fn().mockReturnValue(currentMonth),
      getToday: jest
        .fn()
        .mockReturnValue(
          `${currentMonth.month}-${String(dateInMonth).padStart(2, '0')}`
        ),
      getNextMonth: jest.fn(),
      getPreviousMonth: jest.fn(),
    };
  };

  const setupTimeEntryDataStoreMock = ({
    customFields,
    activities,
    indirectActivities,
    issues,
    projects,
    timeEntries,
    assignedIssueIds,
  }: {
    customFields?: CustomField[];
    activities?: Activity[];
    indirectActivities?: Activity[];
    issues?: Issue[];
    projects?: Project[];
    timeEntries?: TimeEntry[];
    assignedIssueIds?: number[];
  }) => {
    return {
      getForMonth: jest.fn().mockResolvedValue({
        activities: activities || [],
        indirectActivities: indirectActivities || [],
        issues: issues || [],
        projects: projects || [],
        timeEntries: timeEntries || [],
        assignedIssueIds: assignedIssueIds || [],
      }),
      createTimeEntry: jest.fn(async function (entry, _userId) {
        return {
          ...entry,
          id: `timeEntry-${generateId()}`,
          hours: parseFloat(entry.hours),
        } as TimeEntry;
      }),
      updateTimeEntry: jest.fn(),
      deleteTimeEntry: jest.fn().mockResolvedValue(null),
      getCustomFieldsForProject: jest
        .fn()
        .mockResolvedValue(customFields || []),
      getProjectFilters: jest.fn(),
      getFilterValues: jest.fn(),
      getProjects: jest.fn(),
      getAvailableFilters: jest.fn().mockResolvedValueOnce({
        groupedFilterOptions: [],
        availableFilters: {},
        operatorsByFilterType: {},
        operatorsLabels: {},
        queries: [],
      }),
      getFilteredIssues: jest.fn().mockResolvedValueOnce({
        queriedFilters: [],
        issues: [],
        ancestorIssues: [],
        projects: [],
        trackers: [],
      }),
      getProjectActivities: jest.fn().mockResolvedValueOnce({
        activities: [],
      }),
      getInheritProjects: jest.fn().mockResolvedValueOnce({
        projects: [],
      }),
    };
  };

  const renderWith = async ({
    user,
    timeEntryRequiredFields,
    requiredCustomFieldExists,
    indirectTimeEntryPermissions,
    workMonthProvider,
    timeEntryDataStore,
    attachmentApi,
  }: {
    user?: CurrentUser;
    timeEntryRequiredFields?: string[];
    requiredCustomFieldExists?: boolean;
    indirectTimeEntryPermissions?: Permission[];
    workMonthProvider: jest.Mocked<WorkMonthProvider>;
    timeEntryDataStore: jest.Mocked<TimeEntryDataStore>;
    attachmentApi: jest.Mocked<AttachmentApiHandler>;
  }) => {
    const utils = render(
      <RecoilRoot>
        <Root
          currentUser={user || buildUser()}
          targetUsers={[]}
          timeEntryDataStore={timeEntryDataStore}
          workMonthProvider={workMonthProvider}
          labels={labels}
          timeEntryRequiredFields={timeEntryRequiredFields ?? []}
          requiredCustomFieldExists={requiredCustomFieldExists || false}
          indirectTimeEntryPermissions={
            indirectTimeEntryPermissions || ['edit', 'create']
          }
          attachmentApi={attachmentApi}
        />
      </RecoilRoot>
    );

    await waitUntilLoaded();

    return utils;
  }

  const [getHeader] = buildElementQueries('table', { name: 'ヘッダー' });

  const getAddTaskButton = (): HTMLElement =>
    within(getHeader()).getByRole('button', {
      name: 'タスク追加',
    });

  const [getSelectIssuesDialog, querySelectIssuesDialog] = buildElementQueries(
    'dialog',
    { name: 'チケット選択' }
  );

  const getProjectAndIssueRowHeadersArea = (): HTMLElement =>
    screen.getByRole('table', {
      name: 'プロジェクトとチケット',
    });

  const getProjectAndIssueRowGroups = (): HTMLElement[] =>
    within(getProjectAndIssueRowHeadersArea()).getAllByRole('rowgroup');

  const getRowHeaderGroup = (name: string): HTMLElement =>
    within(getProjectAndIssueRowHeadersArea()).getByRole('rowgroup', { name });

  const getIndirectActivityRowGroup = (): HTMLElement =>
    screen.getByRole('rowgroup', {
      name: '間接作業',
    });

  const getActivityRowHeadersIn = (rowGroupName: string): HTMLElement[] => {
    let rowGroup: HTMLElement;
    if (rowGroupName === '間接作業') {
      rowGroup = getIndirectActivityRowGroup();
    } else {
      rowGroup = getRowHeaderGroup(rowGroupName);
    }
    return within(rowGroup).queryAllByRole('row');
  };

  const getDateHeaders = (): HTMLElement[] => {
    const dateHeaderRow = screen.getByRole('row', { name: '日付' });
    return within(dateHeaderRow).getAllByRole('columnheader');
  };

  const getMonthHeaders = (): HTMLElement[] => {
    const monthHeaderRow = screen.getByRole('row', { name: '月' });
    return within(monthHeaderRow).getAllByRole('columnheader');
  };

  const getTimeEntryRows = (): HTMLElement[] => {
    const timeEntriesArea = screen.getByRole('rowgroup', {
      name: '工数',
    });
    return within(timeEntriesArea).queryAllByRole('row');
  };

  const findAncestorWithLabel = (
    element: HTMLElement,
    label: string
  ): HTMLElement | null => {
    let result = element.parentElement;
    while (
      result &&
      result.attributes.getNamedItem('aria-label')?.value !== label
    ) {
      result = result.parentElement;
    }
    return result;
  };

  const getRowHeaderRows = (): HTMLElement[] =>
    within(getProjectAndIssueRowHeadersArea()).getAllByRole('row');

  const getRowIndex = (rowName: string, rowgroupName?: string): number => {
    return getRowHeaderRows().findIndex(
      (row) =>
        row.textContent?.includes(rowName) &&
        (!rowgroupName || findAncestorWithLabel(row, rowgroupName))
    );
  };

  const getTimeEntryCellsFor = (
    rowName: string,
    opt?: { inRowgroup: string }
  ): HTMLElement[] => {
    const rowIndex = getRowIndex(rowName, opt?.inRowgroup);
    if (rowIndex === -1) {
      throw `No rows found with name ${rowName}`;
    }

    const row = getTimeEntryRows()[rowIndex];
    return within(row).getAllByRole('cell');
  };

  const getRowSumCellFor = (
    rowName: string,
    opt?: { inRowgroup: string }
  ): HTMLElement => {
    const rowIndex = getRowIndex(rowName, opt?.inRowgroup);
    if (rowIndex === -1) {
      throw `No rows found with name ${rowName}`;
    }
    const rowHeader = getRowHeaderRows()[rowIndex];
    const cells = within(rowHeader).getAllByRole('rowheader');
    const lastCellInRowHeader = cells[cells.length - 1];
    return lastCellInRowHeader;
  };

  const dateIndex = (date: string): number => {
    const dateHeaders = getDateHeaders();
    const dayString = parseInt(date.split('-')[2]).toString(); // Remove leading zero;
    return dateHeaders.findIndex((header) =>
      header.textContent?.includes(dayString)
    );
  };

  const getDateSumCells = (): HTMLElement[] => {
    const dateSumRow = screen.getByRole('row', { name: '日付の工数' });
    return within(dateSumRow).getAllByRole('columnheader');
  };

  const getDateSumCellFor = (date: string): HTMLElement =>
    getDateSumCells()[dateIndex(date)];

  const [getTotalSumCell] = buildElementQueries('cell', { name: '合計' });

  const getTimeEntryCell = (rowName: string, date: string): HTMLElement =>
    getTimeEntryCellsFor(rowName)[dateIndex(date)];

  const [getEditModal, queryEditModal] = buildElementQueries('tooltip', {
    name: '工数の編集',
  });

  const waitForModal = async (): Promise<void> =>
    waitFor(() => {
      getEditModal();
    });

  const clickSomewhere = async (user: UserEvent): Promise<void> => {
    await user.click(getTotalSumCell());
  };

  const openSelectIssuesModal = async (user: UserEvent): Promise<void> => {
    await user.click(getAddTaskButton());
    await waitFor(() => getSelectIssuesDialog());
    await waitUntilLoaded();
  };

  const clickPlusButton = async (
    user: UserEvent,
    rowgroupName: string
  ): Promise<void> => {
    const rowgroup = getRowHeaderGroup(rowgroupName);
    const plusButton = within(rowgroup).getByRole('button', {
      name: '作業分類を追加',
      hidden: true,
    });
    await user.click(plusButton);
  };

  const getActivityChoices = (): HTMLElement[] => {
    const activityChoiceMenu = getMenuWithName('作業分類の選択肢');
    return within(activityChoiceMenu).getAllByRole('menuitem');
  };

  const LIGHT_YELLOW = '#fcfce8';
  const GREY = '#f3f5f6';
  const WHITE = 'white';

  describe('Top Nav', () => {
    it('displays the current user as heading', async () => {
      const timeEntryDataStore = setupTimeEntryDataStoreMock({});
      await renderWith({
        user: { id: 1, name: '水口　崇', locale: 'ja' },
        workMonthProvider: setupWorkMonthProviderMock(),
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      expect(
        screen.getByRole('heading', { name: '水口　崇' })
      ).toBeInTheDocument();
    });

    describe('Month Selection', () => {
      const [getMonthSelection] = buildElementQueries('navigation', {
        name: '月の選択',
      });

      const getNextMonthButton = (): HTMLElement =>
        within(getMonthSelection()).getByRole('button', {
          name: '次',
        });

      const getPreviousMonthButton = (): HTMLElement =>
        within(getMonthSelection()).getByRole('button', {
          name: '前',
        });

      it('shows the current month', async () => {
        const workMonth = buildWorkMonth({ month: '2021-02' });
        const workMonthProvider = setupWorkMonthProviderMock(workMonth);
        const timeEntryDataStore = setupTimeEntryDataStoreMock({});
        await renderWith({
          workMonthProvider,
          timeEntryDataStore,
          attachmentApi: setupAttachmentApiMock(),
        });

        expect(getMonthSelection()).toHaveTextContent(
          '2021/02/01 〜 2021/02/28'
        );
      });

      it('shows date range which is specified by dates', async () => {
        const workMonth = buildWorkMonth({
          month: '2021-02',
          days: [27, 28, 1, 2],
        });
        const workMonthProvider = setupWorkMonthProviderMock(workMonth);
        const timeEntryDataStore = setupTimeEntryDataStoreMock({});
        await renderWith({
          workMonthProvider,
          timeEntryDataStore,
          attachmentApi: setupAttachmentApiMock(),
        });

        expect(getMonthSelection()).toHaveTextContent(
          '2021/02/27 〜 2021/03/02'
        );
      });

      it('can change months', async () => {
        const user = userEvent.setup();
        const workMonth = buildWorkMonth({ month: '2021-02' });
        const workMonthProvider = setupWorkMonthProviderMock(workMonth);
        const timeEntryDataStore = setupTimeEntryDataStoreMock({});
        await renderWith({
          workMonthProvider,
          timeEntryDataStore,
          attachmentApi: setupAttachmentApiMock(),
        });

        const nextMonth = buildWorkMonth({ month: '2021-03' });
        workMonthProvider.getNextMonth.mockReturnValueOnce(nextMonth);
        await user.click(getNextMonthButton());
        await waitUntilLoaded();

        expect(workMonthProvider.getNextMonth).toHaveBeenCalledWith(workMonth);
        expect(getMonthSelection()).toHaveTextContent(
          '2021/03/01 〜 2021/03/31'
        );

        workMonthProvider.getPreviousMonth.mockReturnValueOnce(workMonth);
        await user.click(getPreviousMonthButton());
        await waitUntilLoaded();

        expect(workMonthProvider.getPreviousMonth).toHaveBeenCalledWith(
          nextMonth
        );
        expect(getMonthSelection()).toHaveTextContent(
          '2021/02/01 〜 2021/02/28'
        );
      });

      it('Bugfix: Switch from month with data to an empty month', async () => {
        const user = userEvent.setup();
        const workMonth = buildWorkMonth({ month: '2021-02' });
        const workMonthProvider = setupWorkMonthProviderMock(workMonth);
        const activities = [buildActivity({ id: 1 })];
        const projects = [buildProject({ id: 1, activityIds: [1] })];
        const issues = [buildIssue({ id: 1, projectId: 1 })];
        const timeEntries = [
          buildIssueTimeEntry({
            activity: { issueId: 1, activityId: 1 },
            date: '2021-02-01',
          }),
        ];
        const timeEntryDataStore = setupTimeEntryDataStoreMock({
          activities,
          projects,
          issues,
          timeEntries,
        });
        await renderWith({
          workMonthProvider,
          timeEntryDataStore,
          attachmentApi: setupAttachmentApiMock(),
        });

        const nextMonth = buildWorkMonth({ month: '2021-03' });
        workMonthProvider.getNextMonth.mockReturnValueOnce(nextMonth);
        timeEntryDataStore.getForMonth.mockResolvedValueOnce({
          activities,
          indirectActivities: [],
          issues: [],
          projects: [],
          timeEntries: [],
          assignedIssueIds: [],
        });

        await user.click(getNextMonthButton());
        await waitUntilLoaded();

        expect(getMonthSelection()).toHaveTextContent(
          '2021/03/01 〜 2021/03/31'
        );
      });

      it('the initial focus cell should move to a predetermined location when the month is changed', async () => {
        const user = userEvent.setup();
        const workMonth = buildWorkMonth({
          month: '2021-02',
          days: [14],
        });
        const workMonthProvider = setupWorkMonthProviderMock(workMonth, 14);
        const activities = [
          buildActivity({ id: 1, name: 'Activity1', position: 1 }),
        ];
        const issues = [buildIssue({ id: 1, projectId: 1, subject: 'Issue' })];
        const projects = [
          buildProject({ id: 1, name: 'Project', activityIds: [1] }),
        ];
        const previousMonthTimeEntries = [
          buildIssueTimeEntry({
            activity: { issueId: 1, activityId: 1 },
            date: '2021-01-01',
            hours: 1,
          }),
        ];
        const currentMonthTimeEntries = [
          buildIssueTimeEntry({
            activity: { issueId: 1, activityId: 1 },
            date: '2021-02-14',
            hours: 2,
          }),
        ];
        const nextMonthTimeEntries = [
          buildIssueTimeEntry({
            activity: { issueId: 1, activityId: 1 },
            date: '2021-03-01',
            hours: 3,
          }),
        ];
        const timeEntryDataStore = setupTimeEntryDataStoreMock({
          activities,
          issues,
          projects,
          timeEntries: currentMonthTimeEntries,
        });
        await renderWith({
          workMonthProvider,
          timeEntryDataStore,
          attachmentApi: setupAttachmentApiMock(),
        });

        expectFocusCell(getTimeEntryCell('Activity1', '2021-02-14'));

        const changeTimeEntries = (timeEntries: IssueTimeEntry[]) => {
          timeEntryDataStore.getForMonth.mockResolvedValueOnce({
            activities: activities,
            indirectActivities: [],
            issues: issues,
            projects: projects,
            timeEntries,
            assignedIssueIds: [],
          });
        };
        const previousMonth = buildWorkMonth({ month: '2021-01', days: [1] });
        const currentMonth = workMonth;
        const nextMonth = buildWorkMonth({ month: '2021-03', days: [1] });

        // change to next month
        changeTimeEntries(nextMonthTimeEntries);
        workMonthProvider.getNextMonth.mockReturnValueOnce(nextMonth);
        await user.click(getNextMonthButton());
        await waitUntilLoaded();

        expect(workMonthProvider.getNextMonth).toHaveBeenCalledWith(workMonth);
        await waitFor(() => {
          expectFocusCell(getTimeEntryCell('Activity1', '2021-03-01'));
        });

        // return to current month
        changeTimeEntries(currentMonthTimeEntries);
        workMonthProvider.getPreviousMonth.mockReturnValueOnce(currentMonth);
        await user.click(getPreviousMonthButton());
        await waitUntilLoaded();

        expect(workMonthProvider.getPreviousMonth).toHaveBeenCalledWith(
          nextMonth
        );
        await waitFor(() => {
          expectFocusCell(getTimeEntryCell('Activity1', '2021-02-14'));
        });

        // change to previous month
        changeTimeEntries(previousMonthTimeEntries);
        workMonthProvider.getPreviousMonth.mockReturnValueOnce(previousMonth);
        await user.click(getPreviousMonthButton());
        await waitUntilLoaded();

        expect(workMonthProvider.getPreviousMonth).toHaveBeenCalledWith(
          currentMonth
        );
        await waitFor(() => {
          expectFocusCell(getTimeEntryCell('Activity1', '2021-01-01'));
        });

        // return to current month
        changeTimeEntries(currentMonthTimeEntries);
        workMonthProvider.getNextMonth.mockReturnValueOnce(currentMonth);
        await user.click(getNextMonthButton());
        await waitUntilLoaded();

        expect(workMonthProvider.getNextMonth).toHaveBeenCalledWith(
          previousMonth
        );
        await waitFor(() => {
          expectFocusCell(getTimeEntryCell('Activity1', '2021-02-14'));
        });
      });
    });
  });

  describe('Top Left Header', () => {
    it('displays the column headers for issue and activity area', async () => {
      const timeEntryDataStore = setupTimeEntryDataStoreMock({});
      await renderWith({
        workMonthProvider: setupWorkMonthProviderMock(),
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });
      const header = getHeader();

      const headerRow = within(header).getByRole('row', {
        name: 'チケットと作業のヘッダー',
      });
      const headerCells = within(headerRow).getAllByRole('columnheader');

      await expectEmptyText(headerCells[0]);
      expect(headerCells[1]).toHaveTextContent('ステータス');
      expect(headerCells[2]).toHaveTextContent('予定工数');
      expect(headerCells[3]).toHaveTextContent('作業分類');

      expect(within(header).getByText('合計')).toBeInTheDocument();
    });

    it('displays the "タスク追加" button and shows the modal when clicking it', async () => {
      const user = userEvent.setup();
      const timeEntryDataStore = setupTimeEntryDataStoreMock({});
      await renderWith({
        workMonthProvider: setupWorkMonthProviderMock(),
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });
      await openSelectIssuesModal(user);

      expect(getSelectIssuesDialog()).toBeInTheDocument();
    });
  });

  describe('Select Issue modal', () => {
    const setup = async () => {
      const user = userEvent.setup();
      await renderWith({
        workMonthProvider: setupWorkMonthProviderMock(),
        timeEntryDataStore: setupTimeEntryDataStoreMock({}),
        attachmentApi: setupAttachmentApiMock(),
      });
      await openSelectIssuesModal(user);
      return { user };
    };

    it('closes the modal when selecting issues', async () => {
      const { user } = await setup();
      const selectButton = within(getSelectIssuesDialog()).getByRole('button', {
        name: '選択したチケットを配置',
      });
      await user.click(selectButton);

      expect(querySelectIssuesDialog()).not.toBeInTheDocument();
    });

    it('closes the modal when canceling selection', async () => {
      const { user } = await setup();
      const selectButton = within(getSelectIssuesDialog()).getByRole('link', {
        name: 'キャンセル',
      });
      await user.click(selectButton);

      expect(querySelectIssuesDialog()).not.toBeInTheDocument();
    });

    it('closes the modal when outside of the modal is clicked', async () => {
      const { user } = await setup();
      const overlay = document.querySelector(
        '.ReactModal__Overlay'
      ) as HTMLElement;
      await user.click(overlay);

      expect(querySelectIssuesDialog()).not.toBeInTheDocument();
    });
  });

  describe('Date Header', () => {
    const setup = async () => {
      const workMonth: WorkMonth = {
        month: '2021-01',
        dates: [
          {
            date: '2021-01-01',
            type: 'holiday',
            past: true,
          },
          { date: '2021-01-02', type: 'saturday', past: true },
          { date: '2021-01-03', type: 'holiday', past: true },
          { date: '2021-01-04', type: 'workDay', past: true },
          { date: '2021-01-05', type: 'today' },
          { date: '2021-01-06', type: 'workDay' },
        ],
      };
      await renderWith({
        workMonthProvider: setupWorkMonthProviderMock(workMonth),
        timeEntryDataStore: setupTimeEntryDataStoreMock({}),
        attachmentApi: setupAttachmentApiMock(),
      });
    };

    it('displays the current month dates', async () => {
      await setup();
      expectTextContents(getDateHeaders(), [/1/, /2/, /3/, /4/, /5/, /6/]);
    });

    it('displays the current month date weekdays', async () => {
      await setup();
      expectTextContents(getDateHeaders(), [
        /金/,
        /土/,
        /日/,
        /月/,
        /火/,
        /水/,
      ]);
    });
  });

  describe('Month Header', () => {
    const setup = async (dates: WorkDate[]) => {
      const workMonth: WorkMonth = {
        month: '2021-01',
        dates,
      };
      await renderWith({
        workMonthProvider: setupWorkMonthProviderMock(workMonth),
        timeEntryDataStore: setupTimeEntryDataStoreMock({}),
        attachmentApi: setupAttachmentApiMock(),
      });
    };

    it('displays the current month header', async () => {
      await setup([
        {
          date: '2021-01-01',
          type: 'holiday',
          past: true,
        },
      ]);
      expectTextContents(getMonthHeaders(), [/2021\/01/]);
    });

    it('displays multile month headers', async () => {
      await setup([
        {
          date: '2020-12-31',
          type: 'holiday',
          past: true,
        },
        {
          date: '2021-01-01',
          type: 'holiday',
          past: true,
        },
      ]);
      expectTextContents(getMonthHeaders(), [/2020\/12/, /2021\/01/]);
    });
  });

  it('displays the projects, issues, and activities of the entered time entries', async () => {
    const activities = [
      buildActivity({
        id: 1,
        name: 'Development',
        position: 2,
      }),
      buildActivity({
        id: 2,
        name: 'Test',
        position: 3,
      }),
      buildActivity({
        id: 3,
        name: 'Design',
        position: 1,
      }),
    ];
    const indirectActivities = [
      buildActivity({
        id: 1,
        name: 'Check Mails',
        position: 2,
      }),
      buildActivity({
        id: 2,
        name: 'Meetings',
        position: 3,
      }),
      buildActivity({
        id: 3,
        name: 'Paperworks',
        position: 1,
      }),
    ];
    const projects = [
      buildProject({ id: 1, name: 'Lab', activityIds: [1, 2] }),
      buildProject({ id: 2, name: 'Outside Work', activityIds: [1, 2] }),
    ];
    const issues = [
      buildIssue({
        id: 1,
        projectId: 1,
        subject: 'New Feature FG22',
        status: '進行中',
        estimatedHours: 4,
      }),
      buildIssue({
        id: 2,
        projectId: 2,
        subject: 'Critical Bug',
        status: '未着手',
        estimatedHours: 2,
      }),
    ];
    const timeEntries = [
      buildIssueTimeEntry({
        activity: { issueId: 1, activityId: 1 },
      }),
      buildIssueTimeEntry({
        activity: { issueId: 1, activityId: 2 },
      }),
      buildIssueTimeEntry({
        activity: { issueId: 1, activityId: 3 },
      }),
      buildIssueTimeEntry({
        activity: { issueId: 2, activityId: 2 },
      }),
      buildIssueTimeEntry({
        activity: { issueId: 2, activityId: 3 },
      }),
      buildProjectTimeEntry({
        activity: { projectId: 1, activityId: 1 },
      }),
      buildProjectTimeEntry({
        activity: { projectId: 1, activityId: 2 },
      }),
      buildIndirectTimeEntry({
        activity: { indirectActivityId: 1 },
      }),
      buildIndirectTimeEntry({
        activity: { indirectActivityId: 2 },
      }),
      buildIndirectTimeEntry({
        activity: { indirectActivityId: 3 },
      }),
    ];
    const timeEntryDataStore = setupTimeEntryDataStoreMock({
      activities,
      indirectActivities,
      issues,
      projects,
      timeEntries,
    });
    await renderWith({
      workMonthProvider: setupWorkMonthProviderMock(),
      timeEntryDataStore,
      attachmentApi: setupAttachmentApiMock(),
    });

    expectTextContents(getProjectAndIssueRowGroups(), [
      /Lab/,
      /プロジェクト工数/,
      /#1 New Feature FG22/,
      /Outside Work/,
      /#2 Critical Bug/,
      /間接作業/,
    ]);

    expectTextContents(getActivityRowHeadersIn('Labのプロジェクト工数'), [
      /Development/,
      /Test/,
    ]);

    expectTextContents(getActivityRowHeadersIn('New Feature FG22'), [
      /Design/,
      /Development/,
      /Test/,
    ]);

    expectTextContents(getActivityRowHeadersIn('Critical Bug'), [
      /Design/,
      /Test/,
    ]);

    expectTextContents(getActivityRowHeadersIn('間接作業'), [
      /Paperworks/,
      /Check Mails/,
      /Meetings/,
    ]);
  });

  it('displays the assigned issues and their projects and activities', async () => {
    const activities = [
      buildActivity({
        id: 1,
        name: 'Design',
      }),
      buildActivity({
        id: 2,
        name: 'Development',
      }),
    ];
    const indirectActivities = [
      buildActivity({
        id: 1,
        name: 'Check Mails',
      }),
    ];
    const projects = [
      buildProject({ id: 1, name: 'Lab', activityIds: [1, 2] }),
      buildProject({
        id: 2,
        name: 'Only Project Time Entry',
        activityIds: [1, 2],
      }),
    ];
    const issues = [
      buildIssue({
        id: 1,
        projectId: 1,
        subject: 'Assigned Issue',
        status: '進行中',
        estimatedHours: 3,
      }),
    ];
    const timeEntries = [
      buildProjectTimeEntry({
        activity: { projectId: 2, activityId: 1 },
      }),
    ];
    const assignedIssueIds = [1];
    const timeEntryDataStore = setupTimeEntryDataStoreMock({
      activities,
      indirectActivities,
      projects,
      issues,
      timeEntries,
      assignedIssueIds,
    });
    await renderWith({
      workMonthProvider: setupWorkMonthProviderMock(),
      timeEntryDataStore,
      attachmentApi: setupAttachmentApiMock(),
    });

    expectTextContents(getProjectAndIssueRowGroups(), [
      /Only Project Time Entry/,
      /プロジェクト工数/,
      /Lab/,
      /#1 Assigned Issue/,
      /間接作業/,
    ]);

    expectTextContents(
      getActivityRowHeadersIn('Only Project Time Entryのプロジェクト工数'),
      [/Design/]
    );

    // assigned issues don't have ActivityRowHeader
    expect(getActivityRowHeadersIn('Assigned Issue')).toHaveLength(0);
  });

  it('displays activity when project has only one activity', async () => {
    const activities = [
      buildActivity({
        id: 1,
        name: 'Design',
      }),
      buildActivity({
        id: 2,
        name: 'Development',
      }),
      buildActivity({
        id: 3,
        name: 'Design',
      }),
    ];

    const projects = [
      buildProject({ id: 1, name: 'Project 1', activityIds: [1, 2] }),
      buildProject({
        id: 2,
        name: 'Project 2',
        activityIds: [3],
      }),
    ];
    const issues = [
      buildIssue({
        id: 1,
        projectId: 1,
        subject: 'Assigned Issue 1',
        status: '進行中',
        estimatedHours: 3,
      }),
      buildIssue({
        id: 2,
        projectId: 2,
        subject: 'Assigned Issue 2',
        status: '進行中',
        estimatedHours: 3,
      }),
    ];

    const assignedIssueIds = [1, 2];
    const timeEntryDataStore = setupTimeEntryDataStoreMock({
      activities,
      indirectActivities: [],
      projects,
      issues,
      timeEntries: [],
      assignedIssueIds,
    });

    await act(async () => {
      renderWith({
        workMonthProvider: setupWorkMonthProviderMock(),
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });
    })

    expectTextContents(getProjectAndIssueRowGroups(), [
      /Project 1/,
      /#1 Assigned Issue 1/,
      /Project 2/,
      /#2 Assigned Issue 2/,
      /間接作業/,
    ]);

    expect(getActivityRowHeadersIn('Assigned Issue 1')).toHaveLength(0);
    expectTextContents(getActivityRowHeadersIn('Assigned Issue 2'), [/Design/]);
  });

  it('displays the correct time entry hours', async () => {
    const dates: WorkDate[] = [
      { date: '2021-01-04', type: 'workDay', past: true },
      { date: '2021-01-05', type: 'today' },
      { date: '2021-01-06', type: 'workDay' },
    ];
    const workMonth = { month: '2021-01', dates };
    const workMonthProvider = setupWorkMonthProviderMock(workMonth);
    const activities = [
      buildActivity({ id: 1, name: 'Activity 1' }),
      buildActivity({ id: 2, name: 'Activity 2' }),
    ];
    const indirectActivities = [
      buildActivity({ id: 1, name: 'Indirect Activity 1' }),
      buildActivity({ id: 2, name: 'Indirect Activity 2' }),
    ];
    const projects = [
      buildProject({ id: 1, name: 'Project A', activityIds: [1, 2] }),
      buildProject({ id: 2, name: 'Project B', activityIds: [1, 2] }),
    ];
    const issues = [
      buildIssue({ id: 1, projectId: 1, subject: 'Pr A Issue 1' }),
      buildIssue({ id: 2, projectId: 1, subject: 'Pr A Issue 2' }),
      buildIssue({ id: 3, projectId: 2, subject: 'Pr B Issue 1' }),
    ];
    const timeEntries = [
      buildIssueTimeEntry({
        activity: { issueId: 1, activityId: 1 },
        date: dates[0].date,
        hours: 3,
      }),
      buildIssueTimeEntry({
        activity: { issueId: 1, activityId: 1 },
        date: dates[2].date,
        hours: 4,
      }),
      buildIssueTimeEntry({
        activity: { issueId: 1, activityId: 2 },
        date: dates[0].date,
        hours: 4,
      }),
      buildIssueTimeEntry({
        activity: { issueId: 1, activityId: 2 },
        date: dates[1].date,
        hours: 2,
      }),
      buildIssueTimeEntry({
        activity: { issueId: 2, activityId: 1 },
        date: dates[0].date,
        hours: 7,
      }),
      buildIssueTimeEntry({
        activity: { issueId: 3, activityId: 2 },
        date: dates[0].date,
        hours: 5,
      }),
      buildIssueTimeEntry({
        activity: { issueId: 3, activityId: 2 },
        date: dates[1].date,
        hours: 5,
      }),
      buildProjectTimeEntry({
        activity: { projectId: 2, activityId: 1 },
        date: dates[2].date,
        hours: 1,
      }),
      buildIndirectTimeEntry({
        activity: { indirectActivityId: 1 },
        date: dates[0].date,
        hours: 2,
      }),
      buildIndirectTimeEntry({
        activity: { indirectActivityId: 1 },
        date: dates[1].date,
        hours: 5,
      }),
      buildIndirectTimeEntry({
        activity: { indirectActivityId: 2 },
        date: dates[0].date,
        hours: 3,
      }),
      buildIndirectTimeEntry({
        activity: { indirectActivityId: 2 },
        date: dates[1].date,
        hours: 8,
      }),
    ];
    const timeEntryDataStore = setupTimeEntryDataStoreMock({
      activities,
      indirectActivities,
      projects,
      issues,
      timeEntries,
    });
    await renderWith({
      workMonthProvider,
      timeEntryDataStore,
      attachmentApi: setupAttachmentApiMock(),
    });

    expectTextContents(getTimeEntryCellsFor('Project A'), [
      '14.00',
      '2.00',
      '4.00',
    ]);
    expectTextContents(
      getTimeEntryCellsFor('Activity 1', { inRowgroup: 'Pr A Issue 1' }),
      ['3.00', '', '4.00']
    );
    expectTextContents(
      getTimeEntryCellsFor('Activity 2', { inRowgroup: 'Pr A Issue 1' }),
      ['4.00', '2.00', '']
    );
    expectTextContents(
      getTimeEntryCellsFor('Activity 1', { inRowgroup: 'Pr A Issue 2' }),
      ['7.00', '', '']
    );

    expectTextContents(getTimeEntryCellsFor('Project B'), [
      '5.00',
      '5.00',
      '1.00',
    ]);
    expectTextContents(
      getTimeEntryCellsFor('Activity 1', {
        inRowgroup: 'Project Bのプロジェクト工数',
      }),
      ['', '', '1.00']
    );
    expectTextContents(
      getTimeEntryCellsFor('Activity 2', { inRowgroup: 'Pr B Issue 1' }),
      ['5.00', '5.00', '']
    );

    expectTextContents(
      getTimeEntryCellsFor('Indirect Activity 1', { inRowgroup: '間接作業' }),
      ['2.00', '5.00', '']
    );
    expectTextContents(
      getTimeEntryCellsFor('Indirect Activity 2', { inRowgroup: '間接作業' }),
      ['3.00', '8.00', '']
    );

    expect(getRowSumCellFor('Project A')).toHaveTextContent('20.00');
    expect(
      getRowSumCellFor('Activity 1', { inRowgroup: 'Pr A Issue 1' })
    ).toHaveTextContent('7.00');
    expect(
      getRowSumCellFor('Activity 2', { inRowgroup: 'Pr A Issue 1' })
    ).toHaveTextContent('6.00');
    expect(
      getRowSumCellFor('Activity 1', { inRowgroup: 'Pr A Issue 2' })
    ).toHaveTextContent('7.00');

    expect(getRowSumCellFor('Project B')).toHaveTextContent('11.00');
    expect(
      getRowSumCellFor('Activity 1', {
        inRowgroup: 'Project Bのプロジェクト工数',
      })
    ).toHaveTextContent('1.00');
    expect(
      getRowSumCellFor('Activity 2', { inRowgroup: 'Pr B Issue 1' })
    ).toHaveTextContent('10.00');

    expect(
      getRowSumCellFor('Indirect Activity 1', { inRowgroup: '間接作業' })
    ).toHaveTextContent('7.00');
    expect(
      getRowSumCellFor('Indirect Activity 2', { inRowgroup: '間接作業' })
    ).toHaveTextContent('11.00');

    expectTextContents(getDateSumCells(), ['24.00', '20.00', '5.00']);

    expect(getTotalSumCell()).toHaveTextContent('49.00');
  });

  it('displays the correct colors for the time entry cells', async () => {
    const workMonth: WorkMonth = {
      month: '2021-01',
      dates: [
        {
          date: '2021-01-01',
          type: 'holiday',
          past: true,
        },
        { date: '2021-01-02', type: 'saturday', past: true },
        { date: '2021-01-03', type: 'holiday', past: true },
        { date: '2021-01-04', type: 'workDay', past: true },
        { date: '2021-01-05', type: 'today' },
        { date: '2021-01-06', type: 'workDay' },
      ],
    };
    const workMonthProvider = setupWorkMonthProviderMock(workMonth);
    const activities = [buildActivity({ id: 1 })];
    const issues = [buildIssue({ id: 1, projectId: 1 })];
    const projects = [buildProject({ id: 1, activityIds: [1] })];
    const timeEntries = [
      buildIssueTimeEntry({
        activity: { issueId: 1, activityId: 1 },
        date: '2021-01-01',
        hours: 3,
      }),
    ];
    const timeEntryDataStore = setupTimeEntryDataStoreMock({
      activities,
      issues,
      projects,
      timeEntries,
    });
    await renderWith({
      workMonthProvider,
      timeEntryDataStore,
      attachmentApi: setupAttachmentApiMock(),
    });

    const rows = getTimeEntryRows();
    const VERY_LIGHT_BLUE = '#e7f0fa';

    // Project
    expect(rows[0]).toHaveStyle(`background-color: ${VERY_LIGHT_BLUE}`);

    // Issue Activities
    expectBackgroundColors(within(rows[1]).getAllByRole('cell'), [
      GREY,
      GREY,
      GREY,
      WHITE,
      LIGHT_YELLOW,
      WHITE,
    ]);
  });

  describe('Creating, editing, deleting single time entries via click', () => {
    const clickAndType = async (
      user: UserEvent,
      cell: HTMLElement,
      {
        inputValue,
        expectedFieldValue,
      }: {
        inputValue: string;
        expectedFieldValue?: string;
      }
    ): Promise<void> => {
      await user.click(cell);
      const editField = within(cell).getByRole('textbox');
      await waitFor(() => {
        expect(editField).toHaveFocus();
      });
      if (expectedFieldValue !== undefined) {
        expect(editField).toHaveValue(expectedFieldValue);
      }

      await user.type(editField, `${inputValue}`, { skipClick: true });
      await waitUntilLoaded();
    };

    const expectEditField = (cell: HTMLElement): void => {
      expect(within(cell).getByRole('textbox')).toBeInTheDocument();
    };

    const expectEditFieldToDisappear = (cell: HTMLElement): void => {
      expect(within(cell).queryByRole('textbox')).not.toBeInTheDocument();
    };

    const lastCreatedEntry = async (
      timeEntryDataStore: jest.Mocked<TimeEntryDataStore>
    ): Promise<TimeEntry> => {
      const createResults = timeEntryDataStore.createTimeEntry.mock.results;

      return createResults[createResults.length - 1].value;
    };

    it('for issue time entry', async () => {
      const user = userEvent.setup({ skipHover: true });
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const activities = [
        buildActivity({ id: 1, name: 'Activity1', position: 2 }),
        buildActivity({ id: 2, name: 'Activity2', position: 1 }),
      ];
      const issues = [buildIssue({ id: 1, projectId: 1, subject: 'Issue' })];
      const projects = [
        buildProject({ id: 1, name: 'Project', activityIds: [1, 2] }),
      ];
      const timeEntries = [
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 2,
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        issues,
        projects,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const emptyCell = getTimeEntryCell('Activity1', '2021-01-05');
      await clickAndType(user, emptyCell, {
        inputValue: '4{enter}',
        expectedFieldValue: '',
      });

      expectEditFieldToDisappear(emptyCell);
      expect(timeEntryDataStore.createTimeEntry).toHaveBeenCalledWith(
        {
          date: '2021-01-05',
          hours: '4',
          activityType: 'issue',
          activity: { issueId: 1, activityId: 1 },
        },
        1
      );
      await waitFor(() => {
        expect(emptyCell).toHaveTextContent('4.00');
      });
      expect(getRowSumCellFor('Project')).toHaveTextContent('6.00');
      expect(getRowSumCellFor('Activity1')).toHaveTextContent('6.00');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('4.00');
      expect(getTotalSumCell()).toHaveTextContent('6.00');

      const createdEntry = await lastCreatedEntry(timeEntryDataStore);
      timeEntryDataStore.updateTimeEntry.mockResolvedValue({
        ...createdEntry,
        hours: 6,
      });
      await clickAndType(user, emptyCell, {
        inputValue: '{backspace}6{enter}',
        expectedFieldValue: '4',
      });

      expectEditFieldToDisappear(emptyCell);
      expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalledWith(
        createdEntry.id,
        {
          hours: '6',
        }
      );
      await waitFor(() => {
        expect(emptyCell).toHaveTextContent('6.00');
      });
      expect(getRowSumCellFor('Project')).toHaveTextContent('8.00');
      expect(getRowSumCellFor('Activity1')).toHaveTextContent('8.00');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('6.00');
      expect(getTotalSumCell()).toHaveTextContent('8.00');

      await clickAndType(user, emptyCell, {
        inputValue: '{backspace}{enter}',
        expectedFieldValue: '6',
      });

      expectEditFieldToDisappear(emptyCell);
      expect(timeEntryDataStore.deleteTimeEntry).toHaveBeenCalledWith(
        createdEntry.id
      );
      await expectEmptyText(emptyCell);
      expect(getRowSumCellFor('Project')).toHaveTextContent('2.00');
      expect(getRowSumCellFor('Activity1')).toHaveTextContent('2.00');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('');
      expect(getTotalSumCell()).toHaveTextContent('2.00');

      // add new row and input activity
      await clickPlusButton(user, 'Issue');
      const activityChoices = getActivityChoices();
      await user.click(activityChoices[0]);

      expectTextContents(getActivityRowHeadersIn('Issue'), [
        /Activity2/,
        /Activity1/,
      ]);

      const addRowEntry = getTimeEntryCell('Activity2', '2021-01-05');
      await clickAndType(user, addRowEntry, {
        inputValue: '5{enter}',
        expectedFieldValue: '',
      });

      expectEditFieldToDisappear(addRowEntry);
      expect(timeEntryDataStore.createTimeEntry).toHaveBeenCalledWith(
        {
          date: '2021-01-05',
          hours: '5',
          activityType: 'issue',
          activity: { issueId: 1, activityId: 2 },
        },
        1
      );
      await waitFor(() => {
        expect(addRowEntry).toHaveTextContent('5.00');
      });
      expect(getRowSumCellFor('Project')).toHaveTextContent('7.00');
      expect(getRowSumCellFor('Activity2')).toHaveTextContent('5.00');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('5.00');
      expect(getTotalSumCell()).toHaveTextContent('7.00');
    });

    it('for project time entry', async () => {
      const user = userEvent.setup({ skipHover: true });
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const activities = [
        buildActivity({ id: 1, name: 'Activity1', position: 2 }),
        buildActivity({ id: 2, name: 'Activity2', position: 1 }),
      ];
      const projects = [
        buildProject({ id: 1, name: 'Project', activityIds: [1, 2] }),
      ];
      const timeEntries = [
        buildProjectTimeEntry({
          activity: { projectId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 3,
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        projects,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const emptyCell = getTimeEntryCell('Activity1', '2021-01-05');
      await clickAndType(user, emptyCell, {
        inputValue: '6.2{enter}',
        expectedFieldValue: '',
      });

      expectEditFieldToDisappear(emptyCell);
      expect(timeEntryDataStore.createTimeEntry).toHaveBeenCalledWith(
        {
          date: '2021-01-05',
          hours: '6.2',
          activityType: 'project',
          activity: { projectId: 1, activityId: 1 },
        },
        1
      );
      await waitFor(() => {
        expect(emptyCell).toHaveTextContent('6.20');
      });
      expect(getRowSumCellFor('Project')).toHaveTextContent('9.20');
      expect(getRowSumCellFor('Activity1')).toHaveTextContent('9.20');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('6.20');
      expect(getTotalSumCell()).toHaveTextContent('9.20');

      const createdEntry = await lastCreatedEntry(timeEntryDataStore);
      timeEntryDataStore.updateTimeEntry.mockResolvedValue({
        ...createdEntry,
        hours: 2,
      });
      await clickAndType(user, emptyCell, {
        inputValue: '{Control>}a{/Control}2{enter}',
        expectedFieldValue: '6.2',
      });

      expectEditFieldToDisappear(emptyCell);
      expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalledWith(
        createdEntry.id,
        {
          hours: '2',
        }
      );
      await waitFor(() => {
        expect(emptyCell).toHaveTextContent('2.00');
      });
      expect(getRowSumCellFor('Project')).toHaveTextContent('5.00');
      expect(getRowSumCellFor('Activity1')).toHaveTextContent('5.00');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('2.00');
      expect(getTotalSumCell()).toHaveTextContent('5.00');

      await clickAndType(user, emptyCell, {
        inputValue: '{backspace}{enter}',
        expectedFieldValue: '2',
      });

      expectEditFieldToDisappear(emptyCell);
      expect(timeEntryDataStore.deleteTimeEntry).toHaveBeenCalledWith(
        createdEntry.id
      );
      await expectEmptyText(emptyCell);
      expect(getRowSumCellFor('Project')).toHaveTextContent('3.00');
      expect(getRowSumCellFor('Activity1')).toHaveTextContent('3.00');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('');
      expect(getTotalSumCell()).toHaveTextContent('3.00');

      // add new row and input activity
      await clickPlusButton(user, 'Project');
      const activityChoices = getActivityChoices();
      await user.click(activityChoices[0]);

      expectTextContents(getActivityRowHeadersIn('Projectのプロジェクト工数'), [
        /Activity2/,
        /Activity1/,
      ]);

      const addRowEntry = getTimeEntryCell('Activity2', '2021-01-05');
      await clickAndType(user, addRowEntry, {
        inputValue: '4.5{enter}',
        expectedFieldValue: '',
      });

      expectEditFieldToDisappear(addRowEntry);
      expect(timeEntryDataStore.createTimeEntry).toHaveBeenCalledWith(
        {
          date: '2021-01-05',
          hours: '4.5',
          activityType: 'project',
          activity: { projectId: 1, activityId: 2 },
        },
        1
      );
      await waitFor(() => {
        expect(addRowEntry).toHaveTextContent('4.50');
      });
      expect(getRowSumCellFor('Project')).toHaveTextContent('7.50');
      expect(getRowSumCellFor('Activity2')).toHaveTextContent('4.50');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('4.50');
      expect(getTotalSumCell()).toHaveTextContent('7.50');
    });

    it('for indirect time entry', async () => {
      const user = userEvent.setup({ skipHover: true });
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const indirectActivities = [
        buildActivity({
          id: 1,
          name: 'Indirect Activity1',
          active: true,
          position: 2,
        }),
        buildActivity({
          id: 2,
          name: 'Indirect Activity2',
          active: true,
          position: 1,
        }),
      ];
      const timeEntries = [
        buildIndirectTimeEntry({
          activity: { indirectActivityId: 1 },
          date: '2021-01-04',
          hours: 2,
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        indirectActivities,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const emptyCell = getTimeEntryCell('Indirect Activity1', '2021-01-05');
      await clickAndType(user, emptyCell, {
        inputValue: '5{enter}',
        expectedFieldValue: '',
      });

      expectEditFieldToDisappear(emptyCell);
      expect(timeEntryDataStore.createTimeEntry).toHaveBeenCalledWith(
        {
          date: '2021-01-05',
          hours: '5',
          activityType: 'indirect',
          activity: { indirectActivityId: 1 },
        },
        1
      );
      await waitFor(() => {
        expect(emptyCell).toHaveTextContent('5.00');
      });
      expect(getRowSumCellFor('Indirect Activity1')).toHaveTextContent('7.00');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('5.00');
      expect(getTotalSumCell()).toHaveTextContent('7.00');

      const createdEntry = await lastCreatedEntry(timeEntryDataStore);
      timeEntryDataStore.updateTimeEntry.mockResolvedValue({
        ...createdEntry,
        hours: 11,
      });

      await clickAndType(user, emptyCell, {
        inputValue: '{backspace}11{enter}',
        expectedFieldValue: '5',
      });

      expectEditFieldToDisappear(emptyCell);
      expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalledWith(
        createdEntry.id,
        {
          hours: '11',
        }
      );
      await waitFor(() => {
        expect(emptyCell).toHaveTextContent('11.00');
      });
      expect(getRowSumCellFor('Indirect Activity1')).toHaveTextContent('13.00');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('11.00');
      expect(getTotalSumCell()).toHaveTextContent('13.00');

      await clickAndType(user, emptyCell, {
        inputValue: '{backspace}{backspace}{enter}',
        expectedFieldValue: '11',
      });

      expectEditFieldToDisappear(emptyCell);
      expect(timeEntryDataStore.deleteTimeEntry).toHaveBeenCalledWith(
        createdEntry.id
      );
      await expectEmptyText(emptyCell);
      expect(getRowSumCellFor('Indirect Activity1')).toHaveTextContent('2.00');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('');
      expect(getTotalSumCell()).toHaveTextContent('2.00');

      // add new row and input activity
      await clickPlusButton(user, '間接作業');
      const activityChoices = getActivityChoices();
      await user.click(activityChoices[0]);

      expectTextContents(getActivityRowHeadersIn('間接作業'), [
        /Indirect Activity2/,
        /Indirect Activity1/,
      ]);

      const addRowEntry = getTimeEntryCell('Indirect Activity2', '2021-01-05');
      await clickAndType(user, addRowEntry, {
        inputValue: '3{enter}',
        expectedFieldValue: '',
      });

      expectEditFieldToDisappear(addRowEntry);
      expect(timeEntryDataStore.createTimeEntry).toHaveBeenCalledWith(
        {
          date: '2021-01-05',
          hours: '3',
          activityType: 'indirect',
          activity: { indirectActivityId: 2 },
        },
        1
      );
      await waitFor(() => {
        expect(addRowEntry).toHaveTextContent('3.00');
      });
      expect(getRowSumCellFor('Indirect Activity2')).toHaveTextContent('3.00');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('3.00');
      expect(getTotalSumCell()).toHaveTextContent('5.00');
    });

    it('clicking outside will close the edit field and update', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const indirectActivities = [
        buildActivity({ id: 1, name: 'Indirect Activity' }),
      ];
      const timeEntries = [
        buildIndirectTimeEntry({
          activity: { indirectActivityId: 1 },
          date: '2021-01-04',
          hours: 4,
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        indirectActivities,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });
      timeEntryDataStore.updateTimeEntry.mockResolvedValue({
        ...timeEntries[0],
        hours: 5,
      });

      const cell = getTimeEntryCell('Indirect Activity', '2021-01-04');
      await clickAndType(user, cell, {
        inputValue: '{backspace}5',
      });

      expectEditField(cell);

      await clickSomewhere(user);
      expectEditFieldToDisappear(cell);
      expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalledWith(
        timeEntries[0].id,
        {
          hours: '5',
        }
      );
      await waitFor(() => {
        expect(cell).toHaveTextContent('5.00');
      });
    });

    it('type escape key will close the edit field and revert to previous value', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const indirectActivities = [
        buildActivity({ id: 1, name: 'Indirect Activity' }),
      ];
      const timeEntries = [
        buildIndirectTimeEntry({
          activity: { indirectActivityId: 1 },
          date: '2021-01-04',
          hours: 4,
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        indirectActivities,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });
      timeEntryDataStore.updateTimeEntry.mockResolvedValue({
        ...timeEntries[0],
      });

      const cell = getTimeEntryCell('Indirect Activity', '2021-01-04');

      await clickAndType(user, cell, {
        inputValue: '{backspace}5',
      });
      expectEditField(cell);

      await user.type(cell, '{escape}', { skipClick: true });
      expectEditFieldToDisappear(cell);
      expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalledWith(
        timeEntries[0].id,
        {
          hours: '4',
        }
      );
      await waitFor(() => {
        expect(cell).toHaveTextContent('4.00');
      });
    });

    describe('Error notifications', () => {
      const setup = async () => {
        const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
        const workMonthProvider = setupWorkMonthProviderMock(workMonth);
        const activities = [buildActivity({ id: 1, name: 'Activity' })];
        const issues = [buildIssue({ id: 1, projectId: 1, subject: 'Issue' })];
        const projects = [
          buildProject({ id: 1, name: 'Project', activityIds: [1] }),
        ];
        const timeEntries = [
          buildIssueTimeEntry({
            activity: { issueId: 1, activityId: 1 },
            date: '2021-01-04',
            hours: 2,
          }),
        ];
        const timeEntryDataStore = setupTimeEntryDataStoreMock({
          activities,
          issues,
          projects,
          timeEntries,
        });
        await renderWith({
          workMonthProvider,
          timeEntryDataStore,
          attachmentApi: setupAttachmentApiMock(),
        });
        return { timeEntryDataStore };
      };

      it('displays a notification on create error', async () => {
        const user = userEvent.setup();
        const { timeEntryDataStore } = await setup();
        timeEntryDataStore.createTimeEntry.mockRejectedValueOnce({
          error: 'Create Error',
        });
        const emptyCell = getTimeEntryCell('Activity', '2021-01-05');
        await clickAndType(user, emptyCell, { inputValue: '2{enter}' });

        expectEditField(emptyCell);
        const alert = await screen.findByRole('alert');
        expect(alert).toHaveTextContent('Create Error');
      });

      it('displays a notification on update error', async () => {
        const user = userEvent.setup();
        const { timeEntryDataStore } = await setup();
        timeEntryDataStore.updateTimeEntry.mockRejectedValueOnce({
          error: 'Update Error',
        });
        const cellWithEntry = getTimeEntryCell('Activity', '2021-01-04');
        await clickAndType(user, cellWithEntry, {
          inputValue: '2{enter}',
        });

        expectEditField(cellWithEntry);
        const alert = await screen.findByRole('alert');
        expect(alert).toHaveTextContent('Update Error');
      });

      it('displays a notification on delete error', async () => {
        const user = userEvent.setup();
        const { timeEntryDataStore } = await setup();
        timeEntryDataStore.deleteTimeEntry.mockRejectedValueOnce({
          error: 'Delete Error',
        });
        const cellWithEntry = getTimeEntryCell('Activity', '2021-01-04');
        await clickAndType(user, cellWithEntry, {
          inputValue: '{Control>}a{/Control}{backspace}{enter}',
        });

        expectEditField(cellWithEntry);
        const alert = await screen.findByRole('alert');
        expect(alert).toHaveTextContent('Delete Error');
      });
    });
  });

  describe('Permissions', () => {
    const renderWithPermission = async (
      permissions: Permission[]
    ): Promise<void> => {
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5, 6] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const activities = [
        buildActivity({ id: 1, name: 'Issue Activity' }),
        buildActivity({ id: 2, name: 'Project Activity' }),
      ];
      const indirectActivities = [
        buildActivity({ id: 1, name: 'Indirect Activity' }),
      ];
      const issues = [buildIssue({ id: 1, projectId: 1 })];
      const projects = [
        buildProject({ id: 1, permissions, activityIds: [1, 2] }),
      ];
      const timeEntries = [
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          comments: 'Single Entry',
        }),
        buildProjectTimeEntry({
          activity: { projectId: 1, activityId: 2 },
          date: '2021-01-04',
          comments: 'Single Entry',
        }),
        buildIndirectTimeEntry({
          activity: { indirectActivityId: 1 },
          date: '2021-01-04',
          comments: 'Single Entry',
        }),
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-05',
          comments: 'Entry 1',
        }),
        buildProjectTimeEntry({
          activity: { projectId: 1, activityId: 2 },
          date: '2021-01-05',
          comments: 'Entry 1',
        }),
        buildIndirectTimeEntry({
          activity: { indirectActivityId: 1 },
          date: '2021-01-05',
          comments: 'Entry 1',
        }),
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-05',
          comments: 'Entry 2',
        }),
        buildProjectTimeEntry({
          activity: { projectId: 1, activityId: 2 },
          date: '2021-01-05',
          comments: 'Entry 2',
        }),
        buildIndirectTimeEntry({
          activity: { indirectActivityId: 1 },
          date: '2021-01-05',
          comments: 'Entry 2',
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        indirectActivities,
        issues,
        projects,
        timeEntries,
      });
      timeEntries.forEach((t) => {
        timeEntryDataStore.updateTimeEntry.mockResolvedValue({ ...t });
      });
      await renderWith({
        indirectTimeEntryPermissions: permissions,
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });
    };

    const getCellWithOneEntry = (activityName: string): HTMLElement =>
      getTimeEntryCellsFor(activityName)[0]; // 2021-01-04
    const getCellWithMultipleEntries = (activityName: string): HTMLElement =>
      getTimeEntryCellsFor(activityName)[1]; // 2021-01-05
    const getEmptyCell = (activityName: string): HTMLElement =>
      getTimeEntryCellsFor(activityName)[2]; // 2021-01-06

    const expectNoTextFieldAndNoModal = (
      user: UserEvent,
      cellGetter: (activityName: string) => HTMLElement
    ): void => {
      Promise.all(
        ['Issue Activity', 'Project Activity', 'Indirect Activity'].map(
          (activityName) => {
            (async () => {
              const cell = cellGetter(activityName);
              await user.click(cell);

              expect(
                within(cell).queryByRole('textbox')
              ).not.toBeInTheDocument();
              expect(queryEditModal()).not.toBeInTheDocument();

              openContextMenu(cell);

              expect(queryEditModal()).not.toBeInTheDocument();
            })();
          }
        )
      );
    };

    const expectTextField = async (
      user: UserEvent,
      activityName: 'Issue Activity' | 'Project Activity' | 'Indirect Activity',
      cellGetter: (activityName: string) => HTMLElement
    ): Promise<void> => {
      const cell = cellGetter(activityName);
      await clickSomewhere(user);
      await user.click(cell);
      expect(within(cell).getByRole('textbox')).toBeInTheDocument();
    };

    const openModal = async (
      cellGetter: (activityName: string) => HTMLElement,
      activityName: string
    ): Promise<void> => {
      const cell = cellGetter(activityName);
      openContextMenu(cell);
      await waitForModal();
    };

    const expectModalWithCreateButNoEdit = async (
      activityName: string,
      cellGetter: (activityName: string) => HTMLElement
    ): Promise<void> => {
      await openModal(cellGetter, activityName);

      const editModal = getEditModal();
      expect(within(editModal).getByText('作成')).toBeInTheDocument();
      expect(
        within(editModal).queryByRole('button', { name: '編集' })
      ).not.toBeInTheDocument();
    };

    const expectModalWithEditButNoCreate = async (
      activityName: string,
      cellGetter: (activityName: string) => HTMLElement
    ): Promise<void> => {
      await openModal(cellGetter, activityName);

      const editModal = getEditModal();
      expect(within(editModal).queryByText('作成')).not.toBeInTheDocument();
      expect(
        within(editModal).getAllByRole('button', { name: '編集' })[0] // getByRole will throw error when multiple elements exist
      ).toBeInTheDocument();
    };

    describe('without any permissions', () => {
      it('cannot edit empty cells', async () => {
        const user = userEvent.setup();
        await renderWithPermission([]);
        expectNoTextFieldAndNoModal(user, getEmptyCell);
      });

      it('cannot edit cells with one entry', async () => {
        const user = userEvent.setup();
        await renderWithPermission([]);
        expectNoTextFieldAndNoModal(user, getCellWithOneEntry);
      });

      it('cannot edit cells with multiple entries', async () => {
        const user = userEvent.setup();
        await renderWithPermission([]);
        expectNoTextFieldAndNoModal(user, getCellWithMultipleEntries);
      });
    });

    describe('with only edit permissions', () => {
      it('cannot edit empty cells', async () => {
        const user = userEvent.setup();
        await renderWithPermission(['edit']);
        expectNoTextFieldAndNoModal(user, getEmptyCell);
      });

      it('can edit cells with one entry via double click', async () => {
        const user = userEvent.setup();
        await renderWithPermission(['edit']);
        await expectTextField(user, 'Issue Activity', getCellWithOneEntry);
        await expectTextField(user, 'Project Activity', getCellWithOneEntry);
        await expectTextField(user, 'Indirect Activity', getCellWithOneEntry);
      });

      it('can edit but not create entries in cells with one entry via right click', async () => {
        await renderWithPermission(['edit']);
        await expectModalWithEditButNoCreate(
          'Issue Activity',
          getCellWithOneEntry
        );
        await expectModalWithEditButNoCreate(
          'Project Activity',
          getCellWithOneEntry
        );
        await expectModalWithEditButNoCreate(
          'Indirect Activity',
          getCellWithOneEntry
        );
      });

      it('can edit but not create entries in cells with multiple entries via right click', async () => {
        await renderWithPermission(['edit']);
        await expectModalWithEditButNoCreate(
          'Issue Activity',
          getCellWithMultipleEntries
        );
        await expectModalWithEditButNoCreate(
          'Project Activity',
          getCellWithMultipleEntries
        );
        await expectModalWithEditButNoCreate(
          'Indirect Activity',
          getCellWithMultipleEntries
        );
      });
    });

    describe('with only create permissions', () => {
      it('can create entries via double click', async () => {
        const user = userEvent.setup();
        await renderWithPermission(['create']);
        await expectTextField(user, 'Issue Activity', getEmptyCell);
        await expectTextField(user, 'Project Activity', getEmptyCell);
        await expectTextField(user, 'Indirect Activity', getEmptyCell);
      });

      it('can create but not edit entries in empty cells via right click', async () => {
        await renderWithPermission(['create']);
        await expectModalWithCreateButNoEdit('Issue Activity', getEmptyCell);
        await expectModalWithCreateButNoEdit('Project Activity', getEmptyCell);
        await expectModalWithCreateButNoEdit('Indirect Activity', getEmptyCell);
      });

      it('can create but not edit entries in cells with one entry via right click', async () => {
        await renderWithPermission(['create']);
        await expectModalWithCreateButNoEdit(
          'Issue Activity',
          getCellWithOneEntry
        );
        await expectModalWithCreateButNoEdit(
          'Project Activity',
          getCellWithOneEntry
        );
        await expectModalWithCreateButNoEdit(
          'Indirect Activity',
          getCellWithOneEntry
        );
      });

      it('can create but not edit entries in cells with multiple entries via right click', async () => {
        await renderWithPermission(['create']);
        await expectModalWithCreateButNoEdit(
          'Issue Activity',
          getCellWithMultipleEntries
        );
        await expectModalWithCreateButNoEdit(
          'Project Activity',
          getCellWithMultipleEntries
        );
        await expectModalWithCreateButNoEdit(
          'Indirect Activity',
          getCellWithMultipleEntries
        );
      });
    });
  });

  describe('Adding new activity rows', () => {
    it('for issues', async () => {
      const user = userEvent.setup();
      const activities = [
        buildActivity({ id: 1, name: 'Review', position: 3 }),
        buildActivity({ id: 2, name: 'Development', position: 2 }),
        buildActivity({ id: 3, name: 'Test', position: 1 }),
      ];
      const issues = [
        buildIssue({ id: 1, projectId: 1, subject: 'Issue1' }),
        buildIssue({ id: 2, projectId: 2, subject: 'Issue2' }),
      ];
      const projects = [
        buildProject({ id: 1, name: 'Project A', activityIds: [1, 2, 3] }),
        buildProject({ id: 2, name: 'Project B', activityIds: [2, 3] }),
      ];
      const timeEntries = [
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
        }),
        buildIssueTimeEntry({
          activity: { issueId: 2, activityId: 3 },
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        issues,
        projects,
        timeEntries,
      });
      await renderWith({
        workMonthProvider: setupWorkMonthProviderMock(),
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      expectTextContents(getActivityRowHeadersIn('Issue1'), [/Review/]);

      await clickPlusButton(user, 'Issue1');

      const activityChoices1 = getActivityChoices();
      expectTextContents(activityChoices1, [/Test/, /Development/]);

      await user.click(activityChoices1[0]);

      expectTextContents(getActivityRowHeadersIn('Issue1'), [/Test/, /Review/]);

      expectTextContents(getActivityRowHeadersIn('Issue2'), [/Test/]);

      await clickPlusButton(user, 'Issue2');

      const activityChoices2 = getActivityChoices();
      expectTextContents(activityChoices2, ['Development']);

      await user.click(activityChoices2[0]);

      expectTextContents(getActivityRowHeadersIn('Issue2'), [
        /Test/,
        /Development/,
      ]);
    });

    it('for projects', async () => {
      const user = userEvent.setup();
      const activities = [
        buildActivity({ id: 1, name: 'Review', position: 3 }),
        buildActivity({ id: 2, name: 'Development', position: 2 }),
        buildActivity({ id: 3, name: 'Test', position: 1 }),
      ];
      const projects = [
        buildProject({ id: 1, name: 'Project A', activityIds: [1, 2, 3] }),
        buildProject({ id: 2, name: 'Project B', activityIds: [2, 3] }),
      ];
      const timeEntries = [
        buildProjectTimeEntry({
          activity: { projectId: 1, activityId: 2 },
        }),
        buildProjectTimeEntry({
          activity: { projectId: 2, activityId: 3 },
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        projects,
        timeEntries,
      });
      await renderWith({
        workMonthProvider: setupWorkMonthProviderMock(),
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      expectTextContents(
        getActivityRowHeadersIn('Project Aのプロジェクト工数'),
        [/Development/]
      );

      await clickPlusButton(user, 'Project A');
      const activityChoicesA = getActivityChoices();
      expectTextContents(activityChoicesA, [/Test/, /Review/]);

      await user.click(activityChoicesA[0]);

      expectTextContents(
        getActivityRowHeadersIn('Project Aのプロジェクト工数'),
        [/Test/, /Development/]
      );

      expectTextContents(
        getActivityRowHeadersIn('Project Bのプロジェクト工数'),
        [/Test/]
      );

      await clickPlusButton(user, 'Project B');
      const activityChoicesB = getActivityChoices();
      expectTextContents(activityChoicesB, ['Development']);

      await user.click(activityChoicesB[0]);

      expectTextContents(
        getActivityRowHeadersIn('Project Bのプロジェクト工数'),
        [/Test/, /Development/]
      );
    });

    it('for indirect activities', async () => {
      const user = userEvent.setup();
      const indirectActivities = [
        buildActivity({
          id: 1,
          name: 'Meetings',
          active: false,
          position: 4,
        }),
        buildActivity({
          id: 2,
          name: 'Smalltalk',
          active: true,
          position: 3,
        }),
        buildActivity({ id: 3, name: 'Mails', active: true, position: 2 }),
        buildActivity({
          id: 4,
          name: 'Paperworks',
          active: true,
          position: 1,
        }),
      ];
      const timeEntries = [
        buildIndirectTimeEntry({
          activity: { indirectActivityId: 2 },
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        indirectActivities,
        timeEntries,
      });
      await renderWith({
        workMonthProvider: setupWorkMonthProviderMock(),
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      expectTextContents(getActivityRowHeadersIn('間接作業'), [/Smalltalk/]);

      await clickPlusButton(user, '間接作業');

      const activityChoices = getActivityChoices();
      expectTextContents(activityChoices, [/Paperworks/, /Mails/]);

      await user.click(activityChoices[0]);

      expectTextContents(getActivityRowHeadersIn('間接作業'), [
        /Paperworks/,
        /Smalltalk/,
      ]);
    });

    it('does not show the plus button when all activites are added', async () => {
      const activities = [buildActivity({ id: 1, name: 'Development' })];
      const issues = [buildIssue({ id: 1, projectId: 1, subject: 'Issue' })];
      const projects = [
        buildProject({ id: 1, name: 'Project', activityIds: [1] }),
      ];
      const timeEntries = [
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        issues,
        projects,
        timeEntries,
      });
      await renderWith({
        workMonthProvider: setupWorkMonthProviderMock(),
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      expect(
        within(getRowHeaderGroup('Issue')).queryByRole('button')
      ).not.toBeInTheDocument();
    });
  });

  describe('Edit modal', () => {
    const clickOutsideModal = async (user: UserEvent): Promise<void> => {
      await clickSomewhere(user);
    };

    const getTimeEntryItem = (name: string | RegExp): HTMLElement =>
      within(getEditModal()).getByRole('listitem', { name });

    const selectTimeEntryItem = async (
      user: UserEvent,
      name: string | RegExp
    ): Promise<void> => {
      await user.click(
        within(getTimeEntryItem(name)).getByRole('button', { name: '編集' })
      );
    };

    const deleteEntry = async (
      user: UserEvent,
      name: string | RegExp
    ): Promise<void> => {
      const deleteButton = within(getTimeEntryItem(name)).getByRole('button', {
        name: '削除',
      });
      const confirm = jest.spyOn(window, 'confirm').mockReturnValue(true);
      await user.click(deleteButton);
      expect(confirm).toHaveBeenCalled();
    };

    const getEditModalField = (name: string): HTMLElement =>
      within(getEditModal()).getByLabelText(new RegExp(name));

    const getEditModalCreateButton = (): HTMLElement =>
      within(getEditModal()).getByRole('button', { name: '作成' });

    const getEditModalSaveButton = (): HTMLElement =>
      within(getEditModal()).getByRole('button', { name: '保存' });

    it('editing cell with multiple entries', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const issues = [buildIssue({ id: 1, projectId: 1, subject: 'Issue' })];
      const projects = [
        buildProject({ id: 1, name: 'Project', activityIds: [1] }),
      ];
      const timeEntries = [
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 2,
          comments: 'Entry A',
        }),
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 3,
          comments: 'Entry B',
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        issues,
        projects,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Activity', '2021-01-04');
      await user.click(cell);
      await waitForModal();

      expect(within(cell).queryByRole('textbox')).not.toBeInTheDocument();

      const editModal = getEditModal();
      expect(getTimeEntryItem(/Entry A/)).toBeInTheDocument();
      expect(getTimeEntryItem(/Entry B/)).toBeInTheDocument();
      const hoursEditField = getEditModalField('時間');
      const commentsEditField = getEditModalField('コメント');
      expect(hoursEditField).toHaveFocus();
      expect(hoursEditField).toHaveValue('');
      expect(commentsEditField).toHaveValue('');

      await selectTimeEntryItem(user, /Entry A/);

      await waitFor(() => {
        expect(hoursEditField).toHaveValue('2');
      });
      expect(commentsEditField).toHaveValue('Entry A');

      timeEntryDataStore.updateTimeEntry.mockResolvedValue({
        ...timeEntries[0],
        hours: 5.5,
        comments: 'A new comment',
      });
      await user.type(hoursEditField, '{Control>}a{/Control}5.5');
      await user.type(commentsEditField, '{Control>}a{/Control}A new comment');
      await user.click(getEditModalSaveButton());
      await waitUntilLoaded();

      expect(editModal).not.toBeInTheDocument();
      expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalledWith(
        timeEntries[0].id,
        {
          hours: '5.5',
          comments: 'A new comment',
        }
      );
      await waitFor(() => {
        expect(cell).toHaveTextContent('8.50');
      });
      expect(getRowSumCellFor('Project')).toHaveTextContent('8.50');
      expect(getRowSumCellFor('Activity')).toHaveTextContent('8.50');
      expect(getDateSumCellFor('2021-01-04')).toHaveTextContent('8.50');
      expect(getTotalSumCell()).toHaveTextContent('8.50');
    });

    it('press Escape to discard edits.', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const issues = [buildIssue({ id: 1, projectId: 1, subject: 'Issue' })];
      const projects = [
        buildProject({ id: 1, name: 'Project', activityIds: [1] }),
      ];
      const timeEntries = [
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 2,
          comments: 'Entry A',
        }),
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 3,
          comments: 'Entry B',
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        issues,
        projects,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Activity', '2021-01-04');
      await user.click(cell);
      await waitForModal();

      const editModal = getEditModal();
      const hoursEditField = getEditModalField('時間');

      await selectTimeEntryItem(user, /Entry A/);

      await waitFor(() => {
        expect(hoursEditField).toHaveValue('2');
      });

      await user.type(hoursEditField, '{Control>}a{/Control}5.5');
      await user.type(hoursEditField, '{Escape}');

      await waitFor(() => {
        expect(editModal).not.toBeInTheDocument();
      });
      expect(timeEntryDataStore.updateTimeEntry).not.toHaveBeenCalled();
      await waitFor(() => {
        expect(cell).toHaveTextContent('5.00');
      });
    });

    it('press Enter to save edits.', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const issues = [buildIssue({ id: 1, projectId: 1, subject: 'Issue' })];
      const projects = [
        buildProject({ id: 1, name: 'Project', activityIds: [1] }),
      ];
      const timeEntries = [
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 2,
          comments: 'Entry A',
        }),
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 3,
          comments: 'Entry B',
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        issues,
        projects,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Activity', '2021-01-04');
      await user.click(cell);
      await waitForModal();

      const editModal = getEditModal();
      const hoursEditField = getEditModalField('時間');
      const commentsEditField = getEditModalField('コメント');

      await selectTimeEntryItem(user, /Entry A/);

      await waitFor(() => {
        expect(hoursEditField).toHaveValue('2');
      });
      expect(commentsEditField).toHaveValue('Entry A');

      timeEntryDataStore.updateTimeEntry.mockResolvedValue({
        ...timeEntries[0],
        hours: 5.5,
        comments: 'A new comment',
      });
      await user.type(hoursEditField, '{Control>}a{/Control}5.5');
      await user.type(commentsEditField, '{Control>}a{/Control}A new comment');
      await user.type(hoursEditField, '{Enter}');
      await waitUntilLoaded();

      expect(editModal).not.toBeInTheDocument();
      expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalledWith(
        timeEntries[0].id,
        {
          hours: '5.5',
          comments: 'A new comment',
        }
      );
      await waitFor(() => {
        expect(cell).toHaveTextContent('8.50');
      });
    });

    it('presssing enter does not save the data because the required fields have not been filled in.', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const issues = [buildIssue({ id: 1, projectId: 1, subject: 'Issue' })];
      const projects = [
        buildProject({ id: 1, name: 'Project', activityIds: [1] }),
      ];
      const timeEntries = [
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 2,
          comments: 'Entry A',
        }),
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 3,
          comments: 'Entry B',
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        issues,
        projects,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Activity', '2021-01-04');
      await user.click(cell);
      await waitForModal();

      const editModal = getEditModal();
      const hoursEditField = getEditModalField('時間');
      const commentsEditField = getEditModalField('コメント');

      await selectTimeEntryItem(user, /Entry A/);

      await waitFor(() => {
        expect(hoursEditField).toHaveValue('2');
      });
      expect(commentsEditField).toHaveValue('Entry A');

      await user.type(hoursEditField, '{Control>}a{/Control}5.5');
      await user.clear(hoursEditField);
      await user.type(commentsEditField, '{Control>}a{/Control}A new comment');
      await user.type(hoursEditField, '{Enter}');

      expect(editModal).toBeInTheDocument();
      expect(timeEntryDataStore.updateTimeEntry).not.toHaveBeenCalled();

      await user.type(hoursEditField, '{Escape}');
      expect(editModal).not.toBeInTheDocument();

      await waitFor(() => {
        expect(cell).toHaveTextContent('5.00');
      });
    });

    it('deleting from cell with multiple entries', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const projects = [
        buildProject({ id: 1, name: 'Project', activityIds: [1] }),
      ];
      const timeEntries = [
        buildProjectTimeEntry({
          activity: { projectId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 3,
          comments: 'Entry A',
        }),
        buildProjectTimeEntry({
          activity: { projectId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 4,
          comments: 'Entry B',
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        projects,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Activity', '2021-01-04');
      await user.click(cell);
      await waitForModal();
      const editModal = getEditModal();
      await deleteEntry(user, /Entry A/);
      await waitUntilLoaded();

      expect(editModal).not.toBeInTheDocument();
      expect(timeEntryDataStore.deleteTimeEntry).toHaveBeenCalledWith(
        timeEntries[0].id
      );
      await waitFor(() => {
        expect(cell).toHaveTextContent('4.00');
      });
      expect(getRowSumCellFor('Project')).toHaveTextContent('4.00');
      expect(getRowSumCellFor('Activity')).toHaveTextContent('4.00');
      expect(getDateSumCellFor('2021-01-04')).toHaveTextContent('4.00');
      expect(getTotalSumCell()).toHaveTextContent('4.00');
    });

    it('right clicking will open the modal, too', async () => {
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const indirectActivities = [
        buildActivity({ id: 1, name: 'Indirect Activity' }),
      ];
      const timeEntries = [
        buildIndirectTimeEntry({
          activity: { indirectActivityId: 1 },
          date: '2021-01-04',
          hours: 2,
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        indirectActivities,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Indirect Activity', '2021-01-04');
      openContextMenu(cell);
      await waitForModal();

      expect(getEditModal()).toBeInTheDocument();
    });

    it('edits custom field values for issue time entries', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const customField = buildCustomField({ name: 'Category' });
      const timeEntry = buildIssueTimeEntry({
        activity: { issueId: 1, activityId: 1 },
        date: '2021-01-04',
        hours: 2,
        comments: 'Entry',
        customFieldValues: {
          [customField.id]: 'General',
        },
      });
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const projects = [buildProject({ id: 1, activityIds: [1] })];
      const issues = [buildIssue({ id: 1, projectId: 1 })];
      const customFields = [customField];
      const timeEntries = [timeEntry];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        projects,
        issues,
        customFields,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Activity', '2021-01-04');
      openContextMenu(cell);
      await waitForModal();

      const customFieldInput = getEditModalField('Category');
      expect(customFieldInput).toBeInTheDocument();
      expect(customFieldInput).toHaveValue('');

      await selectTimeEntryItem(user, 'Entry');

      await waitFor(() => {
        expect(customFieldInput).toHaveValue('General');
      });

      timeEntryDataStore.updateTimeEntry.mockResolvedValue({
        ...timeEntry,
        customFieldValues: { [customField.id]: 'Overtime' },
      });
      await user.type(customFieldInput, '{Control>}a{/Control}Overtime');
      await user.click(getEditModalSaveButton());
      await waitUntilLoaded();

      expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalledWith(
        timeEntry.id,
        {
          hours: timeEntry.hours.toString(),
          comments: timeEntry.comments,
          customFieldValues: { [customField.id]: 'Overtime' },
        }
      );
    });

    it('edits custom field values for project time entries', async () => {
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const customField = buildCustomField({ name: 'Category' });
      const timeEntry = buildProjectTimeEntry({
        activity: { projectId: 1, activityId: 1 },
        date: '2021-01-04',
        hours: 2,
        comments: 'Entry',
        customFieldValues: {
          [customField.id]: 'General',
        },
      });
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const projects = [buildProject({ id: 1, activityIds: [1] })];
      const customFields = [customField];
      const timeEntries = [timeEntry];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        projects,
        customFields,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Activity', '2021-01-04');
      openContextMenu(cell);
      await waitForModal();

      expect(getEditModalField('Category')).toBeInTheDocument();
    });

    it('retrieves custom fields before opening', async () => {
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const customField = buildCustomField({
        name: 'Color',
        fieldFormat: 'list',
        possibleValuesOptions: [
          ['Red', 'red'],
          ['Green', 'green'],
        ],
      });
      const timeEntry = buildProjectTimeEntry({
        activity: { projectId: 1, activityId: 1 },
        date: '2021-01-04',
      });
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const projects = [buildProject({ id: 1, activityIds: [1] })];
      const customFields = [customField];
      const timeEntries = [timeEntry];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        projects,
        customFields,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Activity', '2021-01-04');
      openContextMenu(cell);
      await waitForModal();

      expect(timeEntryDataStore.getCustomFieldsForProject).toHaveBeenCalledWith(
        1
      );

      const customFieldInput = getEditModalField('Color');
      const options = within(customFieldInput).getAllByRole('option');
      expectTextContents(options, ['', 'Red', 'Green']);
    });

    it('does not edit custom field values for indirect time entries', async () => {
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const customField = buildCustomField({ name: 'Category' });
      const timeEntry = buildIndirectTimeEntry({
        activity: { indirectActivityId: 1 },
        date: '2021-01-04',
        hours: 2,
        comments: 'Entry',
      });
      const indirectActivities = [buildActivity({ id: 1, name: 'Activity' })];
      const customFields = [customField];
      const timeEntries = [timeEntry];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        indirectActivities,
        customFields,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Activity', '2021-01-04');
      openContextMenu(cell);
      await waitForModal();

      expect(
        within(getEditModal()).queryByRole('textbox', {
          name: new RegExp('Category'),
        })
      ).not.toBeInTheDocument();
    });

    it('edits custom field values for issue time entriestextarea custom fields can be submitted with ctrl+enter', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const customField = buildCustomField({
        name: 'Text',
        fieldFormat: 'text',
      });
      const timeEntry = buildIssueTimeEntry({
        activity: { issueId: 1, activityId: 1 },
        date: '2021-01-04',
        hours: 2,
        comments: 'Entry',
        customFieldValues: {
          [customField.id]: 'General',
        },
      });
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const projects = [buildProject({ id: 1, activityIds: [1] })];
      const issues = [buildIssue({ id: 1, projectId: 1 })];
      const customFields = [customField];
      const timeEntries = [timeEntry];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        projects,
        issues,
        customFields,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Activity', '2021-01-04');
      openContextMenu(cell);
      await waitForModal();

      const customFieldInput = getEditModalField('Text');
      expect(customFieldInput).toBeInTheDocument();
      expect(customFieldInput).toHaveValue('');

      await selectTimeEntryItem(user, 'Entry');

      await waitFor(() => {
        expect(customFieldInput).toHaveValue('General');
      });

      timeEntryDataStore.updateTimeEntry.mockResolvedValue({
        ...timeEntry,
        customFieldValues: { [customField.id]: 'Overtime\nHoge' },
      });
      await user.type(
        customFieldInput,
        '{Control>}a{/Control}Overtime{Enter}Hoge'
      );
      await user.type(customFieldInput, '{Control>}{Enter}{/Control}');
      await waitUntilLoaded();

      expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalledWith(
        timeEntry.id,
        {
          hours: timeEntry.hours.toString(),
          comments: timeEntry.comments,
          customFieldValues: { [customField.id]: 'Overtime\nHoge' },
        }
      );
    });

    describe('Newly uploaded attachments', () => {
      const openModal = async (): Promise<void> => {
        const cell = getTimeEntryCell('Activity', '2021-01-04');
        openContextMenu(cell);
        await waitForModal();
      };

      const setup = async () => {
        const workMonth = buildWorkMonth({ month: '2021-01', days: [4] });
        const workMonthProvider = setupWorkMonthProviderMock(workMonth);
        const timeEntry = buildProjectTimeEntry({
          activity: { projectId: 1, activityId: 1 },
          date: '2021-01-04',
          comments: 'Existing Entry',
        });

        const activities = [buildActivity({ id: 1, name: 'Activity' })];
        const projects = [buildProject({ id: 1, activityIds: [1] })];
        const customFields = [
          buildCustomField({
            id: 1,
            name: 'Screenshot',
            fieldFormat: 'attachment',
          }),
        ];
        const timeEntries = [timeEntry];
        const timeEntryDataStore = setupTimeEntryDataStoreMock({
          activities,
          projects,
          customFields,
          timeEntries,
        });
        const attachmentApi = setupAttachmentApiMock();
        await renderWith({
          workMonthProvider,
          timeEntryDataStore,
          attachmentApi,
        });

        await openModal();

        return { timeEntryDataStore, attachmentApi, timeEntry };
      };

      const selectOtherEntry = async (user: UserEvent): Promise<void> =>
        selectTimeEntryItem(user, /Existing Entry/);

      const uploadAttachment = async (
        user: UserEvent,
        attachmentApi: jest.Mocked<AttachmentApiHandler>
      ): Promise<void> => {
        const input = getEditModalField('Screenshot');
        attachmentApi.createAttachment.mockResolvedValue({
          id: 11,
          filename: 'screenshot.png',
          token: '11.abc',
        });
        const file = new File(['content'], 'screenshot.png', {
          type: 'image/png',
        });
        await user.upload(input, file);

        await waitFor(() => {
          const input = getEditModalField('Screenshot');
          expect(
            within(input).getByRole('button', { name: '削除' })
          ).toBeInTheDocument();
        });
      };

      const createEntry = async (user: UserEvent): Promise<void> => {
        await user.type(getEditModalField('時間'), '2');
        await user.click(getEditModalCreateButton());
        await waitUntilLoaded();
      };

      const saveEntry = async (
        user: UserEvent,
        timeEntryDataStore: jest.Mocked<TimeEntryDataStore>,
        timeEntry: TimeEntry
      ): Promise<void> => {
        timeEntryDataStore.updateTimeEntry.mockResolvedValue({
          ...timeEntry,
          customFieldValues: { 1: '11' },
        });
        await user.click(getEditModalSaveButton());
        await waitUntilLoaded();
      };

      const expectAttachmentToBeDeleted = (
        attachmentApi: jest.Mocked<AttachmentApiHandler>
      ): void =>
        expect(attachmentApi.deleteAttachment).toHaveBeenCalledWith(11);

      const expectAttachmentNotToBeDeleted = (
        attachmentApi: jest.Mocked<AttachmentApiHandler>
      ): void => expect(attachmentApi.deleteAttachment).not.toHaveBeenCalled();

      it('deletes uploaded attachments when closing the modal', async () => {
        const user = userEvent.setup();
        const { attachmentApi } = await setup();
        await uploadAttachment(user, attachmentApi);
        await clickOutsideModal(user);

        expectAttachmentToBeDeleted(attachmentApi);

        await openModal();
        await selectOtherEntry(user);
        await uploadAttachment(user, attachmentApi);
        await clickOutsideModal(user);

        expectAttachmentToBeDeleted(attachmentApi);
      });

      it('does not delete after creating an entry', async () => {
        const user = userEvent.setup();
        const { attachmentApi } = await setup();
        await uploadAttachment(user, attachmentApi);
        await createEntry(user);
        await openModal();
        await clickOutsideModal(user);

        expectAttachmentNotToBeDeleted(attachmentApi);
      });

      it('does not delete after saving an entry', async () => {
        const user = userEvent.setup();
        const { timeEntryDataStore, attachmentApi, timeEntry } = await setup();
        await openModal();
        await selectOtherEntry(user);
        await uploadAttachment(user, attachmentApi);
        await saveEntry(user, timeEntryDataStore, timeEntry);

        await openModal();
        await clickOutsideModal(user);

        expectAttachmentNotToBeDeleted(attachmentApi);
      });

      it('deletes uploaded attachments when changing the edited entry', async () => {
        const user = userEvent.setup();
        const { attachmentApi } = await setup();
        await uploadAttachment(user, attachmentApi);
        await selectOtherEntry(user);

        expectAttachmentToBeDeleted(attachmentApi);
      });

      it('does not delete when changing the edited entry after saving first', async () => {
        const user = userEvent.setup();
        const { attachmentApi } = await setup();
        await createEntry(user);
        await openModal();
        await selectOtherEntry(user);

        expectAttachmentNotToBeDeleted(attachmentApi);
      });

      it('attachment field can be submitted with ctrl enter', async () => {
        const user = userEvent.setup();
        const { timeEntryDataStore, attachmentApi, timeEntry } = await setup();
        await openModal();
        await selectOtherEntry(user);
        await uploadAttachment(user, attachmentApi);

        timeEntryDataStore.updateTimeEntry.mockResolvedValue({
          ...timeEntry,
          customFieldValues: { 1: '11' },
        });
        await waitFor(async () => {
          const input = getEditModalField('Screenshot');
          await user.type(input, '{Control>}{Enter}{/Control}');
        });
        await waitUntilLoaded();

        expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalled();

        await openModal();
        await clickOutsideModal(user);

        expectAttachmentNotToBeDeleted(attachmentApi);
      });
    });

    it('creating entries', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const indirectActivities = [
        buildActivity({ id: 1, name: 'Indirect Activity' }),
      ];
      const timeEntries = [
        buildIndirectTimeEntry({
          activity: { indirectActivityId: 1 },
          date: '2021-01-04',
          hours: 2,
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        indirectActivities,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const cell = getTimeEntryCell('Indirect Activity', '2021-01-05');
      openContextMenu(cell);
      await waitForModal();
      const editModal = getEditModal();
      const hoursEditField = getEditModalField('時間');
      const commentsEditField = getEditModalField('コメント');
      await user.type(hoursEditField, '10');
      await user.type(commentsEditField, 'New Entry');
      await user.click(getEditModalCreateButton());
      await waitUntilLoaded();

      expect(editModal).not.toBeInTheDocument();
      expect(timeEntryDataStore.createTimeEntry).toHaveBeenCalledWith(
        {
          date: '2021-01-05',
          activityType: 'indirect',
          activity: { indirectActivityId: 1 },
          hours: '10',
          comments: 'New Entry',
        },
        1
      );
      await waitFor(() => {
        expect(cell).toHaveTextContent('10.00');
      });
      expect(getRowSumCellFor('Indirect Activity')).toHaveTextContent('12.00');
      expect(getDateSumCellFor('2021-01-05')).toHaveTextContent('10.00');
      expect(getTotalSumCell()).toHaveTextContent('12.00');
    });

    it('when comments are required, modal will always appear', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const issues = [buildIssue({ id: 1, projectId: 1, subject: 'Issue' })];
      const projects = [
        buildProject({ id: 1, name: 'Project', activityIds: [1] }),
      ];
      const timeEntries = [
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 2,
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        issues,
        projects,
        timeEntries,
      });
      await renderWith({
        timeEntryRequiredFields: ['comments'],
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const emptyCell = getTimeEntryCell('Activity', '2021-01-05');
      await user.click(emptyCell);
      await waitForModal();

      expect(getEditModal()).toBeInTheDocument();

      const commentsField = getEditModalField('コメント');
      expect(commentsField).toBeRequired();
    });

    it('when a custom field is required, modal will always appear for issue time entries', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const customField = buildCustomField({ isRequired: true });
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const issues = [buildIssue({ id: 1, projectId: 1 })];
      const projects = [buildProject({ id: 1, activityIds: [1] })];
      const timeEntries = [
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 2,
        }),
      ];
      const customFields = [customField];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        issues,
        projects,
        timeEntries,
        customFields,
      });
      await renderWith({
        requiredCustomFieldExists: true,
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const emptyCell = getTimeEntryCell('Activity', '2021-01-05');
      await user.click(emptyCell);
      await waitForModal();

      expect(getEditModal()).toBeInTheDocument();
      expect(getEditModalField(customField.name)).toBeRequired();
    });

    it('when a custom field is required, modal will always appear for project time entries', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const customField = buildCustomField({ isRequired: true });
      const activities = [buildActivity({ id: 1, name: 'Activity' })];
      const projects = [buildProject({ id: 1, activityIds: [1] })];
      const timeEntries = [
        buildProjectTimeEntry({
          activity: { projectId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 2,
        }),
      ];
      const customFields = [customField];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        projects,
        timeEntries,
        customFields,
      });
      await renderWith({
        requiredCustomFieldExists: true,
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const emptyCell = getTimeEntryCell('Activity', '2021-01-05');
      await user.click(emptyCell);
      await waitForModal();

      expect(getEditModal()).toBeInTheDocument();
      expect(getEditModalField(customField.name)).toBeRequired();
    });

    it('modal will not appear on double click for indirect time entries even when a custom field is required', async () => {
      const user = userEvent.setup();
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth);
      const customField = buildCustomField({ isRequired: true });
      const indirectActivities = [buildActivity({ id: 1, name: 'Activity' })];
      const timeEntries = [
        buildIndirectTimeEntry({
          activity: { indirectActivityId: 1 },
          date: '2021-01-04',
          hours: 2,
        }),
      ];
      const customFields = [customField];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        indirectActivities,
        timeEntries,
        customFields,
      });
      await renderWith({
        requiredCustomFieldExists: true,
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      const emptyCell = getTimeEntryCell('Activity', '2021-01-05');
      await user.click(emptyCell);

      const editField = within(emptyCell).getByRole('textbox');
      expect(editField).toBeInTheDocument();
    });

    describe('Error notifications', () => {
      describe('Error notifications', () => {
        const setup = async () => {
          const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
          const workMonthProvider = setupWorkMonthProviderMock(workMonth);
          const activities = [buildActivity({ id: 1, name: 'Activity' })];
          const issues = [
            buildIssue({ id: 1, projectId: 1, subject: 'Issue' }),
          ];
          const projects = [
            buildProject({ id: 1, name: 'Project', activityIds: [1] }),
          ];
          const timeEntries = [
            buildIssueTimeEntry({
              activity: { issueId: 1, activityId: 1 },
              date: '2021-01-04',
              comments: 'Entry',
              hours: 2,
            }),
          ];
          const timeEntryDataStore = setupTimeEntryDataStoreMock({
            activities,
            issues,
            projects,
            timeEntries,
          });
          await renderWith({
            workMonthProvider,
            timeEntryDataStore,
            attachmentApi: setupAttachmentApiMock(),
          });

          return { timeEntryDataStore };
        };

        it('displays a notification on create error', async () => {
          const user = userEvent.setup();
          const { timeEntryDataStore } = await setup();
          timeEntryDataStore.createTimeEntry.mockRejectedValueOnce({
            error: 'Create Error',
          });
          const emptyCell = getTimeEntryCell('Activity', '2021-01-05');

          openContextMenu(emptyCell);
          await waitForModal();
          await user.type(getEditModalField('時間'), '1');
          await user.click(getEditModalCreateButton());
          await waitUntilLoaded();

          expect(getEditModal()).toBeInTheDocument();
          const alert = await screen.findByRole('alert');
          expect(alert).toHaveTextContent('Create Error');
        });

        it('displays a notification on update error', async () => {
          const user = userEvent.setup();
          const { timeEntryDataStore } = await setup();
          timeEntryDataStore.updateTimeEntry.mockRejectedValueOnce({
            error: 'Update Error',
          });
          const cellWithEntry = getTimeEntryCell('Activity', '2021-01-04');

          openContextMenu(cellWithEntry);
          await waitForModal();
          await selectTimeEntryItem(user, /Entry/);
          await user.type(getEditModalField('時間'), '1');
          await user.click(getEditModalSaveButton());
          await waitUntilLoaded();

          expect(getEditModal()).toBeInTheDocument();
          const alert = await screen.findByRole('alert');
          expect(alert).toHaveTextContent('Update Error');
        });

        it('displays a notification on delete error', async () => {
          const user = userEvent.setup();
          const { timeEntryDataStore } = await setup();
          timeEntryDataStore.deleteTimeEntry.mockRejectedValueOnce({
            error: 'Delete Error',
          });
          const cellWithEntry = getTimeEntryCell('Activity', '2021-01-04');

          openContextMenu(cellWithEntry);
          await waitForModal();
          await deleteEntry(user, /Entry/);
          await waitUntilLoaded();

          expect(getEditModal()).toBeInTheDocument();
          const alert = await screen.findByRole('alert');
          expect(alert).toHaveTextContent('Delete Error');
        });
      });
    });
  });

  describe('Keyboard operations on Data Cells', () => {
    const setup = async (
      params: { projects?: Project[]; timeEntries?: IssueTimeEntry[] } = {}
    ) => {
      const workMonth = buildWorkMonth({ month: '2021-01', days: [4, 5] });
      const workMonthProvider = setupWorkMonthProviderMock(workMonth, 4);
      const activities = [
        buildActivity({ id: 1, name: 'Activity1', position: 1 }),
        buildActivity({ id: 2, name: 'Activity2', position: 2 }),
      ];
      const issues = [buildIssue({ id: 1, projectId: 1, subject: 'Issue' })];
      const projects = params.projects ?? [
        buildProject({ id: 1, name: 'Project', activityIds: [1, 2] }),
      ];
      const timeEntries = params.timeEntries ?? [
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 1 },
          date: '2021-01-04',
          hours: 2,
        }),
        buildIssueTimeEntry({
          activity: { issueId: 1, activityId: 2 },
          date: '2021-01-04',
          hours: 2,
        }),
      ];
      const timeEntryDataStore = setupTimeEntryDataStoreMock({
        activities,
        issues,
        projects,
        timeEntries,
      });
      await renderWith({
        workMonthProvider,
        timeEntryDataStore,
        attachmentApi: setupAttachmentApiMock(),
      });

      /**
         00 01
         10 11
       */
      const cell00 = getTimeEntryCell('Activity1', '2021-01-04');
      const cell01 = getTimeEntryCell('Activity1', '2021-01-05');
      const cell10 = getTimeEntryCell('Activity2', '2021-01-04');
      const cell11 = getTimeEntryCell('Activity2', '2021-01-05');
      const cells = [cell00, cell01, cell10, cell11] as const;

      const expectFocusCellByKey = (key: '00' | '01' | '10' | '11') => {
        const returnCells = [...cells];
        const { focus, rest } = match(key)
          .with('00', () => ({
            focus: returnCells.splice(0, 1)[0],
            rest: returnCells,
          }))
          .with('01', () => ({
            focus: returnCells.splice(1, 1)[0],
            rest: returnCells,
          }))
          .with('10', () => ({
            focus: returnCells.splice(2, 1)[0],
            rest: returnCells,
          }))
          .with('11', () => ({
            focus: returnCells.splice(3, 1)[0],
            rest: returnCells,
          }))
          .exhaustive();

        expectFocusCell(focus);
        rest.forEach((cell) => expectNotFocusCell(cell));

        return { focus, rest };
      };

      return { timeEntryDataStore, expectFocusCellByKey };
    };

    it('change the focus position of a cell up, down, left, or right with the keyboard.', async () => {
      const user = userEvent.setup();

      const { expectFocusCellByKey } = await setup();

      expectFocusCellByKey('00');
      await user.keyboard('[ArrowDown]');
      expectFocusCellByKey('10');
      await user.keyboard('[ArrowDown]');
      expectFocusCellByKey('10');
      await user.keyboard('[ArrowRight]');
      expectFocusCellByKey('11');
      await user.keyboard('[ArrowRight]');
      expectFocusCellByKey('11');
      await user.keyboard('[ArrowUp]');
      expectFocusCellByKey('01');
      await user.keyboard('[ArrowUp]');
      expectFocusCellByKey('01');
      await user.keyboard('[ArrowLeft]');
      expectFocusCellByKey('00');
      await user.keyboard('[ArrowLeft]');
      expectFocusCellByKey('00');
    });

    it('pressing Enter while typing confirms the input and moves the focus to the cell below', async () => {
      const user = userEvent.setup();

      const { timeEntryDataStore, expectFocusCellByKey } = await setup();

      // focus the base (00) cell, and type enter to make it the editing mode.
      const { focus } = expectFocusCellByKey('00');
      await user.keyboard('[Enter]');
      const editField = within(focus).getByRole('textbox');
      expect(editField).toHaveFocus();

      // So the cell is in edit mode, the user type '[Enter]'. This causes the time entry to be updated.
      timeEntryDataStore.updateTimeEntry.mockResolvedValueOnce({
        id: '0',
        activityType: 'issue',
        activity: { issueId: 1, activityId: 1 },
      });
      await user.type(editField, `[Enter]`, { skipClick: true });

      expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalledTimes(1);
      expectFocusCellByKey('10');
    });

    it('pressing Tab while typing confirms the input and moves the focus to the right cell', async () => {
      const user = userEvent.setup();

      const { timeEntryDataStore, expectFocusCellByKey } = await setup();

      // focus the base (00) cell, and type enter to make it the editing mode.
      const { focus } = expectFocusCellByKey('00');
      await user.keyboard('[Enter]');
      const editField = within(focus).getByRole('textbox');
      expect(editField).toHaveFocus();

      // So the cell is in edit mode, the user type '[Tab]'. This causes the time entry to be updated.
      timeEntryDataStore.updateTimeEntry.mockResolvedValueOnce({
        id: '0',
        activityType: 'issue',
        activity: { issueId: 1, activityId: 1 },
      });
      await user.type(editField, `[Tab]`, { skipClick: true });

      expect(timeEntryDataStore.updateTimeEntry).toHaveBeenCalledTimes(1);
      expectFocusCellByKey('01');
    });

    it('for cells that cannot be entered, pressing Enter should not enter input mode.', async () => {
      const user = userEvent.setup();

      const { expectFocusCellByKey } = await setup({
        projects: [
          buildProject({
            id: 1,
            name: 'Project',
            activityIds: [1, 2],
            permissions: [],
          }),
        ],
      });

      // focus the base (00) cell, but type enter to no effect due to the project does not give us any permissions.
      const { focus } = expectFocusCellByKey('00');
      await user.keyboard('[Enter]');
      expect(within(focus).queryByRole('textbox')).not.toBeInTheDocument();

      await user.keyboard('[ArrowDown]');
      expectFocusCellByKey('10');
    });

    it('there are multiple time entries, the Enter key opens a modal dialog', async () => {
      const user = userEvent.setup();

      const { expectFocusCellByKey } = await setup({
        timeEntries: [
          buildIssueTimeEntry({
            activity: { issueId: 1, activityId: 1 },
            date: '2021-01-04',
            hours: 1,
          }),
          buildIssueTimeEntry({
            activity: { issueId: 1, activityId: 1 },
            date: '2021-01-04',
            hours: 2,
          }),
          buildIssueTimeEntry({
            activity: { issueId: 1, activityId: 2 },
            date: '2021-01-04',
            hours: 1,
          }),
        ],
      });

      // focus the base (00) cell, and type enter to open modal dialog.
      expectFocusCellByKey('00');
      await user.keyboard('[Enter]');
      await waitForModal();
    });
  });
});
