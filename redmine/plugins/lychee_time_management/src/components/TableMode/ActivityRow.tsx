import * as React from 'react';
import { TIME_ENTRY_ROW_HEIGHT } from '@/lib/tableMode/cellStyle';
import { ActivityRowData, CellData } from '@/lib/tableMode/tableData';
import {
  RowCoordinates,
  TimeEntryId,
  WorkMonth,
  WorkDate,
} from '@/lib/tableMode/types';

import TableRow from './atoms/TableRow';

export type CellComponentType = React.FC<{
  cell: CellData;
  rowCoordinates: RowCoordinates;
  workDate: WorkDate;
  onEditAllEntries: () => void;
  className?: string;
  offset?: number;
}>;

type ActivityRowProps = {
  row: ActivityRowData;
  workMonth: WorkMonth;
  height?: number;
  activityRowOffset?: number;
  CellComponent: CellComponentType;
  rowCoordinates: RowCoordinates;
  onEditMultipleTimeEntries: (
    ids: TimeEntryId[],
    cellElement: Element,
    date: string
  ) => void;
};

const ActivityRow: React.FC<ActivityRowProps> = ({
  row,
  workMonth,
  height,
  activityRowOffset = 0,
  CellComponent,
  rowCoordinates,
  onEditMultipleTimeEntries,
}: ActivityRowProps) => {
  const [, setIsRowRefReady] = React.useState(false);
  const rowRef = React.useRef<HTMLDivElement>(null);

  React.useEffect(() => {
    if (rowRef.current) {
      setIsRowRefReady(true);
    }
  }, []);

  return (
    <TableRow ref={rowRef} rowHeight={height || TIME_ENTRY_ROW_HEIGHT}>
      {workMonth.dates.map((workDate, index) => {
        const cell = row.cells[index];
        const cellElement = rowRef.current?.children.item(index) as Element;

        return (
          <CellComponent
            key={workDate.date}
            workDate={workDate}
            cell={cell}
            offset={activityRowOffset}
            onEditAllEntries={(): void => {
              onEditMultipleTimeEntries(
                cell.entries,
                cellElement,
                workDate.date
              );
            }}
            rowCoordinates={rowCoordinates}
          />
        );
      })}
    </TableRow>
  );
};

export default ActivityRow;
