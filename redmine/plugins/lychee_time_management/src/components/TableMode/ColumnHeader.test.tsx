import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { RecoilRoot } from 'recoil';

import drag from '@/lib/test/drag';
import { RecoilObserver } from '@/lib/test/RecoilObserver';
import { ColumnHeader } from './ColumnHeader';
import { cellWidths } from '@/recoil/cellWidths';

describe('ColumnHeader', () => {
  test('dragging updates the cellWidths state', async () => {
    const cellName = 'issueNameCellWidth';
    const onChange = jest.fn();
    render(
      <RecoilRoot>
        <RecoilObserver node={cellWidths} onChange={onChange} />
        <ColumnHeader cellName={cellName} />
      </RecoilRoot>
    );

    const handle = screen.getByTestId('resize-handle');
    await drag(handle, { delta: { x: 500, y: 0 } });

    expect(
      onChange.mock.calls[onChange.mock.calls.length - 1][0][cellName]
    ).toEqual(500);
  });
})
