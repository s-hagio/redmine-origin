import Tippy from '@tippyjs/react';
import * as React from 'react';
import styled from 'styled-components';

import addBlueImage from '@/assets/icn_add_blue@2x.png';
import { MapById } from '@/lib/tableMode/mapById';
import { sortActivities } from '@/lib/tableMode/rowSorter';
import { Activity } from '@/lib/tableMode/types';
import ImageButton from './atoms/ImageButton';
import { match } from 'ts-pattern';

const ActivityMenu = styled.div.attrs({
  role: 'menu',
  ['aria-label']: '作業分類の選択肢',
})`
  border-radius: 4px;
  background-color: white;
  line-height: 1.3;
  max-height: 170px;
  overflow: auto;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.12), 0px 4px 3px rgba(0, 0, 0, 0.14),
    0px 3px 3px -2px rgba(0, 0, 0, 0.2);
`;

const ActivityItem = styled.div.attrs({ role: 'menuitem' })`
  font-size: 12px;
  color: #555555;
  font-weight: normal;
  cursor: pointer;
  padding: 10px;
  &:hover {
    background-color: #efefef;
  }
`;

type TooltipPosition = 'top' | 'bottom';

// https://zenn.dev/catnose99/articles/26bd8dac9ea5268486c8
const Tooltip = styled.span.attrs<{
  position: TooltipPosition;
}>((props) => ({
  style: {
    '--translate-y': match(props.position)
      .with('top', () => '-120%')
      .with('bottom', () => '120%')
      .exhaustive(),
  },
}))<{ position: TooltipPosition }>`
  opacity: 0;
  visibility: hidden;
  position: absolute;
  transform: translateY(
    var(--translate-y)
  ); /* bottomで移動させるとどこかへ行ってしまうのでtransformで移動させる */
  display: inline-block;
  padding: 3px;
  white-space: nowrap;
  font-size: 11px;
  line-height: 1.3;
  background: #333;
  color: #fff;
  border-radius: 3px;
  transition: 0.3s ease-in;
`;

const FilledCircleWrapper = styled.div<{ isVisible: boolean }>`
  display: ${(props) => (props.isVisible ? 'flex' : 'none')};
  width: 24px;
  height: 24px;
  border-radius: 50%;
  background-color: #e4e4e4;
  justify-content: center;
  align-items: center;

  :hover {
    ${Tooltip} {
      opacity: 0.8;
      visibility: visible;
    }
  }
`;

type AddActivityButtonProps = {
  activitiesById: MapById<Activity>;
  activities: Activity[];
  onSelectActivity: (activityId: number) => void;
  isVisibleButton?: boolean;
  isLast?: boolean;
};

const AddActivityButton: React.FC<AddActivityButtonProps> = ({
  activitiesById,
  activities,
  onSelectActivity,
  isVisibleButton = false,
  isLast = false,
}: AddActivityButtonProps) => {
  const [menuVisible, setMenuVisible] = React.useState<boolean>(false);
  const sortedActivities = React.useMemo(
    () => sortActivities(activities, activitiesById),
    [activities, activitiesById]
  );
  const handleClick = React.useCallback(() => {
    setMenuVisible(true);
  }, []);
  const handleClickOutside = React.useCallback(() => {
    setMenuVisible(false);
  }, []);
  const handleSelectActivity = React.useCallback(
    (activityId: number): void => {
      setMenuVisible(false);
      onSelectActivity(activityId);
    },
    [onSelectActivity]
  );

  React.useEffect(() => {
    // ホバーが外れてボタンが隠れた際に、Tippyのメニュー自体も隠す
    if (!isVisibleButton) {
      requestAnimationFrame(() => {
        setMenuVisible(false);
      });
    }
  }, [isVisibleButton]);

  if (activities.length === 0) {
    return null;
  }

  return (
    <Tippy
      interactive
      arrow={false}
      placement="bottom"
      offset={[0, 0]}
      content={
        // Tippyのvisibleがfalseになっても、すぐに消えないここも消しておく
        menuVisible && (
          <ActivityMenu>
            {sortedActivities.map((activity) => (
              <ActivityItem
                key={activity.id}
                onClick={(): void => {
                  handleSelectActivity(activity.id);
                }}
              >
                {activity.name}
              </ActivityItem>
            ))}
          </ActivityMenu>
        )
      }
      visible={menuVisible}
      onClickOutside={handleClickOutside}
    >
      <FilledCircleWrapper isVisible={isVisibleButton}>
        <Tooltip position={isLast ? 'top' : 'bottom'}>作業分類を追加</Tooltip>
        <ImageButton
          tabIndex={-1}
          src={addBlueImage}
          aria-label="作業分類を追加"
          onClick={handleClick}
          height={18}
          width={18}
        />
      </FilledCircleWrapper>
    </Tippy>
  );
};

export default React.memo(AddActivityButton);
