import {
  HOURS_CELL_RIGHT_PADDING,
  TIME_ENTRY_ROW_HEIGHT,
} from '@/lib/tableMode/cellStyle';
import * as React from 'react';
import styled from 'styled-components';

import TableCell from './atoms/TableCell';
import TableRow from './atoms/TableRow';

const Root = styled(TableRow)`
  display: flex;
  justify-content: space-between;
  width: 100%;
  border-top: none;
  border-left: none;
`;

const RowHeader = styled(TableCell).attrs({ role: 'rowheader' })``;

export const PrimaryRowLeftLabelCell = styled(RowHeader)`
  display: flex;
  justify-content: space-between;
  padding-left: 1em;
  font-size: 13px;
  font-weight: bold;
`;

export const PrimaryRowRightLabelCell = styled(TableCell)`
  display: flex;
  justify-content: flex-end;
  padding-left: 1em;
  border-left: none;
  padding-right: ${HOURS_CELL_RIGHT_PADDING}px;
`;

type PrimaryRowHeaderProps = {
  className?: string;
  label?: string;
  rightLabel?: string;
};

const PrimaryRowHeader: React.FC<PrimaryRowHeaderProps> = ({
  className,
  label,
  rightLabel,
}: PrimaryRowHeaderProps) => {
  return (
    <Root className={className} rowHeight={TIME_ENTRY_ROW_HEIGHT}>
      <PrimaryRowLeftLabelCell role="rowheader">{label}</PrimaryRowLeftLabelCell>
      <PrimaryRowRightLabelCell aria-label="合計">
        {rightLabel}
      </PrimaryRowRightLabelCell>
    </Root>
  );
};

export default PrimaryRowHeader;
