import * as React from 'react';
import styled from 'styled-components';

import {
  PROJECT_ROW_STYLE,
  TIME_ENTRY_ROW_HEIGHT,
} from '@/lib/tableMode/cellStyle';
import { MapById } from '@/lib/tableMode/mapById';
import { ProjectData } from '@/lib/tableMode/tableData';
import {
  Activity,
  Issue,
  IssueRowAppearance,
  Project,
} from '@/lib/tableMode/types';

import TableCell from './atoms/TableCell';
import TableRowGroup from './atoms/TableRowGroup';

import ActivityRowHeader from './ActivityRowHeader';
import IssueRowHeaders from './IssueRowHeaders';
import ProjectRowHeader from './ProjectRowHeader';
import { sortActivities } from '@/lib/tableMode/rowSorter';
import HoverableRow from './HoverableRow';

const RowHeader = styled(TableCell).attrs({ role: 'rowheader' })``;

const ProjectActivitiesRowsHeader = styled(TableRowGroup)``;

const ProjectActivitiesLabelHoverableRow = styled(HoverableRow)`
  padding-left: 24px;
`;

export const ProjectActivitiesLabelCell = styled(RowHeader)`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  height: ${TIME_ENTRY_ROW_HEIGHT}px;
  font-size: 13px;
  padding-left: 0px;
`;

export const ProjectActivitiesEmptyCell = styled(RowHeader)``;

const ProjectSumRow = styled(ProjectRowHeader)``;

const ProjectSumRowGroup = styled(TableRowGroup)`
  ${PROJECT_ROW_STYLE};

  &&& ${ProjectSumRow} {
    height: 100%;
  }
`;

type ProjectRowHeadersProps = {
  project: Project;
  projectData: ProjectData;
  activities: MapById<Activity>;
  issues: MapById<Issue>;
  issueRowAppearances: MapById<IssueRowAppearance>;
  onAddIssueActivity: (issueActivity: {
    issueId: number;
    activityId: number;
  }) => void;
  onAddActivity: (activityId: number) => void;
  timeEntryRequiredFields: string[];
};

const ProjectRowHeaders: React.FC<ProjectRowHeadersProps> = ({
  project,
  projectData,
  activities,
  issues,
  issueRowAppearances,
  onAddIssueActivity,
  onAddActivity,
  timeEntryRequiredFields,
}: ProjectRowHeadersProps) => {
  const unusedActivities = React.useMemo(() => {
    const usedActivities = projectData.activityRows.map((row) =>
      activities.get(row.id)
    ) as Activity[];

    return activities
      .valueSeq()
      .toArray()
      .filter(
        (activity) => !usedActivities.includes(activity) && activity.active
      );
  }, [activities, projectData.activityRows]);

  const sortedActivities = React.useMemo(
    () => sortActivities(projectData.activityRows, activities),
    [projectData.activityRows, activities]
  );

  return (
    <>
      <ProjectSumRowGroup aria-label={project.name}>
        <ProjectSumRow
          project={project}
          timeEntrySum={projectData.sum}
          activitiesById={activities}
          availableActivities={unusedActivities}
          onAddActivity={onAddActivity}
          allowInsertingRows={
            project.active && !timeEntryRequiredFields.includes('issue_id')
          }
        />
      </ProjectSumRowGroup>
      {sortedActivities.length > 0 && (
        <ProjectActivitiesRowsHeader
          aria-label={`${project.name}のプロジェクト工数`}
          height={sortedActivities.length * TIME_ENTRY_ROW_HEIGHT}
          cellsBeforeRows={
            <ProjectActivitiesLabelHoverableRow
              projectId={projectData.id}
              activityId={sortedActivities[0].id}
            >
              <ProjectActivitiesLabelCell>
                プロジェクト工数
              </ProjectActivitiesLabelCell>
            </ProjectActivitiesLabelHoverableRow>
          }
        >
          {sortedActivities.map((activityRow) => {
            const activity = activities.get(activityRow.id) as Activity;

            return (
              <HoverableRow
                key={activity.id}
                projectId={projectData.id}
                activityId={activityRow.id}
              >
                <ActivityRowHeader
                  activity={activity}
                  rowHeight={TIME_ENTRY_ROW_HEIGHT}
                  timeEntrySum={activityRow.sum}
                />
              </HoverableRow>
            );
          })}
        </ProjectActivitiesRowsHeader>
      )}
      {projectData.dataForIssues.map((issueData, i) => {
        const issue = issues.get(issueData.id) as Issue;
        const issueRowAppearance = issueRowAppearances.get(
          issueData.id
        ) as IssueRowAppearance;
        const isLast = i === projectData.dataForIssues.length - 1;

        return (
          <IssueRowHeaders
            key={issue.id}
            issue={issue}
            issues={issues}
            issueData={issueData}
            isLastIssue={isLast}
            activities={activities}
            rowHeight={issueRowAppearance.rowHeight}
            activityRowOffset={issueRowAppearance.activityRowOffset}
            onAddActivity={(activityId): void =>
              onAddIssueActivity({ issueId: issueData.id, activityId })
            }
            allowInsertingRows={project.active}
          />
        );
      })}
    </>
  );
};

export default ProjectRowHeaders;
