import { initialKey as initialSortKey } from '@/components/TableMode/SortKeySelector';
import * as moment from 'moment';
import * as React from 'react';
import * as ReactModal from 'react-modal';
import {
  Flip as ToastFlipTransition,
  toast,
  ToastContainer,
} from 'react-toastify';

import {
  AttachmentApiHandler,
  CustomField,
} from '@/components/RedmineCustomField';
import { Labels, LabelsProvider } from '@/lib/tableMode/labels';
import { buildMapById, MapById } from '@/lib/tableMode/mapById';
import { sortIssueData, sortProjectData } from '@/lib/tableMode/rowSorter';
import { buildTableData, tableDataReducer } from '@/lib/tableMode/tableData';
import {
  initTimeEntries,
  timeEntriesReducer,
} from '@/lib/tableMode/timeEntries';
import {
  Activity,
  CurrentUser,
  IndirectTimeEntry,
  Issue,
  IssueTimeEntry,
  Permission,
  Project,
  ProjectTimeEntry,
  TargetUser,
  TimeEntry,
  TimeEntryData,
  TimeEntryId,
  Tracker,
  UpdatableTimeEntryAttributes,
  WorkMonth,
} from '@/lib/tableMode/types';

import LoadingOverlay from './atoms/LoadingOverlay';

import type { GroupedFilterOptions } from '@/components/TableMode/FilterTable/types';
import { useProjectSelector } from '@/hooks/useProjectSelector';
import { useDataCellsUpdator } from '@/recoil/focusableContainerState';
import type {
  AvailableFilters,
  Filter,
  OperatorsByFilterType,
  OperatorsLabels,
  Query,
} from './FilterTable';
import SelectIssueForm from './SelectIssueForm';
import Table from './Table';
import TopNav from './TopNav';

export type TimeEntryDataStore = {
  getForMonth: (workMonth: WorkMonth, userId: number) => Promise<TimeEntryData>;
  createTimeEntry: (
    timeEntry: Omit<TimeEntry, 'id' | 'hours'> & { hours: string },
    userId: number
  ) => Promise<TimeEntry>;
  updateTimeEntry: (
    id: TimeEntryId,
    attributes: Partial<UpdatableTimeEntryAttributes>
  ) => Promise<TimeEntry>;
  deleteTimeEntry: (id: TimeEntryId) => Promise<void>;
  getCustomFieldsForProject: (projectId: number) => Promise<CustomField[]>;
  getProjectFilters: () => Promise<{
    groupedFilterOptions: GroupedFilterOptions;
    availableFilters: AvailableFilters;
    operatorsByFilterType: OperatorsByFilterType;
    operatorsLabels: OperatorsLabels;
    queries: Query[];
  }>;
  getFilterValues: (
    type: 'ProjectQuery' | 'IssueQuery',
    name: string
  ) => Promise<{ values: [string, string][] }>;
  getProjects: ({
    userId,
    queryId,
    filters,
    page,
  }: {
    userId: number;
    queryId: string;
    filters: Filter[];
    page: number;
  }) => Promise<{
    projects: Project[];
    queriedFilters: Filter[];
    totalCount: number;
    offset: number;
    limit: number;
  }>;
  getAvailableFilters: () => Promise<{
    groupedFilterOptions: GroupedFilterOptions;
    availableFilters: AvailableFilters;
    operatorsByFilterType: OperatorsByFilterType;
    operatorsLabels: OperatorsLabels;
    queries: Query[];
  }>;
  getFilteredIssues: (
    filters: Filter[],
    page: number,
    queryId: string,
    userId: number
  ) => Promise<{
    queriedFilters: Filter[];
    issues: Issue[];
    ancestorIssues: Issue[];
    projects: Project[];
    trackers: Tracker[];
    totalCount: number;
    offset: number;
    limit: number;
  }>;
  getProjectActivities: (projectIds: string) => Promise<{
    activities: Activity[];
  }>;
  getInheritProjects: (userId: number) => Promise<{
    projects: Project[];
    activities: Activity[];
    project_activities: any[];
  }>;
};

export type WorkMonthProvider = {
  getCurrent: () => WorkMonth;
  getNextMonth: (workMonth: WorkMonth) => WorkMonth;
  getPreviousMonth: (workMonth: WorkMonth) => WorkMonth;
  getToday: () => string;
};

type RootProps = {
  currentUser: CurrentUser;
  targetUsers: TargetUser[];
  timeEntryRequiredFields: string[];
  requiredCustomFieldExists: boolean;
  indirectTimeEntryPermissions: Permission[];
  timeEntryDataStore: TimeEntryDataStore;
  workMonthProvider: WorkMonthProvider;
  attachmentApi: AttachmentApiHandler;
};

const Root: React.FC<RootProps> = ({
  timeEntryRequiredFields,
  requiredCustomFieldExists,
  currentUser,
  targetUsers,
  indirectTimeEntryPermissions,
  timeEntryDataStore,
  workMonthProvider,
  attachmentApi,
}: RootProps) => {
  React.useEffect(() => {
    moment.locale(currentUser.locale);
  }, [currentUser.locale]);

  const [isFocusTable, setFocusTable] = React.useState(true);

  const [targetUserId, setTargetUserId] = React.useState<number>(
    currentUser.id
  );

  const [workMonth, setWorkMonth] = React.useState(
    workMonthProvider.getCurrent()
  );

  const [issueSortKey, setIssueSortKey] = React.useState(initialSortKey());

  const [loading, setLoading] = React.useState(false);

  const loadAndHandleErrors = React.useCallback(
    async (request: () => Promise<void>): Promise<void> => {
      try {
        setLoading(true);
        await request();
        setLoading(false);
      } catch (error: any) {
        toast(error.error, { type: 'error' });
        setLoading(false);
        throw error;
      }
    },
    []
  );

  const [tableData, dispatchForTableData] = React.useReducer(
    tableDataReducer,
    buildTableData()
  );

  const [timeEntries, dispatchForTimeEntries] = React.useReducer(
    timeEntriesReducer,
    initTimeEntries()
  );

  // create/update/deleteTimeEntryの参照が変わらないために、
  // 現在のtimeEntriesによるtableDataへの変更は間接的に起こす
  React.useEffect(() => {
    if (timeEntries.tableDataUpdate) {
      dispatchForTableData(timeEntries.tableDataUpdate);
    }
  }, [timeEntries.tableDataUpdate]);

  const [issuesById, setIssuesById] = React.useState<MapById<Issue>>(
    buildMapById([])
  );
  const [projectsById, setProjectsById] = React.useState<MapById<Project>>(
    buildMapById([])
  );
  const [activitiesById, setActivitiesById] = React.useState<MapById<Activity>>(
    buildMapById([])
  );
  const [indirectActivitiesById, setIndirectActivitiesById] = React.useState<
    MapById<Activity>
  >(buildMapById([]));

  const sortedProjects = React.useMemo(
    () =>
      sortProjectData(tableData.dataForProjects, projectsById).map((data) => ({
        ...data,
        dataForIssues: sortIssueData(
          data.dataForIssues,
          issuesById,
          issueSortKey
        ),
      })),
    [tableData.dataForProjects, projectsById, issuesById, issueSortKey]
  );

  const { resetDataCellsState, saveDataCellInfo } = useDataCellsUpdator();

  const loadTableData = React.useCallback(
    async (
      targetUserId: number,
      timeEntryDataStore: TimeEntryDataStore,
      workMonth: WorkMonth
    ) => {
      setLoading(true);
      const data = await timeEntryDataStore.getForMonth(
        workMonth,
        targetUserId
      );
      // TODO: Optimize re-render
      dispatchForTableData({ type: 'clear' });
      dispatchForTimeEntries({ type: 'init', timeEntries: data.timeEntries });
      setIssuesById(buildMapById(data.issues));
      setProjectsById(buildMapById(data.projects));
      setActivitiesById(buildMapById(data.activities));
      setIndirectActivitiesById(buildMapById(data.indirectActivities));
      dispatchForTableData({
        type: 'init',
        timeEntries: data.timeEntries,
        issues: data.issues,
        assignedIssueIds: data.assignedIssueIds,
        workingHours: data.workingHours,
        workMonth,
      });
      setLoading(false);
    },
    []
  );

  React.useEffect(() => {
    loadTableData(targetUserId, timeEntryDataStore, workMonth);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const mustResetFocus = React.useRef(true);
  React.useEffect(() => {
    resetDataCellsState({
      sortedProjects,
      activitiesById,
      workMonth,
      indirectActivities: tableData.indirectActivities,
      indirectActivitiesById,
      today: workMonthProvider.getToday(),
      mustResetFocus,
    });
  }, [
    activitiesById,
    indirectActivitiesById,
    resetDataCellsState,
    sortedProjects,
    tableData.indirectActivities,
    workMonth,
    workMonthProvider,
  ]);

  const createTimeEntry = React.useCallback(
    async (
      createdData: Omit<TimeEntry, 'id' | 'hours'> & { hours: string },
      userId: number
    ): Promise<void> =>
      loadAndHandleErrors(async (): Promise<void> => {
        const created = await timeEntryDataStore.createTimeEntry(
          createdData,
          userId
        );
        dispatchForTimeEntries({ type: 'set', timeEntry: created });
      }),
    [loadAndHandleErrors, timeEntryDataStore]
  );

  const updateTimeEntry = React.useCallback(
    async (
      id: TimeEntryId,
      attributes: Partial<UpdatableTimeEntryAttributes>
    ): Promise<void> =>
      loadAndHandleErrors(async (): Promise<void> => {
        const updated = await timeEntryDataStore.updateTimeEntry(
          id,
          attributes
        );
        dispatchForTimeEntries({ type: 'set', timeEntry: updated });
      }),
    [loadAndHandleErrors, timeEntryDataStore]
  );

  const deleteTimeEntry = React.useCallback(
    async (id: TimeEntryId): Promise<void> =>
      loadAndHandleErrors(async (): Promise<void> => {
        await timeEntryDataStore.deleteTimeEntry(id);
        dispatchForTimeEntries({ type: 'remove', id });
      }),
    [loadAndHandleErrors, timeEntryDataStore]
  );

  const addIssueActivity = React.useCallback(
    (activity: IssueTimeEntry['activity']) => {
      saveDataCellInfo().then(() =>
        dispatchForTableData({
          type: 'addIssueActivity',
          activity,
        })
      );
    },
    [saveDataCellInfo]
  );

  const addProjectActivity = React.useCallback(
    (activity: ProjectTimeEntry['activity']) => {
      saveDataCellInfo().then(() =>
        dispatchForTableData({
          type: 'addProjectActivity',
          activity,
        })
      );
    },
    [saveDataCellInfo]
  );

  const addIndirectActivity = React.useCallback(
    (activity: IndirectTimeEntry['activity']) => {
      saveDataCellInfo().then(() =>
        dispatchForTableData({
          type: 'addIndirectActivity',
          activity,
        })
      );
    },
    [saveDataCellInfo]
  );

  const addProjectRows = React.useCallback(
    (projectIds: Project['id'][]): void => {
      saveDataCellInfo().then(() =>
        dispatchForTableData({ type: 'addProjectRows', projectIds })
      );
    },
    [saveDataCellInfo]
  );

  const addIssueRows = React.useCallback(
    (issueIds: Issue['id'][]): void => {
      saveDataCellInfo().then(() => {
        const issueProjectIds: { [id: number]: number } = {};
        issueIds.forEach((id) => {
          const issue = issuesById.get(id);
          if (issue) {
            issueProjectIds[id] = issue.projectId;
          }
        });
        dispatchForTableData({ type: 'addIssueRows', issueProjectIds });
      });
    },
    [issuesById, saveDataCellInfo]
  );

  const appRootRef = React.useRef<HTMLDivElement>(null);
  React.useEffect(() => {
    if (appRootRef.current) {
      ReactModal.setAppElement(appRootRef.current);
    }
  }, []);
  const [issueSearchModalOpen, setIssueSearchModalOpen] =
    React.useState<boolean>(false);

  const mergeProjectsByIds = React.useCallback((projects: Project[]) => {
    setProjectsById((prev) => prev.merge(buildMapById(projects)));
  }, []);

  const updateActivitiesById = async (
    projectIds: number[],
    issueIds: number[]
  ) => {
    const issues = issueIds.map((i) => issuesById.get(i));
    const issueProjectIds = issues.map((i) => (i ? i.projectId : null));
    const ids = projectIds.join(',') + issueProjectIds.join(',');
    const { activities } = await timeEntryDataStore.getProjectActivities(ids);
    setActivitiesById(activitiesById.merge(buildMapById(activities)));
  };

  const onClickProjectSelectorButton = React.useCallback(
    () => setFocusTable(false),
    []
  );

  const onCloseProjectSelectorModal = React.useCallback(
    () => setFocusTable(true),
    []
  );

  const {
    renderButton: renderProjectSelectorButton,
    renderModal: renderProjectSelectorModal,
  } = useProjectSelector(
    targetUserId,
    tableData.dataForProjects.map(({ id }) => id),
    mergeProjectsByIds,
    addProjectRows,
    updateActivitiesById,
    onClickProjectSelectorButton,
    onCloseProjectSelectorModal
  );

  const inheritLastMonth = async () => {
    setLoading(true);
    const { projects, activities, project_activities } =
      await timeEntryDataStore.getInheritProjects(targetUserId);
    setProjectsById(projectsById.merge(buildMapById(projects)));
    setActivitiesById(activitiesById.merge(buildMapById(activities)));
    project_activities.forEach((pa) => {
      addProjectRows([pa.project_id]);
      addProjectActivity({
        projectId: pa.project_id,
        activityId: pa.activity_id,
      });
    });
    setLoading(false);
  };

  const handleChangeWorkMonth = React.useCallback(
    async (nextWorkMonth: WorkMonth) => {
      mustResetFocus.current = true;
      await loadTableData(targetUserId, timeEntryDataStore, nextWorkMonth);
      setWorkMonth(nextWorkMonth);
    },
    [loadTableData, targetUserId, timeEntryDataStore]
  );

  const handleChangeTargetUserId = React.useCallback(
    async (nextTargetUserId: number) => {
      mustResetFocus.current = true;
      await loadTableData(nextTargetUserId, timeEntryDataStore, workMonth);
      setTargetUserId(nextTargetUserId);
    },
    [loadTableData, timeEntryDataStore, workMonth]
  );

  return (
    <div style={{ position: 'relative' }}>
      <div ref={appRootRef}>
        <div style={{ flex: 1 }}>
          <TopNav
            workMonthProvider={workMonthProvider}
            onChangeWorkMonth={handleChangeWorkMonth}
            currentUser={currentUser}
            targetUserId={targetUserId}
            targetUsers={targetUsers}
            onChangeTargetUserId={handleChangeTargetUserId}
            issueSortKey={issueSortKey}
            onChageIssueSortKey={setIssueSortKey}
          />
        </div>
        {loading && <LoadingOverlay />}
        <Table
          isFocusTable={isFocusTable}
          targetUserId={targetUserId}
          workMonth={workMonth}
          currentMonth={
            workMonth.month === workMonthProvider.getCurrent().month
          }
          timeEntryRequiredFields={timeEntryRequiredFields}
          requiredCustomFieldExists={requiredCustomFieldExists}
          indirectTimeEntryPermissions={indirectTimeEntryPermissions}
          data={tableData}
          timeEntries={timeEntries.records}
          createTimeEntry={createTimeEntry}
          updateTimeEntry={updateTimeEntry}
          deleteTimeEntry={deleteTimeEntry}
          addIssueActivity={addIssueActivity}
          addProjectActivity={addProjectActivity}
          addIndirectActivity={addIndirectActivity}
          getCustomFieldsForProject={
            timeEntryDataStore.getCustomFieldsForProject
          }
          issuesById={issuesById}
          projectsById={projectsById}
          activitiesById={activitiesById}
          indirectActivitiesById={indirectActivitiesById}
          attachmentApi={attachmentApi}
          openIssueSearchModal={(): void => {
            setIssueSearchModalOpen(true);
            setFocusTable(false);
          }}
          renderProjectSelectorButton={renderProjectSelectorButton}
          inheritLastMonth={inheritLastMonth}
          sortedProjects={sortedProjects}
        />
        {renderProjectSelectorModal()}
        <ReactModal
          isOpen={issueSearchModalOpen}
          contentLabel="チケット選択"
          onRequestClose={(): void => {
            setIssueSearchModalOpen(false);
            setFocusTable(true);
          }}
          style={{
            overlay: { backgroundColor: 'rgba(0, 0, 0, 0.75)', zIndex: 3 },
            content: {
              top: 'calc(50% - 300px)',
              bottom: 'calc(50% - 300px)',
              padding: '20px 30px',
            },
          }}
        >
          <SelectIssueForm
            issuesById={issuesById}
            targetUserId={targetUserId}
            onAddIssues={(issueProjectIds): void => {
              addIssueRows(issueProjectIds);
              setIssueSearchModalOpen(false);
              setFocusTable(true);
            }}
            onCancel={(): void => {
              setIssueSearchModalOpen(false);
              setFocusTable(true);
            }}
            getAvailableFilters={timeEntryDataStore.getAvailableFilters}
            getFilteredIssues={timeEntryDataStore.getFilteredIssues}
            onDataFetched={({ issues, projects }): void => {
              setProjectsById(projectsById.merge(buildMapById(projects)));
              setIssuesById(issuesById.merge(buildMapById(issues)));
            }}
            updateActivitiesById={updateActivitiesById}
          />
        </ReactModal>
        <ToastContainer
          position="bottom-left"
          autoClose={10000}
          hideProgressBar
          transition={ToastFlipTransition}
        />
      </div>
    </div>
  );
};

export default function RootWrapper(props: RootProps & { labels: Labels }) {
  const { labels, ...restProps } = props;
  return (
    <LabelsProvider value={labels}>
      <Root {...restProps} />
    </LabelsProvider>
  );
}
