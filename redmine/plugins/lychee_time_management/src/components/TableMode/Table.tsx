import Tippy from '@tippyjs/react';
import { throttle } from 'lodash';
import * as React from 'react';
import { useRecoilValue } from 'recoil';
import styled from 'styled-components';
import type { LifecycleHooks } from 'tippy.js';

import {
  Attachment,
  AttachmentApiHandler,
  CustomField,
} from '@/components/RedmineCustomField';
import { insideDatepicker } from '@/components/RedmineCustomField/inputComponents/DateFieldInput';
import { useFormatHours } from '@/hooks/useFormatHours';
import {
  backgroundColorOfDate,
  CELL_RIGHT_MARGIN,
  LABEL_CELL_LEFT_PADDING,
  TABLE_BORDER_STYLE,
  TABLE_HEADER_ROW_HEIGHT,
  TIME_ENTRY_CELL_STYLE,
  TIME_ENTRY_CELL_WIDTH,
  TIME_ENTRY_ROW_HEIGHT,
  TOP_EMPTY_AREA_ROW_HEIGHT,
} from '@/lib/tableMode/cellStyle';
import { useLabels } from '@/lib/tableMode/labels';
import { buildMapById, MapById } from '@/lib/tableMode/mapById';
import { ProjectData, TableData } from '@/lib/tableMode/tableData';
import {
  Activity,
  AttachmentApiHandlerWithMemory,
  IndirectTimeEntry,
  Issue,
  IssueTimeEntry,
  Permission,
  Project,
  ProjectTimeEntry,
  RowCoordinates,
  TimeEntry,
  TimeEntryCoordinates,
  TimeEntryId,
  UpdatableTimeEntryAttributes,
  WorkDate,
  WorkMonth,
} from '@/lib/tableMode/types';

import TableCell from './atoms/TableCell';
import TableRow from './atoms/TableRow';
import { RowContainerCell } from './atoms/TableRowGroup';

import {
  CellWidths,
  cellWidths as cellWidthsState,
  rowHeaderWidth as rowHeaderWidthState,
} from '@/recoil/cellWidths';
import {
  useDataCellsFocusableContainer,
  useFocusableCell,
} from '@/recoil/focusableContainerState';
import { ThemeStyle, themeStyleState } from '@/recoil/themeStyleState';
import ReactFocusLock from 'react-focus-lock';
import { match } from 'ts-pattern';
import { CellComponentType } from './ActivityRow';
import {
  ActivityNameCellWrapper,
  ActivityRowSumCell,
} from './ActivityRowHeader';
import PlusButton from './atoms/PlusButton';
import { ColumnHeader } from './ColumnHeader';
import DateHeader from './DateHeader';
import EditableCell from './EditableCell';
import IndirectActivityRowHeaders, {
  IndirectActivitiesEmptyCellForActivities,
  IndirectActivitiesEmptyCellForSum,
  IndirectActivitiesLabelCell,
} from './IndirectActivityRowHeaders';
import IndirectActivityRows from './IndirectActivityRows';
import {
  IssueActivityEmptyCell,
  IssueEstimatedHoursCell,
  IssueNameCell,
  IssueStatusCell,
  IssueSumEmptyCell,
  rowHeight,
} from './IssueRowHeader';
import PrimaryRowHeader, {
  PrimaryRowLeftLabelCell,
  PrimaryRowRightLabelCell,
} from './PrimaryRowHeader';
import ProjectDataRows from './ProjectDataRows';
import { ProjectNameCell, ProjectRowSumCell } from './ProjectRowHeader';
import ProjectRowHeaders, {
  ProjectActivitiesEmptyCell,
  ProjectActivitiesLabelCell,
} from './ProjectRowHeaders';
import TimeEntriesEditor from './TimeEntriesEditor';
import { SelectorContainerClassName } from './atoms/Selector';

type TableProps = {
  isFocusTable: boolean;
  targetUserId: number;
  data: TableData;
  timeEntries: MapById<TimeEntry>;
  workMonth: WorkMonth;
  currentMonth: boolean;
  createTimeEntry: (
    createdData: Omit<TimeEntry, 'id' | 'hours'> & { hours: string },
    userId: number
  ) => Promise<void>;
  deleteTimeEntry: (id: TimeEntryId) => Promise<void>;
  updateTimeEntry: (
    id: TimeEntryId,
    attributes: Partial<UpdatableTimeEntryAttributes>
  ) => Promise<void>;
  addIssueActivity: (activity: IssueTimeEntry['activity']) => void;
  addProjectActivity: (activity: ProjectTimeEntry['activity']) => void;
  addIndirectActivity: (activity: IndirectTimeEntry['activity']) => void;
  getCustomFieldsForProject: (projectId: number) => Promise<CustomField[]>;
  timeEntryRequiredFields: string[];
  requiredCustomFieldExists: boolean;
  indirectTimeEntryPermissions: Permission[];
  issuesById: MapById<Issue>;
  projectsById: MapById<Project>;
  activitiesById: MapById<Activity>;
  indirectActivitiesById: MapById<Activity>;
  attachmentApi: AttachmentApiHandler;
  openIssueSearchModal: () => void;
  renderProjectSelectorButton?: () => React.ReactNode;
  inheritLastMonth: () => void;
  sortedProjects: ProjectData[];
};

const PRIMARY_ROW_HEADER_STYLE = `background-color: #3d7085`;

const TableArea = styled.div`
  display: flex;
  flex-wrap: nowrap;
  flex-direction: row;
`;

const MainArea = styled.main<{ windowHeight: number }>`
  overflow: scroll;
  height: ${({ windowHeight }): number => windowHeight - 150}px;
  background-color: white;
`;

const TableHeader = styled(TableArea)<{ tableWidth: number }>`
  align-items: flex-end;
  position: sticky;
  top: 0;
  z-index: 2;
  width: ${({ tableWidth }): string => tableWidth.toString()}px;

  ${TableRow} {
    :last-child {
      border-bottom-width: 0px;
    }
  }
`;

const TableBody = styled(TableArea)<{ tableWidth: number }>`
  align-items: flex-start;
  width: ${({ tableWidth }): string => tableWidth.toString()}px;
`;

const FieldColumnHeader = styled(ColumnHeader)`
  color: #333333;
  height: ${TABLE_HEADER_ROW_HEIGHT}px;
  text-align: left;
  font-size: 11px;
  padding-left: ${LABEL_CELL_LEFT_PADDING}px;
`;

const NameHeader = styled(FieldColumnHeader).attrs(() => ({
  cellName: 'issueNameCellWidth',
}))`
  padding-left: 16px;
`;

const StatusHeader = styled(FieldColumnHeader).attrs(() => ({
  cellName: 'issueStatusCellWidth',
}))``;

const EstimatedHoursHeader = styled(FieldColumnHeader)``;

const ActivityHeader = styled(FieldColumnHeader).attrs(() => ({
  cellName: 'issueActivitiesCellWidth',
}))``;

const TotalSumHeader = styled(FieldColumnHeader)``;

const TotalSumCell = styled(TableCell)``;

const LeftArea = styled.div.attrs({
  role: 'table',
})<{ cellWidths: CellWidths; themeStyle: ThemeStyle }>`
  position: sticky;
  left: 0;
  z-index: 1;
  background-color: ${({ themeStyle }) => themeStyle.contentBackgroundColor};

  ${TableRow} {
    border-right-style: none;
  }

  ${NameHeader} {
    width: ${({ cellWidths }) => cellWidths.issueNameCellWidth}px;
  }

  ${IssueNameCell} {
    width: ${({ cellWidths }) =>
      cellWidths.issueNameCellWidth -
      24}px; // 左端にマージンを空けるのでマイナスしている
  }

  ${ProjectActivitiesLabelCell} {
    width: ${({ cellWidths }) =>
      cellWidths.issueNameCellWidth +
      cellWidths.issueStatusCellWidth +
      cellWidths.issueEstimateHoursCellWidth -
      24}px; // 左端にマージンを空けるのでマイナスしている
  }

  ${StatusHeader}, ${IssueStatusCell} {
    width: ${({ cellWidths }) => cellWidths.issueStatusCellWidth}px;
  }

  ${EstimatedHoursHeader}, ${IssueEstimatedHoursCell} {
    width: ${({ cellWidths }) => cellWidths.issueEstimateHoursCellWidth}px;
  }

  ${ProjectActivitiesEmptyCell} {
    width: ${({ cellWidths }) =>
      cellWidths.issueStatusCellWidth +
      cellWidths.issueEstimateHoursCellWidth}px;
  }

  ${ActivityHeader} {
    width: ${({ cellWidths }) => cellWidths.issueActivitiesCellWidth}px;
  }

  ${ActivityNameCellWrapper} {
    width: ${({ cellWidths }) =>
      cellWidths.issueActivitiesCellWidth}px; // no left border because row group already has border
  }

  ${TotalSumHeader}, ${TotalSumCell}, ${ProjectRowSumCell}, ${ActivityRowSumCell} {
    width: ${({ cellWidths }) => cellWidths.rowSumCellWidth}px;
  }

  ${TotalSumCell} {
    height: ${TABLE_HEADER_ROW_HEIGHT}px;
  }

  ${RowContainerCell} {
    width: ${({ cellWidths }) =>
      cellWidths.issueActivitiesCellWidth + cellWidths.rowSumCellWidth}px;
  }

  ${ProjectNameCell} {
    width: ${({ cellWidths }) =>
      cellWidths.issueNameCellWidth +
      cellWidths.issueStatusCellWidth +
      cellWidths.issueEstimateHoursCellWidth +
      cellWidths.issueActivitiesCellWidth}px;
  }

  ${IndirectActivitiesLabelCell} {
    width: ${({ cellWidths }) =>
      cellWidths.issueNameCellWidth +
      cellWidths.issueStatusCellWidth +
      cellWidths.issueEstimateHoursCellWidth}px;
  }

  ${IndirectActivitiesEmptyCellForActivities} {
    width: ${({ cellWidths }) => cellWidths.issueActivitiesCellWidth}px;
  }

  ${IndirectActivitiesEmptyCellForSum} {
    width: ${({ cellWidths }) => cellWidths.rowSumCellWidth}px;
  }

  ${PrimaryRowRightLabelCell} {
    width: ${({ cellWidths }) => cellWidths.rowSumCellWidth}px;
  }

  ${IssueActivityEmptyCell} {
    width: ${({ cellWidths }) => cellWidths.issueActivitiesCellWidth}px;
  }

  ${IssueSumEmptyCell} {
    width: ${({ cellWidths }) => cellWidths.rowSumCellWidth}px;
  }
`;

const TopLeftHeader = styled(LeftArea)`
  display: flex;
  flex-direction: column;
  width: fit-content;
  position: sticky;
  left: 0;
  z-index: 2;
  border-top: ${TABLE_BORDER_STYLE};
  box-sizing: border-box;

  h2 {
    text-align: center;
    margin-bottom: 0;
  }
`;

const Spacer = styled.div`
  flex-grow: 1;
`;

const TopLeftEmptyArea = styled.div<{ cellWidths: CellWidths }>`
  display: flex;
  align-items: center;
  width: ${({ cellWidths }) =>
    cellWidths.issueNameCellWidth +
    cellWidths.issueStatusCellWidth +
    cellWidths.issueEstimateHoursCellWidth +
    cellWidths.issueActivitiesCellWidth}px;

  & div {
    text-align: left;
  }

  height: ${TOP_EMPTY_AREA_ROW_HEIGHT -
  1}px; // ボーダー分をさっぴくために-1している
`;

const ButtonArea = styled.div`
  padding-right: 10px;
`;

const RightArea = styled.div.attrs({ role: 'rowgroup' })<{ rowWidth: number }>`
  width: fit-content;
  overflow-x: hidden;

  border-left: ${TABLE_BORDER_STYLE};

  ${TableRow} {
    border-left-style: none;
    width: ${({ rowWidth }): string => rowWidth.toString()}px;
  }
`;

const MonthHeaderRow = styled(TableRow)`
  display: flex;
  height: ${TOP_EMPTY_AREA_ROW_HEIGHT}px;
  align-items: center;
  background-color: #fafafa;
`;

const MonthHeader = styled.div<{ days: number }>`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  height: ${TOP_EMPTY_AREA_ROW_HEIGHT}px;
  width: ${({ days }) => days * TIME_ENTRY_CELL_WIDTH}px;
  font-size: 13px;
  color: #333333;
  padding-left: 16px;
  box-sizing: border-box;
  &:nth-child(n + 2) {
    border-left: ${TABLE_BORDER_STYLE};
  } // 2番目以降の月ヘッダには、区切り線を左側につけたいのでこうする
`;

const DateHeaders = styled(RightArea)`
  z-index: 1;
  overflow-x: visible;
`;

const TableHeaderRow = styled(TableRow)`
  background-color: #fafafa;
`;

const PrimaryHeaderRow = styled(TableRow)`
  ${PRIMARY_ROW_HEADER_STYLE};
`;

const TimeEntries = styled(RightArea)`
  overflow-x: visible;
`;

const PrimaryHeaderHoursCell = styled(TableCell).attrs({
  role: 'columnheader',
})`
  display: flex;
  ${TIME_ENTRY_CELL_STYLE}
  color: #FFFFFF;
  height: ${TIME_ENTRY_ROW_HEIGHT}px;
  justify-content: flex-end;
  align-items: center;
  ${CELL_RIGHT_MARGIN};
`;

const WorkingHoursPrimaryDataRow = styled(TableRow)`
  background-color: #ffffdb;

  ${PrimaryHeaderHoursCell} {
    color: #555555;
  }
`;

const WorkingHoursPrimaryRowHeader = styled(PrimaryRowHeader)`
  background-color: #ffffdb;
`;

const AllWorksPrimaryRowHeader = styled(PrimaryRowHeader)`
  ${PRIMARY_ROW_HEADER_STYLE};

  ${PrimaryRowLeftLabelCell} {
    color: #ffffff;
  }

  ${PrimaryRowRightLabelCell} {
    color: #ffffff;
  }
`;

const EditableTimeEntryCell = styled(EditableCell)<{
  workDate: WorkDate;
}>`
  ${backgroundColorOfDate}
  ${TIME_ENTRY_CELL_STYLE}
`;

const EditorPopup = styled.div.attrs({ role: 'tooltip ' })`
  border: 1px solid black;
  background-color: white;
  padding: 10px;
  min-width: 300px;
`;

const IndirectEntriesArea = styled.div`
  border-top: 2px solid #cccccc;
  border-bottom: ${TABLE_BORDER_STYLE};
`;

type EditorState = {
  timeEntryIds: TimeEntryId[];
  popupReferenceElement: Element;
  coordinates: TimeEntryCoordinates;
  customFields: CustomField[];
  permissions: Permission[];
};

const useSyncedScrollPosition = (
  sourceElement: HTMLElement | null,
  targetElement: HTMLElement | null,
  property: 'scrollLeft' | 'scrollTop'
): void => {
  const syncScrollPosition = React.useCallback(
    throttle(() => {
      if (sourceElement && targetElement) {
        targetElement[property] = sourceElement[property];
      }
    }, 10),
    [sourceElement, targetElement]
  );

  React.useEffect((): (() => void) => {
    if (sourceElement && targetElement) {
      sourceElement.addEventListener('scroll', syncScrollPosition);

      return (): void => {
        sourceElement.removeEventListener('scroll', syncScrollPosition);
      };
    }

    return (): void => {};
  }, [sourceElement, syncScrollPosition, targetElement]);
};

const clickedAllowedElement = (event: Event): boolean => {
  const clickedElement = event.target as HTMLElement;

  return insideDatepicker(clickedElement);
};

const rememberUploadedAttachments = (
  attachmentApi: AttachmentApiHandler
): AttachmentApiHandlerWithMemory => {
  const attachments: Attachment[] = [];
  const { createAttachment, ...otherApis } = attachmentApi;
  const clearMemory = (): void => {
    attachments.length = 0;
  };

  return {
    ...otherApis,
    createAttachment: async (file: File): Promise<Attachment> => {
      const created = await createAttachment(file);
      attachments.push(created);
      return created;
    },
    deleteUploadedAttachments: async (): Promise<void> => {
      await Promise.all(
        attachments.map((attachment) =>
          otherApis.deleteAttachment(attachment.id)
        )
      );
      clearMemory();
    },
    clearMemory,
  };
};

const Table: React.FC<TableProps> = ({
  isFocusTable,
  data,
  timeEntries,
  createTimeEntry,
  deleteTimeEntry,
  updateTimeEntry,
  addIssueActivity,
  addProjectActivity,
  addIndirectActivity,
  timeEntryRequiredFields,
  indirectTimeEntryPermissions,
  targetUserId,
  workMonth,
  currentMonth,
  issuesById,
  projectsById,
  activitiesById,
  indirectActivitiesById,
  requiredCustomFieldExists,
  getCustomFieldsForProject,
  attachmentApi,
  openIssueSearchModal,
  renderProjectSelectorButton,
  inheritLastMonth,
  sortedProjects,
}: TableProps) => {
  const tableRef = React.useRef<HTMLDivElement>(null);
  const l = useLabels();
  const formatTimeEntryHours = useFormatHours();
  const [editorState, setEditorState] = React.useState<EditorState | null>(
    null
  );

  const customFieldsForCoordinates = React.useCallback(
    async (coordinates: TimeEntryCoordinates): Promise<CustomField[]> => {
      if (coordinates.activityType === 'indirect') {
        return [];
      }

      let projectId: number;
      if (coordinates.activityType === 'issue') {
        projectId = issuesById.get(coordinates.activity.issueId)
          ?.projectId as number;
      } else {
        projectId = coordinates.activity.projectId;
      }

      const customFields = await getCustomFieldsForProject(projectId);

      return customFields;
    },
    [getCustomFieldsForProject, issuesById]
  );

  const permissionsForCoordinates = React.useCallback(
    (rowCoordinates: RowCoordinates): Permission[] => {
      if (rowCoordinates.activityType === 'indirect') {
        return indirectTimeEntryPermissions;
      }

      const project = projectsById.get(rowCoordinates.activity.projectId);
      return project?.permissions || [];
    },
    [indirectTimeEntryPermissions, projectsById]
  );

  const openEditModal = React.useCallback(
    (
      ids: TimeEntryId[],
      cellElement: Element,
      coordinates: TimeEntryCoordinates
    ): void => {
      (async (): Promise<void> => {
        const customFields = await customFieldsForCoordinates(coordinates);

        setEditorState({
          timeEntryIds: ids,
          popupReferenceElement: cellElement,
          coordinates,
          customFields,
          permissions: permissionsForCoordinates(coordinates),
        });
      })();
    },
    [customFieldsForCoordinates, permissionsForCoordinates]
  );

  const attachmentApiWithMemory = React.useMemo(
    () => rememberUploadedAttachments(attachmentApi),
    [attachmentApi]
  );

  const editedEntries = React.useMemo<TimeEntry[]>(
    () =>
      editorState
        ? (editorState.timeEntryIds
            .map((id) => timeEntries.get(id))
            .filter((entry) => !!entry) as TimeEntry[])
        : [],
    [editorState, timeEntries]
  );

  const CellComponent = React.useMemo<CellComponentType>(() => {
    const DirectlyEditableCell: CellComponentType = ({
      cell,
      workDate,
      rowCoordinates,
      onEditAllEntries,
      offset,
    }) => {
      const [edit, setEdit] = React.useState<boolean>(false);
      const {
        isFocused,
        isEditing,
        toSelectMode,
        toModalMode,
        toEditMode,
        moveFocusByRowCoordinateAndWorkDate,
        moveDown,
        moveRight,
      } = useFocusableCell({
        rowCoordinates,
        workDate,
      });

      const moveFocusToMyCell = React.useCallback(async () => {
        await moveFocusByRowCoordinateAndWorkDate(rowCoordinates, workDate);
      }, [moveFocusByRowCoordinateAndWorkDate, rowCoordinates, workDate]);

      const handleUpdate = React.useCallback(
        async (
          hours: string,
          afterUpdateFocusBehavior?: 'MOVE_DOWN' | 'MOVE_RIGHT'
        ) => {
          try {
            if (cell.entries.length == 0 && hours !== '') {
              const createdEntry = {
                ...rowCoordinates,
                date: workDate.date,
                hours,
              };
              if (createdEntry.activityType === 'issue') {
                // TODO: refactor types
                // @ts-ignore
                delete createdEntry.activity.projectId;
              }
              await createTimeEntry(createdEntry, targetUserId);
            } else if (cell.entries.length == 1) {
              const id = cell.entries[0];
              if (hours !== '') {
                await updateTimeEntry(id, { hours });
              } else {
                await deleteTimeEntry(id);
              }
            }
            setEdit(false);
            toSelectMode();
            match(afterUpdateFocusBehavior)
              .with('MOVE_DOWN', moveDown)
              .with('MOVE_RIGHT', moveRight)
              .with(undefined, () => ({}))
              .exhaustive();
            return true;
          } catch (error) {
            // Don't close edit field
            return false;
          }
        },
        [
          cell.entries,
          toSelectMode,
          moveDown,
          moveRight,
          rowCoordinates,
          workDate.date,
        ]
      );

      const hasNoPermission = React.useMemo(() => {
        const permissions = permissionsForCoordinates(rowCoordinates);
        return permissions.length === 0;
      }, [rowCoordinates]);

      const hasPermissionForDirectEdit = React.useMemo(() => {
        const permissions = permissionsForCoordinates(rowCoordinates);

        if (cell.entries.length == 0) {
          return permissions.includes('create');
        }
        return permissions.includes('edit');
      }, [cell.entries.length, rowCoordinates]);

      const hasPermissionForModal = React.useMemo(() => {
        const permissions = permissionsForCoordinates(rowCoordinates);

        if (cell.entries.length == 0) {
          return permissions.includes('create');
        }
        return permissions.length > 0;
      }, [cell.entries.length, rowCoordinates]);

      const handleOpenModal = React.useCallback(async (): Promise<void> => {
        if (!hasPermissionForModal) {
          return;
        }

        await moveFocusToMyCell();
        onEditAllEntries();
        toModalMode();
      }, [
        hasPermissionForModal,
        moveFocusToMyCell,
        onEditAllEntries,
        toModalMode,
      ]);

      const enterEditMode = React.useCallback(async (): Promise<void> => {
        await moveFocusToMyCell();
        if (hasNoPermission) {
          toSelectMode();
          return;
        }

        const openModal =
          cell.entries.length > 1 ||
          timeEntryRequiredFields.includes('comments') ||
          (rowCoordinates.activityType !== 'indirect' &&
            requiredCustomFieldExists);

        if (openModal || !hasPermissionForDirectEdit) {
          handleOpenModal();
        } else {
          setEdit(true);
          toEditMode();
        }
      }, [
        moveFocusToMyCell,
        hasNoPermission,
        cell.entries.length,
        rowCoordinates.activityType,
        hasPermissionForDirectEdit,
        toSelectMode,
        handleOpenModal,
        toEditMode,
      ]);

      const handleClick: React.MouseEventHandler<HTMLDivElement> =
        React.useCallback(
          (event) => {
            if (edit) {
              // MainAreaのonClickを発火させないため、ここでイベントを止めておく
              event.stopPropagation();
              return;
            }

            enterEditMode();
          },
          [edit, enterEditMode]
        );

      const handleBlur = React.useCallback((): void => {
        setEdit(false);
        toSelectMode();
        tableRef.current?.focus();
      }, [toSelectMode]);

      React.useEffect(() => {
        // コンテナー側でキーの処理をしているため、Recoilの状態がisEdting=trueになったら編集モードに移行する必要がある
        if (!edit && isEditing) {
          enterEditMode();
        }
      }, [isEditing, enterEditMode, edit]);

      return (
        <EditableTimeEntryCell
          onUpdate={handleUpdate}
          workDate={workDate}
          value={cell.sum}
          offset={offset}
          edit={edit}
          onClick={handleClick}
          onBlur={handleBlur}
          onContextMenu={(event): void => {
            event.preventDefault();
            handleOpenModal();
          }}
          isFocused={isFocused}
        />
      );
    };

    return DirectlyEditableCell;
  }, [
    timeEntryRequiredFields,
    createTimeEntry,
    deleteTimeEntry,
    permissionsForCoordinates,
    requiredCustomFieldExists,
    updateTimeEntry,
    targetUserId,
  ]);

  const dateHeadersSectionRef = React.useRef<HTMLDivElement>(null);
  const timeEntriesSectionRef = React.useRef<HTMLDivElement>(null);
  useSyncedScrollPosition(
    timeEntriesSectionRef.current,
    dateHeadersSectionRef.current,
    'scrollLeft'
  );

  const rowWidth = React.useMemo(
    () => workMonth.dates.length * TIME_ENTRY_CELL_WIDTH + 1, // + border-right 1px
    [workMonth.dates.length]
  );

  const cellWidths = useRecoilValue(cellWidthsState);
  const rowHeaderWidth = useRecoilValue(rowHeaderWidthState);

  // mainのスクロール幅が正しくされるために
  // TableHeaderやTableBodyにもwidthを指定する必要がある
  const tableWidth = React.useMemo(
    () => rowHeaderWidth + workMonth.dates.length * TIME_ENTRY_CELL_WIDTH,
    [rowHeaderWidth, workMonth.dates.length]
  );

  const themeStyle = useRecoilValue(themeStyleState);

  // 行の高さは<TableHeader>側の<IssueRowHeader>で決まる．
  // しかしながら，<TableBody>側でもずれなく行の高さを設定するため，
  // この位置で行の高さを計算して<TableHeader>側と<TableBody>側の各要素に渡す．
  const issueRowAppearancesById = React.useMemo(
    () =>
      buildMapById(
        sortedProjects.flatMap((projectData) => {
          return projectData.dataForIssues.map((issueData) => {
            const rh = rowHeight(issueData, issuesById);
            return {
              id: issueData.id,
              rowHeight: rowHeight(issueData, issuesById),
              activityRowOffset: rh - TIME_ENTRY_ROW_HEIGHT,
            };
          });
        })
      ),
    [issuesById, sortedProjects]
  );

  const [windowHeight, setHeight] = React.useState(0);
  React.useLayoutEffect(() => {
    function updateHeight() {
      setHeight(window.innerHeight);
    }
    window.addEventListener('resize', updateHeight);
    updateHeight();
    return () => {
      window.removeEventListener('resize', updateHeight);
    };
  }, []);

  const displayMonths = React.useMemo(() => {
    const months: Record<string, number> = {};
    for (const date of workMonth.dates) {
      const month = date.date.substring(0, 7);
      months[month] = (months[month] ?? 0) + 1;
    }
    return Object.entries(months);
  }, [workMonth]);

  const { handleFocusableContainerKeyDown, isSelectMode, toSelectMode } =
    useDataCellsFocusableContainer();

  const onCloseModal = React.useCallback((): void => {
    (async (): Promise<void> => {
      await attachmentApiWithMemory.deleteUploadedAttachments();
      setEditorState(null);
      toSelectMode();
    })();
  }, [attachmentApiWithMemory, toSelectMode]);

  const onClickOutsideEditorPopup: LifecycleHooks['onClickOutside'] =
    React.useCallback(
      (_, event) => {
        if (clickedAllowedElement(event)) {
          return;
        }

        onCloseModal();
      },
      [onCloseModal]
    );

  const handleDataCellsContainerKeyDown: React.KeyboardEventHandler<HTMLDivElement> =
    React.useCallback(
      async (event) => {
        if (isSelectMode) {
          if (await handleFocusableContainerKeyDown(event)) {
            // キーが処理された場合はデフォルトのイベントハンドリングを抑制する
            event.preventDefault();
          }
        }
      },
      [handleFocusableContainerKeyDown, isSelectMode]
    );

  const mainAreaScrollPaddingTop = React.useMemo(() => {
    return (
      TOP_EMPTY_AREA_ROW_HEIGHT + // 月ヘッダとかの行
      TABLE_HEADER_ROW_HEIGHT + // 日付ヘッダとかの行
      (data.enableWorkingHours ? TIME_ENTRY_ROW_HEIGHT : 0) + // 実労働時間があれば表示される行
      TIME_ENTRY_ROW_HEIGHT // すべての作業の行
    );
  }, [data.enableWorkingHours]);

  const mainAreaScrollPaddingLeft = React.useMemo(
    () =>
      cellWidths.issueActivitiesCellWidth +
      cellWidths.issueEstimateHoursCellWidth +
      cellWidths.issueNameCellWidth +
      cellWidths.issueStatusCellWidth +
      cellWidths.rowSumCellWidth,
    [cellWidths]
  );

  const handleClickOnMainArea = React.useCallback(() => {
    if (editorState) return;

    tableRef.current?.focus();
  }, [editorState, tableRef]);

  const isFocusLocked = isFocusTable && !editorState;
  const mainScroll = React.useRef({ top: 0, left: 0 });

  const focusLockWhiteList = React.useCallback((node: HTMLElement) => {
    // このメソッドでtrueを返すと、フォーカスロックの対象となる

    // ソート条件・担当者のselectはフォーカスロック対象の要素内にあるが、フォーカスロックさせたくないのでここで除外している
    const selectors = Array.from(
      document.querySelectorAll(`.${SelectorContainerClassName}`)
    );
    if (selectors.some((selector) => selector.contains(node))) {
      return false;
    }

    const tagName = node.tagName.toLowerCase();
    return ['body', 'main'].includes(tagName) || node.tabIndex === 0;
  }, []);

  return (
    <ReactFocusLock
      disabled={!isFocusLocked}
      returnFocus={false}
      whiteList={focusLockWhiteList}
      onActivation={(node) => {
        const mainElement = node.querySelector('main');
        if (!mainElement) return;
        requestAnimationFrame(() => {
          mainElement.scrollTo({ ...mainScroll.current });
        });
      }}
      onDeactivation={(node) => {
        const mainElement = node.querySelector('main');
        mainScroll.current = {
          top: mainElement?.scrollTop ?? 0,
          left: mainElement?.scrollLeft ?? 0,
        };
      }}
    >
      <MainArea
        ref={tableRef}
        aria-label="表形式"
        windowHeight={windowHeight}
        tabIndex={-1}
        onClick={handleClickOnMainArea}
        onKeyDown={handleDataCellsContainerKeyDown}
        style={{
          outline: 'none',
          scrollPaddingTop: mainAreaScrollPaddingTop,
          scrollPaddingLeft: mainAreaScrollPaddingLeft,
        }}
      >
        <TableHeader tableWidth={tableWidth} style={{}}>
          <TopLeftHeader
            aria-label="ヘッダー"
            cellWidths={cellWidths}
            themeStyle={themeStyle}
          >
            <Spacer />
            <div
              style={{
                display: 'flex',
                alignItems: 'flex-end',
                backgroundColor: '#fafafa',
                paddingLeft: 10,
              }}
            >
              <TopLeftEmptyArea cellWidths={cellWidths}>
                <ButtonArea>
                  {renderProjectSelectorButton && renderProjectSelectorButton()}
                </ButtonArea>
                <ButtonArea>
                  <PlusButton onClick={openIssueSearchModal} tabIndex={-1}>
                    {l('button_add_task')}
                  </PlusButton>
                </ButtonArea>
                {currentMonth && (
                  <ButtonArea>
                    <PlusButton onClick={inheritLastMonth} tabIndex={-1}>
                      {l('button_take_over_project')}
                    </PlusButton>
                  </ButtonArea>
                )}
              </TopLeftEmptyArea>
            </div>
            <TableHeaderRow aria-label="チケットと作業のヘッダー">
              <NameHeader>{l('field_subject')}</NameHeader>
              <StatusHeader>{l('field_status')}</StatusHeader>
              <EstimatedHoursHeader>
                {l('field_estimated_hours')}
              </EstimatedHoursHeader>
              <ActivityHeader>{l('field_activity')}</ActivityHeader>
              <TotalSumHeader>{l('label_total')}</TotalSumHeader>
            </TableHeaderRow>
            {data.enableWorkingHours && (
              <TableHeaderRow aria-label="合計実労働時間">
                <WorkingHoursPrimaryRowHeader
                  label={l('label_working_hours')}
                  rightLabel={formatTimeEntryHours(data.sumWorkingHours)}
                />
              </TableHeaderRow>
            )}
            <TableHeaderRow aria-label="すべての作業">
              <AllWorksPrimaryRowHeader
                label={l('label_all_works')}
                rightLabel={formatTimeEntryHours(data.sum)}
              />
            </TableHeaderRow>
          </TopLeftHeader>
          <DateHeaders ref={dateHeadersSectionRef} rowWidth={rowWidth}>
            <MonthHeaderRow aria-label="月">
              {displayMonths.map(([month, days]) => (
                <MonthHeader role="columnheader" key={month} days={days}>
                  {month.replace(/-/, '/')}
                </MonthHeader>
              ))}
            </MonthHeaderRow>
            <TableHeaderRow aria-label="日付">
              {workMonth.dates.map((workDate) => (
                <DateHeader key={workDate.date} workDate={workDate} />
              ))}
            </TableHeaderRow>
            {data.enableWorkingHours && (
              <WorkingHoursPrimaryDataRow aria-label="実労働時間">
                {workMonth.dates.map((workDate, index) => (
                  <PrimaryHeaderHoursCell key={workDate.date}>
                    {formatTimeEntryHours(data.workingHoursByDate[index])}
                  </PrimaryHeaderHoursCell>
                ))}
              </WorkingHoursPrimaryDataRow>
            )}
            <PrimaryHeaderRow aria-label="日付の工数">
              {workMonth.dates.map((workDate, index) => (
                <PrimaryHeaderHoursCell key={workDate.date}>
                  {formatTimeEntryHours(data.sumByDate[index])}
                </PrimaryHeaderHoursCell>
              ))}
            </PrimaryHeaderRow>
          </DateHeaders>
        </TableHeader>
        <TableBody tableWidth={tableWidth}>
          <LeftArea
            aria-label="プロジェクトとチケット"
            cellWidths={cellWidths}
            themeStyle={themeStyle}
          >
            <div>
              {sortedProjects.map((projectData) => {
                const project = projectsById.get(projectData.id) as Project;
                const projectActivities = buildMapById(
                  activitiesById
                    .valueSeq()
                    .toArray()
                    .map((activity) => {
                      return {
                        active: project.activityIds.includes(activity.id),
                        ...activity,
                      };
                    })
                );
                return (
                  <ProjectRowHeaders
                    key={project.id}
                    projectData={projectData}
                    project={project}
                    issues={issuesById}
                    issueRowAppearances={issueRowAppearancesById}
                    activities={projectActivities}
                    onAddIssueActivity={addIssueActivity}
                    onAddActivity={(activityId): void => {
                      addProjectActivity({
                        projectId: projectData.id,
                        activityId,
                      });
                    }}
                    timeEntryRequiredFields={timeEntryRequiredFields}
                  />
                );
              })}
            </div>
            <IndirectEntriesArea>
              <IndirectActivityRowHeaders
                indirectActivities={indirectActivitiesById}
                indirectActivityData={data.indirectActivities}
                onAddActivity={(indirectActivityId): void => {
                  addIndirectActivity({ indirectActivityId });
                }}
              />
            </IndirectEntriesArea>
          </LeftArea>
          <TimeEntries
            ref={timeEntriesSectionRef}
            aria-label="工数"
            rowWidth={rowWidth}
          >
            <div>
              {sortedProjects.map((projectData) => {
                const project = projectsById.get(projectData.id) as Project;
                const projectActivities = buildMapById(
                  activitiesById
                    .valueSeq()
                    .toArray()
                    .map((activity) => {
                      return {
                        active: project.activityIds.includes(activity.id),
                        ...activity,
                      };
                    })
                );
                return (
                  <ProjectDataRows
                    key={projectData.id}
                    projectData={projectData}
                    issueRowAppearances={issueRowAppearancesById}
                    activitiesById={projectActivities}
                    workMonth={workMonth}
                    CellComponent={CellComponent}
                    onEditMultipleTimeEntries={openEditModal}
                  />
                );
              })}
            </div>
            <IndirectEntriesArea>
              <IndirectActivityRows
                indirectActivitiesById={indirectActivitiesById}
                indirectActivities={data.indirectActivities}
                workMonth={workMonth}
                CellComponent={CellComponent}
                onEditMultipleTimeEntries={openEditModal}
              />
            </IndirectEntriesArea>
          </TimeEntries>
        </TableBody>
        <Tippy
          interactive
          placement="auto"
          arrow={false}
          content={
            // Tippyのvisibleがfalseになっても、すぐに消えないここも消しておく
            editorState && (
              <EditorPopup aria-label="工数の編集">
                <TimeEntriesEditor
                  attachmentApi={attachmentApiWithMemory}
                  timeEntries={editedEntries}
                  customFields={editorState.customFields}
                  onCreate={(attributes): void => {
                    (async (): Promise<void> => {
                      try {
                        await createTimeEntry(
                          {
                            ...attributes,
                            ...editorState.coordinates,
                          },
                          targetUserId
                        );
                        onCloseModal();
                      } catch (error) {
                        // Keep editor open
                      }
                    })();
                  }}
                  onDelete={(id): void => {
                    (async (): Promise<void> => {
                      try {
                        await deleteTimeEntry(id);
                        onCloseModal();
                      } catch (error) {
                        // Keep editor open
                      }
                    })();
                  }}
                  onUpdate={(id, attributes): void => {
                    (async (): Promise<void> => {
                      try {
                        await updateTimeEntry(id, attributes);
                        onCloseModal();
                      } catch (error) {
                        // Keep editor open
                      }
                    })();
                  }}
                  requiredFields={timeEntryRequiredFields}
                  permissions={editorState.permissions}
                  onCancel={onCloseModal}
                />
              </EditorPopup>
            )
          }
          visible={!!editorState}
          onClickOutside={onClickOutsideEditorPopup}
          reference={editorState?.popupReferenceElement}
          role=""
          appendTo={document.body}
        />
      </MainArea>
    </ReactFocusLock>
  );
};

export default Table;
