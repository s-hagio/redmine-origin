import * as React from 'react';
import { RecoilRoot } from 'recoil';
import { render, screen, within } from '@testing-library/react';

import { buildIssue } from '@/lib/test/factories';
import { expectTextContents } from '@/lib/test/matchers';
import { buildMapById, MapById } from '@/lib/tableMode/mapById';
import type { Issue } from '@/lib/tableMode/types';

import IssueRowHeader, { buildAncestorSubjectsPerLine } from './IssueRowHeader';

describe('IssueRowHeader', () => {
  const renderWith = ({
    issue,
    issues,
  }: {
    issue?: Issue;
    issues?: MapById<Issue>;
    timeEntrySum?: number;
  }): void => {
    if (issue === undefined) {
      issue = buildIssue();
    }
    render(
      <RecoilRoot>
        <IssueRowHeader
          issue={issue}
          issues={issues || buildMapById([issue])}
          activityRowOffset={0}
        />
      </RecoilRoot>
    );
  };

  it('shows id, subject, status, estimated hours, timeEntrySum', () => {
    const issue = buildIssue({
      id: 1,
      subject: 'New Feature FG22',
      status: '進行中',
      estimatedHours: 4,
    });

    renderWith({ issue, timeEntrySum: 22 });

    const cells = screen.getAllByRole('rowheader');
    expectTextContents(cells, ['#1 New Feature FG22', '進行中', '4']);
  });

  it('shows a link to the subject', () => {
    const issue = buildIssue({ id: 333 });

    renderWith({ issue });

    const firstCell = screen.getByRole('rowheader', { name: /#333/ });
    const issueLink = within(firstCell).getByRole('link');
    expect(issueLink).toHaveTextContent('#333');
    expect(issueLink).toHaveAttribute('href', issue.url);
  });

  it('shows the ancestor issue names', () => {
    const issue = buildIssue({
      id: 333,
      subject: 'Subject',
      parentId: 22,
    });

    const ancestorSubjects = [
      'Great Grand Parent Subject',
      'Grand Parent Subject',
      'Parent Subject',
    ];
    renderWith({
      issue,
      issues: buildMapById([
        buildIssue({
          id: 1,
          subject: ancestorSubjects[0],
        }),
        buildIssue({
          id: 11,
          subject: ancestorSubjects[1],
          parentId: 1,
        }),
        buildIssue({
          id: 22,
          subject: ancestorSubjects[2],
          parentId: 11,
        }),
        issue,
      ]),
    });

    const firstCell = screen.getByRole('rowheader', { name: /#333/ });
    expect(firstCell).toHaveTextContent(ancestorSubjects.join(''));
  });
});

describe('buildAncestorSubjectsPerLine', () => {
  [
    [['1234567890123456789'], ['3', '7']],
    [['1', '567890123456789012'], ['6']],
    [['1', '5', '90123456789012']],
  ].forEach((expectedAncestorSubjectsPerLine, i) => {
    it(`returns the ancestor issue names [${i}]`, () => {
      const issue = buildIssue({
        id: 333,
        subject: 'Subject',
        parentId: 22,
      });
      const ancestorSubjects = expectedAncestorSubjectsPerLine.flat();
      const issues = buildMapById([
        buildIssue({
          id: 1,
          subject: ancestorSubjects[0],
        }),
        buildIssue({
          id: 11,
          subject: ancestorSubjects[1],
          parentId: 1,
        }),
        buildIssue({
          id: 22,
          subject: ancestorSubjects[2],
          parentId: 11,
        }),
        issue,
      ]);
      expect(buildAncestorSubjectsPerLine(issue.id, issues)).toEqual(
        expectedAncestorSubjectsPerLine
      );
    });
  });
});
