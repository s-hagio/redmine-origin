import { useLabels } from '@/lib/tableMode/labels';
import { CurrentUser, TargetUser } from '@/lib/tableMode/types';
import * as React from 'react';
import Selector from './atoms/Selector';

type TargetUserSelectProps = {
  currentUser: CurrentUser;
  targetUserId: number;
  targetUsers: TargetUser[];
  onChangeTargetUserId: (id: number) => void;
};

const TargetUserSelect: React.FC<TargetUserSelectProps> = ({
  currentUser,
  targetUserId,
  targetUsers,
  onChangeTargetUserId,
}: TargetUserSelectProps) => {
  const l = useLabels();

  const handleTargetUserIdChange = (value: unknown): void => {
    onChangeTargetUserId(Number(value));
  };

  const options = React.useMemo(
    () =>
      targetUsers.map(
        (user) => ({ value: user.id, label: user.name } as const)
      ),
    [targetUsers]
  );

  return (
    <Selector
      label={l('field_assigned_to')}
      value={targetUserId}
      onChange={handleTargetUserIdChange}
      noOptionFallback={<h2>{currentUser.name}</h2>}
      options={options}
      selectorWidth={300}
    />
  );
};

export default TargetUserSelect;
