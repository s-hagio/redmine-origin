import * as React from 'react';
import styled from 'styled-components';

import { useLabels } from '@/lib/tableMode/labels';
import { Issue, Project, Tracker } from '@/lib/tableMode/types';
import { MapById } from '@/lib/tableMode/mapById';

import Button from '../atoms/Button';
import LoadingOverlay from '../atoms/LoadingOverlay';
import {
  FilterTable,
  Query,
  Filter,
  AvailableFilters,
  OperatorsByFilterType,
  OperatorsLabels,
} from '@/components/TableMode/FilterTable';
import IssueTable from './IssueTable';
import { Paginator } from './Paginator';
import type { GroupedFilterOptions } from '@/components/TableMode/FilterTable/types';

const Root = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const TableArea = styled.div`
  flex: 0 1 auto;
  overflow: auto;
  margin-bottom: 1.2em;
`;

const ButtonsArea = styled.div`
  display: inline;
  justify-content: space-between;
  margin-top: 1.2em;
  ${Button} {
    display: inline;
  }
`;

type Props = {
  issuesById: MapById<Issue>;
  targetUserId: number;
  getAvailableFilters: () => Promise<{
    groupedFilterOptions: GroupedFilterOptions;
    availableFilters: AvailableFilters;
    operatorsByFilterType: OperatorsByFilterType;
    operatorsLabels: OperatorsLabels;
    queries: Query[];
  }>;
  getFilteredIssues: (
    filters: Filter[],
    page: number,
    queryId: string,
    userId: number
  ) => Promise<{
    queriedFilters: Filter[];
    issues: Issue[];
    ancestorIssues: Issue[];
    projects: Project[];
    trackers: Tracker[];
    totalCount: number;
    offset: number;
    limit: number;
  }>;
  onDataFetched: (data: { issues: Issue[]; projects: Project[] }) => void;
  onAddIssues: (issueProjectIds: Issue['id'][]) => void;
  onCancel: () => void;
  updateActivitiesById: (projectIds: number[], issueIds: number[]) => void;
};

const initialPage = 1 as const;

const SelectIssueForm: React.FC<Props> = ({
  issuesById,
  targetUserId,
  getAvailableFilters,
  getFilteredIssues,
  onDataFetched,
  onAddIssues,
  onCancel,
  updateActivitiesById,
}: Props) => {
  const l = useLabels();
  const [queryId, setQueryId] = React.useState<string>('');
  const [queries, setQueries] = React.useState<Query[]>([]);
  const [appliedFilters, setAppliedFilters] = React.useState<Filter[]>([]);
  const [currentFilters, setCurrentFilters] = React.useState<Filter[]>([
    { field: 'status_id', operator: 'o', enable: true },
    {
      field: 'assigned_to_id',
      operator: '=',
      values: [String(targetUserId)],
      enable: true,
    },
  ]);
  const [loading, setLoading] = React.useState<boolean>(true);
  const [groupedFilterOptions, setGroupedFilterOptions] =
    React.useState<GroupedFilterOptions>([]);
  const [availableFilters, setAvailableFilters] =
    React.useState<AvailableFilters>({});
  const [operatorsByFilterType, setOperatorsByFilterType] =
    React.useState<OperatorsByFilterType>({});
  const [operatorsLabels, setOperatorsLabels] = React.useState<OperatorsLabels>(
    {}
  );
  const [issues, setIssues] = React.useState<Issue[]>([]);
  const [projects, setProjects] = React.useState<Project[]>([]);
  const [trackers, setTrackers] = React.useState<Tracker[]>([]);
  const [page, setPage] = React.useState<number>(initialPage);
  const [paginator, setPaginator] = React.useState({
    totalCount: 0,
    offset: 0,
    limit: 0,
  });
  const [selectedIssueIds, setSelectedIssueIds] = React.useState<Issue['id'][]>(
    []
  );
  const updateTable = async ({
    page,
    queryId,
    filters,
  }: {
    page: number;
    queryId: string;
    filters: Filter[];
  }): Promise<void> => {
    setLoading(true);
    const enableFilters = filters.filter((f) => f.enable);
    const {
      queriedFilters,
      issues,
      ancestorIssues,
      projects,
      trackers,
      totalCount,
      offset,
      limit,
    } = await getFilteredIssues(enableFilters, page, queryId, targetUserId);
    onDataFetched({ issues: [...issues, ...ancestorIssues], projects });
    setCurrentFilters(queriedFilters);
    setAppliedFilters(queriedFilters);
    setIssues(issues);
    setProjects(projects);
    setTrackers(trackers);
    setPaginator({ totalCount, offset, limit });
    setLoading(false);
  };

  const createInitialProjectIdFilter = React.useCallback(
    (availableFilters: AvailableFilters): Filter | undefined => {
      const projectIdValues = availableFilters?.project_id?.values;
      if (projectIdValues?.map((v) => v[1]).includes('mine')) {
        return {
          field: 'project_id',
          operator: '=',
          values: ['mine'],
          enable: true,
        };
      }

      if (projectIdValues?.[0]) {
        // シス管でプロジェクト未アサインかつチケットにアサインされている場合、マイプロジェクトが存在しない
        // そのため、プロジェクトリストの最初のプロジェクトを初期選択値とする
        return {
          field: 'project_id',
          operator: '=',
          values: [projectIdValues[0][1]],
          enable: true,
        };
      }

      return undefined;
    },
    []
  );

  React.useEffect(() => {
    (async (): Promise<void> => {
      const {
        groupedFilterOptions,
        availableFilters,
        operatorsByFilterType,
        operatorsLabels,
        queries,
      } = await getAvailableFilters();
      setGroupedFilterOptions(groupedFilterOptions);
      setAvailableFilters(availableFilters);
      setOperatorsByFilterType(operatorsByFilterType);
      setOperatorsLabels(operatorsLabels);
      setQueries(queries);

      const initialProjectIdFilter =
        createInitialProjectIdFilter(availableFilters);
      const initialFilters = [...currentFilters, initialProjectIdFilter].filter(
        Boolean
      ) as Filter[];

      setCurrentFilters(initialFilters);
      updateTable({ page, queryId, filters: initialFilters });
    })();
  }, []);

  const getProjectActivities = async (selectedIssueIds: number[]) => {
    updateActivitiesById([], selectedIssueIds);
  };

  const handleSubmit = React.useCallback(() => {
    onAddIssues(selectedIssueIds),
    getProjectActivities(selectedIssueIds);
  }, [onAddIssues, selectedIssueIds]);

  const handleFilterSubmit = (e: React.SyntheticEvent<HTMLElement>): void => {
    e.preventDefault();
    setQueryId('');
    setSelectedIssueIds([]);
    setPage(initialPage);
    updateTable({ page: initialPage, queryId: '', filters: currentFilters });
  };

  const handlePageChange = (page: number): void => {
    setPage(page);
    setSelectedIssueIds([]);
    updateTable({ page, queryId, filters: appliedFilters });
  };

  const handleQueryChange = (e: React.ChangeEvent<HTMLSelectElement>): void => {
    const currentValue = e.currentTarget.value;

    setQueryId(currentValue);
    setSelectedIssueIds([]);
    setPage(initialPage);
    updateTable({ page: initialPage, queryId: currentValue, filters: [] });
  };

  return (
    <Root>
      <div>
        <h2>{l('label_select_issues')}</h2>
      </div>
      <div>
        <fieldset id="queries" className="collapsible">
          <legend className="query">{l('label_query')}</legend>
          <select onChange={handleQueryChange} value={queryId}>
            <option value=""></option>
            {queries.map((query) => (
              <option key={query.id} value={query.id}>
                {query.name}
              </option>
            ))}
          </select>
        </fieldset>
      </div>
      <form>
        <FilterTable
          type="IssueQuery"
          groupedFilterOptions={groupedFilterOptions}
          availableFilters={availableFilters}
          setAvailableFilters={setAvailableFilters}
          operatorsByFilterType={operatorsByFilterType}
          operatorsLabels={operatorsLabels}
          filters={currentFilters}
          onChange={(filters): void => setCurrentFilters(filters)}
        />
        <p className="buttons">
          <a
            href="#"
            onClick={handleFilterSubmit}
            className="icon icon-checked"
          >
            {l('button_apply')}
          </a>
        </p>
      </form>
      <TableArea>
        {loading && <LoadingOverlay />}
        <IssueTable
          issuesById={issuesById}
          issues={issues}
          projects={projects}
          trackers={trackers}
          selectedIds={selectedIssueIds}
          onSelectedIdsChanged={(value): void => setSelectedIssueIds(value)}
        />
      </TableArea>
      <Paginator {...paginator} onChange={handlePageChange} />
      <ButtonsArea>
        <Button
          textColor="#6b92b4"
          borderColor="#6b92b4"
          onClick={handleSubmit}
        >
          {l('button_add_selected_issues')}
        </Button>
        &nbsp;
        <a onClick={(): void => onCancel()} href="#">
          {l('button_cancel_selection')}
        </a>
      </ButtonsArea>
    </Root>
  );
};

export default SelectIssueForm;
