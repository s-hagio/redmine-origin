import * as React from 'react';
import classNames from 'classnames';
import { useLabels } from '@/lib/tableMode/labels';
import styled from 'styled-components';

type Props = {
  totalCount: number;
  offset: number;
  limit: number;
  onChange: (page: number) => void;
};

const Link = styled('a')`
  cursor: pointer;
`;

const pageNumbers = ({
  totalCount,
  offset,
  limit,
}: {
  totalCount: number;
  offset: number;
  limit: number;
}): [number, 'current' | 'page' | 'spacer'][] => {
  const currentPage = offset / limit + 1;
  const pagesCount = Math.ceil(totalCount / limit);
  const numbers: [number, 'current' | 'page' | 'spacer'][] = [];

  for (let i = 1; i < pagesCount + 1; i++) {
    if (i === currentPage) {
      numbers.push([i, 'current']);
      continue;
    }
    if (
      i === 1 ||
      i === pagesCount ||
      (i >= currentPage - 2 && i <= currentPage + 2)
    ) {
      numbers.push([i, 'page']);
      continue;
    }
    if (numbers[numbers.length - 1][1] === 'spacer') {
      continue;
    }
    numbers.push([i, 'spacer']);
  }
  return numbers;
};

export const Paginator: React.FC<Props> = ({
  totalCount,
  offset,
  limit,
  onChange,
}) => {
  const l = useLabels();
  const currentPage = offset / limit + 1;
  const tailNumber = offset + limit;
  const pages = pageNumbers({ totalCount, offset, limit });

  const handleClick =
    (page: number) =>
    (e: React.SyntheticEvent<HTMLElement>): void => {
      e.preventDefault();
      if (
        page === currentPage ||
        page < pages[0][0] ||
        page > pages[pages.length - 1][0]
      ) {
        return;
      }
      onChange(page);
    };

  if (pages.length === 0) {
    return null;
  }

  return (
    <span className="pagination">
      <ul className="pages">
        {pages.length > 1 && (
          <>
            <li
              className={classNames('previous', {
                page: currentPage !== pages[0][0],
              })}
              onClick={handleClick(currentPage - 1)}
            >
              {currentPage > pages[0][0] ? (
                <Link>
                  {'\xab'} {l('label_previous')}
                </Link>
              ) : (
                <span>
                  {'\xab'} {l('label_previous')}
                </span>
              )}
            </li>
            {pages.map(([pageNumber, className]) => (
              <li
                key={pageNumber}
                className={className}
                onClick={handleClick(className === 'spacer' ? 0 : pageNumber)}
              >
                {className === 'page' ? (
                  <Link>{pageNumber}</Link>
                ) : (
                  <span>{className === 'current' ? pageNumber : '…'}</span>
                )}
              </li>
            ))}
            <li
              className={classNames('next', {
                page: currentPage !== pages[pages.length - 1][0],
              })}
              onClick={handleClick(currentPage + 1)}
            >
              {currentPage < pages[pages.length - 1][0] ? (
                <Link>
                  {l('label_next')} {'\xbb'}
                </Link>
              ) : (
                <span>
                  {l('label_next')} {'\xbb'}
                </span>
              )}
            </li>
          </>
        )}
      </ul>
      <span>
        <span className="items">
          ({offset + 1}-{tailNumber > totalCount ? totalCount : tailNumber}/
          {totalCount})
        </span>
      </span>
    </span>
  );
};
