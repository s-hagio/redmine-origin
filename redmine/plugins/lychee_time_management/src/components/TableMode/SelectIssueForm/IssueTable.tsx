import * as React from 'react';
import classNames from 'classnames';

import { useLabels } from '@/lib/tableMode/labels';
import { buildMapById, MapById } from '@/lib/tableMode/mapById';
import { Issue, Project, Tracker } from '@/lib/tableMode/types';

type IssueId = Issue['id'];

type Props = {
  issuesById: MapById<Issue>;
  issues: Issue[];
  projects: Project[];
  trackers: Tracker[];
  selectedIds: IssueId[];
  onSelectedIdsChanged: (selectedIssueIds: IssueId[]) => void;
};

const IssueTable: React.FC<Props> = ({
  issuesById,
  issues,
  projects,
  trackers,
  selectedIds,
  onSelectedIdsChanged,
}) => {
  const [checkAll, setCheckAll] = React.useState(false);

  React.useEffect(() => {
    if (checkAll) {
      onSelectedIdsChanged(issues.map(({ id }) => id));
      return;
    }
    onSelectedIdsChanged([]);
  }, [checkAll]);

  React.useEffect(() => {
    if (selectedIds.length === 0) {
      setCheckAll(false);
    }
  }, [selectedIds]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const id = Number(e.currentTarget.name);
    const index = selectedIds.indexOf(id);
    if (index > -1) {
      const newSelectedIds = [...selectedIds];
      newSelectedIds.splice(index, 1);
      onSelectedIdsChanged(newSelectedIds);
      return;
    }
    onSelectedIdsChanged([...selectedIds, id]);
  };

  const l = useLabels();

  const projectsById = React.useMemo(() => buildMapById(projects), [projects]);
  const trackersById = React.useMemo(() => buildMapById(trackers), [trackers]);

  return (
    <table className="list issues odd-even">
      <thead>
        <tr>
          <th className="checkbox">
            <input
              type="checkbox"
              checked={checkAll}
              onChange={(e): void => {
                setCheckAll(e.currentTarget.checked);
              }}
            />
          </th>
          <th className="id">#</th>
          <th className="project">{l('field_project')}</th>
          <th className="parent">{l('field_parent_issue_subject')}</th>
          <th className="tracker">{l('field_tracker')}</th>
          <th className="subject">{l('field_subject')}</th>
          <th className="start_date">{l('field_start_date')}</th>
          <th className="due_date">{l('field_due_date')}</th>
        </tr>
      </thead>
      <tbody>
        {issues.map(
          (
            { id, projectId, parentId, trackerId, subject, startDate, dueDate },
            idx
          ) => (
            <tr
              key={id}
              className={classNames('issue', idx % 2 === 0 ? 'odd' : 'even')}
            >
              <td className="checkbox">
                <input
                  type="checkbox"
                  name={String(id)}
                  checked={selectedIds.includes(id)}
                  onChange={handleChange}
                />
              </td>
              <td className="id">{id}</td>
              <td className="project">{projectsById.get(projectId)?.name}</td>
              <td className="parent">
                {issuesById.get(parentId as number)?.subject}
              </td>
              <td className="tracker">{trackersById.get(trackerId)?.name}</td>
              <td className="subject">{subject}</td>
              <td className="start_date">{startDate}</td>
              <td className="due_date">{dueDate}</td>
            </tr>
          )
        )}
      </tbody>
    </table>
  );
};

export default IssueTable;
