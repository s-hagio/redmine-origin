import * as React from 'react';
import { render, waitFor } from '@testing-library/react';

import SelectIssueForm from './SelectIssueForm';

describe('SelectIssueForm', () => {
  const getFilteredIssues = jest.fn().mockResolvedValue({
    queriedFilters: [],
    issues: [],
    ancestorIssues: [],
    projects: [],
    trackers: [],
    totalCount: 0,
    offset: 0,
    llimit: 0,
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it.each([
    [[['マイプロジェクト', 'mine']]],
    [
      [
        ['マイプロジェクト', 'mine'],
        ['プロジェクト1', '1'],
        ['プロジェクト2', '2'],
      ],
    ],
    [
      [
        ['プロジェクト1', '1'],
        ['マイプロジェクト', 'mine'],
        ['プロジェクト2', '2'],
      ],
    ],
    [
      [
        ['プロジェクト1', '1'],
        ['プロジェクト2', '2'],
        ['マイプロジェクト', 'mine'],
      ],
    ],
  ])(
    'availableFiltersにマイプロジェクトがある場合は、プロジェクトフィルタの初期値としてマイプロジェクトが使われる',
    async (projectIds) => {
      const getAvailableFilters = jest.fn().mockResolvedValue({
        groupedFilterOptions: [],
        availableFilters: {
          project_id: {
            name: 'プロジェクト',
            type: 'list',
            values: projectIds,
            remote: true,
          },
        },
        operatorsByFilterType: {},
        operatorsLabels: {},
        queries: [],
      });

      render(
        <SelectIssueForm
          issuesById={new Map() as any}
          targetUserId={1}
          getAvailableFilters={getAvailableFilters}
          getFilteredIssues={getFilteredIssues}
          onDataFetched={jest.fn()}
          onAddIssues={jest.fn()}
          onCancel={jest.fn()}
          updateActivitiesById={jest.fn()}
        />
      );

      await waitFor(() => {
        expect(getFilteredIssues).toHaveBeenCalled();
      });

      expect(getFilteredIssues.mock.calls[0][0][2]).toEqual({
        field: 'project_id',
        operator: '=',
        values: ['mine'],
        enable: true,
      });
    }
  );

  it.each([
    [[['プロジェクト1', '1']], '1'],
    [
      [
        ['プロジェクト1', '1'],
        ['プロジェクト2', '2'],
        ['プロジェクト3', '3'],
      ],
      '1',
    ],
    [
      [
        ['プロジェクト2', '2'],
        ['プロジェクト3', '3'],
        ['プロジェクト1', '1'],
      ],
      '2',
    ],
    [
      [
        ['プロジェクト3', '3'],
        ['プロジェクト1', '1'],
        ['プロジェクト2', '2'],
      ],
      '3',
    ],
  ])(
    'availableFiltersにマイプロジェクトがない場合は、プロジェクトフィルタの初期値として最初のプロジェクトが使われる',
    async (projectIds, projectFilterId) => {
      const getAvailableFilters = jest.fn().mockResolvedValue({
        groupedFilterOptions: [],
        availableFilters: {
          project_id: {
            name: 'プロジェクト',
            type: 'list',
            values: projectIds,
            remote: true,
          },
        },
        operatorsByFilterType: {},
        operatorsLabels: {},
        queries: [],
      });

      render(
        <SelectIssueForm
          issuesById={new Map() as any}
          targetUserId={1}
          getAvailableFilters={getAvailableFilters}
          getFilteredIssues={getFilteredIssues}
          onDataFetched={jest.fn()}
          onAddIssues={jest.fn()}
          onCancel={jest.fn()}
          updateActivitiesById={jest.fn()}
        />
      );

      await waitFor(() => {
        expect(getFilteredIssues).toHaveBeenCalled();
      });

      expect(getFilteredIssues.mock.calls[0][0][2]).toEqual({
        field: 'project_id',
        operator: '=',
        values: [projectFilterId],
        enable: true,
      });
    }
  );
});
