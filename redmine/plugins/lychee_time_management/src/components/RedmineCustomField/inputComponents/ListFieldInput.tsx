import * as React from 'react';

import { InputContext } from '../context';
import { InputProps, normalizeValueOption } from '../types';
import CheckboxGroup from '../atoms/CheckboxGroup';
import RadioGroup from '../atoms/RadioGroup';
import Select from '../atoms/Select';

const ListFieldInput: React.FC<InputProps> = ({
  value,
  ...inputProps
}: InputProps) => {
  const { customField } = React.useContext(InputContext);
  const options = customField.possibleValuesOptions.map(normalizeValueOption);

  if (customField.editTagStyle === 'check_box') {
    if (customField.multiple) {
      return (
        <CheckboxGroup
          options={options}
          value={value as string[]}
          {...inputProps}
        />
      );
    }
    return (
      <RadioGroup options={options} value={value as string} {...inputProps} />
    );
  }
  return (
    <Select
      options={options}
      multiple={customField.multiple}
      value={value as string | string[]}
      {...inputProps}
    />
  );
};

export default ListFieldInput;
