import * as React from 'react';

import { InputProps } from '../types';

const getJQueryDatepickerRoot = (): HTMLElement | null =>
  document.querySelector('#ui-datepicker-div');

// Return whether element is inside JQuery datepicker
export const insideDatepicker = (element: HTMLElement): boolean => {
  const datepicker = getJQueryDatepickerRoot();
  if (!datepicker) {
    return false;
  }

  return datepicker.contains(element);
};

const fieldWidth = (width: string): React.CSSProperties => ({
  width,
  boxSizing: 'border-box',
});

const DATEPICKER_BUTTON_WIDTH = '24px';

const DateFieldInput: React.FC<InputProps> = ({
  className,
  onChange,
  value,
  ...inputProps
}: InputProps) => {
  const inputRef = React.useRef<HTMLInputElement>(null);
  const [fieldStyle, setFieldStyle] = React.useState<React.CSSProperties>(
    fieldWidth('100%')
  );

  React.useEffect(() => {
    // RedmineのJQueryとグローバル変数を使うために ts-ignore 入れた
    /* eslint-disable @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    if (inputRef.current && window.$) {
      // @ts-ignore
      $(inputRef.current).datepickerFallback(window.datepickerOptions);
      if (getJQueryDatepickerRoot()) {
        setFieldStyle(fieldWidth(`calc(100% - ${DATEPICKER_BUTTON_WIDTH})`));
      }
    }
    /* eslint-enable @typescript-eslint/ban-ts-comment */
  }, []);

  const handleChange = React.useCallback<
    React.ChangeEventHandler<HTMLInputElement>
  >(
    (event) => {
      if (onChange) {
        onChange(event.target.value);
      }
    },
    [onChange]
  );

  return (
    // JQuery Datepickerのボタンがinputの隣になるので、コンポーネント全体にclassName適用するために全部spanに
    <span className={className}>
      <input
        className="date"
        style={fieldStyle}
        type="date"
        onChange={handleChange}
        value={value as string}
        {...inputProps}
        ref={inputRef}
      />
    </span>
  );
};

export default DateFieldInput;
