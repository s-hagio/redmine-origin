import * as React from 'react';

import { InputProps } from '../types';

const TextFieldInput: React.FC<InputProps> = ({
  onChange,
  value,
  ...inputProps
}: InputProps) => {
  const handleChange = React.useCallback<
    React.ChangeEventHandler<HTMLInputElement>
  >(
    event => {
      if (onChange) {
        onChange(event.target.value);
      }
    },
    [onChange]
  );

  return (
    <input
      type="text"
      onChange={handleChange}
      value={value as string}
      {...inputProps}
    />
  );
};

export default TextFieldInput;
