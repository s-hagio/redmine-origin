import * as React from 'react';
import classNames from 'classnames';

import { InputContext } from '../context';
import { Attachment, InputProps } from '../types';

const AttachmentFieldInput: React.FC<InputProps> = ({
  className,
  value,
  onChange,
  ...inputProps
}: InputProps) => {
  const {
    config: { attachmentApi, deleteAttachmentsImmediately },
  } = React.useContext(InputContext);
  const [attachment, setAttachment] = React.useState<Attachment | null>(null);
  React.useEffect(() => {
    if (value && !attachment) {
      (async (): Promise<void> => {
        const attachment = await attachmentApi.getAttachment(
          parseInt(value as string)
        );
        setAttachment(attachment);
      })();
    }
  }, [attachment, attachmentApi, value]);

  const handleChange = React.useCallback(
    (ev: React.ChangeEvent<HTMLInputElement>): void => {
      if (!onChange || !ev.currentTarget.files) {
        return;
      }

      const file = ev.currentTarget.files[0];

      (async (): Promise<void> => {
        const attachment = await attachmentApi.createAttachment(file);
        setAttachment(attachment);
        onChange({ token: attachment.token });
      })();
    },
    [attachmentApi, onChange]
  );

  const handleDelete = React.useCallback(() => {
    if (!onChange || !attachment) {
      return;
    }

    (async (): Promise<void> => {
      if (deleteAttachmentsImmediately) {
        await attachmentApi.deleteAttachment(attachment.id);
      }
      setAttachment(null);
      onChange('');
    })();
  }, [attachment, attachmentApi, deleteAttachmentsImmediately, onChange]);

  return (
    <span className={classNames('attachments_form', className)}>
      {value ? (
        <span className="attachments_fields" {...inputProps}>
          {attachment && (
            <>
              <span style={{ display: 'inline-block' }}>
                {attachment.filename}
              </span>
              <a
                className="icon-only icon-del"
                role="button"
                aria-label="削除"
                onClick={handleDelete}
              />
            </>
          )}
        </span>
      ) : (
        <span className="add_attachment">
          <input
            type="file"
            onChange={handleChange}
            {...inputProps}
            role="button"
          />
        </span>
      )}
    </span>
  );
};

export default AttachmentFieldInput;
