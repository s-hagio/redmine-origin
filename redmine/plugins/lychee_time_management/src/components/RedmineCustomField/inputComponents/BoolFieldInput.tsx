import * as React from 'react';

import { InputContext } from '../context';
import { InputProps, normalizeValueOption } from '../types';

import RadioGroup from '../atoms/RadioGroup';
import Select from '../atoms/Select';

type CheckboxProps = InputProps<string>;

const Checkbox: React.FC<CheckboxProps> = ({
  className,
  value,
  onChange,
  ...inputProps
}: CheckboxProps) => {
  const handleChange = React.useCallback<
    React.ChangeEventHandler<HTMLInputElement>
  >(
    event => {
      if (onChange) {
        const newValue = event.target.checked ? '1' : '0';
        onChange(newValue);
      }
    },
    [onChange]
  );

  return (
    <span className={className}>
      <input
        type="checkbox"
        checked={value === '1'}
        onChange={handleChange}
        {...inputProps}
      />
    </span>
  );
};

const BoolFieldInput: React.FC<InputProps> = ({
  value,
  ...inputProps
}: InputProps) => {
  const { customField } = React.useContext(InputContext);
  const options = customField.possibleValuesOptions.map(normalizeValueOption);

  switch (customField.editTagStyle) {
    case 'radio':
      return (
        <RadioGroup options={options} value={value as string} {...inputProps} />
      );
    case 'check_box':
      return <Checkbox value={value as string} {...inputProps} />;
    default:
      return (
        <Select options={options} value={value as string} {...inputProps} />
      );
  }
};

export default BoolFieldInput;
