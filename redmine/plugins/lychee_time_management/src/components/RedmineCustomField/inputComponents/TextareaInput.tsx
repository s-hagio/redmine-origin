import * as React from 'react';

import { InputProps } from '../types';

const TextareaInput: React.FC<InputProps> = ({
  onChange,
  value,
  ...inputProps
}: InputProps) => {
  const handleChange = React.useCallback<
    React.ChangeEventHandler<HTMLTextAreaElement>
  >(
    event => {
      if (onChange) {
        onChange(event.target.value);
      }
    },
    [onChange]
  );

  return (
    <textarea onChange={handleChange} value={value as string} {...inputProps} />
  );
};

export default TextareaInput;
