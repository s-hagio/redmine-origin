import * as React from 'react';

import { InputProps } from '../types';
import TextFieldInput from './TextFieldInput';

const NumberTextFieldInput: React.FC<InputProps> = ({
  ...inputProps
}: InputProps) => (
  <TextFieldInput
    {
      ...{
        autocomplete: 'off',
        ...inputProps,
      }
    }
  />
);

export default NumberTextFieldInput;
