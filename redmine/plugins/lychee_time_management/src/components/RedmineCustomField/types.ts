import * as React from 'react';

export type Attachment = {
  id: number;
  filename: string;
  token: string;
};

// TODO: Provide standard implementation or Redmine Default API
export type AttachmentApiHandler = {
  createAttachment: (file: File) => Promise<Attachment>;
  deleteAttachment: (id: number) => Promise<void>;
  getAttachment: (id: number) => Promise<Attachment>;
};

export type FieldFormat =
  | 'string'
  | 'text'
  | 'link'
  | 'int'
  | 'float'
  | 'date'
  | 'list'
  | 'bool'
  | 'enumeration'
  | 'user'
  | 'version'
  | 'attachment';

type EditTagStyle = '' | 'check_box' | 'radio';

type ValueOption = string | [string, string];

export type NormalizedValueOption = { text: string; value: string };

export const normalizeValueOption = (
  valueOption: ValueOption
): NormalizedValueOption => {
  if (valueOption instanceof Array) {
    return { text: valueOption[0], value: valueOption[1] };
  }

  return { text: valueOption, value: valueOption };
};

export type CustomField = {
  id: number;
  name: string;
  isRequired: boolean;
  defaultValue: string;
  fieldFormat: FieldFormat;
  multiple: boolean;
  editTagStyle?: EditTagStyle;
  possibleValuesOptions: ValueOption[];
};

export type CustomFieldConfig = {
  attachmentApi: AttachmentApiHandler;
  deleteAttachmentsImmediately: boolean;
  labels: {
    none: string;
    pleaseSelect: string;
  };
};

export type InputProps<ValueType = unknown> = {
  value: ValueType;
  onChange: (value: ValueType) => void;
  id?: string;
  name?: string;
  className?: string;
  role?: string;
  required?: boolean;
  onKeyDown?: React.KeyboardEventHandler;
  onCompositionStart?: React.CompositionEventHandler;
  onCompositionEnd?: React.CompositionEventHandler;
} & React.AriaAttributes;
