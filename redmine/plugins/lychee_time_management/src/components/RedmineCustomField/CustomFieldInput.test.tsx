import * as React from 'react';
import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { buildCustomField } from './testHelpers';

import {
  AttachmentApiHandler,
  CustomField,
  CustomFieldConfig,
  InputProps,
} from './types';
import CustomFieldInput from './CustomFieldInput';

describe('CustomFieldInput', () => {
  type Callbacks = jest.Mocked<AttachmentApiHandler> &
    jest.Mocked<Pick<InputProps, 'onChange'>>;

  const renderWith = ({
    customField,
    value,
  }: {
    customField: CustomField;
    value?: unknown;
  }): Callbacks => {
    const callbacks: Callbacks = {
      createAttachment: jest.fn(),
      deleteAttachment: jest.fn(),
      getAttachment: jest.fn(),
      onChange: jest.fn(),
    };
    const config: CustomFieldConfig = {
      attachmentApi: {
        createAttachment: callbacks.createAttachment,
        deleteAttachment: callbacks.deleteAttachment,
        getAttachment: callbacks.getAttachment,
      },
      deleteAttachmentsImmediately: false,
      labels: {
        none: 'なし',
        pleaseSelect: '選んでください',
      },
    };

    render(
      <CustomFieldInput
        customField={customField}
        value={value || (customField.multiple ? [] : '')}
        config={config}
        onChange={callbacks.onChange}
      />
    );
    return callbacks;
  };

  const expectOptions = (element: HTMLElement, options: string[]): void => {
    const selectOptions = within(element).getAllByRole('option');

    expect(selectOptions).toHaveLength(options.length);
    options.forEach((text, index) => {
      const option = selectOptions[index];
      expect(option).toHaveTextContent(text);
    });
  };

  const expectRadioButtons = (
    element: HTMLElement,
    options: string[]
  ): void => {
    const radioButtons = within(element).getAllByRole('radio');

    expect(radioButtons).toHaveLength(options.length);
    options.forEach((text, index) => {
      const radioButton = radioButtons[index];
      expect(radioButton.parentElement).toHaveTextContent(text);
    });
  };

  const expectCheckboxes = (element: HTMLElement, options: string[]): void => {
    const checkboxes = within(element).getAllByRole('checkbox');

    expect(checkboxes).toHaveLength(options.length);
    options.forEach((text, index) => {
      const checkbox = checkboxes[index];
      expect(checkbox.parentElement).toHaveTextContent(text);
    });
  };

  const fieldFormats: CustomField['fieldFormat'][] = [
    'enumeration',
    'list',
    'user',
    'version',
  ];

  describe.each(fieldFormats)('%s', (fieldFormat) => {
    describe('Select tag (single)', () => {
      it('shows empty option if not required', () => {
        renderWith({
          customField: buildCustomField({
            fieldFormat,
            possibleValuesOptions: [
              ['Good', '1'],
              ['Bad', '2'],
            ],
          }),
        });

        const selectBox = screen.getByRole('combobox');
        expectOptions(selectBox, ['', 'Good', 'Bad']);
      });

      it('shows please choose option if required', () => {
        renderWith({
          customField: buildCustomField({
            fieldFormat,
            isRequired: true,
            possibleValuesOptions: [
              ['Good', '1'],
              ['Bad', '2'],
            ],
          }),
        });

        const selectBox = screen.getByRole('combobox');
        expectOptions(selectBox, ['--- 選んでください ---', 'Good', 'Bad']);
      });

      it('sends the correct onChange event', async () => {
        const user = userEvent.setup();
        const { onChange } = renderWith({
          customField: buildCustomField({
            fieldFormat,
            isRequired: true,
            possibleValuesOptions: [
              ['Good', '1'],
              ['Bad', '2'],
            ],
          }),
        });

        const selectBox = screen.getByRole('combobox');
        await user.selectOptions(selectBox, 'Good');

        expect(onChange).toHaveBeenCalledWith('1');
      });
    });

    describe('Radio group', () => {
      it('shows empty option when not required', () => {
        renderWith({
          customField: buildCustomField({
            fieldFormat,
            editTagStyle: 'check_box',
            possibleValuesOptions: [
              ['Red', '1'],
              ['Green', '2'],
            ],
          }),
        });

        const radioGroup = screen.getByRole('radiogroup');
        expectRadioButtons(radioGroup, ['(なし)', 'Red', 'Green']);
      });

      it('shows no empty option when required', () => {
        renderWith({
          customField: buildCustomField({
            fieldFormat,
            editTagStyle: 'check_box',
            isRequired: true,
            possibleValuesOptions: [
              ['Red', '1'],
              ['Green', '2'],
            ],
          }),
        });

        const radioGroup = screen.getByRole('radiogroup');
        expectRadioButtons(radioGroup, ['Red', 'Green']);
      });

      it('sends correct onChange event', async () => {
        const user = userEvent.setup();
        const { onChange } = renderWith({
          customField: buildCustomField({
            fieldFormat,
            editTagStyle: 'check_box',
            isRequired: true,
            possibleValuesOptions: [
              ['Red', '1'],
              ['Green', '2'],
            ],
          }),
        });

        const radioGroup = screen.getByRole('radiogroup');
        await user.click(within(radioGroup).getByText('Red'));

        expect(onChange).toHaveBeenCalledWith('1');
      });
    });

    describe('Select tag (multiple)', () => {
      it('shows just the options when not required', () => {
        renderWith({
          customField: buildCustomField({
            fieldFormat,
            multiple: true,
            possibleValuesOptions: [
              ['Good', '1'],
              ['Bad', '2'],
            ],
          }),
        });

        const listBox = screen.getByRole('listbox');
        expectOptions(listBox, ['Good', 'Bad']);
      });

      it('shows just the options when required', () => {
        renderWith({
          customField: buildCustomField({
            fieldFormat,
            multiple: true,
            isRequired: true,
            possibleValuesOptions: [
              ['Good', '1'],
              ['Bad', '2'],
            ],
          }),
        });

        const listBox = screen.getByRole('listbox');
        expectOptions(listBox, ['Good', 'Bad']);
      });

      it('sends the correct onChange event', async () => {
        const user = userEvent.setup();
        const { onChange } = renderWith({
          customField: buildCustomField({
            fieldFormat,
            multiple: true,
            possibleValuesOptions: [
              ['Apple', '1'],
              ['Banana', '2'],
              ['Pineapple', '3'],
            ],
          }),
          value: ['3'],
        });

        const listBox = screen.getByRole('listbox');
        await user.selectOptions(listBox, 'Apple');

        expect(onChange).toHaveBeenCalledWith(['1', '3']);
      });
    });

    describe('Checkbox group', () => {
      it('shows just the options when not required', () => {
        renderWith({
          customField: buildCustomField({
            fieldFormat,
            editTagStyle: 'check_box',
            multiple: true,
            possibleValuesOptions: ['Tiger', 'Lion', 'Bear'],
          }),
        });

        const checkboxGroup = screen.getByRole('group');
        expectCheckboxes(checkboxGroup, ['Tiger', 'Lion', 'Bear']);
      });

      it('shows just the options when required', () => {
        renderWith({
          customField: buildCustomField({
            fieldFormat,
            editTagStyle: 'check_box',
            multiple: true,
            isRequired: true,
            possibleValuesOptions: ['Tiger', 'Lion', 'Bear'],
          }),
        });

        const checkboxGroup = screen.getByRole('group');
        expectCheckboxes(checkboxGroup, ['Tiger', 'Lion', 'Bear']);
      });

      it('sends the correct onChange event', async () => {
        const user = userEvent.setup();
        const { onChange } = renderWith({
          customField: buildCustomField({
            fieldFormat,
            editTagStyle: 'check_box',
            multiple: true,
            possibleValuesOptions: ['Tiger', 'Lion', 'Bear'],
          }),
          value: ['Tiger'],
        });

        const checkboxGroup = screen.getByRole('group');
        await user.click(within(checkboxGroup).getByText('Lion'));

        expect(onChange).toHaveBeenCalledWith(['Tiger', 'Lion']);
      });
    });
  });

  describe('bool', () => {
    it('displays as selects by default', () => {
      renderWith({
        customField: buildCustomField({
          fieldFormat: 'bool',
          possibleValuesOptions: [
            ['はい', '1'],
            ['いいえ', '0'],
          ],
        }),
      });

      const selectBox = screen.getByRole('combobox');
      expectOptions(selectBox, ['', 'はい', 'いいえ']);
    });

    it('displays as radiogroup when specified by editTagStyle', () => {
      renderWith({
        customField: buildCustomField({
          fieldFormat: 'bool',
          editTagStyle: 'radio',
          possibleValuesOptions: [
            ['はい', '1'],
            ['いいえ', '0'],
          ],
        }),
      });

      const radioGroup = screen.getByRole('radiogroup');
      expectRadioButtons(radioGroup, ['(なし)', 'はい', 'いいえ']);
    });

    it('displays as single checkbox when specified by editTagStyle', () => {
      renderWith({
        customField: buildCustomField({
          fieldFormat: 'bool',
          editTagStyle: 'check_box',
          possibleValuesOptions: [
            ['はい', '1'],
            ['いいえ', '0'],
          ],
        }),
      });

      expect(screen.getByRole('checkbox')).toBeInTheDocument();
    });
  });
});
