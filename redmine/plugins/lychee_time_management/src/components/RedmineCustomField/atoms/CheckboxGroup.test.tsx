import * as React from 'react';
import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import CheckboxGroup from './CheckboxGroup';

const options = [
  { text: 'Option A', value: 'a' },
  { text: 'Option B', value: 'b' },
  { text: 'Option C', value: 'c' },
];

const setup = (jsx: JSX.Element) => {
  return {
    user: userEvent.setup(),
    ...render(jsx),
  };
};

describe('CheckboxGroup', () => {
  it('shows the correct options', () => {
    const handleChange = jest.fn();
    setup(
      <CheckboxGroup
        options={options}
        value={['a', 'c']}
        onChange={handleChange}
      />
    );

    const checkboxGroup = screen.getByRole('group');
    expect(within(checkboxGroup).getAllByRole('checkbox')).toHaveLength(3);

    const optionA = within(checkboxGroup).getByRole('checkbox', {
      name: 'Option A',
    });
    expect(optionA).toBeInTheDocument();
    expect(optionA).toBeChecked();

    const optionB = within(checkboxGroup).getByRole('checkbox', {
      name: 'Option B',
    });
    expect(optionB).toBeInTheDocument();
    expect(optionB).not.toBeChecked();

    const optionC = within(checkboxGroup).getByRole('checkbox', {
      name: 'Option C',
    });
    expect(optionC).toBeInTheDocument();
    expect(optionC).toBeChecked();
  });

  it('sends the correct onChange event', async () => {
    const handleChange = jest.fn();
    const { user } = setup(
      <CheckboxGroup
        options={options}
        value={['a', 'c']}
        onChange={handleChange}
      />
    );
    const checkbox = within(screen.getByRole('group')).getByRole('checkbox', {
      name: 'Option C',
    });
    await user.click(checkbox);
    expect(handleChange).toHaveBeenCalledWith(['a']);
  });
});
