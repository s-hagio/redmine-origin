import * as React from 'react';
import classNames from 'classnames';

import { InputProps, NormalizedValueOption } from '../types';

type CheckboxGroupProps = InputProps<string[]> & {
  options: NormalizedValueOption[];
};

const CheckboxGroup: React.FC<CheckboxGroupProps> = ({
  name,
  value,
  onChange,
  className,
  options,
  ...inputProps
}: CheckboxGroupProps) => {
  const handleChange = React.useCallback<
    React.ChangeEventHandler<HTMLInputElement>
  >(
    (event) => {
      if (onChange) {
        const values = value as string[];
        const newValue = event.target.checked
          ? [...values, event.target.value]
          : values.filter((v) => v != event.target.value);
        onChange(newValue);
      }
    },
    [onChange, value]
  );

  return (
    <div
      role="group"
      {...inputProps}
      className={classNames('check_box_group', className)}
    >
      {options.map((option) => {
        const id = `${name}_${option.value}`;
        return (
          <label key={option.value}>
            <input
              type="checkbox"
              checked={value.includes(option.value)}
              name={name}
              id={id}
              value={option.value}
              onChange={handleChange}
            />
            {option.text}
          </label>
        );
      })}
    </div>
  );
};

export default CheckboxGroup;
