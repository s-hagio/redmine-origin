import * as React from 'react';
import classNames from 'classnames';

import { InputContext } from '../context';
import { InputProps, NormalizedValueOption } from '../types';

type RadioGroupProps = InputProps<string> & {
  options: NormalizedValueOption[];
};

const RadioGroup: React.FC<RadioGroupProps> = ({
  name,
  value,
  onChange,
  className,
  options,
  ...inputProps
}: RadioGroupProps) => {
  const {
    config: { labels },
  } = React.useContext(InputContext);

  const handleChange = React.useCallback<
    React.ChangeEventHandler<HTMLInputElement>
  >(
    (event) => {
      if (onChange) {
        onChange(event.target.value);
      }
    },
    [onChange]
  );

  return (
    <div
      role="radiogroup"
      {...inputProps}
      className={classNames('check_box_group', className)}
    >
      {!inputProps.required && (
        <label>
          <input
            type="radio"
            checked={value === ''}
            name={name}
            id={`${name}_`}
            value=""
            onChange={handleChange}
          />
          ({labels.none})
        </label>
      )}
      {options.map((option) => {
        const id = `${name}_${option.value}`;
        return (
          <label key={option.value}>
            <input
              type="radio"
              checked={value === option.value}
              name={name}
              id={id}
              value={option.value}
              onChange={handleChange}
            />
            {option.text}
          </label>
        );
      })}
    </div>
  );
};

export default RadioGroup;
