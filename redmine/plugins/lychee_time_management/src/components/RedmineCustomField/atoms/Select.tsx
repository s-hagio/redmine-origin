import * as React from 'react';

import { InputContext } from '../context';
import { InputProps, NormalizedValueOption } from '../types';
import './Select.scss';

type SelectProps = InputProps<string | string[]> & {
  options: NormalizedValueOption[];
  multiple?: boolean;
};

const Select: React.FC<SelectProps> = ({
  onChange,
  options,
  multiple,
  required,
  ...inputProps
}: SelectProps) => {
  const {
    config: { labels },
  } = React.useContext(InputContext);
  const handleChange = React.useCallback<
    React.ChangeEventHandler<HTMLSelectElement>
  >(
    (event) => {
      if (!onChange) {
        return;
      }

      if (multiple) {
        onChange(
          Array.from(event.target.options)
            .filter((option) => option.selected)
            .map((option) => option.value)
        );
        return;
      }

      onChange(event.target.value);
    },
    [multiple, onChange]
  );

  return (
    <select multiple={multiple} onChange={handleChange} {...inputProps}>
      {!multiple && (
        <option>{required ? `--- ${labels.pleaseSelect} ---` : ''}</option>
      )}
      {options.map((option) => (
        <option key={`${option.value}_${option.text}`} value={option.value}>
          {option.text}
        </option>
      ))}
    </select>
  );
};

export default Select;
