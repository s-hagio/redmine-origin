import { CustomField } from './types';

let lastId = 0;

export const generateId = (): number => {
  lastId += 1;
  return lastId;
};

export const buildCustomField = (
  attributes?: Partial<CustomField>
): CustomField => {
  const id = generateId();
  return {
    id,
    name: `Custom Field #${id}`,
    isRequired: false,
    defaultValue: '',
    multiple: false,
    fieldFormat: 'string',
    possibleValuesOptions: [],
    ...(attributes || {}),
  };
};
