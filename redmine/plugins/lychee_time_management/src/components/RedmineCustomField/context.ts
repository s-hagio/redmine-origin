import * as React from 'react';

import { Attachment, CustomField, CustomFieldConfig } from './types';

type InputContextData = {
  customField: CustomField;
  config: CustomFieldConfig;
};

export const InputContext = React.createContext<InputContextData>({
  config: {
    attachmentApi: {
      createAttachment: async (): Promise<Attachment> => {
        throw 'not implemented';
      },
      deleteAttachment: async (): Promise<void> => {
        throw 'not implemented';
      },
      getAttachment: async (): Promise<Attachment> => {
        throw 'Not implemented';
      },
    },
    deleteAttachmentsImmediately: false,
    labels: {
      none: 'None',
      pleaseSelect: 'Please select',
    },
  },
  customField: {
    id: -1,
    fieldFormat: 'text',
    isRequired: false,
    defaultValue: '',
    name: '',
    multiple: false,
    possibleValuesOptions: [],
  },
});
