import * as React from 'react';
import classNames from 'classnames';

import { InputContext } from './context';
import { CustomField, CustomFieldConfig, InputProps } from './types';
import AttachmentFieldInput from './inputComponents/AttachmentFieldInput';
import BoolFieldInput from './inputComponents/BoolFieldInput';
import DateFieldInput from './inputComponents/DateFieldInput';
import ListFieldInput from './inputComponents/ListFieldInput';
import NumberTextFieldInput from './inputComponents/NumberTextFieldInput';
import TextareaInput from './inputComponents/TextareaInput';
import TextFieldInput from './inputComponents/TextFieldInput';

type CustomFieldInputProps = Omit<InputProps, 'required'> & {
  customField: CustomField;
  config: CustomFieldConfig;
};

const CustomFieldInput: React.FC<CustomFieldInputProps> = ({
  config,
  customField,
  className,
  ...inputProps
}: CustomFieldInputProps) => {
  const customFieldClassName = React.useMemo(
    () => classNames(className, `${customField.fieldFormat}_cf`),
    [className, customField.fieldFormat]
  );

  const InputComponent = React.useMemo<React.FC<InputProps>>(() => {
    switch (customField.fieldFormat) {
      case 'int':
      case 'float':
        return NumberTextFieldInput;
      case 'link':
      case 'string':
        return TextFieldInput;
      case 'enumeration':
      case 'list':
      case 'user':
      case 'version':
        return ListFieldInput;
      case 'date':
        return DateFieldInput;
      case 'bool':
        return BoolFieldInput;
      case 'text':
        return TextareaInput;
      case 'attachment':
        return AttachmentFieldInput;
      default:
        return TextFieldInput;
    }
  }, [customField.fieldFormat]);

  return (
    <InputContext.Provider value={{ customField, config }}>
      <InputComponent
        className={customFieldClassName}
        required={customField.isRequired}
        {...inputProps}
      />
    </InputContext.Provider>
  );
};

export default CustomFieldInput;
