import { atom, AtomEffect, selector } from 'recoil';

export type CellWidths = {
  issueNameCellWidth: number;
  issueStatusCellWidth: number;
  issueEstimateHoursCellWidth: number;
  issueActivitiesCellWidth: number;
  rowSumCellWidth: number;
};

export const MIN_CELL_WIDTHS = {
  issueNameCellWidth: 190,
  issueStatusCellWidth: 84,
  issueEstimateHoursCellWidth: 64,
  issueActivitiesCellWidth: 115,
  rowSumCellWidth: 64,
} as const;

const localStorageEffect =
  <T>(key: string): AtomEffect<T> =>
  ({ setSelf, onSet }) => {
    const savedValue = localStorage.getItem(key);
    if (savedValue !== null) {
      setSelf(JSON.parse(savedValue));
    }

    onSet((newValue, _, isReset) => {
      isReset
        ? localStorage.removeItem(key)
        : localStorage.setItem(key, JSON.stringify(newValue));
    });
  };

export const cellWidths = atom<CellWidths>({
  key: 'cellWidths',
  default: MIN_CELL_WIDTHS,
  effects: [localStorageEffect('lychee_time_management__cell_widths')],
});

export const rowHeaderWidth = selector<number>({
  key: 'rowHeaderWidth',
  get: ({ get }) =>
    Object.values(get(cellWidths)).reduce((prev, curr) => prev + curr, 0),
});
