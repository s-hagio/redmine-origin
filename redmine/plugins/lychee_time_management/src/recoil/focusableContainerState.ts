import { MapById } from '@/lib/tableMode/mapById';
import { sortActivities } from '@/lib/tableMode/rowSorter';
import { IndirectActivities, ProjectData } from '@/lib/tableMode/tableData';
import {
  Activity,
  RowCoordinates,
  WorkDate,
  WorkMonth,
} from '@/lib/tableMode/types';
import * as React from 'react';
import {
  atom,
  selector,
  useRecoilCallback,
  useRecoilState,
  useRecoilTransaction_UNSTABLE,
  useRecoilValue,
  useSetRecoilState,
} from 'recoil';
import { match, P } from 'ts-pattern';

type DataCellPosition = {
  x: number;
  y: number;
};

type DataCellInfo =
  | { type: 'project'; projectId: number; activityId: number; date: string }
  | {
      type: 'issue';
      projectId: number;
      issueId: number;
      activityId: number;
      date: string;
    }
  | { type: 'indirect'; activityId: number; date: string };

const focusedDataCellPositionState = atom<DataCellPosition | undefined>({
  key: 'focusedDataCellPositionState',
  default: undefined,
});

const focusedDataCellState = selector<DataCellInfo | undefined>({
  key: 'focusedDataCellState',
  get: ({ get }) => {
    const pos = get(focusedDataCellPositionState);
    return pos && get(dataCellsState)[pos.y]?.[pos.x];
  },
});

type DataCellsContainerModeState = 'SELECT' | 'EDIT' | 'MODAL';
const dataCellsContainerModeState = atom<DataCellsContainerModeState>({
  key: 'dataCellsContainerModeState',
  default: 'SELECT',
});

type DataCellsState = DataCellInfo[][];
const dataCellsState = atom<DataCellsState>({
  key: 'dataCellsState',
  default: [],
});

const cellKeyByProject = (
  projectId: number,
  activityId: number,
  date: string // YYYY-MM-DD
) => `project-${projectId}-activity-${activityId}-${date}`;
const cellKeyByIssue = (
  projectId: number,
  issueId: number,
  activityId: number,
  date: string // YYYY-MM-DD
) => `project-${projectId}-issue-${issueId}-activity-${activityId}-${date}`;
const cellKeyByIndirect = (
  indirectActivityId: number,
  date: string // YYYY-MM-DD
) => `indirect-${indirectActivityId}-${date}`;

const cellKeyByRowCoordinatesAndWorkDate = (
  rowCoordinates: RowCoordinates,
  workDate: WorkDate
) =>
  match(rowCoordinates)
    .with(
      { activityType: 'project' },
      ({ activity: { projectId, activityId } }) =>
        cellKeyByProject(projectId, activityId, workDate.date)
    )
    .with(
      { activityType: 'issue' },
      ({ activity: { projectId, issueId, activityId } }) =>
        cellKeyByIssue(projectId, issueId, activityId, workDate.date)
    )
    .with(
      { activityType: 'indirect' },
      ({ activity: { indirectActivityId } }) =>
        cellKeyByIndirect(indirectActivityId, workDate.date)
    )
    .exhaustive();

const cellKeyByDataCellInfo = (dataCellInfo: DataCellInfo) =>
  match(dataCellInfo)
    .with({ type: 'project' }, ({ projectId, activityId, date }) =>
      cellKeyByProject(projectId, activityId, date)
    )
    .with({ type: 'issue' }, ({ projectId, issueId, activityId, date }) =>
      cellKeyByIssue(projectId, issueId, activityId, date)
    )
    .with({ type: 'indirect' }, ({ activityId, date }) =>
      cellKeyByIndirect(activityId, date)
    )
    .exhaustive();

type DataCellPositionsByCellKey = {
  [cellKey: string]: { x: number; y: number };
};
const dataCellPositionsByCellKeyState = atom<DataCellPositionsByCellKey>({
  key: 'dataCellPositionsByCellKeyState',
  default: {},
});

const useDataCellsContainerModeState = () => {
  const [containerMode, setContainerMode] = useRecoilState(
    dataCellsContainerModeState
  );

  const toSelectMode = React.useCallback(
    () => setContainerMode('SELECT'),
    [setContainerMode]
  );

  const toModalMode = React.useCallback(
    () => setContainerMode('MODAL'),
    [setContainerMode]
  );

  const toEditMode = React.useCallback(
    () => setContainerMode('EDIT'),
    [setContainerMode]
  );

  return { containerMode, toSelectMode, toModalMode, toEditMode };
};

export const useDataCellsMovableFeature = () => {
  const setCellCursorPosition = useSetRecoilState(focusedDataCellPositionState);

  const moveUp = useRecoilCallback(
    ({ snapshot }) =>
      async (): Promise<void | true> => {
        const pos = await snapshot.getPromise(focusedDataCellPositionState);
        if (!pos) return;
        const dataCells = await snapshot.getPromise(dataCellsState);
        const { x, y } = pos;
        const upCell = dataCells[y - 1]?.[x];
        if (upCell) {
          setCellCursorPosition({ x: x, y: y - 1 });
          return true;
        }
      },
    [setCellCursorPosition]
  );

  const moveDown = useRecoilCallback(
    ({ snapshot }) =>
      async (): Promise<void | true> => {
        const pos = await snapshot.getPromise(focusedDataCellPositionState);
        if (!pos) return;
        const dataCells = await snapshot.getPromise(dataCellsState);
        const { x, y } = pos;
        const downCell = dataCells[y + 1]?.[x];
        if (downCell) {
          setCellCursorPosition({ x: x, y: y + 1 });
          return true;
        }
      },
    [setCellCursorPosition]
  );

  const moveRight = useRecoilCallback(
    ({ snapshot }) =>
      async (): Promise<void | true> => {
        const pos = await snapshot.getPromise(focusedDataCellPositionState);
        if (!pos) return;
        const dataCells = await snapshot.getPromise(dataCellsState);
        const { x, y } = pos;
        const rightCell = dataCells[y]?.[x + 1];
        if (rightCell) {
          setCellCursorPosition({ x: x + 1, y: y });
          return true;
        }
      },
    [setCellCursorPosition]
  );

  const moveLeft = useRecoilCallback(
    ({ snapshot }) =>
      async (): Promise<void | true> => {
        const pos = await snapshot.getPromise(focusedDataCellPositionState);
        if (!pos) return;
        const dataCells = await snapshot.getPromise(dataCellsState);
        const { x, y } = pos;
        const leftCell = dataCells[y]?.[x - 1];
        if (leftCell) {
          setCellCursorPosition({ x: x - 1, y: y });
          return true;
        }
      },
    [setCellCursorPosition]
  );

  return {
    moveUp,
    moveDown,
    moveRight,
    moveLeft,
  };
};

export const useDataCellsUpdator = () => {
  const savedDataCellInfo = React.useRef<DataCellInfo>();

  const saveDataCellInfo = useRecoilCallback(
    ({ snapshot }) =>
      async () => {
        const dataCellInfo = await snapshot.getPromise(focusedDataCellState);
        savedDataCellInfo.current = dataCellInfo;
      },
    []
  );

  const resetDataCellsState = useRecoilTransaction_UNSTABLE(
    ({ set }) =>
      ({
        sortedProjects,
        activitiesById,
        workMonth,
        indirectActivities,
        indirectActivitiesById,
        today,
        mustResetFocus,
      }: {
        sortedProjects: ProjectData[];
        activitiesById: MapById<Activity>;
        workMonth: WorkMonth;
        indirectActivities: IndirectActivities;
        indirectActivitiesById: MapById<Activity>;
        today: string;
        mustResetFocus: React.MutableRefObject<boolean>;
      }) => {
        // データがなければ何もしない
        if (
          sortedProjects.length === 0 &&
          indirectActivities.activityRows.length === 0
        ) {
          set(dataCellsState, []);
          set(dataCellPositionsByCellKeyState, {});
        }

        let dataCells: DataCellInfo[][] = [];
        const cellPositionsByCellKey: DataCellPositionsByCellKey = {};

        let y = -1;
        for (const project of sortedProjects) {
          // プロジェクトのActivityの行を作る
          dataCells = dataCells.concat(
            sortActivities(project.activityRows, activitiesById).map(
              (projectActivity) => {
                y++;
                return workMonth.dates.map<DataCellInfo>((workDate, x) => {
                  const cellKey = cellKeyByProject(
                    project.id,
                    projectActivity.id,
                    workDate.date
                  );
                  cellPositionsByCellKey[cellKey] = { x, y };
                  return {
                    type: 'project',

                    projectId: project.id,
                    activityId: projectActivity.id,
                    date: workDate.date,
                  };
                });
              }
            )
          );

          // 各issueのActivityの行を作る
          for (const issueData of project.dataForIssues) {
            dataCells = dataCells.concat(
              sortActivities(issueData.activityRows, activitiesById).map(
                (issueActivity) => {
                  y++;
                  return workMonth.dates.map<DataCellInfo>((workDate, x) => {
                    const cellKey = cellKeyByIssue(
                      project.id,
                      issueData.id,
                      issueActivity.id,
                      workDate.date
                    );
                    cellPositionsByCellKey[cellKey] = { x, y };
                    return {
                      type: 'issue',
                      projectId: project.id,
                      issueId: issueData.id,
                      activityId: issueActivity.id,
                      date: workDate.date,
                    };
                  });
                }
              )
            );
          }
        }

        // 間接作業のActivityの行を作る
        dataCells = dataCells.concat(
          sortActivities(
            indirectActivities.activityRows,
            indirectActivitiesById
          ).map((indirectActivity) => {
            y++;
            return workMonth.dates.map<DataCellInfo>((workDate, x) => {
              const cellKey = cellKeyByIndirect(
                indirectActivity.id,
                workDate.date
              );
              cellPositionsByCellKey[cellKey] = { x, y };
              return {
                type: 'indirect',
                activityId: indirectActivity.id,
                date: workDate.date,
              };
            });
          })
        );

        set(dataCellsState, dataCells);
        set(dataCellPositionsByCellKeyState, cellPositionsByCellKey);

        if (dataCells.length > 0 && mustResetFocus.current) {
          // 初期位置を計算する
          const todayIndex = workMonth.dates.findIndex(
            ({ date }) => date === today
          );
          const todayCell = dataCells[0]?.[todayIndex];
          set(
            focusedDataCellPositionState,
            todayCell ? { x: todayIndex, y: 0 } : { x: 0, y: 0 }
          );
          mustResetFocus.current = false;
        } else if (savedDataCellInfo.current) {
          // 状態が保存されていた場合はレストアする
          const cellKey = cellKeyByDataCellInfo(savedDataCellInfo.current);
          const pos = cellPositionsByCellKey[cellKey];
          if (pos) {
            set(focusedDataCellPositionState, pos);
          }
        }

        savedDataCellInfo.current = undefined;
      },
    []
  );

  return { resetDataCellsState, saveDataCellInfo };
};

export const useDataCellsFocusableContainer = () => {
  const { moveUp, moveDown, moveRight, moveLeft } =
    useDataCellsMovableFeature();
  const { containerMode, toEditMode, toSelectMode } =
    useDataCellsContainerModeState();

  const handleDigitKey = React.useCallback(() => {
    toEditMode();
    // 入力されたキーの値をそのままinputに入れたいので、falseを返してイベントを止めないようにする
    return false;
  }, [toEditMode]);

  const handleEnter = React.useCallback(() => {
    toEditMode();
    return true;
  }, [toEditMode]);

  const handleFocusableContainerKeyDown = React.useCallback(
    async (event: React.KeyboardEvent) => {
      // キーを処理する関数は、処理を完遂した場合にtrueを返す
      return !!(await match(event.key)
        .with('ArrowUp', moveUp)
        .with('ArrowDown', moveDown)
        .with('ArrowRight', moveRight)
        .with('ArrowLeft', moveLeft)
        .with('Enter', handleEnter)
        .with(
          P.union('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'),
          handleDigitKey
        )
        .otherwise(() => undefined));
    },
    [handleDigitKey, handleEnter, moveDown, moveLeft, moveRight, moveUp]
  );

  const isSelectMode = React.useMemo(
    () => containerMode === 'SELECT',
    [containerMode]
  );

  return { handleFocusableContainerKeyDown, isSelectMode, toSelectMode };
};

export const useFocusableCell = ({
  rowCoordinates,
  workDate,
}: {
  rowCoordinates: RowCoordinates;
  workDate: WorkDate;
}) => {
  const { moveDown, moveRight } = useDataCellsMovableFeature();
  const focusedCell = useRecoilValue(focusedDataCellState);
  const { containerMode, toEditMode, toModalMode, toSelectMode } =
    useDataCellsContainerModeState();

  const isFocused = React.useMemo(() => {
    return match(rowCoordinates)
      .with(
        { activityType: 'project' },
        ({ activity: { projectId, activityId } }) =>
          focusedCell?.type === 'project' &&
          focusedCell?.projectId === projectId &&
          focusedCell?.activityId === activityId &&
          focusedCell?.date === workDate.date
      )
      .with(
        { activityType: 'issue' },
        ({ activity: { issueId, activityId, projectId } }) =>
          focusedCell?.type === 'issue' &&
          focusedCell?.projectId === projectId &&
          focusedCell?.issueId === issueId &&
          focusedCell?.activityId === activityId &&
          focusedCell?.date === workDate.date
      )
      .with(
        { activityType: 'indirect' },
        ({ activity: { indirectActivityId } }) =>
          focusedCell?.type === 'indirect' &&
          focusedCell?.activityId === indirectActivityId &&
          focusedCell?.date === workDate.date
      )
      .otherwise(() => false);
  }, [rowCoordinates, focusedCell, workDate.date]);

  const isEditing = React.useMemo(
    () => isFocused && containerMode === 'EDIT',
    [isFocused, containerMode]
  );

  const setFocusedCellPosition = useSetRecoilState(
    focusedDataCellPositionState
  );
  const moveFocusByRowCoordinateAndWorkDate = useRecoilCallback(
    ({ snapshot }) =>
      async (rowCoordinates: RowCoordinates, workDate: WorkDate) => {
        const cellKey = cellKeyByRowCoordinatesAndWorkDate(
          rowCoordinates,
          workDate
        );
        const cellPositionsByCellKey = await snapshot.getPromise(
          dataCellPositionsByCellKeyState
        );
        const pos = cellPositionsByCellKey[cellKey];
        if (pos) {
          setFocusedCellPosition(pos);
        }
      },
    [setFocusedCellPosition]
  );

  return {
    isFocused,
    isEditing,
    toSelectMode,
    toModalMode,
    toEditMode,
    moveFocusByRowCoordinateAndWorkDate,
    moveDown,
    moveRight,
  };
};
