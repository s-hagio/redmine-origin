import { atom } from 'recoil';

export const TIMESPAN_FORMAT = {
  DECIMAL: 'decimal',
  MINUTES: 'minutes',
} as const;

export type TimespanFormat = typeof TIMESPAN_FORMAT[keyof typeof TIMESPAN_FORMAT];

export const timespanFormatState = atom<TimespanFormat>({
  key: 'timespanFormatState',
  default: TIMESPAN_FORMAT.DECIMAL,
});
