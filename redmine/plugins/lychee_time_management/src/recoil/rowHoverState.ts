import { atom } from 'recoil';

type RowHoverState = {
  projectId?: number;
  issueId?: number;
  activityId?: number;
  indirect?: boolean;
};

export const rowHoverState = atom<RowHoverState>({
  key: 'rowHoverState',
  default: {},
});
