import { atom } from 'recoil';

export type ThemeStyle = {
  contentBackgroundColor: string,
};

export const themeStyleState = atom<ThemeStyle>({
  key: 'themeStyleState',
  default: {
    contentBackgroundColor: 'white',
  },
});
