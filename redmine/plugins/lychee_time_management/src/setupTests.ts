// add custom jest matchers from jest-dom
import '@testing-library/jest-dom';

// jsdomはscrollIntoViewを実装していないので、モック関数をはやしておく
// https://github.com/jsdom/jsdom/issues/1695
Element.prototype.scrollIntoView = jest.fn();
Element.prototype.scrollTo = jest.fn();
