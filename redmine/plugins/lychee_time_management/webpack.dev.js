const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  output: {
    path: path.resolve(common.output.path, 'dev'),
  },
  module: {
    rules: [{ enforce: 'pre', test: /\.js$/, loader: 'source-map-loader' }],
  },
  devtool: 'source-map',
  devServer: {
    devMiddleware: {
      publicPath: '/dev/',
    },
    client: {
      webSocketURL: {
        port: 8080,
      },
    },
  },
});
