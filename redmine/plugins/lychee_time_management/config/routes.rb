# frozen_string_literal: true

namespace :lychee_time_management do
  namespace :api do
    namespace :v1 do
      resources :working_hours, only: %i[create], defaults: { format: 'json' }
    end
  end
end

get '/time_management', to: 'time_management#index', as: 'time_management_index'
get '/time_management/project_calendar', to: 'time_management#index', as: 'time_management_project_calendar'

namespace :lychee_time_management do
  resources :projects, only: %i[index show] do
    resource :issue_tree, only: :show
    resource :filter, only: :show
  end
  resources :issues, only: %i[index update] do
    get :mine, on: :collection, as: :my
    member do
      get :allowed_statuses
    end
  end
  resources :versions, only: %i[index]
  resources :activities, only: %i[index]
  resources :indirect_activities, only: %i[index]
  resources :work_plans do
    post 'derive_from_issues', on: :collection
    post 'derive_from_group_issues', on: :collection
    get 'derivable_from_issues', on: :collection
  end
  resources :ltm_time_entries
  resources :principals, only: :index
  resources :attachments, only: %i[show create destroy]
end

namespace :table_mode do
  resources :time_entries, only: %i[index create update destroy]
  resources :custom_fields, only: :index
  resources :project_filters, only: :index
  resources :projects, only: :index do
    get :inherit, on: :collection
  end
  resources :filters, only: :index
  resources :issues, only: :index
  resources :activities, only: :index
end

resource :ltm_google_calendar_syncs, only: %i[edit destroy] do
  collection do
    get 'initiate_oauth_flow'
    get 'callback'
  end
end
