namespace :redmine do
  namespace :plugins do
    namespace :lychee_ccpm do
      desc 'Aggregate done_ratio and buffer consumption rate'
      task aggregate_consumption_rates: :environment do
        Project.aggregate_consumption_rates
      end

      desc 'Calculate due date from remaining estimate for issues'
      task calculate_due_date_for_issues: :environment do
        Issue.calculate_due_date_for_issues
      end
    end
  end
end
