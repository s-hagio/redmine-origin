resources :root_fever_charts, only: [:index]
resources :projects, only: [] do
  resources :fever_charts, only: [:index]
  resource :ccpm, controller: 'lychee_ccpm_projects', only: [:update] do
    get :calc_buffer_days
    get :reflect_project_critical_path
    get :calc_version_buffer_days
    get :reflect_version_critical_path
    get :calc_buffer_start_date
    get :calc_version_buffer_start_date
    post :check_warning
  end
end
