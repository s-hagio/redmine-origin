function ccpmSetItem(prevItem) {
  sessionStorage.setItem("ccpm_prev_item", JSON.stringify(prevItem, null));
}

function ccpmGetItem() {
  const ccpmItem = sessionStorage.getItem('ccpm_prev_item');
  ccpmClearItem();
  return JSON.parse(ccpmItem);
}

function ccpmClearItem() {
  sessionStorage.removeItem('ccpm_prev_item');
}

function ccpmItemChanged(prevItem, currentItem) {
  if(!prevItem || !currentItem) return false;
  if(prevItem.id !== currentItem.id) return false;
  if(!prevItem.dueDate || prevItem.dueDate === '') return false;
  const remainingChanged = parseFloat(currentItem.remaining).toFixed(3) !== parseFloat(prevItem.remaining).toFixed(3);
  const dueDateChanged = prevItem.dueDate !== currentItem.dueDate;
  const startDateChanged = prevItem.startDate !== currentItem.startDate;

  return (dueDateChanged && (remainingChanged || startDateChanged));
}
