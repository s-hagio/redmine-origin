$(function(){
  function calculateBufferDays(obj) {
    var bufferDate = $('#' + obj.data('bufferStartDateTarget'));
    var bufferDays = $('#' + obj.data('bufferDaysTarget'));
    var opertingDays = $('#' + obj.data('operatingDaysTarget'));
    var effectiveDate = $('#' + obj.data('effectiveDateTarget'));
    var bufferPercentage = $('#' + obj.data('bufferPercentageTarget'));
    var versionCcpm = $('#' + obj.data('versionCcpmTarget'));
    var calcBufferSetting = bufferDate.data('calcBuffer');
    var data = {
      buffer_start_date: bufferDate.val(),
      effective_date: effectiveDate.val(),
      version_ccpm_id: versionCcpm.val(),
    };
    if(obj.data('ccpmIdTarget')){
      var ccpmId = $('#' + obj.data('ccpmIdTarget'));
      data.ccpm_attributes_id = ccpmId.val();
    }
    hideErrorMessage()
    $.ajax({
      url: obj.data('path'),
      data: data,
      dataType: 'json',
      success: function(res) {
        bufferDays.text(res.buffer_days);
        opertingDays.text(res.operating_days);
        bufferPercentage.val(calculateBufferPercentage(res.buffer_days, res.operating_days));
      },
      error: function(xhr) {
        showErrorMessage(obj.data('errorMessage'));
        bufferDays.text(0);
        bufferPercentage.val(0);
      }
    });
  }

  function reflectCriticalPath(obj) {
    var bufferDate = $('#' + obj.data('bufferStartDateTarget'));
    var bufferDays = $('#' + obj.data('bufferDaysTarget'));
    var effectiveDate = $('#' + obj.data('effectiveDateTarget'));
    var bufferPercentage = $('#' + obj.data('bufferPercentageTarget'));
    var form = bufferDate.closest('form');
    var data = {
      effective_date: effectiveDate.val(),
    };
    if(obj.data('ccpmIdTarget')){
      var ccpmId = $('#' + obj.data('ccpmIdTarget'));
      data.ccpm_attributes_id = ccpmId.val();
    }
    hideErrorMessage()
    $.ajax({
      url: obj.data('path'),
      data: data,
      dataType: 'json',
      success: function(res) {
        bufferDate.val(res.buffer_start_date);
        bufferDays.text(res.buffer_days);
        bufferPercentage.val(calculateBufferPercentage(res.buffer_days, res.operating_days));
      },
      error: function(xhr) {
        showErrorMessage(obj.data('errorMessage'));
        bufferDate.val('');
        bufferDays.text(0);
        bufferPercentage.val(0);
      }
    });
  }

  function calculateBufferStartDate(obj) {
    var bufferDate = $('#' + obj.data('bufferStartDateTarget'));
    var bufferDays = $('#' + obj.data('bufferDaysTarget'));
    var effectiveDate = $('#' + obj.data('effectiveDateTarget'));
    var opertingDays = $('#' + obj.data('operatingDaysTarget'));
    var bufferPercentage = $('#' + obj.data('bufferPercentageTarget'));

    opertingDaysCount = isNaN(opertingDays.text()) ? 0 : opertingDays.text();
    bufferPercentageValue = isNaN(bufferPercentage.val()) ? 0 : bufferPercentage.val();

    var data = {
      buffer_start_day: calculateBufferStartDay(opertingDaysCount, bufferPercentageValue),
      effective_date: effectiveDate.val(),
    };
    if(obj.data('ccpmIdTarget')){
      var ccpmId = $('#' + obj.data('ccpmIdTarget'));
      data.ccpm_attributes_id = ccpmId.val();
    }
    hideErrorMessage()
    $.ajax({
      url: obj.data('path'),
      data: data,
      dataType: 'json',
      success: function(res) {
        bufferDate.val(res.buffer_start_date);
        bufferDays.text(res.buffer_days);
      },
      error: function(xhr) {
        showErrorMessage(obj.data('errorMessage'));
        bufferDays.text(0);
        bufferPercentage.val(0);
      }
    });
  }

  function calculateBufferStartDay(operatingDays, bufferPercentage){
    if (bufferPercentage <= 0){
      return operatingDays - 1;
    } else {
      return Math.floor(parseFloat(operatingDays) * ((100 - parseFloat(bufferPercentage)) / 100));
    }
  }

  function calculateBufferPercentage(bufferDays, operatingDays) {
    if (bufferDays == 0 || operatingDays == 0) {
      return 0;
    } else {
      return Math.round(parseFloat(bufferDays) / parseFloat(operatingDays) * 100);
    }
  };

  function ccpmUpdateConfirm(messages) {
    return messages.every(confirm);
  }

  function hideErrorMessage() {
    $('#errorExplanation').hide();
  }

  function showErrorMessage(message) {
    var elemId = 'errorExplanation';
    var selector = '#' + elemId;
    if ($('form#project_ccpm_form').find(selector).length === 0) {
      var errorMessageElem = $('<p>', { id: elemId });
      $('form#project_ccpm_form').find('fieldset').first().before(errorMessageElem);
    }
    $(selector).text(message).show();
  }

  $('form#project_ccpm_form').submit(function() {
    var form = $('form#project_ccpm_form');
    hideErrorMessage();
    $.ajax({
      method: 'POST',
      url: form.data('checkWarningPath'),
      data: form.serializeArray().filter(function(obj){ return obj['name'] != '_method' }),
      dataType: 'json',
    }).done(function(res) {
      if(ccpmUpdateConfirm(res)) {
        form[0].submit();
      }
    }).fail(function(xhr) {
      showErrorMessage(form.data('checkWarningErrorMessage'));
    });

    return false;
  });

  $('#ccpm_project_end_date').on('change', function() {
    calculateBufferDays($(this));
  });

  $('.calculate_buffer_days').on('change', function() {
    calculateBufferDays($(this));
    return false;
  });

  $('.calculate_buffer_start_date').on('change', function() {
    calculateBufferStartDate($(this));
    return false;
  });

  $('.calculate_buffer_start_date').on('keydown', function(event) {
    if (event.key === "Enter"){
      calculateBufferStartDate($(this));
      return false;
    }
    // 数値、小数点および、event.key: "Tab", "Backspace" など key値が2文字以上のものは入力可。その他は入力不可。
    else if (event.key.match(/[0-9.]+/) || event.key.length > 2 ){
      return;
    }
    else {
      return false;
    }
  });

  $('.reflect_critical_path').on('click', function() {
    reflectCriticalPath($(this));
    return false;
  });
});
