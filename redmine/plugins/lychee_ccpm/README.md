Redmineのルートディレクトリで下記を実行すると cron 設定ファイルに書き込まれる。

```console
$ bundle exec whenever -f plugins/lychee_ccpm/config/schedule.rb --update-crontab
```
