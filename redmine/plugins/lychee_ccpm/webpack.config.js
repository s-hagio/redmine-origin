const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path')

module.exports = {
  entry: path.resolve(__dirname, './app/javascript/packs/lychee_ccpm.js'),
  output: {
    path: path.resolve(__dirname, './assets/javascripts'),
    filename: 'lychee_ccpm.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015']
      }
    }, {
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        fallbackLoader: 'style-loader',
        loader: 'css-loader'
      })
    }, {
      test: /\.(png|gif)$/, use: 'url-loader'
    }]
  }
};
