import Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting';

Exporting(Highcharts);
window.Highcharts = Highcharts;

Highcharts.setOptions({
  global: {
    useUTC: false
  }
});

if (document.documentElement.lang === 'ja') {
  Highcharts.setOptions({
    lang: {
      printChart: "印刷",
      downloadJPEG: "JPEGでダウンロード",
      downloadPDF: "PDFでダウンロード",
      downloadPNG: "PNGでダウンロード",
      downloadSVG: "SVGでダウンロード",
    },
  });
}
