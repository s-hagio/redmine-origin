resources :projects do
  namespace :lgc do
    resources :plans, only: %w( show create destroy )
    resources :milestones, shallow: true, only: %w( index create update destroy )
    resources :critical_paths, only: %w( index )
    resources :editings, shallow: true, only: %w( index create update destroy )
  end
end

namespace :lgc do
  resource :edits, only: :create
  resources :editings, only: %w( index create )
end

namespace :lychee do
  namespace :api do
    namespace :v1 do
      resources :projects, only: [], shallow: true do
        resources :milestones, except: %w( new edit ), defaults: { format: 'json' }
        resources :baselines, only: %w( create ), defaults: { format: 'json' }
      end
    end
  end
end
