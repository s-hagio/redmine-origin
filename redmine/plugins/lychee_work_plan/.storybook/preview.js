import { RecoilRoot } from "recoil";
import i18n from "@/i18n";
import "@/index.css";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  i18n,
  locale: "ja",
  locales: {
    en: "English",
    ja: "日本語",
  },
};
