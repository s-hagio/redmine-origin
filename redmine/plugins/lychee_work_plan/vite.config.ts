import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  },
  build: {
    outDir: "assets/bundle",
    assetsDir: "",
    rollupOptions: {
      input: "src/main.tsx",
    },
    manifest: true,
  },
  server: {
    host: "0.0.0.0",
    port: 8090,
  },
});
