module.exports = {
  mode: "jit",
  content: ["./src/**/*.{ts,tsx}"],
  darkMode: "media", // or 'class'
  theme: {
    extend: {
      colors: {
        'lychee-blue': '#116699',
        'effort-blue': '#dee8ec',
        'effort-blue-editable': '#c5d6dd',
      },
    },
  },
  variants: {
    extend: {
      backgroundColor: ["odd", "even"],
    },
  },
  plugins: [],
};
