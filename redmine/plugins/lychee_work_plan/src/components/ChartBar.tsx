import styled from "styled-components";

const Absolute = styled.div.attrs<Pick<Props, "posX" | "posY">>(
  ({ posX, posY }) => ({
    style: {
      "--pos-x": `${posX}px`,
      "--pos-y": `${posY}px`,
    },
  })
)<Pick<Props, "posX" | "posY">>`
  position: absolute;
  transform: translate(var(--pos-x), var(--pos-y));
`;

const Rectangle = styled.div<Pick<Props, "width" | "height">>`
  width: ${(props) => props.width}px;
  height: ${(props) => props.height}px;
`;

type Props = {
  posX: number;
  posY: number;
  width: number;
  height: number;
  className?: string;
  children: React.ReactNode;
};

export const ChartBar: React.FC<Props> = ({
  posX,
  posY,
  width,
  height,
  className,
  children,
}) => {
  const style = {
    boxShadow: "rgb(204 204 204) 0px 0px 3px 0px, rgb(204 204 204 / 15%) 3px 3px 3px 1px",
  };
  return (
    <Absolute posX={posX} posY={posY}>
      <Rectangle width={width} height={height}>
        <div className={`ml-px h-full rounded bg-effort-blue ${className}`} style={style}>
          {children}
        </div>
      </Rectangle>
    </Absolute>
  );
};
