import React from "react";
import { useTranslation } from "react-i18next";
import { useRecoilValue, useSetRecoilState, useResetRecoilState } from "recoil";
import styled from "styled-components";
import { eachDayOfInterval, format, formatISO, isToday } from "date-fns";
import ja from "date-fns/locale/ja";
import { sumCalendarEventHours } from "@/lib/calendarEvent";
import { intervalState } from "@/recoil/intervalState";
import { barWidthState } from "@/recoil/barWidthState";
import { effortsByDateState } from "@/recoil/effortsByDateState";
import { issuesByDateState } from "@/recoil/issuesByDateState";
import { calendarEventHoursAryByDateState } from "@/recoil/calendarEventHoursAryByDateState";
import { dayHoursState } from "@/recoil/dayHoursState";
import { availableMicrosoftGraphCalendarState } from "@/recoil/availableMicrosoftGraphCalendarState";
import { selectedIssueIdState } from "@/recoil/selectedIssueIdState";
import { selectedDateEffortsState } from "@/recoil/selectedDateEffortsState";
import { useRestday } from "@/hooks/useRestday";
import { useFormatHours } from "@/hooks/useFormatHours";
import { DATE_FORMAT } from "@/constants";

type DatesWithMonth = {
  month: string;
  dates: Date[];
};
const packDatesWithMonths = (dates: Date[]): DatesWithMonth[] => {
  const months: { [key: string]: Date[] } = {};
  dates.forEach((date: Date) => {
    const key = format(date, 'yyyy-MM');
    months[key] ||= [];
    months[key].push(date);
  });
  return Object.keys(months).map((key: string) => ({
    month: key,
    dates: months[key],
  }));
};

const cautionIcon = new URL("../assets/icn_caution_circle.png", import.meta.url)
  .href;
const waveIcon = new URL("../assets/icn_doublewave.png", import.meta.url).href;

const graphScale = 0.8;
const graphHeight = 120;

const DayHeader = styled.div<{
  barWidth: number;
  isHoliday: boolean;
}>`
  display: flex;
  flex-direction: column;
  min-width: ${(props) => props.barWidth}px;
  height: 35px;
  border-top: 1px solid #ebebeb;
  border-right: 1px solid #ebebeb;
  border-bottom: 1px solid #ebebeb;
  background-color: ${(props) => (props.isHoliday ? "#ebebeb" : "#ffffff")};
  text-align: center;
`;

const GraphColumn = styled.div<{
  barWidth: number;
}>`
  display: flex;
  flex-direction: column;
  min-width: ${(props) => props.barWidth}px;
  height: ${graphHeight}px;
  border-bottom: 1px solid #ebebeb;
  background-color: #ffffff;
  text-align: center;
`;

const GraphLine = styled.div<{
  value: number;
  dayHours: number;
}>`
  position: absolute;
  bottom: 0;
  width: 24px;
  height: ${(props) =>
    (graphHeight * graphScale * props.value) / props.dayHours}px;
  background-color: #d3e5ef;
  border-radius: 4px 4px 0 0;
`;

const Border = styled.div`
  position: absolute;
  bottom: 0;
  width: 100%;
  height: ${graphHeight * graphScale}px;
  border-top: 2px solid #f6cccc;
`;

export const ChartHeader: React.ForwardRefExoticComponent<
  React.RefAttributes<HTMLDivElement>
> = React.forwardRef((_, todayRef) => {
  const { i18n } = useTranslation();
  const { isRestday } = useRestday();
  const interval = useRecoilValue(intervalState);
  const barWidth = useRecoilValue(barWidthState);
  const dayHours = useRecoilValue(dayHoursState);
  // 年月表示分の24pxはグラフを表示できる
  const maxDayHours = dayHours / graphScale + 24 / (graphHeight / dayHours);
  const formatHours = useFormatHours();
  const availableMicrosoftGraphCalendar = useRecoilValue(
    availableMicrosoftGraphCalendarState
  );

  const effortsByDate = useRecoilValue(effortsByDateState);
  const issuesByDate = useRecoilValue(issuesByDateState);
  const calendarEventHoursAryByDate = useRecoilValue(
    calendarEventHoursAryByDateState
  );
  const resetSelectedIssueId = useResetRecoilState(selectedIssueIdState);
  const setSelectedDateEfforts = useSetRecoilState(selectedDateEffortsState);

  const handleDateClick = availableMicrosoftGraphCalendar
    ? (date: Date) => (): void => {
        resetSelectedIssueId();
        setSelectedDateEfforts(date);
      }
    : (_date: Date) => (): void => {};
  const dates = eachDayOfInterval(interval);
  const months = packDatesWithMonths(dates);

  return (
    <>
      <div className="sticky top-0 z-30">
        <div
          className="relative flex"
          style={{ width: barWidth * dates.length }}
        >
          {months.map(({ month, dates }: DatesWithMonth) => (
            <div key={month} className="flex">
              <div
                className="sticky top-0 left-1 h-6 whitespace-nowrap text-base text-[#707070]"
                style={{ minWidth: barWidth }}
              >
                {format(dates[0], "yyyy年MM月")}
              </div>
              {dates.slice(1).map((date: Date) => (
                <div key={formatISO(date)} style={{ minWidth: barWidth }}></div>
              ))}
            </div>
          ))}
        </div>
      </div>
      <div className={`sticky top-6 z-20 flex h-[${graphHeight}px] leading-6`}>
        {eachDayOfInterval(interval).map((date) => {
          const dateKey = format(date, DATE_FORMAT);
          const efforts = effortsByDate.get(dateKey) ?? [];
          const rawEffortsValue = efforts.reduce(
            (sum, i) => sum + (i.value || 0),
            0
          );
          const eventHoursAry = calendarEventHoursAryByDate.get(dateKey) ?? [];
          const rawSumCalendarEventHours = sumCalendarEventHours(eventHoursAry);
          const rawValue = rawEffortsValue + rawSumCalendarEventHours;
          const value = Math.round(rawValue * 100) / 100;
          const issues = issuesByDate.get(dateKey) ?? [];
          const entered = issues.reduce(
            (result, i) => result && !!efforts.find((e) => e.issueId === i.id),
            true
          );
          return (
            <GraphColumn
              key={`graph${formatISO(date)}`}
              ref={isToday(date) ? todayRef : null}
              barWidth={barWidth}
            >
              <Border />
              <div className="flex items-center justify-center py-[10px]">
                <GraphLine
                  key={formatISO(date)}
                  ref={isToday(date) ? todayRef : null}
                  value={value}
                  dayHours={dayHours}
                />
                {value > maxDayHours ? (
                  <img src={waveIcon} className="absolute top-0 z-20" />
                ) : (
                  ""
                )}
                <div className="absolute bottom-1 flex min-w-[16px] items-center justify-center text-center text-base">
                  {!entered && !isRestday(date) ? (
                    <img src={cautionIcon} className="h-[16px] w-[16px]" />
                  ) : (
                    ""
                  )}
                  <p data-date={dateKey}>{formatHours(value)}</p>
                </div>
              </div>
            </GraphColumn>
          );
        })}
      </div>
      <div
        className="sticky z-10 flex h-[35px] leading-6"
        style={{ top: graphHeight + 24 }}
      >
        {eachDayOfInterval(interval).map((date) => {
          return (
            <DayHeader
              key={formatISO(date)}
              ref={isToday(date) ? todayRef : null}
              barWidth={barWidth}
              isHoliday={isRestday(date)}
            >
              <div
                className={`flex items-center justify-center space-x-2 border-b py-[5px] text-xs text-[#707070] ${
                  availableMicrosoftGraphCalendar && "cursor-pointer"
                }`}
                onClick={handleDateClick(date)}
              >
                <div
                  className={`flex h-6 w-6 content-center items-center rounded-full text-sm ${
                    isToday(date) && "bg-lychee-blue text-white"
                  }`}
                >
                  <div className="flex-1">{format(date, "d")}</div>
                </div>
                <div className="">
                  {format(date, "(eee)", {
                    locale: i18n.language === "ja" ? ja : undefined,
                  })}
                </div>
              </div>
            </DayHeader>
          );
        })}
      </div>
    </>
  );
});
ChartHeader.displayName = "ChartHeader";
