import { useRecoilValue } from "recoil";
import { issueRowsState } from "@/recoil/issueRowsState";
import { IssueBar } from "@/components/IssueBar";
import { WORKLOAD_DAY_HEIGHT } from "@/constants";

type Props = {
  project: {
    id: number;
    name: string;
  };
};

export const ProjectIssueRows: React.FC<Props> = ({ project }) => {
  const issueRows = useRecoilValue(issueRowsState(project.id));

  return (
    <>
      <div className="sticky left-[0.5rem] p-3 text-sm text-lychee-blue font-bold">
        {project.name}
      </div>
      <div
        className="w-full"
        style={{
          height: `${21 + (WORKLOAD_DAY_HEIGHT + 7) * issueRows.length}px`,
        }}
      >
        {[...issueRows]
          .reverse() // for Popover z-index
          .flatMap((row, index) =>
            row.issues.map((issue) => (
              <IssueBar
                key={
                  issue.isVirtual
                    ? `virtual-${issue.id}-${issue.startDate}`
                    : issue.id
                }
                issue={issue}
                index={issueRows.length - index - 1}
              />
            ))
          )}
      </div>
      <div className="sticky left-0 w-full border" />
    </>
  );
};
