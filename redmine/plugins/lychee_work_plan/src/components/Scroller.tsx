import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/solid";

type Props = {
  direction: "previous" | "next";
  onClick: () => void;
};

export const Scroller: React.FC<Props> = ({ direction, onClick }) => {
  const Component =
    direction === "previous" ? ChevronLeftIcon : ChevronRightIcon;

  return (
    <div
      className={`absolute top-0 z-20 h-full w-12 ${
        direction === "previous" ? "left-5" : "right-5"
      }`}
    >
      <div className="flex h-full items-center">
        <Component
          data-direction={direction}
          className="z-10 h-12 w-12 cursor-pointer hover:bg-gray-300 hover:bg-opacity-50"
          onClick={onClick}
        />
      </div>
    </div>
  );
};
