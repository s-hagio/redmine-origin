import React from "react";
import { useRecoilValue, useSetRecoilState, useResetRecoilState } from "recoil";
import {
  differenceInCalendarDays,
  eachDayOfInterval,
  formatISO,
  isFuture,
  parseISO,
} from "date-fns";
import { barWidthState } from "@/recoil/barWidthState";
import { issuePosXState } from "@/recoil/issuePosXState";
import { effortsByIssueIdState } from "@/recoil/effortsState";
import { selectedIssueIdState } from "@/recoil/selectedIssueIdState";
import { selectedDateEffortsState } from "@/recoil/selectedDateEffortsState";
import { targetUserState } from "@/recoil/targetUserState";
import { Issue } from "@/recoil/issueState";
import { WORKLOAD_DAY_HEIGHT } from "@/constants";
import { ChartBar } from "@/components/ChartBar";
import { IssueBarMenu } from "@/components/IssueBarMenu";
import { EffortBox } from "@/components/EffortBox";
import { createClient } from "@/api/client";

export const IssueBar: React.FC<{
  issue: Issue;
  index: number;
}> = ({ issue, index }) => {
  const ref = React.useRef<HTMLDivElement>(null);
  const barWidth = useRecoilValue(barWidthState);
  const issuePosX = useRecoilValue(issuePosXState(issue.startDate));
  const setEfforts = useSetRecoilState(effortsByIssueIdState(issue.id));
  const setSelectedIssueId = useSetRecoilState(selectedIssueIdState);
  const targetUser = useRecoilValue(targetUserState);
  const resetSelectedDateEfforts = useResetRecoilState(
    selectedDateEffortsState
  );
  const startDate = parseISO(issue.startDate);
  const dueDate = parseISO(issue.dueDate);
  const dayDiff = differenceInCalendarDays(dueDate, startDate) + 1;
  const client = createClient();

  const handleIssueClick = (): void => {
    resetSelectedDateEfforts();
    setSelectedIssueId(issue.id);
    if (ref.current) {
      ref.current.scrollIntoView({
        behavior: "smooth",
        block: "center",
        inline: "center",
      });
    }
  };

  const handleAutofillClick = async (): Promise<void> => {
    const { body } = await client.lychee_work_plan.issues
      ._issueId(issue.id)
      .autofill.post({ body: { target_user_id: targetUser.id }});
    if (body.success) {
      setEfforts(body.efforts);
    }
    return;
  };

  return (
    <ChartBar
      posX={issuePosX}
      posY={9 + index * (WORKLOAD_DAY_HEIGHT + 7)}
      width={barWidth * dayDiff - 1}
      height={WORKLOAD_DAY_HEIGHT}
      className={issue.isVirtual ? "opacity-50" : ""}
    >
      <div
        ref={ref}
        className="h-full"
        data-issue-id={issue.id}
        {...(issue.isVirtual && { "data-issue-type": "virtual" })}
      >
        <div className="h-full text-lychee-blue">
          <div
            className="flex h-3/5 cursor-pointer flex-col p-[0.4rem_0.4rem]"
            onClick={handleIssueClick}
          >
            {issue.parentSubject && (
              <div className="flex-1 truncate pb-[5px] text-xs text-[#555555]">
                {issue.parentSubject}
              </div>
            )}
            <div className="flex flex-1 truncate text-xs font-semibold">
              <div className="truncate">{`#${issue.id} ${issue.subject}`}</div>
            </div>
          </div>
          <div className="flex h-2/5">
            {eachDayOfInterval({ start: startDate, end: dueDate }).map(
              (date) => (
                <EffortBox
                  key={formatISO(date)}
                  issueId={issue.id}
                  date={date}
                />
              )
            )}
          </div>
        </div>
        {!issue.isVirtual && isFuture(parseISO(issue.startDate)) && (
          <IssueBarMenu
            className="absolute right-0 top-0 w-4"
            onAutofillClick={handleAutofillClick}
          />
        )}
      </div>
    </ChartBar>
  );
};
