import React from "react";
import { useTranslation } from "react-i18next";
import { useRecoilValue } from "recoil";
import { format } from "date-fns";
import styled from "styled-components";
import { DATE_FORMAT } from "@/constants";
import { calendarEventHoursAryByDateState } from "@/recoil/calendarEventHoursAryByDateState";
import { timespanFormatState } from "@/recoil/timespanFormatState";
import { CalendarEventHours, sumCalendarEventHours } from "@/lib/calendarEvent";

const clockIcon = new URL("../assets/icn_clock.png", import.meta.url).href;

const Text = styled.div`
  font-family: Inter;
  font-style: normal;
  font-weight: 700;
  color: #333;
`;

const LightText = styled(Text)`
  color: #7a7a7a;
`;

const Statistic: React.FC<{
  headerText: string;
  valueText: string;
}> = ({ headerText, valueText }) => (
  <div className="flex flex-row flex-wrap gap-[4px] items-center p-0">
    <LightText className="text-[11px] leading-[13px]">{headerText}</LightText>
    <Text className="text-[11px] leading-[13px]">{valueText}</Text>
  </div>
);

type Props = {
  date: Date;
};
export const DateEffortsInfo: React.FC<Props> = ({ date }) => {
  const { t } = useTranslation();
  const timespanFormat = useRecoilValue(timespanFormatState);
  const calendarEventHoursAryByDate = useRecoilValue(calendarEventHoursAryByDateState);
  const eventHoursAry = calendarEventHoursAryByDate.get(format(date, DATE_FORMAT)) ?? [];

  const formatHours = (hours: number) => {
    const hoursPart = Math.floor(hours);
    const minutesPart = Math.round((hours - hoursPart) * 60);
    const formatType =
      minutesPart === 0 ? "Hours" :
      hoursPart === 0 ? "Minutes" :
      "HoursAndMinutes";
    return t(`${timespanFormat}TimeSpan${formatType}Format`, { hours, hoursPart, minutesPart });
  };

  const formatTime = (date: Date) => format(date, "H:mm");

  const formatEventTime = (calendarEventHours: CalendarEventHours) => {
    const formattedTimeRange =
      calendarEventHours.isWholeWorkDay ? t("allDay") :
      `${formatTime(calendarEventHours.start)}-${formatTime(calendarEventHours.end)}`;
    const formattedHours = formatHours(calendarEventHours.hours);
    return `${formattedTimeRange}（${formattedHours}）`;
  };

  return (
    <div className="mb-1 h-full w-full overflow-y-scroll border p-4 flex flex-col gap-[8px]">
      <Text className="text-[13px] leading-[16px]">
        {format(date, t("dateFormat"))}
      </Text>
      <div className="flex flex-row flex-wrap gap-[8px] p-0">
        <Text className="rounded-[6px] bg-[#f0f2f4] px-[8px] py-[4px] text-[10px] leading-[12px]">
          {t("externalCalendarEvents")}
        </Text>
        <div className="flex flex-row flex-wrap gap-[8px]">
          <Statistic
            headerText={t("numberOfCalendarEvents")}
            valueText={
              t("numberOfCalendarEventsFormat", { value: eventHoursAry.length })
            }
          />
          <Statistic
            headerText={t("totalTime")}
            valueText={formatHours(sumCalendarEventHours(eventHoursAry))}
          />
        </div>
      </div>
      <div className="flex flex-col gap-[8px] items-start p-0">
        {eventHoursAry.map((eventHours) => (
          <div
            className="flex flex-col gap-[6px] items-start w-full box-border justify-center border border-solid border-[#a9b3bd] rounded-[4px] p-[16px]"
            key={eventHours.id}
          >
            <Text className="text-[13px] leading-[16px] break-all">
              {eventHours.calendarEvent.subject}
            </Text>
            <div className="flex flex-row gap-[4px] items-center p-0">
              <img src={clockIcon} className="flex-none grow-0 h-[14px] w-[14px]" />
              <Text className="text-[12px] font-[500] leading-[15px]">
                {formatEventTime(eventHours)}
              </Text>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
