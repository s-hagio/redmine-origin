import React from "react";
import { Popover, Transition } from "@headlessui/react";
import { DotsVerticalIcon } from "@heroicons/react/solid";

type Props = {
  selected: "uniform" | "flexible" | null;
  onTypeChange: (type: "uniform" | "flexible") => void;
  className?: string;
};

export const EffortTypeSelector: React.FC<Props> = ({
  selected,
  onTypeChange,
  className,
}) => (
  <div className={className}>
    <Popover className="relative inline-block h-full">
      <Popover.Button
        className="flex h-full w-2 items-center justify-center rounded-r border border-blue-200 bg-blue-200"
        tabIndex={-1}
      >
        <DotsVerticalIcon className="h-5 w-5 flex-shrink-0 text-blue-400" />
      </Popover.Button>
      <Transition
        as={React.Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Popover.Panel className="absolute left-3 top-0">
          <div className="overflow-hidden rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
            <div className="whitespace-nowrap bg-white py-1">
              <div className="p-1">時間の割り振り</div>
              <EffortType
                type="uniform"
                label="均等割"
                selected={selected}
                onTypeChange={onTypeChange}
              />
              <EffortType
                type="flexible"
                label="入力"
                selected={selected}
                onTypeChange={onTypeChange}
              />
            </div>
          </div>
        </Popover.Panel>
      </Transition>
    </Popover>
  </div>
);

const EffortType: React.FC<{
  type: "uniform" | "flexible";
  label: string;
  selected: Props["selected"];
  onTypeChange: Props["onTypeChange"];
}> = ({ type, selected, onTypeChange, label }) => {
  if (type === selected) {
    return (
      <div className="block bg-blue-300 p-1 text-center hover:bg-blue-200">
        {label}
      </div>
    );
  }

  return (
    <a
      className="block p-1 text-center hover:bg-blue-200"
      onClick={() => {
        onTypeChange(type);
      }}
    >
      {label}
    </a>
  );
};
