import React from "react";
import { useRecoilValue } from "recoil";
import { defaultScrollLeftState } from "@/recoil/defaultScrollLeftState";
import { intervalState } from "@/recoil/intervalState";
import { useInterval } from "@/hooks/useInterval";
import { Scroller } from "@/components/Scroller";

type Props = {
  children: React.ReactNode;
};

export const ScrollContainer: React.FC<Props> = ({ children }) => {
  const ref = React.useRef<HTMLDivElement>(null);
  const loadingRef = React.useRef(false);
  const defaultScrollLeft = useRecoilValue(defaultScrollLeftState);
  const { start, end } = useRecoilValue(intervalState);
  const { addPreviousWeek, addNextWeek } = useInterval();

  React.useLayoutEffect(() => {
    const element = ref.current;
    if (element) {
      element.scrollTo({ left: element.scrollLeft + defaultScrollLeft });
    }
  }, [start]);

  React.useEffect(() => {
    loadingRef.current = false;
  }, [start, end]);

  const handleScroll = (): void => {
    if (loadingRef.current) {
      return;
    }
    const container = ref.current;
    if (!container) {
      return;
    }

    if (container.scrollLeft <= defaultScrollLeft / 2) {
      loadingRef.current = true;
      addPreviousWeek();
    }
    if (
      container.scrollLeft + container.clientWidth + defaultScrollLeft / 2 >=
      container.scrollWidth
    ) {
      loadingRef.current = true;
      addNextWeek();
    }
  };

  const handlePreviousClick = (): void => {
    if (ref.current) {
      ref.current.scrollTo({
        left: ref.current.scrollLeft - defaultScrollLeft / 2,
        behavior: "smooth",
      });
    }
  };
  const handleNextClick = (): void => {
    if (ref.current) {
      ref.current.scrollTo({
        left: ref.current.scrollLeft + defaultScrollLeft / 2,
        behavior: "smooth",
      });
    }
  };

  return (
    <div className="relative h-full">
      <Scroller direction="previous" onClick={handlePreviousClick} />
      <div
        ref={ref}
        className="relative h-full overflow-scroll border"
        style={{ overscrollBehaviorX: "none" }}
        onScroll={handleScroll}
      >
        {children}
      </div>
      <Scroller direction="next" onClick={handleNextClick} />
    </div>
  );
};
