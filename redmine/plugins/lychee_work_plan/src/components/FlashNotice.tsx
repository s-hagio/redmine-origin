import React from "react";
import { useRecoilValue } from "recoil";
import { noticeMessageState } from "@/recoil/noticeMessageState";

export const FlashNotice: React.FC = () => {
  const noticeMessage = useRecoilValue(noticeMessageState);
  return noticeMessage ? (
    <div className="flash notice">
      {noticeMessage}
    </div>
  ) : null;
};
