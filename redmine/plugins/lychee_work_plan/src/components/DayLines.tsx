import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";
import { formatISO } from "date-fns";
import { datesState } from "@/recoil/datesState";
import { barWidthState } from "@/recoil/barWidthState";
import { dayLineLengthState } from "@/recoil/dayLineLengthState";
import { useRestday } from "@/hooks/useRestday";

const Line = styled.div.attrs<{ posX: number }>((props) => ({
  style: {
    "--pos-x": `${props.posX}px`,
  },
}))<{ posX: number; height: number }>`
  position: absolute;
  transform: translateX(var(--pos-x));
  ${/* 90px is ChartHeader height */ ""}
  min-height: calc(100% - 90px);
  height: ${(props) => props.height}px;
  width: 1px;
  background-color: #ebebeb;
`;

const HolidayArea = styled(Line)<{
  width: number;
  height: number;
}>`
  width: ${(props) => props.width}px;
  background-color: rgba(0, 0, 0, 0.04);
`;

export const DayLines: React.FC = () => {
  const { isRestday } = useRestday();
  const dates = useRecoilValue(datesState);
  const barWidth = useRecoilValue(barWidthState);
  const posX = (index: number): number => index * barWidth - 1;

  const height = useRecoilValue(dayLineLengthState);

  return (
    <>
      {dates.map((date, index) => (
        <React.Fragment key={formatISO(date)}>
          {isRestday(date) && (
            <HolidayArea posX={posX(index)} width={barWidth} height={height} />
          )}
          <Line posX={posX(index)} height={height} />
        </React.Fragment>
      ))}
      <Line posX={posX(dates.length)} height={height} />
    </>
  );
};
