import React from "react";
import { useRecoilValue } from "recoil";
import { errorMessageState } from "@/recoil/errorMessageState";

export const FlashError: React.FC = () => {
  const errorMessage = useRecoilValue(errorMessageState);
  return errorMessage ? (
    <div className="flash error">
      {errorMessage}
    </div>
  ) : null;
};
