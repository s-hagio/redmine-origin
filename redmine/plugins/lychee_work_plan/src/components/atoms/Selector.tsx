import * as React from 'react';
import styled from 'styled-components';
import ReactSelect, { createFilter, SingleValue } from 'react-select';

const SelectorClassNamePrefix = 'LWP_SELECTOR';
export const SelectorContainerClassName = `${SelectorClassNamePrefix}_Container`;

const Root = styled.div`
  display: flex;
  align-items: center;
  gap: 4px;
`;

type Value = string | number;
type Option = { value: Value; label: string };

type SelectorProps = {
  value: Value;
  onChange: (value: Value) => void;
  label: string;
  noOptionFallback?: React.ReactNode;
  options: Option[];
  selectorWidth: number; // ReactSelectは選択したoptionのラベルに応じて幅が変わってしまう。またHTMLのselect要素のように、子要素の最大幅に合わせるという動きも実装されていない。そのため、固定幅を指定して、検索文字列を入れた時のガタツキや要素選択時のガタツキを回避している
};

const Selector: React.FC<SelectorProps> = ({
  label,
  noOptionFallback,
  value,
  onChange,
  options,
  selectorWidth,
}) => {
  const onChangeSelector = React.useCallback(
    (value: SingleValue<Option>) => {
      value && onChange(value.value);
    },
    [onChange]
  );

  return (
    <Root>
      <span style={{ fontSize: 12 }}>{label}</span>
      {options.length > 0 ? (
        <ReactSelect
          isSearchable
          filterOption={createFilter({
            // デフォルトではlabel / value両方検索対象になってしまうので、labelのみに絞っている
            matchFrom: 'any',
            stringify: (option) => `${option.label}`,
          })}
          className={SelectorContainerClassName}
          onChange={onChangeSelector}
          value={options.find((option) => option.value === value)}
          options={options}
          styles={{
            control: (provided) => ({
              ...provided,
              borderRadius: 6,
              border: '1px #ebebeb solid',
              width: selectorWidth,
            }),
            menu: (provided) => ({
              ...provided,
              zIndex: 100,
            }),
          }}
        />
      ) : (
        noOptionFallback
      )}
    </Root>
  );
};

export default Selector;
