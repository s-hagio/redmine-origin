import React from "react";
import { useRecoilValue, useResetRecoilState } from "recoil";
import { XIcon } from "@heroicons/react/outline";
import { IssueInfo } from "@/components/IssueInfo";
import { selectedIssueIdState } from "@/recoil/selectedIssueIdState";

export const IssueInfoArea: React.FC = () => {
  const selectedIssueId = useRecoilValue(selectedIssueIdState);
  const resetSelectedIssueId = useResetRecoilState(selectedIssueIdState);

  return (
    <div className="relative h-full">
      <XIcon
        data-icon="close-issue-info-area"
        className="absolute right-3 top-3 h-6 w-6 cursor-pointer"
        onClick={resetSelectedIssueId}
      />
      {selectedIssueId && <IssueInfo issueId={selectedIssueId} />}
    </div>
  );
};
