import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { format } from "date-fns";
import { effortsByIssueDateState } from "@/recoil/effortsByDateState";
import { Issue } from "@/recoil/issueState";
import { effortIdsState } from "@/recoil/effortIdsState";
import { effortsState } from "@/recoil/effortsState";
import { targetUserState } from "@/recoil/targetUserState";
import { useFormatHours } from "@/hooks/useFormatHours";
import { createClient } from "@/api/client";
import { DATE_FORMAT } from "@/constants";

type Props = {
  issueId: Issue["id"];
  date: Date;
};

export const EffortBox: React.FC<Props> = ({ issueId, date }) => {
  const effortsByIssueDate = useRecoilValue(effortsByIssueDateState(issueId));
  const effort = effortsByIssueDate.get(format(date, DATE_FORMAT));
  const formatHours = useFormatHours();

  return (
    <div className="flex-1 bg-effort-blue first:rounded-bl last:rounded-br hover:bg-effort-blue-editable" data-date={format(date, DATE_FORMAT)}>
      <EffortInput
        issueId={issueId}
        effortId={effort?.id}
        date={date}
        defaultValue={formatHours(effort?.value)}
        tabIndex={issueId}
      />
    </div>
  );
};

const EffortInput: React.FC<{
  issueId: number;
  effortId: number | undefined;
  date: Date;
  defaultValue: string;
  tabIndex: number;
}> = ({ issueId, effortId, date, defaultValue, tabIndex }) => {
  const [value, setValue] = React.useState(defaultValue);
  const [edit, setEdit] = React.useState(false);
  const inputRef = React.useRef<HTMLInputElement>(null);
  const setEfforts = useSetRecoilState(effortsState);
  const setEffortIds = useSetRecoilState(effortIdsState);
  const targetUser = useRecoilValue(targetUserState);

  React.useEffect(() => {
    setValue(defaultValue);
  }, [defaultValue]);

  const updateValue = async (): Promise<void> => {
    if (value === defaultValue) {
      return;
    }

    const client = createClient();

    if (value === "" && effortId) {
      await client.lychee_work_plan.issues
        ._issueId(issueId)
        .efforts._effortId(effortId)
        .$delete({ query: { target_user_id: targetUser.id }});
      setEffortIds((prev) => {
        const next = [...prev];
        next.splice(prev.indexOf(effortId), 1);
        return next;
      });
      return;
    }

    const { body } = await client.lychee_work_plan.issues
      ._issueId(issueId)
      .efforts.post({
        body: {
          effort: {
            date: format(date, DATE_FORMAT),
            value,
          },
          target_user_id: targetUser.id,
        },
      });
    if (body.success) {
      setEfforts([body.effort]);
      setValue(body.effort.value.toString());
    } else {
      setValue(defaultValue);
    }
  };

  const handleChange = (e: React.SyntheticEvent<HTMLInputElement>): void => {
    setValue(e.currentTarget.value);
  };

  const handleBlur = (): void => {
    updateValue();
    setEdit(false);
  };

  const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>): void => {
    e.preventDefault();
    updateValue();
    setEdit(false);
  };

  React.useEffect(() => {
    if (inputRef.current) {
      inputRef.current.select();
    }
  }, [edit]);

  if (!edit) {
    return (
      <div
        className="flex h-full cursor-pointer items-center"
        tabIndex={tabIndex}
        onFocus={() => {
          setEdit(true);
        }}
      >
        <div className="flex-1 text-center text-base text-black">
          {value === "" ? "-" : value}
        </div>
      </div>
    );
  }

  return (
    <div className="h-full rounded-[inherit] border-2 border-blue-400">
      <form onSubmit={handleSubmit} className="block h-full">
        <input
          ref={inputRef}
          tabIndex={tabIndex}
          value={value}
          onChange={handleChange}
          onBlur={handleBlur}
          className="block h-full w-full rounded-none px-2 text-black outline-none"
        />
      </form>
    </div>
  );
};
