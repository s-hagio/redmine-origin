import React from "react";
import { useTranslation } from "react-i18next";
import { Popover, Transition } from "@headlessui/react";

const imgUrl = new URL("../assets/icn_divide_equally@2x.png", import.meta.url)
  .href;

type Props = {
  onAutofillClick: () => void;
  className?: string;
};

export const IssueBarMenu: React.FC<Props> = ({
  onAutofillClick,
  className,
}) => {
  const { t } = useTranslation();
  return (
    <div className={className}>
      <Popover className="relative inline-block">
        <Popover.Button
          className="border-blue-200 focus:outline-none"
          tabIndex={-1}
        >
          <img className="h-3 w-3" src={imgUrl} />
        </Popover.Button>
        <Transition
          as={React.Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Popover.Panel className="absolute left-0 top-5">
            <div className="rounded-lg bg-white shadow-lg ring-1 ring-black ring-opacity-5">
              <div className="whitespace-nowrap rounded-[inherit] text-sm">
                <a
                  className="inline-block cursor-pointer rounded-[inherit] p-[0.75rem_1rem] hover:bg-blue-200"
                  onClick={() => {
                    onAutofillClick();
                  }}
                >
                  {t("uniformity")}
                </a>
              </div>
            </div>
          </Popover.Panel>
        </Transition>
      </Popover>
    </div>
  );
};
