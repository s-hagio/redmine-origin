import React from "react";
import { createClient } from "@/api/client";
import { Issue } from "@/api/lychee_work_plan/issues/_issueId@number";
import { useTranslation } from "react-i18next";

type Props = {
  issueId: number;
};
export const IssueInfo: React.FC<Props> = ({ issueId }) => {
  const { t } = useTranslation();
  const [issue, setIssue] = React.useState<Issue | null>(null);
  const client = createClient();

  React.useEffect(() => {
    (async () => {
      const {
        body: { issue },
      } = await client.lychee_work_plan.issues._issueId(issueId).get();
      setIssue(issue);
    })();
  }, [issueId]);

  if (!issue) {
    return <Loading />;
  }

  return (
    <div className="mb-1 h-full w-full overflow-scroll border p-4">
      <div className="p-2 mb-2">
        <div className="w-full text-xs font-bold text-[#707070]">
          <Link href={issue.url}>{`${issue.tracker.name} #${issue.id}`}</Link>
        </div>
        <div className="w-full text-base mt-2 text-[#333]">
          {issue.subject}
        </div>
      </div>
      <Row
        header={t("project")}
        value={<Link href={issue.project.url}>{issue.project.name}</Link>}
      />
      <Row
        header={t("parentIssue")}
        value={
          issue.parent ? (
            <Link href={issue.parent.url}>
              {`#${issue.parent.id} ${issue.parent.subject}`}
            </Link>
          ) : null
        }
      />
      <Row
        header={t("description")}
        value={
          <div
            className="wiki"
            dangerouslySetInnerHTML={{ __html: issue.description }}
          />
        }
      />
      <Row header={t("status")} value={issue.status.name} />
      <Row header={t("priority")} value={issue.priority.name} />
      <Row header={t("fixedVersion")} value={issue.fixedVersion?.name} />
      <Row header={t("category")} value={issue.category?.name} />
      <Row header={t("startDate")} value={issue.startDate} />
      <Row header={t("dueDate")} value={issue.dueDate} />
      <Row header={t("estimatedHours")} value={issue.estimatedHours} />
    </div>
  );
};

const Row: React.FC<{
  header: React.ReactNode;
  value: React.ReactNode;
  className?: string;
}> = ({ header, value, className }) => (
  <div className={`flex w-full border-b border-[#f0f2f4] p-2 ${className}`}>
    <div className="w-2/5 font-bold text-[#707070]">{header}</div>
    <div className="w-3/5 text-[#333]">{value}</div>
  </div>
);

const Link: React.FC<{ href: string; children: React.ReactNode }> = ({
  href,
  children,
}) => (
  <a href={href} target="_blank" rel="noreferrer" className="text-blue-500">
    {children}
  </a>
);

const Loading: React.FC = () => (
  <div className="flex h-full items-center justify-center">
    <div
      className="h-10 w-10 animate-spin rounded-full border-4 border-blue-500"
      style={{ borderTopColor: "transparent" }}
    ></div>
  </div>
);
