import { ComponentStory } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { EffortTypeSelector as Component } from "./EffortTypeSelector";

export default {
  title: "EffortTypeSelector",
  component: Component,
};

export const NotSelected: ComponentStory<typeof Component> = () => (
  <Component selected={null} onTypeChange={action("click")} />
);

export const DividedSelected: ComponentStory<typeof Component> = () => (
  <Component selected="uniform" onTypeChange={action("click")} />
);

export const FlexibleSelected: ComponentStory<typeof Component> = () => (
  <Component selected="flexible" onTypeChange={action("click")} />
);
