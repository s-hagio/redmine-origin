import React from "react";
import { useRecoilValue, useResetRecoilState } from "recoil";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import { FlashNotice } from "@/components/FlashNotice";
import { FlashError } from "@/components/FlashError";
import { ScrollContainer } from "@/components/ScrollContainer";
import { ChartHeader } from "@/components/ChartHeader";
import { DayLines } from "@/components/DayLines";
import { ProjectIssueRows } from "@/components/ProjectIssueRows";
import { IssueInfoArea } from "@/components/IssueInfoArea";
import { DateEffortsInfoArea } from "@/components/DateEffortsInfoArea";
import { TargetUserSelect } from "@/components/TargetUserSelect";
import { effortIdsState } from "@/recoil/effortIdsState";
import { intervalState } from "@/recoil/intervalState";
import { issuesState } from "@/recoil/issuesState";
import { projectsState } from "@/recoil/projectsState";
import { selectedIssueIdState } from "@/recoil/selectedIssueIdState";
import { selectedDateEffortsState } from "@/recoil/selectedDateEffortsState";
import { currentUserState, targetUserState } from "@/recoil/targetUserState";
import { useSyncData } from "@/hooks/useSyncData";
import { useHoursSuffix } from "@/hooks/useHoursSuffix";
import { useFormatHours } from "@/hooks/useFormatHours";
import { dayHoursState } from "@/recoil/dayHoursState";
import { enableConnectOutlookState } from "@/recoil/enableConnectOutlookState";
import { availableMicrosoftGraphCalendarState } from "@/recoil/availableMicrosoftGraphCalendarState";

const cautionIcon = new URL("../assets/icn_caution_circle.png", import.meta.url)
  .href;

const heightWithMargin = (selector: string) => {
  const elements = document.querySelectorAll<HTMLElement>(selector);
  return Array.from(elements).reduce((previousValue, element) => {
    const styles = getComputedStyle(element);
    return (
      previousValue +
      element.offsetHeight +
      parseInt(styles.getPropertyValue("margin-top")) +
      parseInt(styles.getPropertyValue("margin-bottom"))
    );
  }, 0);
};

const Wrapper = styled.div<{ otherHeight: number }>`
  display: flex;
  flex-direction: column;
  height: calc(
    100vh + -${(props) => props.otherHeight}px + -${() =>
        heightWithMargin(".flash.notice")}px + -${() => heightWithMargin(".flash.error")}px
  );
`;

export const App: React.FC<{
  otherHeight: number;
  baseURL: string;
}> = ({ otherHeight, baseURL }) => {
  const { t } = useTranslation();
  const interval = useRecoilValue(intervalState);
  const resetEffortIds = useResetRecoilState(effortIdsState);
  const resetIssues = useResetRecoilState(issuesState);
  const projects = useRecoilValue(projectsState);
  const resetProjects = useResetRecoilState(projectsState);
  const targetUser = useRecoilValue(targetUserState);
  const syncData = useSyncData();
  const todayRef = React.useRef<HTMLDivElement>(null);
  const currentUser = useRecoilValue(currentUserState);
  const selectedIssueId = useRecoilValue(selectedIssueIdState);
  const resetSelectedIssueId = useResetRecoilState(selectedIssueIdState);
  const selectedDateEfforts = useRecoilValue(selectedDateEffortsState);
  const resetSelectedDateEfforts = useResetRecoilState(selectedDateEffortsState);
  const formatHours = useFormatHours();
  const dayHours = useRecoilValue(dayHoursState);
  const hoursSuffix = useHoursSuffix();
  const enableConnectOutlook = useRecoilValue(enableConnectOutlookState);
  const availableMicrosoftGraphCalendar = useRecoilValue(
    availableMicrosoftGraphCalendarState
  );

  React.useLayoutEffect(() => {
    handleTodayClick("auto")();
  }, []);

  // React.StrictModeのときにuseEffectが2回実行される件の対応
  // https://qiita.com/asahina820/items/665c55594cfd55e6f14a
  const useEffectIsCalledRef = React.useRef(false);
  React.useEffect(() => {
    if (useEffectIsCalledRef.current) {
      return;
    }

    syncData(interval, targetUser.id);
    useEffectIsCalledRef.current = true;
  }, [interval, targetUser.id]);

  const handleTodayClick = (behavior: ScrollBehavior) => (): void => {
    if (todayRef.current) {
      todayRef.current.scrollIntoView({
        behavior,
        block: "nearest",
        inline: "center",
      });
    }
  };

  const handleTargetUserIdChange = (id: number, newTargetUserId: number) => {
    resetSelectedIssueId();
    resetSelectedDateEfforts();
    resetIssues();
    resetProjects();
    resetEffortIds();
    syncData(interval, newTargetUserId);
    handleTodayClick("smooth")();
  };

  return (
    <>
      <FlashNotice />
      <FlashError />
      <Wrapper otherHeight={otherHeight}>
        <div className="flex h-[2rem] justify-between">
          <div className="flex w-[600px] flex-row">
            <h2 className="text-xl font-bold text-[#555]">{t("title")}</h2>
            <div className="relative w-[200px]">
              <p className="absolute bottom-2 text-xs font-light text-[#555]">
                {t("distributed_workloads")}: <span>{formatHours(dayHours)}</span>
                {hoursSuffix}
              </p>
            </div>
            <div className="relative flex w-[200px] items-center text-xs text-[#555]">
              <img src={cautionIcon} className="h-[16px] w-[16px]" />
              <p>{t("working_hours_blank")}</p>
            </div>
          </div>
          <div className="flex justify-between">
            <div>
              <button
                className="cursor-poiner mr-[10px] h-[30px] rounded border border-[#888] p-[5px_10px] text-sm font-bold text-[#888]"
                onClick={handleTodayClick("smooth")}
              >
                {t("today")}
              </button>
              {enableConnectOutlook && currentUser.id === targetUser.id &&
                (availableMicrosoftGraphCalendar ? (
                  <a
                    className="nav-link"
                    rel="nofollow"
                    data-method="delete"
                    href={`${baseURL}/lychee_work_plan/aad_credential`}
                  >
                    <button className="cursor-poiner mr-[10px] h-[30px] rounded border border-[#888] p-[5px_10px] text-sm font-bold text-[#888]">
                      {t("disconnectExternalCalendar")}
                    </button>
                  </a>
                ) : (
                  <a
                    className="nav-link"
                    rel="nofollow"
                    data-method="post"
                    href={`${baseURL}/lychee_work_plan/aad_credential`}
                  >
                    <button className="cursor-poiner mr-[10px] h-[30px] rounded border border-[#888] p-[5px_10px] text-sm font-bold text-[#888]">
                      {t("connectExternalCalendar")}
                    </button>
                  </a>
                ))}
            </div>
            <TargetUserSelect
              onChangeTargetUserId={handleTargetUserIdChange}
            />
          </div>
        </div>
        <div className="mt-[1rem] flex h-[calc(100%-3rem)]">
          <div
            className={`${
              selectedIssueId || selectedDateEfforts ? "w-3/4" : "w-full"
            } overflow-auto`}
            style={{ transition: "width 300ms ease-in-out" }}
          >
            <ScrollContainer>
              <ChartHeader ref={todayRef} />
              <DayLines />
              {projects.map((project) => (
                <ProjectIssueRows key={project.id} project={project} />
              ))}
            </ScrollContainer>
          </div>
          <div
            className={`${selectedIssueId ? "w-1/4" : "w-0"} overflow-hidden`}
            style={{ transition: "width 300ms ease-in-out" }}
          >
            <IssueInfoArea />
          </div>
          <div
            className={`${
              selectedDateEfforts ? "w-1/4" : "w-0"
            } overflow-hidden`}
            style={{ transition: "width 300ms ease-in-out" }}
          >
            <DateEffortsInfoArea />
          </div>
        </div>
      </Wrapper>
    </>
  );
};
