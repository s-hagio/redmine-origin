import React from "react";
import { useRecoilValue, useResetRecoilState } from "recoil";
import { XIcon } from "@heroicons/react/outline";
import { DateEffortsInfo } from "@/components/DateEffortsInfo";
import { selectedDateEffortsState } from "@/recoil/selectedDateEffortsState";

export const DateEffortsInfoArea: React.FC = () => {
  const selectedDateEfforts = useRecoilValue(selectedDateEffortsState);
  const resetSelectedDateEfforts = useResetRecoilState(
    selectedDateEffortsState
  );

  return (
    <div className="relative h-full">
      <XIcon
        data-icon="close-date-efforts-info-area"
        className="absolute right-3 top-3 h-6 w-6 cursor-pointer"
        onClick={resetSelectedDateEfforts}
      />
      {selectedDateEfforts && <DateEffortsInfo date={selectedDateEfforts} />}
    </div>
  );
};
