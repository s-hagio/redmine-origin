import { ComponentStory } from "@storybook/react";

const Component: React.FC = () => (
  <div className="relative h-0" style={{ width: "200vw" }}>
    <div
      className="h-8 w-32 transform border border-green-500"
      style={{ "--tw-translate-y": "10px" } as React.CSSProperties}
    >
      <div className="sticky left-0 inline-block">sticky</div>
    </div>
    <div className="w-96 border border-red-500">
      <div className="h-8 bg-gray-100" style={{ width: "200vw" }} />
    </div>
  </div>
);

export default {
  title: "Sticky",
  component: Component,
};

export const Sticky: ComponentStory<typeof Component> = () => <Component />;
