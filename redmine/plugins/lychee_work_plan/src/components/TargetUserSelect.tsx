import * as React from 'react';
import { useTranslation } from "react-i18next";
import { useRecoilValue, useRecoilState, useSetRecoilState } from "recoil";
import Selector from './atoms/Selector';

import { targetUserState, currentUserState, TargetUser } from '@/recoil/targetUserState';
import { dayHoursState } from '@/recoil/dayHoursState';
import { targetUsersState } from '@/recoil/targetUsersState';

type TargetUserSelectProps = {
  onChangeTargetUserId: (id: number, new_target_user_id: number) => void;
};

export const TargetUserSelect: React.FC<TargetUserSelectProps> = ({
  onChangeTargetUserId,
}: TargetUserSelectProps) => {
  const { t } = useTranslation();
  const currentUser = useRecoilValue(currentUserState);
  const [targetUser, setTargetUser] = useRecoilState(targetUserState);
  const targetUsers = useRecoilValue(targetUsersState);
  const [targetUserId, setTargetUserId] = React.useState(targetUser.id);
  const setDayHours = useSetRecoilState(dayHoursState);

  const handleTargetUserIdChange = (value: unknown): void => {
    const newTargetUser = targetUsers.find((user: TargetUser) => user.id === Number(value));
    if (newTargetUser) {
      setTargetUser(newTargetUser);
      setTargetUserId(newTargetUser.id);
      setDayHours(newTargetUser.day_hours);
      onChangeTargetUserId(Number(value), newTargetUser.id);
    }
  };

  const options = React.useMemo(
    () =>
      targetUsers.length > 1 ?
        targetUsers.map(
          (user: TargetUser) => ({ value: user.id, label: user.name } as const)
        ) : [],
    [targetUsers]
  );

  return (
    <Selector
      label={t('field_assigned_to')}
      value={targetUserId}
      onChange={handleTargetUserIdChange}
      noOptionFallback={<h2>{currentUser.name}</h2>}
      options={options}
      selectorWidth={300}
    />
  );
};
