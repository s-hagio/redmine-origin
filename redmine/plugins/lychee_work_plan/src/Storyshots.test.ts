import path from "path";
import initStoryshots, {
  Stories2SnapsConverter,
  multiSnapshotWithOptions,
} from "@storybook/addon-storyshots";
import i18n from "@/i18n";

initStoryshots({
  suite: "ja language",
  stories2snapsConverter: new Stories2SnapsConverter({
    snapshotExtension: ".ja.storyshot",
  }),
  test: (story) => {
    i18n.changeLanguage("ja");

    // https://github.com/storybookjs/storybook/issues/16692#issuecomment-1048915433
    const fileName = path.resolve(__dirname, "..", story.context.fileName);
    return multiSnapshotWithOptions()({
      ...story,
      context: {
        ...story.context,
        fileName,
      },
    });
  },
});

initStoryshots({
  suite: "en language",
  stories2snapsConverter: new Stories2SnapsConverter({
    snapshotExtension: ".en.storyshot",
  }),
  test: (story) => {
    i18n.changeLanguage("en");

    const fileName = path.resolve(__dirname, "..", story.context.fileName);
    return multiSnapshotWithOptions()({
      ...story,
      context: {
        ...story.context,
        fileName,
      },
    });
  },
});
