import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";

const resources = {
  ja: {
    translation: {
      today: "今日",
      uniformity: "予定工数を均等割りする",
      connectExternalCalendar: "外部カレンダー連携",
      disconnectExternalCalendar: "外部カレンダー連携解除",
      fetchingEventsFailed: "外部カレンダーから予定取得できませんでした。連携先とのネットワーク接続、および連携情報の有効期限をご確認ください。",
      dateFormat: "yyyy年M月d日",
      externalCalendarEvents: "外部カレンダー予定",
      numberOfCalendarEvents: "件数",
      numberOfCalendarEventsFormat: "{{value, number}}件",
      totalTime: "合計時間",
      allDay: "終日",
      decimalTimeSpanHoursAndMinutesFormat: "{{hoursPart, number}}時間{{minutesPart, number}}分",
      decimalTimeSpanHoursFormat: "{{hoursPart, number}}時間",
      decimalTimeSpanMinutesFormat: "{{minutesPart, number}}分",
      minutesTimeSpanHoursAndMinutesFormat: "{{hoursPart, number}}時間{{minutesPart, number}}分",
      minutesTimeSpanHoursFormat: "{{hoursPart, number}}時間",
      minutesTimeSpanMinutesFormat: "{{minutesPart, number}}分",
    },
  },
  en: {
    translation: {
      today: "Today",
      uniformity: "Prorate the Estimated time",
      connectExternalCalendar: "Connect to external calendar",
      disconnectExternalCalendar: "Disconnect from external calendar",
      fetchingEventsFailed: "Fetching events from external calendar failed. Check the network connection and client secret expiration date.",
      dateFormat: "MM/dd/yyyy",
      externalCalendarEvents: "External calendar",
      numberOfCalendarEvents: "Events",
      numberOfCalendarEventsFormat: "{{value, number}}",
      totalTime: "Total time",
      allDay: "All day",
      decimalTimeSpanHoursAndMinutesFormat: "{{hours, number(minimumFractionDigits: 2)}} h",
      decimalTimeSpanHoursFormat: "{{hours, number(minimumFractionDigits: 2)}} h",
      decimalTimeSpanMinutesFormat: "{{hours, number(minimumFractionDigits: 2)}} h",
      minutesTimeSpanHoursAndMinutesFormat: "{{hoursPart, number}}:{{minutesPart, number(minimumIntegerDigits: 2)}} h",
      minutesTimeSpanHoursFormat: "{{hoursPart, number}}:{{minutesPart, number(minimumIntegerDigits: 2)}} h",
      minutesTimeSpanMinutesFormat: "{{hoursPart, number}}:{{minutesPart, number(minimumIntegerDigits: 2)}} h",
    },
  },
} as const;

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources,
    fallbackLng: "en",
    interpolation: {
      escapeValue: false,
    },
    detection: {
      order: ["htmlTag"],
    },
  });

export default i18n;
