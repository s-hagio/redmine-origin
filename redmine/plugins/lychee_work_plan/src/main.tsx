import React from "react";
import { createRoot } from "react-dom/client";
import { RecoilRoot } from "recoil";
import { nonWorkingWeekDaysState } from "@/recoil/nonWorkingWeekDaysState";
import {
  timespanFormatState,
  TimespanFormat,
} from "@/recoil/timespanFormatState";
import { setClientConfig } from "@/api/client";
import { DebugObserver } from "@/DebugObserver";
import i18n from "@/i18n";
import "@/index.css";

import { App } from "@/components/App";
import { noticeMessageState } from "@/recoil/noticeMessageState";
import { errorMessageState } from "@/recoil/errorMessageState";
import { restdaysState } from "@/recoil/restdaysState";
import { dayHoursState } from "@/recoil/dayHoursState";
import { enableConnectOutlookState } from "@/recoil/enableConnectOutlookState";
import { enableMicrosoftGraphCalendarState } from "@/recoil/enableMicrosoftGraphCalendarState";
import { targetUserState, currentUserState, TargetUser, CurrentUser } from "@/recoil/targetUserState";
import { targetUsersState } from "@/recoil/targetUsersState";

const contentDivStyles = getComputedStyle(document.getElementById("content")!);
const otherHeight =
  document.getElementById("top-menu")!.offsetHeight +
  document.getElementById("header")!.offsetHeight +
  document.getElementById("footer")!.offsetHeight +
  parseInt(contentDivStyles.getPropertyValue("padding-top")) +
  parseInt(contentDivStyles.getPropertyValue("padding-bottom"));

const LycheeWorkPlan = {
  start: (
    container: Element,
    {
      baseURL,
      noticeMessage,
      errorMessage,
      nonWorkingWeekDays,
      restdays,
      dayHours,
      timespanFormat,
      enableConnectOutlook,
      enableMicrosoftGraphCalendar,
      currentUser,
      targetUsers,
      locales,
    }: {
      baseURL: string;
      noticeMessage: string;
      errorMessage: string;
      nonWorkingWeekDays: number[];
      restdays: string[];
      dayHours: number;
      timespanFormat: TimespanFormat;
      enableConnectOutlook: boolean;
      enableMicrosoftGraphCalendar: boolean;
      currentUser: CurrentUser;
      targetUsers: TargetUser[];
      locales: Record<string, string>;
    }
  ) => {
    setClientConfig({ baseURL });
    i18n.addResourceBundle(i18n.language, "translation", locales);

    const root = createRoot(container);
    root.render(
      <React.StrictMode>
        <RecoilRoot
          initializeState={({ set }) => {
            set(noticeMessageState, noticeMessage);
            set(errorMessageState, errorMessage);
            set(nonWorkingWeekDaysState, nonWorkingWeekDays);
            set(restdaysState, restdays);
            set(dayHoursState, dayHours);
            set(timespanFormatState, timespanFormat);
            set(enableConnectOutlookState, enableConnectOutlook);
            set(enableMicrosoftGraphCalendarState, enableMicrosoftGraphCalendar);
            set(currentUserState, currentUser);
            set(targetUserState, currentUser);
            set(targetUsersState, targetUsers);
          }}
        >
          {process.env.NODE_ENV === "development" && <DebugObserver />}
          <div className="tailwind">
            <App otherHeight={otherHeight} baseURL={baseURL} />
          </div>
        </RecoilRoot>
      </React.StrictMode>
    );
  },
};

declare global {
  interface Window {
    LycheeWorkPlan: typeof LycheeWorkPlan;
  }
}

window.LycheeWorkPlan = LycheeWorkPlan;
