import axios, { AxiosRequestConfig } from "axios";
import aspida from "@aspida/axios";
import applyCaseMiddleware from "axios-case-converter";
import api, { ApiInstance } from "@/api/$api";

const config: AxiosRequestConfig = {
  withCredentials: true,
};
const axiosInstance = applyCaseMiddleware(axios.create(config));

export const setClientConfig = ({
  baseURL,
}: Required<Pick<AxiosRequestConfig, "baseURL">>): void => {
  axiosInstance.defaults.baseURL = baseURL;
};

const methods = ["post", "patch", "delete"] as const;

export const createClient = (): ApiInstance => {
  methods.forEach((method) => {
    axiosInstance.defaults.headers[method]["X-CSRF-Token"] = csrfToken() ?? "";
  });
  return api(aspida(axiosInstance)) as ApiInstance;
};

const csrfToken = (): string | null => {
  const meta = document.querySelector<HTMLMetaElement>("meta[name=csrf-token]");
  return meta && meta.content;
};
