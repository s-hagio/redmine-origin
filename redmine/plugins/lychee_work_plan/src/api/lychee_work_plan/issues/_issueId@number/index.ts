export type Issue = {
  id: number;
  url: string;
  project: {
    identifier: string;
    name: string;
    url: string;
  };
  tracker: {
    name: string;
  };
  subject: string;
  parent?: {
    id: number;
    subject: string;
    url: string;
  };
  description: string;
  status: {
    name: string;
  };
  priority: {
    name: string;
  };
  fixedVersion?: {
    name: string;
  };
  category?: {
    name: string;
  };
  startDate: string;
  dueDate: string;
  estimatedHours: string;
};

export type Methods = {
  get: {
    resBody: { issue: Issue };
  };
};
