import { Effort } from "@/api/lychee_work_plan/issues";

export type Methods = {
  post: {
    reqFormat: string;
    reqBody: {
      target_user_id: number;
    };
    resBody: {
      success: boolean;
      efforts: Effort[];
    };
  };
};
