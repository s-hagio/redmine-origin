import { Effort } from "@/api/lychee_work_plan/issues";

export type Methods = {
  post: {
    reqFormat: string;
    reqBody: {
      effort: {
        date: string;
        value: string;
      };
      target_user_id: number;
    };
    resBody: {
      success: boolean;
      effort: Effort;
    };
  };
};
