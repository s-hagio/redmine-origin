export type Methods = {
  delete: {
    reqFormat: string;
    query: {
      target_user_id: number;
    }
  };
};
