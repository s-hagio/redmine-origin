export type Issue = {
  id: number;
  projectId: number;
  subject: string;
  startDate: string | null;
  dueDate: string | null;
  parentSubject: string | null;
  owned: boolean;
};

export type Effort = {
  id: number;
  issueId: number;
  date: string;
  value: number;
};

export type Project = {
  id: number;
  name: string;
  lft: number;
};

export type Methods = {
  get: {
    query: {
      from: Date;
      to: Date;
      target_user_id: number;
    };
    resBody: {
      issues: Issue[];
      efforts: Effort[];
      projects: Project[];
    };
  };
};
