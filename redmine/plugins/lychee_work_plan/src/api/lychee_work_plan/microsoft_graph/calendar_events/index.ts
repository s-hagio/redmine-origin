import type { MicrosoftGraphEvent } from "@/lib/microsoftGraphEvent";

export type Methods = {
  get: {
    query: {
      from: Date;
      to: Date;
    };
    resBody: {
      value: MicrosoftGraphEvent[];
    };
  };
};
