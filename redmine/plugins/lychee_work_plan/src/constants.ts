export const DATE_FORMAT = "yyyy-MM-dd" as const;
export const DAYS_LOADED_AT_ONCE = 28;
export const WORKLOAD_DAY_HEIGHT = 75;
