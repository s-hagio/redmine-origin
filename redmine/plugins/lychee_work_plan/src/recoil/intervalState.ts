import { atom } from "recoil";
import { addDays, subDays, startOfWeek } from "date-fns";
import { DAYS_LOADED_AT_ONCE } from "@/constants";

const start = subDays(
  startOfWeek(new Date(), { weekStartsOn: 1 }),
  DAYS_LOADED_AT_ONCE
);
const end = addDays(start, DAYS_LOADED_AT_ONCE * 3);

export type Interval = {
  start: Date;
  end: Date;
};

export const intervalState = atom<Interval>({
  key: "intervalState",
  default: { start, end },
});
