import { selector, selectorFamily } from "recoil";
import { parseISO, format } from "date-fns";
import { effortIdsState } from "@/recoil/effortIdsState";
import { effortState, Effort } from "@/recoil/effortState";
import { DATE_FORMAT } from "@/constants";

export const effortsByDateState = selector<Map<string, Effort[]>>({
  key: "effortsByDateState",
  get: ({ get }) => {
    const effortIds = get(effortIdsState);
    const efforts = effortIds.map((effortId) => get(effortState(effortId)));
    const map = new Map<string, Effort[]>();
    efforts.forEach((effort) => {
      if (effort) {
        map.set(
          format(parseISO(effort.date), DATE_FORMAT),
          (map.get(effort.date) ?? []).concat([effort])
        );
      }
    });
    return map;
  },
});

export const effortsByIssueDateState = selectorFamily<
  Map<string, Effort>,
  number
>({
  key: "effortsByIssueDateState",
  get:
    (issueId) =>
    ({ get }) => {
      const effortIds = get(effortIdsState);
      const efforts = effortIds
        .map((effortId) => get(effortState(effortId)))
        .filter((e): e is Effort => e !== null)
        .filter((e) => e.issueId === issueId);
      const map = new Map<string, Effort>();
      efforts.forEach((effort) => {
        map.set(format(parseISO(effort.date), DATE_FORMAT), effort);
      });
      return map;
    },
});
