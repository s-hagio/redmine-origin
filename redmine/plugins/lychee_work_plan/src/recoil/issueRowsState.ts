import { selectorFamily } from "recoil";
import { parseISO, areIntervalsOverlapping } from "date-fns";
import { intervalState } from "@/recoil/intervalState";
import { myIssuesState } from "@/recoil/issuesState";
import { Issue } from "@/recoil/issueState";
import { virtualIssuesState } from "@/recoil/virtualIssuesState";

export type Row = {
  issues: Issue[];
};

export const issueRowsState = selectorFamily<Row[], number>({
  key: "issueRowsState",
  get:
    (projectId) =>
    ({ get }) =>
      buildIssueRows(
        projectId,
        [...get(myIssuesState), ...get(virtualIssuesState)],
        get(intervalState)
      ),
});

const buildIssueRows = (
  projectId: number,
  issues: Issue[],
  wholeInterval: {
    start: Date;
    end: Date;
  }
): Row[] => {
  let rows: Row[] = [];
  issues
    .filter((issue) => issue.projectId === projectId)
    .forEach((issue) => {
      const interval = {
        start: parseISO(issue.startDate),
        end: parseISO(issue.dueDate),
      };

      if (
        !areIntervalsOverlapping(interval, wholeInterval, { inclusive: true })
      ) {
        return;
      }

      const containerRow = rows.find((row) =>
        row.issues.every(
          (containedIssue) =>
            !areIntervalsOverlapping(
              interval,
              {
                start: parseISO(containedIssue.startDate),
                end: parseISO(containedIssue.dueDate),
              },
              { inclusive: true }
            )
        )
      );

      if (containerRow) {
        containerRow.issues = [...containerRow.issues, issue];
      } else {
        rows = [...rows, { issues: [issue] }];
      }
    });

  return rows;
};
