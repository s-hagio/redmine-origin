import { selector } from "recoil";
import { eachDayOfInterval } from "date-fns";
import { intervalState } from "@/recoil/intervalState";

export const datesState = selector<Date[]>({
  key: "datesState",
  get: ({ get }) => eachDayOfInterval(get(intervalState)),
});
