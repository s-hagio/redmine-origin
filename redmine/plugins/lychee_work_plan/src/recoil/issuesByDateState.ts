import { selector } from "recoil";
import { myIssuesState } from "@/recoil/issuesState";
import { Issue } from "@/recoil/issueState";
import { eachDayOfInterval, format, parseISO } from "date-fns";
import { DATE_FORMAT } from "@/constants";

export const issuesByDateState = selector<Map<string, Issue[]>>({
  key: "issuesByDateState",
  get: ({ get }) => {
    const issues = get(myIssuesState);
    const map = new Map<string, Issue[]>();
    issues.forEach((issue) => {
      eachDayOfInterval({ start: parseISO(issue.startDate), end: parseISO(issue.dueDate) }).forEach(
        (date) => {
          map.set(
            format(date, DATE_FORMAT),
            (map.get(format(date, DATE_FORMAT)) ?? []).concat([issue])
          )
        }
      );
    });
    return map;
  },
});
