import { snapshot_UNSTABLE } from "recoil";
import type { CalendarEvent, CalendarEventHours } from "@/lib/calendarEvent";
import { dayHoursState } from "@/recoil/dayHoursState";
import { calendarEventsState } from "@/recoil/calendarEventsState";
import { calendarEventHoursAryByDateState } from "@/recoil/calendarEventHoursAryByDateState";

type DateAndCalendarEventHoursAry = {
  [key: string]: CalendarEventHours[]
};

describe('calendarEventHoursAryByDateState', () => {
  const DUMMY_DATA = {
    id: 'dummy_id',
    subject: 'dummy_subject',
  };
  const DUMMY_DAY_HOURS = 7.5;
  const testIt = (
    testName: string,
    actualCalendarEvents: CalendarEvent[],
    expectedDateAndCalendarEventHoursAry: DateAndCalendarEventHoursAry,
  ) => {
    test(testName, () => {
      const testSnapshot = snapshot_UNSTABLE(({ set }) => {
        set(dayHoursState, DUMMY_DAY_HOURS); // 自動試験ではデフォルト値と変える
        set(calendarEventsState, actualCalendarEvents);
      });
      const expected = new Map(Object.entries(expectedDateAndCalendarEventHoursAry));
      expect(testSnapshot.getLoadable(calendarEventHoursAryByDateState).valueOrThrow()).toEqual(expected);
    })
  };

  testIt('hoursを計算する', [
    {
      ...DUMMY_DATA,
      isAllDay: false,
      start: new Date('2023-01-06T09:00+09:00'),
      end: new Date('2023-01-06T10:00+09:00'),
    },
  ], {
    '2023-01-06': [
      {
        id: `${DUMMY_DATA.id}-2023-01-06`,
        date: '2023-01-06',
        hours: 1,
        isWholeWorkDay: false,
        start: new Date('2023-01-06T09:00+09:00'),
        end: new Date('2023-01-06T10:00+09:00'),
        calendarEvent: {
          ...DUMMY_DATA,
          isAllDay: false,
          start: new Date('2023-01-06T09:00+09:00'),
          end: new Date('2023-01-06T10:00+09:00'),
        },
      },
    ],
  });

  testIt('終日の予定はhoursを割当て稼働時間とする', [
    {
      ...DUMMY_DATA,
      isAllDay: true,
      start: new Date('2023-01-10T00:00Z'),
      end: new Date('2023-01-11T00:00Z'), // 翌日の00:00:00になる
    },
  ], {
    '2023-01-10': [
      {
        id: `${DUMMY_DATA.id}-2023-01-10`,
        date: '2023-01-10',
        hours: DUMMY_DAY_HOURS,
        isWholeWorkDay: true,
        start: new Date('2023-01-10T00:00+09:00'),
        end: new Date('2023-01-11T00:00+09:00'),
        calendarEvent: {
          ...DUMMY_DATA,
          isAllDay: true,
          start: new Date('2023-01-10T00:00Z'),
          end: new Date('2023-01-11T00:00Z'),
        },
      },
    ],
  });

  testIt('日またぎ予定のhoursは開始日を時刻から24:00までの時間，終了日を00:00から時刻までの時間，中日を割当て稼働時間とする', [
    {
      ...DUMMY_DATA,
      isAllDay: false,
      start: new Date('2023-01-11T07:00+09:00'), // UTCで前日になる時刻
      end: new Date('2023-01-13T08:45+09:00'), // UTCで前日になる時刻
    },
  ], {
    '2023-01-11': [
      {
        id: `${DUMMY_DATA.id}-2023-01-11`,
        date: '2023-01-11',
        hours: 17,
        isWholeWorkDay: false,
        start: new Date('2023-01-11T07:00+09:00'),
        end: new Date('2023-01-12T00:00+09:00'),
        calendarEvent: {
          ...DUMMY_DATA,
          isAllDay: false,
          start: new Date('2023-01-11T07:00+09:00'),
          end: new Date('2023-01-13T08:45+09:00'),
        },
      },
    ],
    '2023-01-12': [
      {
        id: `${DUMMY_DATA.id}-2023-01-12`,
        date: '2023-01-12',
        hours: DUMMY_DAY_HOURS,
        isWholeWorkDay: true,
        start: new Date('2023-01-12T00:00+09:00'),
        end: new Date('2023-01-13T00:00+09:00'),
        calendarEvent: {
          ...DUMMY_DATA,
          isAllDay: false,
          start: new Date('2023-01-11T07:00+09:00'),
          end: new Date('2023-01-13T08:45+09:00'),
        },
      },
    ],
    '2023-01-13': [
      {
        id: `${DUMMY_DATA.id}-2023-01-13`,
        date: '2023-01-13',
        hours: 8.75,
        isWholeWorkDay: false,
        start: new Date('2023-01-13T00:00+09:00'),
        end: new Date('2023-01-13T08:45+09:00'),
        calendarEvent: {
          ...DUMMY_DATA,
          isAllDay: false,
          start: new Date('2023-01-11T07:00+09:00'),
          end: new Date('2023-01-13T08:45+09:00'),
        },
      },
    ],
  });

  testIt('日またぎ予定で中日がない場合も処理可能である', [
    {
      ...DUMMY_DATA,
      isAllDay: false,
      start: new Date('2023-01-11T07:00+09:00'), // UTCで前日になる時刻
      end: new Date('2023-01-12T08:45+09:00'), // UTCで前日になる時刻
    },
  ], {
    '2023-01-11': [
      {
        id: `${DUMMY_DATA.id}-2023-01-11`,
        date: '2023-01-11',
        hours: 17,
        isWholeWorkDay: false,
        start: new Date('2023-01-11T07:00+09:00'),
        end: new Date('2023-01-12T00:00+09:00'),
        calendarEvent: {
          ...DUMMY_DATA,
          isAllDay: false,
          start: new Date('2023-01-11T07:00+09:00'),
          end: new Date('2023-01-12T08:45+09:00'),
        },
      },
    ],
    '2023-01-12': [
      {
        id: `${DUMMY_DATA.id}-2023-01-12`,
        date: '2023-01-12',
        hours: 8.75,
        isWholeWorkDay: false,
        start: new Date('2023-01-12T00:00+09:00'),
        end: new Date('2023-01-12T08:45+09:00'),
        calendarEvent: {
          ...DUMMY_DATA,
          isAllDay: false,
          start: new Date('2023-01-11T07:00+09:00'),
          end: new Date('2023-01-12T08:45+09:00'),
        },
      },
    ],
  });

  testIt('終日の日またぎ予定のhoursは割当て稼働時間とする', [
    {
      ...DUMMY_DATA,
      isAllDay: true,
      start: new Date('2023-01-11T00:00Z'),
      end: new Date('2023-01-14T00:00Z'),
    },
  ], {
    '2023-01-11': [
      {
        id: `${DUMMY_DATA.id}-2023-01-11`,
        date: '2023-01-11',
        hours: DUMMY_DAY_HOURS,
        isWholeWorkDay: true,
        start: new Date('2023-01-11T00:00+09:00'),
        end: new Date('2023-01-12T00:00+09:00'),
        calendarEvent: {
          ...DUMMY_DATA,
          isAllDay: true,
          start: new Date('2023-01-11T00:00Z'),
          end: new Date('2023-01-14T00:00Z'),
        },
      },
    ],
    '2023-01-12': [
      {
        id: `${DUMMY_DATA.id}-2023-01-12`,
        date: '2023-01-12',
        hours: DUMMY_DAY_HOURS,
        isWholeWorkDay: true,
        start: new Date('2023-01-12T00:00+09:00'),
        end: new Date('2023-01-13T00:00+09:00'),
        calendarEvent: {
          ...DUMMY_DATA,
          isAllDay: true,
          start: new Date('2023-01-11T00:00Z'),
          end: new Date('2023-01-14T00:00Z'),
        },
      },
    ],
    '2023-01-13': [
      {
        id: `${DUMMY_DATA.id}-2023-01-13`,
        date: '2023-01-13',
        hours: DUMMY_DAY_HOURS,
        isWholeWorkDay: true,
        start: new Date('2023-01-13T00:00+09:00'),
        end: new Date('2023-01-14T00:00+09:00'),
        calendarEvent: {
          ...DUMMY_DATA,
          isAllDay: true,
          start: new Date('2023-01-11T00:00Z'),
          end: new Date('2023-01-14T00:00Z'),
        },
      },
    ],
  });
});
