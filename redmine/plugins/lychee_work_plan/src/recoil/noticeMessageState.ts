import { atom } from "recoil";

export const noticeMessageState = atom<string>({
  key: "noticeMessageState",
  default: '',
});
