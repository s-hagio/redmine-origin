import { selectorFamily } from "recoil";
import { differenceInCalendarDays, parseISO } from "date-fns";
import { intervalState } from "@/recoil/intervalState";
import { Issue } from "@/recoil/issueState";
import { barWidthState } from "@/recoil/barWidthState";

export const issuePosXState = selectorFamily<number, Issue["startDate"]>({
  key: "issuePosXState",
  get:
    (startDate) =>
    ({ get }) => {
      const { start } = get(intervalState);
      const barWidth = get(barWidthState);
      return differenceInCalendarDays(parseISO(startDate), start) * barWidth;
    },
});
