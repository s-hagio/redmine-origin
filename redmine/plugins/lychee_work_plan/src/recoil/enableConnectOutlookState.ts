import { atom } from "recoil";

export const enableConnectOutlookState = atom<boolean>({
  key: "enableConnectOutlookState",
  default: false,
});
