import { atom } from "recoil";

export const restdaysState = atom<string[]>({
  key: "restdaysState",
  default: [],
});
