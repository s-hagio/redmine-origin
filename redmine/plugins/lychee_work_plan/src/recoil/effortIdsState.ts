import { atom } from "recoil";

type EffortId = number;

export const effortIdsState = atom<EffortId[]>({
  key: "effortIdsState",
  default: [],
});
