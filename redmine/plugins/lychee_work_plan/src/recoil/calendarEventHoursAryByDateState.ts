import { selector } from "recoil";
import { format, eachDayOfInterval, addDays, subDays, startOfDay } from "date-fns";
import type { CalendarEvent, CalendarEventHours } from "@/lib/calendarEvent";
import { dayHoursState } from "@/recoil/dayHoursState";
import { calendarEventsState } from "@/recoil/calendarEventsState";
import { currentUserState, targetUserState } from "@/recoil/targetUserState";
import { DATE_FORMAT } from "@/constants";

const msecsToHours = (msecs: number) => msecs / 1000 / 60 / 60;

const buildDayHoursDays = (dayHours: number, start: Date, end: Date, calendarEvent: CalendarEvent) => {
  if (start.getTime() > end.getTime()) {
    return [];
  }

  return eachDayOfInterval({ start, end }).map((date) => {
    const isoDate = format(date, DATE_FORMAT);
    return {
      id: `${calendarEvent.id}-${isoDate}`,
      date: isoDate,
      hours: dayHours,
      isWholeWorkDay: true,
      start: date,
      end: addDays(date, 1),
      calendarEvent,
    };
  });
};

const buildCalendarEventHoursAry = (dayHours: number, calendarEvent: CalendarEvent) => {
  if (calendarEvent.isAllDay) {
    return buildDayHoursDays(
      dayHours,
      calendarEvent.start,
      subDays(calendarEvent.end, 1),
      calendarEvent,
    );
  }

  const startIsoDate = format(calendarEvent.start, DATE_FORMAT);
  const endIsoDate = format(calendarEvent.end, DATE_FORMAT);
  if (startIsoDate === endIsoDate) {
    const hours = msecsToHours(calendarEvent.end.getTime() - calendarEvent.start.getTime());

    return [
      {
        id: `${calendarEvent.id}-${startIsoDate}`,
        date: startIsoDate,
        hours,
        isWholeWorkDay: false,
        start: calendarEvent.start,
        end: calendarEvent.end,
        calendarEvent,
      },
    ];
  }

  const firstDayEnd = startOfDay(addDays(calendarEvent.start, 1));
  const firstDayMsecs = firstDayEnd.getTime() - calendarEvent.start.getTime();
  const firstDay = {
    id: `${calendarEvent.id}-${startIsoDate}`,
    date: startIsoDate,
    hours: msecsToHours(firstDayMsecs),
    isWholeWorkDay: false,
    start: calendarEvent.start,
    end: firstDayEnd,
    calendarEvent,
  }

  const lastDayStart = startOfDay(calendarEvent.end);
  const lastDayMsecs = calendarEvent.end.getTime() - lastDayStart.getTime();
  const lastDay = {
    id: `${calendarEvent.id}-${endIsoDate}`,
    date: endIsoDate,
    hours: msecsToHours(lastDayMsecs),
    isWholeWorkDay: false,
    start: lastDayStart,
    end: calendarEvent.end,
    calendarEvent,
  }

  const middleDays = buildDayHoursDays(
    dayHours,
    startOfDay(addDays(calendarEvent.start, 1)),
    startOfDay(subDays(calendarEvent.end, 1)),
    calendarEvent,
  );
  return [
    firstDay,
    ...middleDays,
    lastDay,
  ];
};

export const calendarEventHoursAryByDateState = selector<Map<string, CalendarEventHours[]>>({
  key: "calendarEventHoursAryByDateState",
  get: ({ get }) => {
    const dayHours = get(dayHoursState);
    const calendarEvents = get(calendarEventsState);
    const currentUser = get(currentUserState);
    const targetUser = get(targetUserState);
    const map = new Map<string, CalendarEventHours[]>();
    if(currentUser.id !== targetUser.id) { return map; }
    calendarEvents.flatMap(
      (calendarEvent) => buildCalendarEventHoursAry(dayHours, calendarEvent),
    ).forEach((calendarEventHours) => {
      const dateKey = calendarEventHours.date;
      map.set(dateKey, [...map.get(dateKey) ?? [], calendarEventHours]);
    });
    return map;
  },
});
