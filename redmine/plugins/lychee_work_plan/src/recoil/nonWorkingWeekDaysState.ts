import { atom } from "recoil";

export const nonWorkingWeekDaysState = atom<number[]>({
  key: "nonWorkingWeekDaysState",
  default: [],
});
