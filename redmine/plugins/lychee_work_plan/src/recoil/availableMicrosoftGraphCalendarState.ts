import { selector } from "recoil";
import { currentUserState, targetUserState } from "@/recoil/targetUserState";
import { enableMicrosoftGraphCalendarState } from "@/recoil/enableMicrosoftGraphCalendarState";

export const availableMicrosoftGraphCalendarState = selector<boolean>({
  key: "availableMicrosoftGraphCalendarState",
  get: ({ get }) => {
    const currentUser = get(currentUserState);
    const targetUser = get(targetUserState);
    const enableMicrosoftGraphCalendar = get(enableMicrosoftGraphCalendarState);
    return(enableMicrosoftGraphCalendar && currentUser.id === targetUser.id);
  },
});
