import { atomFamily } from "recoil";

export type Effort = {
  id: number;
  issueId: number;
  date: string;
  value: number;
};

export const effortState = atomFamily<Effort | null, number>({
  key: "effortState",
  default: null,
});
