import { atom } from "recoil";

export const dayHoursState = atom<number>({
  key: "dayHoursState",
  default: 8,
});
