import { atom } from "recoil";

export type TargetUser = {
  id: number;
  name: string;
  day_hours: number;
};

export type CurrentUser = TargetUser;

export const targetUserState = atom<TargetUser>({
  key: "targetUserState",
  default: {id: 0, name: '', day_hours: 0},
});

export const currentUserState = atom<CurrentUser>({
  key: "currentUserState",
  default: {id: 0, name: '', day_hours: 0},
});
