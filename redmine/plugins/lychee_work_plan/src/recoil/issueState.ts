import { atomFamily } from "recoil";
import { Issue as RawIssue } from "@/api/lychee_work_plan/issues";

export type { RawIssue };

export type Issue = RawIssue & {
  [K in "startDate" | "dueDate"]: NonNullable<RawIssue[K]>;
} & {
  isVirtual?: boolean;
};

export const issueState = atomFamily<RawIssue | null, RawIssue["id"]>({
  key: "issueState",
  default: null,
});
