import { selector } from "recoil";
import { barWidthState } from "@/recoil/barWidthState";
import { DAYS_LOADED_AT_ONCE } from "@/constants";

export const defaultScrollLeftState = selector({
  key: "defaultScrollLeftState",
  get: ({ get }) => get(barWidthState) * DAYS_LOADED_AT_ONCE,
});
