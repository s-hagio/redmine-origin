import { selector, selectorFamily, DefaultValue } from "recoil";
import { effortIdsState } from "@/recoil/effortIdsState";
import { effortState } from "@/recoil/effortState";
import { Effort } from "@/api/lychee_work_plan/issues";

export const effortsState = selector<Effort[]>({
  key: "effortsState",
  get: ({ get }) =>
    get(effortIdsState)
      .map((id) => get(effortState(id)))
      .filter((effort): effort is Effort => effort !== null),
  set: ({ set }, efforts) => {
    if (efforts instanceof DefaultValue) {
      set(effortIdsState, []);
      return;
    }
    efforts.forEach((effort) => {
      set(effortState(effort.id), effort);
    });
    set(effortIdsState, (prevValue) =>
      Array.from(new Set([...prevValue, ...efforts.map((effort) => effort.id)]))
    );
  },
});

export const effortsByIssueIdState = selectorFamily<Effort[], number>({
  key: "effortsByIssueIdState",
  get:
    (issueId) =>
    ({ get }) =>
      get(effortsState).filter((e) => e.issueId === issueId),
  set:
    (issueId) =>
    ({ get, set }, efforts) => {
      if (efforts instanceof DefaultValue) {
        return;
      }
      efforts.forEach((effort) => {
        set(effortState(effort.id), effort);
      });
      set(effortIdsState, (prevValue) => {
        const otherEffortIds = prevValue
          .map((id) => get(effortState(id)))
          .filter((e): e is Effort => e !== null)
          .filter((e) => e.issueId !== issueId)
          .map((e) => e.id);
        return Array.from(
          new Set([...otherEffortIds, ...efforts.map((e) => e.id)])
        );
      });
    },
});
