import { atom, atomFamily, selector, DefaultValue } from "recoil";

type Project = {
  id: number;
  name: string;
  lft: number;
};

export const projectsState = selector<Project[]>({
  key: "projectsState",
  get: ({ get }) =>
    get(projectIdsState)
      .map((id) => get(projectState(id)))
      .filter((project): project is Project => project !== null)
      .sort((a, b) => a.lft - b.lft),
  set: ({ set }, projects) => {
    if (projects instanceof DefaultValue) {
      set(projectIdsState, []);
      return;
    }
    projects.forEach((project) => {
      set(projectState(project.id), project);
    });
    set(projectIdsState, (prevValue) =>
      Array.from(
        new Set([...prevValue, ...projects.map((project) => project.id)])
      )
    );
  },
});

const projectIdsState = atom<Project["id"][]>({
  key: "projectIdsState",
  default: [],
});

const projectState = atomFamily<Project | null, Project["id"]>({
  key: "projectState",
  default: null,
});
