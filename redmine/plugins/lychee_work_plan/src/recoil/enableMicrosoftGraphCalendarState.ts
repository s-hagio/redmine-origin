import { atom } from "recoil";

export const enableMicrosoftGraphCalendarState = atom<boolean>({
  key: "enableMicrosoftGraphCalendarState",
  default: false,
});
