import { selector } from "recoil";
import { effortIdsState } from "@/recoil/effortIdsState";
import { effortState, Effort } from "@/recoil/effortState";
import { issueState, Issue } from "@/recoil/issueState";

export const virtualIssuesState = selector<Issue[]>({
  key: "virtualIssuesState",
  get: ({ get }) =>
    get(effortIdsState)
      .map((id) => get(effortState(id)))
      .filter((effort): effort is Effort => effort !== null)
      .filter((effort) => {
        const issue = get(issueState(effort.issueId));
        if (!issue) {
          return false;
        }
        return (
          !issue.owned ||
          !issue.startDate ||
          !issue.dueDate ||
          effort.date < issue.startDate ||
          issue.dueDate < effort.date
        );
      })
      .map((effort) => {
        const issue = get(issueState(effort.issueId))!;
        return {
          ...issue,
          startDate: effort.date,
          dueDate: effort.date,
          isVirtual: true,
        };
      }),
});
