import { atom } from "recoil";

export const selectedIssueIdState = atom<number | null>({
  key: "selectedIssueIdState",
  default: null,
});
