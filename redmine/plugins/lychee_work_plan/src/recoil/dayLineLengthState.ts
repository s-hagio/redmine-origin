import { selector } from "recoil";
import { projectsState } from "@/recoil/projectsState";
import { issueRowsState } from "@/recoil/issueRowsState";
import { WORKLOAD_DAY_HEIGHT } from "@/constants";

export const dayLineLengthState = selector<number>({
  key: "dayLineLengthState",
  get: ({ get }) =>
    get(projectsState)
      .map(
        (project) =>
          44 + // project name row
          get(issueRowsState(project.id)).length * (WORKLOAD_DAY_HEIGHT + 7) +
          21 // mergin
      )
      .reduce((prev, curr) => prev + curr, 0) +
    get(projectsState).length * 2,
});
