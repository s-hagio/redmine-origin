import { selector, DefaultValue } from "recoil";
import { issueIdsState } from "@/recoil/issueIdsState";
import { issueState, Issue, RawIssue } from "@/recoil/issueState";

export const issuesState = selector<RawIssue[]>({
  key: "issuesState",
  get: ({ get }) =>
    get(issueIdsState)
      .map((id) => get(issueState(id)))
      .filter((issue): issue is RawIssue => issue !== null),
  set: ({ set }, issues) => {
    if (issues instanceof DefaultValue) {
      set(issueIdsState, []);
      return;
    }
    issues.forEach((issue) => {
      set(issueState(issue.id), issue);
    });
    set(issueIdsState, (prevValue) =>
      Array.from(new Set([...prevValue, ...issues.map((issue) => issue.id)]))
    );
  },
});

export const myIssuesState = selector<Issue[]>({
  key: "myIssuesState",
  get: ({ get }) =>
    get(issuesState).filter(
      (i): i is Issue => i.owned && i.startDate !== null && i.dueDate !== null
    ),
});
