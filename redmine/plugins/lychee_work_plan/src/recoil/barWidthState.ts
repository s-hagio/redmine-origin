import { atom } from "recoil";

export const barWidthState = atom<number>({
  key: "barWidthState",
  default: 91,
});
