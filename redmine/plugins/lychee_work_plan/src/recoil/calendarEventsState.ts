import { atom, useRecoilCallback } from "recoil";
import type { CalendarEvent } from "@/lib/calendarEvent";

export const calendarEventsState = atom<CalendarEvent[]>({
  key: "calendarEventsState",
  default: [],
});

export const useConcatCalendarEvents = () =>
  useRecoilCallback(({ snapshot, set }) => async (other: CalendarEvent[]) => {
    const previous = await snapshot.getPromise(calendarEventsState);
    const sum = [...previous];
    other.map((o) => {
      if(sum.findIndex((p) => p.id === o.id) < 0) {
        sum.push(o);
      }
    });
    set(calendarEventsState, sum);
  });
