import { atom } from "recoil";
import { Issue } from "@/recoil/issueState";

export const issueIdsState = atom<Issue["id"][]>({
  key: "issueIdsState",
  default: [],
});
