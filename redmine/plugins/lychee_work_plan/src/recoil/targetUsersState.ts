import { atom } from "recoil";
import { TargetUser } from "@/recoil/targetUserState";

export const targetUsersState = atom<TargetUser[]>({
  key: "targetUsersState",
  default: [],
});
