import { atom } from "recoil";

export const selectedDateEffortsState = atom<Date | null>({
  key: "selectedDateEffortsState",
  default: null,
});
