import { useRecoilState, useRecoilValue } from "recoil";
import { addDays, subDays } from "date-fns";
import { intervalState } from "@/recoil/intervalState";
import { targetUserState } from "@/recoil/targetUserState";
import { useSyncData } from "@/hooks/useSyncData";
import { DAYS_LOADED_AT_ONCE } from "@/constants";

export const useInterval = (): {
  addPreviousWeek: () => void;
  addNextWeek: () => void;
} => {
  const [interval, setInterval] = useRecoilState(intervalState);
  const syncData = useSyncData();
  const targetUser = useRecoilValue(targetUserState);

  return {
    addPreviousWeek: () => {
      const additional = {
        start: subDays(interval.start, DAYS_LOADED_AT_ONCE),
        end: subDays(interval.start, 1),
      };
      syncData(additional, targetUser.id);
      setInterval((currVal) => ({ ...currVal, start: additional.start }));
    },
    addNextWeek: () => {
      const additional = {
        start: addDays(interval.end, 1),
        end: addDays(interval.end, DAYS_LOADED_AT_ONCE),
      };
      syncData(additional, targetUser.id);
      setInterval((currVal) => ({ ...currVal, end: additional.end }));
    },
  };
};
