import { useRecoilValue } from "recoil";
import {
  timespanFormatState,
  TIMESPAN_FORMAT,
} from "@/recoil/timespanFormatState";

export const useFormatHours = (): ((hours: number | undefined) => string) => {
  const timespanFormat = useRecoilValue(timespanFormatState);

  return (hours: number | undefined) => {
    if (hours === undefined) {
      return "";
    }

    if (timespanFormat === TIMESPAN_FORMAT.MINUTES) {
      const h = Math.floor(hours);
      const m = Math.round((hours - h) * 60);
      return `${h}:${m.toString().padStart(2, "0")}`;
    }

    return hours.toString();
  };
};
