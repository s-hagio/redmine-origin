import { useRecoilValue } from "recoil";
import {
  timespanFormatState,
  TIMESPAN_FORMAT,
} from "@/recoil/timespanFormatState";

export const useHoursSuffix = (): string => {
  const timespanFormat = useRecoilValue(timespanFormatState);

  return timespanFormat === TIMESPAN_FORMAT.DECIMAL ? "h" : "";
};
