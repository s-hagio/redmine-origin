import { useRecoilValue } from "recoil";
import { format, getISODay } from "date-fns";
import { nonWorkingWeekDaysState } from "@/recoil/nonWorkingWeekDaysState";
import { restdaysState } from "@/recoil/restdaysState";
import { DATE_FORMAT } from "@/constants";

export const useRestday = (): { isRestday: (date: Date) => boolean } => {
  const holidays = useRecoilValue(nonWorkingWeekDaysState);
  const restdays = useRecoilValue(restdaysState);

  return {
    isRestday: (date: Date) =>
      holidays.includes(getISODay(date)) ||
      restdays.includes(format(date, DATE_FORMAT)),
  };
};
