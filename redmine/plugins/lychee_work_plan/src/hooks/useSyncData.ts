import { useRecoilValue, useSetRecoilState, useRecoilCallback } from "recoil";
import { useTranslation } from "react-i18next";
import { errorMessageState } from "@/recoil/errorMessageState";
import { issuesState } from "@/recoil/issuesState";
import { effortsState } from "@/recoil/effortsState";
import { projectsState } from "@/recoil/projectsState";
import { availableMicrosoftGraphCalendarState } from "@/recoil/availableMicrosoftGraphCalendarState";
import { useConcatCalendarEvents } from "@/recoil/calendarEventsState";
import { targetUserState } from "@/recoil/targetUserState";
import { MicrosoftGraphEvent, buildCalendarEventFromMicrosoftGraphEvent } from "@/lib/microsoftGraphEvent";
import { createClient } from "@/api/client";
import type { Issue, Effort, Project } from "@/api/lychee_work_plan/issues";
import type { Interval } from "@/recoil/intervalState";
import type { TargetUser } from "@/recoil/targetUserState";

type SyncData = (interval: Interval, targetUserId: number) => Promise<void>;

export const useSyncData = (): SyncData => {
  const { t } = useTranslation();
  const setErrorMessage = useSetRecoilState(errorMessageState);
  const availableMicrosoftGraphCalendar = useRecoilValue(availableMicrosoftGraphCalendarState);
  const setIssues = useSetRecoilState(issuesState);
  const setEfforts = useSetRecoilState(effortsState);
  const setProjects = useSetRecoilState(projectsState);
  const concatCalendarEvents = useConcatCalendarEvents();

  const syncData: SyncData = async (interval, targetUserId) => {
    const { issues, efforts, projects } = await getData(interval, targetUserId);
    setIssues(issues);
    setEfforts(efforts);
    setProjects(projects);

    if (!availableMicrosoftGraphCalendar) {
      return;
    }

    try {
      const { value: microsoftGraphEvents } = await getMicrosoftGraphCalendarEvents(interval);
      const fetchedCalendarEvents = microsoftGraphEvents.map(buildCalendarEventFromMicrosoftGraphEvent);
      concatCalendarEvents(fetchedCalendarEvents);
    }
    catch (e) {
      setErrorMessage(t("fetchingEventsFailed"));
    }
  };

  return syncData;
};

const getData = async (
  interval: Interval,
  targetUserId: number,
): Promise<{ issues: Issue[]; efforts: Effort[]; projects: Project[] }> => {
  const client = createClient();
  const { body } = await client.lychee_work_plan.issues.get({
    query: { from: interval.start, to: interval.end, target_user_id: targetUserId },
  });
  return body;
};

const getMicrosoftGraphCalendarEvents = async (
  interval: Interval
): Promise<{ value: MicrosoftGraphEvent[] }> => {
  const client = createClient();
  const { body } = await client.lychee_work_plan.microsoft_graph.calendar_events.get({
    query: { from: interval.start, to: interval.end },
  });
  return body;
};
