import { parseISO } from "date-fns";
import type { CalendarEvent } from "@/lib/calendarEvent";

// https://learn.microsoft.com/ja-jp/graph/api/resources/datetimetimezone?view=graph-rest-1.0
type MicrosoftGraphDateTimeTimeZone = {
  dateTime: string; // UTC
};

// https://learn.microsoft.com/ja-jp/graph/api/resources/event?view=graph-rest-1.0
export type MicrosoftGraphEvent = {
  id: string;
  subject: string;
  isAllDay: boolean;
  start: MicrosoftGraphDateTimeTimeZone;
  end: MicrosoftGraphDateTimeTimeZone;
};

export const buildCalendarEventFromMicrosoftGraphEvent = (event: MicrosoftGraphEvent): CalendarEvent => ({
  id: event.id,
  subject: event.subject,
  isAllDay: event.isAllDay,
  start: parseISO(`${event.start.dateTime}Z`),
  end: parseISO(`${event.end.dateTime}Z`),
});
