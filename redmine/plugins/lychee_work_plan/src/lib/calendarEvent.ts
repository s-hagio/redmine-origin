export type CalendarEvent = {
  id: string;
  subject: string;
  isAllDay: boolean;
  start: Date;
  end: Date;
};

export type CalendarEventHours = {
  id: string;
  date: string;
  hours: number;
  isWholeWorkDay: boolean;
  start: Date;
  end: Date;
  calendarEvent: CalendarEvent;
};

export const sumCalendarEventHours = (calendarEventHoursAry: CalendarEventHours[]) => (
  calendarEventHoursAry.reduce(
    (previousValue, calendarEventHours) => (previousValue + calendarEventHours.hours),
    0
  )
);
