# frozen_string_literal: true

require 'activerecord-import'

namespace :redmine do
  namespace :plugins do
    namespace :lychee_work_plan do
      desc 'Convert WorkPlan and TableMode::PLannedTimeEntry into Lychee::Effort'
      task :convert_into_efforts, %i[from to] => :environment do |_t, args|
        LycheeWorkPlan::Converter.new(from: args.from, to: args.to).convert
      end
    end
  end
end
