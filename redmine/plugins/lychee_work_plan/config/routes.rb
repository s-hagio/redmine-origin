# frozen_string_literal: true

get 'lychee_work_plan', to: 'lychee_work_plan#index'

namespace :lychee_work_plan do
  resources :issues, only: %i[index show], defaults: { format: :json } do
    resources :efforts, only: %i[create destroy]
    resource :autofill, only: :create
  end

  resource :aad_credential, only: %i[create destroy] do
    get :oauth_callback
  end

  namespace :microsoft_graph, defaults: { format: :json } do
    resources :calendar_events, only: %i[index]
  end
end
