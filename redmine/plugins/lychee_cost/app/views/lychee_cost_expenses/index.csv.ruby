require 'csv'

CSV.generate { |csv|
  csv_column_names = [
    LycheeCostExpense.human_attribute_name(:date),
    LycheeCostExpense.human_attribute_name(:name),
    LycheeCostExpense.human_attribute_name(:amount),
    LycheeCostExpense.human_attribute_name(:description)
  ]
  csv << csv_column_names

  @lychee_cost_expenses.each do |lychee_cost_expense|
    csv_column_values = [
      lychee_cost_expense.date,
      lychee_cost_expense.name,
      lychee_cost_expense.amount,
      lychee_cost_expense.description
    ]

    csv << csv_column_values
  end
}.encode(Encoding::UTF_8)
