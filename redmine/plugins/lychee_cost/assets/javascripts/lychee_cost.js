$(function() {
  function toggleTreeExpander($target, method){
    switch(method){
      case 'expand':
        $target.closest('tr').nextUntil('tr:not(:has(th.lychee-cost-cash-forecast__child-table-header))').show();
        $target.removeClass('tree-expander-collapsed').addClass('tree-expander-expanded');
        break;
      case 'collapse':
        $target.closest('tr').nextUntil('tr:not(:has(th.lychee-cost-cash-forecast__child-table-header))').hide();
        $target.removeClass('tree-expander-expanded').addClass('tree-expander-collapsed');
        break;
      default:
        break;
    }
  }

  $('.lychee-cost-cash-forecast__table-header > .tree-expander').each(function(){
    var $target = $(this);
    var method = 'expand'
    if($target.hasClass('tree-expander-collapsed')){
      method = 'collapse'
    }
    toggleTreeExpander($target, method)
  });

  $('.lychee-cost-cash-forecast__table-header > .tree-expander').on('click', function() {
    var $target = $(this);
    var method = 'expand'
    if($target.hasClass('tree-expander-expanded')){
      method = 'collapse'
    }
    toggleTreeExpander($target, method);
  });
});
