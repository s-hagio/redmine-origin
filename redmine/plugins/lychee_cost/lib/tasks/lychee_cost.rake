# frozen_string_literal: true

namespace :redmine do
  namespace :plugins do
    namespace :lychee_cost do
      desc 'プロジェクトCFの名称「受注金額」を変更する.'
      task change_sales_order_name: :environment do
        cf = ProjectCustomField.find_by(name: '受注金額')
        cf.update!(name: LycheeCost::BUDGET_PROJECT_CF_NAME) if cf.present? && cf.field_format == 'int'
      end
    end
  end
end
