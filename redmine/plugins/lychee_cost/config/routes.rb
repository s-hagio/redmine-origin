# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
#
resources :cost_groups, except: [:show] do
  resources :users, only: %i[new create destroy], controller: :cost_group_users
  member do
    get 'autocomplete_for_user'
  end
end

resources :projects do
  resources :lychee_cost_expenses, only: %i[index new create destroy edit update] do
    post :api, on: :collection
  end
end
