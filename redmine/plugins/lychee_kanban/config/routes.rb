# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
resources :projects, only: [], shallow: true do
  resources :kanban_queries, only: %i[show new create edit update destroy]
end

resources :projects, only: [] do
  resources :kanban_options, only: %i[create update]
  get :backlog, controller: :backlog, action: :index

  resources :kanban, only: :index do
    collection do
      get :assigned
      get :ranked
      get :burn_down
    end
  end

  namespace :lychee_issue_board, format: 'json' do
    resources :issues
    resources :memberships, only: :index, controller: :members
    resources :versions, except: :edit
    resources :workflows, only: :index
    resources :attachments, only: :destroy
    resources :timelog, only: :create
    post :uploads, to: 'attachments#upload'
  end

  namespace :backlogs do
    put 'spillover', to: 'sprints#spillover'
  end

  # TODO: Lychee Project ViewでPATHの修正が行われ次第、以下のroutesを削除する
  relative_url_root = ActionController::Base.relative_url_root
  resources :backlog, only: :index
  resources :assigned_kanban, only: [] do
    get :index, on: :collection, to: redirect("#{relative_url_root}/projects/%{project_id}/kanban")
  end
  resources :ranked_kanban, only: [] do
    get :index, on: :collection, to: redirect("#{relative_url_root}/projects/%{project_id}/kanban")
  end
end

put :lychee_issue_board_tracker_options, to: 'lychee_issue_board_tracker_options#update'
