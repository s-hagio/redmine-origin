namespace :redmine do
  namespace :plugins do
    namespace :lychee_issue_board do
      desc 'migrate to member id for lychee issue board'
      task migrate_to_member_id: :environment do
        LycheeKanban::MigrateKanbanDisplayUser.migrate_to_member_id
      end

      desc 'migrate to user id for lychee issue board'
      task migrate_to_user_id: :environment do
        LycheeKanban::MigrateKanbanDisplayUser.migrate_to_user_id
      end
    end
  end
end
