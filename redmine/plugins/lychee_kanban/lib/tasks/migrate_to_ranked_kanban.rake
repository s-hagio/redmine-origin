namespace :redmine do
  namespace :plugins do
    namespace :lychee_issue_board do
      desc 'migrate to ranked kanban for lychee issue board'
      task migrate_to_ranked_kanban: :environment do
        LycheeKanban::Migrate::PermissionHelper.rename_permission :kanban, :ranked_kanban
      end

      desc 'rollback from ranked kanban for lychee issue board'
      task rollback_from_ranked_kanban: :environment do
        LycheeKanban::Migrate::PermissionHelper.rename_permission :ranked_kanban, :kanban
      end
    end
  end
end
