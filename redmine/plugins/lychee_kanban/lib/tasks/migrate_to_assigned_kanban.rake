namespace :redmine do
  namespace :plugins do
    namespace :lychee_issue_board do
      desc 'migrate to assigned kanban for lychee issue board'
      task migrate_to_assigned_kanban: :environment do
        LycheeKanban::Migrate::PermissionHelper.rename_permission :task_kanbans, :assigned_kanban
      end

      desc 'rollback from assigned kanban for lychee issue board'
      task rollback_from_assigned_kanban: :environment do
        LycheeKanban::Migrate::PermissionHelper.rename_permission :assigned_kanban, :task_kanbans
      end
    end
  end
end
