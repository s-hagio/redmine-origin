namespace :redmine do
  namespace :plugins do
    namespace :lychee_issue_board do
      desc 'rename kanban permissions to "kanban"'
      task rename_kanban_permissions: :environment do
        LycheeKanban::Migrate::PermissionHelper.rename_permission :assigned_kanban, :ranked_kanban
      end

      desc 'rollback renaming permissions from "kanban"'
      task rollback_renaming_kanban_permissions: :environment do
        LycheeKanban::Migrate::PermissionHelper.rename_permission :ranked_kanban, %i[assigned_kanban ranked_kanban]
      end
    end
  end
end
