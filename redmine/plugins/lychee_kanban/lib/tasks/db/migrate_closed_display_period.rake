namespace :db do
  desc 'KanbanDefaultFilterに保存されているclosed_display_periodをKanbanOptionに移動する(v1.3.0からアップデート時限定)'
  task migrate_closed_display_period: :environment do
    ActiveRecord::Base.logger = Logger.new(STDOUT)
    ActiveRecord::Base.transaction do
      KanbanDefaultFilter.all.each do |kanban_default_filter|
        attributes = kanban_default_filter.attributes
        project = Project.find_by_id(attributes['project_id'])
        filters = attributes['filters'] || {}

        unique_filters = filters.delete('kanban_unique_filters') || {}
        display_period = unique_filters['closed_display_period'] || {}
        display_period_values = display_period[:values] || []
        if project.present? && display_period_values.present?
          self_and_descendants_users = project.self_and_descendants.map(&:users).flatten.uniq
          kanban_display_users =
            self_and_descendants_users.map { |user| KanbanDisplayUser.new(user_id: user.id) }
          KanbanOption.create!(
            project_id: project.id,
            display_period_closed_tickets: display_period_values[0].to_i,
            kanban_display_users: kanban_display_users
          )
        end

        kanban_default_filter.update(filters: filters)
      end
    end
  end

  desc 'KanbanOption.display_period_closed_tickets を KanbanDefaultFilterに移動する(注:rollback前に実行すること)'
  task rollback_closed_display_period: :environment do
    ActiveRecord::Base.logger = Logger.new(STDOUT)
    ActiveRecord::Base.transaction do
      KanbanOption.all.each do |kanban_option|
        closed_display_period = { values: [kanban_option.display_period_closed_tickets.to_s] }
        kanban_unique_filters = { 'closed_display_period' => closed_display_period }
        project = kanban_option.project
        kanban_default_filter =
          project.kanban_default_filter || project.build_kanban_default_filter
        kanban_default_filter.filters['kanban_unique_filters'] = kanban_unique_filters
        kanban_default_filter.save!
      end
    end
  end
end
