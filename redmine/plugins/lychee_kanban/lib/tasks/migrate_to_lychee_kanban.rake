namespace :redmine do
  namespace :plugins do
    namespace :lychee_kanban do
      desc 'migrate schema_migrations to lychee_kanban'
      task migrate_to_lychee_kanban: :environment do
        migration_lychee_agile = ActiveRecord::SchemaMigration.where("version LIKE ?", "%-lychee_agile")
        migration_lychee_agile.update_all("version = replace(version, 'lychee_agile', 'lychee_kanban')") if migration_lychee_agile.exists?

        migration_lychee_issue_board = ActiveRecord::SchemaMigration.where("version LIKE ?", "%-lychee_issue_board")
        migration_lychee_issue_board.update_all("version = replace(version, 'lychee_issue_board', 'lychee_kanban')") if migration_lychee_issue_board.exists?

        setting = Setting.find_by(name: :plugin_lychee_issue_board)
        setting.update(name: :plugin_lychee_kanban) if setting.present?
      end

      desc 'rollback schema_migrations to lychee_issue_board'
      task rollback_from_lychee_kanban_to_lychee_issue_board: :environment do
        migration_lychee_kanban = ActiveRecord::SchemaMigration.where("version LIKE ?", "%-lychee_kanban")
        migration_lychee_kanban.update_all("version = replace(version, 'lychee_kanban', 'lychee_issue_board')") if migration_lychee_kanban.exists?

        setting = Setting.find_by(name: :plugin_lychee_kanban)
        setting.update(name: :plugin_lychee_issue_board) if setting.present?
      end

      desc 'rollback schema_migrations to lychee_agile'
      task rollback_from_lychee_kanban_to_lychee_agile: :environment do
        migration_lychee_kanban = ActiveRecord::SchemaMigration.where("version LIKE ?", "%-lychee_kanban")
        migration_lychee_kanban.update_all("version = replace(version, 'lychee_kanban', 'lychee_agile')") if migration_lychee_kanban.exists?
      end
    end
  end
end
