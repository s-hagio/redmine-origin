# Lychee Kanban
リリースノートはいかに記載
URL: https://lychee-redmine.jp/release/issueboard/

## Requirement

- Agileware License Manager
- ruby 2.3.4
  - rubocop
- node 12.13.1 with yarn
  - eslint
  - stylelint

## Development

```bash
cd ./redmine
bundle exec rails server # Rails サーバー起動
```

```bash
cd ./plugins/lychee_kanban
yarn start # Node サーバー起動(自動ビルド)
```

Redmineが3000番ポートを、`yarn start` 実行後はwebpack-dev-serverが8080番ポートを使用する。
hot reloadを利用する際はwebpack-dev-serverの稼働している http://localhost:8080 へアクセスすること。

### TEST

#### Redmine

```bash
cd ./redmine
bundle exec rails g rspec:install

cd ./plugins/lychee_kanban
bundle exec guard # 自動テストプログラム起動
```

#### React

```bash
cd ./redmine/plugins/lychee_kanban
yarn test # React ユニットテスト実行
```

## Build Assets

```
yarn build # 本番用ビルドファイル生成
```

## Installation

```bash
cd ./redmine
bundle install -j4
bundle exec rake db:create db:migrate
bundle exec rake redmine:plugins:migrate NAME=lychee_kanban

cd ./plugins/lychee_kanban
yarn install
```
