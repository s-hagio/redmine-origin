version: 2.1

orbs:
  redmine-plugin: agileware-jp/redmine-plugin@3.3.0
  redmine:
    commands:
      install-rspec:
        steps:
          - run: bin/rails generate rspec:install
          - run: sudo apt-get install fonts-migmix
  rubocop:
    executors:
      rubocop-executor:
        parameters:
          ruby_version:
            description: version of Ruby
            type: string
            default: $RUBY_MAX_VERSION
        docker:
          - image: cimg/ruby:<<parameters.ruby_version>>
    commands:
      install-rubocop:
        steps:
          - run:
              name: Install Rubocop
              command: |
                set -e
                # rubocop 1.0に対応していないrubocopプラグイン向けにバージョンを一時下げる
                gem install rubocop -v '< 1.0'
                gem install rubocop-rails
                gem install rubocop-rspec
                gem install rubocop-performance
      install-cop-from-github:
        parameters:
          url:
            description: URL of Git repository of custom cop
            type: string
        steps:
          - run: git clone << parameters.url >> /tmp/cop-repo && cd /tmp/cop-repo && rake install && cd / && rm -rf /tmp/cop-repo
      run-cops:
        parameters:
          rubocop_option:
            description: CLI option for rubocop
            type: string
            default: '--format simple'
        steps:
          - run: rubocop << parameters.rubocop_option >>

jobs:
  build:
    docker:
      - image: cimg/node:12.13.1
    steps:
      - checkout
      - redmine-plugin/build-plugin-assets:
          plugin_folder: '~/project'
      - run: yarn eslint
      - run: yarn stylelint
      - run: yarn jest --runInBand
      - persist_to_workspace:
          root: '~/project'
          paths: [assets]
      # Store built JS for Testcafe tests
      - run:
          working_directory: ~/project
          command: zip -r assets.zip assets
      - store_artifacts:
          path: ~/project/assets.zip
          destination: assets.zip
  rubocop:
    parameters:
      rubocop_files:
        description: files to check by rubocop
        type: string
    executor: rubocop/rubocop-executor
    steps:
      - checkout
      - rubocop/install-rubocop
      - rubocop/install-cop-from-github:
          url: https://github.com/agileware-jp/rubocop-lychee.git
      - rubocop/install-cop-from-github:
          url: https://github.com/agileware-jp/rubocop-no_keyword_args.git
      - rubocop/run-cops:
          rubocop_option: --fail-level E --display-only-fail-level-offenses -f s -c ~/project/.rubocop.yml << parameters.rubocop_files >>
  brakeman:
    docker:
      - image: presidentbeef/brakeman
    steps:
      - checkout
      - run: /usr/src/app/bin/brakeman --color
  rspec:
    parameters:
      redmine_version:
        type: string
      ruby_version:
        type: string
      db:
        type: enum
        enum: ['mysql', 'pg']
      db_version:
        type: string
    executor:
      name: redmine-plugin/ruby-<< parameters.db >>
      ruby_version: << parameters.ruby_version >>
      db_version: << parameters.db_version >>
    steps:
      - run: sudo dpkg --purge google-chrome-stable
      - checkout
      - redmine-plugin/download-redmine:
          version: << parameters.redmine_version >>
      - redmine-plugin/install-self
      - attach_workspace:
          at: ~/project/redmine/plugins/lychee_kanban
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/alm.git
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/lad.git
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/lychee_remaining_estimate.git
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/lychee_status_color.git
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/lychee_profile_icon.git
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/lychee_custom_field.git
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/lychee_version_start_date.git
      - redmine-plugin/generate-database_yml
      - redmine-plugin/bundle-install
      - redmine-plugin/migrate-without-plugins # LAD入っているので、4.1対応で必要
      - redmine-plugin/rspec
      - store_artifacts:
          path: /tmp/coverage
          destination: coverage
      - store_artifacts:
          path: redmine/tmp/capybara
          destination: screenshots
  testcafe:
    machine: true
    parameters:
      redmine_version:
        type: string
      browser:
        type: string
    environment:
      PLUGIN_ROOT: /home/circleci/project/plugins
      BROWSER: '<< parameters.browser >>'
    steps:
      - checkout:
          path: ./plugins/lychee_kanban
      - run: git clone --depth 1 git@github.com:agileware-jp/testcafe.git
      - run:
          command: ./bin/install_lychee_plugins
          working_directory: ./testcafe
      - redmine-plugin/build-plugin-assets:
          plugin_folder: ./plugins/lychee_kanban
          use_docker: true
      - run:
          command: ./bin/run_tests lychee_kanban 4.1
          working_directory: ./testcafe

default_context: &default_context
  context:
    - lychee-ci-environment

ignore_trial: &ignore_trial
  filters:
    branches:
      ignore:
        - trial
        - /.*testcafe/

workflows:
  version: 2
  test:
    jobs:
      - rubocop:
          <<: *default_context
          <<: *ignore_trial
          name: 'rubocop'
          rubocop_files:  ~/project
      - brakeman:
          <<: *ignore_trial
      - build:
          <<: *ignore_trial
      - rspec:
          <<: *default_context
          <<: *ignore_trial
          name: RSpec on supported maximum versions with PostgreSQL
          redmine_version: $REDMINE_MAX_VERSION
          ruby_version: $RUBY_MAX_VERSION
          db: pg
          db_version: $POSTGRES_VERSION
          requires:
            - build
      - rspec:
          <<: *default_context
          <<: *ignore_trial
          name: RSpec on supported minimum versions with MySQL
          redmine_version: $REDMINE_MIN_VERSION
          ruby_version: $RUBY_MIN_VERSION
          db: mysql
          db_version: $MYSQL_VERSION
          requires:
            - build
