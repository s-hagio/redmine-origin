import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import map from 'lodash/map';

Enzyme.configure({ adapter: new Adapter() });

const mockT = (key, options) => {
  let result = key;
  if (options) {
    result += `(${map(options, (value, key) => `${key}: ${value}`).join(', ')})`;
  }
  return result;
};

jest.mock('react-i18next', () => ({
  withNamespaces: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: mockT };
    return Component;
  },
}));

jest.mock('i18next', () => ({
  t: mockT,
}));

jest.mock('react-dnd', () => ({
  useDrop: jest.fn(() => [{}, null]),
  useDrag: jest.fn(() => [{}, null]),
  DragLayer: jest.fn(() => jest.fn(component => component)),
}));
