module.exports = {
  root: true,
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 12,
    sourceType: 'module',
    requireConfigFile: false,
  },
  extends: [
    'plugin:react/recommended',
    'plugin:import/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:react-hooks/recommended',
    'react-app',
    'airbnb',
  ],
  plugins: [
    'react',
    'import',
    'jsx-a11y',
    'react-hooks',
  ],
  rules: {
    'arrow-parens': 'error',
    'camelcase': 'off',
    'comma-dangle': 'off',
    'function-paren-newline': 'off',
    'import/no-named-as-default-member': 'off',
    'import/prefer-default-export': 'off',
    'indent': ['error', 2, {
      SwitchCase: 1,
    }],
    'jsx-a11y/anchor-has-content': 'off',
    'jsx-a11y/anchor-is-valid': 'off',
    'jsx-a11y/click-events-have-key-events': 'off',
    'jsx-a11y/control-has-associated-label': 'off',
    'jsx-a11y/label-has-associated-control': 'off',
    'jsx-a11y/no-static-element-interactions': 'off',
    'max-classes-per-file': 'off',
    'new-cap': 'off',
    'no-return-assign': 'error',
    'no-unexpected-multiline': 'error',
    'no-unused-vars': 'error',
    'object-curly-newline': ['warn', {
      ObjectPattern: {
        multiline: true,
      }
    }],
    'object-curly-spacing': ['error', 'always'],
    'quote-props': ['warn', 'consistent-as-needed'],
    'react/forbid-prop-types': 'off',
    'react/prop-types': 'warn',
    'require-jsdoc': 'off',
    // HACK: 下記lint違反の修正
    'react/jsx-props-no-spreading': 'off',
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: './.webpack/webpack.base.conf.js'
      }
    },
    'import/core-modules': [
      'redux-saga/effects',
      'react-contextmenu'
    ]
  },
};
