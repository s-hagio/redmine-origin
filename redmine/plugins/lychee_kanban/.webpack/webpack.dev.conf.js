const webpack = require('webpack');
const merge = require('webpack-merge');
const path = require('path');
const baseWebpackConfig = require('./webpack.base.conf');

process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

module.exports = merge(baseWebpackConfig, {
  mode: 'development',
  entry: [path.resolve(__dirname, '../src/main.jsx')],
  output: {
    filename: 'bundle.js',
    publicPath: '/webpack',
  },
  devtool: 'eval',
  devServer: {
    port: 8080,
    host: '0.0.0.0',
    disableHostCheck: true,
    inline: true,
    hot: true,
    proxy: {
      '/': 'http://127.0.0.1:3000',
      '/webpack': 'http://127.0.0.1:8080',
    },
  },
  plugins: [new webpack.HotModuleReplacementPlugin()],
});
