/* eslint-env jquery */
/* eslint object-shorthand: 0 */

const BurnDownChart = {};

BurnDownChart.i18n_text = {
  schedule_label: 'Planned',
  actual_label: 'Actual',
  burn_down_types: {
    tickets: 'Number of issues',
    workload: 'Estimated time',
    points: 'Points',
  },
};

BurnDownChart.init = (function ($) {
  function chartData(data, burnDownType) {
    return {
      labels: data.labels,
      datasets: [
        {
          label: BurnDownChart.i18n_text.schedule_label,
          fill: false,
          lineTension: 0.1,
          borderColor: 'rgb(82, 125, 255)',
          backgroundColor: 'rgb(82, 125, 255)',
          borderDash: [],
          borderDashOffset: 0.0,
          pointBorderColor: 'rgb(82, 125, 255)',
          pointBackgroundColor: 'rgb(82, 125, 255)',
          pointRadius: 4,
          data: data.datasets.schedule[burnDownType],
          spanGaps: false,
        },
        {
          label: BurnDownChart.i18n_text.actual_label,
          fill: false,
          lineTension: 0.1,
          borderColor: 'rgb(255, 82, 82)',
          backgroundColor: 'rgb(255, 82, 82)',
          borderDash: [],
          borderDashOffset: 0.0,
          pointBorderColor: 'rgb(255, 82, 82)',
          pointBackgroundColor: 'rgb(255, 82, 82)',
          pointRadius: 4,
          data: data.datasets.actual[burnDownType],
          spanGaps: false,
        },
      ],
    };
  }

  function chartOptions(burnDownType) {
    return {
      scales: {
        yAxes: [
          {
            display: true,
            scaleLabel: {
              display: true,
              labelString: BurnDownChart.i18n_text.burn_down_types[burnDownType],
            },
            ticks: {
              callback: function (label, index, labels) {
                if (burnDownType == 'tickets') {
                  if (Math.floor(label) === label) {
                    return label;
                  } // 整数のlabelのみ表示
                  return null;
                }
                return label.toFixed(1); // 小数点第2位以下切り捨て
              },
            },
          },
        ],
      },
      tooltips: {
        callbacks: {
          title: function (item, data) {
            return data.datasets[item[0].datasetIndex].label;
          },
          label: function (item, data) {
            const fixed_value = burnDownType == 'tickets' ? 0 : 1;
            return data.datasets[item.datasetIndex].data[item.index].toFixed(fixed_value);
          },
        },
      },
    };
  }

  return function (Chart, ctx, data, i18n_text, $selectBox) {
    $.extend(BurnDownChart.i18n_text, i18n_text);

    let burnDownType = $selectBox.val() || data.burnDownType;

    let chart = Chart.Line(ctx, {
      data: chartData(data, burnDownType),
      options: chartOptions(burnDownType),
    });

    $selectBox.on('change', function () {
      burnDownType = $(this).val();

      chart.destroy();
      chart = Chart.Line(ctx, {
        data: chartData(data, burnDownType),
        options: chartOptions(burnDownType),
      });
    });
  };
}(jQuery));
