!function($) {
  $(function() {
    var form = $('#lychee_issue_board_sorting_form').detach();
    $('label[for=query_sort_criteria_attribute_0]').first().parent().replaceWith(form);
  });
}(jQuery);
