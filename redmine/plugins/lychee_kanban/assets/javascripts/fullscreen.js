!function($) {
  var KEY = 'task_kanban:fullscreen:state';
  var state = sessionStorage.getItem(KEY) || 'exit';

  function toggleState() {
    state = ['exit', 'start'][+(state === 'exit')];
  }

  function applyState() {
    $('html').toggleClass('task-kanban-is-fullscreen', state === 'start');
    $(window).trigger('resize');
    sessionStorage.setItem(KEY, state);
  }

  function updateTriggerElements() {
    $('[data-fullscreen]').each(function() {
      var $el = $(this);
      $el.text($el.attr('data-fullscreen-label-' + state));
      $el.toggleClass('task-kanban-icon-fullscreen__exit', state === 'start');
      $el.toggleClass('task-kanban-icon-fullscreen__start', state === 'exit');
    });
  }

  $(document).on('click', '[data-fullscreen]', function() {
    toggleState();
    applyState();
    updateTriggerElements();
    // Reactの,スクロールによる要素のサイズ調整が働かない為、強制的に発火
    window.scrollBy(0,1);
    window.scrollBy(0,-1);

    // Chartのcanvas sizeが更新されない時があるのでresizeします
    if (typeof Chart !== 'undefined') {
      for (var id in Chart.instances) {
        Chart.instances[id].resize();
      }
    }
  });

  $(updateTriggerElements);
  applyState();
}(jQuery);
