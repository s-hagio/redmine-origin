api.version do
  api.id @version.id
  api.project(id: @version.project_id, name: @version.project.name) unless @version.project.nil?

  api.name             @version.name
  api.description      @version.description
  api.status           @version.status
  api.start_date       @version.start_date
  api.due_date         @version.effective_date
  api.sharing          @version.sharing
  api.wiki_page_title  @version.wiki_page_title

  render_api_custom_values @version.custom_field_values, api
  api.estimated_velocity @velocity_calculator.estimated_velocity(@version)
  api.costs costs(@version)

  api.created_on @version.created_on
  api.updated_on @version.updated_on
end

api.safe_attribute_names safe_attribute_names

api.sharings @version.allowed_sharings

api.array :custom_fields do
  @version.visible_custom_field_values.each do |value|
    field = value.custom_field
    api.custom_field do
      api.id                field.id
      api.name              field.name
      api.field_format      field.field_format
      api.format_store      field.format_store
      api.is_required       field.is_required?
      api.multiple          field.multiple?

      possible_values = field.format.possible_values_options field, @version
      if possible_values.present?
        api.array :possible_values do
          possible_values.each do |text, value|
            api.possible_value do
              api.value value || text
              api.text text
            end
          end
        end

        api.default_value field.default_value.presence
      end
    end
  end
end
