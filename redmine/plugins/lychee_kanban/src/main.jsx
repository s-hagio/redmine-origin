import 'core-js/stable';
import 'regenerator-runtime/runtime';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import Stickyfill from 'stickyfilljs';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';

// importing config should come always first !!!
import './config';
import rootSaga, { sagaMiddleware } from './sagas';
import reducer from './reducers';
import { App } from './app';
import './main.css';

const composeEnhancers = process.env.NODE_ENV === 'production'
  ? compose
  : window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(rootSaga);

const stickies = document.querySelectorAll('.sticky');
Stickyfill.add(stickies);

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('target-content')
);
