import { createAction } from 'redux-actions';

export const SPILLOVER = 'backlogs/spillover';
export const spillover = createAction(SPILLOVER);
