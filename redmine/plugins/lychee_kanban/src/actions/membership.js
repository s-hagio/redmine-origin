import { createAction } from 'redux-actions';

// INDEX
export const requestMemberships = createAction('REQUEST_MEMBERSHIPS');
export const receiveMemberships = createAction('RECEIVE_MEMBERSHIPS');
