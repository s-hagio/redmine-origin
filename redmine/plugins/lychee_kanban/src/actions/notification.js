import { createAction } from 'redux-actions';
import * as _ from 'lodash';

export const mount = createAction('MOUNT_NOTIFICATION');
export const unmount = createAction('UNMOUNT_NOTIFICATION');
export const show = createAction(
  'SHOW_NOTIFICATION',
  (msg) => msg,
  (msg, options) => _.pick(options, ['level'])
);
export const showRedmineErrors = createAction('SHOW_REDMINE_ERRORS');
