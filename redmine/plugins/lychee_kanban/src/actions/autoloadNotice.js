import { createAction } from 'redux-actions';

export const show = createAction('SHOW_AUTOLOAD_NOTICE');
export const hide = createAction('HIDE_AUTOLOAD_NOTICE');
