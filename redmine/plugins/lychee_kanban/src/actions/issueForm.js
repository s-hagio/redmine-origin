import { createAction } from 'redux-actions';
import * as _ from 'lodash';

export const requestIssueForm = createAction(
  'REQUEST_ISSUE_FORM',
  (id) => id,
  (id, options) => options
);
export const receiveIssueForm = createAction('RECEIVE_ISSUE_FORM');
export const initializeIssueFormValues = createAction('INITIALIZE_ISSUE_FORM_VALUES');
export const showIssueForm = createAction(
  'SHOW_ISSUE_FORM',
  () => null,
  (options) => _.pick(options, ['position'])
);
export const hideIssueForm = createAction('HIDE_ISSUE_FORM');
