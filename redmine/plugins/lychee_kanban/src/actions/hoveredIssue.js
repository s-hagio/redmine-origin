import { createAction } from 'redux-actions';

export const setHoveredIssue = createAction('SET_HOVERED_ISSUE');
export const deleteHoveredIssue = createAction('DELETE_HOVERED_ISSUE');
