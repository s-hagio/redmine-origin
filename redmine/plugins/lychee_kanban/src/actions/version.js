import { createAction } from 'redux-actions';
import * as _ from 'lodash';

// INDEX
export const receiveVersions = createAction('RECEIVE_VERSIONS');
export const requestVersions = createAction('REQUEST_VERSIONS');

// SHOW
export const requestVersion = createAction(
  'REQUEST_VERSION',
  (id) => id,
  (id, options) => _.pick(options, ['query', 'position'])
);
export const receiveVersion = createAction('RECEIVE_VERSION');

// CREATE
export const createVersion = createAction('CREATE_VERSION');

// UPDATE
export const updateVersion = createAction('UPDATE_VERSION');

// DESTROY
export const destroyVersion = createAction('DESTROY_VERSION');
export const removeVersion = createAction('REMOVE_VERSION');

// OTHERS
export const showVersionForm = createAction(
  'SHOW_VERSION_FORM',
  null,
  (options) => _.pick(options, ['position'])
);
export const hideVersionForm = createAction('HIDE_VERSION_FORM');
export const receiveVersionForm = createAction('RECEIVE_VERSION_FORM');
export const saveVersion = (version) => {
  if (version.id) {
    return updateVersion(version);
  }
  return createVersion(version);
};
