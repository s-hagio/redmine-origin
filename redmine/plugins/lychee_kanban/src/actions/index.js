import { createAction } from 'redux-actions';

import * as attachment from './attachment';
import * as issue from './issue';
import * as issueForm from './issueForm';
import * as hoveredIssue from './hoveredIssue';
import * as loading from './loadingPanel';
import * as membership from './membership';
import * as notification from './notification';
import * as query from './query';
import * as version from './version';
import * as workflow from './workflow';
import * as backlog from './backlog';

// INITIALIZATION
export const initAssignedKanbanApp = createAction('INIT_ASSIGNED_KANBAN_APP');
export const initBacklogApp = createAction('INIT_BACKLOG_APP');
export const initRankedKanbanApp = createAction('INIT_RANKED_KANBAN_APP');
export const initKanbanSettingsApp = createAction('INIT_KANBAN_SETTINGS_APP');

export const Attachment = attachment;
export const Issue = issue;
export const IssueForm = issueForm;
export const HoveredIssue = hoveredIssue;
export const LoadingPanel = loading;
export const Membership = membership;
export const Notification = notification;
export const Query = query;
export const Version = version;
export const Workflow = workflow;
export const Backlog = backlog;
