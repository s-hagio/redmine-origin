import { createAction } from 'redux-actions';

// CREATE
export const createAttachment = createAction('CREATE_ATTACHMENT');
export const receiveAttachment = createAction('RECEIVE_ATTACHMENT');

// DESTROY
export const removeAttachment = createAction('REMOVE_ATTACHMENT');
export const destroyAttachment = createAction('DESTROY_ATTACHMENT');
