import { createAction } from 'redux-actions';

export const requestWorkflows = createAction('REQUEST_WORKFLOWS');
export const receiveWorkflows = createAction('RECEIVE_WORKFLOWS');
export const toggleWorkflow = createAction('TOGGLE_WORKFLOW');
