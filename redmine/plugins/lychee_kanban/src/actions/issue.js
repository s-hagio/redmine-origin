import { createAction } from 'redux-actions';
import * as _ from 'lodash';

// INDEX
export const receiveIssues = createAction('RECEIVE_ISSUES', (issues) => issues);
export const refreshIssues = createAction('REFRESH_ISSUES', (issues) => issues);

// SHOW
export const receiveIssue = createAction(
  'RECEIVE_ISSUE',
  (issue) => issue,
  (issue, options) => _.pick(options, ['before'])
);

// CREATE
export const createIssue = createAction('CREATE_ISSUE');
export const createSimpleIssue = createAction('CREATE_SIMPLE_ISSUE');

// UPDATE
export const updateIssue = createAction(
  'UPDATE_ISSUE',
  (issue) => issue,
  (issue, options) => options
);
export const replaceIssue = createAction('REPLACE_ISSUE');

// TIME ENTRY
export const createTimeEntry = createAction('CREATE_TIME_ENTRY');
export const receiveTimeEntry = createAction('RECEIVE_TIME_ENTRY');

// OTHER
export const sortIssues = createAction(
  'SORT_ISSUES',
  (options) => options.payload,
  (options) => options.meta
);
// TODO: receveIssueに統合したい
export const receiveIssueSorted = createAction(
  'RECEIVE_ISSUE_SORTED',
  (issue) => issue,
  (issue, options) => _.pick(options, [
    'id',
    'sort_column',
    'before_issue_id',
    'after_issue_id',
    'issue_index',
    'before',
  ])
);
export const saveIssue = (issue, { time_entry, path }) => {
  if (issue.id) {
    return updateIssue(issue, { time_entry, path });
  }
  return createIssue(issue);
};
export const requestGetRankedKanbanIssues = createAction('REQUEST_GET_RANKED_KANBAN_ISSUES');
export const requestGetAssignedKanbanIssues = createAction('REQUEST_GET_ASSIGNED_KANBAN_ISSUES');
