import { createAction } from 'redux-actions';

export const show = createAction('SHOW_LOADING_PANEL');
export const hide = createAction('HIDE_LOADING_PANEL');
