import * as React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { DragPreviewImage, useDrag } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';

import { updateIssue } from '@/actions/issue';
import * as ItemTypes from '@/constants/itemTypes';
import Membership from '@/components/Membership';

const MembershipContainer = (props) => {
  const { id, onDrop } = props;
  const [, drag, preview] = useDrag({
    item: { type: ItemTypes.MEMBERSHIP },
    begin: () => props,
    end: (_item, monitor) => {
      const issue = monitor.getDropResult();
      if (issue) {
        onDrop(issue.id, id);
      }
    },
  }, [props]);

  const emptyImage = React.useMemo(() => getEmptyImage().src, []);

  return (
    <div ref={drag}>
      <Membership {...props} />
      <DragPreviewImage connect={preview} src={emptyImage} />
    </div>
  );
};

MembershipContainer.propTypes = {
  id: PropTypes.number.isRequired,
  onDrop: PropTypes.func.isRequired,
};

const mapStateToProps = (state, { membership }) => ({
  id: membership.principal.id,
  name: membership.principal.name,
  avatar: membership.principal.avatar,
});

const mapDispatchToProps = (dispatch) => ({
  onDrop: (issueId, assignedToId) => {
    dispatch(
      updateIssue({
        id: issueId,
        assigned_to_id: assignedToId,
      })
    );
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MembershipContainer);
