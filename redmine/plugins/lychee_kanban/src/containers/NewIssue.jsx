import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import * as _ from 'lodash';
import { compose } from 'recompose';
import { withNamespaces } from 'react-i18next';

import ICON_ADD from '@/images/add.png';
import { requestIssueForm } from '@/actions/issueForm';
import Link from '@/components/Link';
import { AutoloadManager } from '@/autoload_manager';
import { getIssueFormPosition } from '@/utils';

const styleSelector = createSelector(
  (state) => state.redmine.issue_creatable,
  (creatable) => (creatable ? null : { display: 'none' })
);

const mapStateToProps = (state, { t }) => ({
  label: t('new_issue'),
  icon: ICON_ADD,
  style: styleSelector(state),
  filteredVersion: state.redmine.filtered_version,
});

const mapDispatchToProps = (dispatch) => ({
  showIssueForm: (query, position) => {
    AutoloadManager.stop();
    dispatch(requestIssueForm('new', { query, position }));
  },
});

const mergeProps = (
  { filteredVersion, ...stateProps },
  { showIssueForm },
  ownProps
) => ({
  onClick: () => {
    const position = getIssueFormPosition();
    showIssueForm(
      {
        fixed_version_id: _.get(filteredVersion, 'id'),
        start_date: _.get(filteredVersion, 'start_date'),
      },
      position
    );
  },
  ...stateProps,
  ...ownProps,
});

export default compose(
  withNamespaces('NewIssue'),
  connect(mapStateToProps, mapDispatchToProps, mergeProps)
)(Link);
