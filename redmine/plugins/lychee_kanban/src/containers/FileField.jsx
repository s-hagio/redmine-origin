import { connect } from 'react-redux';

import { sagaMiddleware } from '@/sagas';
import * as sagas from '@/sagas/attachment';
import FileField from '@/components/LycheeForm/fields/FileField';

const mapDispatchToProps = (dispatch, props) => ({
  onSelectFile: (file) => {
    sagaMiddleware
      .run(sagas.createAttachment, { payload: file })
      .done.then(({ upload }) => {
        props.input.onChange(upload.id);
      })
      .catch((error) => {
        // eslint-disable-next-line
        console.log(error);
      });
  },
  onDeleteFile: (id) => {
    sagaMiddleware
      .run(sagas.destroyAttachment, { payload: id })
      .done.then(() => {
        props.input.onChange('');
      })
      .catch((error) => {
        // eslint-disable-next-line
        console.log(error);
      });
  },
});

export default connect(null, mapDispatchToProps)(FileField);
