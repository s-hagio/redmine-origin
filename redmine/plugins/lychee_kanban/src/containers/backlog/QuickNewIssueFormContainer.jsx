import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import QuickNewIssueForm from '@/components/backlog/QuickNewIssueForm';
import { createSimpleIssue } from '@/actions/issue';

const trackerSelector = createSelector(
  [(state) => state.workflow, (state) => state.redmine.project_id],
  (workflows, projectId) => workflows.trackers
    .toArray()
    .filter((tracker) => tracker.activated_project_ids.includes(projectId))
);

const makeVersionSelector = () => createSelector(
  (state) => state.versions,
  (_state, props) => props.versionId,
  (versions, id) => versions.find(id) || {}
);

const makeMapStateToProps = () => {
  const versionSelector = makeVersionSelector();

  return (state, props) => ({
    trackers: trackerSelector(state),
    version: versionSelector(state, props),
  });
};

const mapDispatchToProps = { createSimpleIssue, };

const mergeProps = (
  { version, ...stateProps },
  { createSimpleIssue: createIssue },
  { versionId, ...ownProps }
) => ({
  ...stateProps,
  ...ownProps,
  onSubmit: (params) => {
    createIssue({
      ...params,
      fixed_version_id: versionId,
      start_date: version.start_date,
      due_date: version.due_date,
    });
  },
});

export default connect(
  makeMapStateToProps,
  mapDispatchToProps,
  mergeProps
)(QuickNewIssueForm);
