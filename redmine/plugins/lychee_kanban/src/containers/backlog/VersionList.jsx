import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import VersionList from '@/components/backlog/VersionList';

const versionsSelector = (state) => state.versions;

const versionArraySelector = createSelector([versionsSelector], (versions) =>
  // eslint-disable-next-line implicit-arrow-linebreak
  versions.toArray().sort((pre, next) => {
    if (!pre.due_date && !next.due_date) return pre.id - next.id;
    if (!pre.due_date) return -1;
    if (!next.due_date) return 1;
    return new Date(pre.due_date) - new Date(next.due_date);
  })
);

const mapStateToProps = (state) => ({ versions: versionArraySelector(state), });

export default connect(mapStateToProps)(VersionList);
