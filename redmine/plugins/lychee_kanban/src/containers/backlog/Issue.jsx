import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useDrop } from 'react-dnd';
import { createSelector } from 'reselect';
import moment from 'moment';

import { requestIssueForm } from '@/actions/issueForm';
import { setHoveredIssue, deleteHoveredIssue } from '@/actions/hoveredIssue';
import * as ItemTypes from '@/constants/itemTypes';
import Issue from '@/components/Issue';
import { getIssueFormPosition } from '@/utils';

const IssueContainer = (props) => {
  const [{ hovered }, drop] = useDrop({
    accept: ItemTypes.MEMBERSHIP,
    drop: () => {
      const { id } = props;
      return { id };
    },
    collect: (monitor) => ({ hovered: monitor.isOver(), }),
  });

  return (
    <div ref={drop}>
      <Issue {...props} hovered={hovered} />
    </div>
  );
};

IssueContainer.propTypes = { id: PropTypes.number.isRequired, };

const makeUntouchedSelector = () => createSelector(
  (state) => state.trackers,
  (state, props) => props.issue,
  (trackers, issue) => {
    const tracker = trackers.find(issue.tracker.id);
    const firstStatus = tracker ? tracker.issue_statuses.first() : {};
    const isFirstStatus = firstStatus && issue.status.id === firstStatus.id;
    const isBehind = moment(issue.start_date) < moment();
    return isFirstStatus && isBehind;
  }
);

const makeRelationTypeSelector = () => createSelector(
  (state, props) => props.issue,
  (issue) => issue.relationType
);

const makeRelationHighlightTypeSelector = () => createSelector(
  (state) => state.hoveredIssue,
  (state, props) => props.issue,
  (id, issue) => issue.relationTypeWith(id)
);

const makeMapStateToProps = () => {
  const relationTypeSelector = makeRelationTypeSelector();
  const relationHighlightTypeSelector = makeRelationHighlightTypeSelector();
  const untouchedSelector = makeUntouchedSelector();

  return (state, props) => {
    const { redmine } = state;
    const { issue } = props;
    const costUnit = state.versions.unit;
    return {
      assignedToName: issue.assigned_to.name,
      assignedToAvatar: issue.assigned_to.avatar,
      avatarEnabled: redmine.avatar_enabled,
      blocking: issue.blocking,
      closed: issue.closed,
      costs: issue.costs[costUnit],
      costType: costUnit,
      customFields: issue.custom_fields,
      description: issue.description_html,
      dueDate: issue.due_date,
      fixedVersionId: issue.fixed_version.id,
      id: issue.id,
      notes: issue.notes_html,
      projects: issue.parent_projects_list,
      relationType: relationTypeSelector(state, props),
      relationHighlightType: relationHighlightTypeSelector(state, props),
      startDate: issue.start_date,
      statusId: issue.status.id,
      statusColor: issue.lychee_status_color,
      subject: issue.subject,
      trackerColor: issue.color,
      warning: untouchedSelector(state, props),
      days_elapsed: issue.days_elapsed,
    };
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  onClick() {
    const position = getIssueFormPosition();
    dispatch(requestIssueForm(props.issue.id, { position }));
  },
  onMouseOver() {
    if (props.issue.hasRelation) {
      dispatch(setHoveredIssue(props.issue.id));
    }
  },
  onMouseOut() {
    if (props.issue.hasRelation) {
      dispatch(deleteHoveredIssue(props.issue.id));
    }
  },
});

export default connect(makeMapStateToProps, mapDispatchToProps)(IssueContainer);
