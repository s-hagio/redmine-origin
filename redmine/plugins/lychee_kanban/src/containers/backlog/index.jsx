import React, { Component } from 'react';
import { hot } from 'react-hot-loader/root';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '@/actions';
import BacklogApp from '@/components/backlog/BacklogApp';

class BacklogAppContainer extends Component {
  // HACK: lint 違反の修正
  // eslint-disable-next-line react/no-deprecated
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(actions.initBacklogApp());
  }

  render() {
    return <BacklogApp />;
  }
}

BacklogAppContainer.propTypes = { dispatch: PropTypes.func.isRequired, };

export default connect()(hot(BacklogAppContainer));
