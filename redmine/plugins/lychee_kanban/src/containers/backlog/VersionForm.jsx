import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { createSelector } from 'reselect';
import { List, Map } from 'immutable';

import * as actions from '@/actions';
import * as forms from '@/constants/formTypes';
import VersionForm, { SHARING_OPTIONS } from '@/components/backlog/VersionForm';

const initialValuesSelector = createSelector(
  (state) => state.versionForm.version,
  (version) => Map({
    id: version.get('id'),
    name: version.get('name'),
    description: version.get('description'),
    status: version.get('status'),
    wiki_page_title: version.get('wiki_page_title'),
    start_date: version.get('start_date', ''),
    effective_date: version.get('due_date', ''),
    sharing: version.get('sharing'),
    // idをkeyとしたHash形式に変換
    custom_field_values: Map(
      version
        .get('custom_fields', List())
        .map((field) => [field.get('id'), field.get('value')])
    ),
  }).toJS()
);

const versionFormOptionsSelector = createSelector(
  (state) => state.versionForm,
  (versionForm) => {
    const form = versionForm.toJS();
    return {
      customFields: form.custom_fields,
      safeAttributes: form.safe_attribute_names,
    };
  }
);

const fieldOptionsSelector = createSelector(
  (state) => state.versionForm,
  (versionForm) => {
    const sharings = versionForm.get('sharings', []);
    return {
      sharings: SHARING_OPTIONS.filter((option) => sharings.includes(option.value)),
    };
  }
);

const mapStateToProps = (state) => ({
  id: state.versionForm.getIn(['version', 'id']),
  projectId: state.versionForm.getIn(['version', 'project', 'id']),
  open: state.versionForm.isOpen,
  position: state.versionForm.position,
  initialValues: initialValuesSelector(state),
  fieldOptions: fieldOptionsSelector(state),
  dateFormat: state.redmine.date_format,
  ...versionFormOptionsSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  onClose() {
    dispatch(actions.Version.hideVersionForm());
  },
  onSubmit(values) {
    dispatch(actions.Version.saveVersion(values));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: forms.VERSION_FORM,
    enableReinitialize: true,
  })(VersionForm)
);
