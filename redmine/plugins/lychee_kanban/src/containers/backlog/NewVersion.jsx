import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withNamespaces } from 'react-i18next';

import * as actions from '@/actions/version';
import ICON_ADD from '@/images/add.png';
import Link from '@/components/Link';

const mapStateToProps = (state, { t }) => ({
  label: t('new_version'),
  icon: ICON_ADD,
});

const mapDispatchToProps = (dispatch) => ({
  onClick: ({ clientX, clientY }) => {
    const options = { position: { x: clientX, y: clientY } };
    dispatch(actions.requestVersion('new', options));
  },
});

export default compose(
  withNamespaces('NewVersion'),
  connect(mapStateToProps, mapDispatchToProps)
)(Link);
