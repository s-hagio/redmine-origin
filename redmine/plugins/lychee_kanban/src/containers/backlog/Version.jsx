import { connect } from 'react-redux';
import {
  createSelector,
  createSelectorCreator,
  defaultMemoize,
} from 'reselect';
import { isEqual } from 'lodash';

import { IssueForm, Version, Backlog } from '@/actions';
import Component from '@/components/backlog/Version';
import { getIssueFormPosition } from '@/utils';

const createDeepEqualSelector = createSelectorCreator(defaultMemoize, isEqual);

const makeIssuesSelector = () => createDeepEqualSelector(
  (state, { version = {} }) => state.issue.filter(
    (item) => Number(item.fixed_version.id || 0) === Number(version.id || 0)
  ),
  (state) => state.redmine.sort_column,
  (issues, column) => issues.sortByColumn(column)
);

const makeIssueArraySelector = () => createSelector(
  (issues) => issues,
  (issues) => issues.toArray()
);
const makeMapStateToProps = () => {
  const issuesSelector = makeIssuesSelector();
  const issueArraySelector = makeIssueArraySelector();

  return (state, props) => {
    const { redmine, versions: versionList } = state;
    const { version } = props;
    const issues = issuesSelector(state, props);
    const workDays = version.get('work_days');

    return {
      costs: version.get('costs'),
      costType: versionList.unit,
      creatable: redmine.issue_creatable,
      dueDate: version.get('due_date'),
      dateFormat: redmine.date_format,
      estimatedVelocity: version.get('estimated_velocity'),
      workDays,
      id: version.get('id'),
      issues: issueArraySelector(issues),
      isClosed: !version.isOpen,
      name: version.get('name'),
      showEstimate: version.isOpen && version.hasPeriod,
      startDate: version.get('start_date'),
    };
  };
};

const mapDispatchToProps = (dispatch, { version }) => ({
  onShow: (event) => {
    const { clientX, clientY } = event;
    const position = { x: clientX, y: clientY };
    dispatch(Version.requestVersion(version.id, { position }));
  },
  onToggle: () => {
    dispatch(
      Version.updateVersion({
        id: version.id,
        status: version.isOpen ? 'closed' : 'open',
      })
    );
  },
  onDestroy: () => dispatch(Version.destroyVersion(version.id)),
  showIssueForm: (query, position) => {
    dispatch(IssueForm.requestIssueForm('new', { query, position }));
  },
  onMoveOpenIssues: (versionId) => {
    dispatch(Backlog.spillover({ fromVersionId: version.id, toVersionId: versionId }));
  },
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onAddIssue: stateProps.creatable && (() => {
    const position = getIssueFormPosition();
    dispatchProps.showIssueForm(
      {
        fixed_version_id: ownProps.version.id,
        start_date: ownProps.version.start_date,
        due_date: ownProps.version.due_date,
      },
      position
    );
  }),
});

export default connect(
  makeMapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Component);
