import { connect } from 'react-redux';

import { sortIssues } from '@/actions/issue';
import IssueDropArea from '@/components/backlog/IssueDropArea';

const mapStateToProps = ({ redmine, versions }, props) => ({
  sortable: redmine.sort_column === 'sort_by_backlog',
  version: versions.find(props.versionId),
});

const mapDispatchToProps = (dispatch) => ({
  sortIssues: (options) => dispatch(sortIssues(options)),
});

export default connect(mapStateToProps, mapDispatchToProps)(IssueDropArea);
