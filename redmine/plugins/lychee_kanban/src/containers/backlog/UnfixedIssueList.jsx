import { connect } from 'react-redux';
import { createSelectorCreator, defaultMemoize } from 'reselect';
import { isEqual } from 'lodash';

import { requestIssueForm } from '@/actions/issueForm';
import UnfixedIssueList from '@/components/backlog/UnfixedIssueList';
import { getIssueFormPosition } from '@/utils';

const createDeepEqualSelector = createSelectorCreator(defaultMemoize, isEqual);

const issuesSelector = createDeepEqualSelector(
  (state) => state.issue.unfixed,
  (state) => state.redmine.sort_column,
  (issues, column) => issues.sortByColumn(column).toArray()
);

const mapStateToProps = (state) => ({
  issues: issuesSelector(state),
  creatable: state.redmine.issue_creatable,
});

const mapDispatchToProps = (dispatch) => ({
  showIssueForm: (position) => {
    dispatch(requestIssueForm('new', { position }));
  },
});

const mergeProps = (stateProps, { showIssueForm }, ownProps) => ({
  ...ownProps,
  ...stateProps,
  onAddIssue: stateProps.creatable && (() => {
    const position = getIssueFormPosition();
    showIssueForm({ position });
  }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(UnfixedIssueList);
