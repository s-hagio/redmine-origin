import { connect } from 'react-redux';
import { requestVersions } from '@/actions/version';
import Velocity from '@/components/backlog/Velocity';

const mapStateToProps = (state) => {
  const { versions } = state;
  const availableVelocityUnit = state.redmine.available_velocity_unit;

  return {
    velocities: versions.velocities,
    unit: versions.unit,
    dailyVelocity: versions.dailyVelocity,
    availableVelocityUnit,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onChange: (args) => {
    dispatch(requestVersions(args));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Velocity);
