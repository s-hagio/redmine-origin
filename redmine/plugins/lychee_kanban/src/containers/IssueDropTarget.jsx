import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useDrop } from 'react-dnd';
import classnames from 'classnames';
import * as _ from 'lodash';

import { updateIssue } from '@/actions/issue';
import * as ItemTypes from '@/constants/itemTypes';

const needRequest = (currentParams, requestParams) => _.some(
  requestParams,
  (value, key) => String(currentParams[key] || '') !== String(value || '')
);

const IssueDropTarget = (props) => {
  const { assigned_to_id, status_id, onDrop, className, style, children } = props;

  const [{ highlighted, hovered }, drop] = useDrop({
    accept: ItemTypes.ISSUE,
    drop: (sourceProps) => {
      const {
        id: sourceIssueId,
        assignedToId: sourceIssueAssignedToId,
        statusId: sourceIssueStatusId,
      } = sourceProps;

      const currentParams = {
        assigned_to_id: sourceIssueAssignedToId,
        status_id: sourceIssueStatusId,
      };
      const requestParams = {
        assigned_to_id,
        status_id,
      };

      if (needRequest(currentParams, requestParams)) {
        onDrop(sourceIssueId, requestParams);
      }
    },
    canDrop(sourceProps) {
      const { allowedIssueStatuses = [] } = sourceProps;
      const nil = _.isNil(status_id);
      const exists = _.find(allowedIssueStatuses, { id: status_id });
      return nil || exists;
    },
    collect: (monitor) => ({
      highlighted: monitor.canDrop(),
      hovered: monitor.canDrop() && monitor.isOver(),
    }),
  });

  const classes = classnames({
    'react-dnd--hovered': hovered,
    'react-dnd--highlighted': highlighted,
    [className]: true,
  });

  return (
    <div className={classes} style={style} ref={drop}>
      {children}
    </div>
  );
};

IssueDropTarget.propTypes = {
  assigned_to_id: PropTypes.number,
  status_id: PropTypes.number,
  onDrop: PropTypes.func.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.node,
};

IssueDropTarget.defaultProps = {
  assigned_to_id: null,
  status_id: null,
  className: '',
  style: null,
  children: null,
};

const mapDispatchToProps = (dispatch) => ({
  onDrop: (id, requestParams) => {
    dispatch(updateIssue({ id, ...requestParams }));
  },
});

export default connect(null, mapDispatchToProps)(IssueDropTarget);
