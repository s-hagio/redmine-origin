import React from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import MembershipList from '@/components/MembershipList';

const membershipsSelector = createSelector(
  (state) => state.membership,
  (membership) => membership.toArray()
);

const mapStateToProps = (state) => ({
  enabled: state.redmine.avatar_enabled,
  memberships: membershipsSelector(state),
});

export default connect(
  mapStateToProps,
)(({ enabled, ...props }) => enabled && <MembershipList {...props} />);
