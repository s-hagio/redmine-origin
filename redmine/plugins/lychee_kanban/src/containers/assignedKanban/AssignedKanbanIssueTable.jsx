import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import AssignedKanbanIssueTable from '@/components/assignedKanban/AssignedKanbanIssueTable';

const membershipSelector = createSelector(
  (state) => state.membership,
  (membership) => membership.toArray()
);

const issueStatusSelector = createSelector(
  (state) => state.workflow.current,
  (workflow) => workflow.issue_statuses.toArray()
);

const mapStateToProps = (state) => ({
  // issueは使っていないが、テーブル高さの変更を検知するためpropsに入れている
  issue: state.issue,
  issueStatuses: issueStatusSelector(state),
  memberships: membershipSelector(state),
});

export default connect(mapStateToProps)(AssignedKanbanIssueTable);
