import { connect } from 'react-redux';
import AssignedKanbanWorkflowWorkload from '@/components/assignedKanban/AssignedKanbanWorkflowWorkload';

const mapStateToProps = ({ issue }) => ({ issue, });

export default connect(mapStateToProps)(AssignedKanbanWorkflowWorkload);
