import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import * as _ from 'lodash';

import { requestIssueForm } from '@/actions/issueForm';
import UnassignedIssueList from '@/components/assignedKanban/UnassignedIssueList';
import { AutoloadManager } from '@/autoload_manager';
import { getIssueFormPosition } from '@/utils';

const issueSelector = createSelector(
  (state) => state.issue,
  (state) => state.workflow.current,
  (state) => state.redmine.sort_column,
  (issues, workflow, column) =>
    // eslint-disable-next-line implicit-arrow-linebreak
    issues
      .findByWorkflow(workflow)
      .unassigned
      .sortByColumn(column)
      .toArray()
);

const mapStateToProps = (state) => ({
  issues: issueSelector(state),
  creatable: state.redmine.issue_creatable,
  versionId: _.get(state.redmine.filtered_version, 'id'),
  startDate: _.get(state.redmine.filtered_version, 'start_date'),
});

const mapDispatchToProps = (dispatch) => ({
  showIssueForm(query, position) {
    dispatch(requestIssueForm('new', { query, position }));
  },
});

const mergeProps = (stateProps, { showIssueForm }, ownProps) => ({
  ...ownProps,
  ...stateProps,
  onAddIssue: stateProps.creatable && (() => {
    const position = getIssueFormPosition();
    AutoloadManager.stop();
    showIssueForm(
      {
        fixed_version_id: stateProps.versionId,
        start_date: stateProps.startDate,
      },
      position
    );
  }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(UnassignedIssueList);
