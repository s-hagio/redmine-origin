import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import AssignedKanbanTableUserList from '@/components/assignedKanban/AssignedKanbanTableUserList';

const membershipSelector = createSelector(
  (state) => state.membership,
  (membership) => membership.toArray()
);

const mapStateToProps = (state) => ({
  avatarEnabled: state.redmine.avatar_enabled,
  showWorkload: state.redmine.display_workload,
  memberships: membershipSelector(state),
});

export default connect(
  mapStateToProps,
  null,
  null,
  { forwardRef: true },
)(AssignedKanbanTableUserList);
