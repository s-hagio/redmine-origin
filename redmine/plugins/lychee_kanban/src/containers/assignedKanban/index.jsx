import { connect } from 'react-redux';
import { lifecycle } from 'recompose';
import { hot } from 'react-hot-loader/root';

import * as actions from '@/actions';
import AssignedKanbanApp from '@/components/assignedKanban/AssignedKanbanApp';
import { AutoloadManager } from '@/autoload_manager';

const startAutoload = () => {
  AutoloadManager.func();
  AutoloadManager.start();
};

const stopAutoload = () => {
  AutoloadManager.stop();
};

export default connect()(
  lifecycle({
    componentWillMount() {
      this.props.dispatch(actions.initAssignedKanbanApp());
    },
    componentDidMount() {
      const requestGetIssues = () => {
        this.props.dispatch(actions.Issue.requestGetAssignedKanbanIssues());
      };
      AutoloadManager.setFunc(() => { requestGetIssues(); });
      window.addEventListener('focus', startAutoload);
      window.addEventListener('blur', stopAutoload);
    },
    componentWillUnmount() {
      AutoloadManager.stop();
      window.removeEventListener('focus', startAutoload);
      window.removeEventListener('blur', stopAutoload);
    },
  })(hot(AssignedKanbanApp))
);
