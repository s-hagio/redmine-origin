import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import AssignedKanbanTableUserWorkload from '@/components/assignedKanban/AssignedKanbanTableUserList/AssignedKanbanTableUserWorkload';

const activeIssueStatusSelector = createSelector(
  (state) => state.workflow.current,
  (workflow) => workflow.issue_statuses.filter(({ is_closed }) => !is_closed)
);

const makeAssignedIssueSelector = () => createSelector(
  (state) => state.issue,
  (state) => state.workflow.current,
  (state, props) => props.member.principal,
  (issues, workflow, principal) => issues
    .findByWorkflow(workflow)
    .filter(({ assigned_to = {} }) => assigned_to.id === principal.id)
);

const makeEstimatedHoursSelector = () => createSelector(
  (issues) => issues,
  (issues) => issues.workload
);

const makeImcompleteHoursSelector = () => createSelector(
  (issues) => issues,
  (issues, statuses) => statuses,
  (issues, statuses) => {
    const statusIds = statuses.map(({ id }) => id);
    const imcompleteIssues = issues.filter((issue) => statusIds.includes(issue.status.id));
    return imcompleteIssues.workload;
  }
);

const makeMapStateToProps = () => {
  const assignedIssueSelector = makeAssignedIssueSelector();
  const estimatedHoursSelector = makeEstimatedHoursSelector();
  const imcompleteHoursSelector = makeImcompleteHoursSelector();

  return (state, props) => {
    const activeStatuses = activeIssueStatusSelector(state);
    const assignedIssues = assignedIssueSelector(state, props);
    const estimatedHours = estimatedHoursSelector(assignedIssues);
    const imcompleteHours = imcompleteHoursSelector(
      assignedIssues,
      activeStatuses
    );

    return {
      estimate: estimatedHours,
      imcomplete: imcompleteHours,
      remains: estimatedHours - props.member.spent,
      spent: props.member.isUser() ? props.member.spent : null,
    };
  };
};

export default connect(makeMapStateToProps)(AssignedKanbanTableUserWorkload);
