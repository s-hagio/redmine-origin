import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import styled from 'styled-components';
import moment from 'moment';

import { requestIssueForm } from '@/actions/issueForm';
import { setHoveredIssue, deleteHoveredIssue } from '@/actions/hoveredIssue';
import Issue from '@/components/Issue';
import { ORIGIN_PROJECTS_PATH } from '@/constants/endpoints';
import { AutoloadManager } from '@/autoload_manager';
import { getIssueFormPosition } from '@/utils';

const StyledIssue = styled(Issue)`
  .assigned-to {
    display: none;
  }
`;

const makeUntouchedSelector = () => createSelector(
  (state) => state.trackers,
  (state, props) => props.issue,
  (trackers, issue) => {
    const tracker = trackers.find(issue.tracker.id);
    const firstStatus = tracker ? tracker.issue_statuses.first() : {};
    const isFirstStatus = firstStatus && issue.status.id === firstStatus.id;
    const isBehind = moment(issue.start_date) < moment();
    return isFirstStatus && isBehind;
  }
);

const makeRelationTypeSelector = () => createSelector(
  (state, props) => props.issue,
  (issue) => issue.relationType
);

const makeRelationHighlightTypeSelector = () => createSelector(
  (state) => state.hoveredIssue,
  (state, props) => props.issue,
  (id, issue) => issue.relationTypeWith(id)
);

const makeMapStateToProps = () => {
  const relationTypeSelector = makeRelationTypeSelector();
  const relationHighlightTypeSelector = makeRelationHighlightTypeSelector();
  const untouchedSelector = makeUntouchedSelector();

  return (state, props) => {
    const { issue } = props;

    return {
      allowedIssueStatuses: issue.allowed_issue_statuses,
      ancestors: issue.ancestors,
      assignedToId: issue.assigned_to.id,
      assignedToName: issue.assigned_to.name,
      assignedToAvatar: issue.assigned_to.avatar,
      blocking: issue.blocking,
      customFields: issue.custom_fields,
      description: issue.description_html,
      dueDate: issue.due_date,
      estimatedHours: issue.estimated_hours,
      id: issue.id,
      nodeType: issue.nodeType,
      notes: issue.notes_html,
      projects: issue.parent_projects_list,
      relationType: relationTypeSelector(state, props),
      relationHighlightType: relationHighlightTypeSelector(state, props),
      statusId: issue.status?.id,
      subject: issue.subject,
      trackerColor: issue.color,
      warning: untouchedSelector(state, props),
      days_elapsed: issue.days_elapsed,
    };
  };
};

const position = getIssueFormPosition();
const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick() {
    AutoloadManager.stop();
    dispatch(requestIssueForm(ownProps.issue.id, { position }));
  },
  onParentClick(parent) {
    const { id, project_id } = parent;
    AutoloadManager.stop();
    dispatch(
      requestIssueForm(id, {
        position,
        path: `${ORIGIN_PROJECTS_PATH}/${project_id}/lychee_issue_board/issues`,
      })
    );
  },
  onMouseOver() {
    if (ownProps.issue.hasRelation) {
      dispatch(setHoveredIssue(ownProps.issue.id));
    }
  },
  onMouseOut() {
    if (ownProps.issue.hasRelation) {
      dispatch(deleteHoveredIssue(ownProps.issue.id));
    }
  },
});

export default connect(makeMapStateToProps, mapDispatchToProps)(StyledIssue);
