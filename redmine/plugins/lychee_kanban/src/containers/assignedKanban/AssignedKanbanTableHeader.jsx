import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import AssignedKanbanTableHeader from '@/components/assignedKanban/AssignedKanbanTableHeader';

import { issueCountsSelector } from '@/containers/IssueCountSelector';

const targetIssues = createSelector(
  (state) => state.issue,
  (state) => state.membership,
  (issue, membership) => {
    const membershipIds = membership.toArray().map((m) => m.principal.id);
    const assignedIssues = issue.items.filter((item) => {
      const assignedToId = item.assigned_to.id;
      return assignedToId && membershipIds.includes(assignedToId);
    });
    return assignedIssues;
  }
);

const mapStateToProps = (state) => {
  const issueConts = issueCountsSelector();

  return {
    issueStatuses: state.workflow.current.issue_statuses,
    issueCounts: issueConts(targetIssues(state), state.workflow.current),
  };
};

export default connect(mapStateToProps)(AssignedKanbanTableHeader);
