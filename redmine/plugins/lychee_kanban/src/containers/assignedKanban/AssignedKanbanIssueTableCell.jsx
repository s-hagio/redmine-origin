import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import * as _ from 'lodash';

import { requestIssueForm } from '@/actions/issueForm';
import AssignedKanbanIssueTableCell from '@/components/assignedKanban/AssignedKanbanIssueTableCell';
import { AutoloadManager } from '@/autoload_manager';
import { getIssueFormPosition } from '@/utils';

const makeIssuesSelector = () => createSelector(
  (state) => state.issue,
  (state) => state.workflow.current,
  (state) => state.redmine.sort_column,
  (state, props) => props.userId,
  (state, props) => props.statusId,
  (issues, workflow, column, userId, statusId) => issues
    .findByWorkflow(workflow)
    .filter((item) => item.status.id === statusId)
    .filter((item) => item.assigned_to.id === userId)
    .sortByColumn(column)
    .toArray()
);

const makeMapStateToProps = () => {
  const issuesSelector = makeIssuesSelector();

  return (state, props) => ({
    creatable: state.redmine.issue_creatable,
    issues: issuesSelector(state, props),
    versionId: _.get(state.redmine.filtered_version, 'id'),
    startDate: _.get(state.redmine.filtered_version, 'start_date'),
  });
};

const mapDispatchToProps = (dispatch) => ({
  showIssueForm(query, position) {
    dispatch(requestIssueForm('new', { query, position }));
  },
});

const mergeProps = (stateProps, { showIssueForm }, ownProps) => ({
  ...ownProps,
  ...stateProps,
  onAddIssue: stateProps.creatable && (() => {
    const position = getIssueFormPosition();
    AutoloadManager.stop();
    showIssueForm(
      {
        assigned_to_id: ownProps.userId,
        fixed_version_id: stateProps.versionId,
        start_date: stateProps.startDate,
      },
      position
    );
  }),
});

export default connect(
  makeMapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AssignedKanbanIssueTableCell);
