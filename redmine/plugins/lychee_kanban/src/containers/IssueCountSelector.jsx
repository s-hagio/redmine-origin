import { createSelector } from 'reselect';

export const issueCountsSelector = () => createSelector(
  (issues) => issues,
  (issues, workflow) => workflow,
  (issues, workflow) => {
    const counts = {};
    const currentWorkflowTrackerIds = workflow.trackers.toArray().map((t) => t.id);
    workflow.issue_statuses.forEach((issue_status) => {
      counts[issue_status.id] = 0;
    });
    issues
      .filter((item) => currentWorkflowTrackerIds.includes(item.tracker.id))
      .forEach((i) => {
        counts[i.status.id] += 1;
      });
    return counts;
  }
);
