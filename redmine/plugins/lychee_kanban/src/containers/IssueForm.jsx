import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { createSelector } from 'reselect';
import * as _ from 'lodash';

import * as actions from '@/actions';
import * as forms from '@/constants/formTypes';
import IssueForm from '@/components/IssueForm';
import { ORIGIN_PROJECTS_PATH } from '@/constants/endpoints';
import { AutoloadManager } from '@/autoload_manager';
import { sagaMiddleware } from '@/sagas';
import * as sagas from '@/sagas/attachment';

const initialValuesSelector = createSelector(
  (state) => state.issueForm.issue,
  (issue) => issue.toJS()
);

const issueFormOptionsSelector = createSelector(
  (state) => state.issueForm,
  (issueForm) => {
    const form = issueForm.toJS();
    return {
      customFields: form.custom_fields,
      customFieldValues: form.custom_field_values,
      disabledCoreFields: form.disabled_core_fields,
      requiredAttributes: form.required_attribute_names,
      safeAttributes: form.safe_attribute_names,
      journals: form.journals,
      attachments: form.attachments,
    };
  }
);

const projectNameWithPadding = (project) => {
  if (project.level <= 0) {
    return project.name;
  }
  return `${''.padStart(project.level, '　')}>> ${project.name}`;
};

const fieldOptionsSelector = createSelector(
  [(state) => state.issueForm, (state) => state.redmine.page === 'backlog'],
  (issueForm, isBacklog) => {
    const toOption = ({ id, name }) => ({ value: id, text: name });
    const trackerFilter = ({ issue_status_count }) => isBacklog || issue_status_count > 0;
    const form = issueForm.toJS();
    return {
      projects: form.projects.map((project) => ({
        value: project.id,
        text: projectNameWithPadding(project),
      })),
      trackers: form.trackers.filter(trackerFilter).map(toOption),
      issue_statuses: form.issue_statuses.map(toOption),
      issue_priorities: form.issue_priorities.map(toOption),
      assignable_users: form.assignable_users.map(toOption),
      issue_categories: form.issue_categories.map(toOption),
      fixed_versions: form.versions.map(toOption),
    };
  }
);

const mapStateToProps = (state) => ({
  timeEntry: _.get(state, ['form', 'ISSUE_TIME_ENTRY_FORM', 'values']),
  projectId: state.issueForm.getIn(['issue', 'project_id']),
  id: state.issueForm.getIn(['issue', 'id']),
  total_estimated_hours: state.issueForm.getIn([
    'issue',
    'costs',
    'total_estimated_hours',
  ]),
  open: state.issueForm.isOpen,
  position: state.issueForm.position,
  initialValues: initialValuesSelector(state),
  fieldOptions: fieldOptionsSelector(state),
  lad: state.redmine.lad_included,
  workingHours: state.redmine.working_hours,
  lychee_remaining_estimate: state.redmine.lychee_remaining_estimate_included,
  backlog: state.redmine.backlog_point_enabled && state.redmine.page === 'backlog',
  editable: state.issueForm.permissions.includes('edit_issues'),
  dateFormat: state.redmine.date_format,
  maxFileSize: state.redmine.max_file_size,
  maxFileSizeHuman: state.redmine.max_file_size_human,
  commentsSorting: state.redmine.comments_sorting,
  ...issueFormOptionsSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  onClose() {
    AutoloadManager.restart();
    dispatch(actions.IssueForm.hideIssueForm());
  },
  saveIssue(issue, timeEntry, path) {
    dispatch(actions.Issue.saveIssue(issue, { time_entry: timeEntry, path }));
  },
  onDeleteFile: (id) => {
    sagaMiddleware
      .run(sagas.destroyAttachment, { payload: id });
  }
});

const mergeProps = (stateProps, dispatchProps, props) => {
  const { timeEntry, projectId, ...state } = stateProps;
  const { saveIssue, ...dispatch } = dispatchProps;
  return {
    onSubmit(values) {
      const path = `${ORIGIN_PROJECTS_PATH}/${projectId}/lychee_issue_board/issues`;
      saveIssue(values, timeEntry, path);
    },
    ...state,
    ...dispatch,
    ...props,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(
  reduxForm({
    form: forms.ISSUE_FORM,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true,
  })(IssueForm)
);
