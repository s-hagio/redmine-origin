import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import _ from 'lodash';

import SimpleIssueForms from '@/components/SimpleIssueForm';
import { createSimpleIssue } from '@/actions/issue';

const trackerSelector = createSelector(
  [(state) => state.workflow.current, (state) => state.redmine.project_id],
  (workflow, projectId) => workflow.trackers
    .toArray()
    .filter((tracker) => tracker.activated_project_ids.includes(projectId))
);

const mapStateToProps = (state) => ({
  trackers: trackerSelector(state),
  redmine: state.redmine,
});

const mapDispatchToProps = (dispatch) => ({
  createIssue: (params) => {
    dispatch(createSimpleIssue(params));
  },
});

const mergeProps = ({ redmine, trackers }, { createIssue }) => ({
  trackers,
  onSubmit: (params) => {
    const version = _.get(redmine, 'filtered_version') || {};

    createIssue({
      fixed_version_id: version.id,
      start_date: version.start_date,
      due_date: version.due_date,
      ...params,
    });
  },
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
  { pure: false },
)(SimpleIssueForms);
