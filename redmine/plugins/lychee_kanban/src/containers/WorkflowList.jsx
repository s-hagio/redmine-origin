import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import { Workflow } from '@/actions';
import WorkflowList from '@/components/WorkflowList';

const workflowArraySelector = createSelector(
  (state) => state.workflow,
  (workflow) => workflow.items.toJS().filter((item) => item.issue_statuses.length > 0)
);

const mapStateToProps = (state) => ({
  showWorkload: state.redmine.display_workload,
  workflows: workflowArraySelector(state),
});

const mapDispatchToProps = { handleClickWorkflow: Workflow.toggleWorkflow, };

export default connect(mapStateToProps, mapDispatchToProps)(WorkflowList);
