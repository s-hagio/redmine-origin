import { connect } from 'react-redux';
import { lifecycle } from 'recompose';
import { hot } from 'react-hot-loader/root';

import * as actions from '@/actions';
import KanbanSettingsApp from '@/components/KanbanSettings/KanbanSettingsApp';

export default connect()(
  lifecycle({
    componentWillMount() {
      this.props.dispatch(actions.initKanbanSettingsApp());
    },
  })(hot(KanbanSettingsApp))
);
