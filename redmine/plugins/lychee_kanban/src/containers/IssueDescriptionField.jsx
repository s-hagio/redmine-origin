import { connect } from 'react-redux';
import TextAreaField from '@/components/LycheeForm/fields/TextAreaField';

const mapStateToProps = (state) => ({ htmlText: state.issueForm.issue.get('description_html'), });

export default connect(mapStateToProps)(TextAreaField);
