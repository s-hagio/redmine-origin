import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { createSelector } from 'reselect';
import { List, Map } from 'immutable';
import moment from 'moment';

import * as actions from '@/actions/issue';
import IssueTimeEntry from '@/components/IssueTimeEntry';
import * as forms from '@/constants/formTypes';

const initialValuesSelector = createSelector(
  (state) => state.issueForm.getIn(['time_entry', 'time_entry']),
  (time_entry = Map()) => Map({
    activity_id: time_entry.get('activity_id'),
    user_id: time_entry.get('user_id'),
    issue_id: time_entry.get('issue_id'),
    spent_on: moment().format('YYYY-MM-DD'),
    custom_field_values: Map(
      time_entry
        .get('custom_fields', List())
        .map((field) => [field.get('id'), field.get('value')])
    ),
  }).toJS()
);

const activitiesSelector = createSelector(
  (state) => state.issueForm.get('time_entry'),
  (time_entry = Map()) => time_entry
    .get('activities', List())
    .map((activity) => ({
      value: activity.get('id'),
      text: activity.get('name'),
    }))
    .toJS()
);

const customFieldsSelector = createSelector(
  (state) => state.issueForm.get('time_entry'),
  (time_entry = Map()) => time_entry.get('custom_fields', List()).toJS()
);

const mapStateToProps = (state) => {
  const { issueForm } = state;
  return {
    issueId: issueForm.getIn(['issue', 'id']),
    visible: issueForm.permissions.includes('view_time_entries'),
    editable: issueForm.permissions.includes('log_time'),
    activities: activitiesSelector(state),
    customFields: customFieldsSelector(state),
    initialValues: initialValuesSelector(state),
    spentHours: issueForm.spent_hours,
    totalSpentHours: issueForm.total_spent_hours,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onSubmit(values) {
    dispatch(actions.createTimeEntry(values));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: forms.ISSUE_TIME_ENTRY_FORM,
    enableReinitialize: true,
  })(IssueTimeEntry)
);
