import { connect } from 'react-redux';
import LoadingPanel from '@/components/LoadingPanel';

const mapStateToProps = ({ loadingPanel }) => ({ isLoading: loadingPanel.loading, });

export default connect(mapStateToProps)(LoadingPanel);
