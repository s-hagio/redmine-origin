import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import QuickNewIssue from '@/components/rankedKanban/QuickNewIssue';
import { createSimpleIssue } from '@/actions/issue';

const trackerSelector = createSelector(
  (state) => state.workflow.current,
  (workflow) => workflow.trackers.toArray()
);

const mapStateToProps = (state) => ({ trackers: trackerSelector(state), });

const mapDispatchToProps = { onSubmit: createSimpleIssue, };

export default connect(mapStateToProps, mapDispatchToProps)(QuickNewIssue);
