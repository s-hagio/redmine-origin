import { connect } from 'react-redux';
import {
  createSelector,
  createSelectorCreator,
  defaultMemoize,
} from 'reselect';
import isEqual from 'lodash/isEqual';

// eslint-disable-next-line import/no-cycle
import IssueTableRow from '@/components/rankedKanban/IssueTableRow';

const createDeepEqualSelector = createSelectorCreator(defaultMemoize, isEqual);

const makeIssueListDeepSelector = () => createDeepEqualSelector(
  (issues) => issues,
  (issues) => issues
);

const makeIssueListSelector = () => createSelector(
  (state) => state.issue,
  (state) => state.workflow,
  (state) => state.redmine.sort_column,
  (state, props) => props.issue,
  (issues, workflow, column, parent) => issues
    .findByWorkflow(workflow.current)
    .filter((issue) => Number(issue.visual_parent_id) === Number(parent.id))
    .sortByColumn(column)
    .toArray()
);

const issueStatusSelector = createSelector(
  (state) => state.workflow,
  (workflow) => workflow.current.issue_statuses.toArray()
);

const makeMapStateToProps = () => {
  const issueListSelector = makeIssueListSelector();
  const issueListDeepSelector = makeIssueListDeepSelector();
  return (state, props) => ({
    issueStatuses: issueStatusSelector(state),
    issues: issueListDeepSelector(issueListSelector(state, props)),
    draggable: state.redmine.sort_column === 'sort_by_backlog',
  });
};

export default connect(makeMapStateToProps)(IssueTableRow);
