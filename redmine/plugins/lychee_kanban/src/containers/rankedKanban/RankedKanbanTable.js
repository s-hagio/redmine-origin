import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import * as _ from 'lodash';

import { sortIssues } from '@/actions/issue';
import RankedKanbanTable from '@/components/rankedKanban/RankedKanbanTable';

const issueArraySelector = createSelector(
  (state) => state.issue,
  (state) => state.workflow.current,
  (state) => state.redmine.sort_column,
  (issues, workflow, column) =>
  // eslint-disable-next-line implicit-arrow-linebreak
    issues
      .visualRoot
      .findByWorkflow(workflow)
      .sortByColumn(column)
      .toArray()
);

const mapStateToProps = (state) => ({ issues: issueArraySelector(state), });

const mapDispatchToProps = (dispatch) => ({
  onSort(issue, issues) {
    const sortIds = issues.map((item) => item.id);
    const index = sortIds.indexOf(issue.id);
    const beforeIssueId = sortIds[index + 1];
    const sortParams = {
      before_issue_id: beforeIssueId,
      issue_index: _.isNil(beforeIssueId) ? -1 : null,
    };
    dispatch(
      sortIssues({
        payload: {
          id: issue.id,
          ...sortParams,
        },
      })
    );
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(RankedKanbanTable);
