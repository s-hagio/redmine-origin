import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useDrop } from 'react-dnd';
import * as _ from 'lodash';

import * as ItemTypes from '@/constants/itemTypes';
import { updateIssue } from '@/actions/issue';

const IssueTableColumnContainer = (props) => {
  const { status, issueId, children, className, style, onDrop } = props;

  const [{ canDrop, isOver }, drop] = useDrop({
    accept: ItemTypes.ISSUE,
    drop: onDrop,
    canDrop: (sourceProps) => {
      const {
        id: dragedIssueId,
        statudIs: dragedIssueStatusId,
        allowedIssueStatuses,
      } = sourceProps;
      const rejected = _.reject(allowedIssueStatuses, { id: dragedIssueStatusId });
      const allowed = !!_.find(rejected, { id: status?.id });
      return Number(dragedIssueId) === Number(issueId) && allowed;
    },
    collect: (monitor) => ({
      canDrop: monitor.canDrop(),
      isOver: monitor.isOver(),
    }),
  });

  const backgroundColor = React.useMemo(() => {
    if (canDrop) {
      if (isOver) {
        return '#fafa4c';
      }
      return '#ffc';
    }
    return null;
  }, [canDrop, isOver]);

  return (
    <div className={className} style={{ ...style, backgroundColor }} ref={drop}>
      {children}
    </div>
  );
};

IssueTableColumnContainer.propTypes = {
  status: PropTypes.object.isRequired,
  issueId: PropTypes.number.isRequired,
  onDrop: PropTypes.func.isRequired,
  style: PropTypes.object,
  className: PropTypes.string,
  children: PropTypes.any,
};

IssueTableColumnContainer.defaultProps = {
  style: null,
  className: '',
  children: null,
};

const mapDispatchToProps = (dispatch, props) => ({
  onDrop: (sourceProps) => {
    const { status } = props;
    const { id: sourceIssueId, statusId: sourceIssueStatusId } = sourceProps;
    if (Number(status.id) !== Number(sourceIssueStatusId)) {
      dispatch(updateIssue({ id: sourceIssueId, status_id: status.id }));
    }
  },
});

export default connect(
  null,
  mapDispatchToProps
)(IssueTableColumnContainer);
