import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import IssueStatusList from '@/components/rankedKanban/IssueStatusList';

import { issueCountsSelector } from '@/containers/IssueCountSelector';

const statusArraySelector = createSelector(
  (state) => state.workflow.current.issue_statuses,
  (statuses) => statuses.toJS()
);

const mapStateToProps = (state) => {
  const issueCounts = issueCountsSelector();

  return {
    issue_statuses: statusArraySelector(state),
    issue_count: issueCounts(state.issue.items, state.workflow.current),
  };
};

export default connect(mapStateToProps)(IssueStatusList);
