export const ja = {
  AssignedKanbanTableHeader: { assignee_column: '担当者', },
  AutoloadSetting: {
    autoload: '自動更新',
    seconds: '秒',
  },
  Button: {
    cancel: 'キャンセル',
    save: '保存',
    create: '作成',
    no: 'いいえ',
    none: 'なし',
    yes: 'はい',
    readmore: '読む',
    close: '閉じる',
    blank_option: '選んでください',
  },
  ClosedIssueList: { title: '終了済みチケット', },
  Issue: {
    length_of_stay: '滞留日数: {{value}}日',
    length_of_stay_more: '滞留日数: 10日以上',
  },
  IssueForm: {
    project: 'プロジェクト',
    description: '説明',
    status: 'ステータス',
    priority: '優先度',
    assigned_to: '担当者',
    category: 'カテゴリ',
    target_version: '対象バージョン',
    start_date: '開始日',
    due_date: '期日',
    actual_start_date: '実開始日',
    actual_due_date: '実終了日',
    estimated_hours: '予定工数',
    remaining_estimate: '残工数',
    points: 'ポイント',
    done_ratio: '進捗率',
    parent: '親チケット',
    blocking: 'ブロッキング',
    hours: '時間',
    days: '日',
    days_switch_button: '工数を日数で表示する',
    render_estimated_hours: '{{value}}時間',
    render_estimated_days: '{{value}}日',
    render_total_estimated_hours: '合計: {{value}}時間',
    render_total_estimated_days: '合計: {{value}}日',
    notes: 'コメント',
    label_journal: 'コメント（直近3件）',
    updated_time_by: '{{user}}さんが {{age}}前に更新',
    is_private: 'プライベート',
    attachments: 'ファイル',
    download_all_attachments: '一括ダウンロード',
    file_description_placeholder: '説明(任意)',
    max_file_size: '(サイズの上限: {{value}})',
    confirm_delete: 'よろしいですか？',
  },
  IssueTimeEntry: {
    time_entry_header: '作業時間の記録',
    n_hours: '{{hours}}時間',
    total: '合計',
    hours: '時間',
  },
  LoadingPanel: { loading: '読み込み中です...', },
  NewIssue: { new_issue: '新しいチケットを追加する', },
  NewVersion: { new_version: '新しいスプリントを追加する', },
  Notification: {
    overlimit:
      'カンバンは、最大表示件数({{limit}})を超えたため切り捨てられました。',
    createdIssue: 'チケット #{{id}} が作成されました',
    failedToRearrangeIssues: 'チケットの並び替えに失敗しました',
    failedToUpdateIssue: 'チケット #{{issueId}} の更新に失敗しました',
    setActualStartDateByLad: '実開始日がセットされました',
    setActualDueDateByLad: '実終了日がセットされました',
  },
  Sharing: {
    none: '共有しない',
    subprojects: 'サブプロジェクト単位',
    project_hierarchy: 'プロジェクト階層単位',
    project_tree: 'プロジェクトツリー単位',
    all_projects: 'すべてのプロジェクト',
  },
  SimpleIssueForm: {
    label: 'チケット追加',
    tracker: 'トラッカー',
  },
  UnassignedIssueList: { unassigned: '未アサイン', },
  UnfixedIssueList: { backlog: 'バックログ', },
  Velocity: {
    title: 'ベロシティ',
    velocities: '直近3スプリントのベロシティ',
    average: '１稼働日あたりの平均ベロシティ',
    help: `平均ベロシティは直近3スプリントから自動算出されます。<br /><br />

      値を変更すると一時的に予想消化に反映されます。
      変更した値は保存されませんので様々な値でシミュレートしてみてください。`,
  },
  Version: {
    finish_sprint: 'このスプリントを終了する',
    reopen_sprint: 'このスプリントを再開する',
    move_opened_issues: '未完了タスクを別のスプリントに移動する',
    details: '詳細',
    delete: '削除',
    estimated_velocity: '予想消化',
    count: '件',
  },
  VersionForm: {
    name: '名称',
    description: '説明',
    status: 'ステータス',
    wiki_page: 'Wikiページ',
    start_date: '開始日',
    due_date: '期日',
    sharing: '共有',
  },
  VersionStatus: {
    open: '進行中',
    locked: 'ロック中',
    closed: '終了',
  },
  Workload: {
    estimated: '予',
    actual: '実',
    remaining: '残',
    open: '未完',
    n_hours: '{{hours}}H',
  },
};
