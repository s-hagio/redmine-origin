import { IssueForm } from '@/models/issueForm';
import * as actions from '@/actions';
import reducer from './issueForm';

describe('IssueFormReducer', () => {
  describe('INITIALIZE_ISSUE_FORM_VALUES', () => {
    it('update attributes except issue', () => {
      const state = reducer(new IssueForm(), {
        type: actions.IssueForm.initializeIssueFormValues,
        payload: {
          issue: {
            id: 99,
            subject: 'dummy',
            spent_hours: 1,
            total_spent_hours: 10,
          },
        },
      }).toJS();
      expect(state.issue.id).toBe(99);
      expect(state).toHaveProperty('spent_hours', 1);
      expect(state).toHaveProperty('total_spent_hours', 10);
    });
  });

  describe('SHOW_ISSUE_FORM', () => {
    it('set position and "open" to true', () => {
      const result = reducer(new IssueForm(), {
        type: actions.IssueForm.showIssueForm,
        meta: { position: { x: 10, y: 90 }, },
      });
      expect(result.position).toEqual({ x: 10, y: 90 });
      expect(result.isOpen).toBeTruthy();
    });
  });

  describe('HIDE_ISSUE_FORM', () => {
    it('set "open" to false', () => {
      const result = reducer(new IssueForm(), { type: actions.IssueForm.hideIssueForm, });
      expect(result.isOpen).toBeFalsy();
    });
  });
});
