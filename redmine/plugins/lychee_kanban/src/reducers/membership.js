import { handleActions } from 'redux-actions';
import { receiveMemberships } from '@/actions/membership';
import { receiveTimeEntry } from '@/actions/issue';
import { MembershipList } from '@/models/membership';

const reducer = handleActions(
  {
    [receiveMemberships]: (state, action) => state.add(action.payload),
    [receiveTimeEntry]: (state, action) => {
      const { hours, user } = action.payload;
      const member = state.find(user.id);
      if (member) {
        return state.add(
          member.set('spent', Number(member.spent) + Number(hours))
        );
      }
      return state;
    },
  },
  new MembershipList()
);

export default reducer;
