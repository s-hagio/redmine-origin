const content = document.getElementById('target-content');
const data = JSON.parse(content.getAttribute('data-redmine-state'));

const initialState = { ...data };

const reducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default reducer;
