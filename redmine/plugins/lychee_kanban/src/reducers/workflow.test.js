import * as _ from 'lodash';

import { WorkflowList } from '@/models/workflow';
import * as actions from '@/actions/workflow';
import reducer from './workflow';

describe('WorkflowReducer', () => {
  const workflows = _.range(3).map((n) => ({
    trackers: [{ id: n }],
    issue_statuses: [{ id: n }],
  }));

  it('RECEIVE_WORKFLOWS', () => {
    const result = reducer(
      new WorkflowList(),
      actions.receiveWorkflows(workflows)
    );
    expect(result.items.size).toEqual(3);
  });

  it('TOGGLE_WORKFLOW', () => {
    const result = reducer(new WorkflowList(), actions.toggleWorkflow(1));
    expect(result.index).toEqual(1);
  });
});
