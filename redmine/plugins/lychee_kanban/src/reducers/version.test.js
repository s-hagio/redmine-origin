import * as _ from 'lodash';

import * as actions from '@/actions/version';
import { VersionList } from '@/models/version';
import reducer from './version';

describe('VersionsReducer', () => {
  const versions = _.range(1, 4).map((n) => ({
    id: n,
    name: `version_${n}`,
    status: 'open',
  }));

  it('RECEIVE_VERSIONS', () => {
    const result = reducer(
      new VersionList(),
      actions.receiveVersions({ versions })
    );
    expect(result.size).toEqual(3);
  });

  it('RECEIVE_VERSION', () => {
    const result = reducer(
      new VersionList({ versions }),
      actions.receiveVersion({
        id: 1,
        name: 'changed',
        status: {},
        sharing: {},
      })
    );
    expect(result.find(1).name).toEqual('changed');
    expect(result.size).toEqual(3);
    expect(
      reducer(
        result,
        actions.receiveVersion({
          id: 99,
          name: 'changed',
          status: {},
          sharing: {},
        })
      ).size
    ).toEqual(4);
  });

  it('REMOVE_VERSION', () => {
    const result = reducer(new VersionList({ versions }), actions.removeVersion(1));
    expect(result.size).toEqual(2);
  });
});
