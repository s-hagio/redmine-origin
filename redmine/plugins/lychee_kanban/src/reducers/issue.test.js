import * as _ from 'lodash';

import { IssueList } from '@/models/issue';
import * as actions from '@/actions/issue';
import reducer from './issue';

describe('IssueReducer', () => {
  const issues = _.range(3).map((n) => ({ id: n, subject: `issue${n}` }));

  it('RECEIVE_ISSUES', () => {
    const result = reducer(new IssueList(), actions.receiveIssues(issues));
    expect(result.items.size).toEqual(3);
  });

  it.only('RECEIVE_ISSUE', () => {
    let issue = {
      id: 1,
      subject: 'hogehoge',
      assigned_to: { lastname: 'hogehoge' },
      ancestor_ids: [],
    };
    let result = reducer(new IssueList(issues), actions.receiveIssue(issue));
    expect(result.items.get(2).toObject()).toHaveProperty(
      'subject',
      'hogehoge'
    );
    expect(result.items.get(2).toObject()).toHaveProperty(
      'assigned_to',
      issue.assigned_to
    );
    expect(result.items.size).toEqual(3);

    issue = { id: 4, subject: 'new issue', ancestor_ids: [] };
    result = reducer(new IssueList(issues), actions.receiveIssue(issue));
    expect(result.items.size).toEqual(4);
    expect(result.items.last().toObject()).toHaveProperty(
      'subject',
      'new issue'
    );
  });
});
