import { handleActions } from 'redux-actions';
import * as actions from '@/actions/version';
import { VersionList } from '@/models/version';

const reducer = handleActions(
  {
    [actions.receiveVersions]: (_state, action) => new VersionList(action.payload),
    [actions.receiveVersion]: (state, action) => {
      if (state.find(action.payload.id)) {
        return state.update(action.payload.id, action.payload);
      }
      return state.add(action.payload);
    },
    [actions.removeVersion]: (state, action) => {
      const index = state.findIndex(action.payload);
      if (index >= 0) {
        return state.splice(index, 1);
      }
      return state;
    },
  },
  new VersionList()
);

export default reducer;
