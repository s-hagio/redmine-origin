import { handleActions } from 'redux-actions';
import * as actions from '@/actions/version';
import { VersionForm } from '@/models/versionForm';

const reducer = handleActions(
  {
    [actions.receiveVersionForm]: (state, action) => new VersionForm(action.payload),
    [actions.showVersionForm]: (state, action) => state.setPosition(action.meta.position).open,
    [actions.hideVersionForm]: (state) => state.close,
  },
  new VersionForm()
);

export default reducer;
