import { handleActions } from 'redux-actions';
import { QueryList } from '@/models/query';
import * as actions from '@/actions/query';

const content = document.getElementById('target-content');
const queries = JSON.parse(content.getAttribute('data-queries'));

const reducer = handleActions(
  { [actions.add]: (state, action) => state.add(action.payload), },
  new QueryList(queries)
);

export default reducer;
