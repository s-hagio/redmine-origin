import { handleActions } from 'redux-actions';
import * as actions from '@/actions';
import { IssueForm } from '@/models/issueForm';

const reducer = handleActions(
  {
    [actions.IssueForm.receiveIssueForm]: (state, action) => state.merge(action.payload),
    [actions.IssueForm.initializeIssueFormValues]: (state, action) => {
      const { versions, issue } = action.payload;
      const { spent_hours, total_spent_hours } = issue;
      return state.merge({
        issue: new IssueForm({ issue, versions }).issue,
        spent_hours,
        total_spent_hours,
      });
    },
    [actions.IssueForm.showIssueForm]: (state, { meta }) => state.setPosition(meta.position).open,
    [actions.IssueForm.hideIssueForm]: () => new IssueForm(),
    [actions.Issue.receiveTimeEntry]: (state, action) => {
      const { hours } = action.payload;
      const params = {
        spent_hours: Number(state.spent_hours) + Number(hours),
        total_spent_hours: Number(state.total_spent_hours) + Number(hours),
      };
      return state.merge(params);
    },
  },
  new IssueForm()
);

export default reducer;
