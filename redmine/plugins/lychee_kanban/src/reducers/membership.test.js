import * as _ from 'lodash';

import { MembershipList } from '@/models/membership';
import * as actions from '@/actions/membership';
import reducer from './membership';

describe('MembershipReducer', () => {
  const memberships = _.range(3).map((n) => ({ id: n, }));

  it('RECEIVE_MEMBERSHIPS', () => {
    const result = reducer(
      new MembershipList(),
      actions.receiveMemberships(memberships)
    );
    expect(result.items.size).toEqual(3);
  });
});
