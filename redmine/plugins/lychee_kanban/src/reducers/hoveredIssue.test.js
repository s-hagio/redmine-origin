import * as actions from '@/actions/hoveredIssue';
import reducer from './hoveredIssue';

describe('HoveredIssueReducer', () => {
  it('UPDATE_HOVERED_ISSUE', () => {
    const result = reducer(null, actions.setHoveredIssue(1));
    expect(result).toEqual(1);
  });

  it('DELETE_HOVERED_ISSUE', () => {
    const result = reducer(1, actions.deleteHoveredIssue());
    expect(result).toEqual(null);
  });
});
