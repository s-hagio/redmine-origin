import { handleActions } from 'redux-actions';
import * as actions from '@/actions/issue';
import { IssueList } from '@/models/issue';

const reducer = handleActions(
  {
    [actions.receiveIssues]: (state, action) => state.add(action.payload).merge(action.meta),
    [actions.refreshIssues]: (state, action) => state.refresh(action.payload).merge(action.meta),
    [actions.receiveIssue]: (state, action) => state.add(action.payload),
    [actions.replaceIssue]: (state, action) => state.update(action.payload.id, action.payload),
    [actions.receiveIssueSorted]: (state, action) => state.sortIssue(action.payload, action.meta),
    [actions.receiveTimeEntry]: (state, action) => state.addTimeEntry(action.payload),
  },
  new IssueList()
);

export default reducer;
