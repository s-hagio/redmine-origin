// TODO: models/workflowにmodels/trackerの機能を統合次第、削除
import { handleActions } from 'redux-actions';

import * as actions from '@/actions/workflow';
import { TrackerList } from '@/models/tracker';

const reducer = handleActions(
  { [actions.receiveWorkflows]: (state, action) => state.importFromWorkflows(action.payload), },
  new TrackerList()
);

export default reducer;
