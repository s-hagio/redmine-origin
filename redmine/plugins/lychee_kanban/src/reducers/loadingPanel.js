import { handleActions } from 'redux-actions';
import * as actions from '@/actions/loadingPanel';

const reducer = handleActions(
  {
    [actions.show]: () => ({ loading: true }),
    [actions.hide]: () => ({ loading: false }),
  },
  { loading: false }
);

export default reducer;
