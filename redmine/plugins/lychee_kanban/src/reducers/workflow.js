import { handleActions } from 'redux-actions';
import * as actions from '@/actions/workflow';

import { WorkflowList } from '@/models/workflow';

const reducer = handleActions(
  {
    [actions.receiveWorkflows]: (state, action) => new WorkflowList(action.payload),
    [actions.toggleWorkflow]: (state, action) => state.toggleWorkflow(action.payload),
  },
  new WorkflowList()
);

export default reducer;
