import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';

import issue from './issue';
import issueForm from './issueForm';
import loadingPanel from './loadingPanel';
import membership from './membership';
import query from './query';
import redmine from './redmine';
import trackers from './trackers';
import versions from './version';
import versionForm from './versionForm';
import workflow from './workflow';
import hoveredIssue from './hoveredIssue';

export default combineReducers({
  issue,
  issueForm,
  loadingPanel,
  membership,
  query,
  redmine,
  form: reduxForm,
  trackers,
  versions,
  versionForm,
  workflow,
  hoveredIssue,
});
