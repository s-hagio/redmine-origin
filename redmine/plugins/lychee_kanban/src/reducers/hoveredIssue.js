import { handleActions } from 'redux-actions';
import * as actions from '@/actions/hoveredIssue';

const reducer = handleActions(
  {
    [actions.setHoveredIssue]: (state, action) => action.payload,
    [actions.deleteHoveredIssue]: () => null,
  },
  null
);

export default reducer;
