import * as actions from '@/actions/loadingPanel';
import reducer from './loadingPanel';

describe('LoadingPanelReducer', () => {
  it('SHOW_LOADING_PANEL', () => {
    const result = reducer({}, actions.show());
    expect(result.loading).toBe(true);
  });

  it('HIDE_LOADING_PANEL', () => {
    const result = reducer({}, actions.hide());
    expect(result.loading).toBe(false);
  });
});
