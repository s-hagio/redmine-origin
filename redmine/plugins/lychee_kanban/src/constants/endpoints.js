import { REDMINE_VERSION } from '@/constants/redmine';

const CURRENT_PATH = location.pathname; // eslint-disable-line no-restricted-globals
const APP_NAMES = 'kanban|kanban/assigned|backlog|kanban/ranked';
const REG_PATH = new RegExp(`(${APP_NAMES})$`);
const REG_PROJECT_PATH = new RegExp(`(.*)/projects/(.*)/(${APP_NAMES})$`);

export const ATTACHMENTS_PATH = CURRENT_PATH.replace(
  REG_PATH,
  'lychee_issue_board/attachments'
);
export const ISSUES_PATH = CURRENT_PATH.replace(
  REG_PATH,
  'lychee_issue_board/issues'
);
export const MEMBERSHIPS_PATH = CURRENT_PATH.replace(
  REG_PATH,
  'lychee_issue_board/memberships'
);
export const UPLOAD_PATH = CURRENT_PATH.replace(
  REG_PATH,
  'lychee_issue_board/uploads'
);
export const VERSIONS_PATH = CURRENT_PATH.replace(
  REG_PATH,
  'lychee_issue_board/versions'
);
export const WORKFLOWS_PATH = CURRENT_PATH.replace(
  REG_PATH,
  'lychee_issue_board/workflows'
);

export const ADD_TIME_ENTRY_PATH = CURRENT_PATH.replace(
  REG_PATH,
  'lychee_issue_board/timelog.json'
);
// issue_formに表示するissueの情報をコンポーネントの外から取得できないため、関数形式で定義
export const timeEntriesPath = (id) => {
  if (REDMINE_VERSION >= 3.4) {
    return CURRENT_PATH.replace(
      /(backlog|kanban\/assigned|kanban\/ranked|kanban)$/,
      `time_entries?issue_id=~${id}`
    );
  }
  return CURRENT_PATH.replace(
    /projects\/.+\/(backlog|kanban\/assigned|kanban\/ranked|kanban)/,
    `issues/${id}/time_entries`
  );
};
// プロジェクト毎にPathが異なるため
export const projectWikiPath = (id) => CURRENT_PATH.replace(REG_PROJECT_PATH, `$1/projects/${id}/wiki`);

export const BACKLOGS_SPILLOVER_PATH = CURRENT_PATH.replace(
  REG_PROJECT_PATH,
  '$1/projects/$2/backlogs/spillover'
);

export const ORIGIN_ATTACHMENTS_PATH = CURRENT_PATH.replace(
  REG_PROJECT_PATH,
  '$1/attachments'
);
export const ORIGIN_ISSUES_PATH = CURRENT_PATH.replace(
  REG_PROJECT_PATH,
  '$1/issues'
);
export const ORIGIN_SPRINTS_PATH = CURRENT_PATH.replace(
  REG_PROJECT_PATH,
  '$1/versions'
);
export const ORIGIN_VERSIONS_PATH = CURRENT_PATH.replace(
  REG_PROJECT_PATH,
  '$1/versions'
);
export const ORIGIN_PROJECTS_PATH = CURRENT_PATH.replace(
  REG_PROJECT_PATH,
  '$1/projects'
);
export const ORIGIN_WATCHERS_PATH = CURRENT_PATH.replace(
  REG_PROJECT_PATH,
  '$1/watchers'
);
export const AUTOCOMPLETE_FOR_MENTION_PATH = `${ORIGIN_WATCHERS_PATH}/autocomplete_for_mention`;
