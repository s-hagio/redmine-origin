import fetch from 'isomorphic-fetch';
import { ADD_TIME_ENTRY_PATH } from '@/constants/endpoints';
// HACK: lint違反を修正
// eslint-disable-next-line import/no-cycle
import { makeHeaders, handleError } from './index';

export const create = (time_entry) => fetch(ADD_TIME_ENTRY_PATH, {
  method: 'post',
  credentials: 'include',
  headers: makeHeaders(),
  body: JSON.stringify({ time_entry }),
}).then(handleError);
