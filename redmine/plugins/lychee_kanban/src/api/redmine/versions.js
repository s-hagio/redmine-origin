import fetch from 'isomorphic-fetch';
import QueryString from 'query-string';
import * as _ from 'lodash';
import { VERSIONS_PATH } from '@/constants/endpoints';
// HACK: lint違反を修正
// eslint-disable-next-line import/no-cycle
import { makeHeaders, handleError } from './index';

const SAFE_ATTRIBUTE_NAMES = [
  'custom_field_values',
  'description',
  'effective_date',
  'name',
  'sharing',
  'start_date',
  'status',
  'wiki_page_title',
];

const toSafeParameters = (params) => _.pick(params, SAFE_ATTRIBUTE_NAMES);

export const index = (query) => {
  if (query) {
    return fetch(`${VERSIONS_PATH}?${query}`, {
      method: 'get',
      credentials: 'include',
      headers: makeHeaders(),
    }).then(handleError);
  }
  return fetch(VERSIONS_PATH, {
    method: 'get',
    credentials: 'include',
    headers: makeHeaders(),
  }).then(handleError);
};

export const show = (id, query) => fetch(`${VERSIONS_PATH}/${id}?${QueryString.stringify(query)}`, {
  method: 'get',
  credentials: 'include',
  headers: makeHeaders(),
}).then(handleError);

export const create = (version, query = {}) => fetch(`${VERSIONS_PATH}?${QueryString.stringify(query)}`, {
  method: 'post',
  credentials: 'include',
  headers: makeHeaders(),
  body: JSON.stringify({ version: toSafeParameters(version) }),
}).then(handleError);

export const update = (id, version, query = {}) => fetch(`${VERSIONS_PATH}/${id}.json?${QueryString.stringify(query)}`, {
  method: 'put',
  credentials: 'include',
  headers: makeHeaders(),
  body: JSON.stringify({ version: toSafeParameters(version) }),
}).then(handleError);

export const destroy = (id) => fetch(`${VERSIONS_PATH}/${id}`, {
  method: 'delete',
  credentials: 'include',
  headers: makeHeaders(),
}).then(handleError);
