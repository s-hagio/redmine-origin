import fetch from 'isomorphic-fetch';
import { WORKFLOWS_PATH } from '@/constants/endpoints';
// HACK: lint違反を修正
// eslint-disable-next-line import/no-cycle
import { makeHeaders, handleError } from './index';

export const index = () => fetch(WORKFLOWS_PATH, {
  method: 'get',
  credentials: 'include',
  headers: makeHeaders(),
}).then(handleError);
