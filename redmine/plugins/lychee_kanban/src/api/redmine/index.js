import QueryString from 'query-string';

// HACK: lint違反の修正
/* eslint-disable import/no-cycle */
import * as _Attachments from './attachments';
import * as _Issues from './issues';
import * as _Memberships from './memberships';
import * as _Versions from './versions';
import * as _Workflows from './workflows';
import * as _TimeEntry from './time_entry';
/* eslint-enable */

const DOM_CSRF_TOKEN = document.getElementsByName('csrf-token')[0];
const CSRF_TOKEN = DOM_CSRF_TOKEN && DOM_CSRF_TOKEN.getAttribute('content');

export const createQuery = () => QueryString.stringify({
  'set_filter': 1,
  'f[]': ['parent_id', 'subproject_id'],
  'op[parent_id]': '!*',
  'op[subproject_id]': '!*',
});

export const makeHeaders = (options = {}) => ({
  'Accept': 'application/json',
  'Content-Type': 'application/json',
  'X-CSRF-Token': CSRF_TOKEN,
  ...options,
});

export const handleError = (response) => {
  if (!response.ok) {
    const error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
  if (response.status === 204) {
    return response;
  }
  return response.json();
};

export const Attachments = _Attachments;
export const Issues = _Issues;
export const Memberships = _Memberships;
export const Versions = _Versions;
export const Workflows = _Workflows;
export const TimeEntry = _TimeEntry;
