import fetch from 'isomorphic-fetch';
import * as _ from 'lodash';
import QueryString from 'query-string';
import { ISSUES_PATH } from '@/constants/endpoints';
// HACK: lint違反を修正
// eslint-disable-next-line import/no-cycle
import { makeHeaders, handleError } from './index';

const SAFE_QUERY_ATTRIBUTE_NAMES = [
  'actual_due_date',
  'actual_start_date',
  'after_issue_id',
  'assigned_to_id',
  'before_issue_id',
  'category_id',
  'custom_field_values',
  'description',
  'done_ratio',
  'due_date',
  'estimated_hours',
  'remaining_estimate',
  'fixed_version_id',
  'id',
  'issue_index',
  'parent_issue_id',
  'priority_id',
  'project_id',
  'root_id',
  'start_date',
  'status_id',
  'subject',
  'tracker_id',
  'lychee_issue_board_issue_option_attributes',
  'notes',
  'attachments',
];

const toSafeParameters = (params) => {
  const attributes = {};
  _.each(params, (value, key) => {
    if (SAFE_QUERY_ATTRIBUTE_NAMES.includes(key)) {
      attributes[key] = value;
    }
  });
  return attributes;
};

export const index = (props = {}) => {
  const { query } = props;
  const filter = `set_filter=1&${query}`;
  return fetch(`${ISSUES_PATH}?${filter}`, {
    method: 'get',
    credentials: 'include',
    headers: makeHeaders(),
  }).then(handleError);
};

export const show = (id, params, options = {}) => {
  const { path = ISSUES_PATH } = options;
  const query = QueryString.stringify(params);
  return fetch(`${path}/${id}?${query}`, {
    method: 'get',
    credentials: 'include',
    headers: makeHeaders(),
  }).then(handleError);
};

export const create = (issue, options) => fetch(ISSUES_PATH, {
  method: 'post',
  credentials: 'include',
  headers: makeHeaders(),
  body: JSON.stringify({
    issue: toSafeParameters(issue),
    ...options,
  }),
}).then(handleError);

export const update = (id, params, options) => {
  const { path = ISSUES_PATH, attachments } = options;
  return fetch(`${path}/${id}`, {
    method: 'put',
    credentials: 'include',
    headers: makeHeaders(),
    body: JSON.stringify({
      issue: toSafeParameters(params),
      attachments,
      ...options,
    }),
  }).then(handleError);
};
