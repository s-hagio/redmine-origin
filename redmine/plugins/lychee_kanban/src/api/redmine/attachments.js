import fetch from 'isomorphic-fetch';
import { ATTACHMENTS_PATH, UPLOAD_PATH } from '@/constants/endpoints';
// HACK: line違反の修正
// eslint-disable-next-line import/no-cycle
import { makeHeaders, handleError } from './index';

export const create = (attachment_id, file) => {
  const filename = encodeURIComponent(file.name);
  const content_type = encodeURIComponent(file.type);
  // NOTE: attachment_idは利用されていないが、本家redmineのリクエストには含まれているため一応記述
  return fetch(
    `${UPLOAD_PATH}?attachment_id=${attachment_id}&filename=${filename}&content_type=${content_type}`,
    {
      method: 'post',
      credentials: 'include',
      headers: makeHeaders({ 'Content-Type': 'application/octet-stream', }),
      body: file,
    }
  ).then(handleError);
};

export const destroy = (id) => fetch(`${ATTACHMENTS_PATH}/${id}`, {
  method: 'delete',
  credentials: 'include',
  headers: makeHeaders(),
});
