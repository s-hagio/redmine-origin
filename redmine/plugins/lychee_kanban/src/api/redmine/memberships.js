import fetch from 'isomorphic-fetch';
import { MEMBERSHIPS_PATH } from '@/constants/endpoints';
// HACK: lint違反を修正
// eslint-disable-next-line import/no-cycle
import { makeHeaders, handleError } from './index';

export const index = ({ query }) => fetch(`${MEMBERSHIPS_PATH}?set_filter=1&${query}`, {
  method: 'get',
  credentials: 'include',
  headers: makeHeaders(),
}).then(handleError);
