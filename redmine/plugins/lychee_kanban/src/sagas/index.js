import createSagaMiddleware from 'redux-saga';
import { fork, all } from 'redux-saga/effects';

import initializeHandlers from './initialize';
import issueHandlers from './issue';
import issueFormHandlers from './issueForm';
import membershipHandlers from './membership';
import notificationHandlers from './notification';
import versionHandlers from './version';
import backlog from './backlog';

export const sagaMiddleware = createSagaMiddleware();

export default function* root() {
  yield all([
    ...Object.values(initializeHandlers).map((handler) => fork(handler)),
    ...Object.values(issueHandlers).map((handler) => fork(handler)),
    ...Object.values(issueFormHandlers).map((handler) => fork(handler)),
    ...Object.values(membershipHandlers).map((handler) => fork(handler)),
    ...Object.values(notificationHandlers).map((handler) => fork(handler)),
    ...Object.values(versionHandlers).map((handler) => fork(handler)),
    ...Object.values(backlog).map((handler) => fork(handler)),
  ]);
}
