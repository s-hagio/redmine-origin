import { call, put, takeLatest } from 'redux-saga/effects';
import * as actions from '@/actions';
import { BACKLOGS_SPILLOVER_PATH } from '@/constants/endpoints';
import { makeHeaders } from '@/api/redmine';

const requestSpillover = async (fromVersionId, toVersionId) => fetch(`${BACKLOGS_SPILLOVER_PATH}.json`, {
  method: 'put',
  credentials: 'include',
  headers: makeHeaders(),
  body: JSON.stringify({
    from_version_id: fromVersionId,
    to_version_id: toVersionId,
  }),
});

function* spillover(action) {
  const { fromVersionId, toVersionId } = action.payload;
  const response = yield call(requestSpillover, fromVersionId, toVersionId);
  if (response.ok) {
    yield put(actions.initBacklogApp());
  }
}

function* requestBacklogSpillover() {
  yield takeLatest(actions.Backlog.spillover, spillover);
}

const handlers = [requestBacklogSpillover];
export default handlers;
