import { call, put } from 'redux-saga/effects';
import * as Redmine from '@/api/redmine';
import * as actions from '@/actions';

import { mask } from './common';

export function* createAttachment(action) {
  try {
    const response = yield call(
      mask(Redmine.Attachments.create),
      null,
      action.payload
    );
    yield put(actions.Attachment.receiveAttachment(response.upload));
    return response;
  } catch (error) {
    yield put(actions.Attachment.createAttachment(error));
    throw error;
  }
}

export function* destroyAttachment(action) {
  try {
    yield call(mask(Redmine.Attachments.destroy), action.payload);
    yield put(actions.Attachment.removeAttachment(action.payload));
  } catch (error) {
    yield put(actions.Attachment.destroyAttachment(error));
    throw error;
  }
}
