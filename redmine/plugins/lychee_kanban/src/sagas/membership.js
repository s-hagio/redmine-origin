import { call, put, select, takeLatest } from 'redux-saga/effects';

import * as Redmine from '@/api/redmine';
import * as actions from '@/actions';

export function* getMemberships() {
  try {
    const { query, redmine } = yield select();
    const queryString = query.toURL({ limit: redmine.issues_limit });
    const { memberships } = yield call(Redmine.Memberships.index, { query: queryString, });
    yield put(actions.Membership.receiveMemberships(memberships));
  } catch (error) {
    yield put(actions.Membership.receiveMemberships(error));
  }
}

export function* requestMemberships() {
  yield takeLatest(
    [actions.Membership.requestMemberships, actions.Issue.receiveTimeEntry],
    getMemberships
  );
}

const Handlers = [requestMemberships];
export default Handlers;
