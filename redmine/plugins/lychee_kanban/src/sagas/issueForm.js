import { all, call, put, takeLatest, select } from 'redux-saga/effects';
import { SAGA_ACTION } from '@redux-saga/symbols';
import * as reduxForm from 'redux-form';
import * as _ from 'lodash';

import * as Redmine from '@/api/redmine';
import * as actions from '@/actions';
import * as formTypes from '@/constants/formTypes';
import { IssueForm } from '@/models/issueForm';
import { mask } from './common';
import { updateDataSourceUsers } from '@/utils';

const toParams = (values) => {
  if (values) {
    const keys = Object.keys(values);
    return Object.assign(...keys.map((key) => ({ [`issue[${key}]`]: values[key], })));
  }
  return null;
};
export function* showIssueForm({ payload, meta }) {
  try {
    const { position, query, path } = meta;
    const { redmine } = yield select();
    const { issue, versions, ...options } = yield call(
      Redmine.Issues.show,
      payload,
      {
        ...toParams(query),
        context: redmine.page,
      },
      { path }
    );
    const { custom_fields: custom_field_values, journals, attachments } = issue;
    updateDataSourceUsers(issue.project.id, issue.id);
    yield put(
      actions.IssueForm.receiveIssueForm({
        versions,
        custom_field_values,
        journals,
        attachments,
        ...options,
      })
    );
    yield put(actions.IssueForm.initializeIssueFormValues({ issue, versions }));
    yield put(actions.IssueForm.showIssueForm({ position }));
  } catch (error) {
    yield put(actions.Notification.showRedmineErrors(error));
    yield put(actions.Issue.receiveIssue(error));
  }
}

export function* requestShowIssueForm() {
  yield takeLatest(actions.IssueForm.requestIssueForm, mask(showIssueForm));
}

const makeFieldEvent = (saga) => function* fieldEvent(action) {
  if (action.meta.form !== formTypes.ISSUE_FORM || action[SAGA_ACTION]) {
    return null;
  }
  const response = yield call(saga, action);
  return response;
};

const getTargetCustomFields = (fields) => {
  const required = _.filter(fields, { has_required_custom_fields: true });
  const invisible = _.filter(fields, { has_invisible_custom_field_enumerations: true, });
  const targets = _.uniq([...required, ...invisible]);
  return targets.map((field) => `custom_field_values.${field.id}`);
};

const getAptStatusId = (action, issueForm, default_status_id) => {
  const {
    meta: { field },
    payload,
  } = action;
  const { issue, trackers } = issueForm;
  const isNew = !issue.get('id');
  if (field === 'tracker_id' && isNew) {
    const tracker = trackers.find((trk) => Number(trk.get('id')) === Number(payload)) || {};
    return tracker.get('default_status_id') || default_status_id;
  }
  return default_status_id;
};

export function* mergeIssueFormValues(params) {
  const state = yield select();
  const { issue } = new IssueForm(params).toJS();
  const { ISSUE_FORM: { registeredFields = {}, values = {} } = {} } = state.form;
  yield all(
    Object.keys(registeredFields).map((key) => {
      const value = _.get(issue, key);
      if (reduxForm.isPristine(formTypes.ISSUE_FORM)(state, [key])) {
        if (!_.isEmpty(value) && !_.isEqual(value, _.get(values, key))) {
          return put(reduxForm.autofill(formTypes.ISSUE_FORM, key, value));
        }
      }
      return null;
    })
  );
}

const TARGET_FIELDS = ['project_id', 'tracker_id', 'status_id'];

export function* updateIssueForm(action) {
  const { issueForm, form, redmine } = yield select();
  const {
    project_id,
    tracker_id,
    status_id,
    custom_field_values = {},
    start_date,
  } = form.ISSUE_FORM.values;
  const targetCustomFields = getTargetCustomFields(
    issueForm.get('custom_fields').toJS()
  );
  const isTargetCustomField = targetCustomFields.includes(action.meta.field);
  if (TARGET_FIELDS.includes(action.meta.field) || isTargetCustomField) {
    try {
      const params = {
        'context': redmine.page,
        'issue[project_id]': project_id,
        'issue[tracker_id]': tracker_id,
        'issue[status_id]': getAptStatusId(action, issueForm, status_id),
        'issue[start_date]': start_date,
      };
      _.map(custom_field_values, (value, key) => {
        if (targetCustomFields.includes(`custom_field_values.${key}`)) {
          if (_.isArray(value)) {
            params[`issue[custom_field_values][${key}][]`] = _.isEmpty(value) ? null : value;
          } else {
            params[`issue[custom_field_values][${key}]`] = value;
          }
        }
      });
      const response = yield call(
        mask(Redmine.Issues.show),
        issueForm.issue.get('id') || 'new',
        params
      );
      const { issue, ...options } = response;
      yield put(actions.IssueForm.receiveIssueForm(options));
      yield call(mergeIssueFormValues, { issue, versions: options.versions });
      const { project, status, tracker } = issue;
      yield all([
        put(reduxForm.change(formTypes.ISSUE_FORM, 'project_id', project.id)),
        put(reduxForm.change(formTypes.ISSUE_FORM, 'status_id', status.id)),
        put(reduxForm.change(formTypes.ISSUE_FORM, 'tracker_id', tracker.id)),
      ]);
    } catch (error) {
      yield put(actions.Issue.receiveIssue(error));
    }
  }
}

export function* requestUpdateIssueForm() {
  yield takeLatest(
    [
      reduxForm.actionTypes.CHANGE,
      reduxForm.actionTypes.ARRAY_PUSH,
      reduxForm.actionTypes.ARRAY_REMOVE,
    ],
    makeFieldEvent(updateIssueForm)
  );
}

export function* updateIssueDates(action) {
  const { issueForm, form } = yield select();
  if (action.meta.field === 'fixed_version_id') {
    const { fixed_version_id } = form.ISSUE_FORM.values;
    const { versions } = issueForm;
    const version = versions.find((item) => Number(item.get('id')) === Number(fixed_version_id));
    if (version) {
      yield all([
        put(
          reduxForm.change(
            formTypes.ISSUE_FORM,
            'start_date',
            version.get('start_date', '')
          )
        ),
        put(
          reduxForm.change(
            formTypes.ISSUE_FORM,
            'due_date',
            version.get('due_date', '')
          )
        ),
      ]);
    }
  }
}

export function* requestUpdateIssueDates() {
  yield takeLatest(
    reduxForm.actionTypes.CHANGE,
    makeFieldEvent(updateIssueDates)
  );
}

export function* updateDueDate(action) {
  const { form } = yield select();
  const { start_date, due_date } = form.ISSUE_FORM.values;
  if (action.meta.field === 'start_date' && !due_date) {
    yield put(reduxForm.change(formTypes.ISSUE_FORM, 'due_date', start_date));
  }
}

export function* requestUpdateDueDate() {
  yield takeLatest(reduxForm.actionTypes.CHANGE, makeFieldEvent(updateDueDate));
}

const Handlers = [
  requestShowIssueForm,
  requestUpdateIssueForm,
  requestUpdateDueDate,
  requestUpdateIssueDates,
];
export default Handlers;
