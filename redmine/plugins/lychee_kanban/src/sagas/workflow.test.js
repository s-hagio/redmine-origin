import { put, call } from 'redux-saga/effects';

import * as Redmine from '@/api/redmine';
import * as actions from '@/actions/workflow';

import { getWorkflows } from './workflow';

describe('workflowSaga', () => {
  describe('getWorkflows', () => {
    let saga;
    beforeEach(() => {
      saga = getWorkflows();
    });

    it('receives fetch request and get data', () => {
      expect(saga.next().value).toEqual(call(Redmine.Workflows.index));
      expect(saga.next({ workflows: [] }).value).toEqual(
        put(actions.receiveWorkflows([]))
      );
    });

    it('receives fetch request and raise error', () => {
      expect(saga.next().value).toEqual(call(Redmine.Workflows.index));
      expect(saga.throw().value).toEqual(put(actions.receiveWorkflows()));
    });
  });
});
