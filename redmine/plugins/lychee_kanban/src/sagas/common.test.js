import { put, call } from 'redux-saga/effects';

import * as actions from '@/actions';
import { mask } from './common';

// HACK: returnでの返却、throwによるエラーハンドリングがテストできていない
describe('mask', () => {
  it('calls saga prop and toggle LoadingPanel', () => {
    const mockSaga = jest.fn();
    const saga = mask(mockSaga)();
    expect(saga.next().value).toEqual(put(actions.LoadingPanel.show()));
    expect(saga.next().value).toEqual(call(mockSaga));
    expect(saga.next().value).toEqual(put(actions.LoadingPanel.hide()));
  });
});
