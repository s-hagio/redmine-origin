import { takeEvery } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import * as _ from 'lodash';

import * as actions from '@/actions/notification';

export function* showRedmineErrors(action) {
  const { response } = action.payload;
  const messages = yield response
    .json()
    .then((json) => json.messages || json.errors) || [];
  _.each(messages, (message) => {
    toast.error(message);
  });
}

export function* takeRedmineErrors() {
  yield takeEvery(actions.showRedmineErrors, showRedmineErrors);
}

const Handlers = [takeRedmineErrors];
export default Handlers;
