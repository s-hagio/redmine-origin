import {
  all,
  call,
  put,
  takeEvery,
  takeLatest,
  takeLeading,
  select,
} from 'redux-saga/effects';
import { toast } from 'react-toastify';
import * as _ from 'lodash';
import * as reduxForm from 'redux-form';
import i18n from 'i18next';

import * as Redmine from '@/api/redmine';
import * as actions from '@/actions';
import * as formTypes from '@/constants/formTypes';

import { mask } from './common';
import { getMemberships } from './membership';
import { AutoloadManager } from '@/autoload_manager';

export function* needToUpdateDescendants(issue) {
  const { issueForm, workflow } = yield select();
  const {
    id,
    subject: prevSubject,
    trakcer_id: prevTrackerId,
    parent_issue_id: prevParentIssueId,
  } = issueForm.issue.toJS();

  if (id) {
    const changedSubject = issue.subject !== prevSubject;

    const parentIssueId = _.get(issue, ['parent', 'id']) || '';
    const changedParent = parentIssueId !== (prevParentIssueId || '');

    const beforeWorkflow = workflow.findByTracker(prevTrackerId);
    const afterWorkflow = workflow.findByTracker(issue.tracker.id);
    const changedWorkflow = beforeWorkflow !== afterWorkflow;

    return changedParent || changedWorkflow || changedSubject;
  }
  return false;
}

export function* noticeLadUpdate(payload, response) {
  const state = yield select();
  const issue = state.issue.find(payload.id);
  if (issue) {
    let changeStatus = false;
    if (payload.status_id) {
      changeStatus = issue.status.id !== response.status.id;
    }
    const excludeActualDates = !(
      payload.actual_start_date || payload.actual_due_date
    );
    if (state.redmine.lad_included && changeStatus && excludeActualDates) {
      const changeActualStartDate = (issue.actual_start_date || '') !== (response.actual_start_date || '');
      const changeActualDueDate = (issue.actual_due_date || '') !== (response.actual_due_date || '');
      if (changeActualStartDate) {
        toast.info(i18n.t('Notification:setActualStartDateByLad'));
      }
      if (changeActualDueDate) {
        toast.info(i18n.t('Notification:setActualDueDateByLad'));
      }
    }
  }
}

export function* findVisualParent(action) {
  const { issue, workflow } = yield select();
  const ancestorIds = action.payload.ancestors.map((item) => item.id);
  const newWorkflow = workflow.findByTracker(action.payload.tracker.id);
  return issue
    .findByWorkflow(newWorkflow)
    .filter((item) => ancestorIds.includes(item.id))
    .maxBy((item) => item.depth);
}

export function* getIssues(action) {
  const response = yield call(Redmine.Issues.index, { query: action.query });
  // eslint-disable-next-line no-unused-vars
  const { issues, ...meta } = response;
  yield put(actions.Issue.receiveIssues(response.issues, { ...meta }));
  return response;
}

export function* refreshIssues(action) {
  const response = yield call(Redmine.Issues.index, { query: action.query });
  // eslint-disable-next-line no-unused-vars
  const { issues, ...meta } = response;
  if (!meta.no_updated) {
    yield put(actions.Issue.refreshIssues(response.issues, { ...meta }));
  }
  return response;
}

export function* getIssuesWithPaging({ query, options }) {
  const limit = 500;
  for (let page = 1; ; page += 1) {
    const response = yield call(getIssues, { query: query.toURL({ ...options, page, limit }), });
    if (response.issues.length < limit) {
      break;
    }
  }
}

export function* getRankedKanbanIssues() {
  const { query, redmine } = yield select();
  const queryString = query.toURL({
    'sort_criteria[0]': redmine.sort_column,
    'limit': redmine.issues_limit,
    'include_visual_tree_info': 1,
    'journal_id': redmine.journal_id,
    'issue_id': redmine.issue_id,
    'sorted_ids': redmine.sorted_ids,
  });
  const res = yield call(refreshIssues, { query: queryString });
  redmine.journal_id = res.journal_id;
  redmine.issue_id = res.issue_id;
  redmine.sorted_ids = res.sorted_ids;
  return res;
}

export function* requestGetRankedKanbanIssues() {
  yield takeLeading(actions.Issue.requestGetRankedKanbanIssues, getRankedKanbanIssues);
}

export function* getAssignedKanbanIssues() {
  const { query, redmine } = yield select();
  const queryString = query.toURL({
    'sort_criteria[0]': redmine.sort_column,
    'limit': redmine.issues_limit,
    'journal_id': redmine.journal_id,
    'issue_id': redmine.issue_id,
    'sorted_ids': redmine.sorted_ids,
  });
  const res = yield call(refreshIssues, { query: queryString });
  redmine.journal_id = res.journal_id;
  redmine.issue_id = res.issue_id;
  redmine.sorted_ids = res.sorted_ids;
  yield call(getMemberships);
  return res;
}

export function* requestGetAssignedKanbanIssues() {
  yield takeLeading(actions.Issue.requestGetAssignedKanbanIssues, getAssignedKanbanIssues);
}

export function* getIssue(action) {
  const { redmine } = yield select();
  const { issue } = yield call(Redmine.Issues.show, action.payload, { context: redmine.page, });
  const visualParent = yield call(findVisualParent, { payload: issue });
  yield put(
    actions.Issue.replaceIssue({
      ...issue,
      visual_parent_id: (visualParent || {}).id,
    })
  );
}

export function* updateDescendants(issue) {
  const needToUpdate = yield call(needToUpdateDescendants, issue);
  if (needToUpdate) {
    const state = yield select();
    const issueIds = state.issue.ids;
    const calls = issue.descendants
      .filter((descendant) => issueIds.includes(descendant.id))
      .map((descendant) => call(getIssue, { payload: descendant.id }));
    yield all(calls);
  }
}

export function* updateParents(issue) {
  const state = yield select();
  const before = state.issue.find(issue.id);
  const parentId = _.get(issue, ['parent', 'id'], null);
  const beforeParentId = before ? before.parent.id : null;
  if (parentId !== beforeParentId) {
    const issueIds = state.issue.ids;
    const calls = [parentId, beforeParentId]
      .filter((id) => issueIds.includes(id))
      .map((id) => call(getIssue, { payload: id }));
    yield all(calls);
  }
}

export function* createIssue(action) {
  if (!action.error) {
    try {
      const { attachments } = action.payload;
      const attachments_param = {};
      if (attachments) {
        const files = action.payload.attachments.filter((v) => v);
        const response = yield all(
          files.map((file) => call(mask(Redmine.Attachments.create), null, file[0],))
        );
        response.forEach((res, index) => {
          const description = attachments[index][0].description ?? '';
          attachments_param[index] = {
            filename: res.upload.filename, token: res.upload.token, description,
          };
        });
      }

      const { redmine } = yield select();
      const { issue } = yield call(
        Redmine.Issues.create,
        action.payload,
        {
          context: redmine.page,
          attachments: attachments_param,
        },
      );
      yield call(updateParents, issue);
      const visualParent = yield call(findVisualParent, { payload: issue });
      const payload = { ...issue, visual_parent_id: (visualParent || {}).id };
      yield put(actions.Issue.receiveIssue(payload));
      toast.success(i18n.t('Notification:createdIssue', { id: issue.id }));
      AutoloadManager.restart();
      yield put(actions.IssueForm.hideIssueForm());
    } catch (error) {
      yield put(actions.Notification.showRedmineErrors(error));
      yield put(actions.Issue.createIssue(error));
    }
  }
}

export function* requestCreateIssue() {
  yield takeLatest(actions.Issue.createIssue, mask(createIssue));
}

export function* createSimpleIssue(action) {
  try {
    const { redmine } = yield select();
    const options = { context: redmine.page };
    const { issue } = yield call(
      Redmine.Issues.create,
      action.payload,
      options
    );
    yield call(Redmine.Issues.update, issue.id, { issue_index: -1 }, options);
    yield call(updateParents, issue);
    const visualParent = yield call(findVisualParent, { payload: issue });
    const payload = { ...issue, visual_parent_id: (visualParent || {}).id };
    yield put(actions.Issue.receiveIssue(payload));
    toast.success(i18n.t('Notification:createdIssue', { id: issue.id }));
  } catch (error) {
    yield put(actions.Notification.showRedmineErrors(error));
    yield put(
      actions.IssueForm.requestIssueForm('new', { query: action.payload })
    );
  }
}

export function* requestCreateSimpleIssue() {
  yield takeLatest(actions.Issue.createSimpleIssue, mask(createSimpleIssue));
}

export function* updateIssue({ payload, meta = {}, error }) {
  if (!error) {
    try {
      const state = yield select();
      const { time_entry, path } = meta;
      const { attachments } = payload;
      const attachments_param = {};
      if (attachments) {
        const files = payload.attachments.filter((v) => v);
        const response = yield all(
          files.map((file) => call(mask(Redmine.Attachments.create), null, file[0],))
        );
        response.forEach((res, index) => {
          const description = attachments[index][0].description ?? '';
          attachments_param[index] = {
            filename: res.upload.filename, token: res.upload.token, description
          };
        });
      }

      const { issue } = yield call(Redmine.Issues.update, payload.id, payload, {
        context: state.redmine.page,
        time_entry,
        path,
        attachments: attachments_param,
      });
      if (time_entry) {
        yield put(
          actions.Issue.receiveTimeEntry({
            issue: payload,
            hours: time_entry.hours,
            user: { id: time_entry.user_id },
          })
        );
      }
      yield call(updateParents, issue);
      yield call(updateDescendants, issue);
      yield call(noticeLadUpdate, payload, issue);
      const visualParent = yield call(findVisualParent, { payload: issue });
      yield put(
        actions.Issue.replaceIssue({
          ...issue,
          visual_parent_id: (visualParent || {}).id,
        })
      );
      AutoloadManager.restart();
      yield put(actions.IssueForm.hideIssueForm());
    } catch (exception) {
      yield put(actions.Notification.showRedmineErrors(exception));
      toast.error(
        i18n.t('Notification:failedToUpdateIssue', { issueId: payload.id })
      );
      yield put(actions.Issue.updateIssue(exception));
    }
  }
}

export function* requestUpdateIssue() {
  yield takeEvery(actions.Issue.updateIssue, mask(updateIssue));
}

export function* sortIssues({ payload }) {
  try {
    const state = yield select();
    const { issue } = yield call(
      Redmine.Issues.update,
      payload.id,
      payload,
      { context: state.redmine.page },
    );
    const sortParams = _.omitBy(
      _.pick(payload, ['before_issue_id', 'after_issue_id', 'issue_index']),
      _.isNil
    );
    const before = state.issue.find(issue.id);
    yield put(
      actions.Issue.receiveIssueSorted(issue, {
        id: issue.id,
        sort_column: state.redmine.sort_column,
        before,
        ...sortParams,
      })
    );

    yield put(actions.HoveredIssue.deleteHoveredIssue());
  } catch (exception) {
    yield put(actions.Notification.showRedmineErrors(exception));
    toast.error(
      i18n.t('Notification:failedToUpdateIssue', { issueId: payload.id })
    );
    yield put(actions.Issue.updateIssue(exception));
  }
}

export function* requestSortIssues() {
  yield takeEvery(actions.Issue.sortIssues, mask(sortIssues));
}

export function* createTimeEntry({ payload, error }) {
  if (!error) {
    try {
      const { time_entry } = yield call(Redmine.TimeEntry.create, payload);
      yield put(actions.Issue.receiveTimeEntry(time_entry));
      yield put(reduxForm.reset(formTypes.ISSUE_TIME_ENTRY_FORM));
    } catch (exception) {
      yield put(actions.Notification.showRedmineErrors(exception));
      yield put(actions.Issue.createTimeEntry(exception));
    }
  }
}

export function* requestCreateTimeEntry() {
  yield takeLatest(actions.Issue.createTimeEntry, mask(createTimeEntry));
}

const Handlers = [
  requestGetRankedKanbanIssues,
  requestGetAssignedKanbanIssues,
  requestCreateIssue,
  requestUpdateIssue,
  requestSortIssues,
  requestCreateTimeEntry,
  requestCreateSimpleIssue,
];
export default Handlers;
