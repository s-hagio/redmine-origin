import { put, call, select, takeLatest } from 'redux-saga/effects';

import * as Redmine from '@/api/redmine';
import * as actions from '@/actions';

import { requestMemberships, getMemberships } from './membership';

describe('membershipSaga', () => {
  let saga;
  describe('requestMemberships', () => {
    it('calls getMemberships', () => {
      saga = requestMemberships();
      const ret = saga.next();
      const expected = takeLatest(
        [actions.Membership.requestMemberships, actions.Issue.receiveTimeEntry],
        getMemberships
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('getMemberships', () => {
    beforeEach(() => {
      saga = getMemberships();
    });

    it('receives fetch request and get data', () => {
      expect(saga.next().value).toEqual(select());
      const state = { query: { toURL: jest.fn(() => 'query') }, redmine: {} };
      expect(saga.next(state).value).toEqual(
        call(Redmine.Memberships.index, { query: 'query' })
      );
      expect(saga.next({ memberships: [] }).value).toEqual(
        put(actions.Membership.receiveMemberships([]))
      );
    });

    it('receives fetch request and raise error', () => {
      expect(saga.next().value).toEqual(select());
      expect(saga.throw().value).toEqual(
        put(actions.Membership.receiveMemberships())
      );
    });
  });
});
