import { all, call, put, takeLatest, select } from 'redux-saga/effects';
import * as _ from 'lodash';

import * as Redmine from '@/api/redmine/versions';
import * as actions from '@/actions';

import { mask } from './common';

export function* getVersions(action = {}) {
  try {
    const { query } = yield select();
    const response = yield call(Redmine.index, query.toURL(action.payload));
    yield put(actions.Version.receiveVersions(response));
    return response.versions;
  } catch (error) {
    yield put(actions.Version.receiveVersions(error));
    throw error;
  }
}

export function* requestVersions() {
  yield takeLatest(actions.Version.requestVersions, mask(getVersions));
}

const parseQuery = (values) => {
  if (values) {
    const keys = Object.keys(values);
    return Object.assign(...keys.map((key) => ({ [`issue[${key}]`]: values[key], })));
  }
  return null;
};
export function* showVersion({ payload, meta, error }) {
  if (!error) {
    try {
      const response = yield call(
        Redmine.show,
        payload,
        parseQuery(meta.query)
      );
      yield put(actions.Version.receiveVersionForm(response));
      yield put(actions.Version.showVersionForm({ position: meta.position }));
    } catch (exception) {
      yield put(actions.Notification.showRedmineErrors(exception));
      yield put(actions.Version.receiveVersion(exception));
    }
  }
}

export function* requestShowVersion() {
  yield takeLatest(actions.Version.requestVersion, mask(showVersion));
}

export function* createVersion({ payload, error }) {
  if (!error) {
    try {
      const state = yield select();
      const unit = state?.versions?.unit || undefined;
      const input_daily_velocity = state?.versions?.dailyVelocity || undefined;
      const { version } = yield call(Redmine.create, payload, { unit, input_daily_velocity });
      yield put(actions.Version.receiveVersion(version));
      yield put(actions.Version.hideVersionForm());
    } catch (exception) {
      yield put(actions.Notification.showRedmineErrors(exception));
      yield put(actions.Version.createVersion(exception));
    }
  }
}

export function* requestCreateVersion() {
  yield takeLatest(actions.Version.createVersion, mask(createVersion));
}

export function* updateVersion({ payload: { id, ...params }, error }) {
  if (!error) {
    try {
      const state = yield select();
      const unit = state?.versions?.unit || undefined;
      const dailyVelocity = state?.versions?.dailyVelocity || undefined;

      const query = {};
      if (unit) query.unit = unit;
      if (dailyVelocity) query.input_daily_velocity = dailyVelocity;

      const { version } = yield call(Redmine.update, id, params, query);
      yield put(actions.Version.receiveVersion(version));
      yield put(actions.Version.hideVersionForm());
    } catch (exception) {
      yield put(actions.Notification.showRedmineErrors(exception));
      yield put(actions.Version.updateVersion(exception));
    }
  }
}

export function* requestUpdateVersion() {
  yield takeLatest(actions.Version.updateVersion, mask(updateVersion));
}

export function* destroyVersion({ payload, error }) {
  if (!error) {
    try {
      yield call(Redmine.destroy, payload);
      yield put(actions.Version.removeVersion(payload));
    } catch (exception) {
      yield put(actions.Notification.showRedmineErrors(exception));
      yield put(actions.Version.destroyVersion(exception));
    }
  }
}

export function* requestDestroyVersion() {
  yield takeLatest(actions.Version.destroyVersion, mask(destroyVersion));
}

const costDiff = (backlogPointEnabled, beforeIssue, costs) => {
  let beforeCosts;
  let afterCosts;

  if (backlogPointEnabled) {
    beforeCosts = Number(beforeIssue.point || 0);
    afterCosts = Number(costs.points || 0);
  } else {
    beforeCosts = Number(beforeIssue.estimated_hours || 0);
    afterCosts = Number(costs.total_estimated_hours || 0);
  }

  return {
    isChanged: beforeCosts !== afterCosts,
    wasZero: beforeCosts === 0,
    toZero: afterCosts === 0,
  };
};

export function* updateVersionsTotalCosts({ payload, meta = {}, error }) {
  if (!error) {
    const state = yield select();
    if (state.redmine.page !== 'backlog') {
      return;
    }
    const beforeIssue = meta.before ? meta.before.toJS() : {};
    const existBefore = !!meta.before;

    const beforeVersionId = Number(_.get(beforeIssue, 'fixed_version.id') || 0);
    const afterVersionId = Number(_.get(payload, 'fixed_version.id') || 0);
    const changedVersion = beforeVersionId !== afterVersionId;
    const fromBacklog = beforeVersionId === 0;
    const toBacklog = afterVersionId === 0;
    const costState = costDiff(state.redmine.backlog_point_enabled, beforeIssue, payload.costs);
    const query = {
      unit: state.versions.unit,
      input_daily_velocity: state.versions.dailyVelocity,
    };

    try {
      /* eslint-disable */
      // HACK: 読みやすいように書き直す
      const versions = yield all([
        existBefore &&
        !fromBacklog &&
        ((changedVersion && !costState.wasZero) || (costState.isChanged && !changedVersion)) &&
        call(Redmine.show, beforeVersionId, { ...query }),
        changedVersion &&
        !costState.toZero &&
        !toBacklog &&
        call(Redmine.show, afterVersionId, { ...query }),
      ]);
      /* eslint-enable */
      const compactedVersions = _.compact(_.map(versions, 'version'));
      yield all(compactedVersions.map((version) => put(actions.Version.receiveVersion(version))));
    } catch (exception) {
      yield put(actions.Notification.showRedmineErrors(exception));
      yield put(actions.Version.receiveVersion(exception));
    }
  }
}

export function* requestUpdateVersionsTotalCosts() {
  yield takeLatest(
    [actions.Issue.receiveIssue, actions.Issue.receiveIssueSorted],
    updateVersionsTotalCosts,
  );
}

const Handlers = [
  requestVersions,
  requestShowVersion,
  requestCreateVersion,
  requestUpdateVersion,
  requestDestroyVersion,
  requestUpdateVersionsTotalCosts,
];
export default Handlers;
