import { all, call, select, takeLatest } from 'redux-saga/effects';

import * as actions from '@/actions';
import { QueryList } from '@/models/query';
import * as sagas from './initialize';
import { mask } from './common';
import { getIssuesWithPaging, getAssignedKanbanIssues, getRankedKanbanIssues } from './issue';
import { getMemberships } from './membership';
import { getVersions } from './version';
import { getWorkflows } from './workflow';

describe('initialize', () => {
  describe('requestInitializeAssignedKanban', () => {
    it('calls initializeAssignedKanban', () => {
      const saga = sagas.requestInitializeAssignedKanban();
      const ret = saga.next();
      const expected = takeLatest(
        actions.initAssignedKanbanApp,
        mask(sagas.initializeAssignedKanban)
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('initializeAssignedKanban', () => {
    it('receives fetch request and get data', () => {
      const saga = sagas.initializeAssignedKanban();
      expect(saga.next().value).toEqual(select());
      const queryState = { toURL: jest.fn() };
      const issues_limit = 10;
      const redmineState = { sort_column: 'sort_by_backlog', issues_limit };
      expect(
        saga.next({ query: queryState, redmine: redmineState }).value
      ).toEqual(
        all({
          issue: call(getAssignedKanbanIssues),
          membership: call(getMemberships),
          workflow: call(getWorkflows),
        })
      );
    });
  });

  describe('requestInitializeBacklog', () => {
    it('calls initializeBacklog', () => {
      const saga = sagas.requestInitializeBacklog();
      const ret = saga.next();
      const expected = takeLatest(
        actions.initBacklogApp,
        mask(sagas.initializeBacklog)
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('initializeBacklog', () => {
    const versions = [
      { id: 99, status: 'open' },
      { id: 88, status: 'closed' },
    ];
    const sort_column = 'sort_by_backlog';
    const visible_subproject = false;
    const query = new QueryList({
      parent_id: { operator: '!*' },
      subproject_id: { operator: '!*' },
    });
    const versionQuery = new QueryList({});
    const queryOptions = { 'sort_criteria[0]': sort_column };
    const backlogIssuesQueryBase = query.add({
      fixed_version_id: { operator: '!*' },
      status_id: { operator: 'o' },
    });

    it('receives fetch request and get data', () => {
      const saga = sagas.initializeBacklog();
      expect(saga.next().value).toEqual(call(getWorkflows));
      expect(saga.next().value).toEqual(select());
      expect(
        saga.next({
          redmine: { sort_column, visible_subproject },
          query: versionQuery,
        }).value
      ).toEqual(call(getVersions));
      expect(saga.next(versions).value).toEqual(
        all([
          call(getIssuesWithPaging, {
            query: query.add({ fixed_version_id: { operator: '=', values: [99] }, }),
            options: queryOptions,
          }),
          call(getIssuesWithPaging, {
            query: query.add({ fixed_version_id: { operator: '=', values: [88] }, }),
            options: queryOptions,
          }),
          call(getIssuesWithPaging, {
            query: backlogIssuesQueryBase,
            options: queryOptions,
          }),
        ])
      );
    });

    describe('avatar enabled', () => {
      it('receives memberships', () => {
        const saga = sagas.initializeBacklog();
        saga.next();
        saga.next(); // select が実行される
        saga.next({
          redmine: { sort_column, visible_subproject, avatar_enabled: true },
          query: versionQuery,
        });
        expect(saga.next(versions).value).toEqual(
          all([
            call(getIssuesWithPaging, {
              query: query.add({ fixed_version_id: { operator: '=', values: [99] }, }),
              options: queryOptions,
            }),
            call(getIssuesWithPaging, {
              query: query.add({ fixed_version_id: { operator: '=', values: [88] }, }),
              options: queryOptions,
            }),
            call(getIssuesWithPaging, {
              query: backlogIssuesQueryBase,
              options: queryOptions,
            }),
            call(getMemberships),
          ])
        );
      });
    });
  });

  describe('requestInitializeRankedKanban', () => {
    it('calls initializeRankedKanban', () => {
      const saga = sagas.requestInitializeRankedKanban();
      const ret = saga.next();
      const expected = takeLatest(
        actions.initRankedKanbanApp,
        mask(sagas.initializeRankedKanban)
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('initializeRankedKanban', () => {
    const query = { toURL: jest.fn() };
    const issues_limit = 10;
    const sort_column = 'sort_by_backlog';

    it('receives fetch request and get data', () => {
      const saga = sagas.initializeRankedKanban();
      expect(saga.next().value).toEqual(select());
      expect(
        saga.next({ query, redmine: { sort_column, issues_limit } }).value
      ).toEqual(
        all({
          issue: call(getRankedKanbanIssues),
          workflow: call(getWorkflows),
          versions: call(getVersions),
        })
      );
    });

    describe('avatar enabled', () => {
      it('receives memberships', () => {
        const saga = sagas.initializeRankedKanban();
        saga.next();
        expect(
          saga.next({ query, redmine: { avatar_enabled: true } }).value
        ).toEqual(
          all({
            issue: call(getRankedKanbanIssues),
            workflow: call(getWorkflows),
            membership: call(getMemberships),
            versions: call(getVersions),
          })
        );
      });
    });
  });
});
