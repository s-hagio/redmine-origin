import { call, put } from 'redux-saga/effects';
import * as Redmine from '@/api/redmine';
import * as actions from '@/actions';

import * as sagas from './attachment';
import { mask } from './common';

describe('attachmentSaga', () => {
  describe('createAttachment', () => {
    const action = { payload: { name: 'sample' }, };
    it('requests create attachment and returns response', () => {
      const saga = sagas.createAttachment(action);
      const expected = JSON.stringify(
        call(mask(Redmine.Attachments.create), null, action.payload)
      );
      const result = JSON.stringify(saga.next().value);
      expect(result).toEqual(expected);
      const response = { upload: {} };
      expect(saga.next(response).value).toEqual(
        put(actions.Attachment.receiveAttachment(response.upload))
      );
      expect(saga.next().value).toEqual(response);
    });

    it('raises and handling error', () => {
      const saga = sagas.createAttachment(action);
      saga.next();
      expect(saga.throw('sample error').value).toEqual(
        put(actions.Attachment.createAttachment('sample error'))
      );
    });
  });

  describe('destroyAttachment', () => {
    const action = { payload: 1, };
    it('requests delete attachment and returns response', () => {
      const saga = sagas.destroyAttachment(action);
      const expected = JSON.stringify(
        call(mask(Redmine.Attachments.destroy), action.payload)
      );
      const result = JSON.stringify(saga.next().value);
      expect(result).toEqual(expected);
      expect(saga.next().value).toEqual(
        put(actions.Attachment.removeAttachment(action.payload))
      );
    });

    it('raises and handling error', () => {
      const saga = sagas.destroyAttachment(action);
      saga.next();
      expect(saga.throw('sample error').value).toEqual(
        put(actions.Attachment.destroyAttachment('sample error'))
      );
    });
  });
});
