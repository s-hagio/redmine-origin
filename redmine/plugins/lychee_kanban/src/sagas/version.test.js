import { call, put, takeLatest, select } from 'redux-saga/effects';

import * as Redmine from '@/api/redmine';
import * as actions from '@/actions/version';
import * as Notification from '@/actions/notification';

import * as versionSaga from './version';
import { mask } from './common';
import { QueryList } from '../models/query';

describe('versionSaga', () => {
  let saga;
  describe('getVersions', () => {
    beforeEach(() => {
      saga = versionSaga.getVersions();
    });

    const state = { query: new QueryList() };

    it('receives fetch request and get data', () => {
      expect(saga.next().value).toEqual(select());
      expect(saga.next(state).value).toEqual(call(Redmine.Versions.index, ''));
      expect(saga.next({ versions: [] }).value).toEqual(
        put(actions.receiveVersions({ versions: [] }))
      );
    });
    it('receives fetch request and raise error', () => {
      expect(saga.next().value).toEqual(select());
      expect(saga.next(state).value).toEqual(call(Redmine.Versions.index, ''));
      expect(saga.throw().value).toEqual(put(actions.receiveVersions()));
    });
  });

  describe('showVersion', () => {
    const action = {
      payload: 0,
      meta: { position: { x: 0, y: 0 }, },
    };
    beforeEach(() => {
      saga = versionSaga.showVersion(action);
    });

    it('receives fetch request and get data', () => {
      expect(saga.next().value).toEqual(call(Redmine.Versions.show, 0, null));
      const response = { version: { id: 0, name: 'hoge' } };
      expect(saga.next(response).value).toEqual(
        put(actions.receiveVersionForm(response))
      );
      expect(saga.next().value).toEqual(
        put(actions.showVersionForm({ position: { x: 0, y: 0 } }))
      );
    });

    it('receives fetch request and raise error', () => {
      saga.next();
      expect(saga.throw({}).value).toEqual(
        put(Notification.showRedmineErrors({}))
      );
      expect(saga.next().value).toEqual(put(actions.receiveVersion({})));
    });
  });

  describe('requestShowVersion', () => {
    it('calls showVersion', () => {
      saga = versionSaga.requestShowVersion();
      const ret = saga.next();
      const expected = takeLatest(
        actions.requestVersion,
        mask(versionSaga.showVersion)
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('createVersion', () => {
    it('calls create and stores response', () => {
      const action = { payload: { name: 'hoge' } };
      saga = versionSaga.createVersion(action);
      expect(saga.next().value).toEqual(select());
      expect(saga.next().value).toEqual(
        call(Redmine.Versions.create, { name: 'hoge' }, {})
      );
      const response = { version: { id: 1, name: 'hoge' } };
      expect(saga.next(response).value).toEqual(
        put(actions.receiveVersion(response.version))
      );
      expect(saga.next().value).toEqual(put(actions.hideVersionForm()));
    });

    it('receive fetch request and throw error', () => {
      const action = { payload: { name: 'hoge' } };
      saga = versionSaga.createVersion(action);
      expect(saga.next().value).toEqual(select());
      saga.next();
      expect(saga.throw({}).value).toEqual(
        put(Notification.showRedmineErrors({}))
      );
      expect(saga.next().value).toEqual(put(actions.createVersion({})));
    });
  });

  describe('requestCreateVersion', () => {
    it('calls createVersion with mask', () => {
      saga = versionSaga.requestCreateVersion();
      const ret = saga.next();
      const expected = takeLatest(
        actions.createVersion,
        mask(versionSaga.createVersion)
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('updateVersion', () => {
    it('calls update and stores response', () => {
      const action = { payload: { id: 1, name: 'hoge' } };
      saga = versionSaga.updateVersion(action);
      expect(saga.next().value).toEqual(select());
      expect(saga.next().value).toEqual(
        call(Redmine.Versions.update, 1, { name: 'hoge' }, {})
      );
      const response = { version: { id: 1, name: 'hoge' } };
      expect(saga.next(response).value).toEqual(
        put(actions.receiveVersion(response.version))
      );
      expect(saga.next().value).toEqual(put(actions.hideVersionForm()));
    });

    it('receive fetch request and throw error', () => {
      const action = { payload: { id: 1 } };
      saga = versionSaga.updateVersion(action);
      saga.next();
      expect(saga.throw({}).value).toEqual(
        put(Notification.showRedmineErrors({}))
      );
      expect(saga.next().value).toEqual(put(actions.updateVersion({})));
    });
  });

  describe('requestUpdateVersion', () => {
    it('calls updateVersion with mask', () => {
      saga = versionSaga.requestUpdateVersion();
      const ret = saga.next();
      const expected = takeLatest(
        actions.updateVersion,
        mask(versionSaga.updateVersion)
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('destroyVersion', () => {
    const action = { payload: 1 };
    beforeEach(() => {
      saga = versionSaga.destroyVersion(action);
    });

    it('calls destroy and removes from store', () => {
      expect(saga.next().value).toEqual(call(Redmine.Versions.destroy, 1));
      expect(saga.next().value).toEqual(put(actions.removeVersion(1)));
    });

    it('throws error and displays message', () => {
      saga.next();
      expect(saga.throw({}).value).toEqual(
        put(Notification.showRedmineErrors({}))
      );
      expect(saga.next().value).toEqual(put(actions.destroyVersion({})));
    });
  });

  describe('requestDestroyVersion', () => {
    it('calls destroyVersion', () => {
      saga = versionSaga.requestDestroyVersion();
      const ret = saga.next();
      const expected = takeLatest(
        actions.destroyVersion,
        mask(versionSaga.destroyVersion)
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });
});
