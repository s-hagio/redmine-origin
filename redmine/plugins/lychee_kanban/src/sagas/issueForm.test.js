import { all, call, put, takeLatest, select } from 'redux-saga/effects';
import * as reduxForm from 'redux-form';

import * as Redmine from '@/api/redmine';
import * as actions from '@/actions';
import * as formTypes from '@/constants/formTypes';

import { mask } from './common';
import * as sagas from './issueForm';

describe('issueFormSaga', () => {
  describe('requestShowIssueForm', () => {
    it('calls updateIssueForm', () => {
      const saga = sagas.requestShowIssueForm();
      const expected = takeLatest(
        actions.IssueForm.requestIssueForm,
        mask(sagas.showIssueForm)
      );
      const result = saga.next().value;
      expect(JSON.stringify(result)).toEqual(JSON.stringify(expected));
    });
  });

  describe('showIssueForm', () => {
    const action = {
      payload: 99,
      meta: {
        query: { tracker_id: 99 },
        position: { x: 99, y: 88 },
        path: 'sample/path',
      },
    };

    it('request issue ans show issue form', () => {
      const saga = sagas.showIssueForm(action);
      expect(saga.next().value).toEqual(select());
      const state = { redmine: { page: 'backlog' } };
      expect(saga.next(state).value).toEqual(
        call(
          Redmine.Issues.show,
          action.payload,
          { 'issue[tracker_id]': 99, 'context': 'backlog' },
          { path: 'sample/path' }
        )
      );

      const response = { issue: { id: 99, project: { id: 100 } }, trackers: [], projects: [] };
      expect(saga.next(response).value).toEqual(
        put(actions.IssueForm.receiveIssueForm({ trackers: [], projects: [] }))
      );
      expect(saga.next().value).toEqual(
        put(actions.IssueForm.initializeIssueFormValues({
          issue: { id: 99, project: { id: 100 } }
        }))
      );
      expect(saga.next().value).toEqual(
        put(actions.IssueForm.showIssueForm({ position: action.meta.position }))
      );
    });

    it('puts action when raise error', () => {
      const saga = sagas.showIssueForm(action);
      saga.next();
      expect(saga.throw().value).toEqual(
        put(actions.Notification.showRedmineErrors())
      );
      expect(saga.next().value).toEqual(put(actions.Issue.receiveIssue()));
    });
  });

  describe('requestUpdateIssueForm', () => {
    it('calls updateIssueForm', () => {
      const saga = sagas.requestUpdateIssueForm();
      const expected = takeLatest(
        [
          reduxForm.actionTypes.CHANGE,
          reduxForm.actionTypes.ARRAY_PUSH,
          reduxForm.actionTypes.ARRAY_REMOVE,
        ],
        sagas.updateIssueForm
      );
      const result = saga.next().value;
      expect(JSON.stringify(result)).toEqual(JSON.stringify(expected));
    });
  });

  describe('updateIssueForm', () => {
    const action = {
      type: actions.IssueForm.requestIssueForm,
      meta: {
        field: 'tracker_id',
        form: 'ISSUE_FORM',
      },
    };
    const state = {
      issueForm: {
        issue: { get: jest.fn(() => 99), },
        get: jest.fn(() => ({ toJS: jest.fn() })),
      },
      form: {
        ISSUE_FORM: {
          values: {
            tracker_id: 88,
            status_id: 33,
          },
        },
      },
      redmine: { page: 'backlog', },
    };

    it('request issue ans show issue form', () => {
      const saga = sagas.updateIssueForm(action);
      expect(saga.next().value).toEqual(select());
      const params = {
        'context': 'backlog',
        'issue[tracker_id]': 88,
        'issue[status_id]': 33,
      };
      const expected = JSON.stringify(saga.next(state).value);
      const result = JSON.stringify(
        call(mask(Redmine.Issues.show), 99, params)
      );
      expect(expected).toEqual(result);
      const response = {
        issue: {
          subject: 'hogehoge',
          tracker: { id: 99 },
          status: { id: 88 },
          project: { id: 77 },
        },
        projects: [],
        trackers: [],
        versions: [],
      };
      expect(saga.next(response).value).toEqual(
        put(
          actions.IssueForm.receiveIssueForm({
            projects: [],
            trackers: [],
            versions: [],
          })
        )
      );
      expect(saga.next().value).toEqual(
        call(sagas.mergeIssueFormValues, {
          issue: response.issue,
          versions: [],
        })
      );
      expect(saga.next().value).toEqual(
        all([
          put(
            reduxForm.change(
              formTypes.ISSUE_FORM,
              'project_id',
              response.issue.project.id
            )
          ),
          put(
            reduxForm.change(
              formTypes.ISSUE_FORM,
              'status_id',
              response.issue.status.id
            )
          ),
          put(
            reduxForm.change(
              formTypes.ISSUE_FORM,
              'tracker_id',
              response.issue.tracker.id
            )
          ),
        ])
      );
    });

    it('puts action when raise error', () => {
      const saga = sagas.updateIssueForm(action);
      saga.next();
      saga.next(state);
      expect(saga.throw().value).toEqual(put(actions.Issue.receiveIssue()));
    });
  });
});
