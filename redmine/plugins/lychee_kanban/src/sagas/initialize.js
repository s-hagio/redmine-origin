import { all, call, select, takeLatest } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import i18n from 'i18next';
import * as _ from 'lodash';

import * as actions from '@/actions';
import { QueryList } from '@/models/query';
import { mask } from './common';
import { getIssuesWithPaging, getRankedKanbanIssues, getAssignedKanbanIssues } from './issue';
import { getMemberships } from './membership';
import { getVersions } from './version';
import { getWorkflows } from './workflow';
import { AutoloadManager } from '@/autoload_manager';

export function* initializeAssignedKanban() {
  const { redmine } = yield select();
  const { issue } = yield all({
    issue: call(getAssignedKanbanIssues),
    membership: call(getMemberships),
    workflow: call(getWorkflows),
  });
  if (issue.total_count > redmine.issues_limit) {
    toast.warning(
      i18n.t('Notification:overlimit', { limit: redmine.issues_limit })
    );
  }
  AutoloadManager.init();
}

export function* requestInitializeAssignedKanban() {
  yield takeLatest(
    actions.initAssignedKanbanApp,
    mask(initializeAssignedKanban)
  );
}

export function* initializeRankedKanban() {
  const { redmine } = yield select();
  const { issue } = yield all({
    issue: call(getRankedKanbanIssues),
    workflow: call(getWorkflows),
    membership: redmine.avatar_enabled && call(getMemberships),
    versions: call(getVersions),
  });
  if (issue.total_count > redmine.issues_limit) {
    toast.warning(
      i18n.t('Notification:overlimit', { limit: redmine.issues_limit })
    );
  }
  AutoloadManager.init();
}

export function* requestInitializeRankedKanban() {
  yield takeLatest(actions.initRankedKanbanApp, mask(initializeRankedKanban));
}

export function* initializeBacklog() {
  yield call(getWorkflows);
  const { redmine } = yield select();
  const versions = yield call(getVersions);
  let query = new QueryList({ parent_id: { operator: '!*' } });
  if (!redmine.visible_subproject) {
    query = query.add({ subproject_id: { operator: '!*' } });
  }
  const options = { 'sort_criteria[0]': redmine.sort_column, };
  const requestsVersionIssues = versions.map((version) => {
    const querystring = query.add({ fixed_version_id: { operator: '=', values: [version.id] }, });
    return call(getIssuesWithPaging, { query: querystring, options });
  });
  const requestBacklogIssues = call(getIssuesWithPaging, {
    query: query.add({
      fixed_version_id: { operator: '!*' },
      status_id: { operator: 'o' },
    }),
    options,
  });
  yield all(
    _.compact([
      ...requestsVersionIssues,
      requestBacklogIssues,
      redmine.avatar_enabled && call(getMemberships),
    ])
  );
}

export function* requestInitializeBacklog() {
  yield takeLatest(actions.initBacklogApp, mask(initializeBacklog));
}

const Handlers = [
  requestInitializeAssignedKanban,
  requestInitializeBacklog,
  requestInitializeRankedKanban,
];

export default Handlers;
