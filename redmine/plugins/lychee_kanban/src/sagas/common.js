import { call, put } from 'redux-saga/effects';
import * as actions from '@/actions';

export const mask = (saga) => function* loading(...props) {
  try {
    yield put(actions.LoadingPanel.show());
    const response = yield call(saga, ...props);
    yield put(actions.LoadingPanel.hide());
    return response;
  } catch (error) {
    yield put(actions.LoadingPanel.hide());
    throw error;
  }
};
