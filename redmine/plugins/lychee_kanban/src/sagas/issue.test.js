import { call, put, takeEvery, takeLatest, takeLeading, select } from 'redux-saga/effects';
import * as reduxForm from 'redux-form';
import moment from 'moment';

import * as Redmine from '@/api/redmine';
import * as actions from '@/actions';
import * as formTypes from '@/constants/formTypes';

import { mask } from './common';
import * as issueSaga from './issue';
import { getMemberships } from './membership';
import { AutoloadManager } from '@/autoload_manager';

describe('issueSaga', () => {
  let saga;
  beforeEach(() => { AutoloadManager.setFunc(() => 1); });
  describe('getIssues', () => {
    beforeEach(() => {
      saga = issueSaga.getIssues({ query: '' });
    });

    it('receives fetch request and get data', () => {
      expect(saga.next({ query: '' }).value).toEqual(
        call(Redmine.Issues.index, { query: '' })
      );
      expect(saga.next({ issues: [] }).value).toEqual(
        put(actions.Issue.receiveIssues([]))
      );
    });
  });

  describe('getIssue', () => {
    beforeEach(() => {
      saga = issueSaga.getIssue({ payload: 99 });
    });

    it('receives an issue data', () => {
      const response = { issue: { id: 99 } };
      const visualParent = { id: 88 };
      const state = { redmine: { page: 'backlog' } };
      expect(saga.next().value).toEqual(select());
      expect(saga.next(state).value).toEqual(
        call(Redmine.Issues.show, 99, { context: 'backlog' })
      );
      expect(saga.next(response).value).toEqual(
        call(issueSaga.findVisualParent, { payload: response.issue })
      );
      expect(saga.next(visualParent).value).toEqual(
        put(
          actions.Issue.replaceIssue({
            ...response.issue,
            visual_parent_id: visualParent.id,
          })
        )
      );
    });
  });

  describe('getIssues', () => {
    beforeEach(() => {
      saga = issueSaga.getIssues({ query: '' });
    });

    it('receives an issues data', () => {
      const response = { issues: [{ id: 50 }, { id: 51 }] };
      expect(saga.next().value).toEqual(call(Redmine.Issues.index, { query: '' }));
      expect(saga.next(response).value).toEqual(put(actions.Issue.receiveIssues(response.issues)));
    });
  });

  describe('requestGetRankedKanbanIssues', () => {
    beforeEach(() => {
      saga = issueSaga.requestGetRankedKanbanIssues();
    });

    it('calls getRankedKanbanIssues', () => {
      const ret = saga.next();
      const expected = takeLeading(
        actions.Issue.requestGetRankedKanbanIssues, issueSaga.getRankedKanbanIssues
      );
      expect(ret.value.payload.args.toString()).toEqual(expected.payload.args.toString());
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('getRankedKanbanIssues', () => {
    beforeEach(() => {
      saga = issueSaga.getRankedKanbanIssues();
    });

    it('calls getIssues', () => {
      expect(saga.next().value).toEqual(select());
      const queryState = { toURL: jest.fn() };
      const issues_limit = 10;
      const redmineState = { sort_column: 'sort_by_backlog', issues_limit };
      expect(
        saga.next({ query: queryState, redmine: redmineState }).value
      ).toEqual(call(issueSaga.refreshIssues, {}));
    });
  });

  describe('requestGetAssignedKanbanIssues', () => {
    beforeEach(() => {
      saga = issueSaga.requestGetAssignedKanbanIssues();
    });

    it('calls getAssignedKanbanIssues', () => {
      const ret = saga.next();
      const expected = takeLeading(
        actions.Issue.requestGetAssignedKanbanIssues, issueSaga.getAssignedKanbanIssues
      );
      expect(ret.value.payload.args.toString()).toEqual(expected.payload.args.toString());
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('getAssignedKanbanIssues', () => {
    beforeEach(() => {
      saga = issueSaga.getAssignedKanbanIssues();
    });

    it('calls getIssues with query', () => {
      expect(saga.next().value).toEqual(select());
      const queryState = { toURL: jest.fn() };
      const issues_limit = 10;
      const redmineState = { sort_column: 'sort_by_backlog', issues_limit };
      expect(
        saga.next({ query: queryState, redmine: redmineState }).value
      ).toEqual(call(issueSaga.refreshIssues, {}));
      expect(
        saga.next({ issues: [{}], journal_id: 1, issue_id: 1, sorted_ids: 'foo' }).value
      ).toEqual(call(getMemberships));
    });
  });

  describe('requestCreateIssue', () => {
    beforeEach(() => {
      saga = issueSaga.requestCreateIssue();
    });

    it('calls createIssue', () => {
      const ret = saga.next();
      const expected = takeLatest(actions.Issue.createIssue, mask(issueSaga.createIssue));
      expect(ret.value.payload.args.toString()).toEqual(expected.payload.args.toString());
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('requestCreateIssue', () => {
    beforeEach(() => {
      saga = issueSaga.requestCreateIssue();
    });

    it('calls createIssue', () => {
      const ret = saga.next();
      const expected = takeLatest(
        actions.Issue.createIssue,
        mask(issueSaga.createIssue)
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('createIssue', () => {
    const action = { payload: { subject: 'hogehoge' } };
    beforeEach(() => {
      saga = issueSaga.createIssue(action);
    });

    it('posts params and receives created issue', () => {
      const state = { redmine: { page: 'backlog' } };
      expect(saga.next().value).toEqual(select());
      expect(saga.next(state).value).toEqual(
        call(Redmine.Issues.create, action.payload, { context: 'backlog', attachments: {} })
      );
      const issue = { id: 99 };
      expect(saga.next({ issue }).value).toEqual(
        call(issueSaga.updateParents, issue)
      );
      expect(saga.next().value).toEqual(
        call(issueSaga.findVisualParent, { payload: issue })
      );
      const payload = { ...issue, visual_parent_id: 88 };
      expect(saga.next({ id: 88 }).value).toEqual(
        put(actions.Issue.receiveIssue(payload))
      );
    });
  });

  describe('requestUpdateIssue', () => {
    beforeEach(() => {
      saga = issueSaga.requestUpdateIssue();
    });

    it('calls updateIssue', () => {
      const ret = saga.next();
      const expected = takeEvery(
        actions.Issue.updateIssue,
        mask(issueSaga.updateIssue)
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('updateIssue', () => {
    const action = {
      payload: { id: 99, subject: 'hogehoge' },
      meta: {
        time_entry: {
          hours: 99,
          user_id: 99,
        },
      },
    };
    beforeEach(() => {
      saga = issueSaga.updateIssue(action);
    });

    it('receives fetch request and get data', () => {
      expect(saga.next().value).toEqual(select());
      const state = {
        redmine: { page: 'backlog' },
        issue: { find: jest.fn() },
      };
      expect(saga.next(state).value).toEqual(
        call(Redmine.Issues.update, action.payload.id, action.payload, {
          context: 'backlog',
          time_entry: { hours: 99, user_id: 99 },
          attachments: {},
        })
      );
      const response = { issue: action.issue };
      expect(saga.next(response).value).toEqual(
        put(
          actions.Issue.receiveTimeEntry({
            hours: 99,
            issue: action.payload,
            user: { id: 99, },
          })
        )
      );
      expect(saga.next().value).toEqual(
        call(issueSaga.updateParents, response.issue)
      );
      expect(saga.next().value).toEqual(
        call(issueSaga.updateDescendants, response.issue)
      );
      expect(saga.next().value).toEqual(
        call(issueSaga.noticeLadUpdate, action.payload, response.issue)
      );
      expect(saga.next(true).value).toEqual(
        call(issueSaga.findVisualParent, { palyload: response.issue })
      );
      expect(saga.next({ id: 99 }).value).toEqual(
        put(
          actions.Issue.replaceIssue({ ...action.issue, visual_parent_id: 99 })
        )
      );
      expect(saga.next().value).toEqual(put(actions.IssueForm.hideIssueForm()));
    });

    it('receives fetch request and raise error', () => {
      saga.next();
      expect(saga.throw().value).toEqual(
        put(actions.Notification.showRedmineErrors())
      );
      expect(saga.next().value).toEqual(put(actions.Issue.updateIssue()));
    });
  });

  describe('requestSortIssues', () => {
    beforeEach(() => {
      saga = issueSaga.requestSortIssues();
    });

    it('calls sortIssues', () => {
      const ret = saga.next();
      const expected = takeEvery(
        actions.Issue.sortIssues,
        mask(issueSaga.sortIssues)
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('sortIssues', () => {
    it('receives fetch request and dispatch RECEIVE_ISSUE_SORTED', () => {
      const action = { payload: { id: 0, issue_index: -1 } };
      saga = issueSaga.sortIssues(action);
      expect(saga.next().value).toEqual(select());
      const before = { subject: 'before' };
      const state = {
        redmine: { sort_column: 'sort_by_backlog', page: 'backlog' },
        issue: { find: jest.fn(() => before) },
      };
      expect(saga.next(state).value).toEqual(
        call(Redmine.Issues.update, 0, action.payload, { context: 'backlog' })
      );
      const response = { issue: { id: 0, issue_index: -1 } };
      expect(saga.next(response).value).toEqual(
        put(
          actions.Issue.receiveIssueSorted(response.issue, {
            sort_column: 'sort_by_backlog',
            issue_index: -1,
            id: 0,
            before,
          })
        )
      );
    });

    it('receives fetch request and raise error', () => {
      const action = { payload: { id: 1 } };
      saga = issueSaga.sortIssues(action);
      saga.next();
      expect(saga.throw().value).toEqual(
        put(actions.Notification.showRedmineErrors())
      );
      expect(saga.next().value).toEqual(put(actions.Issue.updateIssue()));
    });
  });

  describe('requestCreateTimeEntry', () => {
    beforeEach(() => {
      saga = issueSaga.requestCreateTimeEntry();
    });

    it('calls createTimeEntry', () => {
      const ret = saga.next();
      const expected = takeLatest(
        actions.Issue.createTimeEntry,
        mask(issueSaga.createTimeEntry)
      );
      expect(ret.value.payload.args.toString()).toEqual(
        expected.payload.args.toString()
      );
      expect(ret.value.payload.fn).toEqual(expected.payload.fn);
    });
  });

  describe('createTimeEntry', () => {
    const action = {
      payload: {
        issue_id: '99',
        activity_id: '99',
        comments: 'sample comment',
        hours: '99',
        spent_on: moment('2000-01-01'),
      },
    };
    beforeAll(() => {
      saga = issueSaga.createTimeEntry(action);
    });

    it('creates time_entry', () => {
      expect(saga.next().value).toEqual(
        call(Redmine.TimeEntry.create, action.payload)
      );
      const response = { time_entry: {} };
      expect(saga.next(response).value).toEqual(
        put(actions.Issue.receiveTimeEntry(response.time_entry))
      );
      expect(saga.next().value).toEqual(
        put(reduxForm.reset(formTypes.ISSUE_TIME_ENTRY_FORM))
      );
    });

    it('raise error', () => {
      const ret = saga.throw();
      expect(ret.value).toEqual(put(actions.Notification.showRedmineErrors()));
      expect(saga.next().value).toEqual(put(actions.Issue.createTimeEntry()));
    });
  });
});
