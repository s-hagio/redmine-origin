import { call, put } from 'redux-saga/effects';

import * as Redmine from '@/api/redmine';
import * as actions from '@/actions';

export function* getWorkflows() {
  try {
    const { workflows } = yield call(Redmine.Workflows.index);
    yield put(actions.Workflow.receiveWorkflows(workflows));
  } catch (error) {
    yield put(actions.Workflow.receiveWorkflows(error));
  }
}
