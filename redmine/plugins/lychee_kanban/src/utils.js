import { AUTOCOMPLETE_FOR_MENTION_PATH } from '@/constants/endpoints';
import { REDMINE_VERSION } from '@/constants/redmine';

export const getScrollingElement = () => {
  if ('scrollingElement' in document) {
    return document.scrollingElement;
  }
  if (navigator.userAgent.indexOf('WebKit') > 0) {
    return document.body;
  }
  return document.documentElement;
};

export const stopPropagation = (func) => (event) => {
  const { target } = event;
  event.stopPropagation();
  if (target.tagName === 'A') {
    target.target = '_blank';
  }
  if (func) {
    func(event);
  }
};

export const getIssueFormPosition = () => {
  const offset = 360; // IssueForm の表示における左上端と中心の位置調整
  const centerX = (document.documentElement.clientWidth / 2) - offset;
  const centerY = (document.documentElement.clientHeight / 2) - offset;
  return { x: centerX, y: centerY };
};

export const updateDataSourceUsers = (project_id, issue_id) => {
  if (REDMINE_VERSION < '5.0') { return; }
  const { rm } = window;
  const object_id = issue_id ? `object_id=${issue_id}` : '';
  rm.AutoComplete.dataSources.users = `${AUTOCOMPLETE_FOR_MENTION_PATH}?object_type=issue&project_id=${project_id}&${object_id}`;
};
