class _AutoloadManager {
  constructor() {
    this.MAX_INTERVAL = 9999;
    this.DEFAULT_INTERVAL = 3;
    this.func = null;
    this.intervalId = null;
    this.initialized = false;
  }

  setFunc(func) {
    this.func = func;
  }

  start() {
    this.stop();
    this.intervalId = setInterval(() => {
      if (this.initialized) {
        this.func();
      }
    }, this.intervalSeconds * 1000);
  }

  stop() {
    if (this.intervalId) {
      clearInterval(this.intervalId);
      this.intervalId = null;
    }
  }

  restart() {
    if (!this.initialized) { return; }
    this.stop();
    this.func();
    this.start();
  }

  init() {
    this.initialized = true;
    this.intervalSeconds = this.getInterval();
    this.start();
  }

  getInterval() {
    let interval = this.DEFAULT_INTERVAL;
    const content = document.getElementById('target-content');
    const redmineState = JSON.parse(content.getAttribute('data-redmine-state'));
    const seconds = parseInt(redmineState.auto_reload_seconds, 10);
    if (seconds && seconds > 0 && seconds <= this.MAX_INTERVAL) { interval = seconds; }
    return interval;
  }
}

export const AutoloadManager = new _AutoloadManager();
