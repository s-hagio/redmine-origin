import Cookies from 'universal-cookie';
import * as _ from 'lodash';

const cookies = new Cookies();
const ENABLED_FEATURES = cookies.get('dark') || [];

// eslint-disable-next-line max-len
export const isEnabled = (feature) => !_.isEmpty(_.intersection(ENABLED_FEATURES, _.flatten([feature])));

export const featureSelector = (feature) => (darkFeature, currentFeature) => {
  if (isEnabled(feature)) {
    return darkFeature;
  }
  return currentFeature;
};

const DarkFeature = ({ children = null, feature = '', hide = false }) => {
  const enabled = isEnabled(feature);
  if ((enabled && !hide) || (!enabled && hide)) {
    return children;
  }
  return null;
};

export default DarkFeature;
