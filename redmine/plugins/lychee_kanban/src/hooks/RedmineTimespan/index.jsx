import { useCallback } from 'react';

const TIMESPAN_FORMAT = {
  DECIMAL: 'decimal',
  MINUTES: 'minutes',
};
// needed for jest tests
const defaultFormat = {
  timespan_format: TIMESPAN_FORMAT.DECIMAL,
};

const content = document.getElementById('target-content');
const data = content
  ? JSON.parse(content.getAttribute('data-redmine-state'))
  : defaultFormat;

// see lib/redmine/i18n.rb :: format_hours
const formatHours = (hours, format = TIMESPAN_FORMAT.DECIMAL, truncate = true) => {
  if (hours === '') return '';
  const numHours = Number(hours).toFixed(2);
  if (format === TIMESPAN_FORMAT.DECIMAL) return truncate ? parseFloat(numHours) : numHours;
  const h = Math.floor(numHours);
  const m = Math.round((numHours - h) * 60);
  const min = m < 10 ? `0${m}` : `${m}`;
  return m > 0 ? `${h}:${min}` : `${h}`;
};

export const useRedmineTimespan = () => {
  const format = useCallback(
    (hours, truncate = true) => formatHours(hours, data.timespan_format, truncate),
    []
  );
  return format;
};
