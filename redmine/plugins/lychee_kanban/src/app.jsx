import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import styled from 'styled-components';

import Toast from './components/Toast';
import AssignedKanban from './containers/assignedKanban';
import Backlog from './containers/backlog';
import RankedKanban from './containers/rankedKanban';
import KanbanSettings from './containers/kanbanSettings';

const KANBAN_TYPE = document
  .getElementById('target-content')
  .getAttribute('data-kanban-type');

const Styled = (Component) => styled(Component)`
  height: 100%;

  /***********
   * 共有CSS *
   ***********/
  .d-flex {
    display: flex !important;
  }

  .align-items {
    &-baseline {
      align-items: baseline !important;
    }

    &-end {
      align-items: flex-end !important;
    }
  }

  .flex {
    &-column {
      flex-direction: column !important;
    }

    &-grow {
      flex-grow: 1 !important;
    }
  }

  .text-wrap {
    white-space: normal !important;
  }

  .minw {
    min-width: 1px !important;
  }

  .w-100 {
    width: 100% !important;
  }

  ${[3, 5].map((v) => `
    .line-clamp-${v} {
      display: -webkit-box;
      -webkit-line-clamp: ${v};
      -webkit-box-orient: vertical;
      overflow: hidden;
    }
  `)}
`;

export const App = Styled((props) => {
  const { className } = props;

  const redirectTo = React.useMemo(() => () => {
    switch (KANBAN_TYPE) {
      case 'ranked':
        return <RankedKanban />;
      case 'assigned':
        return <AssignedKanban />;
      case 'settings':
        return <KanbanSettings />;
      default:
        return <RankedKanban />;
    }
  }, []);

  return (
    <div className={className}>
      <Router>
        <Switch>
          <Route path="*/assigned" component={AssignedKanban} />
          <Route path="*/backlog" component={Backlog} />
          <Route path="*/ranked" component={RankedKanban} />
          <Route path="*" component={redirectTo} />
        </Switch>
      </Router>
      <Toast />
    </div>
  );
});
