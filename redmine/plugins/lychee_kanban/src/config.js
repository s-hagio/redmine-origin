import { use } from 'i18next';
import { reactI18nextModule } from 'react-i18next';
import moment from 'moment';
import 'moment/min/locales';
import * as locales from '@/locales';

const content = document.getElementById('target-content');
const { locale } = JSON.parse(content.getAttribute('data-redmine-state'));

moment.locale(locale);

use(reactI18nextModule).init({
  lng: locale,
  fallbackLng: 'en',
  interpolation: {
    // react already safes from xss
    escapeValue: false,
  },
  resources: locales,
});
