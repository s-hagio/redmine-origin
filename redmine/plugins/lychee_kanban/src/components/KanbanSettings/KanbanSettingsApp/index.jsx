import React from 'react';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd';

import TrackerGroupList from '@/components/TrackerGroupList';

const KanbanSettingsApp = () => (
  <DndProvider backend={HTML5Backend}>
    <TrackerGroupList />
  </DndProvider>
);

export default KanbanSettingsApp;
