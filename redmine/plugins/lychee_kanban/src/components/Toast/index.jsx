import * as React from 'react';
import styled from 'styled-components';
import { ToastContainer, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import ICON_ERROR from '@/images/exclamation.png';
import ICON_INFO from '@/images/comment.png';
import ICON_SUCCESS from '@/images/true.png';
import ICON_WARNING from '@/images/warning.png';

export default styled((props) => (
  <ToastContainer
    position="top-center"
    autoClose={5000}
    hideProgressBar
    closeOnClick
    pauseOnHover
    transition={Slide}
    {...props}
  />
))`
  width: 100%;

  .Toastify__toast {
    color: #454545;
    background-color: #f2f2f2;
    border: 1px solid;
    border-radius: 3px;
    border-color: #b3b3b3;
    font-size: 13px;
    margin-bottom: 12px;
    padding: 6px 4px 6px 30px;
    min-height: 32px;

    button {
      display: none;
    }

    &--info {
      background: url(${ICON_INFO}) 8px 50% no-repeat;
      background-color: #dff0ff;
      border-color: #9faecf;
      color: #00385f;
    }

    &--success {
      background: url(${ICON_SUCCESS}) 8px 50% no-repeat;
      background-color: #dfffdf;
      border-color: #5ea400;
      color: #005f00;
    }

    &--warning {
      background: url(${ICON_WARNING}) 8px 50% no-repeat;
      background-color: #ffebc1;
      border-color: #ebad1a;
      color: #a6750c;
    }

    &--error {
      background: url(${ICON_ERROR}) 8px 50% no-repeat;
      background-color: #ffe3e3;
      border-color: #ec3d3d;
      color: #800;
    }
  }
`;
