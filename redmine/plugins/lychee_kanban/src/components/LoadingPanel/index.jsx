import React from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';

import style from './style.css';

const LoadingPanel = ({ isLoading, t }) => {
  if (isLoading) {
    return (
      <div className={style.loadingPanel}>
        <p>{t('loading')}</p>
      </div>
    );
  }

  return null;
};

LoadingPanel.defaultProps = { isLoading: false, };

LoadingPanel.propTypes = {
  isLoading: PropTypes.bool,
  t: PropTypes.func.isRequired,
};

export default withNamespaces('LoadingPanel')(LoadingPanel);
