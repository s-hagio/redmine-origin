import React from 'react';
import { shallow } from 'enzyme';

import LoadingPanel from './index';

describe('LoadingPanel', () => {
  it('show component only if isLoading is true', () => {
    let wrapper = shallow(<LoadingPanel isLoading />);
    expect(wrapper.text()).toBe('loading');

    wrapper = shallow(<LoadingPanel />);
    expect(wrapper.getElement()).toBeNull();
  });
});
