import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';

class CustomSortContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sortItems: props.items,
      swappedItems: {
        source: {},
        target: {},
      },
    };
  }

  // HACK: line違反を除去
  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(props) {
    this.setState({ sortItems: props.items, });
  }

  componentWillUnmount() {
    cancelAnimationFrame(this.requestedFrame);
  }

  drawFrame = () => {
    const nextState = update(this.state, this.pendingUpdateFn);
    this.setState(nextState);
    this.pendingUpdateFn = null;
    this.requestedFrame = null;
  };

  handleMoveItem = (src, target) => {
    const { sortItems } = this.state;
    if (this.preventSwap(src, target)) {
      const srcIndex = sortItems.findIndex((item) => item.id === src.id);
      const targetIndex = sortItems.findIndex((item) => item.id === target.id);

      this.scheduleUpdate({
        sortItems: {
          $splice: [
            [srcIndex, 1],
            [targetIndex, 0, src],
          ],
        },
        swappedItems: { $set: { source: src, target }, },
      });
    }
  };

  handleDropItem = (item) => {
    const { items, onSort } = this.props;
    const { sortItems } = this.state;
    const before = items.findIndex(({ id }) => id === item.id);
    const after = sortItems.findIndex(({ id }) => id === item.id);

    if (before !== after) {
      onSort(item, sortItems);
    }
  };

  scheduleUpdate(updateFn) {
    this.pendingUpdateFn = updateFn;

    if (!this.requestedFrame) {
      this.requestedFrame = requestAnimationFrame(this.drawFrame);
    }
  }

  preventSwap(source, target) {
    const { swappedItems } = this.state;
    if (source.id === swappedItems.source.id && target.id === swappedItems.target.id) {
      return false;
    }
    return true;
  }

  renderItem = (item) => {
    const { extProps, render } = this.props;
    return render(
      item,
      {
        onDrop: this.handleDropItem,
        onHover: this.handleMoveItem,
      },
      extProps
    );
  };

  render() {
    const { className, tag } = this.props;
    const { sortItems } = this.state;
    const Tag = tag;

    return <Tag className={className}>{sortItems.map(this.renderItem)}</Tag>;
  }
}

CustomSortContainer.defaultProps = {
  className: '',
  extProps: {},
  items: [],
  onSort: () => ({}),
  tag: 'div',
};

CustomSortContainer.propTypes = {
  className: PropTypes.string,
  extProps: PropTypes.object,
  items: PropTypes.array,
  onSort: PropTypes.func,
  render: PropTypes.func.isRequired,
  tag: PropTypes.string,
};

export default CustomSortContainer;
