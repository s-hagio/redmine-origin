import { Component } from 'react';
import PropTypes from 'prop-types';
import { DragLayer } from 'react-dnd';

const INTERVAL_RATE = 1000 / 60;
const SCROLL_SPEED = 5;
const THRESHOLD = 70;

class CustomDragScrollLayer extends Component {
  constructor() {
    super();

    this.state = { intervalId: null, };
  }

  // HACK: lint違反の修正
  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    const { isDragging, itemType, excepts } = nextProps;
    if (!excepts.includes(itemType)) {
      this.toggleScrollEvent(isDragging);
    }
  }

  handleHorizontalScroll() {
    const { currentPosition } = this.props;
    const { x: positionX } = currentPosition;
    const { left, width } = this.targetBoundingClientRect;

    const halfWidth = width * 0.5;
    const vec = (positionX - (left + halfWidth)) / halfWidth;
    const scala = Math.abs(vec);
    if (scala < 1 && scala >= THRESHOLD * 0.01) {
      const css = window.getComputedStyle(this.horizontalTarget);
      this.horizontalTarget.style.scrollBehavior = '';
      this.horizontalTarget.scrollLeft += SCROLL_SPEED * vec ** 3;
      this.horizontalTarget.style.scrollBehavior = css.scrollBehavior;
    }
  }

  handleVerticalScroll() {
    const { currentPosition } = this.props;
    const { y: positionY } = currentPosition;
    const { top, height } = this.targetBoundingClientRect;

    const halfHeight = height * 0.5;
    const vec = (positionY - (top + halfHeight)) / halfHeight;
    const scala = Math.abs(vec);
    if (scala < 1 && scala >= THRESHOLD * 0.01) {
      this.verticalTarget.scrollTop += SCROLL_SPEED * vec ** 3;
    }
  }

  handleDragScroll() {
    const { currentPosition } = this.props;
    if (currentPosition && this.isInRect) {
      this.handleHorizontalScroll();
      this.handleVerticalScroll();
    }
  }

  get targetBoundingClientRect() {
    const rect = {
      top: 0,
      left: 0,
      right: window.innerWidth,
      bottom: window.innerHeight,
      width: window.innerWidth,
      height: window.innerHeight,
    };

    const {
      horizontalScrollTarget,
      verticalScrollTarget,
    } = this.props;
    const horizontalTarget = document.getElementById(horizontalScrollTarget);
    if (horizontalTarget) {
      const hrect = horizontalTarget.getBoundingClientRect();
      rect.left = hrect.left;
      rect.width = hrect.width;
      rect.right = hrect.left + hrect.width;
    }

    const verticalTarget = document.getElementById(verticalScrollTarget);
    if (verticalTarget) {
      const vrect = verticalTarget.getBoundingClientRect();
      rect.top = vrect.top;
      rect.height = vrect.height;
      rect.bottom = vrect.top + vrect.height;
    }

    return rect;
  }

  get horizontalTarget() {
    const { horizontalScrollTarget } = this.props;
    return (
      document.getElementById(horizontalScrollTarget)
      || document.documentElement
    );
  }

  get verticalTarget() {
    const { verticalScrollTarget } = this.props;
    return (
      document.getElementById(verticalScrollTarget)
      || document.documentElement
    );
  }

  get isInRect() {
    const { currentPosition } = this.props;
    const { x, y } = currentPosition;
    const { top, bottom, left, right } = this.targetBoundingClientRect;
    const betweenHorizontal = x >= left && x <= right;
    const betweenVertical = y >= top && y <= bottom;
    return betweenVertical && betweenHorizontal;
  }

  toggleScrollEvent(isDragging) {
    const { intervalId } = this.state;
    if (isDragging && !intervalId) {
      this.setState({
        intervalId: setInterval(
          this.handleDragScroll.bind(this),
          INTERVAL_RATE
        ),
      });
      this.verticalTarget.style.scrollBehavior = 'auto';
      this.horizontalTarget.style.scrollBehavior = 'auto';
    } else if (!isDragging && !!intervalId) {
      clearInterval(intervalId);
      this.verticalTarget.style.scrollBehavior = '';
      this.horizontalTarget.style.scrollBehavior = '';
      this.setState({ intervalId: null, });
    }
  }

  render() {
    return null;
  }
}

CustomDragScrollLayer.defaultProps = {
  currentPosition: {
    x: 0,
    y: 0,
  },
  excepts: [],
  horizontalScrollTarget: null,
  itemType: '',
  verticalScrollTarget: null,
};

CustomDragScrollLayer.propTypes = {
  currentPosition: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
  }),
  excepts: PropTypes.array,
  horizontalScrollTarget: PropTypes.string,
  isDragging: PropTypes.bool.isRequired,
  itemType: PropTypes.string,
  verticalScrollTarget: PropTypes.string,
};

const collect = (monitor) => ({
  currentPosition: monitor.getClientOffset(),
  isDragging: monitor.isDragging(),
  itemType: monitor.getItemType(),
});

export default DragLayer(collect)(CustomDragScrollLayer);
