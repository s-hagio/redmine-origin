import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './style.css';

const Membership = ({ name, avatar, className }) => {
  if (avatar) {
    return (
      <img
        src={avatar}
        alt={name}
        title={name}
        className={classNames([className, styles.icon])}
      />
    );
  }

  return (
    <div title={name} className={classNames([className, styles.icon])}>
      {name[0]}
    </div>
  );
};

Membership.defaultProps = {
  className: '',
  avatar: '',
  name: '',
};

Membership.propTypes = {
  className: PropTypes.string,
  avatar: PropTypes.string,
  name: PropTypes.string,
};

export default Membership;
