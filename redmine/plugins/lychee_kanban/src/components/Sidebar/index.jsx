import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import * as _ from 'lodash';
import { FaPlus } from 'react-icons/fa';

import style from './style.css';

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { open: false };
    this.root = document.getElementById('target-content');
  }

  componentDidMount() {
    window.addEventListener(
      'scroll',
      _.throttle(this.handleContainer.bind(this), 10)
    );
    window.addEventListener(
      'resize',
      _.throttle(this.handleContainer.bind(this), 10)
    );
    this.handleContainer();
  }

  handleContainer() {
    if (!this.container) {
      return;
    }

    const top = 300 - (window.scrollY || window.pageYOffset);
    this.container.style.top = `${top < 0 ? 0 : top}px`;

    // NOTE: スクロールなし＋初回render時に$('.footer')が
    // 取得できずエラーを出すため、回避用に三項演算子を追加しています。
    const footerTop = document.getElementById('footer')
      ? document.getElementById('footer').getBoundingClientRect().top
      : document.documentElement.clientHeight;
    const bottom = document.documentElement.clientHeight - footerTop;
    this.container.style.bottom = `${bottom < 0 ? 0 : bottom}px`;
  }

  toggle() {
    const { open } = this.state;
    this.setState({ open: !open });
    this.root.classList.toggle('sidebar_open', !open);
  }

  render() {
    const { children, onAddIssue, title, issueCount } = this.props;
    const { open } = this.state;

    return (
      <div className={classnames(['sidebar', style.wrapper])}>
        <div
          className={classnames({ [style.toggle]: true, [style.open]: open })}
        >
          <div
            className={style.title}
            style={{ maxHeight: '140px' }} // writing-modeで縦書きにした場合、IEとedgeで高さが正常に取得されないため、高さを明示的に指定する
            onClick={this.toggle}
          >
            {`${title} (${issueCount})`}
          </div>
          {onAddIssue && (
            <div className={style.icon} onClick={onAddIssue}>
              <FaPlus size={16} />
            </div>
          )}
        </div>
        <div
          className={classnames({
            [style.container]: true,
            [style.open]: open,
          })}
          ref={(ref) => {
            this.container = ref;
          }}
        >
          {children}
        </div>
      </div>
    );
  }
}

Sidebar.defaultProps = {
  onAddIssue: null,
  title: '',
  issueCount: 0,
};

Sidebar.propTypes = {
  children: PropTypes.node.isRequired,
  onAddIssue: PropTypes.func,
  title: PropTypes.node,
  issueCount: PropTypes.number,
};

export default Sidebar;
