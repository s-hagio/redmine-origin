import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, FormSection } from 'redux-form';
import { withNamespaces } from 'react-i18next';
import classNames from 'classnames';
import * as _ from 'lodash';

import { stopPropagation } from '@/utils';
import { ORIGIN_ISSUES_PATH, ORIGIN_ATTACHMENTS_PATH } from '@/constants/endpoints';
import { REDMINE_VERSION } from '@/constants/redmine';
import IssueTimeEntry from '@/containers/IssueTimeEntry';
import IssueDescriptionField from '@/containers/IssueDescriptionField';
import LycheeModal from '@/components/LycheeModal';
import CustomField from '@/components/LycheeForm/CustomField';
import {
  DateField,
  BooleanField,
  SelectField,
  TextField,
  TextAreaField,
  AttachmentField,
} from '@/components/LycheeForm/fields';
import style from '@/components/LycheeForm/style.css';
import {
  EstimateField,
  ESTIMATE_INDAYS,
  ESTIMATE_INHOURS,
  EstimateFieldContext,
} from './EstimateField';
import { JournalsList } from './Journals';
import { UploadedFiles } from './UploadedFiles';

const TriggerMouseUpEventOnIE = ({ target }) => {
  // for IE10
  if (target.fireEvent) {
    target.fireEvent('onmouseup');
  }
  // for IE11
  if (target.dispatchEvent) {
    const event = document.createEvent('MouseEvents');
    event.initEvent('mouseup', false, true);
    target.dispatchEvent(event);
  }
};
const FULL_WIDTH_VALUE = '1';

// TODO: React v16.3のContextAPIに移行する
export const ConsumerField = (
  { name, type, label, safe, editOnly, required, onDoubleClick, ...props },
  context
) => {
  const isRequired = required || context.requiredAttributes.includes(name);
  const isSafe = safe || context.safeAttributes.includes(name);
  const classes = classNames({
    [style.field]: true,
    [style[name]]: Boolean(style[name]),
    [style[type]]: Boolean(style[type]),
    [style.required]: isRequired,
  });
  const editable = context.editing && !context.disabledCoreFields.includes(name) && isSafe;
  const toggleClickable = !context.editing && !context.disabledCoreFields.includes(name) && isSafe;

  if (editOnly && !context.editing) {
    return null;
  }
  return (
    <div className={classes}>
      {label && <label htmlFor={name}>{label}</label>}
      <div className={style.item} onDoubleClick={toggleClickable ? onDoubleClick : null}>
        <Field
          name={name}
          type={type}
          editing={editable}
          required={isRequired}
          {...props}
        />
      </div>
    </div>
  );
};

ConsumerField.defaultProps = {
  label: null,
  required: false,
  safe: false,
  type: null,
  editOnly: false,
  onDoubleClick: null,
};

ConsumerField.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  required: PropTypes.bool,
  safe: PropTypes.bool,
  type: PropTypes.string,
  editOnly: PropTypes.bool,
  onDoubleClick: PropTypes.func,
};

ConsumerField.contextTypes = {
  disabledCoreFields: PropTypes.array,
  editing: PropTypes.bool,
  requiredAttributes: PropTypes.array,
  safeAttributes: PropTypes.array,
};

const DisplayDayField = (props) => {
  const { checked, onCheck } = props;

  const handleChange = React.useCallback((event) => {
    onCheck(event.target.checked ? ESTIMATE_INDAYS : ESTIMATE_INHOURS);
  }, [onCheck]);

  return (
    <input
      type="checkbox"
      checked={checked === ESTIMATE_INDAYS}
      onChange={handleChange}
    />
  );
};

class IssueForm extends Component {
  DONE_RATIOS = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100].map((ratio) => ({
    value: ratio,
    text: `${ratio}%`,
  }));

  constructor() {
    super();

    this.cancel = this.cancel.bind(this);
    this.toggle = this.toggle.bind(this);
    this.state = {
      editing: false,
      estimateIn: localStorage.Lychee_Kanban_displayDays,
      files: [],
      uploadedfiles: [],
    };
  }

  getChildContext() {
    const { editing } = this.state;
    const {
      disabledCoreFields,
      requiredAttributes,
      safeAttributes,
    } = this.props;

    return {
      editing,
      disabledCoreFields,
      requiredAttributes,
      safeAttributes,
    };
  }

  // HACK: lint違反を修正
  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    const { reset, attachments } = this.props;
    if (!nextProps.open) {
      reset();
      this.setState({ editing: false, files: [], uploadedfiles: attachments });
    } else if (!nextProps.id) {
      this.setState({ editing: true });
    }
  }

  get editable() {
    const { id, editable } = this.props;
    return id && editable;
  }

  get projectsWithoutPadding() {
    const { fieldOptions } = this.props;
    return fieldOptions.projects.map((project) => ({
      value: project.value,
      text: project.text.trim().replace('>> ', ''),
    }));
  }

  handleToggleEstimateIn = (value) => {
    this.setState({ estimateIn: value });
    localStorage.setItem('Lychee_Kanban_displayDays', value);
  };

  cancel() {
    const { onClose } = this.props;
    if (!this.editable) {
      onClose();
    } else {
      this.toggle();
    }
  }

  toggle() {
    const { editing } = this.state;
    const { reset } = this.props;
    reset();
    this.setState({ editing: !editing, files: [] });
  }

  render() {
    const { editing, estimateIn, files, uploadedfiles } = this.state;
    const {
      backlog,
      customFields,
      customFieldValues,
      dateFormat,
      fieldOptions,
      handleSubmit,
      id,
      lad,
      workingHours,
      lychee_remaining_estimate,
      onClose,
      onSubmit,
      open,
      position,
      safeAttributes,
      total_estimated_hours,
      t,
      journals,
      disabledCoreFields,
      maxFileSizeHuman,
      commentsSorting,
    } = this.props;

    const newerJournals = journals.filter((journal) => !!journal.notes).slice(-3);
    const notes = commentsSorting === 'asc' ? newerJournals : newerJournals.reverse();
    const cfWide = customFields.filter((field) => field.is_full_width_layout === FULL_WIDTH_VALUE);
    const cfNarrow = customFields.filter((field) => !cfWide.includes(field));
    const leftCoreFields = [];
    const rightCoreFields = [];

    const handleKeyDown = (e) => {
      if (e.key === 'Enter' && (e.ctrlKey || e.metaKey)) {
        handleSubmit(onSubmit)();
      }
    };

    if (!disabledCoreFields.includes('status_id')) {
      leftCoreFields.push(
        <ConsumerField
          name="status_id"
          label={t('status')}
          hasBlank={false}
          component={SelectField}
          options={fieldOptions.issue_statuses}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (!disabledCoreFields.includes('priority_id')) {
      leftCoreFields.push(
        <ConsumerField
          name="priority_id"
          label={t('priority')}
          hasBlank={false}
          component={SelectField}
          options={fieldOptions.issue_priorities}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (!disabledCoreFields.includes('assigned_to_id')) {
      leftCoreFields.push(
        <ConsumerField
          name="assigned_to_id"
          label={t('assigned_to')}
          component={SelectField}
          options={fieldOptions.assignable_users}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (!disabledCoreFields.includes('category_id')) {
      leftCoreFields.push(
        <ConsumerField
          name="category_id"
          label={t('category')}
          component={SelectField}
          options={fieldOptions.issue_categories}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (!disabledCoreFields.includes('fixed_version_id')) {
      leftCoreFields.push(
        <ConsumerField
          name="fixed_version_id"
          label={t('target_version')}
          component={SelectField}
          options={fieldOptions.fixed_versions}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (!disabledCoreFields.includes('parent_issue_id')) {
      leftCoreFields.push(
        <ConsumerField
          name="parent_issue_id"
          label={t('parent')}
          component={TextField}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (!disabledCoreFields.includes('blocking')) {
      leftCoreFields.push(
        <FormSection
          name="lychee_issue_board_issue_option_attributes"
          className={style.section}
        >
          <ConsumerField
            name="blocking"
            label={t('blocking')}
            hasBlank={false}
            safe={safeAttributes.includes('lychee_issue_board_issue_option_attributes')}
            component={BooleanField}
            onDoubleClick={this.toggle}
          />
        </FormSection>
      );
    }
    if (backlog && !disabledCoreFields.includes('point')) {
      leftCoreFields.push(
        <FormSection
          name="lychee_issue_board_issue_option_attributes"
          className={style.section}
        >
          <ConsumerField
            name="point"
            safe={
              safeAttributes.includes('lychee_issue_board_issue_option_attributes')
            }
            label={t('points')}
            component={TextField}
            onDoubleClick={this.toggle}
          />
        </FormSection>
      );
    }

    if (!disabledCoreFields.includes('start_date')) {
      rightCoreFields.push(
        <ConsumerField
          name="start_date"
          label={t('start_date')}
          component={DateField}
          dateFormat={dateFormat}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (!disabledCoreFields.includes('due_date')) {
      rightCoreFields.push(
        <ConsumerField
          name="due_date"
          label={t('due_date')}
          component={DateField}
          dateFormat={dateFormat}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (lad && !disabledCoreFields.includes('actual_start_date')) {
      rightCoreFields.push(
        <ConsumerField
          name="actual_start_date"
          label={t('actual_start_date')}
          component={DateField}
          dateFormat={dateFormat}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (lad && !disabledCoreFields.includes('actual_due_date')) {
      rightCoreFields.push(
        <ConsumerField
          name="actual_due_date"
          label={t('actual_due_date')}
          component={DateField}
          dateFormat={dateFormat}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (!disabledCoreFields.includes('done_ratio')) {
      rightCoreFields.push(
        <ConsumerField
          name="done_ratio"
          label={t('done_ratio')}
          hasBlank={false}
          component={SelectField}
          options={this.DONE_RATIOS}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (!disabledCoreFields.includes('estimated_hours')) {
      rightCoreFields.push(
        <ConsumerField
          name="estimated_hours"
          label={t('estimated_hours')}
          component={EstimateField}
          total={total_estimated_hours}
          workingHours={workingHours}
          onDoubleClick={this.toggle}
        />
      );
    }
    if (lychee_remaining_estimate && !disabledCoreFields.includes('remaining_estimate')) {
      rightCoreFields.push(
        <ConsumerField
          name="remaining_estimate"
          label={t('remaining_estimate')}
          component={EstimateField}
          workingHours={workingHours}
          onDoubleClick={this.toggle}
        />
      );
    }
    const space_field = (
      <div className={style.field} />
    );

    const coreFields = [];
    _.times(Math.max(leftCoreFields.length, rightCoreFields.length), (index) => {
      if (leftCoreFields.length <= index) {
        coreFields.push(space_field);
      } else {
        coreFields.push(leftCoreFields[index]);
      }
      if (rightCoreFields.length <= index) {
        coreFields.push(space_field);
      } else {
        coreFields.push(rightCoreFields[index]);
      }
    });

    const addFile = (file) => {
      this.setState({ files: [...files, file] });
    };

    const delFile = (idx) => {
      files.splice(idx, 1);
      this.setState({ files: [...files] });
    };

    const hundleDeleteFile = (fileId) => {
      const { onDeleteFile } = this.props;
      if (window.confirm(t('confirm_delete'))) {
        onDeleteFile(fileId);
        this.setState({ uploadedfiles: uploadedfiles.filter((file) => file.id !== fileId) });
      }
    };

    return (
      <EstimateFieldContext.Provider value={estimateIn}>
        <LycheeModal
          cancel="a, input, select, textarea"
          position={position}
          onOverlayClicked={onClose}
          isVisible={open}
        >
          <div
            className={classNames({
              [style.container]: true,
              [style.editing]: editing,
            })}
          >
            <form onSubmit={handleSubmit(onSubmit)}>
              <header>
                <ConsumerField
                  name="tracker_id"
                  component={SelectField}
                  options={fieldOptions.trackers}
                  hasBlank={false}
                  render={(values) => (
                    <a href={`${ORIGIN_ISSUES_PATH}/${id}`} target="_blank" rel="noreferrer">
                      {`${values[0].text} #${id}`}
                    </a>
                  )}
                />
                <ConsumerField name="subject" component={TextField} />
                {this.editable && (editing ? (
                  <i className={style.cancel} onClick={this.toggle} />
                ) : (
                  <i className={style.edit} onClick={this.toggle} />
                ))}
              </header>
              <div className={style.main}>
                <div onScroll={TriggerMouseUpEventOnIE} className={style.body}>
                  <div className={style['project-row']}>
                    <ConsumerField
                      name="project_id"
                      label={t('project')}
                      hasBlank={false}
                      component={SelectField}
                      options={
                        editing
                          ? fieldOptions.projects
                          : this.projectsWithoutPadding
                      }
                    />
                    <label>
                      <ConsumerField
                        name="estimate_in"
                        checked={estimateIn}
                        safe
                        onCheck={this.handleToggleEstimateIn}
                        component={DisplayDayField}
                      />
                      {t('days_switch_button')}
                    </label>
                  </div>
                  <div className={style['item-row']}>
                    {coreFields.map((field) => field)}
                    <FormSection
                      name="custom_field_values"
                      className={style.section}
                    >
                      {cfNarrow.map((field) => (
                        <ConsumerField
                          key={field.id}
                          name={String(field.id)}
                          label={field.name}
                          safe={safeAttributes.includes('custom_field_values')}
                          required={field.is_required}
                          hasBlank={!field.default_value}
                          type={field.field_format}
                          onKeyDown={field.field_format === 'text' ? (e) => handleKeyDown(e) : false}
                          editStyle={field.format_store?.edit_tag_style}
                          options={field.possible_values}
                          multiple={field.multiple}
                          component={CustomField}
                          dateFormat={dateFormat}
                          formattedValue={_.find(customFieldValues,
                            { id: field.id })?.formatted_value}
                          placeholder={field.description}
                          render={() => {
                            const value = _.find(customFieldValues, { id: field.id, });
                            return (
                              <div
                                dangerouslySetInnerHTML={{ __html: value?.formatted_value }}
                                onClick={stopPropagation()}
                              />
                            );
                          }}
                          onDoubleClick={this.toggle}
                        />
                      ))}
                    </FormSection>
                    {!disabledCoreFields.includes('description') && (
                      <div className={style.description}>
                        <ConsumerField
                          name="description"
                          type="text"
                          label={t('description')}
                          component={IssueDescriptionField}
                          onDoubleClick={this.toggle}
                          onKeyDown={(e) => handleKeyDown(e)}
                        />
                      </div>
                    )}
                    <FormSection
                      name="custom_field_values"
                      className={style['wide-section']}
                    >
                      {cfWide.map((field) => (
                        <ConsumerField
                          key={field.id}
                          name={String(field.id)}
                          label={field.name}
                          safe={safeAttributes.includes('custom_field_values')}
                          required={field.is_required}
                          hasBlank={!field.default_value}
                          type={field.field_format}
                          onKeyDown={field.field_format === 'text' ? (e) => handleKeyDown(e) : false}
                          editStyle={field.format_store?.edit_tag_style}
                          options={field.possible_values}
                          multiple={field.multiple}
                          component={CustomField}
                          dateFormat={dateFormat}
                          formattedValue={_.find(customFieldValues,
                            { id: field.id })?.formatted_value}
                          placeholder={field.description}
                          render={() => {
                            const value = _.find(customFieldValues, { id: field.id, });
                            return (
                              <div
                                dangerouslySetInnerHTML={{ __html: value?.formatted_value }}
                                onClick={stopPropagation()}
                              />
                            );
                          }}
                          onDoubleClick={this.toggle}
                        />
                      ))}
                    </FormSection>
                  </div>

                  <div className={style.notes}>
                    {this.editable && (
                      <ConsumerField
                        name="notes"
                        label={t('notes')}
                        type="text"
                        component={TextAreaField}
                        editOnly
                        onKeyDown={(e) => handleKeyDown(e)}
                      />
                    )}
                  </div>
                  <div className={style.attachments}>
                    <label>{t('attachments')}</label>
                    {(REDMINE_VERSION >= 5.0 && uploadedfiles.length > 1) && (
                      <div className={style.wrapper_download_all_attachments}>
                        <a
                          className="icon-only icon-download"
                          href={`${ORIGIN_ATTACHMENTS_PATH}/issues/${id}/download`}
                          title={t('download_all_attachments')}
                        />
                      </div>
                    )}
                    <ul>
                      {uploadedfiles.map((attachment) => (
                        <UploadedFiles
                          id={attachment.id}
                          url={attachment.url}
                          filename={attachment.filename}
                          filesize={attachment.filesize}
                          download_url={attachment.download_url}
                          description={attachment.description}
                          author={attachment.author}
                          created_on={attachment.created_on}
                          hundleDeleteFile={hundleDeleteFile}
                        />
                      ))}
                    </ul>

                    {editing && (
                      <FormSection
                        name="attachments"
                        className={style.upload}
                      >
                        <Field
                          id="0"
                          name="0"
                          component={AttachmentField}
                          funcAddFile={addFile}
                          funcDelFile={delFile}
                          placeholder={t('file_description_placeholder')}
                        />
                        { files.map((file, index) => (
                          <Field
                            id={index + 1}
                            name={String(index + 1)}
                            component={AttachmentField}
                            funcAddFile={addFile}
                            funcDelFile={delFile}
                            placeholder={t('file_description_placeholder')}
                          />
                        ))}
                        {t('max_file_size', { value: maxFileSizeHuman, })}
                      </FormSection>
                    )}
                  </div>
                  {editing && (
                    <footer>
                      <button
                        type="button"
                        onClick={this.cancel}
                        className={style.cancel}
                      >
                        {t('Button:cancel')}
                      </button>
                      <button type="submit" className={style.save}>
                        {t('Button:save')}
                      </button>
                    </footer>
                  )}
                  <IssueTimeEntry />
                  {!!notes.length && (
                    <JournalsList
                      journals={notes}
                      t={t}
                    />
                  )}
                </div>
              </div>
            </form>
          </div>
        </LycheeModal>
      </EstimateFieldContext.Provider>
    );
  }
}

IssueForm.defaultProps = {
  backlog: false,
  customFields: [],
  customFieldValues: [],
  disabledCoreFields: [],
  editable: false,
  fieldOptions: {
    assignable_users: [],
    fixed_versions: [],
    issue_categories: [],
    issue_priorities: [],
    issue_statuses: [],
    projects: [],
    trackers: [],
  },
  id: null,
  lad: false,
  workingHours: 8,
  lychee_remaining_estimate: false,
  onClose: null,
  open: false,
  position: null,
  requiredAttributes: [],
  safeAttributes: [],
  total_estimated_hours: null,
  dateFormat: 'YYYY-MM-DD',
  journals: [],
  attachments: [],
};

IssueForm.propTypes = {
  backlog: PropTypes.bool,
  customFields: PropTypes.array,
  customFieldValues: PropTypes.array,
  disabledCoreFields: PropTypes.array,
  editable: PropTypes.bool,
  fieldOptions: PropTypes.shape({
    projects: PropTypes.array,
    trackers: PropTypes.array,
    issue_statuses: PropTypes.array,
    issue_priorities: PropTypes.array,
    issue_categories: PropTypes.array,
    fixed_versions: PropTypes.array,
    assignable_users: PropTypes.array,
  }),
  lad: PropTypes.bool,
  workingHours: PropTypes.number,
  lychee_remaining_estimate: PropTypes.bool,
  id: PropTypes.number,
  onClose: PropTypes.func,
  onSubmit: PropTypes.func.isRequired,
  onDeleteFile: PropTypes.func.isRequired,
  open: PropTypes.bool,
  position: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
  }),
  requiredAttributes: PropTypes.array,
  safeAttributes: PropTypes.array,
  total_estimated_hours: PropTypes.number,
  dateFormat: PropTypes.string,
  journals: PropTypes.array,
  attachments: PropTypes.array,
  // ReduxFrom Props
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
};

IssueForm.childContextTypes = {
  disabledCoreFields: PropTypes.array,
  editing: PropTypes.bool,
  requiredAttributes: PropTypes.array,
  safeAttributes: PropTypes.array,
};

export default withNamespaces(['IssueForm', 'Button'])(IssueForm);
