import React from 'react';
import { shallow } from 'enzyme';

import IssueForm, { ConsumerField } from './index';

describe('IssueForm', () => {
  const props = {
    id: 99,
    fieldOptions: {
      projects: [],
      fixed_versions: [{ id: 99, text: 'dummy' }],
      issue_categories: [{ id: 99, text: 'dummy' }],
      trackers: [{ id: 99, text: 'dummy' }],
    },
    requiredAttributes: ['tracker_id'],
    onSubmit: jest.fn(),
    handleSubmit: jest.fn(),
    reset: jest.fn(),
    editable: true,
  };

  it('has tracker and subject field in header', () => {
    const form = shallow(<IssueForm {...props} />);
    const header = form.find('header');
    const fields = header.find(ConsumerField);
    expect(fields).toHaveLength(2);

    const [tracker, subject] = fields;
    expect(tracker.props).toHaveProperty('name', 'tracker_id');
    expect(tracker.props).toHaveProperty(
      'options',
      props.fieldOptions.trackers
    );
    expect(subject.props).toHaveProperty('name', 'subject');
  });

  it('toggles state after click icon', () => {
    const form = shallow(<IssueForm {...props} />);
    const icon = form.find('header i');
    expect(form.state().editing).toBeFalsy();
    expect(form.find('footer').exists()).toBeFalsy();
    icon.simulate('click');
    expect(form.state().editing).toBeTruthy();
    expect(form.find('footer').exists()).toBeTruthy();
  });

  it('hides icon when create issue', () => {
    const form = shallow(<IssueForm {...props} id={null} />);
    const icon = form.find('header i');
    expect(icon.exists()).toBeFalsy();
  });

  it('hides icon when cannot edit issue', () => {
    const form = shallow(<IssueForm {...props} editable={false} />);
    const icon = form.find('header i');
    expect(icon.exists()).toBeFalsy();
  });

  it('hides footer when not editing', () => {
    const form = shallow(<IssueForm {...props} />);
    const icon = form.find('footer');
    expect(icon.exists()).toBeFalsy();
  });

  it('hides version and category field when options is empty', () => {
    let form = shallow(<IssueForm {...props} />);
    let versionField = form.find({ name: 'fixed_version_id' });
    let categoryField = form.find({ name: 'category_id' });
    expect(versionField.exists()).toBeTruthy();
    expect(categoryField.exists()).toBeTruthy();

    const options = {
      ...props.fieldOptions,
      fixed_versions: [],
      issue_categories: [],
    };
    form = shallow(<IssueForm {...props} fieldOptions={options} />);
    versionField = form.find({ name: 'fixed_version_id' });
    categoryField = form.find({ name: 'category_id' });
    expect(versionField.exists()).toBeTruthy();
    expect(categoryField.exists()).toBeTruthy();
  });

  it('shows lad fields when enabled "lad"', () => {
    const form = shallow(<IssueForm {...props} lad />);
    expect(form.find({ name: 'actual_start_date' }).exists()).toBeTruthy();
    expect(form.find({ name: 'actual_due_date' }).exists()).toBeTruthy();
  });

  it('shows estimate_in fields', () => {
    const form = shallow(<IssueForm {...props} />);
    expect(form.find({ name: 'estimate_in' }).exists()).toBeTruthy();
  });

  it('shows lychee_remaining_estimate fields when enabled "lychee_remaining_estimate"', () => {
    const form = shallow(<IssueForm {...props} lychee_remaining_estimate />);
    expect(form.find({ name: 'remaining_estimate' }).exists()).toBeTruthy();
  });

  it('check "start_date" and "due_date" formats', () => {
    const dateFormat = 'DD.MM.YYYY';
    const form = shallow(<IssueForm {...props} dateFormat={dateFormat} />);
    expect(form.find({ name: 'start_date' }).props()).toHaveProperty('dateFormat', dateFormat);
    expect(form.find({ name: 'due_date' }).props()).toHaveProperty('dateFormat', dateFormat);
  });

  it('shows "point" field when enabled "backlog"', () => {
    const form = shallow(<IssueForm {...props} backlog />);
    expect(form.find({ name: 'point' }).exists()).toBeTruthy();
  });

  it('shows custom fields when has "customFields"', () => {
    const customFields = [{ id: '99', name: 'dummy' }];
    const form = shallow(<IssueForm {...props} customFields={customFields} />);
    expect(form.find({ name: '99' }).exists()).toBeTruthy();
  });

  it('shows custom field values when has "customFields" and "customFieldValues"', () => {
    const customFields = [{ id: '99', name: 'dummy' }];
    const customFieldValues = [
      { id: '99', formatted_value: '<div>hoge</div>' },
    ];
    const form = shallow(
      <IssueForm
        {...props}
        customFields={customFields}
        customFieldValues={customFieldValues}
      />
    );
    const customField = form.find({ name: '99' });
    const { dangerouslySetInnerHTML: { __html }, } = customField.props().render().props;
    expect(__html).toBe('<div>hoge</div>');
  });

  it('shows notes fields', () => {
    const form = shallow(<IssueForm {...props} />);
    expect(form.find({ name: 'notes' }).exists()).toBeTruthy();
  });
});

describe('Create New Issue', () => {
  const props = {
    id: null,
    fieldOptions: {
      projects: [],
      fixed_versions: [],
      issue_categories: [],
      trackers: [],
    },
    requiredAttributes: ['tracker_id'],
    onSubmit: jest.fn(),
    handleSubmit: jest.fn(),
    reset: jest.fn(),
    editable: true,
  };

  it('shows notes fields', () => {
    const form = shallow(<IssueForm {...props} />);
    expect(form.find({ name: 'notes' }).exists()).toBeFalsy();
  });
});
