import React from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import ICON_DELETE from '@/images/delete.png';
import ICON_DOWNLOAD from '@/images/download.png';

export const UploadedFiles = withNamespaces(['IssueForm'])((props) => {
  const {
    id,
    url,
    filename,
    filesize,
    download_url,
    description,
    author,
    created_on,
    hundleDeleteFile,
  } = props;

  return (
    <li>
      <span className="icon icon-attachment">
        <a href={url} target="_blank" rel="noreferrer noopener">
          { filename }
        </a>
      </span>
      <span>
        (
        { filesize }
        )
      </span>
      <a href={download_url}>
        <span>
          <img src={ICON_DOWNLOAD} alt="download" />
        </span>
      </a>

      {description && (
        <span>
          { description }
        </span>
      )}

      {author && (
        <span>
          { author.name }
          ,
        </span>
      )}
      <span>
        { created_on }
      </span>
      <span
        onClick={() => hundleDeleteFile(id)}
      >
        <img src={ICON_DELETE} alt="delete" />
      </span>
    </li>
  );
});

UploadedFiles.propTypes = {
  id: PropTypes.number,
  url: PropTypes.string,
  filename: PropTypes.string,
  filesize: PropTypes.string,
  download_url: PropTypes.string,
  description: PropTypes.string,
  author: PropTypes.object,
  created_on: PropTypes.string,
  hundleDeleteFile: PropTypes.func.isRequired,
};
