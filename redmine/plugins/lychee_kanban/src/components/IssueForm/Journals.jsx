import React from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import style from '@/components/LycheeForm/style.css';

const Journals = withNamespaces(['IssueForm'])((props) => {
  const {
    formatted_notes, indice, note_url,
    relative_created_time, activity_url, is_private, user, t
  } = props;
  const linkString = (url, text) => `<a href=${url} rel="noreferrer noopener" target="_blank">${text}</a>`;

  return (
    <div name="journal_row" className={style.row}>
      {!!user.avatar && (
        <div
          className={style.gravatar}
          dangerouslySetInnerHTML={{
            __html: user.avatar
          }}
        />
      )}
      <div className={user.avatar ? style.content : style['content-full']}>
        <div name="update_info" className={style['update-info']}>
          <div
            dangerouslySetInnerHTML={{
              __html: t('updated_time_by', {
                user: linkString(user.url, user.name),
                age: linkString(activity_url, relative_created_time)
              })
            }}
          />
          {is_private && (
            <span className={style.private}>
              {t('is_private')}
            </span>
          )}
          <a href={note_url} rel="noreferrer noopener" target="_blank">{`#${indice}`}</a>
        </div>
        <div
          className="wiki"
          dangerouslySetInnerHTML={{ __html: formatted_notes }}
        />
      </div>
    </div>
  );
});

Journals.propTypes = {
  id: PropTypes.number,
  notes: PropTypes.string,
  formatted_notes: PropTypes.string,
  indice: PropTypes.number,
  note_url: PropTypes.string,
  formatted_created_time: PropTypes.string,
  relative_created_time: PropTypes.string,
  activity_url: PropTypes.string,
  details: PropTypes.array,
  is_replyable: PropTypes.bool,
  is_editable: PropTypes.bool,
  is_private: PropTypes.bool,
  user: PropTypes.object,
  t: PropTypes.func.isRequired,
};

export const JournalsList = withNamespaces(['IssueForm'])((field) => {
  const { journals, t } = field;

  return (
    <div name="journals" className={style.journals}>
      <div name="upper_space" className={style['upper-space']} />
      <label>
        <i className={style.icon} />
        {t('label_journal')}
      </label>
      {journals.map((journal) => (
        <Journals
          key={journal.id}
          id={journal.id}
          notes={journal.notes}
          formatted_notes={journal.formatted_notes}
          indice={journal.indice}
          note_url={journal.note_url}
          formatted_created_time={journal.formatted_created_time}
          relative_created_time={journal.relative_created_time}
          activity_url={journal.activity_url}
          details={journal.details}
          is_replyable={journal.is_replyable}
          is_editable={journal.is_editable}
          is_private={journal.is_private}
          user={journal.user}
          t={t}
        />
      ))}
    </div>
  );
});

JournalsList.propTypes = {
  journals: [],
};

JournalsList.defaultProps = {
  journals: PropTypes.array,
  t: PropTypes.func.isRequired,
};
