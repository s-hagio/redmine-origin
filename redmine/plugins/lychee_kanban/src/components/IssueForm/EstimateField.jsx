import React from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import styled from 'styled-components';
import * as _ from 'lodash';

import { useRedmineTimespan } from '@/hooks/RedmineTimespan';

export const ESTIMATE_INDAYS = 'indays';
export const ESTIMATE_INHOURS = 'inhours';

const Root = styled.div`
  display: flex;
  align-items: baseline;

  input {
    width: 100px;

    & + span {
      margin-left: 0.25rem;
    }
  }
`;

export const EstimateFieldContext = React.createContext(false);

const isValid = (val) => {
  const exist = !!val || val === 0;
  return exist && !_.isNaN(Number(val));
};

const toHours = (value, workingHours) => Number(value) * Number(workingHours);
const toDays = (value, workingHours) => Number(value) * (1 / Number(workingHours));

const EstimateInput = withNamespaces(['IssueForm'])((props) => {
  const { value: srcValue, onChange, t, workingHours } = props;
  const [value, setValue] = React.useState(srcValue);
  const estimateIn = React.useContext(EstimateFieldContext);

  const mount = React.useRef(false);
  React.useEffect(() => {
    if (estimateIn === ESTIMATE_INDAYS) {
      setValue((prev) => (isValid(prev) ? toDays(prev, workingHours) : prev));
    } else if (mount.current) {
      setValue((prev) => (isValid(prev) ? toHours(prev, workingHours) : prev));
    }
    mount.current = true;
  }, [estimateIn, workingHours]);

  const handleChange = React.useCallback((event) => {
    setValue(event.target.value);
    let val = Number(event.target.value);
    if (estimateIn === ESTIMATE_INDAYS) {
      val = toHours(val, workingHours);
    }

    if (!event.target.value) {
      onChange('');
    } else if (!_.isNaN(val)) {
      onChange(val);
    } else {
      onChange(event.target.value);
    }
  }, [estimateIn, workingHours, onChange]);

  const unit = React.useMemo(() => {
    if (estimateIn === ESTIMATE_INDAYS) {
      return t('days');
    }
    return t('hours');
  }, [estimateIn, t]);

  return (
    <Root>
      <input value={value} onChange={handleChange} />
      <span>{unit}</span>
    </Root>
  );
});

EstimateInput.propTypes = {
  t: PropTypes.func.isRequired,
  value: PropTypes.any.isRequired, // string or number
  onChange: PropTypes.func.isRequired,
  workingHours: PropTypes.number,
};

export const EstimateField = withNamespaces(['IssueForm'])((field) => {
  const formatTimespan = useRedmineTimespan();
  const estimateIn = React.useContext(EstimateFieldContext);
  const {
    t,
    input: { value: srcValue, onChange },
    total,
    editing,
    workingHours,
  } = field;

  if (editing) {
    return (
      <EstimateInput
        value={formatTimespan(srcValue)}
        onChange={onChange}
        workingHours={workingHours}
      />
    );
  }

  const hasValue = isValid(srcValue);
  const showTotal = _.isNumber(total) && !_.isEqual(srcValue, total);

  if (estimateIn === ESTIMATE_INDAYS) {
    const estimatedDays = Math.ceil(toDays(srcValue, workingHours) * 10) / 10;
    const show = hasValue && !_.isNaN(estimatedDays);

    return (
      // 予定工数、残工数 日数表示
      <Root>
        {show && `${t('render_estimated_days', { value: estimatedDays })} `}
        {showTotal && `(${t('render_total_estimated_days', { value: Math.ceil(toDays(total, workingHours) * 10) / 10 })})`}
      </Root>
    );
  }

  return (
    // 予定工数、残工数 時数表示
    <Root>
      {hasValue && `${t('render_estimated_hours', { value: formatTimespan(srcValue), })} `}
      {showTotal && `(${t('render_total_estimated_hours', { value: formatTimespan(total), })})`}
    </Root>
  );
});

EstimateField.propTypes = {
  t: PropTypes.func.isRequired,
  input: PropTypes.object.isRequired,
  total: PropTypes.number,
  editing: PropTypes.bool,
  workingHours: PropTypes.number,
};

EstimateField.defaultProps = {
  editing: false,
  total: null,
  workingHours: 8,
};
