import React from 'react';
import PropTypes from 'prop-types';
import { useDrop } from 'react-dnd';
import classNames from 'classnames';
import style from './style.css';
import Tracker from '@/components/Tracker';
import * as ItemTypes from '@/constants/itemTypes';

const TrackerGroup = (props) => {
  const { Group, setTrackerToGroup } = props;
  const { workflow, items } = Group;
  const onDrop = (item) => {
    const { tracker } = item;
    if (tracker.workflow === workflow) {
      return;
    }
    setTrackerToGroup(workflow, tracker);
  };
  const [{ isOver }, drop] = useDrop({
    accept: ItemTypes.TRACKER,
    drop: onDrop,
    collect: (monitor) => ({
      isOver: monitor.isOver(),
    }),
  });
  const backgroundColor = React.useMemo(() => {
    if (isOver) {
      return '#ffc';
    }
    return '#fff';
  }, [isOver]);
  return (
    <div
      className={classNames({
        [style.container]: true,
      })}
      style={{ ...style, backgroundColor }}
      ref={drop}
    >
      {items.map((t) => (<Tracker tracker={t} workflow={workflow} key={t.id} />))}
    </div>
  );
};

TrackerGroup.propTypes = {
  Group: PropTypes.object.isRequired,
  setTrackerToGroup: PropTypes.func.isRequired,
};

export default TrackerGroup;
