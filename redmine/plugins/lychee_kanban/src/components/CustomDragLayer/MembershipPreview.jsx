import React from 'react';
import PropTypes from 'prop-types';

import Membership from '@/components/Membership';
import style from './style.css';

const MembershipPreview = ({ offset, membership }) => {
  const { x, y } = offset;
  const itemStyle = { transform: `translate(${x}px, ${y}px)` };

  return (
    <div className={style.layer}>
      <div style={itemStyle}>
        <Membership
          name={membership.principal.name}
          avatar={membership.principal.avatar}
        />
      </div>
    </div>
  );
};

MembershipPreview.defaultProps = {
  offset: {
    x: 0,
    y: 0,
  },
  membership: null,
};

MembershipPreview.propTypes = {
  offset: PropTypes.shape({
    x: PropTypes.number,
    y: PropTypes.number,
  }),
  membership: PropTypes.object,
};

export default MembershipPreview;
