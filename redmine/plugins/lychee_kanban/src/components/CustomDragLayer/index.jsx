import React from 'react';
import PropTypes from 'prop-types';
import { DragLayer } from 'react-dnd';

import * as ItemTypes from '@/constants/itemTypes';
import MembershipPreview from './MembershipPreview';

const CustomDragLayer = ({ currentOffset, item, isDragging, itemType }) => {
  if (!isDragging) {
    return null;
  }

  const offset = currentOffset || {};
  switch (itemType) {
    // TODO: refactor
    // case ItemTypes.ISSUE:
    //   return <IssuePreview offset={offset} issue={item} width={item.issueWidth} />;
    case ItemTypes.MEMBERSHIP:
      return <MembershipPreview offset={offset} membership={item.membership} />;
    default:
      return null;
  }
};

CustomDragLayer.defaultProps = {
  currentOffset: {
    x: 0,
    y: 0,
  },
  item: null,
  itemType: null,
};

CustomDragLayer.propTypes = {
  currentOffset: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
  }),
  item: PropTypes.object,
  itemType: PropTypes.string,
  isDragging: PropTypes.bool.isRequired,
};

const collect = (monitor) => ({
  currentOffset: monitor.getSourceClientOffset(),
  item: monitor.getItem(),
  itemType: monitor.getItemType(),
  isDragging: monitor.isDragging(),
});

export default DragLayer(collect)(CustomDragLayer);
