import React from 'react';
import PropTypes from 'prop-types';

import Issue from '@/components/Issue';
import style from './style.css';

const IssuePreview = ({ offset, issue, width }) => {
  const { x, y } = offset;
  const itemStyle = {
    opacity: '0.5',
    transform: `translate(${x}px, ${y}px)`,
  };
  const widthStyle = { width: `${width}px` };

  return (
    <div className={style.layer}>
      <div style={itemStyle}>
        <div style={widthStyle}>
          {/* NOTE: バックログではtitleが与えられているが、かんばんでは与えられていない */}
          {/* TODO: 与えるpropsが共通になるよう整理 */}
          <Issue title={issue.subject} {...issue} />
        </div>
      </div>
    </div>
  );
};

IssuePreview.defaultProps = {
  offset: {
    x: 0,
    y: 0,
  },
  issue: null,
  width: 0,
};

IssuePreview.propTypes = {
  offset: PropTypes.shape({
    x: PropTypes.number,
    y: PropTypes.number,
  }),
  issue: PropTypes.object,
  width: PropTypes.number,
};

export default IssuePreview;
