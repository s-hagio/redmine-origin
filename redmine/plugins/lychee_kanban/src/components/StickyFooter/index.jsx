import * as React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ReactResizeDetector from 'react-resize-detector';

import StickyBar from '@/components/StickyBar';

const FOOTER_HEIGHT = 54;
const BORDER_WIDTH = 2;
const StyledStickyBar = styled(StickyBar)`
  top: ${({ top }) => `${top}px`};
`;

const StickyFooter = (props) => {
  const { targetTable: TargetTable, children, className } = props;
  const [innerHeight, setInnerHeight] = React.useState(FOOTER_HEIGHT);
  const [top, setTop] = React.useState(window.innerHeight - innerHeight);

  React.useEffect(() => {
    const handleResize = () => {
      setTop(window.innerHeight - innerHeight);
    };
    handleResize();
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [innerHeight]);

  return (
    <React.Fragment key="sticky-footer">
      <StyledStickyBar className={className} top={top}>
        {children}
        <ReactResizeDetector
          handleHeight
          onResize={(width, height) => setInnerHeight(height)}
        />
      </StyledStickyBar>
      <div
        style={{
          marginTop: -(innerHeight + BORDER_WIDTH),
          paddingBottom: innerHeight,
        }}
      >
        <TargetTable />
      </div>
    </React.Fragment>
  );
};

StickyFooter.propTypes = {
  children: PropTypes.node.isRequired,
  targetTable: PropTypes.func.isRequired,
  className: PropTypes.string,
};

StickyFooter.defaultProps = { className: '', };

export default StickyFooter;
