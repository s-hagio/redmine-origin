import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import OutsideClickHandler from 'react-outside-click-handler';

import { getScrollingElement } from '@/utils';
import ICON_DUPLICATE from '@/images/duplicate.png';
import Link from '@/components/Link';
import { AutoloadManager } from '@/autoload_manager';
import style from './style.css';

class SimpleIssueForm extends Component {
  constructor() {
    super();

    this.input = React.createRef();
    this.state = {
      open: false,
      subject: '',
      trackerId: null,
    };
  }

  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    const { trackers } = this.props;
    if (trackers !== nextProps.trackers) {
      this.setState({ trackerId: null });
      this.close();
    }
  }

  componentDidUpdate() {
    this.input.current?.focus?.();
  }

  close = () => {
    AutoloadManager.restart();
    this.setState({
      open: false,
      subject: '',
    });
  };

  open = () => {
    AutoloadManager.stop();
    const { trackers } = this.props;
    const { trackerId } = this.state;
    this.setState({
      open: true,
      trackerId: trackerId || trackers[0].id,
    });

    const rootEl = getScrollingElement();
    rootEl.scrollTop = rootEl.scrollHeight;
  };

  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };

  handleSubmit = (event) => {
    const { onSubmit } = this.props;
    const { subject, trackerId } = this.state;
    this.setState({ subject: '' });
    event.preventDefault();
    onSubmit({
      subject,
      tracker_id: trackerId,
    });
  };

  render() {
    const { open, trackerId, subject } = this.state;
    const { t, trackers } = this.props;

    if (open) {
      return (
        <OutsideClickHandler onOutsideClick={() => this.close()}>
          <form className={style.root} onSubmit={this.handleSubmit}>
            <div>
              <label>{`${t('SimpleIssueForm:tracker')}:`}</label>
              <select
                onChange={this.handleChange('trackerId')}
                defaultValue={trackerId}
              >
                {trackers.map((tracker) => (
                  <option key={tracker.id} value={tracker.id}>
                    {tracker.name}
                  </option>
                ))}
              </select>
              <a
                className={`icon-only icon-close close-icon ${style.close_button}`}
                onClick={this.close}
              />
            </div>
            <input
              type="text"
              value={subject}
              onChange={this.handleChange('subject')}
              ref={this.input}
            />
          </form>
        </OutsideClickHandler>
      );
    }

    return (
      <div className={style.root}>
        <Link
          label={t('SimpleIssueForm:label')}
          icon={ICON_DUPLICATE}
          onClick={this.open}
          className={style.link}
        />
      </div>
    );
  }
}

SimpleIssueForm.propTypes = {
  onSubmit: PropTypes.func,
  t: PropTypes.func.isRequired,
  trackers: PropTypes.array,
};

SimpleIssueForm.defaultProps = {
  onSubmit: () => {},
  trackers: [],
};

export default withNamespaces(['IssueForm', 'Button'])(SimpleIssueForm);
