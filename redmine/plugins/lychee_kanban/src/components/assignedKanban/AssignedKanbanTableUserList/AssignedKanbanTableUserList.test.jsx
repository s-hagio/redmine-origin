import React from 'react';
import { shallow } from 'enzyme';

import AssignedKanbanTableUserWorkload from '@/containers/assignedKanban/AssignedKanbanTableUserWorkload';
import Component from './AssignedKanbanTableUserList';

describe('AssignedKanbanTableUserList', () => {
  let component;
  const props = {
    memberships: [
      { id: 1, principal: { name: 'sample one' } },
      { id: 2, principal: { name: 'sample two' } },
      { id: 3, principal: { name: 'group' } },
    ],
  };

  beforeEach(() => {
    component = shallow(<Component {...props} />);
  });

  it('renders each member names', () => {
    expect(component.find('.item').at(0).text()).toContain('sample one');
    expect(component.find('.item').at(1).text()).toContain('sample two');
  });

  it('ables to resize each row heights', () => {
    let row = component.find('.item').at(0);
    expect(row.props().style).toHaveProperty('height', undefined); // eslint-disable-line no-undefined

    component.instance().resize([200]);
    component.update();
    row = component.find('.item').at(0);
    expect(row.props().style).toHaveProperty('height', 200);
  });

  it('visibles workload', () => {
    component = shallow(<Component {...props} showWorkload />);
    expect(component.find(AssignedKanbanTableUserWorkload)).toHaveLength(3);
  });
});
