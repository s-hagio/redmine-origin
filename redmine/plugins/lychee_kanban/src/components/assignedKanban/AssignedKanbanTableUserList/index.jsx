import AssignedKanbanTableUserList from './AssignedKanbanTableUserList';

export { default as AssignedKanbanTableUserWorkload } from './AssignedKanbanTableUserWorkload';
export default AssignedKanbanTableUserList;
