import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';

import AssignedKanbanTableUserWorkload from '@/containers/assignedKanban/AssignedKanbanTableUserWorkload';
import style from './style.css';

const renderAssignedTo = (name, avatar, enabled) => {
  if (enabled) {
    if (avatar) {
      return (
        <img src={avatar} alt={name} title={name} className={style.avatar} />
      );
    }
    if (name) {
      return (
        <div title={name} className={style.avatar}>
          {name[0]}
        </div>
      );
    }
  }
  return null;
};

class AssignedKanbanTableUserList extends Component {
  constructor() {
    super();

    this.state = { heights: {}, };
  }

  resize(rowHeights) {
    const { memberships } = this.props;
    const memberIds = memberships.map((member) => member.id);
    this.setState({ heights: _.zipObject(memberIds, rowHeights), });
  }

  render() {
    const { heights } = this.state;
    const { memberships, showWorkload, avatarEnabled } = this.props;
    return (
      <div className={style.list}>
        {memberships.map((member) => (
          <div
            key={member.id}
            className={style.item}
            style={{ height: heights[member.id] }}
          >
            <div className={style.user}>
              <div className={style.name}>{member.principal.name}</div>
              {renderAssignedTo(
                member.principal.name,
                member.principal.avatar,
                avatarEnabled
              )}
            </div>
            {showWorkload && (
              <AssignedKanbanTableUserWorkload member={member} />
            )}
          </div>
        ))}
      </div>
    );
  }
}

AssignedKanbanTableUserList.defaultProps = {
  memberships: [],
  showWorkload: false,
  avatarEnabled: false,
};

AssignedKanbanTableUserList.propTypes = {
  memberships: PropTypes.array,
  showWorkload: PropTypes.bool,
  avatarEnabled: PropTypes.bool,
};

export default AssignedKanbanTableUserList;
