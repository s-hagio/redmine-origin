import React from 'react';
import { shallow } from 'enzyme';

import Component from './AssignedKanbanTableUserWorkload';

describe('AssignedKanbanTableUserWorkload', () => {
  let component;
  const props = {
    estimate: 1,
    imcomplete: 2,
    remains: 3,
    spent: 4,
  };

  beforeEach(() => {
    component = shallow(<Component {...props} />);
  });

  it('renders each values', () => {
    const rows = component.find('.row');
    expect(rows.at(0).text()).toBe(
      'estimatedn_hours(hours: 1.00)<ReactTooltip />'
    );
    expect(rows.at(1).text()).toBe(
      'remainingn_hours(hours: 3.00)<ReactTooltip />'
    );
    expect(rows.at(2).text()).toBe(
      'actualn_hours(hours: 4.00)<ReactTooltip />'
    );
    expect(rows.at(3).text()).toBe('openn_hours(hours: 2.00)<ReactTooltip />');
  });

  it('colors remains value when negative', () => {
    component = shallow(<Component {...props} remains={-1} />);
    const row = component.find('.hours').at(1);
    expect(row.text()).toBe('n_hours(hours: -1.00)<ReactTooltip />');
    expect(row.props().style).toHaveProperty('color', '#F55');
  });
});
