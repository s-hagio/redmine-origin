import React from 'react';
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
import { withNamespaces } from 'react-i18next';
import * as _ from 'lodash';

import { useRedmineTimespan } from '@/hooks/RedmineTimespan';

import style from './style.css';

const WARNING_COLOR = '#F55';

const AssignedKanbanTableUserWorkload = (props) => {
  const { estimate, imcomplete, remains, spent, t } = props;
  const remainsStyle = {};
  if (remains < 0) {
    remainsStyle.color = WARNING_COLOR;
  }
  const formatTimespan = useRedmineTimespan();

  return (
    <div className={style.workload}>
      <div className={style.column}>
        <div className={style.row}>
          <div className={style.label}>{t('estimated')}</div>
          <div
            className={style.hours}
            data-tip={t('n_hours', {
              hours: formatTimespan(estimate, false),
            })}
          >
            {t('n_hours', { hours: formatTimespan(estimate, false) })}
            <ReactTooltip effect="float" type="light" place="top" />
          </div>
        </div>
        {!_.isNull(spent) && (
          <div className={style.row}>
            <div className={style.label}>{t('remaining')}</div>
            <div
              className={style.hours}
              style={remainsStyle}
              data-tip={t('n_hours', { hours: formatTimespan(remains, false) })}
            >
              {t('n_hours', { hours: formatTimespan(remains, false) })}
              <ReactTooltip effect="float" type="light" place="top" />
            </div>
          </div>
        )}
      </div>
      <div className={style.column}>
        {!_.isNull(spent) && (
          <div className={style.row}>
            <div className={style.label}>{t('actual')}</div>
            <div
              className={style.hours}
              data-tip={t('n_hours', { hours: formatTimespan(spent, false) })}
            >
              {t('n_hours', { hours: formatTimespan(spent, false) })}
              <ReactTooltip effect="float" type="light" place="top" />
            </div>
          </div>
        )}
        <div className={style.row}>
          <div className={style.label}>{t('open')}</div>
          <div
            className={style.hours}
            data-tip={t('n_hours', { hours: formatTimespan(imcomplete, false) })}
          >
            {t('n_hours', { hours: formatTimespan(imcomplete, false) })}
            <ReactTooltip effect="float" type="light" place="top" />
          </div>
        </div>
      </div>
    </div>
  );
};

AssignedKanbanTableUserWorkload.defaultProps = {
  estimate: 0,
  imcomplete: 0,
  remains: 0,
  spent: 0,
};

AssignedKanbanTableUserWorkload.propTypes = {
  estimate: PropTypes.number,
  imcomplete: PropTypes.number,
  remains: PropTypes.number,
  spent: PropTypes.number,
  t: PropTypes.func.isRequired,
};

export default withNamespaces('Workload')(AssignedKanbanTableUserWorkload);
