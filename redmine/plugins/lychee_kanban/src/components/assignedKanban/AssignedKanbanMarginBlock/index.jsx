import React from 'react';
import PropTypes from 'prop-types';
import ICON_ADD from '@/images/add.png';
import style from './style.css';

const AssignedKanbanMarginBlock = ({ onAddIssue }) => (
  <div className={style.body}>
    <div className={style.button} onClick={onAddIssue}>
      <img src={ICON_ADD} alt="add" />
    </div>
  </div>
);

AssignedKanbanMarginBlock.defaultProps = { onAddIssue: null, };

AssignedKanbanMarginBlock.propTypes = { onAddIssue: PropTypes.func, };

export default AssignedKanbanMarginBlock;
