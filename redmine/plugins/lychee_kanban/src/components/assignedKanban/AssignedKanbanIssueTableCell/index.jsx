import React from 'react';
import PropTypes from 'prop-types';
import Issue from '@/containers/assignedKanban/Issue';
import QuickNewIssueContainer from '@/containers/assignedKanban/QuickNewIssueContainer';
import AssignedKanbanMarginBlock from '@/components/assignedKanban/AssignedKanbanMarginBlock';
import style from './style.css';

const AssignedKanbanIssueTableCell = (props) => {
  const {
    issues,
    isFirstStatus,
    parentIssue,
    childFormOpen,
    onAddIssue,
    onCloseChildForm,
  } = props;
  return (
    <div className={style.container}>
      {issues.map((issue) => (
        <Issue
          key={issue.id}
          issue={issue}
          onAddChild={props.onAddChildGenerator({ parentIssue: issue })}
        />
      ))}
      {isFirstStatus && (
        <QuickNewIssueContainer
          parent={parentIssue}
          open={childFormOpen}
          onClose={onCloseChildForm}
          offsetTop={150}
        />
      )}
      <AssignedKanbanMarginBlock onAddIssue={onAddIssue} />
    </div>
  );
};

AssignedKanbanIssueTableCell.defaultProps = {
  issues: [],
  parentIssue: null,
};

AssignedKanbanIssueTableCell.propTypes = {
  issues: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  isFirstStatus: PropTypes.bool.isRequired,
  parentIssue: PropTypes.object,
  childFormOpen: PropTypes.bool.isRequired,
  onAddIssue: PropTypes.func.isRequired,
  onAddChildGenerator: PropTypes.func.isRequired,
  onCloseChildForm: PropTypes.func.isRequired,
};

export default AssignedKanbanIssueTableCell;
