import React from 'react';
import { shallow } from 'enzyme';

import { IssueList } from '@/models/issue';
import Component from './index';

describe('AssignedKanbanTableHeader', () => {
  it('renders a summary of the workloads.', () => {
    const props = {
      issue: new IssueList([
        {
          id: 1,
          subject: 'Issue 1',
          tracker: { id: 1 },
          assigned_to: 'User 1',
          estimated_hours: 1.0,
          spent_hours: 1.0,
        },
        {
          id: 2,
          subject: 'Issue 1',
          tracker: { id: 1 },
          assigned_to: 'User 1',
          estimated_hours: 2.0,
          spent_hours: 2.1,
        },
        {
          id: 3,
          subject: 'Issue 1',
          tracker: { id: 1 },
          assigned_to: 'User 1',
          estimated_hours: 3.0,
          spent_hours: 2.8,
        },
      ]),
      trackers: [{ id: 1, name: 'Bug' }],
    };

    const component = shallow(<Component {...props} />);
    expect(component.text()).toBe(
      'estimatedn_hours(hours: 6.00)actualn_hours(hours: 5.90)remainingn_hours(hours: 0.10)openn_hours(hours: 6.00)'
    );
  });

  it('does not count closed issues for "incomplete".', () => {
    const props = {
      issue: new IssueList([
        {
          id: 1,
          subject: 'Issue 1',
          tracker: { id: 1 },
          assigned_to: 'User 1',
          estimated_hours: 1.0,
          closed: true,
        },
        {
          id: 2,
          subject: 'Issue 2',
          tracker: { id: 1 },
          assigned_to: 'User 1',
          estimated_hours: 2.0,
          closed: false,
        },
        {
          id: 2,
          subject: 'Issue 2',
          tracker: { id: 1 },
          assigned_to: 'User 1',
          estimated_hours: 3.0,
          closed: false,
        },
      ]),
      trackers: [{ id: 1, name: 'Bug' }],
    };

    const component = shallow(<Component {...props} />);
    expect(component.text()).toBe(
      'estimatedn_hours(hours: 6.00)actualn_hours(hours: 0.00)remainingn_hours(hours: 6.00)openn_hours(hours: 5.00)'
    );
  });

  it('does not count an unassigned issue.', () => {
    const props = {
      issue: new IssueList([
        { id: 1, subject: 'Issue 1', estimated_hours: 1.0, tracker: { id: 1 } },
        {
          id: 2,
          subject: 'Issue 2',
          estimated_hours: 1.0,
          tracker: { id: 1 },
          assigned_to: 'User 1',
        },
      ]),
      trackers: [{ id: 1, name: 'Bug' }],
    };

    const component = shallow(<Component {...props} />);
    expect(component.text()).toBe(
      'estimatedn_hours(hours: 2.00)actualn_hours(hours: 0.00)remainingn_hours(hours: 2.00)openn_hours(hours: 2.00)'
    );
  });

  it('does not count issues belong to other trackers.', () => {
    const props = {
      issue: new IssueList([
        {
          id: 1,
          subject: 'Issue 1',
          estimated_hours: 1.0,
          tracker: { id: 1 },
          assigned_to: 'User 1',
        },
        {
          id: 2,
          subject: 'Issue 2',
          estimated_hours: 2.0,
          tracker: { id: 2 },
          assigned_to: 'User 1',
        },
        {
          id: 3,
          subject: 'Issue 3',
          estimated_hours: 3.0,
          tracker: { id: 2 },
          assigned_to: 'User 1',
        },
      ]),
      trackers: [{ id: 2, name: 'Feature' }],
    };

    const component = shallow(<Component {...props} />);
    expect(component.text()).toBe(
      'estimatedn_hours(hours: 5.00)actualn_hours(hours: 0.00)remainingn_hours(hours: 5.00)openn_hours(hours: 5.00)'
    );
  });
});
