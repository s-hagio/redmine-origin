import React from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import { withNamespaces } from 'react-i18next';
import style from './style.css';

import { useRedmineTimespan } from '@/hooks/RedmineTimespan';

// このWorkloadが対象とするissueを選択する
const target_issues = (issues, trackers) => {
  const tracker_ids = trackers.map((tracker) => tracker.id);
  return issues.filter(
    (issue) => issue.assigned_to && _.includes(tracker_ids, issue.tracker.id)
  );
};

// issuesから工数を計算する
const calc_hours = (issues) => {
  const hours = {
    estimated: 0,
    spent: 0,
    remaining: 0,
    incomplete: 0,
  };
  issues.forEach((issue) => {
    if (issue.estimated_hours) {
      hours.estimated += issue.estimated_hours;
      if (!issue.closed) {
        hours.incomplete += issue.estimated_hours;
      }
    }
    if (issue.spent_hours) {
      hours.spent += issue.spent_hours;
    }
  });
  hours.remaining = hours.estimated - hours.spent;
  return hours;
};

export const Workload = ({ issue, trackers, t }) => {
  const hours = calc_hours(target_issues(issue.items, trackers));
  const remainingStyle = hours.remaining < 0 ? { color: '#f66' } : {};

  const formatTimespan = useRedmineTimespan();

  return (
    <div className={style.workload}>
      <span>
        {t('estimated')}
        <span>{t('n_hours', { hours: formatTimespan(hours.estimated, false) })}</span>
      </span>
      <span>
        {t('actual')}
        <span>{t('n_hours', { hours: formatTimespan(hours.spent, false) })}</span>
      </span>
      <span>
        {t('remaining')}
        <span style={remainingStyle}>
          {t('n_hours', { hours: formatTimespan(hours.remaining, false) })}
        </span>
      </span>
      <span>
        {t('open')}
        <span>{t('n_hours', { hours: formatTimespan(hours.incomplete, false) })}</span>
      </span>
    </div>
  );
};

Workload.propTypes = {
  issue: PropTypes.PropTypes.object.isRequired,
  trackers: PropTypes.array.isRequired,
  t: PropTypes.func.isRequired,
};

export default withNamespaces('Workload')(Workload);
