import * as React from 'react';
import styled from 'styled-components';

import AssignedKanbanTableHeader from '@/containers/assignedKanban/AssignedKanbanTableHeader';
import AssignedKanbanTableUserList from '@/containers/assignedKanban/AssignedKanbanTableUserList';
import AssignedKanbanIssueTable from '@/containers/assignedKanban/AssignedKanbanIssueTable';
import StickyBar from '@/components/StickyBar';
import StickyFooter from '@/components/StickyFooter';
import SimpleIssueForm from '@/containers/SimpleIssueForm';
import ScrollHandler from '@/components/ScrollHandler';

const StyledStickyBar = styled(StickyBar)`
  top: 74px;
  z-index: 98;

  @media screen and (max-width: 899px) {
    top: 138px;

    :global(.task-kanban-is-fullscreen) {
      top: 74px;
    }
  }
`;

const StyledStickyFooter = styled(StickyFooter)`
  background-color: #fff;
  border: 1px solid #ccc;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  justify-content: center;
  z-index: 97;

  @media print {
    visibility: hidden;
  }
`;

const AssignedKanbanTable = () => {
  const userListRef = React.useRef(null);
  const [scrollLeft, setScrollLeft] = React.useState(0);

  const handleScroll = React.useCallback((value) => {
    setScrollLeft(value);
  }, []);

  const handleResize = React.useCallback((...args) => {
    if (userListRef.current) {
      userListRef.current.resize(...args);
    }
  }, [userListRef]);

  const TableContainingStickyFooterSpaces = React.useMemo(() => () => (
    <div style={{ display: 'flex' }}>
      <AssignedKanbanTableUserList ref={userListRef} />
      <AssignedKanbanIssueTable
        scrollTableHeader={handleScroll}
        resizeUserList={handleResize}
      />
    </div>
  ), [userListRef, handleScroll, handleResize]);

  return (
    <div className="assigned-kanban-table">
      <StyledStickyBar>
        <AssignedKanbanTableHeader scrollLeft={scrollLeft} />
      </StyledStickyBar>
      <StyledStickyFooter targetTable={TableContainingStickyFooterSpaces}>
        <SimpleIssueForm />
        <ScrollHandler
          target="assigned-kanban-issue-table-sync-scroll"
          widthOffset={180}
        />
      </StyledStickyFooter>
    </div>
  );
};

export default AssignedKanbanTable;
