import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import { List } from 'immutable';
import ReactResizeDetector from 'react-resize-detector';
import { withNamespaces } from 'react-i18next';

import style from './style.css';

const THROTTLE_RATE = 1000 / 60;
class AssignedKanbanTableHeader extends Component {
  constructor() {
    super();
    this.anchor = null;
    this.scroll = null;

    this.onResizeThrottle = _.throttle(this.onResize.bind(this), THROTTLE_RATE);
    this.state = { width: '100%', };
  }

  componentDidUpdate() {
    const { scrollLeft } = this.props;
    this.scroll.scrollLeft = scrollLeft;
  }

  onResize() {
    const rect = this.anchor.getBoundingClientRect();
    this.setState({ width: rect.width });
  }

  render() {
    const { issueStatuses, issueCounts, t } = this.props;
    const { width } = this.state;
    const containerStyle = {
      top: 0,
      width,
    };

    return (
      <div className={style.assigned_kanban_table_header}>
        <div ref={(ref) => { this.anchor = ref; }} />
        <div className={style.container} style={containerStyle}>
          <div className={style.row}>
            <div className={style.column}>{t('assignee_column')}</div>
          </div>
          <div
            className={style.status_container}
            ref={(ref) => { this.scroll = ref; }}
          >
            <div className={style.row}>
              {issueStatuses.map((issueStatus) => (
                <div className={style.status_column} key={issueStatus.id}>
                  {`${issueStatus.name}(${issueCounts[issueStatus.id]})`}
                </div>
              )).toJS()}
            </div>
          </div>
        </div>
        <ReactResizeDetector handleWidth onResize={this.onResizeThrottle} />
      </div>
    );
  }
}

AssignedKanbanTableHeader.defaultProps = { issueStatuses: new List(), };

AssignedKanbanTableHeader.propTypes = {
  issueStatuses: PropTypes.instanceOf(List),
  issueCounts: PropTypes.object.isRequired,
  scrollLeft: PropTypes.number.isRequired,
  t: PropTypes.func.isRequired,
};

export default withNamespaces('AssignedKanbanTableHeader')(
  AssignedKanbanTableHeader
);
