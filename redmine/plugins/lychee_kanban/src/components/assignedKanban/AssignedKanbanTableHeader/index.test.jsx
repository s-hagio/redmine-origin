import React from 'react';
import { shallow } from 'enzyme';
import { List } from 'immutable';

import Component from './index';

describe('AssignedKanbanTableHeader', () => {
  const props = {
    issueStatuses: new List([
      { id: 1, name: 'sample_status_1' },
      { id: 2, name: 'sample_status_2' },
    ]),
    issueCounts: { 1: 3, 2: 0 },
  };

  it('renders "担当者" and issue_statuses prop names.', () => {
    const component = shallow(<Component {...props} />);
    expect(component.text()).toContain(
      'assignee_columnsample_status_1(3)sample_status_2(0)'
    );
  });
});
