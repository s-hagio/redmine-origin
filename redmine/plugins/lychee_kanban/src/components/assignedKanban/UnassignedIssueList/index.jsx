import React from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';

import Issue from '@/containers/assignedKanban/Issue';
import IssueDropTarget from '@/containers/IssueDropTarget';
import Sidebar from '@/components/Sidebar';

const UnassignedIssueList = ({ onAddIssue, issues, t }) => (
  <Sidebar
    title={t('unassigned')}
    issueCount={issues.length}
    onAddIssue={onAddIssue}
  >
    <IssueDropTarget assigned_to_id="" style={{ height: '100%' }}>
      {issues.map((issue) => (
        <Issue key={issue.id} issue={issue} />
      ))}
    </IssueDropTarget>
  </Sidebar>
);

UnassignedIssueList.defaultProps = {
  issues: [],
  onAddIssue: null,
};

UnassignedIssueList.propTypes = {
  issues: PropTypes.array,
  onAddIssue: PropTypes.func,
  t: PropTypes.func.isRequired,
};

export default withNamespaces('UnassignedIssueList')(UnassignedIssueList);
