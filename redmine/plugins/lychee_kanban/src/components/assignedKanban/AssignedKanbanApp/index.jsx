import React from 'react';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd';
import { IssueProvider } from '@/components/Issue';
import IssueForm from '@/containers/IssueForm';
import LoadingPanel from '@/containers/LoadingPanel';
import NewIssue from '@/containers/NewIssue';
import WorkflowList from '@/containers/WorkflowList';
import UnassignedIssueList from '@/containers/assignedKanban/UnassignedIssueList';
import StickyBar from '@/components/StickyBar';
import AssignedKanbanTable from '../AssignedKanbanTable';
import style from './style.css';

const App = () => (
  <DndProvider backend={HTML5Backend}>
    <IssueProvider>
      <StickyBar className={style.sticky}>
        <div className={style.head}>
          <NewIssue className={style.new} />
        </div>
        <WorkflowList showWorkload />
      </StickyBar>
      <AssignedKanbanTable />
      <UnassignedIssueList />
      <IssueForm />
      <LoadingPanel />
    </IssueProvider>
  </DndProvider>
);

export default App;
