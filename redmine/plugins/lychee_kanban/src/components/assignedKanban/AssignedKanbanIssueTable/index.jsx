import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';

import AssignedKanbanIssueTableRow from '@/components/assignedKanban/AssignedKanbanIssueTableRow';
import CustomDragScrollLayer from '@/components/CustomDragScrollLayer';
import SyncScroll from '@/components/SyncScroll';
import style from './style.css';

const THROTTLE_RATE = 1000 / 60;
class AssignedKanbanIssueTable extends Component {
  constructor() {
    super();
    this.onResizeThrottle = _.throttle(
      this.resizeUserList.bind(this),
      THROTTLE_RATE
    );
    this.syncScroll = this.syncScroll.bind(this);
  }

  // 担当者欄の高さを追従させる
  resizeUserList() {
    const { resizeUserList } = this.props;
    const rowHeights = _.map(
      this.issueTable.childNodes,
      (node) => node.getBoundingClientRect().height
    );
    resizeUserList(rowHeights);
  }

  // チケット一覧がスクロールされたとき、ヘッダもスクロールさせる
  syncScroll(event) {
    const { scrollTableHeader } = this.props;
    scrollTableHeader(event.target.scrollLeft);
  }

  render() {
    const { className, issueStatuses, memberships } = this.props;

    return (
      <div
        id="assigned-kanban-issue-table"
        className={`${className} ${style.container}`}
        onScroll={this.syncScroll}
      >
        <SyncScroll hidebar>
          <div
            id="assigned-kanban-issue-table-sync-scroll"
            className={style.table}
            // prettier-ignore
            ref={(ref) => { this.issueTable = ref; }}
          >
            {memberships.map((membership) => (
              <AssignedKanbanIssueTableRow
                key={membership.id}
                membership={membership}
                issueStatuses={issueStatuses}
                onResizeThrottle={this.onResizeThrottle}
              />
            ))}
          </div>
          <CustomDragScrollLayer horizontalScrollTarget="assigned-kanban-issue-table" />
        </SyncScroll>
      </div>
    );
  }
}

AssignedKanbanIssueTable.defaultProps = {
  className: '',
  issueStatuses: [],
  memberships: [],
  scrollTableHeader() {},
  resizeUserList() {},
};

AssignedKanbanIssueTable.propTypes = {
  className: PropTypes.string,
  issueStatuses: PropTypes.array,
  memberships: PropTypes.array,
  scrollTableHeader: PropTypes.func,
  resizeUserList: PropTypes.func,
};

export default AssignedKanbanIssueTable;
