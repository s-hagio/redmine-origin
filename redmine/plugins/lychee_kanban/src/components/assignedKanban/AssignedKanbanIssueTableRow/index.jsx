import React from 'react';
import ReactResizeDetector from 'react-resize-detector';

import IssueDropTarget from '@/containers/IssueDropTarget';
import AssignedKanbanIssueTableCell from '@/containers/assignedKanban/AssignedKanbanIssueTableCell';

import style from './style.css';
import { AutoloadManager } from '@/autoload_manager';

export default class AssignedKanbanIssueTableRow extends React.Component {
  constructor() {
    super();
    this.state = {
      parentIssue: null,
      childFormOpen: false,
    };
  }

  onAddChildGenerator = ({ parentIssue }) => () => {
    AutoloadManager.stop();
    this.setState({ parentIssue, childFormOpen: true });
  };

  onCloseChildForm = () => {
    AutoloadManager.restart();
    this.setState({ parentIssue: null, childFormOpen: false });
  };

  render() {
    const { membership, issueStatuses, onResizeThrottle } = this.props;
    const { parentIssue, childFormOpen } = this.state;

    return (
      <div className={style.row}>
        {issueStatuses.map((issueStatus, index) => (
          <IssueDropTarget
            key={issueStatus.id}
            className={style.cell}
            assigned_to_id={membership.principal.id}
            status_id={issueStatus.id}
          >
            <AssignedKanbanIssueTableCell
              statusId={issueStatus.id}
              userId={membership.principal.id}
              isFirstStatus={index === 0}
              parentIssue={parentIssue}
              childFormOpen={childFormOpen}
              onAddChildGenerator={this.onAddChildGenerator}
              onCloseChildForm={this.onCloseChildForm}
            />
          </IssueDropTarget>
        ))}
        <ReactResizeDetector
          handleWidth
          handleHeight
          onResize={onResizeThrottle}
        />
      </div>
    );
  }
}
