import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import OutsideClickHandler from 'react-outside-click-handler';

import { getScrollingElement } from '@/utils';
import style from './style.css';

class QuickNewIssue extends Component {
  constructor(props) {
    super(props);

    const { parent } = props;
    this.state = {
      subject: '',
      trackerId: parent?.tracker?.id,
    };
  }

  // HACK: lint違反の修正
  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    const { trackers } = this.props;
    if (trackers !== nextProps.trackers) {
      this.close();
    }
    if (nextProps.parent) {
      this.setState({ trackerId: nextProps.parent.tracker.id });
    }
  }

  componentDidUpdate() {
    const { open } = this.props;
    if (open) {
      this.scrollIntoViewIfNeeded();
      // safariでは preventScroll が効かないため、スクロール完了後にfoucsを実行
      setTimeout(() => {
        this.input.focus({ preventScroll: true });
      }, 500);
    }
  }

  close = () => {
    const { onClose } = this.props;
    onClose();
    this.setState({ subject: '' });
  };

  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };

  handleSubmit = (event) => {
    const {
      onSubmit,
      parent: {
        id: parentId,
        fixed_version,
        start_date,
        due_date,
        assigned_to: { id: assigned_to_id },
        project: { id: project_id },
      },
    } = this.props;
    const { subject, trackerId } = this.state;
    this.setState({ subject: '' });
    event.preventDefault();
    onSubmit({
      subject,
      tracker_id: trackerId,
      parent_issue_id: parentId,
      fixed_version_id: fixed_version.id,
      assigned_to_id,
      start_date,
      due_date,
      project_id,
    });
  };

  preventScroll = (event) => {
    if (this.scrollIntoViewIfNeeded()) {
      event.preventDefault();
    }
  };

  scrollIntoViewIfNeeded() {
    const { offsetTop, offsetBottom } = this.props;
    const bounding = this.form.getBoundingClientRect();
    const distanceTop = Math.floor(bounding.top - offsetTop);
    const distanceBottom = Math.round(
      bounding.top + bounding.height + offsetBottom - window.innerHeight
    );
    const scrollingElement = getScrollingElement();
    if (distanceTop < 0) {
      scrollingElement.scrollTop += distanceTop;
      return true;
    }
    if (distanceBottom > 0) {
      scrollingElement.scrollTop += distanceBottom;
      return true;
    }
    return false;
  }

  render() {
    const { trackerId, subject } = this.state;
    const { open, t, trackers } = this.props;

    if (!open) {
      return null;
    }

    return (
      <OutsideClickHandler
        onOutsideClick={() => {
          this.close();
        }}
      >
        <form
          className={style.root}
          onSubmit={this.handleSubmit}
          ref={(el) => {
            this.form = el;
          }}
        >
          <div>
            <label>{`${t('SimpleIssueForm:tracker')}:`}</label>
            <select
              onChange={this.handleChange('trackerId')}
              defaultValue={trackerId}
            >
              {trackers.map((tracker) => (
                <option key={tracker.id} value={tracker.id}>
                  {tracker.name}
                </option>
              ))}
            </select>
            <a
              className={`icon-only icon-close close-icon ${style.close_button}`}
              onClick={this.close}
            />
          </div>
          <input
            type="text"
            value={subject}
            onChange={this.handleChange('subject')}
            onKeyPress={this.preventScroll}
            ref={(input) => {
              this.input = input;
            }}
          />
        </form>
      </OutsideClickHandler>
    );
  }
}
QuickNewIssue.propTypes = {
  parent: PropTypes.object,
  open: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  trackers: PropTypes.array,
  offsetTop: PropTypes.number,
  offsetBottom: PropTypes.number,
};

QuickNewIssue.defaultProps = {
  parent: {},
  trackers: [],
  offsetTop: 0,
  offsetBottom: 0,
};

export default withNamespaces(['IssueForm', 'Button'])(QuickNewIssue);
