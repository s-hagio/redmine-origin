import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Draggable from 'react-draggable';
import ReactResizeDetector from 'react-resize-detector';
import classNames from 'classnames';
import * as _ from 'lodash';

import { getScrollingElement } from '@/utils';
import style from './style.css';

class LycheeModal extends Component {
  constructor() {
    super();

    this.state = { initialized: true, };
  }

  // HACK: lint違反の修正
  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    if (!nextProps.isVisible) {
      document.getElementById('wrapper').style.minHeight = '';
      this.setState({ initialized: false });
    }
  }

  get defaultPosition() {
    const { position: propPosition } = this.props;
    const { x, y } = propPosition || {};
    const calcDefaultPosition = (pos, length, max, scrollMax, offset) => {
      let position = pos;
      if (!_.isNumber(position)) {
        position = max / 2 - length / 2;
      }
      if (length > scrollMax) {
        return 0;
      }
      if (length > max) {
        return offset;
      }
      if (position + length > max) {
        return max - length + offset;
      }
      return position + offset;
    };
    const { height, width } = this.dialog.getBoundingClientRect();
    const { innerHeight, innerWidth, pageXOffset, pageYOffset } = window;
    const { scrollHeight, scrollWidth } = getScrollingElement();
    return {
      x: calcDefaultPosition(x, width, innerWidth, scrollWidth, pageXOffset),
      y: calcDefaultPosition(y, height, innerHeight, scrollHeight, pageYOffset),
    };
  }

  get wrapperPosition() {
    if (this.anchor) {
      const { top, left } = this.anchor.getBoundingClientRect();
      return {
        x: -(left + window.pageXOffset),
        y: -(top + window.pageYOffset),
      };
    }
    return { x: 0, y: 0 };
  }

  initializePosition = () => {
    const wrapper = document.getElementById('wrapper');
    wrapper.style.minHeight = `${getScrollingElement().scrollHeight}px`;

    const { initialized } = this.state;
    if (!initialized) {
      this.draggable.setState({ ...this.defaultPosition });
      this.setState({ initialized: true });
    }
  };

  render() {
    const { initialized } = this.state;
    const { isVisible, cancel, children, onOverlayClicked } = this.props;
    const { x: wrapperLeft, y: wrapperTop } = this.wrapperPosition;

    const wrapperStyles = {
      top: wrapperTop,
      left: wrapperLeft,
    };

    // prettier-ignore
    return (
      <div>
        {isVisible && (
          <div className={style.wrapper} style={wrapperStyles}>
            <Draggable
              handle=".handle"
              cancel={cancel}
              bounds="#wrapper"
              ref={(ref) => { this.draggable = ref; }}
            >
              <div
                className={classNames({
                  handle: true,
                  [style.dialog]: true,
                  [style.initialized]: initialized,
                })}
                ref={(ref) => { this.dialog = ref; }}
              >
                {children}
                <ReactResizeDetector handleWidth handleHeight onResize={this.initializePosition} />
              </div>
            </Draggable>
            <div className={style.overlay} onClick={onOverlayClicked} />
          </div>
        )}
        <div className={style.anchor} ref={(ref) => { this.anchor = ref; }} />
      </div>
    );
  }
}

LycheeModal.propTypes = {
  cancel: PropTypes.string,
  children: PropTypes.node.isRequired,
  isVisible: PropTypes.bool,
  position: PropTypes.shape({
    // eslint-disable-next-line react/no-typos
    x: PropTypes.number.isRequried,
    // eslint-disable-next-line react/no-typos
    y: PropTypes.number.isRequried,
  }),
  onOverlayClicked: PropTypes.func.isRequired,
};

LycheeModal.defaultProps = {
  cancel: null,
  isVisible: false,
  position: {},
};

export default LycheeModal;
