import React from 'react';
import { shallow } from 'enzyme';
import Draggable from 'react-draggable';

import LycheeModal from './index';

describe('LycheeModal', () => {
  const props = {
    children: <div>children</div>,
    isVisible: true,
    position: { x: 0, y: 0 },
    onOverlayClicked: jest.fn(),
  };

  it('shows children when isVisible is true', () => {
    const lycheeModal = shallow(<LycheeModal {...props} />);
    expect(lycheeModal.find('.handle').text()).toEqual(
      'children<ResizeDetector />'
    );
  });

  it('do not shows modal when isVisible is false', () => {
    const lycheeModal = shallow(<LycheeModal {...props} isVisible={false} />);
    expect(lycheeModal.find(Draggable)).toHaveLength(0);
  });

  it('calls onOverlayClicked props when clicked overlay', () => {
    const lycheeModal = shallow(<LycheeModal {...props} />);
    lycheeModal.find({ onClick: props.onOverlayClicked }).simulate('click');
    expect(props.onOverlayClicked).toBeCalled();
  });
});
