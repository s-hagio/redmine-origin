import React from 'react';
import { useDrop } from 'react-dnd';
import TrackerGroup from '@/components/TrackerGroup';
import NewTrackerGroup from '@/components/NewTrackerGroup';
import * as ItemTypes from '@/constants/itemTypes';

const content = document.getElementById('target-content');
const CurrentTrackers = JSON.parse(content.getAttribute('data-trackers'));
const ButtonSave = content.getAttribute('data-button-save');

class Tracker {
  constructor(tracker) {
    this.tracker = tracker;
  }

  get id() { return this.tracker.id; }

  get name() { return this.tracker.name; }

  get workflow() { return this.tracker.workflow; }

  set workflow(v) { this.tracker.workflow = v; }

  get color() { return this.tracker.color; }

  get input_name() {
    const result = `tracker_options[${this.id}][workflow]`;
    return result;
  }
}

const TrackerGroupList = () => {
  const TrackerGroups = [];
  CurrentTrackers.forEach((tracker) => {
    const index = TrackerGroups.findIndex((i) => i.workflow === tracker.workflow);
    if (index >= 0) {
      TrackerGroups[index].items.push(new Tracker(tracker));
    } else {
      TrackerGroups.push({ workflow: tracker.workflow, items: [new Tracker(tracker)] });
    }
  });
  const [list, setList] = React.useState(TrackerGroups);
  const setTrackerToGroup = (workflow, tracker) => {
    // 移動元から削除
    const oldGroup = list.find((i) => i.workflow === tracker.workflow);
    if (oldGroup) {
      const oldItems = oldGroup.items;
      const index = oldItems.findIndex((i) => i.name === tracker.name);
      if (index >= 0) {
        oldItems.splice(index, 1);
      }
    }
    // 移動先へ追加
    const newGroup = list.find((i) => i.workflow === workflow);
    const newItems = newGroup.items;
    // const newTracker = new Tracker(...tracker);
    const newTracker = new Tracker({
      id: tracker.id,
      name: tracker.name,
      color: tracker.color,
    });
    newTracker.workflow = workflow;
    newItems.push(newTracker);
    // 空になったグループがあれば削除
    const empty = list.filter((i) => i.items.length === 0);
    if (empty.length > 0) {
      const emptyIndex = list.indexOf(empty[0]);
      list.splice(emptyIndex, 1);
    }
    setList([...list]);
  };
  const addNewTrackerGroup = (item) => {
    const { tracker } = item;
    let max = 0;
    list.forEach((i) => {
      if (i.workflow > max) { max = i.workflow; }
    });
    max += 1;
    list.push({ workflow: max, items: [] });
    setTrackerToGroup(max, tracker);
  };
  const onDrop = (tracker, monitor) => {
    if (!monitor.canDrop()) { return; }
    addNewTrackerGroup(tracker);
  };
  const [{ isOver }, drop] = useDrop({
    accept: ItemTypes.TRACKER,
    drop: onDrop,
    collect: (monitor) => ({
      isOver: monitor.isOver({ shallow: true }),
    }),
  });
  return (
    <div ref={drop} style={{ height: '100%' }}>
      {list.map((t) => (
        <TrackerGroup Group={t} key={t.workflow} setTrackerToGroup={setTrackerToGroup} />)
      )}
      <NewTrackerGroup addNewTrackerGroup={addNewTrackerGroup} isVisible={isOver} />
      <input type="submit" value={ButtonSave} />
    </div>
  );
};

export default TrackerGroupList;
