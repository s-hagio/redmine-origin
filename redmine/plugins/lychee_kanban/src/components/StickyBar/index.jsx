import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Stickyfill from 'stickyfilljs';
import ReactResizeDetector from 'react-resize-detector';

import styles from './style.css';

class StickyBar extends Component {
  componentDidMount() {
    this.sticky = new Stickyfill.Sticky(this.wrapper);
  }

  componentWillUnmount() {
    this.sticky.remove();
  }

  handleResize = () => {
    this.sticky.refresh();
  };

  render() {
    const { children, className, style } = this.props;
    return (
      <div
        className={classNames([styles.root, className])}
        style={style}
        ref={(ref) => {
          this.wrapper = ref;
        }}
      >
        {children}
        <ReactResizeDetector
          handleHeight
          handleWidth
          onResize={this.handleResize}
        />
      </div>
    );
  }
}

StickyBar.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
};

StickyBar.defaultProps = {
  className: '',
  style: {},
};

export default StickyBar;
