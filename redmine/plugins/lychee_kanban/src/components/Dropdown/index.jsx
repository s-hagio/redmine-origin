import React, { Component, cloneElement } from 'react';
import PropTypes from 'prop-types';

/* eslint-disable no-underscore-dangle */
class Dropdown extends Component {
  constructor() {
    super();

    this.state = {
      open: false,
      _windowClicked: false,
    };
  }

  componentDidMount() {
    window.addEventListener('click', this._onWindowClick);
    window.addEventListener('touchstart', this._onWindowClick);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this._onWindowClick);
    window.removeEventListener('touchstart', this._onWindowClick);
  }

  _onWindowClick = () => {
    const { open, _windowClicked } = this.state;
    if (open) {
      if (_windowClicked) {
        this.setState({
          open: false,
          _windowClicked: false,
        });
      } else {
        this.setState({ _windowClicked: true });
      }
    }
  };

  render() {
    const { children, trigger, ...props } = this.props;
    const { open } = this.state;
    const triggerElement = cloneElement(trigger, {
      onClick: () => {
        this.setState({ open: true });
        if (trigger.props.onClick) {
          // eslint-disable-next-line prefer-rest-params
          trigger.props.onClick.apply(trigger, arguments);
        }
      },
    });
    return (
      <div {...props}>
        {triggerElement}
        {open && children}
      </div>
    );
  }
}
/* eslint-enable no-underscore-dangle */

Dropdown.propTypes = {
  children: PropTypes.node.isRequired,
  trigger: PropTypes.element.isRequired,
};

export default Dropdown;
