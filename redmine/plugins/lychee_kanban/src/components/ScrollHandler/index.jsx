import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactResizeDetector from 'react-resize-detector';
import SyncScroll from '@/components/SyncScroll';

class ScrollHandler extends Component {
  constructor() {
    super();

    this.state = { width: 0, };
  }

  handleResize = () => {
    const { target, widthOffset } = this.props;
    const targetEl = document.getElementById(target);
    if (targetEl) {
      this.setState({ width: targetEl.scrollWidth + widthOffset });
    }
  };

  render() {
    const { width } = this.state;
    const { target } = this.props;
    const style = {
      height: 1,
      width,
    };

    return (
      <SyncScroll>
        <div style={style} />
        <ReactResizeDetector
          handleWidth
          handleHeight
          querySelector={`#${target}`}
          onResize={this.handleResize}
        />
      </SyncScroll>
    );
  }
}

ScrollHandler.propTypes = {
  target: PropTypes.string.isRequired,
  widthOffset: PropTypes.number,
};

ScrollHandler.defaultProps = { widthOffset: 0, };

export default ScrollHandler;
