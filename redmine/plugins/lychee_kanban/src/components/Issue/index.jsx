import React, { createContext } from 'react';
import { withNamespaces } from 'react-i18next';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useDrag } from 'react-dnd';
import classNames from 'classnames';
import styled from 'styled-components';
import moment from 'moment';
import isEmpty from 'lodash/isEmpty';
import isNumber from 'lodash/isNumber';

import * as ItemTypes from '@/constants/itemTypes';
import { ORIGIN_ISSUES_PATH } from '@/constants/endpoints';
import IMG_ADD from '@/images/add.png';
import IMG_HOT from '@/images/hot.png';
import IMG_NODE from '@/images/node.svg';
import IMG_NOTES from '@/images/notes.png';
import IMG_WARNING from '@/images/warning.png';
import IMG_ROOT_NODE from '@/images/root.svg';
import IMG_RELATION_BOTH from '@/images/relation_precede_and_follow.png';
import IMG_RELATION_FOLLOW from '@/images/relation_follow.png';
import IMG_RELATION_PRECEDE from '@/images/relation_precede.png';
import { stopPropagation } from '@/utils';

import { useRedmineTimespan } from '@/hooks/RedmineTimespan';

const RIGHT_SPACE_WIDTH = 36;
const Root = styled.div`
  display: flex;
  background-color: #fff;
  box-shadow: 2px 2px 2px 0 #ccc;
  cursor: pointer;
  margin: 1px;

  &.root,
  &.seed {
    background-color: #fdfbe3;
  }

  &.hovered {
    position: relative;

    &::after {
      position: absolute;
      content: '';
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      z-index: 10;
      background-color: rgba(0, 0, 255, 0.1);
    }
  }

  .tracker {
    min-width: 8px;
    width: 8px;
  }

  .ancestors {
    color: #f7792a;
    font-size: 9px;
    margin-bottom: 2px;
    display: flex;
    align-items: baseline;
    flex-wrap: wrap;

    span {
      &:not(:last-child)::after {
        content: '>>';
        display: inline-block;
        margin: 0 4px;
      }

      &:hover {
        text-decoration: underline;
        text-decoration-skip: '>>';
      }
    }
  }

  .projects {
    color: #5e768c;
    font-size: 10px;
    margin-bottom: 2px;
    display: flex;
    flex-wrap: wrap;

    span {
      white-space: nowrap;
      overflow: hidden;
      max-width: 100%;
      text-overflow: ellipsis;

      &:not(:last-child)::after {
        content: '>>';
        display: inline-block;
        margin: 0 4px;
      }
    }
  }

  .cost {
    color: #185c64;
    height: 32px;
    width: 32px;
    min-height: 32px;
    min-width: 32px;
    border-radius: 50%;
    display: none;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    box-sizing: border-box;
    padding: 0;
    margin-right: 4px;

    &--dummy::before {
      display: block;
      content: '--';
      visibility: hidden;
    }

    &::after {
      display: block;
      font-size: 10px;
    }

    &--points {
      background-color: #cef8f3;

      &::after {
        content: 'pt';
      }
    }

    &--total_estimated_hours {
      background-color: #fff3ab;

      &::after {
        content: 'h';
      }
    }
  }

  .main {
    display: flex;
    align-items: baseline;
  }

  .subject {
    font-size: 13px;
    word-break: break-all;
  }

  .flex_column {
    display: flex;
    flex-flow: column;
  }

  .progress_indicator_day {
    display: block;
    float: left;
    width: 8px;
    height: 8px;
    margin-right: 3px;
    background-color: #f3a68c;
  }

  .right-box {
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    min-width: ${RIGHT_SPACE_WIDTH}px;
  }

  .node-icon {
    height: 12px;
    width: 12px;
    background-position: center;
    background-repeat: no-repeat;
    background-size: contain;
    margin-left: 4px;
    vertical-align: middle;
    display: none;

    &--root {
      display: inline-block;
      background-image: url(${IMG_ROOT_NODE});
    }

    &--node {
      display: inline-block;
      background-image: url(${IMG_NODE});
    }
  }

  .description,
  .comments {
    font-size: 11px;
    margin-top: 4px;
    padding-left: 4px;
  }

  .description,
  .comments,
  .custom-field-item__value {
    line-height: 12px;
    max-height: 38px;

    p {
      margin-top: 0;
      margin-bottom: 0;
    }

    h1,
    h2,
    h3 {
      font-size: 11px;
      margin: 0;
      line-height: 1;

      .wiki-anchor {
        display: none;
      }
    }

    ul,
    ol {
      margin: 0;
      padding-left: 16px;
    }

    blockquote {
      margin: 0;
    }

    pre {
      margin: 0;
      white-space: normal;
    }

    img {
      max-width: 100%;
    }

    @-moz-document url-prefix() {
      ol li {
        margin-top: -1px;
      }
    }

    @media all and (-ms-high-contrast: none) {
      ol {
        padding-left: 20px;
      }
    }
  }

  .comments {
    display: flex;
    align-items: baseline;
    margin-top: 0 !important;
    padding-left: 0 !important;

    &::before {
      content: '_';
      display: inline-block;
      min-width: 14px;
      min-height: 14px;
      background-image: url(${IMG_NOTES});
      background-position: center;
      background-repeat: no-repeat;
      background-size: contain;
      margin-right: 4px;
      color: transparent;
    }

    .LinesEllipsis {
      flex: 1;
      min-width: 1px;
    }
  }

  .custom-field {
    &-list {
      color: #666;
      font-size: 10px;
      line-height: 12px;

      hr:last-child {
        display: none;
      }
    }

    &-item {
      display: flex;
      align-items: baseline;

      &__name {
        font-weight: bold;
        white-space: nowrap;
        min-width: 20%;
        max-width: 50%;
        overflow: hidden;
        text-overflow: ellipsis;
      }

      &__value {
        word-break: break-all;
      }

      &--text {
        display: block;
      }

      &:not(:last-child) {
        margin-bottom: 2px;
      }
    }
  }

  .status {
    &-list {
      display: flex;
      align-items: flex-end;
      margin: 4px -2px 0;
      flex-grow: 1;
    }

    &-item {
      height: 14px;
      width: 14px;
      background-position: center;
      background-repeat: no-repeat;
      background-size: 14px;
      margin: 0 2px;

      &-relation {
        &--both {
          background-image: url(${IMG_RELATION_BOTH});
        }

        &--precede {
          background-image: url(${IMG_RELATION_PRECEDE});
        }

        &--follow {
          background-image: url(${IMG_RELATION_FOLLOW});
        }
      }

      &-warning {
        background-image: url(${IMG_WARNING});
      }

      &-blocking {
        background-image: url(${IMG_HOT});
      }

      &-add {
        background-image: url(${IMG_ADD});
        cursor: pointer;
        flex-grow: 1;
        visibility: hidden;
      }

      &--highlight {
        padding: 2px;
        margin: -2px 0;
        border-radius: 50%;

        &-precede {
          background-color: #c3d7ff;
        }

        &-follow {
          background-color: #fdd8e8;
        }

        &-both {
          background-color: #e0d0ff;
        }
      }
    }
  }

  &:hover .status-item-add {
    visibility: visible;
  }

  .due-date {
    color: #999;
    display: flex;
    align-items: center;
    justify-content: center;

    &--overdue {
      color: #e21414;
      font-weight: bold;
    }

    &--dummy {
      visibility: hidden;
      height: 12px;
      margin-bottom: -12px;
    }
  }

  .assigned-to {
    display: flex;
    align-items: flex-start;
    justify-content: flex-end;
    flex-grow: 1;
    width: 100%;
    white-space: normal;
    word-break: break-all;

    &__name {
      text-align: right;
    }

    &__avatar {
      display: flex;
      align-items: center;
      justify-content: center;
      color: #fff;
      background-color: #00ab9f;
      font-weight: bold;
      border-radius: 3px;
      height: 24px;
      width: 24px;
      margin: 2px 0;
    }

    img {
      background-color: #fff;
    }
  }

  .estimated-hours {
    align-items: center;
    display: flex;
    font-weight: bold;
    justify-content: center;
    height: 14px;
    word-break: break-all;
    margin-left: auto;
    margin-right: 4px;

    span {
      white-space: nowrap;

      &::after {
        content: 'H';
      }
    }
  }

  .comments,
  .custom-field-item {
    margin-right: ${RIGHT_SPACE_WIDTH}px;
  }
`;

export const IssueContext = createContext({ attributes: [], });

export const IssueProvider = (props) => {
  const { children } = props;
  const redmine = useSelector((state) => state.redmine);
  const value = React.useMemo(() => ({
    attributes: redmine.display_attribute_names || [],
  }), [redmine]);

  return (
    <IssueContext.Provider value={value}>{children}</IssueContext.Provider>
  );
};

IssueProvider.propTypes = { children: PropTypes.node.isRequired, };

export const IssueAttribute = ({ children, name }) => {
  const { attributes } = React.useContext(IssueContext);

  if (attributes.includes(name)) {
    return children || null;
  }
  return null;
};

export const IssueCustomValue = ({ field }) => {
  const { attributes } = React.useContext(IssueContext);

  if (attributes.includes(`cf_${field.id}`) && !isEmpty(field.value)) {
    return (
      <div
        key={field.id}
        className={`custom-field-item custom-field-item--${field.format}`}
      >
        <div className="custom-field-item__name">{`${field.name}:`}</div>
        <div
          className="custom-field-item__value line-clamp-3"
          dangerouslySetInnerHTML={{ __html: field.formatted_value }}
          onClick={stopPropagation()}
        />
      </div>
    );
  }

  return null;
};

const AssignedTo = (props) => {
  const { name, avatar, avatarEnabled: enabled } = props;

  if (enabled) {
    if (avatar) {
      return (
        <img
          src={avatar}
          alt={name}
          title={name}
          className="assigned-to__avatar"
        />
      );
    }
    if (name) {
      return (
        <div title={name} className="assigned-to__avatar">
          {name[0]}
        </div>
      );
    }
  }
  return <span className="assigned-to__name">{name}</span>;
};

const Issue = (props) => {
  const {
    ancestors,
    allowedIssueStatuses,
    assignedToId,
    assignedToName,
    assignedToAvatar,
    avatarEnabled,
    blocking,
    className,
    closed,
    costs,
    costType,
    customFields,
    description,
    dueDate,
    estimatedHours,
    fixedVersionId,
    hovered,
    id,
    notes,
    nodeType,
    onAddChild,
    onClick,
    onParentClick,
    onMouseOver,
    onMouseOut,
    relationHighlightType,
    relationType,
    projects,
    startDate,
    statusId,
    statusColor,
    subject,
    trackerColor,
    warning,
    days_elapsed,
    t,
  } = props;
  const [, drag] = useDrag({
    item: { type: ItemTypes.ISSUE },
    begin: () => ({
      id,
      statusId,
      fixedVersionId,
      assignedToId,
      startDate,
      dueDate,
      allowedIssueStatuses,
    }),
  });

  const handleClickParent = React.useCallback((parent) => (event) => {
    if (onParentClick) {
      onParentClick(parent, event);
    }
  }, [onParentClick]);

  const formatTimespan = useRedmineTimespan();

  return (
    <Root
      className={classNames({
        [className]: true,
        [nodeType]: true,
        closed,
        hovered,
      })}
      onClick={onClick}
      onMouseEnter={onMouseOver}
      onMouseLeave={onMouseOut}
      title={subject}
      ref={drag}
    >
      <div className="tracker" style={{ backgroundColor: trackerColor }} />
      <div
        className="d-flex flex-grow flex-column minw"
        style={{
          backgroundColor: !closed && statusColor,
          padding: '4px 4px 4px 6px',
        }}
      >
        <IssueAttribute name="parent_issue">
          {!isEmpty(ancestors) && (
            <strong className="ancestors">
              {ancestors.map((ancestor) => (
                <span
                  key={ancestor.id}
                  onClick={stopPropagation(handleClickParent(ancestor))}
                >
                  {ancestor.subject}
                </span>
              ))}
            </strong>
          )}
        </IssueAttribute>
        <div className="flex-grow">
          <IssueAttribute name="project">
            {!isEmpty(projects) && (
              <div className="projects">
                {projects.map((project) => (
                  <span key={project}>{project}</span>
                ))}
              </div>
            )}
          </IssueAttribute>
          <div className="d-flex flex-grow">
            {costType !== 'issue_count' && (
              <IssueAttribute name="estimated_hours">
                <strong
                  className={classNames({
                    'cost': true,
                    [`cost--${costType}`]: true,
                    'cost--dummy': !isNumber(costs),
                  })}
                >
                  {isNumber(costs) ? formatTimespan(costs) : ''}
                </strong>
              </IssueAttribute>
            )}
            <div className="d-flex flex-column flex-grow text-wrap minw">
              <div className="main">
                <div className="flex-grow minw w-100 flex_column">
                  <div className="subject">
                    <IssueAttribute name="issue_id">
                      <a
                        href={`${ORIGIN_ISSUES_PATH}/${id}`}
                        style={{ marginRight: 2 }}
                        onClick={stopPropagation()}
                      >
                        {`#${id}`}
                      </a>
                    </IssueAttribute>
                    <span>{subject}</span>
                    <span className={`node-icon node-icon--${nodeType}`} />
                  </div>
                  <div title={t(`length_of_stay${days_elapsed >= 10 ? '_more' : ''}`, { value: days_elapsed })}>
                    {[...Array(days_elapsed)].map(() => <span className="progress_indicator_day" />)}
                  </div>
                  <IssueAttribute name="description">
                    {description && (
                      <div
                        className="description line-clamp-3"
                        dangerouslySetInnerHTML={{ __html: description }}
                        onClick={stopPropagation()}
                      />
                    )}
                  </IssueAttribute>
                </div>
                <div className="right-box">
                  <IssueAttribute name="due_date">
                    <div
                      className={classNames({
                        'due-date': true,
                        'due-date--overdue': moment().isAfter(dueDate),
                        'due-date--dummy': !dueDate,
                      })}
                    >
                      {dueDate ? moment(dueDate).format('M/D') : '--'}
                    </div>
                  </IssueAttribute>
                  <IssueAttribute name="assigned_to">
                    <div className="assigned-to">
                      <AssignedTo
                        name={assignedToName}
                        avatar={assignedToAvatar}
                        avatarEnabled={avatarEnabled}
                      />
                    </div>
                  </IssueAttribute>
                </div>
              </div>
              <IssueAttribute name="comments">
                {notes && (
                  <React.Fragment key="issue">
                    <hr />
                    <div className="comments">
                      <div
                        className="line-clamp-3"
                        dangerouslySetInnerHTML={{ __html: notes }}
                        onClick={stopPropagation()}
                      />
                    </div>
                  </React.Fragment>
                )}
              </IssueAttribute>
              <div className="custom-field-list">
                <hr />
                {customFields.map((field) => (
                  <IssueCustomValue key={field.id} field={field} />
                ))}
              </div>
              <div className="status-list">
                <IssueAttribute name="relations">
                  {relationType && (
                    <div
                      className={classNames({
                        'status-item': true,
                        'status-item-relation': true,
                        [`status-item-relation--${relationType}`]: true,
                        'status-item--highlight': !!relationHighlightType,
                        [`status-item--highlight-${relationHighlightType}`]: !!relationHighlightType,
                      })}
                    />
                  )}
                </IssueAttribute>
                <IssueAttribute name="warning">
                  {warning && (
                    <div className="status-item status-item-warning" />
                  )}
                </IssueAttribute>
                <IssueAttribute name="blocking">
                  {blocking && (
                    <div className="status-item status-item-blocking" />
                  )}
                </IssueAttribute>
                {onAddChild && (
                  <div
                    className="status-item status-item-add"
                    onClick={stopPropagation(onAddChild)}
                  />
                )}
                <IssueAttribute name="estimated_hours">
                  <div className="estimated-hours">
                    <span>{isNumber(estimatedHours) ? formatTimespan(estimatedHours) : ''}</span>
                  </div>
                </IssueAttribute>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Root>
  );
};

Issue.propTypes = {
  allowedIssueStatuses: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })
  ),
  ancestors: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      subject: PropTypes.string,
    })
  ),
  assignedToId: PropTypes.number,
  assignedToName: PropTypes.string,
  assignedToAvatar: PropTypes.string,
  avatarEnabled: PropTypes.bool,
  blocking: PropTypes.bool,
  className: PropTypes.string,
  closed: PropTypes.bool,
  costs: PropTypes.number,
  costType: PropTypes.oneOf(['points', 'total_estimated_hours', 'issue_count']),
  customFields: PropTypes.array,
  description: PropTypes.string,
  dueDate: PropTypes.string,
  estimatedHours: PropTypes.number,
  fixedVersionId: PropTypes.number,
  hovered: PropTypes.bool,
  id: PropTypes.number.isRequired,
  nodeType: PropTypes.oneOf(['root', 'node', 'leaf', 'seed']),
  notes: PropTypes.string,
  onAddChild: PropTypes.func,
  onClick: PropTypes.func,
  onParentClick: PropTypes.func,
  onMouseOver: PropTypes.func,
  onMouseOut: PropTypes.func,
  relationHighlightType: PropTypes.oneOf(['precede', 'follow', 'both']),
  relationType: PropTypes.oneOf(['precede', 'follow', 'both']),
  projects: PropTypes.arrayOf(PropTypes.string),
  statusId: PropTypes.number,
  statusColor: PropTypes.string,
  startDate: PropTypes.string,
  subject: PropTypes.string.isRequired,
  trackerColor: PropTypes.string,
  warning: PropTypes.bool,
  days_elapsed: PropTypes.number.isRequired,
};

Issue.defaultProps = {
  allowedIssueStatuses: [],
  ancestors: [],
  assignedToId: null,
  assignedToName: '',
  assignedToAvatar: '',
  avatarEnabled: false,
  blocking: false,
  className: '',
  closed: false,
  costs: null,
  costType: 'hours',
  customFields: [],
  description: '',
  dueDate: null,
  estimatedHours: 0,
  fixedVersionId: null,
  hovered: false,
  nodeType: null,
  notes: '',
  onAddChild: null,
  onClick: null,
  onParentClick: null,
  onMouseOver: null,
  onMouseOut: null,
  relationHighlightType: null,
  relationType: null,
  projects: null,
  statusId: null,
  statusColor: null,
  startDate: '',
  trackerColor: null,
  warning: false,
};

export default withNamespaces('Issue')(Issue);
