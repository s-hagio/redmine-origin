import React from 'react';
import { shallow } from 'enzyme';

import Issue, { IssueContext, IssueAttribute } from './index';

describe('Issue', () => {
  it('renders "subject"', () => {
    const issue = shallow(<Issue id={0} subject="hogehoge" />);
    expect(issue.find('.subject').text()).toEqual('<IssueAttribute />hogehoge');
  });

  it('changes color by "trackerColor"', () => {
    const issue = shallow(<Issue id={0} subject="hoge" trackerColor="#fff" />);
    const { style: { backgroundColor }, } = issue.find('.tracker').props();
    expect(backgroundColor).toEqual('#fff');
  });

  it('changes backgroundColor by "statusColor"', () => {
    const issue = shallow(<Issue id={0} subject="hoge" statusColor="#fff" />);
    const { style: { backgroundColor }, } = issue.find('.tracker + *').props();
    expect(backgroundColor).toEqual('#fff');
  });

  describe('IssueAttribute', () => {
    // eslint-disable-next-line prefer-destructuring
    const useContext = React.useContext;
    beforeEach(() => {
      React.useContext = jest.fn(() => ({ attributes: ['hoge'] }));
    });
    afterEach(() => {
      React.useContext = useContext;
    });

    it('renders children if "attributes" includes "props.name"', () => {
      /* eslint-disable */
      const attribute = shallow(
        <IssueContext.Provider value={{ attributes: ['hoge'] }}>
          <div>
            <IssueAttribute name="hoge">
              <div>hoge</div>
            </IssueAttribute>
            <IssueAttribute name="fuga">
              <div>fuga</div>
            </IssueAttribute>
          </div>
        </IssueContext.Provider>
      );
      /* eslint-enable */

      expect(attribute.render().text()).toBe('hoge');
    });
  });
});
