import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactResizeDetector from 'react-resize-detector';
import * as _ from 'lodash';

const THROTTLE_RATE = 1000 / 60;
const handleScroll = _.throttle(({ target }) => {
  const { scrollLeft } = target;
  const id = target.getAttribute('data-sync-scroll');
  const nodelist = document.querySelectorAll(`[data-sync-scroll=${id}]`);
  const elements = Array.from(nodelist);
  elements.forEach((element) => {
    if (element !== target && element.scrollLeft !== scrollLeft) {
      element.removeEventListener('scroll', handleScroll);
      // HACK: lint違反の修正
      // eslint-disable-next-line no-use-before-define
      element.removeEventListener('scroll', reattachScrollEvents);
      // eslint-disable-next-line no-param-reassign
      element.scrollLeft = scrollLeft;
    }
  });
}, THROTTLE_RATE);

const REATTACH_INTERVAL = 100;
const reattachScrollEvents = _.debounce(({ target }) => {
  const id = target.getAttribute('data-sync-scroll');
  const nodelist = document.querySelectorAll(`[data-sync-scroll=${id}]`);
  const elements = Array.from(nodelist);
  elements.forEach((element) => {
    if (element !== target) {
      element.addEventListener('scroll', handleScroll);
      element.addEventListener('scroll', reattachScrollEvents);
    }
  });
}, REATTACH_INTERVAL);

class SyncScroll extends Component {
  constructor() {
    super();
    this.state = {
      width: 'auto',
      height: 'auto',
    };
  }

  componentDidMount() {
    this.scroller.addEventListener('scroll', handleScroll);
    this.scroller.addEventListener('scroll', reattachScrollEvents);
  }

  componentWillUnmount() {
    this.scroller.removeEventListener('scroll', handleScroll);
    this.scroller.removeEventListener('scroll', reattachScrollEvents);
  }

  handleResize = _.throttle((_width, height) => {
    this.setState({ height });
  }, THROTTLE_RATE);

  handleResizeWidth = _.throttle((width) => {
    this.setState({ width });
  }, THROTTLE_RATE);

  render() {
    const { children, className, id, hidebar, style } = this.props;
    const { width, height } = this.state;
    const wrapperStyle = hidebar ? { width, height, overflow: 'hidden' } : {};
    const scrollerStyle = { overflow: 'auto' };
    return (
      <div className={className} style={style}>
        <div style={wrapperStyle}>
          <div
            data-sync-scroll={id}
            style={scrollerStyle}
            ref={(ref) => {
              this.scroller = ref;
            }}
          >
            {children}
            <ReactResizeDetector handleHeight onResize={this.handleResize} />
          </div>
        </div>
        <ReactResizeDetector handleWidth onResize={this.handleResizeWidth} />
      </div>
    );
  }
}

SyncScroll.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  id: PropTypes.string,
  hidebar: PropTypes.bool,
  style: PropTypes.object,
};

SyncScroll.defaultProps = {
  id: 'default',
  className: '',
  hidebar: false,
  style: {},
};

export default SyncScroll;
