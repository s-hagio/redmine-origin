import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import AssignedKanbanWorkflowWorkload from '@/containers/assignedKanban/AssignedKanbanWorkflowWorkload';
import style from './style.css';

export const TrackerButton = ({ name, color }) => {
  const trackerStyle = { backgroundColor: color };
  return (
    <div className={style.tracker} style={trackerStyle}>
      {name}
    </div>
  );
};

TrackerButton.defaultProps = { color: '', };

TrackerButton.propTypes = {
  color: PropTypes.string,
  name: PropTypes.string.isRequired,
};

export const Workflow = ({
  active,
  trackers = [],
  index,
  onClick,
  showWorkload,
}) => {
  const workflowClass = classNames([style.workflow, active && style.active]);
  const handleClickWorkflow = () => {
    if (!active) {
      onClick(index);
    }
  };

  return (
    <div className={workflowClass} onClick={handleClickWorkflow}>
      {trackers.map((tracker, tracker_index) => (
        // HACK: lint違反を修正
        /* eslint-disable-next-line */
        <TrackerButton key={tracker_index} {...tracker} />
      ))}
      {showWorkload ? (
        <AssignedKanbanWorkflowWorkload trackers={trackers} />
      ) : (
        ''
      )}
    </div>
  );
};

Workflow.defaultProps = {
  active: false,
  showWorkload: false,
};

Workflow.propTypes = {
  active: PropTypes.bool,
  index: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
  showWorkload: PropTypes.bool,
  trackers: PropTypes.array.isRequired,
};

export default class WorkflowList extends Component {
  handleScrollTab(vector) {
    const scroll_speed = 10;
    const callback = () => {
      this.container.scrollLeft += scroll_speed * vector;
    };
    const intervalID = setInterval(callback, 20);
    document.addEventListener('mouseup', () => clearInterval(intervalID));
  }

  renderController(direction, vector, text) {
    return (
      <button
        type="button"
        className={classNames(style.controller, style[direction])}
        onMouseDown={() => this.handleScrollTab(vector)}
      >
        {text}
      </button>
    );
  }

  render() {
    const { showWorkload, workflows, handleClickWorkflow } = this.props;

    return (
      <div className={style.scrollFrame}>
        <div className={style.wrapper}>
          {/* prettier-ignore */}
          <div className={style.container} ref={(ref) => { this.container = ref; }}>
            {workflows.map((workflow, index) => (
              <Workflow
                // HACK: lint違反を修正
                /* eslint-disable-next-line */
                key={index}
                active={workflow.active}
                trackers={workflow.trackers}
                index={index}
                onClick={handleClickWorkflow}
                showWorkload={showWorkload}
              />
            ))}
          </div>
          {showWorkload ? this.renderController('left', -1, '<') : ''}
          {showWorkload ? this.renderController('right', +1, '>') : ''}
        </div>
      </div>
    );
  }
}

WorkflowList.defaultProps = {
  handleClickWorkflow: null,
  showWorkload: false,
  workflows: [],
};

WorkflowList.propTypes = {
  handleClickWorkflow: PropTypes.func,
  showWorkload: PropTypes.bool,
  workflows: PropTypes.array,
};
