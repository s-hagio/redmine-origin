import React from 'react';
import { shallow } from 'enzyme';
import * as _ from 'lodash';

import WorkflowList, { Workflow, TrackerButton } from './index';

const trackers = _.range(3).map((value) => ({
  id: value,
  name: `tracker${value}`,
  color: `color${value}`,
}));

const workflows = _.range(3).map((n, index) => ({
  trackers: [_.sample(trackers)],
  index,
}));

describe('WorkflowList', () => {
  let list;
  const props = {
    showWorkload: false,
    workflows,
    handleClickWorkflow: jest.fn(),
  };

  beforeEach(() => {
    list = shallow(<WorkflowList {...props} />);
  });

  it('renders workflows', () => {
    const workflowComponents = list.find(Workflow);
    const workflowSample = _.sample(workflowComponents.getElements());
    expect(list.find(Workflow).length).toBe(props.workflows.length);
    expect(workflowSample.props).toHaveProperty('trackers');
    expect(workflowSample.props).toHaveProperty('index');
    expect(workflowSample.props).toHaveProperty('onClick');
  });
});

describe('Workflow', () => {
  let workflow;
  const props = {
    showWorkload: false,
    onClick: jest.fn(),
    ..._.sample(workflows),
  };

  beforeEach(() => {
    workflow = shallow(<Workflow {...props} />);
  });

  it('renders workflow', () => {
    expect(workflow.props()).toHaveProperty('onClick');

    const trackerButtons = workflow.find(TrackerButton);
    expect(trackerButtons.length).toBe(1);
    expect(trackerButtons.props()).toHaveProperty('id');
    expect(trackerButtons.props()).toHaveProperty('name');
    expect(trackerButtons.props()).toHaveProperty('color');
  });

  it('calls onClick props', () => {
    workflow.simulate('click');
    expect(props.onClick).toBeCalled();
  });
});

describe('TrackerButton', () => {
  let button;
  const props = { ..._.sample(trackers), };

  beforeEach(() => {
    button = shallow(<TrackerButton {...props} />);
  });

  it('renders button', () => {
    expect(button.text()).toBe(props.name);
    expect(button.props().style).toEqual({ backgroundColor: props.color });
  });
});
