import React from 'react';
import { shallow } from 'enzyme';

import Link from './index';

describe('Link', () => {
  let link;
  const props = {
    label: 'hogehoge',
    icon: 'hogehoge',
    onClick: jest.fn(),
  };

  beforeEach(() => {
    link = shallow(<Link {...props} />);
  });

  it('renders label props', () => {
    expect(link.text()).toEqual(props.label);
  });

  it('incluces icon props', () => {
    expect(link.find('img').props()).toHaveProperty('src', props.icon);
  });

  it('calls onClick props', () => {
    link.simulate('click');
    expect(props.onClick).toBeCalled();
  });
});
