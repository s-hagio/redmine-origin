import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import style from './style.css';

const Link = ({ className, label, icon, onClick, style: customStyle }) => (
  <div
    className={classNames([style.container, className])}
    style={customStyle}
    onClick={onClick}
  >
    {icon ? <img className={style.icon} src={icon} alt="icon" /> : null}
    <span>{label}</span>
  </div>
);

Link.defaultProps = {
  className: '',
  icon: null,
  style: {},
};

Link.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  icon: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  style: PropTypes.object,
};

export default Link;
