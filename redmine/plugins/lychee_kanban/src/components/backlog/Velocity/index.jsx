import * as React from 'react';
import { createPortal } from 'react-dom';
import styled from 'styled-components';
import { withNamespaces } from 'react-i18next';
import ReactTooltip from 'react-tooltip';

const Root = styled.div`
  .velocity {
    &__container {
      select {
        margin-bottom: 5px;
      }
      div {
        margin-bottom: 0;
        font-weight: bold;
      }
      p {
        margin-top: 4px;
        margin-bottom: 8px;
      }
      input {
        width: 5em;
      }
    }
  }
`;

const Dashed = styled.span`
  border-bottom: 1px dashed;
`;

const Velocity = (props) => {
  const { target, velocities, unit, dailyVelocity, availableVelocityUnit, onChange, t } = props;
  const handleChange = React.useCallback(
    (name) => (event) => {
      onChange({
        unit,
        [name]: event.target.value,
      });
    },
    [unit, onChange],
  );

  const [average, setAverage] = React.useState(0);
  React.useEffect(() => setAverage(Math.round(dailyVelocity)), [dailyVelocity]);
  const handleChangeAverage = React.useCallback(
    (name) => (event) => {
      if (event.target.validity.valid) {
        setAverage(event.target.value);
        onChange({
          unit,
          [name]: event.target.value,
        });
      }
    },
    [unit, onChange],
  );

  return createPortal(
    <Root>
      <h3>{t('title')}</h3>
      <div className="velocity__container">
        <select value={unit} onChange={handleChange('unit')}>
          {Object.keys(availableVelocityUnit).map((key) => (
            <option key={key} value={key}>
              {availableVelocityUnit[key]}
            </option>
          ))}
        </select>

        <div>{t('velocities')}</div>
        <p>{velocities.join('→')}</p>

        <div data-tip={t('help')}>
          <div>
            <Dashed>{t('average')}</Dashed>
          </div>
          <input
            type="number"
            min="0"
            step="1"
            value={average}
            onChange={handleChangeAverage('input_daily_velocity')}
          />
          <ReactTooltip effect="solid" type="dark" place="bottom" multiline="true" />
        </div>
      </div>
    </Root>,
    document.getElementById(target),
  );
};

export default withNamespaces('Velocity')(Velocity);
