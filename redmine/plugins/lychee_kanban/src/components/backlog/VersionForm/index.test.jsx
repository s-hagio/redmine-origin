import React from 'react';
import { shallow } from 'enzyme';

import VersionForm, { ConsumerField } from './index';

describe('VersionForm', () => {
  const props = {
    id: 99,
    fieldOptions: { sharings: [{ value: 'dummy', text: 'ダミー' }], },
    onSubmit: jest.fn(),
    handleSubmit: jest.fn(),
    reset: jest.fn(),
  };

  it('has name field in header', () => {
    const form = shallow(<VersionForm {...props} />);
    const header = form.find('header');
    const field = header.find(ConsumerField);
    expect(field.props()).toHaveProperty('name', 'name');
  });

  it('has each fields in body', () => {
    const form = shallow(<VersionForm {...props} />);
    expect(form.find({ name: 'description' }).exists()).toBeTruthy();
    expect(form.find({ name: 'status' }).exists()).toBeTruthy();
    expect(form.find({ name: 'wiki_page_title' }).exists()).toBeTruthy();
    expect(form.find({ name: 'start_date' }).exists()).toBeTruthy();
    expect(form.find({ name: 'effective_date' }).exists()).toBeTruthy();
    expect(form.find({ name: 'sharing' }).exists()).toBeTruthy();
  });

  it('toggles state after click icon', () => {
    const form = shallow(<VersionForm {...props} />);
    const icon = form.find('header i');
    expect(form.state().editing).toBeFalsy();
    expect(form.find('footer').exists()).toBeFalsy();
    icon.simulate('click');
    expect(form.state().editing).toBeTruthy();
    expect(form.find('footer').exists()).toBeTruthy();
  });

  it('hides icon when create version', () => {
    const form = shallow(<VersionForm {...props} id={null} />);
    const icon = form.find('header i');
    expect(icon.exists()).toBeFalsy();
  });

  it('hides footer when not editing', () => {
    const form = shallow(<VersionForm {...props} />);
    const icon = form.find('footer');
    expect(icon.exists()).toBeFalsy();
  });

  it('shows custom fields when has "customFields"', () => {
    const customFields = [{ id: '99', name: 'dummy' }];
    const form = shallow(
      <VersionForm {...props} customFields={customFields} />
    );
    expect(form.find({ name: '99' }).exists()).toBeTruthy();
  });
});
