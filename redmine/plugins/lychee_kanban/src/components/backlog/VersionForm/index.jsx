import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, FormSection } from 'redux-form';
import classnames from 'classnames';
import i18n from 'i18next';
import { withNamespaces } from 'react-i18next';
import * as _ from 'lodash';

import { ORIGIN_VERSIONS_PATH, projectWikiPath } from '@/constants/endpoints';
import LycheeModal from '@/components/LycheeModal';
import CustomField from '@/components/LycheeForm/CustomField';
import {
  DateField,
  SelectField,
  TextField,
  TextAreaField,
} from '@/components/LycheeForm/fields';
import style from '@/components/LycheeForm/style.css';

const TriggerMouseUpEventOnIE = ({ target }) => {
  // for IE10
  if (target.fireEvent) {
    target.fireEvent('onmouseup');
  }
  // for IE11
  if (target.dispatchEvent) {
    const event = document.createEvent('MouseEvents');
    event.initEvent('mouseup', false, true);
    target.dispatchEvent(event);
  }
};

// TODO: React v16.3のContextAPIに移行する
export const ConsumerField = (
  { name, label, type, safe, required, ...props },
  context
) => {
  const classes = classnames({
    [style.field]: true,
    [style[name]]: Boolean(style[name]),
    [style[type]]: Boolean(style[type]),
    [style.required]: required,
  });
  if (safe || context.safeAttributes.includes(name)) {
    return (
      <div className={classes}>
        {label && <label htmlFor={name}>{label}</label>}
        <Field
          name={name}
          type={type}
          editing={context.editing}
          required={required}
          {...props}
        />
      </div>
    );
  }
  return null;
};

ConsumerField.defaultProps = {
  label: null,
  type: '',
  required: false,
  safe: false,
};

ConsumerField.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string.isRequired,
  required: PropTypes.bool,
  safe: PropTypes.bool,
};

ConsumerField.contextTypes = {
  editing: PropTypes.bool,
  safeAttributes: PropTypes.array,
};

export const SHARING_OPTIONS = [
  { value: 'none', text: i18n.t('Sharing:none') },
  { value: 'descendants', text: i18n.t('Sharing:subprojects') },
  { value: 'hierarchy', text: i18n.t('Sharing:project_hierarchy') },
  { value: 'tree', text: i18n.t('Sharing:project_tree') },
  { value: 'system', text: i18n.t('Sharing:all_projects') },
];
const STATUS_OPTIONS = [
  { value: 'open', text: i18n.t('VersionStatus:open') },
  { value: 'locked', text: i18n.t('VersionStatus:locked') },
  { value: 'closed', text: i18n.t('VersionStatus:closed') },
];
class VersionForm extends Component {
  constructor() {
    super();

    this.cancel = this.cancel.bind(this);
    this.toggle = this.toggle.bind(this);
    this.state = { editing: false, };
  }

  getChildContext() {
    const { editing } = this.state;
    const { safeAttributes } = this.props;
    return {
      editing,
      safeAttributes,
    };
  }

  // HACK: lint違反を修正
  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    const { reset } = this.props;
    if (!nextProps.open) {
      reset();
      this.setState({ editing: false });
    } else if (!nextProps.id) {
      this.setState({ editing: true });
    }
  }

  get isNew() {
    const { id } = this.props;
    return !id;
  }

  cancel() {
    const { onClose } = this.props;
    if (this.isNew) {
      onClose();
    } else {
      this.toggle();
    }
  }

  toggle() {
    const { editing } = this.state;
    const { reset } = this.props;
    reset();
    this.setState({ editing: !editing, });
  }

  render() {
    const { editing } = this.state;
    const {
      customFields,
      dateFormat,
      fieldOptions,
      handleSubmit,
      id,
      onClose,
      onSubmit,
      open,
      position,
      projectId,
      t,
    } = this.props;
    return (
      <LycheeModal
        cancel="a, input, select, textarea"
        position={position}
        onOverlayClicked={onClose}
        isVisible={open}
      >
        <div
          className={classnames({
            [style['thin-container']]: true,
            [style.editing]: editing,
          })}
        >
          <form onSubmit={handleSubmit(onSubmit)}>
            <header>
              <ConsumerField
                name="name"
                component={TextField}
                label={t('name')}
                required
                render={(values) => (
                  <a href={`${ORIGIN_VERSIONS_PATH}/${id}`} target="_blank" rel="noreferrer">
                    {values.input.value}
                  </a>
                )}
              />
              {!this.isNew && (
                editing ? (
                  <i className={style.cancel} onClick={this.toggle} />
                ) : (
                  <i className={style.edit} onClick={this.toggle} />
                )
              )}
            </header>
            <div onScroll={TriggerMouseUpEventOnIE} className={style.body}>
              <ConsumerField
                name="description"
                type="text"
                label={t('description')}
                component={TextAreaField}
              />
              <ConsumerField
                name="status"
                label={t('status')}
                component={SelectField}
                options={STATUS_OPTIONS}
              />
              <ConsumerField
                name="wiki_page_title"
                label={t('wiki_page')}
                component={TextField}
                render={(values) => (
                  <a
                    href={`${projectWikiPath(projectId)}/${values.input.value}`}
                    target="_blank"
                    rel="noreferrer"
                  >
                    {values.input.value}
                  </a>
                )}
              />
              <ConsumerField
                name="start_date"
                label={t('start_date')}
                component={DateField}
                dateFormat={dateFormat}
              />
              <ConsumerField
                name="effective_date"
                label={t('due_date')}
                component={DateField}
                dateFormat={dateFormat}
              />
              <ConsumerField
                name="sharing"
                label={t('sharing')}
                component={SelectField}
                options={fieldOptions.sharings}
              />
              <FormSection name="custom_field_values" className={style.section}>
                {customFields.map((field) => (
                  <ConsumerField
                    key={field.id}
                    name={String(field.id)}
                    label={field.name}
                    safe
                    required={field.is_required}
                    hasBlank={!field.default_value}
                    type={field.field_format}
                    editStyle={_.get(field, ['format_store', 'edit_tag_style'])}
                    options={field.possible_values}
                    multiple={field.multiple}
                    dateFormat={dateFormat}
                    component={CustomField}
                  />
                ))}
              </FormSection>
            </div>
            {editing && (
              <footer>
                <button
                  type="button"
                  onClick={this.cancel}
                  className={style.cancel}
                >
                  {t('Button:cancel')}
                </button>
                <button type="submit" className={style.save}>
                  {t('Button:save')}
                </button>
              </footer>
            )}
          </form>
        </div>
      </LycheeModal>
    );
  }
}

VersionForm.defaultProps = {
  customFields: [],
  dateFormat: '',
  fieldOptions: { sharings: [], },
  id: null,
  onClose: null,
  open: false,
  position: {
    x: 0,
    y: 0,
  },
  projectId: null,
  safeAttributes: [],
};

VersionForm.propTypes = {
  customFields: PropTypes.array,
  dateFormat: PropTypes.string,
  fieldOptions: PropTypes.shape({ sharings: PropTypes.array, }),
  id: PropTypes.number,
  onClose: PropTypes.func,
  onSubmit: PropTypes.func.isRequired,
  open: PropTypes.bool,
  position: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
  }),
  projectId: PropTypes.number,
  safeAttributes: PropTypes.array,
  // ReduxFrom Props
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
};

VersionForm.childContextTypes = {
  editing: PropTypes.bool,
  safeAttributes: PropTypes.array,
};

export default withNamespaces(['VersionForm', 'Button'])(VersionForm);
