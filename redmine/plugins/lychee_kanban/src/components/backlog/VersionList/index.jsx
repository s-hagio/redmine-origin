import React from 'react';
import PropTypes from 'prop-types';

import { Version as VersionRecord } from '@/models/version';
import * as ItemTypes from '@/constants/itemTypes';
import Version from '@/containers/backlog/Version';
import CustomDragScrollLayer from '@/components/CustomDragScrollLayer';
import style from './style.css';

const VersionList = ({ versions }) => (
  <div id="backlog-version-list" className={style.container}>
    {versions.map((version) => (
      <Version key={version.id} version={version} versions={versions} />
    ))}
    <CustomDragScrollLayer
      excepts={[ItemTypes.MEMBERSHIP]}
      horizontalScrollTarget="backlog-version-list"
    />
  </div>
);

VersionList.defaultProps = { versions: [], };

VersionList.propTypes = { versions: PropTypes.arrayOf(PropTypes.instanceOf(VersionRecord)), };

export default VersionList;
