import React from 'react';
import { shallow } from 'enzyme';

import { VersionList as VersionListRecord } from '@/models/version';
import Version from '@/containers/backlog/Version';
import VersionList from './index';

describe('VersionList', () => {
  const props = {
    versions: new VersionListRecord({
      versions: [
        { id: 1 },
        { id: 2 },
        { id: 3 },
      ]
    }).toArray(),
  };

  it('shows Version Component as children with versions props', () => {
    const versionList = shallow(<VersionList {...props} />);
    expect(versionList.find(Version)).toHaveLength(3);
  });
});
