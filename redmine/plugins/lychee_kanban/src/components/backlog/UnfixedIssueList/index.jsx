import React from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';

import Sidebar from '@/components/Sidebar';
import IssueList from '@/components/backlog/IssueList';

const UnfixedIssueList = ({ onAddIssue, issues, t }) => {
  const activeIssueCount = issues.filter((issue) => !issue.closed).length;
  return (
    <Sidebar
      title={t('backlog')}
      issueCount={activeIssueCount}
      onAddIssue={onAddIssue}
    >
      <IssueList issues={issues} />
    </Sidebar>
  );
};

UnfixedIssueList.defaultProps = {
  issues: [],
  onAddIssue: null,
};

UnfixedIssueList.propTypes = {
  issues: PropTypes.array,
  onAddIssue: PropTypes.func,
  t: PropTypes.func.isRequired,
};

export default withNamespaces('UnfixedIssueList')(UnfixedIssueList);
