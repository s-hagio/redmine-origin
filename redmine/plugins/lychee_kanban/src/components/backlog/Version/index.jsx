import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classNames from 'classnames';
import { withNamespaces } from 'react-i18next';
import { withState } from 'recompose';
import { FaPlus, FaEllipsisV } from 'react-icons/fa';

import ICON_DUPLICATE from '@/images/duplicate.png';
import QuickNewIssueFormContainer from '@/containers/backlog/QuickNewIssueFormContainer';
import Link from '@/components/Link';
import IssueList from '@/components/backlog/IssueList';
import Dropdown from '@/components/Dropdown';
import MoveIssuesMenu from '@/components/backlog/MoveIssuesMenu';
import { Version as VersionRecord } from '@/models/version';
import { useRedmineTimespan } from '@/hooks/RedmineTimespan';
import styles from './style.css';

const parseDate = (str, dateFormat = 'YYYY-MM-DD') => {
  if (str) {
    return moment(str).format(dateFormat);
  }
  return '';
};

const Version = (props) => {
  const {
    costs,
    costType,
    dateFormat,
    dueDate,
    estimatedVelocity,
    id,
    issues,
    isClosed,
    name,
    onAddIssue,
    onShow,
    onToggle,
    onMoveOpenIssues,
    onDestroy,
    open,
    versions,
    setOpen,
    showEstimate,
    startDate,
    style,
    t,
  } = props;

  const costUnits = React.useMemo(
    () => ({
      points: 'pt',
      total_estimated_hours: 'h',
      issue_count: t('count'),
    }),
    [t],
  );

  // for some reason jest tests require digits = 0 here...
  const formatTimespan = useRedmineTimespan();

  return (
    <div
      className={classNames({
        [styles.root]: true,
        [styles.close]: isClosed,
      })}
      style={style}
    >
      <div className={styles.header}>
        {onAddIssue && (
          <div className={styles.addIcon} onClick={onAddIssue}>
            <FaPlus size={24} />
          </div>
        )}
        <div className={styles.headerBody}>
          <div className={styles.title}>{name}</div>
          <div className={styles.period}>
            <div>{parseDate(startDate, dateFormat)}</div>
            <div>
              ~
              {parseDate(dueDate, dateFormat)}
            </div>
          </div>
        </div>
        <Dropdown
          className={classNames([styles.menuIcon, styles.dropdown])}
          trigger={<FaEllipsisV size={24} />}
        >
          <div className={styles.dropdownContent}>
            <div className={styles.dropdownItem}>
              <a onClick={onShow}>{t('details')}</a>
            </div>
            <div className={styles.dropdownItem}>
              <a onClick={onToggle}>{isClosed ? t('reopen_sprint') : t('finish_sprint')}</a>
            </div>
            <MoveIssuesMenu
              versions={versions.filter((v) => v.id !== id)}
              onMoveOpenIssues={onMoveOpenIssues}
            />
            <div className={styles.dropdownItem}>
              <a onClick={onDestroy} className={styles.danger}>
                {t('delete')}
              </a>
            </div>
          </div>
        </Dropdown>
      </div>
      <div className={styles.pointDetails}>
        <div className={styles.estimate}>
          {showEstimate && (
            <div>
              <span>{t('estimated_velocity')}</span>
              <span className={styles.estimatePoint}>
                {formatTimespan(estimatedVelocity)}
              </span>
              <small>{costUnits[costType]}</small>
            </div>
          )}
        </div>
        <div
          className={classNames({
            [styles.total]: true,
            [styles.over]: costs[costType] > estimatedVelocity,
          })}
        >
          <div className={styles.totalCircle}>
            {formatTimespan(costs[costType])}
          </div>
          <div>
            <small>{costUnits[costType]}</small>
          </div>
        </div>
      </div>
      <div className={styles.container}>
        <div className={styles.containerFluid}>
          <IssueList
            costType={costType}
            issues={issues}
            versionId={id}
            estimatedVelocity={estimatedVelocity}
            showEstimate={showEstimate}
          />
        </div>
      </div>
      <div className={styles.footer}>
        {open ? (
          <QuickNewIssueFormContainer versionId={id} onClose={() => setOpen(false)} />
        ) : (
          <div className={styles.quick}>
            <Link
              label={t('SimpleIssueForm:label')}
              icon={ICON_DUPLICATE}
              onClick={() => setOpen(true)}
            />
          </div>
        )}
      </div>
    </div>
  );
};

Version.propTypes = {
  costs: PropTypes.object,
  costType: PropTypes.string,
  dateFormat: PropTypes.string,
  dueDate: PropTypes.string,
  estimatedVelocity: PropTypes.number,
  id: PropTypes.number,
  isClosed: PropTypes.bool,
  issues: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  onAddIssue: PropTypes.func.isRequired,
  onShow: PropTypes.func,
  onToggle: PropTypes.func,
  onMoveOpenIssues: PropTypes.func,
  onDestroy: PropTypes.func,
  open: PropTypes.bool,
  versions: PropTypes.arrayOf(PropTypes.instanceOf(VersionRecord)),
  setOpen: PropTypes.func,
  showEstimate: PropTypes.bool,
  startDate: PropTypes.string,
  style: PropTypes.object,
  t: PropTypes.func.isRequired,
};

Version.defaultProps = {
  costs: {},
  costType: '',
  dateFormat: '',
  dueDate: '',
  estimatedVelocity: 0,
  id: null,
  isClosed: false,
  open: false,
  onShow: null,
  onToggle: null,
  onMoveOpenIssues: () => { },
  onDestroy: null,
  versions: [],
  setOpen: () => { },
  showEstimate: false,
  startDate: '',
  style: {},
};

export const StatelessVersion = withNamespaces('Version')(Version);
export default withState('open', 'setOpen', false)(StatelessVersion);
