import React from 'react';
import { shallow } from 'enzyme';

import Link from '@/components/Link';
import QuickNewIssueFormContainer from '@/containers/backlog/QuickNewIssueFormContainer';
import { StatelessVersion as Version } from './index';

const dateFormat = 'DD.MM.YYYY';

describe('Version', () => {
  const props = {
    id: 1,
    name: 'name',
    startDate: '1990-01-01',
    dueDate: '2000-01-01',
    status: 'open',
    dateFormat,
    onAddIssue: jest.fn(),
    issues: [],
  };

  it('shows name, sprint period, and workload', () => {
    const version = shallow(<Version {...props} />);
    expect(version.find('.title').text()).toEqual('name');
    expect(version.find('.period').text()).toEqual('01.01.1990~01.01.2000');
  });

  it('Monthly Full Name startDate and dueDate', () => {
    const version = shallow(<Version {...props} dateFormat="DD MMMM YYYY" />);
    expect(version.find('.period').text()).toEqual(
      '01 January 1990~01 January 2000'
    );
  });

  it('Monthly Abbreviation Name startDate and dueDate', () => {
    const version = shallow(<Version {...props} dateFormat="DD MMM YYYY" />);
    expect(version.find('.period').text()).toEqual('01 Jan 1990~01 Jan 2000');
  });

  it('shows "このスプリントを終了する" when status is "open"', () => {
    const version = shallow(<Version {...props} />);
    const node = version.find('.dropdown a').at(1);
    expect(node.text()).toEqual('finish_sprint');
  });

  it('shows "このスプリントを再開する" when status is not "open"', () => {
    const version = shallow(<Version {...props} isClosed />);
    const node = version.find('.dropdown a').at(1);
    expect(node.text()).toEqual('reopen_sprint');
  });

  it('shows estimated costs', () => {
    const version = shallow(
      <Version
        {...props}
        showEstimate
        costType="points"
        estimatedVelocity={5}
        costs={{ points: 10 }}
      />
    );
    expect(version.find('.estimate').text()).toEqual('estimated_velocity5pt');
    expect(version.find('.total').text()).toEqual('10pt');
  });

  it('calls onAddIssue props', () => {
    const onAddIssue = jest.fn();
    const version = shallow(<Version {...props} onAddIssue={onAddIssue} />);
    version.find('.addIcon').simulate('click');
    expect(onAddIssue).toBeCalled();
  });

  it('calls onShow props', () => {
    const onShow = jest.fn();
    const version = shallow(<Version {...props} onShow={onShow} />);
    version.find('.dropdown a').at(0).simulate('click');
    expect(onShow).toBeCalled();
  });

  it('calls onToggle props', () => {
    const onToggle = jest.fn();
    const version = shallow(<Version {...props} onToggle={onToggle} />);
    version.find('.dropdown a').at(1).simulate('click');
    expect(onToggle).toBeCalled();
  });

  it('calls onDestroy props', () => {
    const onDestroy = jest.fn();
    const version = shallow(<Version {...props} onDestroy={onDestroy} />);
    version.find('.dropdown a').at(2).simulate('click');
    expect(onDestroy).toBeCalled();
  });

  it('backlog hides tool icon', () => {
    const version = shallow(<Version {...props} isBacklog />);
    expect(version.find('.dropdown li a').length).toEqual(0);
  });

  it('shows QuickIssueForm when "open" = true', () => {
    let version = shallow(<Version {...props} />);
    const link = version.find(Link);
    expect(link.exists()).toEqual(true);
    expect(link.render().text()).toEqual('SimpleIssueForm:label');

    version = shallow(<Version {...props} open />);
    expect(version.find(QuickNewIssueFormContainer).exists()).toEqual(true);
  });
});
