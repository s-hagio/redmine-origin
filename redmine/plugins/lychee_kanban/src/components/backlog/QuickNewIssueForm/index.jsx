import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import OutsideClickHandler from 'react-outside-click-handler';

import style from './style.css';

class QuickNewIssueForm extends Component {
  constructor() {
    super();

    this.state = {
      subject: '',
      tracker: null,
    };
  }

  componentDidMount() {
    this.input.focus();
  }

  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };

  handleSubmit = (event) => {
    const { onSubmit, trackers } = this.props;
    const { subject, tracker } = this.state;
    this.setState({ subject: '' });
    event.preventDefault();
    onSubmit({
      subject,
      tracker_id: tracker || trackers[0].id,
    });
  };

  render() {
    const { subject } = this.state;
    const { onClose, t, trackers } = this.props;

    return (
      <OutsideClickHandler onOutsideClick={onClose}>
        <form className={style.root} onSubmit={this.handleSubmit}>
          <div>
            <label>{`${t('SimpleIssueForm:tracker')}:`}</label>
            <select onChange={this.handleChange('tracker')}>
              {trackers.map((tracker) => (
                <option key={tracker.id} value={tracker.id}>
                  {tracker.name}
                </option>
              ))}
            </select>
            <a
              className={`icon-only icon-close close-icon ${style.close_button}`}
              onClick={onClose}
            />
          </div>
          <input
            type="text"
            value={subject}
            onChange={this.handleChange('subject')}
            ref={(input) => {
              this.input = input;
            }}
          />
        </form>
      </OutsideClickHandler>
    );
  }
}

QuickNewIssueForm.defaultProps = {
  onClose: () => {},
  onSubmit: () => {},
  trackers: [],
};

QuickNewIssueForm.propTypes = {
  onClose: PropTypes.func,
  onSubmit: PropTypes.func,
  t: PropTypes.func.isRequired,
  trackers: PropTypes.array,
};

export default withNamespaces(['IssueForm', 'Button'])(QuickNewIssueForm);
