import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withState } from 'recompose';
import { withNamespaces } from 'react-i18next';
import { FaAngleRight, FaAngleDown } from 'react-icons/fa';

import { Version } from '@/models/version';
import styles from './style.css';

const MoveIssuesMenu = ({ setOpen, open, versions, t, onMoveOpenIssues }) => {
  const toggleMoveMenu = (event) => {
    event.stopPropagation();
    setOpen(!open);
  };

  return (
    <div>
      <div className={styles.dropdownItem}>
        <a onClick={toggleMoveMenu}>
          {open ? <FaAngleDown /> : <FaAngleRight />}
          {t('move_opened_issues')}
        </a>
      </div>
      {open
        ? versions.map((version) => (
          <div
            key={version.id}
            className={classNames([
              styles.dropdownItem,
              styles.dropdownItemChildren,
            ])}
          >
            <a onClick={() => onMoveOpenIssues(version.id)}>
              {version.get('name')}
            </a>
          </div>
        ))
        : null}
    </div>
  );
};

MoveIssuesMenu.propTypes = {
  setOpen: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  versions: PropTypes.arrayOf(PropTypes.instanceOf(Version)),
  t: PropTypes.func.isRequired,
  onMoveOpenIssues: PropTypes.func,
};

MoveIssuesMenu.defaultProps = {
  versions: [],
  onMoveOpenIssues: () => {},
};

export const StatelessMoveIssuesMenu = withNamespaces('Version')(MoveIssuesMenu);
export default withState('open', 'setOpen', false)(StatelessMoveIssuesMenu);
