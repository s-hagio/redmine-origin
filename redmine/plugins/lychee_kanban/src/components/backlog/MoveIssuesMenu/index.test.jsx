import React from 'react';
import { shallow } from 'enzyme';

import { Version } from '@/models/version';
import { StatelessMoveIssuesMenu as MoveIssuesMenu } from './index';

describe('MoveIssueMenu', () => {
  const props = {
    open: false,
    setOpen: () => {},
    onMoveOpenIssues: null,
    versions: [],
  };

  it('calls onMoveOpenIssues props', () => {
    const onMoveOpenIssues = jest.fn();
    const versions = [new Version({ id: 1, name: 'hoge' })];
    const open = true;
    const moveIssuesMenu = shallow(
      <MoveIssuesMenu {...props} {...{ onMoveOpenIssues, versions, open }} />
    ); // eslint-disable-line max-len
    moveIssuesMenu.find('.dropdownItemChildren a').at(0).simulate('click');
    expect(onMoveOpenIssues).toBeCalled();
  });
});
