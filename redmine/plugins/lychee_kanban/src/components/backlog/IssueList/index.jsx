import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { If, Then } from 'react-if';
import styled from 'styled-components';
import * as _ from 'lodash';

import Issue from '@/containers/backlog/Issue';
import IssueDropArea from '@/containers/backlog/IssueDropArea';
import CustomDragScrollLayer from '@/components/CustomDragScrollLayer';
import ClosedIssueList from '@/components/backlog/ClosedIssueList';
import style from './style.css';

const StyledIssue = styled(Issue)`
  .cost {
    display: inline-flex;
  }

  .node-icon {
    display: none !important;
  }

  .main {
    align-items: center;
    flex-grow: 1;
  }

  .right-box {
    flex-direction: row;
    align-items: center;
    justify-content: flex-end;
    min-width: 72px;

    .due-date,
    .assigned-to {
      min-width: 36px;
    }

    .due-date {
      justify-content: flex-end;
    }

    .assigned-to {
      flex-grow: unset;
    }
  }

  &.closed {
    background-color: #ddd;
  }

  .status-list {
    flex-grow: 0;
  }

  .estimated-hours {
    display: none;
  }
`;

const renderIssue = (issue, showBorder, isOver) => (
  <div key={issue.id} id={`issue-${issue.id}`}>
    <If condition={showBorder && isOver(issue)}>
      <Then>
        <div className={style.pointBorder} />
      </Then>
    </If>
    <StyledIssue key={issue.id} issue={issue} />
  </div>
);

class IssueList extends Component {
  componentDidUpdate({ issues: prevIssues }) {
    const { issues } = this.props;
    if (issues !== prevIssues) {
      const target = _.first(_.difference(issues, prevIssues));
      if (target) {
        const targetEl = document.getElementById(`issue-${target.id}`);
        if (targetEl) {
          this.scrollIntoViewIfNeeded(targetEl);
        }
      }
    }
  }

  scrollIntoViewIfNeeded(target) {
    const offset = this.container.getBoundingClientRect();
    const { top, bottom } = target.getBoundingClientRect();
    const diffs = { top: top - offset.top, bottom: bottom - offset.bottom };
    if (diffs.top < 0) {
      this.container.scrollTop += diffs.top;
    }
    if (diffs.bottom > 0) {
      this.container.scrollTop += diffs.bottom;
    }
  }

  render() {
    const {
      costType,
      issues,
      showEstimate,
      versionId,
      estimatedVelocity
    } = this.props;

    let showBorder = showEstimate;
    let costs = 0;
    // TODO: 赤線の描画処理のリファクタ
    const isOver = (issue) => {
      costs += issue.costs[costType];
      const result = costs > estimatedVelocity;
      if (result) {
        showBorder = false;
      }
      return result;
    };
    const activeIssueIds = issues
      .filter((issue) => !issue.closed)
      .map((issue) => issue.id);

    return (
      <IssueDropArea
        className={style.container}
        versionId={versionId}
        issueIdsList={activeIssueIds}
      >
        <div
          id={`backlog-issue-list-${versionId}`}
          className={style.list}
          ref={(ref) => {
            this.container = ref;
          }}
        >
          {versionId ? (
            <ClosedIssueList>
              {issues.map((issue) => {
                if (!issue.closed) {
                  return null;
                }
                return renderIssue(issue, showBorder, isOver);
              })}
            </ClosedIssueList>
          ) : null}
          {issues.map((issue) => {
            if (issue.closed) {
              return null;
            }
            return (
              <IssueDropArea
                key={issue.id}
                versionId={versionId}
                issue={issue}
                issueIdsList={activeIssueIds}
              >
                {renderIssue(issue, showBorder, isOver)}
              </IssueDropArea>
            );
          })}
        </div>
        <CustomDragScrollLayer
          verticalScrollTarget={`backlog-issue-list-${versionId}`}
          horizontalScrollTarget={`backlog-issue-list-${versionId}`}
        />
      </IssueDropArea>
    );
  }
}

IssueList.propTypes = {
  costType: PropTypes.oneOf(['points', 'total_estimated_hours', 'issue_count']),
  estimatedVelocity: PropTypes.number,
  issues: PropTypes.array.isRequired,
  showEstimate: PropTypes.bool,
  versionId: PropTypes.number,
};

IssueList.defaultProps = {
  costType: 'total_estimated_hours',
  estimatedVelocity: 0,
  showEstimate: false,
  versionId: null,
};

export default IssueList;
