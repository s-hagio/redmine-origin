import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';

import ICON_EXPANDED from '@/images/expanded.png';
import ICON_COLLAPSED from '@/images/collapsed.png';
import style from './style.css';

class ClosedIssueList extends Component {
  constructor() {
    super();
    this.state = { opened: false };
    this.toggleStatus = this.toggleStatus.bind(this);
  }

  toggleStatus() {
    const { opened } = this.state;
    this.setState({ opened: !opened });
  }

  render() {
    const { children, t } = this.props;
    const { opened } = this.state;

    if (children.every((e) => e === null)) {
      return null;
    }

    return (
      <div className={style.container}>
        <div className={style.header} onClick={this.toggleStatus}>
          <img
            src={opened ? ICON_EXPANDED : ICON_COLLAPSED}
            alt={opened ? 'expanded' : 'collapsed'}
          />
          <b>{t('title')}</b>
        </div>
        <div className={opened ? null : style.closed}>
          {children}
        </div>
      </div>
    );
  }
}

ClosedIssueList.defaultProps = { children: null, };

ClosedIssueList.propTypes = {
  children: PropTypes.node,
  t: PropTypes.func.isRequired,
};

export default withNamespaces('ClosedIssueList')(ClosedIssueList);
