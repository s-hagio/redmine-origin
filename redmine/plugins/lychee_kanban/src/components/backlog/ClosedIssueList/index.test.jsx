import React from 'react';
import { shallow } from 'enzyme';

import ClosedIssueList from './index';

describe('ClosedIssueList', () => {
  const props = { children: [<div />] };

  it('toggles with status', () => {
    const component = shallow(<ClosedIssueList {...props} />);
    expect(component.find('.closed').length).toEqual(1);
    component.find('.header').simulate('click');
    expect(component.find('.closed').length).toEqual(0);
  });
});
