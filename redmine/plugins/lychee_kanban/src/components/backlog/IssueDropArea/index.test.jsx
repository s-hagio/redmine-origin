import React from 'react';
import { shallow } from 'enzyme';
import { useDrop } from 'react-dnd';

import IssueDropArea from './index';

const HOVER_COLOR = 'rgba(255, 255, 0, 0.5)';
const HIGHLIGHT_COLOR = 'rgba(255, 255, 0, 0.2)';

describe('IssueDropArea', () => {
  // TODO: react-dndのdocを参考にD&D時のテストを実装
  it('enables hovered style when hovered is true', () => {
    let dropArea = shallow(<IssueDropArea />);
    expect(dropArea.props().style).toHaveProperty(
      'backgroundColor',
      'transparent'
    );

    useDrop.mockReturnValue([{ hovered: true }, null]);
    dropArea = shallow(<IssueDropArea />);
    expect(dropArea.props().style).toHaveProperty(
      'backgroundColor',
      HOVER_COLOR
    );

    useDrop.mockReturnValue([{ highlighted: true }, null]);
    dropArea = shallow(<IssueDropArea highlighted />);
    expect(dropArea.props().style).toHaveProperty(
      'backgroundColor',
      HIGHLIGHT_COLOR
    );
  });
});
