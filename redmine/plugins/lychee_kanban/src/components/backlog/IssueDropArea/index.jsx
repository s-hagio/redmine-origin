import React from 'react';
import PropTypes from 'prop-types';
import { useDrop } from 'react-dnd';
import * as _ from 'lodash';

import * as ItemTypes from '@/constants/itemTypes';

const HOVER_COLOR = 'rgba(255, 255, 0, 0.5)';
const HIGHLIGHT_COLOR = 'rgba(255, 255, 0, 0.2)';

const getBackgroundColor = (highlighted, hovered) => {
  if (hovered) {
    return HOVER_COLOR;
  }
  if (highlighted) {
    return HIGHLIGHT_COLOR;
  }
  return 'transparent';
};

const IssueDropArea = ({
  issue,
  className,
  children,
  sortable,
  sortIssues,
  version,
}) => {
  const [{ highlighted, hovered }, drop] = useDrop({
    accept: ItemTypes.ISSUE,
    drop: (sourceProps, monitor) => {
      if (monitor.didDrop()) {
        return;
      }

      const {
        id: sourceIssueId,
        fixedVersionId: sourceVersionId,
        startDate: sourceStartDate,
        dueDate: sourceDueDate,
      } = sourceProps;
      const targetIssue = issue || {};
      if (_.isEqual(sourceIssueId, targetIssue.id)) {
        return;
      }

      const targetVersion = version || { id: '' };
      const versionChanged = Number(sourceVersionId || 0) !== Number(targetVersion.id || 0);
      sortIssues({
        payload: {
          id: sourceIssueId,
          fixed_version_id: targetVersion.id,
          start_date: versionChanged
            ? targetVersion.start_date
            : sourceStartDate,
          due_date: versionChanged ? targetVersion.due_date : sourceDueDate,
          before_issue_id: sortable && issue ? issue.id : null,
          issue_index: !issue ? -1 : null,
        },
        meta: { previousFixedVersionId: sourceVersionId },
      });
    },
    canDrop: (sourceProps) => {
      const { fixedVersionId: sourceVersionId } = sourceProps;
      const targetIssue = issue || {};
      const targetVersion = version || {};
      const versionChanged = Number(sourceVersionId || 0) !== Number(targetVersion.id || 0);
      return !targetIssue.closed || versionChanged;
    },
    collect: (monitor) => ({
      highlighted: monitor.canDrop(),
      hovered: monitor.canDrop() && monitor.isOver({ shallow: true }),
    }),
  });

  const style = {
    position: 'relative',
    zIndex: 5,
    backgroundColor: getBackgroundColor(highlighted, hovered),
    paddingTop: issue && hovered && 32,
  };

  return (
    <div className={className} style={style} ref={drop}>
      {children}
    </div>
  );
};

IssueDropArea.defaultProps = {
  issue: null,
  className: '',
  children: null,
  sortable: false,
  version: null,
};

IssueDropArea.propTypes = {
  issue: PropTypes.object,
  children: PropTypes.any,
  className: PropTypes.string,
  sortable: PropTypes.bool,
  sortIssues: PropTypes.func.isRequired,
  version: PropTypes.object,
};

export default IssueDropArea;
