import React from 'react';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd';

import { IssueProvider } from '@/components/Issue';
import NewVersion from '@/containers/backlog/NewVersion';
import VersionList from '@/containers/backlog/VersionList';
import VersionForm from '@/containers/backlog/VersionForm';
import UnfixedIssueList from '@/containers/backlog/UnfixedIssueList';
import MembershipList from '@/containers/MembershipList';
import LoadingPanel from '@/containers/LoadingPanel';
import IssueForm from '@/containers/IssueForm';
import Velocity from '@/containers/backlog/Velocity';
import style from './style.css';

const App = () => (
  <DndProvider backend={HTML5Backend}>
    <IssueProvider>
      <div className={style.container}>
        <div className={style.contentHeader}>
          <MembershipList />
          <NewVersion className={style.new} />
        </div>
        <VersionList />
        <VersionForm />
        <IssueForm backlogPointEnabled />
        <LoadingPanel />
        <UnfixedIssueList />
        <Velocity target="velocity" />
      </div>
    </IssueProvider>
  </DndProvider>
);

export default App;
