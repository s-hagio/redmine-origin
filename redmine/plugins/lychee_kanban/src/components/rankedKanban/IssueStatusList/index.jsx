import React from 'react';
import PropTypes from 'prop-types';
import style from './style.css';

const IssueStatusList = ({ issue_statuses, issue_count }) => (
  <div className={style.container}>
    <div className={style.empty} />
    {issue_statuses.map((issue_status) => (
      <div key={issue_status.id} className={style.status}>
        {`${issue_status.name}(${issue_count[issue_status.id]})`}
      </div>
    ))}
  </div>
);

IssueStatusList.propTypes = {
  issue_statuses: PropTypes.array.isRequired,
  issue_count: PropTypes.object.isRequired,
};

export default IssueStatusList;
