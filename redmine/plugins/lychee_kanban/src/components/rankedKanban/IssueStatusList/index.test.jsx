import React from 'react';
import { shallow } from 'enzyme';

import IssueStatusList from './index';

describe('IssueStatusList', () => {
  let list;
  const props = {
    issue_statuses: [
      { id: 1, name: 'issue_status1' },
      { id: 2, name: 'issue_status2' },
      { id: 3, name: 'issue_status3' },
    ],
    issue_count: { 1: 2, 2: 0, 3: 4 },
  };

  beforeEach(() => {
    list = shallow(<IssueStatusList {...props} />);
  });

  it('renders issue statuses', () => {
    const issueStatuses = list.find('.status');
    expect(issueStatuses.length).toEqual(3);
    expect(list.text()).toEqual(
      'issue_status1(2)issue_status2(0)issue_status3(4)'
    );
  });
});
