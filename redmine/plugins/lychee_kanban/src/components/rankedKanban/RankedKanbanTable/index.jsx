import React from 'react';
import PropTypes from 'prop-types';

import * as ItemTypes from '@/constants/itemTypes';
import IssueTableRow from '@/containers/rankedKanban/IssueTableRow';
import CustomSortContainer from '@/components/CustomSortContainer';
import CustomDragScrollLayer from '@/components/CustomDragScrollLayer';
import style from './style.css';

const RankedKanbanTable = (props) => {
  const {
    issues,
    onSort,
  } = props;

  return (
    <div id="ranked-kanban-table">
      <CustomSortContainer
        className={style.body}
        items={issues}
        onSort={onSort}
        render={(issue, options) => (
          <IssueTableRow
            key={issue.id}
            issue={issue}
            onSort={props.onSort}
            {...options}
          />
        )}
      />
      <CustomDragScrollLayer
        excepts={[ItemTypes.MEMBERSHIP]}
        horizontalScrollTarget="ranked-kanban-table"
      />
    </div>
  );
};

RankedKanbanTable.defaultProps = { issues: [], };

RankedKanbanTable.propTypes = {
  issues: PropTypes.array,
  onSort: PropTypes.func.isRequired,
};

export default RankedKanbanTable;
