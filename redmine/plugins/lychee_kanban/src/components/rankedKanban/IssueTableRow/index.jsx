import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useDrag, useDrop } from 'react-dnd';
import styled from 'styled-components';

import * as ItemTypes from '@/constants/itemTypes';
import Issue from '@/containers/rankedKanban/Issue';
import IssueTableColumn from '@/containers/rankedKanban/IssueTableColumn';
// HACK: lint 違反の修正
// eslint-disable-next-line import/no-cycle
import IssueTableRowContainer from '@/containers/rankedKanban/IssueTableRow';
import QuickNewIssueContainer from '@/containers/rankedKanban/QuickNewIssueContainer';
import CustomSortContainer from '@/components/CustomSortContainer';
import style from './style.css';
import { AutoloadManager } from '@/autoload_manager';

const StyledIssue = styled(Issue)`
  .node-icon {
    display: none !important;
  }

  .rankedKanban .left .dueDate {
    display: none;
  }

  .rankedKanban.wrapper {
    border: 1px solid #ddd;
  }

  .rankedKanban.wrapper .tracker {
    min-width: 2px;
    width: 2px;
  }

  .rankedKanban.wrapper.root .tracker,
  .rankedKanban.wrapper.seed .tracker {
    min-width: 12px;
    width: 12px;
  }
`;

const indent = (depth) => {
  const calculatedDepth = (90 * depth) / (depth + 5);
  return calculatedDepth < 60 ? calculatedDepth : 60;
};

const IssueTableRow = (props) => {
  const { issue, issues, issueStatuses, onSort, onHover, onDrop, draggable } = props;

  const [{ isDragging }, drag, preview] = useDrag({
    item: { type: ItemTypes.ISSUE_ROW },
    begin: () => ({ issue }),
    end: () => {
      onDrop(issue);
    },
    canDrag() {
      return draggable;
    },
    collect: (monitor) => ({ isDragging: monitor.isDragging(), }),
  });

  const [{ canDrop }, drop] = useDrop({
    accept: ItemTypes.ISSUE_ROW,
    hover: (sourceProps) => {
      const { issue: sourceIssue } = sourceProps;
      const sameParent = sourceIssue.parent.id === issue.parent.id;
      if (sameParent) {
        onHover(sourceIssue, issue);
      }
    },
    canDrop: (sourceProps) => {
      const { issue: sourceIssue } = sourceProps;
      const sameIssue = sourceIssue.id === issue.id;
      const sameParent = sourceIssue.parent.id === issue.parent.id;
      return !sameIssue && sameParent;
    },
    collect: (monitor) => ({ canDrop: monitor.canDrop(), }),
  });

  const [open, setOpen] = React.useState(false);
  const handleOpen = React.useCallback(() => {
    setOpen(true);
    AutoloadManager.stop();
  }, []);
  const handleClose = React.useCallback(() => {
    setOpen(false);
    AutoloadManager.restart();
  }, []);
  const columnStyle = React.useMemo(() => ({
    paddingLeft: `${indent(issue.depth)}px`,
  }), [issue.depth]);

  return (
    <div
      className={classNames({
        [style.container]: true,
        [style.dragging]: isDragging,
      })}
    >
      <div style={{ display: 'none' }} ref={preview} />
      <div
        className={classNames({
          [style.row]: true,
          [style.highlight]: canDrop,
        })}
        ref={drop}
      >
        <div
          className={classNames({
            [style.handle]: true,
            [style.draggable]: draggable,
          })}
          ref={drag}
        />
        {issueStatuses.map((status) => (
          <IssueTableColumn
            key={status.id}
            status={status}
            issueId={issue.id}
            className={style.column}
            style={columnStyle}
          >
            {status.id === issue.status.id && (
              <StyledIssue issue={issue} onAddChild={handleOpen} />
            )}
          </IssueTableColumn>
        ))}
      </div>
      <CustomSortContainer
        items={issues}
        onSort={onSort}
        render={(item, options) => (
          <IssueTableRowContainer
            key={item.id}
            issue={item}
            onSort={onSort}
            {...options}
          />
        )}
      />
      <QuickNewIssueContainer
        open={open}
        parent={issue.toJS()}
        onClose={handleClose}
        offsetTop={160}
        offsetBottom={54}
      />
    </div>
  );
};

IssueTableRow.propTypes = {
  issue: PropTypes.object.isRequired,
  issues: PropTypes.array,
  issueStatuses: PropTypes.array,
  onSort: PropTypes.func.isRequired,
  onHover: PropTypes.func.isRequired,
  onDrop: PropTypes.func.isRequired,
  draggable: PropTypes.bool,
};

IssueTableRow.defaultProps = {
  issues: [],
  issueStatuses: [],
  draggable: false,
};

export default IssueTableRow;
