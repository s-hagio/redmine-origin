import React from 'react';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd';

import { IssueProvider } from '@/components/Issue';
import IssueForm from '@/containers/IssueForm';
import IssueStatusList from '@/containers/rankedKanban/IssueStatusList';
import LoadingPanel from '@/containers/LoadingPanel';
import MembershipList from '@/containers/MembershipList';
import NewIssue from '@/containers/NewIssue';
import RankedKanbanTable from '@/containers/rankedKanban/RankedKanbanTable';
import WorkflowList from '@/containers/WorkflowList';
import SimpleIssueForm from '@/containers/SimpleIssueForm';
import StickyBar from '@/components/StickyBar';
import SyncScroll from '@/components/SyncScroll';
import ScrollHandler from '@/components/ScrollHandler';
import StickyFooter from '@/components/StickyFooter';
import styles from './style.css';

const TableContainingStickyFooterSpaces = () => (
  <SyncScroll hidebar className={styles.tableWrapper}>
    <RankedKanbanTable />
  </SyncScroll>
);

const RankedKanbanApp = () => (
  <DndProvider backend={HTML5Backend}>
    <IssueProvider>
      <StickyBar className={styles.stickyHeader}>
        <div className={styles.contentHeader}>
          <MembershipList />
          <div>
            <NewIssue className={styles.new} />
          </div>
        </div>
        <WorkflowList />
      </StickyBar>
      <StickyBar className={styles.stickyStatusHeader}>
        <SyncScroll hidebar>
          <IssueStatusList />
        </SyncScroll>
      </StickyBar>
      <StickyFooter
        className={styles.stickyFooter}
        targetTable={TableContainingStickyFooterSpaces}
      >
        <SimpleIssueForm />
        <ScrollHandler target="ranked-kanban-table" />
      </StickyFooter>
      <IssueForm />
      <LoadingPanel />
    </IssueProvider>
  </DndProvider>
);

export default RankedKanbanApp;
