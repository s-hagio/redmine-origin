import React from 'react';
import PropTypes from 'prop-types';
import { useDrag } from 'react-dnd';
import classNames from 'classnames';
import style from './style.css';
import * as ItemTypes from '@/constants/itemTypes';

const Tracker = (props) => {
  const { tracker } = props;
  const [{ isDragging }, preview] = useDrag({
    item: {
      type: ItemTypes.TRACKER,
      tracker,
    },
    collect: (monitor) => ({ isDragging: monitor.isDragging() }),
  });
  const backgroundColor = tracker.color;
  return (
    <div
      ref={preview}
      className={classNames({
        [style.container]: true,
        [style.dragging]: isDragging,
      })}
      style={{ backgroundColor }}
    >
      {tracker.name}
      <input type="hidden" name={tracker.input_name} value={tracker.workflow} />
    </div>
  );
};

Tracker.propTypes = {
  tracker: PropTypes.object.isRequired,
  workflow: PropTypes.number.isRequired,
};

export default Tracker;
