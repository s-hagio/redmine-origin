import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field as BaseField, FormSection } from 'redux-form';
import classnames from 'classnames';
import { withNamespaces } from 'react-i18next';

import { timeEntriesPath } from '@/constants/endpoints';
import CustomField from '@/components/LycheeForm/CustomField';
import {
  DateField,
  SelectField,
  TextField,
  TextAreaField,
} from '@/components/LycheeForm/fields';
import { useRedmineTimespan } from '@/hooks/RedmineTimespan';
import style from './style.css';

const Field = (props) => <BaseField editing {...props} />;

const FormattedTimespan = (props) => {
  const { hours, t } = props;
  const formatTimespan = useRedmineTimespan();
  return <>{t('n_hours', { hours: formatTimespan(hours) })}</>;
};
FormattedTimespan.propTypes = {
  hours: PropTypes.number.isRequired,
  t: PropTypes.func.isRequired,
};

class IssueTimeEntry extends Component {
  constructor() {
    super();
    this.toggle = this.toggle.bind(this);
    this.state = { editing: false, };
  }

  toggle() {
    const { editing } = this.state;
    this.setState({ editing: !editing, });
  }

  render() {
    const { editing } = this.state;
    const {
      activities,
      customFields,
      editable,
      handleSubmit,
      issueId,
      onSubmit,
      spentHours,
      totalSpentHours,
      visible,
      t,
    } = this.props;
    const handleKeyDown = (e) => {
      if (e.key === 'Enter' && (e.ctrlKey || e.metaKey)) {
        handleSubmit(onSubmit)();
      }
    };

    if (!visible && !editable) {
      return null;
    }

    return (
      <div
        className={classnames({
          [style.container]: true,
          [style.editing]: editing,
        })}
      >
        <div className={style.header}>
          {editable && (
            <div>
              <div className={style.label}>{t('time_entry_header')}</div>
              <div className={style['spent-hours']}>
                <span>
                  <FormattedTimespan hours={spentHours} t={t} />
                  {`  ${t('total')}:`}
                </span>
                <span>
                  <a href={timeEntriesPath(issueId)} target="_blank" rel="noreferrer">
                    <FormattedTimespan hours={totalSpentHours} t={t} />
                  </a>
                </span>
              </div>
            </div>
          )}
          {editable && (
            <div onClick={this.toggle}>
              <i className={style.icon} />
            </div>
          )}
        </div>
        {editing && (
          <div className={style.form}>
            <div className={style.row}>
              <div className={style.activity_id}>
                <Field
                  name="activity_id"
                  component={SelectField}
                  options={activities}
                  blank
                />
              </div>
              <div className={style.spent_on}>
                <Field name="spent_on" component={DateField} />
              </div>
              <div className={classnames({
                [style.hours]: true,
                [style.required]: true,
              })}
              >
                <Field name="hours" component={TextField} />
                <span>{t('hours')}</span>
              </div>
            </div>
            <div className={style.comments}>
              <Field name="comments" rows="3" component={TextAreaField} onKeyDown={(e) => handleKeyDown(e)} />
            </div>
            <FormSection name="custom_field_values" className={style.section}>
              {customFields.map((field) => (
                <div
                  key={field.id}
                  className={classnames({
                    [style.custom_field]: true,
                    [style.required]: field.is_required,
                  })}
                >
                  <label htmlFor={String(field.id)}>{field.name}</label>
                  <Field
                    name={String(field.id)}
                    required={field.is_required}
                    type={field.field_format}
                    onKeyDown={field.field_format === 'text' ? (e) => handleKeyDown(e) : false}
                    options={field.possible_values}
                    multiple={field.multiple}
                    component={CustomField}
                  />
                </div>
              ))}
            </FormSection>
            <div className={style.action}>
              <button type="button" onClick={handleSubmit(onSubmit)} className={style.submit}>
                {t('Button:create')}
              </button>
            </div>
          </div>
        )}
      </div>
    );
  }
}

IssueTimeEntry.defaultProps = {
  activities: [],
  customFields: [],
  editable: false,
  issueId: null,
  spentHours: 0.0,
  totalSpentHours: 0.0,
  visible: false,
};

IssueTimeEntry.propTypes = {
  activities: PropTypes.array,
  customFields: PropTypes.array,
  editable: PropTypes.bool,
  handleSubmit: PropTypes.func.isRequired,
  issueId: PropTypes.number,
  onSubmit: PropTypes.func.isRequired,
  spentHours: PropTypes.number,
  totalSpentHours: PropTypes.number,
  visible: PropTypes.bool,
  t: PropTypes.func.isRequired,
};

export default withNamespaces(['IssueTimeEntry', 'Button'])(IssueTimeEntry);
