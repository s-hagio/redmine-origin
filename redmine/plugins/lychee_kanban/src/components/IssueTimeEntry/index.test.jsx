import React from 'react';
import { shallow } from 'enzyme';

import IssueTimeEntry from './index';

describe('IssueTimeEntry', () => {
  const props = {
    activities: [
      { value: 0, text: 'zero' },
      { value: 1, text: 'one' },
    ],
    spent_hours: 2,
    total_spent_hours: 1,
    issueId: 0,
    handleSubmit: jest.fn((fn) => fn),
    onSubmit: jest.fn(),
  };

  it('renders with no status', () => {
    const component = shallow(<IssueTimeEntry {...props} />);
    expect(component.find('.header a').exists()).toBeFalsy();
    expect(component.find('.icon').exists()).toBeFalsy();
  });

  it('renders with canView props', () => {
    const component = shallow(<IssueTimeEntry {...props} editable />);
    expect(component.find('.header a').exists()).toBeTruthy();
    expect(component.find('.icon').exists()).toBeTruthy();
  });

  it('renders with canLog props', () => {
    const component = shallow(<IssueTimeEntry {...props} visible />);
    expect(component.find('.header a').exists()).toBeFalsy();
    expect(component.find('.icon').exists()).toBeFalsy();
  });

  it('renders with canLog and canView props', () => {
    const component = shallow(<IssueTimeEntry {...props} visible editable />);
    expect(component.find('.header a').exists()).toBeTruthy();
    expect(component.find('.icon').exists()).toBeTruthy();
  });

  it('toggles edit/cancel button', () => {
    const component = shallow(<IssueTimeEntry {...props} editable />);
    expect(component.state()).toHaveProperty('editing', false);
    component.find('.icon').parent().simulate('click');
    expect(component.state()).toHaveProperty('editing', true);
    expect(component.find('.form').exists()).toBeTruthy();
  });

  it('calls handleSubmit props', () => {
    const component = shallow(<IssueTimeEntry {...props} visible editable />);
    component.setState({ editing: true });
    expect(props.onSubmit).not.toBeCalled();
    component.find('button').simulate('click');
    expect(props.onSubmit).toBeCalled();
  });
});
