import React from 'react';
import PropTypes from 'prop-types';

import { stopPropagation } from '@/utils';
import {
  BooleanField,
  DateField,
  FileField,
  SelectField,
  TextField,
  TextAreaField,
} from './fields';

const CustomField = (props) => {
  const { type, formattedValue } = props;

  const render = React.useCallback(() => (
    <div
      // eslint-disable-next-line
      dangerouslySetInnerHTML={{ __html: formattedValue }}
      onClick={stopPropagation()}
    />
  ), [formattedValue]);

  switch (type) {
    case 'list':
    case 'enumeration':
    case 'version':
    case 'user':
      return <SelectField render={render} {...props} />;
    case 'attachment':
      return <FileField render={render} {...props} />;
    case 'date':
      return <DateField render={render} {...props} />;
    case 'bool':
      return <BooleanField {...props} />;
    case 'text':
      return <TextAreaField htmlText={formattedValue} {...props} />;
    default:
      return <TextField render={render} {...props} />;
  }
};

CustomField.defaultProps = { formattedValue: '', };

CustomField.propTypes = {
  type: PropTypes.string.isRequired,
  formattedValue: PropTypes.string,
};

export default CustomField;
