import * as React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import LinesEllipsis from 'react-lines-ellipsis';
import HTMLEllipsis from 'react-lines-ellipsis/lib/html';
import styled from 'styled-components';
import i18n from 'i18next';
import { stopPropagation } from '@/utils';
import style from '@/components/LycheeForm/style.css';

const StyledHTMLEllipsis = styled(HTMLEllipsis)`
  p {
    margin: 0.125rem 0.75rem;
  }
`;

const MAX_LINES = 5;
const Ellipsis = (props) => {
  const { onClick } = props;
  return (
    <span>
      <span>...</span>
      <br />
      <a style={{ cursor: 'pointer' }} onClick={onClick}>
        {i18n.t('Button:readmore')}
      </a>
    </span>
  );
};

const TextReadMore = (props) => {
  const { text, isHtml } = props;
  const [open, setOpen] = React.useState(false);
  const [clamped, setClamped] = React.useState(false);

  const handleReflow = React.useCallback((value) => {
    setClamped((prev) => {
      if (value.clamped) {
        return true;
      }
      return prev;
    });
  }, []);

  if (isHtml) {
    return (
      <div>
        <StyledHTMLEllipsis
          unsafeHTML={text}
          maxLine={open ? Number.MAX_VALUE : MAX_LINES}
          basedOn="letters"
          className={classNames({
            'wiki': true,
            'line-clamp-5': !open,
          },
          text ? '' : style.spacetext
          )}
          style={{ overflow: 'hidden', whiteSpace: 'normal' }}
          onClick={stopPropagation()}
          onReflow={handleReflow}
        />
        {clamped && (
          <a style={{ cursor: 'pointer', margin: '1em' }} onClick={() => setOpen(!open)}>
            {open ? i18n.t('Button:close') : i18n.t('Button:readmore')}
          </a>
        )}
      </div>
    );
  }
  return (
    <LinesEllipsis
      text={text}
      maxLine={open ? Number.MAX_VALUE : MAX_LINES}
      ellipsis={<Ellipsis onClick={() => setOpen(!open)} />}
      basedOn="letters"
      className={classNames({ 'line-clamp-5': !open })}
      style={{ overflow: 'hidden', whiteSpace: 'pre-line' }}
    />
  );
};

const TextAreaField = (props) => {
  const {
    editing,
    htmlText,
    input,
    rows,
    placeholder,
    onKeyDown,
  } = props;

  if (editing) {
    return <textarea {...input} rows={rows} placeholder={placeholder} data-auto-complete="true" onKeyDown={onKeyDown} />;
  }

  return (
    <TextReadMore
      text={htmlText || input.value}
      isHtml={htmlText !== null}
    />
  );
};

TextAreaField.defaultProps = {
  editing: false,
  rows: 5,
  htmlText: null,
  placeholder: null,
  onKeyDown: false,
};

TextAreaField.propTypes = {
  editing: PropTypes.bool,
  input: PropTypes.shape({ value: PropTypes.string, }).isRequired,
  rows: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  htmlText: PropTypes.string,
  placeholder: PropTypes.string,
  onKeyDown: PropTypes.func,
};

export default TextAreaField;
