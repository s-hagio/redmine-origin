import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import style from '@/components/LycheeForm/style.css';
import ICON_DELETE from '@/images/delete.png';

class AttachmentField extends Component {
  constructor() {
    super();
    this.inputRef = React.createRef();
  }

  get hasFile() {
    const { input } = this.props;
    if (input.value && typeof input.value === 'object') {
      return input.value.length > 0;
    }
    return Boolean(input.value);
  }

  handleSelectFile = (event) => {
    const { input, funcAddFile } = this.props;
    const file = event.target.files.item(0);

    funcAddFile(file);
    input.onChange(event);
  };

  handleDeleteFile = () => {
    const { input, funcDelFile, id } = this.props;

    funcDelFile(id);
    input.onChange('');
  };

  handleInputDescription = (e) => {
    const { input } = this.props;
    input.value[0].description = e.target.value;
  };

  render() {
    const { input, placeholder } = this.props;

    if (this.hasFile) {
      const { name, description } = input.value[0];
      if (this.inputRef.current) {
        this.inputRef.current.value = description ?? null;
      }

      return (
        <span>
          <span className={`icon icon-attachment ${style.filename}`}>
            { name }
          </span>
          <input
            className={style.description}
            ref={this.inputRef}
            type="text"
            onChange={this.handleInputDescription}
            placeholder={placeholder}
          />
          <span
            onClick={this.handleDeleteFile}
            style={{ margin: '1px 5px' }}
          >
            <img src={ICON_DELETE} alt="delete" />
          </span>
        </span>
      );
    }

    return (
      <input
        type="file"
        {..._.omit(input, 'value')}
        onChange={this.handleSelectFile}
      />
    );
  }
}

AttachmentField.defaultProps = {
  funcAddFile: () => {},
  placeholder: null,
};

AttachmentField.propTypes = {
  input: PropTypes.shape({ value: PropTypes.any, }).isRequired,
  funcAddFile: PropTypes.func,
  placeholder: PropTypes.string,
};

export default AttachmentField;
