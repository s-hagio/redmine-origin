import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import classNames from 'classnames';

const DateField = (props) => {
  const { editing, input, meta, dateFormat, placeholder } = props;

  if (!editing) {
    return (
      <span>{input.value ? moment(input.value).format(dateFormat) : null}</span>
    );
  }

  return (
    <DatePicker
      {...input}
      placeholderText={placeholder}
      dateFormat="YYYY-MM-DD"
      showMonthDropdown
      showYearDropdown
      isClearable
      autoComplete="off"
      dropdownMode="select"
      selected={input.value ? moment(input.value) : null}
      className={classNames({ 'react-datepicker__input--autofilled': meta.autofilled, })}
      onChange={(value) => input.onChange(value ? value.format('YYYY-MM-DD') : '')}
    />
  );
};

DateField.defaultProps = {
  editing: false,
  dateFormat: 'YYYY-MM-DD',
};

DateField.propTypes = {
  editing: PropTypes.bool,
  input: PropTypes.shape({
    value: PropTypes.string,
    onChange: PropTypes.func,
  }).isRequired,
  dateFormat: PropTypes.string,
};

export default DateField;
