import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ORIGIN_ATTACHMENTS_PATH } from '@/constants/endpoints';

class FileField extends Component {
  constructor() {
    super();

    this.state = {
      filename: '',
      uploaded: false,
    };
  }

  get hasFile() {
    const { input } = this.props;
    if (input.value && typeof input.value === 'object') {
      return input.value.length > 0;
    }
    return Boolean(input.value);
  }

  handleSelectFile = (event) => {
    const { onSelectFile } = this.props;
    const file = event.target.files.item(0);
    onSelectFile(file, event);
    this.setState({
      filename: file.name,
      uploaded: true,
    });
  };

  handleDeleteFile = () => {
    // NOTE: アップロード済みのファイルの削除を実行するが、元からアップロード済みのファイルの削除は行わない。
    const { input, onDeleteFile } = this.props;
    const { uploaded } = this.state;

    if (uploaded) {
      onDeleteFile(input.value);
    } else {
      input.onChange('');
    }

    this.setState({
      filename: '',
      uploaded: false,
    });
  };

  render() {
    const { editing, input, render } = this.props;
    const { filename, uploaded } = this.state;

    if (!editing) {
      render(this.props);
    }

    if (this.hasFile) {
      return (
        <span style={{ alignItems: 'center', display: 'flex' }}>
          {uploaded ? (
            <a
              href={`${ORIGIN_ATTACHMENTS_PATH}/${input.value}`}
              target="_blank"
              rel="noreferrer"
            >
              {filename}
            </a>
          ) : (
            render(this.props)
          )}
        </span>
      );
    }
    return (
      <span />
    );
  }
}

FileField.defaultProps = {
  editing: false,
  render: () => {},
};

FileField.propTypes = {
  editing: PropTypes.bool,
  input: PropTypes.shape({ value: PropTypes.any, }).isRequired,
  onDeleteFile: PropTypes.func.isRequired,
  onSelectFile: PropTypes.func.isRequired,
  render: PropTypes.func,
};

export default FileField;
