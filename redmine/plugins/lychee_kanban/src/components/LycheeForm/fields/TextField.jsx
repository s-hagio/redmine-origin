import React from 'react';
import PropTypes from 'prop-types';

const defaultRender = (values) => <span>{values.input.value}</span>;
const TextField = ({ render, ...field }) => {
  if (field.editing) {
    return (
      <input
        {...field.input}
        placeholder={field.placeholder}
        data-auto-complete={field.type === 'string' ? 'true' : null}
      />
    );
  }
  return render(field);
};

TextField.defaultProps = {
  editing: false,
  render: defaultRender,
};

TextField.propTypes = {
  editing: PropTypes.bool,
  input: PropTypes.shape({
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  }).isRequired,
  render: PropTypes.func,
};

export default TextField;
