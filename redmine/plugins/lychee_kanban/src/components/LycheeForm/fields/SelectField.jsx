import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FieldArray } from 'redux-form';
import createFragment from 'react-addons-create-fragment';
import * as _ from 'lodash';
import i18n from 'i18next';

// eslint-disable-next-line max-len
const createFragmentWrapper = (options, func) => createFragment(Object.assign(...options.map(func)));

const defaultRender = (values) => (
  <div>
    {createFragmentWrapper(values, ({ value, text }) => ({ [`val-${value}`]: <div key={text}>{text}</div>, }))}
  </div>
);

const CheckBox = (props) => {
  const { fields, options } = props;
  const values = fields.getAll() || [];
  const toggle = (value) => () => {
    const index = _.indexOf(values, value);
    if (index >= 0) {
      fields.remove(index);
    } else {
      fields.push(value);
    }
  };
  return (
    <div>
      {options.map((option, index) => (
        <label
          key={option.value}
          htmlFor={`${fields.name}.${index}`}
          style={{ display: 'block' }}
        >
          <input
            id={`${fields.name}.${index}`}
            type="checkbox"
            checked={Boolean(values.includes(String(option.value)))}
            onChange={toggle(String(option.value))}
          />
          {option.text}
        </label>
      ))}
    </div>
  );
};

const RadioButton = ({ input, options, required }) => (
  <div>
    {!required && (
      <label htmlFor={`${input.name}.blank`}>
        <input
          {...input}
          type="radio"
          id={`${input.name}.blank`}
          value=""
          checked={!input.value}
        />
        {`(${i18n.t('Button:none')})`}
      </label>
    )}
    {createFragmentWrapper(options, (option, index) => ({
      [`opt-${option.value}`]: (
        <label htmlFor={`${input.name}.${index}`} style={{ display: 'block' }}>
          <input
            {...input}
            type="radio"
            id={`${input.name}.${index}`}
            value={option.value}
            checked={_.isEqual(input.value, option.value)}
          />
          {option.text}
        </label>
      ),
    }))}
  </div>
);

const SelectBox = ({ input, multiple, required, options, hasBlank }) => (
  <select {...input} value={input.value || ''} multiple={multiple}>
    {hasBlank && (required ? (
      <option value="">{`---${i18n.t('Button:blank_option')}---`}</option>
    ) : (
      <option />
    ))}
    {createFragmentWrapper(options, (option) => ({
      [`opt-${option.value}`]: (
        <option value={option.value}>{option.text}</option>
      ),
    }))}
  </select>
);

class SelectField extends Component {
  // HACK: lint違反の修正
  // eslint-disable-next-line react/no-deprecated
  componentWillUpdate(nextProps) {
    const { input, multiple, options } = nextProps;

    const values = _.isArray(input.value)
      ? input.value.map((value) => String(value))
      : [String(input.value)];
    const optionValues = _.map(options, ({ value }) => String(value));
    const differences = _.difference(_.compact(values), optionValues);
    if (!_.isEmpty(differences)) {
      const result = multiple ? _.difference(values, differences) : '';
      input.onChange(result);
    }
  }

  render() {
    const {
      options,
      editing,
      input,
      render,
      multiple,
      required,
      editStyle,
      hasBlank,
    } = this.props;

    let values = _.isArray(input.value) ? input.value : [input.value];
    values = values.map((value) => String(value));
    if (editing) {
      if (_.isEmpty(options)) {
        return null;
      }
      switch (editStyle) {
        case 'check_box':
          if (multiple) {
            const name = _.last(input.name.split('.'));
            return (
              <FieldArray name={name} options={options} component={CheckBox} />
            );
          }
          return (
            <RadioButton input={input} options={options} required={required} />
          );
        default:
          return (
            <SelectBox
              input={input}
              options={options}
              multiple={multiple}
              required={required}
              hasBlank={hasBlank}
            />
          );
      }
    }

    const results = _.filter(options, (option) => values.includes(String(option.value)));
    if (_.isEmpty(results)) {
      return null;
    }
    return render(results);
  }
}

SelectField.defaultProps = {
  editing: false,
  editStyle: '',
  hasBlank: true,
  multiple: false,
  options: [],
  render: defaultRender,
  required: false,
};

SelectField.propTypes = {
  editing: PropTypes.bool,
  editStyle: PropTypes.string,
  hasBlank: PropTypes.bool,
  input: PropTypes.object.isRequired,
  multiple: PropTypes.bool,
  options: PropTypes.array,
  render: PropTypes.func,
  required: PropTypes.bool,
};

export default SelectField;
