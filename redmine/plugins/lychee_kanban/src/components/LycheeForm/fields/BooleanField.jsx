import React from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import i18n from 'i18next';

const LOCALED_TEXT = {
  '': `(${i18n.t('Button:none')})`,
  '0': i18n.t('Button:no'),
  '1': i18n.t('Button:yes'),
  'blank': `---${i18n.t('Button:blank_option')}---`,
};

const parseInput = (input) => {
  switch (String(input.value)) {
    case '0':
    case 'false':
      return { ...input, value: '0' };
    case '1':
    case 'true':
      return { ...input, value: '1' };
    default:
      return { ...input, value: '' };
  }
};

const RadioButton = ({ input, required }) => (
  <div>
    {!required && (
      <label htmlFor={`${input.name}.blank`}>
        <input
          {...input}
          type="radio"
          id={`${input.name}.blank`}
          value=""
          checked={!input.value}
        />
        {LOCALED_TEXT['']}
      </label>
    )}
    <label htmlFor={`${input.name}.1`} style={{ display: 'block' }}>
      <input
        {...input}
        type="radio"
        id={`${input.name}.1`}
        value="1"
        checked={_.isEqual(input.value, '1')}
      />
      {LOCALED_TEXT['1']}
    </label>
    <label htmlFor={`${input.name}.0`} style={{ display: 'block' }}>
      <input
        {...input}
        type="radio"
        id={`${input.name}.0`}
        value="0"
        checked={_.isEqual(input.value, '0')}
      />
      {LOCALED_TEXT['0']}
    </label>
  </div>
);

const SelectBox = ({ input, required, hasBlank }) => (
  <select {...input} value={input.value}>
    {hasBlank && (required ? (
      <option value="">{LOCALED_TEXT.blank}</option>
    ) : (
      <option />
    ))}
    <option value="1">{LOCALED_TEXT['1']}</option>
    <option value="0">{LOCALED_TEXT['0']}</option>
  </select>
);

const BooleanField = ({ editing, input, required, editStyle, hasBlank }) => {
  const parsedInput = parseInput(input);
  if (editing) {
    switch (editStyle) {
      case 'check_box': {
        const checked = Number(parsedInput.value);
        const toggle = () => parsedInput.onChange(checked ? '0' : '1');
        return <input type="checkbox" checked={checked} onChange={toggle} />;
      }
      case 'radio':
        return <RadioButton input={parsedInput} required={required} />;
      default:
        return (
          <SelectBox
            input={parsedInput}
            required={required}
            hasBlank={hasBlank}
          />
        );
    }
  }
  return <div>{LOCALED_TEXT[String(parsedInput.value)]}</div>;
};

BooleanField.defaultProps = {
  editing: false,
  editStyle: '',
  hasBlank: true,
  required: false,
};

BooleanField.propTypes = {
  editing: PropTypes.bool,
  editStyle: PropTypes.string,
  hasBlank: PropTypes.bool,
  input: PropTypes.object.isRequired,
  required: PropTypes.bool,
};

export default BooleanField;
