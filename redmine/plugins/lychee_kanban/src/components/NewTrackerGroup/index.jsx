import React from 'react';
import PropTypes from 'prop-types';
import { useDrop } from 'react-dnd';
import classNames from 'classnames';
import style from './style.css';
import * as ItemTypes from '@/constants/itemTypes';

const NewTrackerGroup = (props) => {
  const { addNewTrackerGroup, isVisible } = props;
  const onDrop = (item) => {
    const { tracker } = item;
    addNewTrackerGroup(tracker);
  };
  const { drop } = useDrop({
    accept: ItemTypes.TRACKER,
    drop: onDrop,
  });
  return (
    <div
      className={classNames({
        [style.container]: true,
        [style.hidden]: !isVisible
      })}
      ref={drop}
    >
      +
    </div>
  );
};

NewTrackerGroup.propTypes = {
  addNewTrackerGroup: PropTypes.func.isRequired,
  isVisible: PropTypes.bool.isRequired,
};

export default NewTrackerGroup;
