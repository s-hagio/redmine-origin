import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import * as _ from 'lodash';

import Membership from '@/containers/Membership';
import Dropdown from '@/components/Dropdown';
import CustomDragLayer from '@/components/CustomDragLayer';
import style from './style.css';

const DISPLAY_NUMBER = 12;
const MembershipList = ({ memberships, className }) => (
  <div className={classNames([style.container, className])}>
    {_.take(memberships, DISPLAY_NUMBER).map((membership) => (
      <Membership key={membership.id} membership={membership} />
    ))}
    {memberships.length > DISPLAY_NUMBER && (
      <Dropdown
        trigger={<div className={style.trigger}>{memberships.length}</div>}
      >
        <div className={style.dropdown}>
          <div className={style.list}>
            {memberships.map((membership) => (
              <Membership key={membership.id} membership={membership} />
            ))}
          </div>
        </div>
      </Dropdown>
    )}
    <CustomDragLayer />
  </div>
);

MembershipList.defaultProps = {
  className: '',
  memberships: [],
};

MembershipList.propTypes = {
  className: PropTypes.string,
  memberships: PropTypes.array,
};

export default MembershipList;
