// TODO: models/workflowに機能を統合次第、削除
import { Record, List, Map } from 'immutable';
import * as _ from 'lodash';

const TrackerRecord = Record({
  id: null,
  name: '',
  position: null,
  color: '',
  issue_statuses: List(),
  activated_project_ids: [],
});

export class Tracker extends TrackerRecord {
  constructor({ lychee_issue_board_tracker_option = {}, ...values }) {
    super({
      ...values,
      color: lychee_issue_board_tracker_option.color,
    });
  }
}

const TrackerListRecord = Record({ items: Map(), });

export class TrackerList extends TrackerListRecord {
  find(id) {
    return this.items.get(Number(id));
  }

  importFromWorkflows(workflows) {
    return this.merge({
      items: Map(
        _.flatMap(workflows, (workflow) => workflow.trackers.map((tracker) => [
          Number(tracker.id),
          new Tracker({
            id: Number(tracker.id),
            name: tracker.name,
            position: tracker.position,
            issue_statuses: List(workflow.issue_statuses),
          }),
        ])
        )
      ),
    });
  }
}
