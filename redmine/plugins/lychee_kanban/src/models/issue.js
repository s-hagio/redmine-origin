import { Record, List, Map } from 'immutable';
import * as _ from 'lodash';
import moment from 'moment';

const dateSort = (dateA, dateB) => {
  if (!dateA) {
    return 1;
  }
  if (!dateB) {
    return -1;
  }
  return moment(dateA) > moment(dateB);
};

const IssueRecord = Record({
  activities: null,
  actual_due_date: '',
  actual_start_date: '',
  allowed_issue_statuses: [],
  ancestors: [],
  ancestor_ids: [],
  assigned_to: {},
  category: {},
  closed: false,
  costs: {},
  created_on: '',
  custom_fields: [],
  descendants: [],
  description: '',
  description_html: '',
  due_date: '',
  done_ratio: null,
  estimated_hours: null,
  notes: '',
  notes_html: '',
  remaining_estimate: null,
  fixed_version: {},
  id: null,
  leaf: false,
  lychee_issue_board_issue_option: {
    blocking: false,
    point: null,
  },
  lychee_status_color: null,
  parent: {},
  relations: [],
  root: false,
  parent_projects_list: [],
  priority: {},
  project: {},
  safe_attribute_names: [],
  spent_hours: null,
  start_date: '',
  status: {},
  subject: '',
  tracker: {},
  updated_on: '',
  visual_parent_id: null,
  your_spent_hours: null,
  days_elapsed: null,
});

export class Issue extends IssueRecord {
  get blocking() {
    return Boolean(this.lychee_issue_board_issue_option.blocking);
  }

  get color() {
    const options = this.tracker.lychee_issue_board_tracker_option || {};
    return options.color;
  }

  get depth() {
    return this.ancestors.length;
  }

  get point() {
    return this.lychee_issue_board_issue_option.point;
  }

  get nodeType() {
    if (this.root && this.leaf) {
      return 'seed';
    }
    if (this.root) {
      return 'root';
    }
    if (this.leaf) {
      return 'leaf';
    }
    return 'node';
  }

  get relationType() {
    const precede = this.hasPrecedeRelation;
    const follow = this.hasFollowRelation;
    if (precede && follow) {
      return 'both';
    }
    if (precede) {
      return 'precede';
    }
    if (follow) {
      return 'follow';
    }
    return null;
  }

  get hasRelation() {
    return !_.isEmpty(this.relations);
  }

  get hasPrecedeRelation() {
    return _.some(this.relations, {
      issue_from_id: this.id,
      relation_type: 'precedes',
    });
  }

  get hasFollowRelation() {
    return _.some(this.relations, {
      issue_to_id: this.id,
      relation_type: 'precedes',
    });
  }

  relationTypeWith(id) {
    const isThis = id === this.id;
    const precede = _.some(this.relations, {
      issue_to_id: id,
      relation_type: 'precedes',
    });
    const follow = _.some(this.relations, {
      issue_from_id: id,
      relation_type: 'precedes',
    });
    if (precede && follow) {
      return 'both';
    }
    if (precede) {
      return isThis ? 'follow' : 'precede';
    }
    if (follow) {
      return isThis ? 'precede' : 'follow';
    }
    return null;
  }

  toJS() {
    return Map(this)
      .merge({
        blocking: this.blocking,
        color: this.color,
        point: this.point,
      })
      .toJS();
  }
}

const IssueListRecord = Record({ items: List(), });

export class IssueList extends IssueListRecord {
  constructor(issues = []) {
    super({ items: List(issues.map((issue) => new Issue(issue))) });
  }

  get ids() {
    return this.map((issue) => issue.id).toArray();
  }

  get point() {
    return _.sum(this.items.map((issue) => Number(issue.point || 0)).toArray());
  }

  get count() {
    return this.items.count();
  }

  get unassigned() {
    return this.set(
      'items',
      this.items.filter((item) => !item.assigned_to.id)
    );
  }

  get unfixed() {
    return this.set(
      'items',
      this.items.filter((item) => !item.fixed_version.id)
    );
  }

  get workload() {
    return _.sum(
      this.items.map((issue) => Number(issue.estimated_hours || 0)).toArray()
    );
  }

  get visualRoot() {
    return this.set(
      'items',
      this.items.filter((item) => !item.visual_parent_id)
    );
  }

  add(_issues = []) {
    const issues = _.castArray(_issues);
    const addedIssuesIds = new Set(issues.map((issue) => issue.id));
    return this.filter((issue) => !addedIssuesIds.has(issue.id)).concat(
      new IssueList(issues)
    );
  }

  refresh(_issues = []) {
    const issues = _.castArray(_issues);
    return this.set('items', List(issues.map((issue) => new Issue(issue))));
  }

  concat(list) {
    return this.set('items', this.items.concat(list.items));
  }

  uniqBy(key) {
    return this.set('items', new List(_.uniqBy(this.toArray(), key)));
  }

  filter(...args) {
    return this.set('items', this.items.filter(...args));
  }

  find(id) {
    return this.items.find((issue) => _.isEqual(Number(issue.id), Number(id)));
  }

  findIndex(id) {
    return this.items.findIndex((issue) => _.isEqual(Number(issue.id), Number(id))
    );
  }

  insert(index, issue) {
    if (index === -1) {
      return this.set('items', this.items.push(new Issue(issue)));
    }
    return this.set('items', this.items.insert(index, new Issue(issue)));
  }

  map(...args) {
    return this.items.map(...args);
  }

  maxBy(...args) {
    return this.items.maxBy(...args);
  }

  reduce(...args) {
    return this.items.reduce(...args);
  }

  remove(id) {
    return this.set(
      'items',
      this.items.filter((item) => item.id !== id)
    );
  }

  sortBy(...args) {
    return this.items.sortBy(...args);
  }

  sortByColumn(column) {
    const sortByMoment = (date) => {
      if (date) {
        return moment(date);
      }
      return null;
    };
    switch (column) {
      case 'start_date':
        return this.sortBy((item) => sortByMoment(item.start_date));
      case 'due_date':
        return this.sortBy((item) => sortByMoment(item.due_date));
      default:
        return this;
    }
  }

  sumBy(attr) {
    return this.items.reduce(
      (accumulator, currentValue) => accumulator + Number(currentValue.get(attr) || 0),
      0
    );
  }

  update(id, values) {
    const index = this.findIndex(id);
    if (index < 0) {
      return this;
    }
    return this.set('items', this.items.splice(index, 1, new Issue(values)));
  }

  toArray() {
    return this.items.toArray();
  }

  toJS() {
    return this.items.toJS();
  }

  getVisualChildren(id) {
    return this.items.filter((issue) => issue.visual_parent_id === id).toJS();
  }

  getDescendants(id, list = this) {
    const children = list.items.filter((issue) => _.isEqual(issue.parent.id, id)
    );
    const descendants = children
      .map((issue) => list.getDescendants(issue.id, list))
      .flatten(1);
    return children.concat(descendants);
  }

  findByWorkflow(workflow) {
    const trackerIds = workflow ? workflow.trackers.map((tracker) => tracker.id) : [];
    const items = this.items.filter((issue) => trackerIds.includes(issue.tracker.id));
    return this.set('items', items);
  }

  findDescendants(id) {
    return this.set('items', this.getDescendants(id));
  }

  sorted_ids(rootId) {
    const issueTreeIds = (id) => {
      const parentIssues = this.items.filter((issue) => _.isEqual(issue.parent.id, id)
      );
      return parentIssues.map((issue) => List([issue.id, issueTreeIds(issue.id)])
      );
    };
    return issueTreeIds(rootId).flatten().toArray();
  }

  sortIssue(
    issue,
    { before_issue_id, after_issue_id, issue_index, sort_column }
  ) {
    const values = _.clone(issue);
    // 現在のvisual_parent_idを引き継ぐ
    const oldissue = this.find(issue.id);
    values.visual_parent_id = oldissue ? oldissue.visual_parent_id : null;
    let insertIndex;
    let resortedIssues = this.remove(issue.id);
    if (!_.isNil(issue_index)) {
      insertIndex = issue_index;
    } else {
      const issueId = after_issue_id || before_issue_id;
      insertIndex = resortedIssues.findIndex(issueId);
      if (after_issue_id) {
        insertIndex += 1;
      }
    }

    resortedIssues = resortedIssues.insert(insertIndex, values).items;
    if (sort_column !== 'sort_by_backlog') {
      resortedIssues = resortedIssues.sort((a, b) => dateSort(a[sort_column], b[sort_column])
      );
    }
    return this.set('items', resortedIssues);
  }

  addTimeEntry({ issue, hours }) {
    const items = this.items.map((item) => {
      if (Number(item.id) === Number(issue.id)) {
        return item.set('spent_hours', item.spent_hours + Number(hours));
      }
      return item;
    });
    return this.set('items', items);
  }
}
