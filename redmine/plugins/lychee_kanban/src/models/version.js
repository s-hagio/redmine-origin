import { List, Record } from 'immutable';
import * as _ from 'lodash';

const VersionRecord = Record({
  custom_fields: [],
  created_on: '',
  description: '',
  due_date: '',
  estimated_velocity: 0,
  work_days: 0,
  id: null,
  name: '',
  project: {},
  sharing: '',
  start_date: '',
  status: '',
  updated_on: '',
  wiki_page_title: '',
  costs: {},
});

export class Version extends VersionRecord {
  get hasPeriod() {
    return Boolean(this.start_date && this.due_date);
  }

  get isBacklog() {
    return !this.id;
  }

  get isOpen() {
    return this.status === 'open';
  }
}

const VersionListRecord = Record({
  items: List(),
  unit: 'points',
  velocities: [],
  dailyVelocity: null,
});

export class VersionList extends VersionListRecord {
  constructor(args = {}) {
    const { versions = [], unit = 'points', velocities = [], dailyVelocity = null } = args;
    super({
      items: List(versions.map((version) => new Version(version))),
      unit,
      velocities,
      dailyVelocity,
    });
  }

  get compact() {
    return this.set(
      'items',
      this.items.filter((version) => !(_.isNil(version) || _.isEmpty(version)))
    );
  }

  get nonopen() {
    return this.set(
      'items',
      this.items.filter((version) => !version.isOpen),
    );
  }

  get open() {
    return this.set(
      'items',
      this.items.filter((version) => version.isOpen),
    );
  }

  get size() {
    return this.items.size;
  }

  add(items = []) {
    const velocityItems = {
      unit: this.unit,
      dailyVelocity: this.dailyVelocity,
    };
    const versions = new VersionList({ versions: _.castArray(items), ...velocityItems });
    return versions.concat(this).uniqBy('id');
  }

  concat(list) {
    return this.set('items', this.items.concat(list.items));
  }

  uniqBy(key) {
    return this.set('items', new List(_.uniqBy(this.toArray(), key)));
  }

  filter(...args) {
    return this.set('items', this.items.filter(...args));
  }

  find(id) {
    return this.items.find((item) => Number(id) === Number(item.id));
  }

  findIndex(id) {
    return this.items.findIndex((item) => Number(id) === Number(item.id));
  }

  findBy(...args) {
    return this.items.find(...args);
  }

  map(...args) {
    return this.items.map(...args).toArray();
  }

  splice(...args) {
    return this.set('items', this.items.splice(...args));
  }

  toArray() {
    return this.items.toArray();
  }

  update(id, value) {
    const index = this.findIndex(id);
    if (index < 0) {
      return this;
    }
    return this.set(
      'items',
      this.items.update(index, (item) => item.merge(new Version(value))),
    );
  }
}
