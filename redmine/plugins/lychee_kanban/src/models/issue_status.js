import { Record } from 'immutable';

const IssueStatusRecord = Record({
  default_done_ratio: null,
  id: null,
  is_closed: false,
  name: '',
  position: null,
});

export class IssueStatus extends IssueStatusRecord {}
