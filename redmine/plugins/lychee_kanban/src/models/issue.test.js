import { IssueList } from './issue';

describe('IssueList', () => {
  let issueList;

  describe('getVisualChildren', () => {
    beforeEach(() => {
      issueList = new IssueList([
        { id: 1 },
        { id: 2, visual_parent_id: 1 },
        { id: 3, visual_parent_id: 2 },
      ]);
    });

    it('returns array of visual children', () => {
      const ret = issueList.getVisualChildren(1);
      expect(ret.length).toEqual(1);
      expect(ret[0].id).toEqual(2);
    });
  });
});
