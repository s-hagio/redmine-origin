import { Record, Map, List, fromJS } from 'immutable';

const VersionFormRecord = Record({
  custom_fields: List(),
  open: false,
  position: {
    x: 0,
    y: 0,
  },
  safe_attribute_names: List(),
  sharings: List(),
  version: Map(),
});

export class VersionForm extends VersionFormRecord {
  constructor(values = {}) {
    super(fromJS(values));
  }

  get close() {
    return this.set('open', false);
  }

  get isOpen() {
    return this.get('open');
  }

  get open() {
    return this.set('open', true);
  }

  setPosition(values) {
    return this.set('position', values);
  }
}
