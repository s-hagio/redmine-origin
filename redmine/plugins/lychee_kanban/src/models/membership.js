import { Record, OrderedMap } from 'immutable';
import * as _ from 'lodash';

const MembershipRecord = Record({
  id: null,
  project: {},
  roles: [],
  user: null,
  group: null,
  spent: 0,
});

export class Membership extends MembershipRecord {
  get principal() {
    return this.user || this.group;
  }

  isUser() {
    return Boolean(this.user);
  }

  isGroup() {
    return Boolean(this.group);
  }
}

const MembershipListRecord = Record({ items: OrderedMap(), });

export class MembershipList extends MembershipListRecord {
  constructor(values) {
    super();
    return this.add(values);
  }

  add(items = []) {
    const values = _.castArray(items);
    return this.merge({
      items: this.items.merge(
        values.map((item) => [item.id, new Membership(item)])
      ),
    });
  }

  find(id) {
    return this.items.get(id);
  }

  findBy(...args) {
    return this.items.find(...args);
  }

  map(...args) {
    return this.items.toList().map(...args);
  }

  toArray() {
    return this.items.toList().toArray();
  }

  toJS() {
    return this.items.toList().toJS();
  }
}
