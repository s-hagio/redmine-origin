import { Record, List, fromJS } from 'immutable';
import * as _ from 'lodash';
import { IssueStatus } from './issue_status';
import { Tracker } from './tracker';

const WorkflowRecord = Record({
  trackers: List(),
  issue_statuses: List(),
  active: false,
});

export class Workflow extends WorkflowRecord {
  constructor(values) {
    if (values) {
      super(
        fromJS({
          ...values,
          issue_statuses: new List(
            values.issue_statuses.map((value) => new IssueStatus(value))
          ),
          trackers: values.trackers.map((tracker) => new Tracker(tracker)),
        })
      );
    } else {
      super(values);
    }
  }

  get statuses() {
    return this.issue_statuses.sortBy((status) => status.position);
  }

  hasTracker(trackerId) {
    return this.trackers.find((tracker) => tracker.id === trackerId);
  }
}

const WorkflowListRecord = Record({
  items: List(),
  index: 0,
});

export class WorkflowList extends WorkflowListRecord {
  constructor(values = [], active = 0) {
    const items = _.isArray(values) ? values : [values];
    super({
      items: List(
        items.map(
          (item, index) => new Workflow({
            ...item,
            active: _.isEqual(active, index),
          })
        )
      ),
    });
    return this;
  }

  get current() {
    return this.items.get(this.index) || new Workflow();
  }

  get trackers() {
    return this.items.flatMap((workflow) => workflow.trackers);
  }

  find(...args) {
    return this.items.find(...args);
  }

  findByTracker(id) {
    return this.find((item) => item.hasTracker(id));
  }

  toggleWorkflow(activateIndex) {
    return this.merge({
      items: this.items.map((workflow, index) => workflow.set('active', _.isEqual(index, activateIndex))
      ),
      index: activateIndex,
    });
  }
}
