import { Record, Map } from 'immutable';
import { stringify } from 'query-string';
import * as _ from 'lodash';

const QueryRecord = Record({
  key: '',
  operator: '',
  values: [],
});

export class Query extends QueryRecord {
  toURL() {
    const query = {};
    query['f[]'] = this.key;
    query[`op[${this.key}]`] = this.operator;
    query[`v[${this.key}][]`] = this.values;
    return stringify(query);
  }
}

const QueryListRecord = Record({ items: Map(), });

export class QueryList extends QueryListRecord {
  constructor(values = {}) {
    super({
      items: Map(
        _.map(values, (value, key) => [key, new Query({ key, ...value })])
      ),
    });
  }

  add(values = {}) {
    return this.merge({
      items: this.items.merge(
        _.map(values, (value, key) => [key, new Query({ key, ...value })])
      ),
    });
  }

  remove(name) {
    return this.merge({ items: this.items.delete(name) });
  }

  toURL(extraParams = {}) {
    let items = this.items.toArray().map((item) => item.toURL());
    items = items.concat(stringify(extraParams));
    return items.join('&');
  }
}
