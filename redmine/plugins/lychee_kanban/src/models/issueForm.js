import { Record, Map, List, fromJS } from 'immutable';
import * as _ from 'lodash';

const IssueFormRecord = Record({
  assignable_users: List(),
  custom_fields: List(),
  custom_field_values: List(),
  journals: List(),
  disabled_core_fields: List(),
  issue: Map(),
  issue_categories: List(),
  issue_priorities: List(),
  issue_statuses: List(),
  open: false,
  position: {
    x: 0,
    y: 0,
  },
  required_attribute_names: List(),
  projects: List(),
  safe_attribute_names: List(),
  trackers: List(),
  versions: List(),
  permissions: List(),
  spent_hours: 0,
  total_spent_hours: 0,
  time_entry: Map({
    time_entry: Map(),
    activities: List(),
    custom_fields: List(),
  }),
  attachments: List(),
});

export class IssueForm extends IssueFormRecord {
  constructor(values = {}) {
    const { issue = {}, versions = [] } = values;
    const fixedVersionId = _.get(issue, 'fixed_version.id');
    const version = versions.find(({ id }) => id === fixedVersionId) || {};
    const isNew = !issue.id;

    super(
      fromJS({
        ...values,
        issue: {
          ..._.pick(issue, [
            'id',
            'subject',
            'description',
            'description_html',
            'actual_start_date',
            'actual_due_date',
            'remaining_estimate',
            'estimated_hours',
            'costs',
            'done_ratio',
          ]),
          project_id: _.get(issue, 'project.id'),
          tracker_id: _.get(issue, 'tracker.id'),
          status_id: _.get(issue, 'status.id'),
          priority_id: _.get(issue, 'priority.id'),
          assigned_to_id: _.get(issue, 'assigned_to.id'),
          category_id: _.get(issue, 'category.id'),
          fixed_version_id: fixedVersionId,
          start_date: issue.start_date || (isNew && version.start_date) || '',
          due_date: issue.due_date || (isNew && version.due_date) || '',
          parent_issue_id: _.get(issue, 'parent.id'),
          lychee_issue_board_issue_option_attributes:
            issue.lychee_issue_board_issue_option,
          // idをkeyとしたHash形式に変換
          custom_field_values: _.get(issue, 'custom_fields', []).reduce(
            (obj, field) => ({ ...obj, [field.id]: field.value }),
            {}
          ),
          journals: _.get(issue, 'journals', []),
          attachments: _.get(issue, 'attachments', []),
        },
      })
    );
  }

  get close() {
    return this.set('open', false);
  }

  get isOpen() {
    return this.get('open');
  }

  get open() {
    return this.set('open', true);
  }

  setPosition(values) {
    return this.set('position', values);
  }
}
