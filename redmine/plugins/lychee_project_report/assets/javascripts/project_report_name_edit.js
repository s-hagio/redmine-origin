$(document).ready(function () {
  const originValueClassName = 'originValue';
  const editableTarget = 'table#report-table.minilist td.report-name';

  function editModeClick(event) {
    event.stopPropagation();
    const target = $(event.target);
    deleteEditTags(
      $(editableTarget + ' td:has(.' + originValueClassName + ')')
    );
    // GET data
    switchBoxType(target);
  }

  function switchBoxType(element) {
    const currentText = $(element).text();
    const textBox = $(
      '<input type="text" maxlength="50" id="editable_issue_value"/>'
    ).val(currentText);
    createEditTag(element, textBox);
    focusBlur(element, currentText);
  }

  function createEditTag(element, editTag) {
    const originValue = $(element).html();
    $(element)
      .html(editTag)
      .append(
        $('<input>', {
          type: 'hidden',
          value: originValue,
          class: originValueClassName,
        })
      );
  }

  function deleteEditTags(elements) {
    $(elements).each(function () {
      deleteEditTag($(this));
    });
  }

  function deleteEditTag(element) {
    $(element).html(
      $(element)
        .find('input.' + originValueClassName)
        .val()
    );
  }

  function focusBlur(element, text) {
    $(element)
      .children()
      .focus()
      .blur(function () {
        const value = $(element).children().val();
        update(element, value, text);
      });
  }

  function update(element, value, text) {
    const date = $(element).data('date');
    const projectUrl = $('[data-project-report-project]').data(
      'project-report-project'
    );
    // security - do not process URLs, only absolute paths
    var pat = /^https?:\/\//i;
    if (pat.test(projectUrl)) {
      return;
    }
    const versionId = $('[data-project-report-version]').data(
      'project-report-version'
    );
    const paramName = versionId ? 'projectVersionReport' : 'projectReport';
    let url = projectUrl;
    if (versionId) {
      url = url + `/versions/${versionId}`;
    }
    url = url + `/project_reports/${date}`;
    let data = {};
    data[paramName] = { record_name: value };
    $.ajax({
      url,
      type: 'PATCH',
      dataType: 'json',
      data,
    })
      .done(function (obj) {
        if (obj.errors) {
          $(element).html(text);
        } else {
          $(element).html(obj.record_name || '');
        }
      })
      .fail(function (_) {
        deleteEditTag(element);
      });
  }

  $(document).on('click', editableTarget, editModeClick);
  $(document).on('keydown', '#editable_issue_value', function (e) {
    // textbox の enter で form の submit を抑制する
    if (
      ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) &&
      $(this).is('input[type=text]') // textareaの場合は改行可能にしたい
    ) {
      e.preventDefault();
      $(this).blur();
    }
  });
});
