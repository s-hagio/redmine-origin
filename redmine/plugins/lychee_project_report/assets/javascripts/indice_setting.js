$(function () {
  MathJax.Callback = function (args) {
    if (Array.isArray(args)) {
      if (args.length === 1 && typeof args[0] === 'function') {
        return args[0];
      } else if (
        typeof args[0] === 'string' &&
        args[1] instanceof Object &&
        typeof args[1][args[0]] === 'function'
      ) {
        return Function.bind.apply(args[1][args[0]], args.slice(1));
      } else if (typeof args[0] === 'function') {
        return Function.bind.apply(args[0], [window].concat(args.slice(1)));
      } else if (typeof args[1] === 'function') {
        return Function.bind.apply(args[1], [args[0]].concat(args.slice(2)));
      }
    } else if (typeof args === 'function') {
      return args;
    }
    throw Error("Can't make callback from given data");
  };
  MathJax.Hub = {
    Queue: function () {
      for (var i = 0, m = arguments.length; i < m; i++) {
        var fn = MathJax.Callback(arguments[i]);
        MathJax.startup.promise = MathJax.startup.promise.then(fn);
      }
      return MathJax.startup.promise;
    },
    Typeset: function (elements, callback) {
      var promise = MathJax.typesetPromise(elements);
      if (callback) {
        promise = promise.then(callback);
      }
      return promise;
    },
    Register: {
      MessageHook: function () {
        console.log('MessageHooks are not supported in version 3');
      },
      StartupHook: function () {
        console.log('StartupHooks are not supported in version 3');
      },
      LoadHook: function () {
        console.log('LoadHooks are not supported in version 3');
      },
    },
    Config: function () {
      console.log('MathJax configurations should be converted for version 3');
    },
  };
});

$(function () {
  $(document).on('change', '#indice_setting_indice_type', function () {
    $.get($('#indice_type_indice_settings_path').val(), {
      indice_type: $('#indice_setting_indice_type').val(),
    })
      .done(function (data) {
        $('#indice_setting_calculate_module_id')
          .html(data.render_indice_type)
          .change();
        $('#custom-issue-filter').toggle(
          $('#indice_setting_indice_type').val() !== 'evm'
        );
      })
      .fail(function (data, err) {
        // fail
      });
  });
  $(document).on('change', '#indice_setting_calculate_module_id', function () {
    $.get($('#calculate_module_indice_settings_path').val(), {
      indice_setting_id: $('#indice_setting_id').val(),
      calculate_module_id: $('#indice_setting_calculate_module_id').val(),
    })
      .done(function (data) {
        console.log(data);
        $('#description-area').text(data.calculate_module.description);
        $('#partial-area').html(data.render_partial);
        $('#unit-string-area').html(data.render_unit_string);
        $('#custom-issue-filter').trigger(
          'change',
          data.calculate_module.partial_template || ''
        );
        $('.indice-setting-unit-string').text(
          data.calculate_module.unit_string
        );
        var bigger_better = data.calculate_module.bigger_better
          ? 'true'
          : 'false';
        if ($('#indice_setting_indice_type').val() === 'evm') {
          bigger_better = 'not_use';
        }
        $('#indice_setting_bigger_better').val(bigger_better);
        $('#indice_setting_bigger_better').trigger('change');
        $('#custom_parameter_cf_numerator').trigger('change');
        $('#custom_parameter_cf_denominator').trigger('change');
        MathJax.Hub.Queue(['Typeset', MathJax.Hub]);
      })
      .fail(function (data, err) {
        // fail
      });
  });
  $(document).on(
    'change',
    '#custom-issue-filter',
    function (event, module_name) {
      if (
        module_name === 'calculate_modules/project_eac' ||
        $('#indice_setting_indice_type').val() === 'evm'
      ) {
        $('#custom-issue-filter').hide();
      } else {
        $('#custom-issue-filter').show();
      }
    }
  );
  $(document).on('change', '#indice_setting_unit_string', function () {
    $('.indice-setting-unit-string').text(this.value);
  });
  $(document).on('change', '#indice_setting_bigger_better', function () {
    if (this.value == 'not_use') {
      $('#indice_setting_signal_disabled').val('true');
      $('#indice_setting_upper_threshold').prop('required', false);
      $('#indice_setting_middle_threshold').prop('required', false);
      $('*[data-indice-setting-field]').hide();
      return;
    }

    $('#indice_setting_signal_disabled').val('false');
    $('#indice_setting_upper_threshold').prop('required', true);
    $('#indice_setting_middle_threshold').prop('required', true);
    $('*[data-indice-setting-field]').show();

    if (this.value == 'true') {
      $('#indice-setting-form-thresholds .curled-box')
        .first()
        .removeClass('bgre')
        .addClass('bggr');
      $('#indice-setting-form-thresholds .curled-box')
        .last()
        .removeClass('bggr')
        .addClass('bgre');
    } else if (this.value == 'false') {
      $('#indice-setting-form-thresholds .curled-box')
        .first()
        .removeClass('bggr')
        .addClass('bgre');
      $('#indice-setting-form-thresholds .curled-box')
        .last()
        .removeClass('bgre')
        .addClass('bggr');
    }
  });
  $('#indice_setting_bigger_better').trigger('change');
  $(document).on('change', '#indice_setting_upper_threshold', function () {
    $('#upper_threshold').text(this.value);
  });
  $('#indice_setting_upper_threshold').trigger('change');
  $(document).on('change', '#indice_setting_middle_threshold', function () {
    $('#middle_threshold').text(this.value);
  });
  $('#indice_setting_middle_threshold').trigger('change');

  var target_values = ['total_work_hours'];
  var invisible_values_for_parent = [
    'estimate_hour_in_past',
    'cost_pv',
    'cost_ev',
    'cost_ac',
    'total_estimate_hour',
  ];
  function change_visibility_of_include_parent_issues_field(
    numerator,
    denominator
  ) {
    if (
      invisible_values_for_parent.indexOf(numerator) >= 0 ||
      invisible_values_for_parent.indexOf(denominator) >= 0
    ) {
      $('#field_custom_parameter_include_parent_issues').hide();
    } else {
      $('#field_custom_parameter_include_parent_issues').show();
    }
  }
  $(document).on('change', '#custom_parameter_cf_numerator', function () {
    var numerator = $('#custom_parameter_cf_numerator').val();
    var time_entry_activity_selector = $(
      '#field_custom_parameter_time_entry_activity_ids_for_numerator'
    );
    if ($.inArray(numerator, target_values) >= 0) {
      time_entry_activity_selector.show();
    } else {
      time_entry_activity_selector.hide();
      $('#custom_parameter_time_entry_activity_ids_for_numerator').val([]);
    }

    var denominator = $('#custom_parameter_cf_denominator').val();
    change_visibility_of_include_parent_issues_field(numerator, denominator);
  });
  $(document).on('change', '#custom_parameter_cf_denominator', function () {
    var denominator = $('#custom_parameter_cf_denominator').val();
    var time_entry_activity_selector = $(
      '#field_custom_parameter_time_entry_activity_ids_for_denominator'
    );
    if ($.inArray(denominator, target_values) >= 0) {
      time_entry_activity_selector.show();
    } else {
      time_entry_activity_selector.hide();
      $('#custom_parameter_time_entry_activity_ids_for_denominator').val([]);
    }

    var numerator = $('#custom_parameter_cf_numerator').val();
    change_visibility_of_include_parent_issues_field(numerator, denominator);
  });
  $('#custom_parameter_cf_numerator').trigger('change');
  $('#custom_parameter_cf_denominator').trigger('change');

  $(document).on('change', '.use_denominator_issue_filter', function () {
    var $customIssueFilterWrapper = $(
      '#custom-denominator-issue-filter-wrapper'
    );

    if ($(this).val() == 'true') {
      $customIssueFilterWrapper.show();

      var $numeratorItems = $('#custom-numerator-issue-filter-items');
      $('#custom-denominator-issue-filter-items').html(
        $numeratorItems.html().replaceAll('numerator', 'denominator')
      );

      var $customDenominatorIssueFilter = $(
        '#add_custom_denominator_issue_filter'
      );
      $numeratorItems.find('.custom-issue-filter-item').each(function () {
        var $numeratorItem = $(this);

        $customDenominatorIssueFilter
          .find("option[value='" + $numeratorItem.data('item') + "']")
          .attr('disabled', 'disabled');

        var $matcher = $numeratorItem.find('.custom-issue-filter-matcher');
        $(
          '#custom_parameter_denominator_filters_' +
            $matcher.parents('.custom-issue-filter-item').data('item') +
            '_matcher'
        ).val($matcher.val());

        $numeratorItem
          .find('.custom-issue-filter-value-content')
          .find('input, select')
          .each(function () {
            var $this = $(this);
            $(
              "[name='" +
                $this.attr('name').replace('numerator', 'denominator') +
                "']"
            ).val($this.val());
          });
      });
    } else {
      $customIssueFilterWrapper
        .find('#add_custom_denominator_issue_filter option')
        .removeAttr('disabled');
      $customIssueFilterWrapper.find('.custom-issue-filter-item').remove();
      $customIssueFilterWrapper.hide();
    }
  });

  $(document).on('change', '.add_custom_issue_filter', function () {
    var $customIssueFilter = $(this);

    $.getJSON(
      $customIssueFilter.data('item-url'),
      {
        number_type: $customIssueFilter.data('number-type'),
        item: $customIssueFilter.val(),
      },
      function (json) {
        $customIssueFilter
          .parents('.custom-issue-filter-tools')
          .siblings('.custom-issue-filter-items')
          .append(json.html);
        $customIssueFilter.find('option:selected').attr('disabled', 'disabled');
        $customIssueFilter.val('');
      }
    );
  });

  $(document).on('change', '.custom-issue-filter-matcher', function () {
    var $matcher = $(this);
    var $filterItem = $matcher.parents('.custom-issue-filter-item');
    var $customIssueFilter = $filterItem
      .parents('.custom-issue-filter-items')
      .siblings('.custom-issue-filter-tools')
      .find('.add_custom_issue_filter');

    $.getJSON(
      $customIssueFilter.data('value-url'),
      {
        number_type: $customIssueFilter.data('number-type'),
        item: $filterItem.data('item'),
        matcher: $matcher.val(),
      },
      function (json) {
        $filterItem.find('.custom-issue-filter-value-content').html(json.html);
      }
    );
  });

  $(document).on(
    'click',
    '.custom-issue-filter-item .ui-icon-close',
    function () {
      var $filterItem = $(this).parents('.custom-issue-filter-item');
      var $customIssueFilter = $filterItem
        .parents('.custom-issue-filter-items')
        .siblings('.custom-issue-filter-tools')
        .find('.add_custom_issue_filter');

      $customIssueFilter
        .find('option[value="' + $filterItem.data('item') + '"]')
        .removeAttr('disabled');
      $filterItem.remove();
    }
  );
});
