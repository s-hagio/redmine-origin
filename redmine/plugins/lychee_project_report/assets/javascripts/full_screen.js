$(function () {
  var FULL_SCREEN_KEY = 'project_report:fullscreen:state';
  var is_full_screen_state = sessionStorage.getItem(FULL_SCREEN_KEY) || 'start';

  updateTriggerElements();

  function toggleState() {
    is_full_screen_state = is_full_screen_state === 'exit' ? 'start' : 'exit';
  }

  function updateTriggerElements() {
    var full_screen_button = document.getElementById('icon-full-screen');
    var custom_query = document.getElementsByClassName('hide-when-print')[0];
    var query_form = document.getElementById('query_form_content');
    var query_buttons = document.getElementById('project_report_query_buttons');
    var setting_buttons = document.getElementById('setting_buttons');
    var project_report_full_screen_buttons = document.getElementById(
      'project_report_full_screen_buttons'
    );
    var project_report_windowed_buttons = document.getElementById(
      'project_report_windowed_buttons'
    );

    full_screen_button.classList.toggle(
      'icon-full-screen-start',
      is_full_screen_state === 'start'
    );
    full_screen_button.classList.toggle(
      'icon-full-screen-exit',
      is_full_screen_state === 'exit'
    );
    custom_query.classList.toggle('invisible', is_full_screen_state === 'exit');
    query_form.classList.toggle('invisible', is_full_screen_state === 'exit');
    query_buttons.classList.toggle(
      'invisible',
      is_full_screen_state === 'exit'
    );

    for (var i = 0; i < setting_buttons.children.length; i++) {
      setting_buttons.children[i].classList.toggle(
        'invisible',
        is_full_screen_state === 'exit'
      );
    }
    full_screen_button.text = full_screen_button.getAttribute(
      'data-fullscreen-label-' + is_full_screen_state
    );

    if (is_full_screen_state === 'start') {
      while (project_report_full_screen_buttons.childNodes.length > 0) {
        project_report_windowed_buttons.appendChild(
          project_report_full_screen_buttons.childNodes[0]
        );
      }
    } else {
      while (project_report_windowed_buttons.childNodes.length > 0) {
        project_report_full_screen_buttons.appendChild(
          project_report_windowed_buttons.childNodes[0]
        );
      }
    }

    window.dispatchEvent(new Event('resize')); // 表の部分の大きさを変えるためresizeイベントを発火させる
  }

  function toggleFullScreen() {
    toggleState();
    updateTriggerElements();
    sessionStorage.setItem(FULL_SCREEN_KEY, is_full_screen_state);
  }

  $('.icon-full-screen').on('click', function () {
    toggleFullScreen();
  });
});
