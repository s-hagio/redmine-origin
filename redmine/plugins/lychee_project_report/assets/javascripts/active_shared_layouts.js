$(function() {
  const $checkAllBoxes = $("a[data-check-all-for]");

  function checkBoxesFor($checkAllBox) {
    const sharedLayoutId = $checkAllBox.data("check-all-for");
    return $(
      '.tab-content:visible table input[value="' + sharedLayoutId + '"]'
    );
  }

  // All boxes for some layout checked already
  function allChecked($checkAllBox) {
    let checked = true;
    checkBoxesFor($checkAllBox).each(function() {
      if (!$(this).is(":checked")) {
        checked = false;
      }
    });
    return checked;
  }

  function addCheckAllHandlers() {
    $checkAllBoxes.click(function(ev) {
      // still some unchecked       -> changedTo = true
      // everything already checked -> changedTo = false
      const changedTo = !allChecked($(ev.target));
      checkBoxesFor($(ev.target)).each(function() {
        this.checked = changedTo;
      });
    });
  }

  let headerVisible = false;
  const $stickyHeader = $('<div class="fixed-thead"/>');

  function findTableHead() {
    return $(".tab-content:visible thead");
  }

  function showStickyHeader() {
    const $head = findTableHead();
    $head.find("th").each(function() {
      const $header = $('<div class="table-list-header"/>');
      $(this)
        .children()
        .clone(true)
        .appendTo($header);
      $header.width(this.clientWidth);
      $header.height($head.height());
      $header.appendTo($stickyHeader);
    });
    $stickyHeader.css("left", $head.offset().left);
    $stickyHeader.appendTo("body");
  }

  function hideStickyHeader() {
    $stickyHeader.detach();
    $stickyHeader.empty();
  }

  function throttle(f, interval) {
    let lastInvokeTime = 0;
    let timeoutId;

    function finalInvoke() {
      f();
      timeoutId = null;
    }

    return function() {
      const now = new Date().getTime();
      if (now - lastInvokeTime < interval) {
        return;
      }

      lastInvokeTime = now;
      if (timeoutId) {
        clearTimeout(timeoutId);
      }
      timeoutId = setTimeout(finalInvoke, interval);
      return f();
    };
  }

  function initializeScrollingHeader() {
    const tableTop = $(".tab-content:visible table").offset().top;
    $(window).scroll(function() {
      if (!headerVisible && window.pageYOffset > tableTop) {
        showStickyHeader();
        headerVisible = true;
      } else if (headerVisible && window.pageYOffset < tableTop) {
        hideStickyHeader();
        headerVisible = false;
      }
    });

    $(window).resize(function() {
      if (headerVisible) {
        hideStickyHeader();
        showStickyHeader();
      }
    });

    $(".tab-content .autoscroll").scroll(
      throttle(function() {
        $stickyHeader.css("left", findTableHead().offset().left);
      }, 100)
    );
  }

  addCheckAllHandlers();
  initializeScrollingHeader();
});
