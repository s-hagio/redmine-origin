$(function() {
  // 一括チェック
  $("a[data-check-all-for]").click(function() {
    const selector =
      "td[data-indice-setting-id='" +
      $(this).data("check-all-for") +
      "'] input[type='checkbox']:not(:disabled)";
    let allChecked = true;
    $(selector).each(function() {
      if (!$(this).is(":checked")) {
        allChecked = false;
      }
    });
    $(selector)
      .prop("checked", !allChecked)
      .trigger("change");
  });

  // 「個別」のチェックボックス
  $(".indivisual-checkbox")
    .on("change", function() {
      var project_id = $(this).data("project-id");
      var checkboxes = $(
        "tr[data-root-project-id=" + project_id + '] input[type="checkbox"]'
      );
      checkboxes.prop("disabled", !$(this).prop("checked"));
    });

  // 指標を追加するダイアログ
  (function() {
    var $dialog = $("#add-indice-type-dialog").dialog({
      autoOpen: false,
      modal: true,
      buttons: [
        {
          text: "追加",
          class: "submit-button",
          click: function() {
            $("#new-project-report-setting-column form").submit();
            $dialog.dialog("close");
          }
        },
        {
          text: "キャンセル",
          click: function() {
            $dialog.dialog("close");
          }
        }
      ]
    });

    var $dialogForm = $dialog.find("form");

    $(".open-indice-setting-dialog-button").on("click", function() {
      var type = $(this).data("type");

      var $inputType = $dialogForm.find('input[type="hidden"][name="type"]');
      if ($inputType.length === 0) {
        $('<input type="hidden">')
          .attr("value", type)
          .attr("name", "type")
          .appendTo($dialogForm);
      } else {
        $inputType.val(type);
      }

      updateOptions();
      $dialog.dialog("open");
    });

    var $indiceTypeSelect = $('[name="indice_type"]').on(
      "change",
      updateOptions
    );

    // 選択された指標種別によって指標設定の選択肢を出し分ける
    function updateOptions() {
      var type = $dialogForm.find('[name="type"]').val();
      var $button = $(
        '.open-indice-setting-dialog-button[data-type="' + type + '"]'
      );
      var indiceSettings =
        $button.data("indiceSettings")[$indiceTypeSelect.val()] || [];

      $(".submit-button").prop("disabled", indiceSettings.length === 0);

      var $options = $.map(indiceSettings, function(setting) {
        return $("<option>")
          .attr("value", setting.id)
          .text(setting.name);
      });

      $("#enabled_report_indice_setting_indice_setting_id").html($options);
    }
  })();
});
