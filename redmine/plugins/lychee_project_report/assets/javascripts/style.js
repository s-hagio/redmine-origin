$(function() {
  function set_more_link(comment_box) {
    const column_box = $(comment_box).parents(".columnbox");
    const box_height = column_box.outerHeight();
    let box_title_height = 0;
    const box_title = column_box.find("h3");
    if (box_title.length > 0) {
      box_title_height = box_title.outerHeight(true);
    }
    const comment_height = $(comment_box).outerHeight(true);
    if (box_height < box_title_height + comment_height) {
      column_box
        .append('<a href="#"><span class="more">もっと見る</span></a>')
        .scrollTop(0);
    } else {
      column_box
        .find(".more")
        .parent()
        .remove();
    }
  }

  $(".edit").click(function() {
    const $column_box = $(this).parent(".column");
    $column_box.addClass("on");
    $(".column.on p.comment").addClass("on");
    const $comment_box = $(".column.on p.comment");
    const $textarea = $('<textarea rows="10" />').html($comment_box.html());
    $(".column.on p.comment").html($textarea);
    //同時にtextareaにフォーカスをする
    $textarea.focus().blur(function() {
      $.ajax({
        type: "PATCH",
        dataType: "json",
        url: $(this)
          .parents(".column")
          .children(".ajax-path")
          .data("update-path"),
        data: { projectReportComment: { text: $(this).val() } }
      })
        .done(function(json) {
          //編集が終わったらtextareaで置き換える
          $comment_box.removeClass("on").text(json.text);
          set_more_link($comment_box);
          $column_box.removeClass("on");
        })
        .fail(function() {
          alert("エラーが発生しました。");
        });
    });
  });

  $(".column p.comment").each(function(_, comment_box) {
    set_more_link(comment_box);
  });
  $(document).on("click", ".more", function() {
    $.ajax({
      type: "GET",
      url: $(this)
        .parents(".column")
        .children(".ajax-path")
        .data("show-path")
    });
  });
});
