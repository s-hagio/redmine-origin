$(function(){
  function getColor(idx){
    var colorsets = [
      'rgb(255, 99, 132)', // red
      'rgb(54, 162, 235)', // blue
      'rgb(75, 192, 192)', // green
      'rgb(255, 159, 64)', // orange
      'rgb(255, 205, 86)', // yellow
      'rgb(153, 102, 255)',// purple
      'rgb(201, 203, 207)' // grey
    ];
    var color = colorsets[idx];
    if(color){
      return(color);
    }else{
      return '#'+Math.floor(Math.random()*16777215).toString(16);
    }
  }
  var labels = $("#report-table td:nth-of-type(1) a").map(function(){ return($(this).text().trim()) }).toArray().reverse();
  var column_count = $("#report-table th").length - 1;
  var datasets = [];
  if(column_count > 0){
    for(var idx = 0; idx < column_count; idx++){
      datasets[idx] = {}
      datasets[idx].label =  $("#report-table th:nth-of-type(" + (idx + 2) + ")").text();
      datasets[idx].data = $("#report-table td:nth-of-type(" + (idx + 2) + ")").map(function(){
        var val = $(this).clone().children().remove().end().text().trim();
        return(val.replace(/%$/,""));
      }).toArray().reverse();
      var color = getColor(idx)
      datasets[idx].borderColor = color;
      datasets[idx].backgroundColor = color;
      datasets[idx].fill = false
    }
  }
  var graph_config = {
    type: 'line',
    data: {
      labels: labels,
      datasets: datasets
    },
    options: {
      responsive: true,
      title:{
        display: false,
        text:''
      },
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: projectReportGraphConfig.xAxesLabel
          }
        }],
        yAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: projectReportGraphConfig.yAxesLabel
          }
        }]
      }
    }
  };

  var element = document.getElementById("report-graph");
  if(element){
    var ctx = element.getContext("2d");
    new Chart(ctx, graph_config);
  }
});
