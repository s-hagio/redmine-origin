$(function () {
  $('.open-box').each(function () {
    var $ele = $(this);
    $ele.find('.open-contents').hide();
    $ele.find('.open-contents2').hide();
    $ele.find('.open-arrow').click(function () {
      $ele.find('.open-contents').slideToggle(); //slideToggleは両方　slideDownは開く　slideUpは閉じる
      return false;
    });
    $ele.find('.open-arrow2').click(function () {
      $ele.find('.open-contents2').slideToggle(); //slideToggleは両方　slideDownは開く　slideUpは閉じる
      return false;
    });
  });

  // jump to link on select version
  $(document).on('change', '#version_report_link', function () {
    var link = $(this).val();
    if (link !== '') location.href = link;
  });

  $(document).on('change', '#query_id', function () {
    var query_id = $.trim($(this).val());
    location.search = 'query_id=' + query_id;
  });

  $('#query_form').on('submit', function (event) {
    $.ajax({
      type: 'GET',
      url: $('#validates_project_report_queries_path').val(),
      data: $('#query_form').serialize(),
      async: false,
    }).done(function (data) {
      if (data.valid) {
        return true;
      } else {
        event.preventDefault();
        if (!$('#errorExplanation').length) {
          $('h2').after($('<div>', { id: 'errorExplanation' }));
        }
        $('#errorExplanation')
          .html(
            $('<ul>').html(
              $.map(data.error_messages, function (message) {
                return $('<li>').html(message);
              })
            )
          )
          .show();
      }
    });
  });

  $('#query_form_with_buttons .icon.icon-checked').on('click', function () {
    $('#query_form').submit();
  });
});
