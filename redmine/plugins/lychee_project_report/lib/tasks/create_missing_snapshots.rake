# frozen_string_literal: true

namespace :redmine do
  namespace :plugins do
    namespace :lychee_project_report do
      desc 'Create missing panel snapshots.'
      task create_missing_panel_snapshots: :environment do
        project_reports = ProjectReport.where(record: ProjectPeriodicReportRow.fixed)
        LycheeProjectReportPanel.joins(:project_report_layout).where(
          type: LycheeProjectReportPanel.snapshot_types,
          project_report_layouts: { project_report_id: project_reports }
        ).each(&:take_snapshot)

        version_reports = ProjectReport.where(record: ProjectVersionPeriodicReportRow.fixed)
        LycheeProjectReportPanel.joins(:project_report_layout).where(
          type: LycheeProjectReportPanel.snapshot_types,
          project_report_layouts: { project_report_id: version_reports }
        ).each(&:take_snapshot)
      end
    end
  end
end
