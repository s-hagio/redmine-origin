namespace :redmine do
  namespace :plugins do
    namespace :lychee_project_report do
      desc 'initial data for lychee project report'
      task initial_data: :environment do
        LycheeProjectReport::Initializer.invoke
      end

      desc 'add custom field modules'
      task add_custom_field_modules: :environment do
        LycheeProjectReport::Initializer.add_custom_field_modules
      end

      desc 'remove custom field modules'
      task remove_custom_field_modules: :environment do
        LycheeProjectReport::Initializer.remove_custom_field_modules
      end

      desc 'add project EAC modules'
      task add_project_eac_modules: :environment do
        LycheeProjectReport::Initializer.add_project_eac_modules
      end

      desc 'remove project EAC modules'
      task remove_project_eac_modules: :environment do
        LycheeProjectReport::Initializer.remove_project_eac_modules
      end

      desc 'add project additional EVM modules'
      task add_additional_evm_modules: :environment do
        LycheeProjectReport::Initializer.add_additional_evm_modules
      end

      desc 'remove project additional EVM modules'
      task remove_additional_evm_modules: :environment do
        LycheeProjectReport::Initializer.remove_additional_evm_modules
      end

      desc 'add indice_settings'
      task add_indice_settings: :environment do
        LycheeProjectReport::Initializer.add_indice_settings
      end

      desc 'add EVM to calculate modules'
      task add_evm_modules: :environment do
        LycheeProjectReport::Initializer.add_evm_modules
      end

      desc 'remove EVM from calculate modules'
      task remove_evm_modules: :environment do
        LycheeProjectReport::Initializer.remove_evm_modules
      end
    end
  end
end
