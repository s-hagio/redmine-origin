namespace :redmine do
  namespace :plugins do
    namespace :lychee_project_report do
      namespace :migrate do
        desc 'migrate to project periodic report row from project periodic report.'
        task periodic_report_to_periodic_report_row: :environment do
          LycheeProjectReport::Migrate.periodic_report_to_periodic_report_row
        end

        desc 'rollback from project periodic report row to project periodic report.'
        task periodic_report_row_to_periodic_report: :environment do
          LycheeProjectReport::Migrate.periodic_report_row_to_periodic_report
        end

        desc 'update serialized columns by hash'
        task update_serialized_columns_by_hash: :environment do
          module MigrateUpdateSerializedColumnsByHash
            class IndiceSetting < ActiveRecord::Base
              self.table_name = "#{table_name_prefix}indice_settings#{table_name_suffix}"
              serialize :custom_parameter
            end
          end
          puts 'update IndiceSetting#custom_parameter'
          MigrateUpdateSerializedColumnsByHash::IndiceSetting.find_each do |indice_setting|
            if indice_setting.custom_parameter.is_a?(ActionController::Parameters)
              indice_setting.update_columns(
                custom_parameter: indice_setting.custom_parameter.to_unsafe_hash.with_indifferent_access
              )
            end
          end
        end

        desc 'fix test env migration numbers after squash'
        task fix_test_env_migration_numbers: :environment do
          module FixMigrationNumbers
            class SchemaMigration < ActiveRecord::Base
              self.primary_key = :version

              def self.project_report_migrations
                where("version LIKE '%-lychee_project_report'")
              end

              def version_number
                version.split('-')[0].to_i
              end
            end
          end
          FixMigrationNumbers::SchemaMigration.project_report_migrations.each do |migration|
            migration.destroy if migration.version_number > 68
          end
        end
      end
    end
  end
end
