namespace :redmine do
  namespace :plugins do
    namespace :lychee_project_report do
      desc 'update calculate modules for lychee project report'
      task update_calculate_modules: :environment do
        LycheeProjectReport::Initializer.update_calculate_modules
      end
    end
  end
end
