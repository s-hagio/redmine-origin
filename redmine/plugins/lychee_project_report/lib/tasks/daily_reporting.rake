namespace :redmine do
  namespace :plugins do
    namespace :lychee_project_report do
      desc 'Create a daily project report'
      task daily_reporting: :environment do
        begin
          execute_user = User.current
          User.current = User.where(admin: true).active.order(:id).first
          Project.daily_report
        ensure
          User.current = execute_user
        end
      end
    end
  end
end
