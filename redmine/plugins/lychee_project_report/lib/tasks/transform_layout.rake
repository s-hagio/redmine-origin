namespace :redmine do
  namespace :plugins do
    namespace :lychee_project_report do
      namespace :transform_layout do
        desc 'transform all layout panels(width: 1/4, height: 2/1)'
        task transform_all_layouts: :environment do
          width_ratio = 1.0/4
          height_ratio = 1.0/2
          LycheeProjectReport::TransformLayout.update_all width_ratio, height_ratio
        end
      end
    end
  end
end
