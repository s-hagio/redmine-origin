# frozen_string_literal: true

namespace :redmine do
  namespace :plugins do
    namespace :lychee_project_report do
      task seed: :environment do
        load Rails.root.join('plugins/lychee_project_report/db/seed.rb')
      end
    end
  end
end
