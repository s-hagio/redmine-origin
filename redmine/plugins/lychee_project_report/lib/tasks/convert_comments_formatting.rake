namespace :redmine do
  namespace :plugins do
    namespace :lychee_project_report do
      desc 'convert textile and markdown comments to html'
      task convert_comments_formatting: :environment do
        ProjectReportComment.convert_all_to_html
      end
    end
  end
end
