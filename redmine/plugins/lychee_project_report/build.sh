# /bin/bash

yarn parcel build src/bundle.tsx src/overview/index.ts -d ./assets/javascripts --public-url ./

mv ./assets/javascripts/*.css* ./assets/stylesheets
