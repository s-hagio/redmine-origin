# frozen_string_literal: true

resources :project_reports, controller: :project_report_overview, only: %i[index] do
  collection do
    resources :queries,
              only: %i[new edit create update destroy],
              controller: :project_report_queries,
              as: :project_report_queries do
      get :validates, on: :collection
    end

    resources :comments, controller: :project_report_comments, only: %i[show update], as: :project_report_comments

    resources :layouts, controller: :project_report_layouts, only: %i[update destroy], as: :project_report_layouts do
      resources :panels, controller: :lychee_project_report_panels, only: %i[index create show update destroy]

      post :create_shared_layout, to: 'project_report_shared_layouts#create'
    end

    resources :shared_layouts, controller: :project_report_shared_layouts, only: %i[update destroy], as: :project_report_shared_layouts
    resources :active_shared_layouts,
              controller: :project_report_active_shared_layouts,
              only: %i[index],
              as: :project_report_active_shared_layouts
    patch :active_shared_layouts, to: 'project_report_active_shared_layouts#bulk_update', as: :update_active_shared_layouts
  end

  resources :layouts, controller: :project_report_layouts, only: %i[create]

  resources :shared_layouts, only: %i[] do
    resources :panels, controller: :lychee_project_report_panels, only: %i[index create show update destroy]
  end
end

resource :project_report_setting, only: [:edit] do
  collection do
    patch :bulk_update
  end
end

resources :enabled_report_indice_settings, only: %i[index create update destroy]

resources :indice_settings do
  collection do
    get :calculate_module
    get :indice_type
    get :filter_item
    get :filter_value
  end
end

namespace :lychee_project_report do
  resources :projects, only: [:update] do
    resources :issues, only: [] do
      get :render, action: :render_html, on: :collection
    end
  end
end

resources :projects, only: [] do
  namespace :project_reports do
    resources :histories, only: %i[index]
  end

  resources :project_reports, only: %i[index show update], param: :date
  resource :project_report, only: %i[show], as: :current_report do
    member do
      post :reculculate
      get :operation_finished
    end
  end

  resources :versions, only: [] do
    resources :project_reports, only: %i[index show update], param: :date
    resource :project_report, only: %i[show], controller: :project_reports, as: :current_report do
      member do
        post :reculculate
        get :operation_finished
      end
    end
  end
end

patch 'lychee_project_report/settings', to: 'lychee_project_report/settings#update'
