import Highcharts from "highcharts";
import i18n from "i18next";
import { initReactI18next, useTranslation } from "react-i18next";

import enJson from "./locales/en.json";
import jaJson from "./locales/ja.json";

export async function init(lng: string): Promise<void> {
  i18n.use(initReactI18next).init({
    debug: true,
    resources: {
      en: { translation: enJson },
      ja: { translation: jaJson }
    },
    lng: lng,
    fallbackLng: "en",
    returnEmptyString: false,
    nsSeparator: "|"
  });

  if (lng === "ja") {
    Highcharts.setOptions({
      lang: {
        thousandsSep: ',',
        numericSymbolMagnitude: 10000,
        numericSymbols: ["万", "億", "兆"],
        shortWeekdays: ["日", "月", "火", "水", "木", "金", "土"],
        shortMonths: [
          "1月",
          "2月",
          "3月",
          "4月",
          "5月",
          "6月",
          "7月",
          "8月",
          "9月",
          "10月",
          "11月",
          "12月"
        ]
      }
    });
  }
}

export const useTranslationWithPrefix = (prefix: string): { t: TFunction } => {
  const { t } = useTranslation();
  return { t: (key: string): string => t(`${prefix}.${key}`) };
};
