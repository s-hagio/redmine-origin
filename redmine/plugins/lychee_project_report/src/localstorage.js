const LOCALSTORAGE_EVM_FILTER = 'Lychee_Project_Report_Evm_Filter';

const initialfunc = () => {
  const Filter = localStorage.getItem(LOCALSTORAGE_EVM_FILTER);
  const initialState = JSON.parse(Filter) || {};
  return initialState;
};

export const setEvmFilter = (projectId, value) => {
  const initialState = initialfunc();
  initialState[projectId] = value;
  localStorage.setItem(LOCALSTORAGE_EVM_FILTER, JSON.stringify(initialState));
};

export const getEvmFilter = (projectId) => {
  const initialState = initialfunc();
  return initialState[projectId];
};
