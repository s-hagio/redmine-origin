export interface PanelPosition {
  x: number;
  y: number;
  w: number;
  h: number;
}

export type PanelType =
  | 'LycheeProjectReport::Panels::Overview'
  | 'LycheeProjectReport::Panels::Comment'
  | 'LycheeProjectReport::Panels::Report'
  | 'LycheeProjectReport::Panels::PeriodicReport'
  | 'LycheeProjectReport::Panels::Evm'
  | 'LycheeProjectReport::Panels::Milestone'
  | 'LycheeProjectReport::Panels::Info'
  | 'LycheeProjectReport::Panels::Organization'
  | 'LycheeProjectReport::Panels::IssueList'
  | 'LycheeProjectReport::Panels::Memo';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Json = { [key: string]: any };

export interface Comment {
  id: number;
  text: string;
  renderedText: string;
}

export type Panel<C extends Json = Json, S extends Json = Json> = {
  id: number;
  type: PanelType;
  content: C;
  comment?: Comment;
  position: PanelPosition;
  settings: S;
};

export type PanelResponse =
  | Panel
  | {
      id: number;
      type: PanelType;
      content: null;
      error: true;
      position: PanelPosition;
      settings: Json;
    };

export interface PanelRootComponentProps {
  children: React.ReactNode;
  title: string;
  isTitleDisplay?: boolean;
  titlebarButtons?: React.ReactNode;
  renderPanelSettings?: ({
    updateSettings,
    closeSettings,
  }) => React.ReactElement;
}

export interface PanelProps<C extends Json, S extends Json = {}> {
  PanelRootComponent: React.FC<PanelRootComponentProps>;
  updateSettings: (settings: S) => void;
  panel: Panel<C, S>;
  editLayoutMode?: boolean;
}

export interface Layout {
  id: number;
  name: string;
  shared: boolean;
  params: { layout_id: number; shared?: boolean };
}
