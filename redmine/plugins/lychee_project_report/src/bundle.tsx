import "@babel/polyfill";
import "url-polyfill";
import "isomorphic-unfetch";
import * as React from "react";
import * as ReactDOM from "react-dom";
import smoothScroll from "smoothscroll-polyfill";
import "react-toastify/dist/ReactToastify.min.css";
import ProjectReport from "./components/ProjectReport";
import "./module.scss";
import { init as initI18n } from "./i18n";

const language = document.getElementById("project-report").dataset.language;
initI18n(language);

smoothScroll.polyfill(); // IEやEdgeなどに対応するため

ReactDOM.render(<ProjectReport />, document.getElementById("project-report"));
