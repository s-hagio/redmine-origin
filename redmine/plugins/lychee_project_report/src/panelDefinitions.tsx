import {
  PanelType,
  PanelPosition,
  PanelProps,
  Json
} from "./interfaces/interface";
import OverviewPanel from "./components/OverviewPanel";
import CommentPanel from "./components/CommentPanel";
import ReportPanel from "./components/ReportPanel";
import PeriodicReportPanel from "./components/PeriodicReportPanel";
import IssueListPanel from "./components/IssueListPanel";
import MemoPanel from "./components/MemoPanel";
import EvmPanel from "./components/EvmPanel";
import MilestonePanel from "./components/MilestonePanel";
import InfoPanel from "./components/InfoPanel";
import OrganizationPanel from "./components/OrganizationPanel";

const definitions: {
  [key in PanelType]: {
    component: React.FC<PanelProps<Json, Json>>;
    defaultPosition: PanelPosition;
  };
} = {
  "LycheeProjectReport::Panels::Overview": {
    component: OverviewPanel,
    defaultPosition: { w: 10, h: 3, x: 0, y: 0 }
  },
  "LycheeProjectReport::Panels::Comment": {
    component: CommentPanel,
    defaultPosition: { w: 10, h: 3, x: 0, y: 0 }
  },
  "LycheeProjectReport::Panels::Report": {
    component: ReportPanel,
    defaultPosition: { w: 10, h: 3, x: 0, y: 0 }
  },
  "LycheeProjectReport::Panels::PeriodicReport": {
    component: PeriodicReportPanel,
    defaultPosition: { w: 20, h: 6, x: 0, y: 0 }
  },
  "LycheeProjectReport::Panels::IssueList": {
    component: IssueListPanel,
    defaultPosition: { w: 15, h: 5, x: 0, y: 0 }
  },
  "LycheeProjectReport::Panels::Memo": {
    component: MemoPanel,
    defaultPosition: { w: 10, h: 3, x: 0, y: 0 }
  },
  "LycheeProjectReport::Panels::Evm": {
    component: EvmPanel,
    defaultPosition: { w: 30, h: 7, x: 0, y: 0 }
  },
  "LycheeProjectReport::Panels::Milestone": {
    component: MilestonePanel,
    defaultPosition: { w: 20, h: 5, x: 0, y: 0 }
  },
  "LycheeProjectReport::Panels::Info": {
    component: InfoPanel,
    defaultPosition: { w: 10, h: 3, x: 0, y: 0 }
  },
  "LycheeProjectReport::Panels::Organization": {
    component: OrganizationPanel,
    defaultPosition: { w: 10, h: 4, x: 0, y: 0 }
  }
};
export default definitions;
