import * as React from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import JoditEditor from 'jodit-react';
import 'jodit/build/jodit.min.css';
import { sendRequest } from '../api';
import { Comment } from '../interfaces/interface';
import PageVariables from '../pageVariables';
import Modal from './atoms/Modal';
import { TitlebarButton } from './atoms/PanelLayout';

const MAX_SUBMIT = 3 * 1024 * 1024; // text + image < 3MB
const EditCommentButton = styled(TitlebarButton).attrs(() => ({
  'data-test': 'edit',
}))`
  background: url(${PageVariables.urls.root}/images/edit.png) no-repeat;
`;

const EditCloseButton = styled.button`
  background: lightblue;
  color: white;
  cursor: pointer;
`;

const CommentArea = styled.div`
  overflow-y: auto;
  height: 100%;
`;

const CommentDisplay = styled.p`
  overflow-wrap: break-word;
  white-space: normal;
  word-wrap: break-word; // IE11 対策。 overflow-wrap が使えないから

  ul,
  ol {
    padding-left: 40px;
  }

  ul {
    list-style: disc;

    ul {
      list-style: circle;

      ul {
        list-style: square;
      }
    }
  }

  ol {
    list-style: decimal;
  }
`;

const CommentTextArea = styled(CommentDisplay)`
  resize: none;
  width: 100%;
  height: 100%;
  min-height: 85px;
  box-sizing: border-box;
`;

export const MoreButton = styled.span`
  display: inline-block;
  position: absolute;
  bottom: 0px;
  right: 5px;
  background-color: #bbc8e6;
  border: solid 1px #ddd;
  color: #fff;
  padding: 5px 10px;
  cursor: pointer;
`;

interface CommentFieldProps {
  className?: string;
  editMode: boolean;
  content: string;
  submit: (value: string) => void;
}

const CommentField: React.FC<CommentFieldProps> = ({
  className,
  editMode,
  content,
  submit,
}: CommentFieldProps) => {
  const { t, i18n } = useTranslation();
  const textArea = React.useRef(null);
  const commentArea = React.useRef(null);
  const commentDisplay = React.useRef(null);
  const [moreButtonVisible, setMoreButtonVisible] = React.useState(false);

  // elements の要素群がリサイズされた場合に callback を処理するオブザーバー
  const useResizeObserver = (
    elements: React.RefObject<HTMLObjectElement>[],
    callback: Function
  ) => {
    React.useEffect(() => {
      const resizeObserver = new ResizeObserver((entries) => {
        callback(entries);
      });
      for (const elem of elements) {
        elem.current && resizeObserver.observe(elem.current);
      }
      return () => resizeObserver.disconnect();
    }, []);
  };

  const handleMoreButtonVisible = () => {
    if (editMode) {
      textArea.current.focus();
      if (typeof commentArea.current.scrollIntoViewIfNeeded === 'function') {
        commentArea.current.scrollIntoViewIfNeeded();
      }
    } else {
      if (commentDisplay.current) {
        setMoreButtonVisible(
          commentDisplay.current.clientHeight > commentArea.current.clientHeight
        );
      }
    }
  }

  // リサイズ時にボタン表示を判定
  useResizeObserver([commentArea], handleMoreButtonVisible);

  React.useEffect(() => {
    handleMoreButtonVisible();
  }, [editMode]);

  const config = {
    language: i18n.language.toString() || 'ja',
    readonly: false, // all options from https://xdsoft.net/jodit/doc/
    useSearch: false,
    autofocus: true,
    toolbarButtonSize: 'xsmall',
    showCharsCounter: false,
    showWordsCounter: false,
    showXPathInStatusbar: false,
    toolbarAdaptive: false,
    buttons:
      'bold,italic,underline,strikethrough,ul,ol,paragraph,table,indent,outdent,brush',
    colorPickerDefaultTab: 'color',
    disablePlugins: 'iframe',
    askBeforePasteHTML: false,
    askBeforePasteFromWord: false,
    defaultActionOnPaste: 'insert_clear_html',
    uploader: {
      insertImageAsBase64URI: true,
    },
  };

  return (
    <>
      {editMode ? (
        <CommentTextArea className={className} ref={commentArea}>
          <JoditEditor
            ref={textArea}
            value={content}
            config={config}
            onChange={(): void => {
              if (textArea.current.value.length > MAX_SUBMIT) {
                alert(t('data_too_big_error'));
              }
            }}
            onBlur={(): void => {
              if (textArea.current.value.length <= MAX_SUBMIT) {
                submit(textArea.current.value);
              }
            }}
          />
        </CommentTextArea>
      ) : (
        <CommentArea className="wiki" ref={commentArea}>
          <CommentDisplay
            data-test="comment"
            dangerouslySetInnerHTML={{ __html: content }}
            ref={commentDisplay}
          />
          {moreButtonVisible && (
            <>
              <Modal
                trigger={
                  <MoreButton role="button">{t('label_more')}</MoreButton>
                }
                title={t('label_comment_modal')}
              >
                {
                  <CommentDisplay
                    dangerouslySetInnerHTML={{ __html: content }}
                  />
                }
              </Modal>
            </>
          )}
        </CommentArea>
      )}
    </>
  );
};

interface UseCommentFieldResult {
  renderEditButton: () => React.ReactElement;
  renderComment: () => React.ReactElement;
}

// 編集モード切り替えなどのボイラープレートコードを担当するカスタムHook
export const useCommentField = (
  comment: Comment,
  setCommentVisible: (value: boolean) => void
): UseCommentFieldResult => {
  const { t } = useTranslation();
  const [editMode, setEditMode] = React.useState(false);
  const [currentComment, setCurrentComment] = React.useState(comment);
  const [prevComment, setPrevComment] = React.useState(comment.text);
  const commentUrl = `project_reports/comments/${comment.id}`;

  const handleSubmit = React.useCallback(
    (text) => {
      if (prevComment === text) {
        return;
      }

      sendRequest('PATCH', commentUrl, {
        projectReportComment: { text },
      })
        .then((response) => response.json())
        .then(
          (response) => {
            setPrevComment(response.text);
            setCurrentComment(response);
            setCommentVisible(!!text);
            setEditMode(false);
          },
          (err) => {
            alert(t('submit_error'));
            console.error('sendRequest error', err);
          }
        );
    },
    [commentUrl, prevComment, setCommentVisible]
  );

  const renderEditButton = React.useCallback(
    (): React.ReactElement =>
      editMode ? (
        <EditCloseButton
          onClick={(): void => {
            setEditMode(false);
            setCommentVisible(!!currentComment.text);
          }}
        >
          {t('button_close')}
        </EditCloseButton>
      ) : (
        <EditCommentButton
          onClick={(): void => {
            setEditMode(true);
            setCommentVisible(true);
          }}
        />
      ),
    [editMode, setCommentVisible]
  );

  const renderComment = React.useCallback(
    (): React.ReactElement => (
      <CommentField
        editMode={editMode}
        content={currentComment.text || ''}
        submit={handleSubmit}
      />
    ),
    [currentComment, editMode, handleSubmit]
  );
  return { renderEditButton, renderComment };
};

export default CommentField;
