import * as React from "react";
import { toast } from "react-toastify";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

const Select = styled.select`
  width: auto;
  max-width: 300px;
  text-overflow: ellipsis;
`;

export interface ReportPanelSettings {
  indexId: number | null;
}

interface ReportSettingsFormProps {
  settings: ReportPanelSettings;
  closeSettings: () => void;
  updateSettings: (settings: ReportPanelSettings) => void;
  availableIndexes: Array<{ id: number; name: string }>;
}

const ReportPanelSettingsForm: React.FC<ReportSettingsFormProps> = ({
  closeSettings,
  updateSettings,
  settings,
  availableIndexes
}: ReportSettingsFormProps) => {
  const { t } = useTranslation();
  const [selected, setSelected] = React.useState(settings.indexId);
  const options = [
    <option key="-1" value="">
      {t("select_indice")}
    </option>
  ].concat(
    availableIndexes.map((obj, i) => (
      <option key={`${i}`} value={obj.id}>
        {obj.name}
      </option>
    ))
  );

  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const { value } = event.target;
    if (value !== "") {
      setSelected(parseInt(value, 10));
    }
  };

  const submit = (): void => {
    updateSettings({ indexId: selected });
    toast(t("save_setting_alert"));
    closeSettings();
  };

  return (
    <div>
      <Select value={selected} onChange={handleChange}>
        {options}
      </Select>
      <div>
        <button disabled={!selected} onClick={submit}>
          {t("button_save")}
        </button>
        <button onClick={closeSettings}>{t("button_cancel")}</button>
      </div>
    </div>
  );
};

export default ReportPanelSettingsForm;
