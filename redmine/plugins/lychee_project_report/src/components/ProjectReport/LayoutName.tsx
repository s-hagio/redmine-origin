import * as React from "react";
import ReactDOMServer from 'react-dom/server';
import styled from "styled-components";
import tippy from "tippy.js";

import textMetrics from "./text-metrics";
import { updateLayout } from "../../api";
import { Layout } from "../../interfaces/interface";
import PageVariables from "../../pageVariables";
import LayoutSelector from "./LayoutSelector";

interface WrapperProps {
  editing: boolean;
  updating: boolean;
  width: number;
}

const Wrapper = styled.div<WrapperProps>`
  display: flex;
  align-items: center;
  margin-right: ${(props): string => (props.editing ? "10px" : "")};
  height: 40px;
  flex-basis: ${(props): string => props.width.toString()}px;

  &::after {
    content: "_";
    color: transparent;
    display: block;
    height: 14px;
    width: ${(props): string =>
      props.editing || props.updating ? "14px" : "0"};
    background: ${(props): string =>
      props.updating
        ? `url(${PageVariables.urls.root}/images/loading.gif) no-repeat`
        : ""};
  }
`;

const nameStyle: React.CSSProperties = {
  fontSize: "16px",
  fontWeight: "bold",
  fontFamily: "Verdana, sans-serif"
};

const calcWidth = () => {
  const metrics = textMetrics.init(nameStyle);
  const maxOptionWidth = Math.max(
    ...PageVariables.availableLayouts.map(o => metrics.width(o.name))
  );
  return maxOptionWidth + 60;
};

const width = calcWidth();

const StyledInput = styled.input`
  width: 100%;
  padding-right: 24px !important;
  margin-right: -20px;
`;

const StyledLayoutSelector = styled(LayoutSelector)`
  width: 100%;
`;

type LayoutNameProps = {
  layout: Layout;
  editing: boolean;
  onChange?: (layout: Layout) => void;
};

const LayoutName: React.FC<LayoutNameProps> = (props: LayoutNameProps) => {
  const { editing, layout, onChange } = props;
  const [title, setTitle] = React.useState<string>(layout.name);
  const [updating, setUpdating] = React.useState<boolean>(false);
  const ref = React.useRef(null);

  const submit = React.useCallback(
    async (value): Promise<void> => {
      try {
        if (title !== value && value !== "") {
          setUpdating(true);
          const response = await updateLayout({ name: value });
          if (!response.ok) throw response;

          setTitle(value);
          if (onChange) {
            onChange({
              ...layout,
              name: value
            });
          }
        }
      } catch (error) {
        const { messages } = await error.json();
        const [message] = messages;
        const style = { fontSize: 14 };
        const poppover = tippy(ref.current, {
          allowHTML: true,
          content: ReactDOMServer.renderToString(<span style={style} dangerouslySetInnerHTML={{ __html: message }} />),
          theme: 'error',
          trigger: 'manual',
        });
        poppover.show();
      } finally {
        setUpdating(false);
      }
    },
    [title, layout, onChange, ref]
  );

  const handleBlur = React.useCallback(
    (event: React.FocusEvent<HTMLInputElement>) => {
      submit(event.target.value);
    },
    [submit]
  );

  const handleKeyDown = React.useCallback(
    (event: React.FocusEvent<HTMLInputElement>) => {
      switch (event.key) {
        case "Enter":
          submit(event.target.value);
          break;
        default:
          break;
      }
    },
    [submit]
  );

  return (
    <Wrapper editing={editing} updating={updating} width={width} ref={ref}>
      {editing ? (
        <StyledInput
          id="layout-title"
          defaultValue={layout.name}
          onBlur={handleBlur}
          onKeyDown={handleKeyDown}
          style={nameStyle}
        />
      ) : (
        <StyledLayoutSelector currentLayout={layout} nameStyle={nameStyle} />
      )}
    </Wrapper>
  );
};

export default LayoutName;
