// Copied from text-metrics package so parcel will compile it
function e(e, t, r) {
  return (
    t in e
      ? Object.defineProperty(e, t, {
          value: r,
          enumerable: !0,
          configurable: !0,
          writable: !0
        })
      : (e[t] = r),
    e
  );
}
function t(e, t) {
  const r = Object.keys(e);
  if (Object.getOwnPropertySymbols) {
    let n = Object.getOwnPropertySymbols(e);
    t &&
      (n = n.filter(function(t) {
        return Object.getOwnPropertyDescriptor(e, t).enumerable;
      })),
      r.push.apply(r, n);
  }
  return r;
}
function r(r) {
  for (let n = 1; n < arguments.length; n++) {
    var s = null != arguments[n] ? arguments[n] : {};
    n % 2
      ? t(Object(s), !0).forEach(function(t) {
          e(r, t, s[t]);
        })
      : Object.getOwnPropertyDescriptors
      ? Object.defineProperties(r, Object.getOwnPropertyDescriptors(s))
      : t(Object(s)).forEach(function(e) {
          Object.defineProperty(r, e, Object.getOwnPropertyDescriptor(s, e));
        });
  }
  return r;
}
const n = new Set(["—"]),
  s = new Set(["­"]),
  i = new Set([
    " ",
    " ",
    " ",
    " ",
    " ",
    " ",
    " ",
    " ",
    " ",
    " ",
    " ",
    " ",
    " ",
    "　",
    "\t",
    "​",
    "\u2028",
    "\u2029"
  ]),
  o = new Set([
    "֊",
    "‐",
    "‒",
    "–",
    "־",
    "་",
    "፡",
    "៘",
    "៚",
    "‧",
    "|",
    "᛫",
    "᛬",
    "᛭",
    "⁖",
    "⁘",
    "⁙",
    "⁚",
    "⁛",
    "⁝",
    "⁞",
    "⸙",
    "⸪",
    "⸫",
    "⸬",
    "⸭",
    "⸰",
    "တ0",
    "တ1",
    "တ2",
    "္F",
    "ွ0",
    "႑F",
    "ቇ0"
  ]),
  a = new Set(["´", "´"]),
  c = new Set(["\n"]);
function l(e, t) {
  t || (t = {});
  const r = Number.parseInt(S(t, "base-font-size", 16), 10),
    n = Number.parseFloat(e),
    s = e.replace(n, "");
  switch (s) {
    case "rem":
    case "em":
      return n * r;
    case "pt":
      return n / (96 / 72);
    case "px":
      return n;
  }
  throw new Error("The unit " + s + " is not supported");
}
function u(e, t) {
  const r = new Set(["inherit", "initial", "unset", "normal"]);
  let n = 0;
  e && !r.has(e) && (n = l(e));
  let s = 0;
  return (
    t && !r.has(t) && (s = l(t)),
    e =>
      (e
        .trim()
        .replace(/\s+/gi, " ")
        .split(" ").length -
        1) *
        n +
      e.length * s
  );
}
function p(e, t) {
  const r = [],
    n = S(t, "font-weight", e.getPropertyValue("font-weight")) || "400";
  [
    "normal",
    "bold",
    "bolder",
    "lighter",
    "100",
    "200",
    "300",
    "400",
    "500",
    "600",
    "700",
    "800",
    "900"
  ].includes(n.toString()) && r.push(n);
  const s = S(t, "font-style", e.getPropertyValue("font-style"));
  ["normal", "italic", "oblique"].includes(s) && r.push(s);
  const i = S(t, "font-variant", e.getPropertyValue("font-variant"));
  ["normal", "small-caps"].includes(i) && r.push(i);
  const o = l(S(t, "font-size", e.getPropertyValue("font-size")) || "16px");
  r.push(o + "px");
  const a =
    S(t, "font-family", e.getPropertyValue("font-family")) ||
    "Helvetica, Arial, sans-serif";
  return r.push(a), r.join(" ");
}
function h(e) {
  return e && "function" == typeof e.getPropertyValue;
}
function f(e) {
  return (
    d(e) &&
    e.style &&
    "undefined" != typeof window &&
    "function" == typeof window.getComputedStyle
  );
}
function d(e) {
  return "object" == typeof HTMLElement
    ? e instanceof HTMLElement
    : Boolean(
        e &&
          "object" == typeof e &&
          null !== e &&
          1 === e.nodeType &&
          "string" == typeof e.nodeName
      );
}
function g(e) {
  return "object" == typeof e && null !== e && !Array.isArray(e);
}
function w(e, t) {
  const n = r({}, t || {}),
    { style: s } = n;
  return (
    t || (t = {}),
    h(s)
      ? s
      : f(e)
      ? window.getComputedStyle(e, S(t, "pseudoElt", null))
      : { getPropertyValue: e => S(t, e) }
  );
}
function y(e, t) {
  switch (t) {
    case "pre":
    case "pre-wrap":
      return e;
    case "pre-line":
      return (e || "").replace(/\s+/gm, " ").trim();
    default:
      return (e || "")
        .replace(/[\r\n]/gm, " ")
        .replace(/\s+/gm, " ")
        .trim();
  }
}
function m(e, t) {
  switch (t.getPropertyValue("text-transform")) {
    case "uppercase":
      return e.toUpperCase();
    case "lowercase":
      return e.toLowerCase();
    default:
      return e;
  }
}
function b(e) {
  return (
    (e = (e || "")
      .replace(/<wbr>/gi, "​")
      .replace(/<br\s*\/?>/gi, "\n")
      .replace(/&shy;/gi, "­")
      .replace(/&mdash;/gi, "—")),
    /&#(\d+)(;?)|&#[xX]([a-fA-F\d]+)(;?)|&([\da-zA-Z]+);/g.test(e) &&
      console &&
      console.error(
        "text-metrics: Found encoded htmlenties. You may want to use https://mths.be/he to decode your text first."
      ),
    e
  );
}
function x(e) {
  return (e && (e.textContent || e.textContent)) || "";
}
function S(e, t, r) {
  return (e && void 0 !== e[t] && e[t]) || r;
}
function P(e) {
  const t = {};
  return (
    Object.keys(e || {}).forEach(r => {
      const n = r.replace(/([A-Z])/g, e => "-" + e.toLowerCase());
      t[n] = e[r];
    }),
    t
  );
}
function v(e) {
  try {
    const t = document.createElement("canvas").getContext("2d"),
      r = window.devicePixelRatio || 1,
      n =
        t.webkitBackingStorePixelRatio ||
        t.mozBackingStorePixelRatio ||
        t.msBackingStorePixelRatio ||
        t.oBackingStorePixelRatio ||
        t.backingStorePixelRatio ||
        1;
    return (t.font = e), t.setTransform(r / n, 0, 0, r / n, 0, 0), t;
  } catch (e) {
    throw new Error("Canvas support required" + e.message);
  }
}
function B(e) {
  return (
    (n.has(e) ? "B2" : i.has(e) && "BAI") ||
    (s.has(e) && "SHY") ||
    (o.has(e) && "BA") ||
    (a.has(e) && "BB") ||
    (c.has(e) && "BK")
  );
}
function k({ ctx: e, text: t, max: r, wordSpacing: n, letterSpacing: s }) {
  const o = u(n, s),
    a = [],
    c = [],
    l = [];
  let p = "",
    h = "";
  if (!t) return [];
  for (const e of t) {
    const t = B(e);
    ("" === h && "BAI" === t) ||
      (t ? (l.push({ chr: e, type: t }), c.push(h), (h = "")) : (h += e));
  }
  h && c.push(h);
  for (let t = 0; t < c.length; t++) {
    if (0 === t) {
      p = c[t];
      continue;
    }
    const n = c[t];
    if (i.has(c[t - 1]) && i.has(c[t])) continue;
    const s = l[t - 1],
      u = "SHY" === s.type ? "" : s.chr;
    if ("BK" === s.type) {
      a.push(p), (p = n);
      continue;
    }
    const h = e.measureText(p + u + n).width + o(p + u + n);
    if (Math.round(h, 10) <= r) p += u + n;
    else
      switch (s.type) {
        case "SHY":
          a.push(p + "-"), (p = n);
          break;
        case "BA":
          a.push(p + u), (p = n);
          break;
        case "BAI":
          a.push(p), (p = n);
          break;
        case "BB":
          a.push(p), (p = u + n);
          break;
        case "B2":
          Number.parseInt(e.measureText(p + u).width + o(p + u), 10) <= r
            ? (a.push(p + u), (p = n))
            : Number.parseInt(e.measureText(u + n).width + o(u + n), 10) <= r
            ? (a.push(p), (p = u + n))
            : (a.push(p), a.push(u), (p = n));
          break;
        default:
          throw new Error("Undefoined break");
      }
  }
  return 0 !== [...p].length && a.push(p), a;
}
function O({ ctx: e, text: t, max: r, wordSpacing: n, letterSpacing: s }) {
  const o = u(n, s),
    a = [];
  let c = "",
    l = 0;
  if (!t) return [];
  for (const n of t) {
    const s = B(n);
    if ("BK" === s) {
      a.push(c), (c = "");
      continue;
    }
    const u = c.length;
    if (i.has(n) && (0 === u || i.has(c[u - 1]))) continue;
    let p = e.measureText(c + n).width + o(c + n),
      h = Math.ceil(p);
    if ("SHY" === s) {
      const r = t[l + 1] || "";
      (p = e.measureText(c + n + r).width + o(c + n + r)), (h = Math.ceil(p));
    }
    if (h > r && 0 !== [...c].length)
      switch (s) {
        case "SHY":
          a.push(c + "-"), (c = "");
          break;
        case "BA":
          a.push(c + n), (c = "");
          break;
        case "BAI":
          a.push(c), (c = "");
          break;
        default:
          a.push(c), (c = n);
      }
    else "­" !== n && (c += n);
    l++;
  }
  return 0 !== [...c].length && a.push(c), a;
}
const A = {
  __proto__: null,
  addWordAndLetterSpacing: u,
  getFont: p,
  isCSSStyleDeclaration: h,
  canGetComputedStyle: f,
  isElement: d,
  isObject: g,
  getStyle: w,
  normalizeWhitespace: y,
  getStyledText: m,
  prepareText: b,
  getText: x,
  prop: S,
  normalizeOptions: P,
  getContext2d: v,
  computeLinesDefault: k,
  computeLinesBreakAll: O
};
class j {
  constructor(e, t = {}) {
    !d(e) && g(e)
      ? ((this.el = void 0), (this.overwrites = P(e)))
      : ((this.el = e), (this.overwrites = P(t))),
      (this.style = w(this.el, this.overwrites)),
      (this.font = S(t, "font", null) || p(this.style, this.overwrites));
  }
  padding() {
    return this.el
      ? Number.parseInt(this.style.paddingLeft || 0, 10) +
          Number.parseInt(this.style.paddingRight || 0, 10)
      : 0;
  }
  parseArgs(e, t = {}, n = {}) {
    "object" == typeof e && e && ((n = t), (t = e || {}), (e = void 0));
    const s = r(r({}, this.overwrites), P(n)),
      i = S(s, "white-space") || this.style.getPropertyValue("white-space");
    return (
      t || (t = {}),
      n || (t = {}),
      {
        text: (e = !e && this.el ? y(x(this.el), i) : b(y(e))),
        options: t,
        overwrites: n,
        styles: s
      }
    );
  }
  width() {
    const { text: e, options: t, overwrites: r, styles: n } = this.parseArgs(
      ...[].slice.call(arguments)
    );
    if (!e) return 0;
    const s = p(this.style, n),
      i =
        S(n, "letter-spacing") || this.style.getPropertyValue("letter-spacing"),
      o = u(
        S(n, "word-spacing") || this.style.getPropertyValue("word-spacing"),
        i
      ),
      a = v(s),
      c = m(e, this.style);
    return t.multiline
      ? this.lines(c, t, r).reduce((e, t) => {
          const r = a.measureText(t).width + o(t);
          return Math.max(e, r);
        }, 0)
      : a.measureText(c).width + o(c);
  }
  height() {
    const { text: e, options: t, styles: r } = this.parseArgs(
        ...[].slice.call(arguments)
      ),
      n = Number.parseFloat(
        S(r, "line-height") || this.style.getPropertyValue("line-height")
      );
    return Math.ceil(this.lines(e, t, r).length * n || 0);
  }
  lines() {
    const { text: e, options: t, overwrites: r, styles: n } = this.parseArgs(
        ...[].slice.call(arguments)
      ),
      s = p(this.style, n);
    let i =
      Number.parseInt(S(t, "width") || S(r, "width"), 10) ||
      S(this.el, "offsetWidth", 0) ||
      Number.parseInt(S(n, "width", 0), 10) ||
      Number.parseInt(this.style.width, 10);
    i -= this.padding();
    const o = S(n, "word-break") || this.style.getPropertyValue("word-break"),
      a =
        S(n, "letter-spacing") || this.style.getPropertyValue("letter-spacing"),
      c = S(n, "word-spacing") || this.style.getPropertyValue("word-spacing"),
      l = v(s),
      u = m(e, this.style);
    return "break-all" === o
      ? O({ ctx: l, text: u, max: i, wordSpacing: c, letterSpacing: a })
      : k({ ctx: l, text: u, max: i, wordSpacing: c, letterSpacing: a });
  }
  maxFontSize() {
    const { text: e, options: t, overwrites: n, styles: s } = this.parseArgs(
        ...[].slice.call(arguments)
      ),
      i = n =>
        Math.ceil(this.width(e, t, r(r({}, s), {}, { "font-size": n + "px" })));
    let o =
      Number.parseInt(S(t, "width") || S(n, "width"), 10) ||
      S(this.el, "offsetWidth", 0) ||
      Number.parseInt(S(s, "width", 0), 10) ||
      Number.parseInt(this.style.width, 10);
    o -= this.padding();
    let a = Math.floor(o / 2),
      c = i(a);
    if (((a = Math.floor((a / c) * o)), (c = i(a)), Math.ceil(c) === o))
      return a ? a + "px" : void 0;
    const l = c > o && a > 0;
    for (; c > o && a > 0; ) (a -= 1), (c = i(a));
    if (!l)
      for (; c < o; ) {
        if (((c = i(a + 1)), c > o)) return a ? a + "px" : void 0;
        a += 1;
      }
    return a ? a + "px" : void 0;
  }
}
const I = r({}, A);
(exports.init = (e, t) => new j(e, t)), (exports.utils = I);
