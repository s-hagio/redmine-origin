import * as React from 'react';
import BaseGridLayout, { WidthProvider } from 'react-grid-layout';
import { Layout } from 'react-grid-layout/lib/utils';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import throttle from 'lodash/throttle';

import {
  Panel,
  Json,
  PanelResponse,
  PanelRootComponentProps,
} from '../../interfaces/interface';
import panelDefinitions from '../../panelDefinitions';
import PanelLayout, { TitlebarButton } from '../atoms/PanelLayout';
import PanelSettingsButton from '../atoms/PanelSettingsButton';

const ReactiveGridLayout = WidthProvider(BaseGridLayout);

const dragHandleSelector = '.panel-drag-handle';
const dragCancelSelector = '.popup-content, .popup-arrow, .popup-overlay';

const GridLayout = styled(ReactiveGridLayout)`
  background: #f6f6f6;

  .react-grid-item {
    background: #fff;
    border: gray solid 2px;
    border-radius: 5px;
    box-shadow: darkgray 1px 1px 2px;
  }

  .react-draggable {
    ${dragHandleSelector} {
      cursor: move;

      /* Disable for settings popup */
      ${dragCancelSelector} {
        cursor: default;
      }
    }
  }
`;

const RemovePanelButton = styled(TitlebarButton)`
  display: inline;
  font-size: 24px;
`;

// パネルのデフォルトのボタンを表示するコンポーネントを生成する
const buildPanelRoot = ({
  removePanel,
  updateSettings,
  editLayoutMode,
}): React.FC<PanelRootComponentProps> => {
  const PanelRootComponent: React.FC<PanelRootComponentProps> = ({
    titlebarButtons,
    children,
    renderPanelSettings,
    ...otherAttributes
  }: PanelRootComponentProps) => (
    <PanelLayout
      titlebarButtons={
        <>
          {titlebarButtons}
          {editLayoutMode && (
            <>
              {renderPanelSettings && (
                <PanelSettingsButton
                  renderForm={(closeSettings): React.ReactElement =>
                    renderPanelSettings({ closeSettings, updateSettings })
                  }
                />
              )}
              <RemovePanelButton onClick={removePanel}>
                &times;
              </RemovePanelButton>
            </>
          )}
        </>
      }
      {...otherAttributes}
    >
      {children}
    </PanelLayout>
  );

  return PanelRootComponent;
};

interface ProjectReportPanelContainerProps {
  className?: string;
  panels: Array<PanelResponse>;
  editLayoutMode: boolean;
  updatePanelSettings: (targetPanel: Panel, settings: Json) => void;
  removePanel: (targetPanel: Panel) => void;
  onLayoutChange: (currentLayout: Layout) => void;
}

const ProjectReportPanelContainer: React.FC<ProjectReportPanelContainerProps> = React.memo(
  ({
    className,
    panels,
    removePanel,
    onLayoutChange,
    updatePanelSettings,
    editLayoutMode,
  }: ProjectReportPanelContainerProps) => {
    const { t } = useTranslation();
    const refGridLayout = React.useRef();
    const layout = React.useMemo(
      () =>
        panels.map((panel: PanelResponse) => ({
          i: panel.id.toString(),
          ...panel.position,
        })),
      [panels]
    );

    const renderPanel = React.useCallback(
      (panel: PanelResponse): React.ReactNode => {
        const updateSettings = (settings: Json): void =>
          updatePanelSettings(panel, settings);

        const PanelRootComponent = buildPanelRoot({
          removePanel: () => removePanel(panel),
          updateSettings,
          editLayoutMode,
        });

        if ('error' in panel) {
          return (
            <PanelRootComponent title={t('label_error')}>
              {t('message_panel_error')}
            </PanelRootComponent>
          );
        }

        if ('error' in panel.content) {
          return (
            <PanelRootComponent title={t('label_error')}>
              {panel.content.error}
            </PanelRootComponent>
          );
        }

        const PanelComponent = panelDefinitions[panel.type].component;

        return (
          <PanelComponent
            PanelRootComponent={PanelRootComponent}
            panel={panel}
            editLayoutMode={editLayoutMode}
            updateSettings={updateSettings}
          />
        );
      },
      [editLayoutMode, removePanel, t, updatePanelSettings]
    );

    const handleResize = React.useCallback(
      (context = '') =>
        throttle((): void => {
          const { current } = refGridLayout;
          switch (context) {
            case 'start':
              current.style.minHeight = '';
              break;
            case 'stop':
              current.style.minHeight = '0px';
              break;
            default: {
              const regex = /^[0-9]*/;
              const currentHeight = current.style.height.match(regex).shift();
              const currentMinHeight =
                current.style.minHeight.match(regex).shift() || 0;
              if (Number(currentHeight) > Number(currentMinHeight)) {
                current.style.minHeight = current.style.height;
              }
            }
          }
        }, 1000 / 60),
      [refGridLayout]
    );

    const renderedPanels = React.useMemo(
      () =>
        panels.map((panel: PanelResponse) => (
          <div key={panel.id}>{renderPanel(panel)}</div>
        )),
      [panels, renderPanel]
    );

    return (
      <GridLayout
        className={className}
        layout={layout}
        cols={30}
        rowHeight={60}
        compactType={null}
        onLayoutChange={onLayoutChange}
        useCSSTransforms={false}
        preventCollision
        isDraggable={editLayoutMode}
        isResizable={editLayoutMode}
        draggableHandle={dragHandleSelector}
        draggableCancel={dragCancelSelector}
        onResize={handleResize()}
        onResizeStart={handleResize('start')}
        onResizeStop={handleResize('stop')}
        innerRef={refGridLayout}
      >
        {renderedPanels}
      </GridLayout>
    );
  }
);

export default ProjectReportPanelContainer;
