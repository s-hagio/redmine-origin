import * as React from 'react';
import { Layout, LayoutItem } from 'react-grid-layout/lib/utils';
import styled from 'styled-components';
import { toast, ToastContainer, Slide } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import find from 'lodash/find';

import {
  fetchPanels,
  createPanel,
  updatePanel,
  deletePanel,
  createLayout,
  copyLayout,
  deleteLayout,
  createSharedLayout,
} from '../../api';
import {
  Panel,
  PanelPosition,
  PanelType,
  Json,
  Layout as PanelLayout,
} from '../../interfaces/interface';
import PageVariables from '../../pageVariables';
import panelDefinitions from '../../panelDefinitions';
import FlexBox from '../atoms/FlexBox';
import IconButton from '../atoms/IconButton';
import Sticky from '../atoms/Sticky';

import LayoutName from './LayoutName';
import PanelSelector from './PanelSelector';
import ProjectReportPanelContainer from './ProjectReportPanelContainer';

const StickyHeader = styled(Sticky)`
  background-color: #fff;
  top: 0;
  z-index: 5;
  margin-top: -6px;
  padding: 6px 0;
  white-space: nowrap;

  .contextual {
    margin: 0;
  }
`;

const ReportedOn = styled.div`
  margin: 0 10px;
  font-size: 14px;
  font-weight: bold;
`;

const PanelContainer = styled(ProjectReportPanelContainer)`
  padding-top: 15px;
  transition: height 200ms ease, min-height 200ms ease !important;
`;

const Toasts = styled(ToastContainer)`
  && {
    width: 60%;
    margin-left: -30%;
  }

  .Toastify__toast {
    border-radius: 8px;
    min-height: 0px;
  }

  button {
    padding-left: 5px;
  }
`;

const isSamePosition = (
  oldPosition: PanelPosition,
  newPosition: PanelPosition
): boolean =>
  oldPosition.w === newPosition.w &&
  oldPosition.h === newPosition.h &&
  oldPosition.x === newPosition.x &&
  oldPosition.y === newPosition.y;

const loadLayout = (options: {
  id?: number;
  message?: string;
  editMode?: boolean;
  shared?: boolean;
}): void => {
  // Defined in project_reports/show.html.erb since non-React frontend needs it too
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  LycheeProjectReport.onLayoutCreated(options);
};

const defaultPositionFor = (type: PanelType): PanelPosition =>
  panelDefinitions[type].defaultPosition;

const ProjectReport: React.FC = () => {
  const { t } = useTranslation();
  const contextualEl = React.createRef<HTMLDivElement>();
  const reportNameEl = React.createRef<HTMLDivElement>();

  const [panels, setPanels] = React.useState<Panel[]>([]);
  const [editLayoutMode, setEditLayoutMode] = React.useState<boolean>(
    PageVariables.permissions.editLayout && PageVariables.startInEditMode
  );
  const [initialized, setInitialized] = React.useState<boolean>(false);

  React.useEffect(() => {
    fetchPanels()
      .then((response) => response.json())
      .then(setPanels);
  }, []);
  React.useEffect(() => {
    const redmineContextualEl = document.querySelector(
      '#content > .contextual'
    );
    if (contextualEl.current && redmineContextualEl) {
      contextualEl.current.insertAdjacentElement(
        'afterend',
        redmineContextualEl
      );
    }
  }, [contextualEl]);
  React.useEffect(() => {
    const redmineReportNameEl = document.querySelector(
      '#content > .project-report-name'
    );
    if (reportNameEl.current && redmineReportNameEl) {
      reportNameEl.current.insertAdjacentElement(
        'afterend',
        redmineReportNameEl
      );
    }
  }, [reportNameEl]);

  const setPanel = React.useCallback(
    (panel: Panel) => {
      const updatedPanels = panels.filter((p) => p.id !== panel.id);
      updatedPanels.push(panel);
      setPanels(updatedPanels);
    },
    [panels]
  );

  const addPanel = React.useCallback(
    (type: PanelType): void => {
      const layoutHeight = Math.max(
        0,
        ...panels.map(({ position: { h, y } }) => h + y)
      );
      const position = { ...defaultPositionFor(type), y: layoutHeight };
      createPanel(type, position)
        .then((response) => response.json())
        .then(setPanel);
    },
    [panels, setPanel]
  );

  const handleLayoutChange = React.useCallback(
    (currentLayout: Layout): void => {
      if (!editLayoutMode) return;
      (async (): Promise<void> => {
        let changed = false;
        const newLayout = await Promise.all(
          currentLayout.map(
            async (layout: LayoutItem): Promise<Panel> => {
              const oldPanel = find(panels, { id: Number(layout.i) });
              const oldPosition = oldPanel.position;
              const newPosition = {
                w: layout.w,
                h: layout.h,
                x: layout.x,
                y: layout.y,
              };

              if (isSamePosition(oldPosition, newPosition)) {
                return oldPanel;
              }

              const response = await updatePanel(oldPanel.id, {
                position: newPosition,
              });
              changed = true;
              return response.json();
            }
          )
        );
        if (!initialized && changed) {
          toast.info(t('message_panel_repositioned'));
        }
        if (!initialized) {
          setInitialized(true);
        }

        if (changed) {
          setPanels(newLayout);
        }
      })();
    },
    [initialized, panels, t, editLayoutMode]
  );

  const removePanel = React.useCallback(
    (targetPanel: Panel): void => {
      if (window.confirm(t('confirm_remove_panel'))) {
        panels.forEach((panel) => {
          if (panel.id !== targetPanel.id) {
            return;
          }
          deletePanel(panel).then(() => {
            setPanels(panels.filter((panel) => panel.id !== targetPanel.id));
          });
        });
      }
    },
    [panels, t]
  );

  const updateSettings = React.useCallback(
    (targetPanel: Panel, settings: Json): void => {
      updatePanel(targetPanel.id, { settings })
        .then((response) => response.json())
        .then(setPanel);
    },
    [setPanel]
  );

  const [currentLayout, setCurrentLayout] = React.useState<PanelLayout>(
    PageVariables.availableLayouts.find(
      (l) =>
        l.id === PageVariables.currentLayoutId &&
        l.shared === PageVariables.currentLayoutShared
    )
  );

  const addLayout = React.useCallback(() => {
    createLayout()
      .then((response) => response.json())
      .then(({ id }: PanelLayout) => {
        loadLayout({ id, editMode: true });
      });
  }, []);

  const handleCopyLayout = React.useCallback(() => {
    const key = PageVariables.currentLayoutShared
      ? 'projectReportSharedLayoutId'
      : 'projectReportLayoutId';
    copyLayout({ [key]: PageVariables.currentLayoutId })
      .then((response) => response.json())
      .then(({ id }: PanelLayout) => {
        loadLayout({ id, message: 'copy_success', editMode: true });
      });
  }, []);

  const removeLayout = React.useCallback(() => {
    const confirmMessage = PageVariables.currentLayoutShared
      ? t('confirm_remove_shared_report')
      : t('confirm_remove_report');
    if (window.confirm(confirmMessage)) {
      deleteLayout().then((response) => {
        const message =
          response.status === 204 ? 'delete_success' : 'delete_failure';
        const otherLayout = PageVariables.availableLayouts.find(
          (l) =>
            l.id !== PageVariables.currentLayoutId ||
            l.shared !== PageVariables.currentLayoutShared
        );
        if (otherLayout) {
          loadLayout({ id: otherLayout.id, message });
        } else {
          loadLayout({ message });
        }
      });
    }
  }, [t]);

  const addSharedLayout = React.useCallback(() => {
    createSharedLayout()
      .then((response) => response.json())
      .then((response) => {
        loadLayout({
          id: response.id,
          message: 'convert_success',
          editMode: true,
          shared: true,
        });
      });
  }, []);

  return (
    <>
      <StickyHeader>
        <FlexBox
          id="project-report-header"
          align="center"
          justify="space-between"
        >
          <FlexBox
            align="center"
            justify="flex-start"
            flex-wrap="nowrap"
            flex-grow="1"
            flex-basis="0%"
          >
            <LayoutName
              editing={editLayoutMode}
              layout={currentLayout}
              onChange={setCurrentLayout}
            />
            {editLayoutMode ? (
              <>
                <PanelSelector addPanel={addPanel} />
                <IconButton
                  icon="checked"
                  onClick={(): void => setEditLayoutMode(false)}
                >
                  {t('button_finish_editing')}
                </IconButton>
                {PageVariables.permissions.createSharedLayout && (
                  <IconButton icon="move" onClick={addSharedLayout}>
                    {t('button_convert_to_shared_report')}
                  </IconButton>
                )}
              </>
            ) : (
              <>
                {PageVariables.reportedOn && (
                  <ReportedOn>
                    {t('field_reporting_label_cwday')}{' '}
                    {PageVariables.reportedOn}
                  </ReportedOn>
                )}
                {PageVariables.permissions.editLayout && (
                  <IconButton
                    icon="settings"
                    onClick={(): void => setEditLayoutMode(true)}
                  >
                    {t('button_edit_mode')}
                  </IconButton>
                )}
                {PageVariables.permissions.editLayout && (
                  <IconButton icon="del" onClick={removeLayout}>
                    {t('button_delete')}
                  </IconButton>
                )}
                {PageVariables.permissions.createLayout && (
                  <IconButton icon="copy" onClick={handleCopyLayout}>
                    {t('button_copy')}
                  </IconButton>
                )}
                {PageVariables.permissions.createLayout && (
                  <IconButton icon="add" onClick={addLayout}>
                    {t('button_new_layout')}
                  </IconButton>
                )}
                {!PageVariables.reportedOn && PageVariables.nextReportDate && (
                  <ReportedOn>
                    {t('field_reporting_label_next_date')}{' '}
                    {PageVariables.nextReportDate}
                  </ReportedOn>
                )}
              </>
            )}
            <FlexBox align="left" flex-wrap="nowrap" ref={reportNameEl}>
              <div style={{ width: 10 }} />
            </FlexBox>
          </FlexBox>
          <FlexBox align="center" justify="flex-end" flex-wrap="nowrap">
            <FlexBox align="center" flex-wrap="nowrap" ref={contextualEl}>
              <div style={{ width: 10 }} />
            </FlexBox>
          </FlexBox>
        </FlexBox>
      </StickyHeader>
      <PanelContainer
        panels={panels}
        editLayoutMode={editLayoutMode}
        onLayoutChange={handleLayoutChange}
        removePanel={removePanel}
        updatePanelSettings={updateSettings}
      />
      <Toasts position="bottom-center" transition={Slide} hideProgressBar />
    </>
  );
};

export default ProjectReport;
