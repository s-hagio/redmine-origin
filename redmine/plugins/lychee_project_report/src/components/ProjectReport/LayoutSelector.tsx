import * as React from "react";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import ReactSelect, { StylesConfig, ValueType } from "react-select";

import { Layout } from "../../interfaces/interface";
import PageVariables from "../../pageVariables";

const Select = styled(ReactSelect)`
  min-width: 240px;
`;

const layoutsEqual = (l1: Layout, l2: Layout): boolean =>
  l1.shared === l2.shared && l1.id === l2.id;

const goToLayout = (layout: Layout): void => {
  const searchParams = new URLSearchParams();
  searchParams.set("layout_id", layout.params.layout_id.toString());
  if (layout.params.shared) {
    searchParams.set("shared", "true");
  }
  window.location.search = searchParams.toString();
};

interface LayoutOption {
  label: string;
  layout?: Layout;
  selected?: boolean;
  separator?: boolean;
}

const toOption = (layout: Layout): LayoutOption => ({
  label: layout.name,
  layout
});

const underlineColor = PageVariables.currentLayoutShared
  ? "#ccffcc"
  : "#ffffcc";

const styles = (nameStyle: React.CSSProperties): StylesConfig => ({
  singleValue: (styles): React.CSSProperties => ({
    ...styles,
    ...nameStyle,
    background: `linear-gradient(transparent 60%, ${underlineColor} 0%)`
  }),
  option: (styles, { data }): React.CSSProperties => ({
    ...styles,
    fontSize: nameStyle.fontSize,
    ...(data.separator
      ? {
          fontWeight: "bold",
          color: "rgb(78, 115, 190)",
          backgroundColor: "rgb(253, 241, 208)",
          textAlign: "center"
        }
      : {})
  })
});

interface LayoutSelectorProps {
  className?: string;
  nameStyle?: React.CSSProperties;
  currentLayout: Layout;
}

const LayoutSelector: React.FC<LayoutSelectorProps> = ({
  className,
  nameStyle,
  currentLayout
}: LayoutSelectorProps) => {
  const { t } = useTranslation();

  const handleChange = React.useCallback(
    (value: ValueType<LayoutOption>): void => {
      if ("layout" in value) {
        goToLayout(value.layout);
      }
    },
    []
  );

  const displayedLayouts = React.useMemo(
    () =>
      PageVariables.availableLayouts.map(l =>
        layoutsEqual(l, currentLayout) ? currentLayout : l
      ),
    [currentLayout]
  );

  const options = [
    { label: `ーーー${t("label_individual_report")}ーーー`, separator: true },
    ...displayedLayouts.filter(l => !l.shared).map(toOption),
    { label: `ーーー${t("label_shared_report")}ーーー`, separator: true },
    ...displayedLayouts.filter(l => l.shared).map(toOption)
  ];

  return (
    <Select
      id="layout-selector"
      className={className}
      styles={styles(nameStyle)}
      options={options}
      value={toOption(currentLayout)}
      isOptionDisabled={(option: LayoutOption): boolean => option.separator}
      isOptionSelected={(): boolean => false}
      onChange={handleChange}
      isSearchable={false}
    />
  );
};

export default LayoutSelector;
