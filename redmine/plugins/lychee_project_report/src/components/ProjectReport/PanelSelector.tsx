import * as React from "react";
import { toast } from "react-toastify";
import { useTranslation } from "react-i18next";

import { PanelType } from "../../interfaces/interface";

interface PanelSelectorProps {
  addPanel: (type: PanelType) => void;
}

const panelTypes: PanelType[] = JSON.parse(
  document.getElementById("project-report").dataset.allowedPanels
);

const PanelSelector: React.FC<PanelSelectorProps> = ({
  addPanel
}: PanelSelectorProps) => {
  const { t } = useTranslation();
  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const panelType = panelTypes.find(type => type === event.target.value);
    const screenHeight = document.documentElement.scrollHeight;

    if (!panelType) return;

    addPanel(panelType);
    toast(t("add_panel_alert"));
    setTimeout(() => {
      window.scrollTo({
        top: screenHeight,
        behavior: "smooth"
      });
    }, 500);
  };

  return (
    <>
      <select id="panel-list" value={""} onChange={handleChange}>
        <option value="">ーーー{t("label_add_panel")}ーーー</option>
        {panelTypes.map(panelType => (
          <option key={panelType} value={panelType}>
            {t(`panel_names.${panelType}`)}
          </option>
        ))}
      </select>
    </>
  );
};

export default PanelSelector;
