import * as React from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import { PanelProps } from '../interfaces/interface';

const Table = styled.table`
  width: 100%;

  th,
  td {
    border-bottom: 2px solid;
    border-color: #e4e4e4;
    text-align: left;
    white-space: pre-wrap;
  }

  tr:nth-child(2n + 2) {
    background-color: #fcfcfc;
  }
`;

interface OverviewContentProps {
  manager: string | null;
  subManager: string | null;
  projectName: string;
  projectStartDate: string;
  projectEndDate: string;
  reportedOn: string;
  pending: boolean;
  reportingCwday: string;
  lastReportedOn: string;
  versionName: string | null;
  versionStartDate: string;
  versionEndDate: string;
}

const OverviewPanel: React.FC<PanelProps<OverviewContentProps>> = ({
  panel,
  PanelRootComponent,
}: PanelProps<OverviewContentProps>) => {
  const {
    projectName,
    projectStartDate,
    projectEndDate,
    reportedOn,
    pending,
    reportingCwday,
    lastReportedOn,
    versionName,
    versionStartDate,
    versionEndDate,
  } = panel.content;
  const { t } = useTranslation();

  return (
    <PanelRootComponent
      title={t('label_overview')}
      data-test="overview-panel" // For feature spec
    >
      <Table>
        <tbody>
          <tr>
            <th>{t('field_project')}</th>
            <td>
              {projectName} ({projectStartDate} 〜 {projectEndDate})
            </td>
          </tr>
          {versionName && (
            <tr>
              <th>{t('field_version')}</th>
              <td>
                {versionName} ({versionStartDate} ~ {versionEndDate})
              </td>
            </tr>
          )}{' '}
          {pending ? (
            <tr>
              <th>{t('field_reporting_label_cwday')}</th>
              <td>{reportingCwday}</td>
            </tr>
          ) : (
            <tr>
              <th>{t('field_reporting_label_date')}</th>
              <td>{reportedOn}</td>
            </tr>
          )}
          <tr>
            <th>{t('field_reporting_label_last')}</th>
            <td>{lastReportedOn}</td>
          </tr>
        </tbody>
      </Table>
    </PanelRootComponent>
  );
};

export default OverviewPanel;
