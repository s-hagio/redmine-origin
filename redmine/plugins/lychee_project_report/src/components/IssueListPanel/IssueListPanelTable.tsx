import * as React from 'react';
import {
  useTable,
  useBlockLayout,
  useResizeColumns,
  useSortBy,
} from 'react-table';
import styled from 'styled-components';
import throttle from 'lodash/throttle';
import maxBy from 'lodash/maxBy';
import PerfectScrollbar from 'perfect-scrollbar';
import 'perfect-scrollbar/css/perfect-scrollbar.css';

import PageVariables from '../../pageVariables';

const Table = styled.div`
  display: inline-flex;
  flex-direction: column;
  table-layout: fixed;
  border-bottom: 2px solid #ccc;
  width: auto;

  .tbody {
    position: relative;
    flex-grow: 1;
  }

  .tr:nth-child(even) {
    background-color: rgba(0, 0, 0, 0.03);
  }

  .th,
  .td {
    border: 1px solid #ccc;
    font-weight: bold;
    text-align: center;
    padding: 4px 16px;

    & > span {
      padding-left: 16px;
      padding-right: 16px;
      margin-left: -16px;
      margin-right: -16px;
    }
  }

  .th {
    background-color: #fff;
    border-top: 0;
    border-left: ${({ disableResizing }): string =>
      disableResizing ? '' : '0'};
    border-right: ${({ disableResizing }): string =>
      disableResizing ? '' : '0'};
    white-space: nowrap;

    &:first-child {
      border-left: 0;
    }

    &:last-child {
      border-right: 0;
    }

    [sort] {
      background-position: calc(100% - 5px) 50% !important;
      background-size: 8px !important;

      &[sort='asc'] {
        background: url(${PageVariables.urls.root}/images/arrow_up.png)
          no-repeat;
      }

      &[sort='desc'] {
        background: url(${PageVariables.urls.root}/images/arrow_down.png)
          no-repeat;
      }
    }

    [role='separator'] {
      position: absolute;
      top: 0;
      right: -2px;
      bottom: 0;
      border-left: 1px solid;
      border-right: 1px solid;
      width: 2px;
      user-select: none;
      z-index: 1;
    }
  }

  .td {
    white-space: normal;
    word-break: break-all;

    &:first-child {
      border-left: 0;
    }

    &:last-child {
      border-right: 0;
    }

    &[nowrap='true'] {
      white-space: nowrap;
    }
  }

  @media print {
    display: block !important;
  }
`;

const stopPropagation = (event): void => event.stopPropagation();

const usePerfectScroll = (ref, options): PerfectScrollbar => {
  const [scroll, setScroll] = React.useState(null);
  React.useEffect(() => {
    if (ref.current) {
      if (scroll) {
        scroll.destroy();
      }
      const ps = new PerfectScrollbar(ref.current, options);
      setScroll(ps);
    }
  }, [ref, options]); // eslint-disable-line react-hooks/exhaustive-deps
  React.useEffect(
    () => (): void => {
      if (scroll) {
        scroll.destroy();
      }
    },
    [scroll]
  );

  return scroll;
};
const PERFECT_SCROLL_OPTIONS = { suppressScrollX: true };

type Props = {
  columns: Array<any>;
  data: Array<any>;
  disableResizing?: boolean;
  onResizeColumn?: (name: string, width: number) => void;
  onSortBy?: (name: string, desc: boolean) => void;
};

const IssueListPanelTable: React.FC<Props> = (props: Props) => {
  const {
    columns,
    className,
    data,
    disableResizing = false,
    onResizeColumn,
    onSortBy,
  } = props;
  const [minWidths, setMinWidths] = React.useState({});

  const [resizingColumnWidths, setResizingColumnWidths] = React.useState({});
  React.useEffect(() => {
    const columnId = Object.keys(resizingColumnWidths || {})[0];
    if (columnId) {
      const columnWidth = resizingColumnWidths[columnId];
      onResizeColumn(columnId, columnWidth);
      setResizingColumnWidths([]);
    }
  }, [onResizeColumn, resizingColumnWidths]);

  const [sortBy, setSortBy] = React.useState([]);
  React.useEffect(() => {
    const sort = (sortBy || [])[0] || {};
    if (sort.id) {
      onSortBy(sort.id, sort.desc);
      setSortBy([]);
    }
  }, [onSortBy, sortBy]);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable(
    {
      columns: React.useMemo(
        () =>
          columns.map((column) => ({
            ...column,
            minWidth: minWidths[column.accessor] || 30,
          })),
        [columns, minWidths]
      ),
      data,
      manualSortBy: true,
      disableResizing,
      stateReducer: React.useCallback((state, action) => {
        const {
          columnResizing: { columnWidths = {} },
          sortBy,
        } = state;
        switch (action.type) {
          case 'columnDoneResizing': {
            setResizingColumnWidths(columnWidths);
            return state;
          }
          case 'toggleSortBy':
            setSortBy(sortBy);
            return state;
          default:
            return state;
        }
      }, []),
    },
    useBlockLayout,
    useResizeColumns,
    useSortBy
  );

  const tableRef = React.useRef<HTMLDivElement>(null);
  React.useEffect(() => {
    if (tableRef.current) {
      const widths = {};
      columns.forEach((column) => {
        const accessor = column.accessor;
        const spacers = tableRef.current.querySelectorAll(
          `[role=columnheader] [name=${accessor}], [role=cell][nowrap=true] span[name=${accessor}]`
        );
        const maxWidthEl = maxBy(spacers, 'offsetWidth') || {};
        widths[column.accessor] = maxWidthEl.offsetWidth || 0;
      });
      setMinWidths(widths);
    }
  }, [tableRef, columns, data]);

  const tbodyRef = React.useRef(null);
  const pscroll = usePerfectScroll(tbodyRef, PERFECT_SCROLL_OPTIONS);
  const updatePScroll = React.useMemo(() => {
    const INTERVAL = 100;
    return throttle(() => {
      if (pscroll) {
        pscroll.update();
      }
    }, INTERVAL);
  }, [pscroll]);
  updatePScroll();

  /* eslint-disable react/jsx-key */
  return (
    <Table
      className={`table ${className}`}
      disableResizing={disableResizing}
      ref={tableRef}
      {...getTableProps()}
    >
      <div className="thead">
        {headerGroups.map((headerGroup) => (
          <div className="tr" {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => {
              const {
                id,
                isSorted,
                isSortedDesc,
                nosort,
                getHeaderProps,
                getSortByToggleProps,
                getResizerProps,
                render,
              } = column;
              const sort = isSortedDesc ? 'desc' : 'asc';
              const sortProps = nosort ? {} : getSortByToggleProps();
              return (
                <div className="th" {...getHeaderProps()}>
                  <span
                    name={id}
                    sort={String(isSorted && sort)}
                    {...sortProps}
                  >
                    {render('Header')}
                  </span>
                  {!disableResizing && (
                    <div {...getResizerProps({ onClick: stopPropagation })} />
                  )}
                </div>
              );
            })}
          </div>
        ))}
      </div>
      <div className="tbody" ref={tbodyRef} {...getTableBodyProps()}>
        {rows.map((row) => {
          prepareRow(row);
          return (
            <div className="tr" {...row.getRowProps()}>
              {row.cells.map((cell) => (
                <div
                  className="td"
                  name={cell.column.id}
                  nowrap={cell.column.nowrap ? 'true' : 'false'}
                  {...cell.column.data}
                  {...cell.getCellProps()}
                >
                  <span name={cell.column.id}>{cell.render('Cell')}</span>
                </div>
              ))}
            </div>
          );
        })}
      </div>
    </Table>
  );
  /* eslint-enable react/jsx-key */
};

export default IssueListPanelTable;
