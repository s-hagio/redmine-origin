import * as React from 'react';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { updatePanel, renderIssues } from '../../api';
import { PanelProps } from '../../interfaces/interface';
import LoadSpinner from '../atoms/LoadSpinnerPopup';
import { useCommentField } from '../CommentField';
import CommentFrame from '../CommentFrame';
import IssueListPanelTable from './IssueListPanelTable';
import PageVariables from '../../pageVariables';

const Link = styled.a``;
const ErrorMessage = styled.div``;
const StyledCommentFrame = styled(CommentFrame)``;
const IssueListPanelTableWrapper = styled.div`
  flex-shrink: 0;
`;
const IssueListWrapper = styled.div`
  flex-grow: 1;
  height: 100%;
  position: relative;
  display: flex;
  flex-direction: column;

  ${StyledCommentFrame} {
    background-color: #fff;
    margin-top: 4px;
    display: flex;
    min-height: 8.5rem;
    bottom: 0;
    position: relative;
    width: 100%;
  }

  ${ErrorMessage} {
    color: #f00;
    margin: 0.25rem 0;
  }

  ${Link} {
    display: flex;
    justify-content: flex-end;
  }

  ${IssueListPanelTableWrapper} {
    position: relative;
    overflow-x: auto;
    overflow-y: hidden;
    margin-bottom: 4px;
    @media all and (-ms-high-contrast: none) {
      overflow-x: scroll;
    }

    .table {
      height: 100%;
    }

    .td {
      span[name='subject'],
      span[name='relations'] {
        display: inline-block;
        width: 100%;
        text-align: left;
      }

      span[name='done_ratio'] > span {
        display: inline-block;
        width: 80px;

        .progress td {
          height: 10px;
          box-sizing: border-box;
        }
      }

      p {
        display: inline-block;
      }
    }

    .td[data-format='text'] {
      text-align: left;
    }
  }
`;

const ColumnSelect = styled.select`
  height: auto;
  width: 100%;
  padding: 0;

  option {
    padding: 6px 4px;
  }
`;

interface FormProps {
  onSubmit: (setting) => void;
  onClose: () => void;
  settings: {
    panelTitle: string;
  };
}

const PanelSettingForm = ({
  options,
  values,
  onSubmit,
  onClose,
}: FormProps): React.FC => {
  const { availableQueries, maxLimit } = options;
  const { t } = useTranslation();

  const defaultQuery = React.useMemo(
    () => availableQueries.find((query) => !query.id),
    [availableQueries]
  );
  const defaultColumnNames = React.useMemo(() => {
    return defaultQuery.columns
      .filter((column) => column.name !== 'id')
      .map((column) => column.name);
  }, [defaultQuery]);

  const [settings, setSettings] = React.useState({
    selectedColumns: defaultColumnNames,
    ...values,
  });
  React.useEffect(
    () =>
      setSettings({
        selectedColumns: defaultColumnNames,
        ...values,
      }),
    [values, defaultColumnNames]
  );

  const handleChange = (name) => (
    event: React.ChangeEvent<HTMLSelectElement>
  ): void => {
    const { value } = event.target;

    if (name === 'queryId') {
      const { availableQueries } = options;
      const query = availableQueries.find(
        (query) => Number(value) === Number(query.id)
      );
      setSettings({
        ...settings,
        [name]: value,
        selectedColumns: query.columns
          .filter((column) => column.name !== 'id')
          .map((column) => column.name),
      });
    } else {
      setSettings({
        ...settings,
        [name]: value,
      });
    }
  };

  const handleToggleVisibleColumn = (
    event: React.ChangeEvent<HTMLSelectElement>
  ): void => {
    const { selectedOptions } = event.target;
    setSettings({
      ...settings,
      selectedColumns: [...selectedOptions].map((opt) => opt.value),
    });
  };

  const handleSubmit = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    event.preventDefault();
    onSubmit(settings);
    toast(t('save_setting_alert'));
    onClose();
  };

  const { limit, panelTitle, queryId, selectedColumns } = settings;
  const currentQuery =
    availableQueries.find((query) => Number(queryId) === Number(query.id)) ||
    defaultQuery;

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <input
          value={panelTitle || ''}
          placeholder={t('input_panel_title')}
          onChange={handleChange('panelTitle')}
        />
      </div>
      <div>
        <input
          value={limit || ''}
          type="number"
          min={1}
          max={maxLimit}
          placeholder={t('label_issues_limit')}
          onChange={handleChange('limit')}
        />
        <span>{t('label_count_issues', { value: '' })}</span>
      </div>
      <div>
        <select value={queryId} onChange={handleChange('queryId')}>
          {availableQueries.map((query) => (
            <option key={query.id} value={query.id || ''}>
              {query.name || t('select_query')}
            </option>
          ))}
        </select>
      </div>
      <div>
        <ColumnSelect
          multiple
          value={selectedColumns}
          onChange={handleToggleVisibleColumn}
        >
          {currentQuery.columns
            .filter((column) => column.name !== 'id')
            .map((column) => (
              <option key={column.name} value={column.name}>
                {column.caption}
              </option>
            ))}
        </ColumnSelect>
      </div>
      <div>
        <button type="submit">{t('button_save')}</button>
        <button onClick={onClose}>{t('button_cancel')}</button>
      </div>
    </form>
  );
};

// eslint-disable-next-line react/display-name
const renderPanelSettings = (values, options) => ({
  updateSettings,
  closeSettings,
}): React.ReactElement => (
  <PanelSettingForm
    values={values}
    options={options}
    onSubmit={updateSettings}
    onClose={closeSettings}
  />
);

interface IssueListContentProps {
  query: {
    columns: any[];
  };
  availableQueries: any[];
  snapshot: any[];
  pending: boolean;
}

const NOWRAP_COLUMN_NAMES = [
  'id',
  'updated_on',
  'start_date',
  'due_date',
  'estimated_hours',
  'done_ratio',
  'created_on',
  'closed_on',
  'actual_start_date',
  'actual_due_date',
];
const NOWRAP_CUSTOM_FIELD_COLUMN_FORMATS = ['date'];
const DEFAULT_COLUMN_WIDTH = 150;
const IssueListPanel: React.FC<PanelProps<IssueListContentProps>> = ({
  panel,
  editLayoutMode,
  PanelRootComponent,
}: PanelProps<IssueListContentProps>) => {
  const { t } = useTranslation();
  const { comment, content, settings: storedSettings } = panel;
  const [isCommentVisible, setCommentVisible] = React.useState(true);
  const { renderEditButton, renderComment } = useCommentField(
    comment,
    setCommentVisible
  );
  React.useEffect(() => {
    setCommentVisible(!!comment.text);
  }, []);
  const [settings, setSettings] = React.useState(storedSettings);
  React.useEffect(() => setSettings(storedSettings), [storedSettings]);
  const { panelTitle, queryId, limit = 10, columnWidths = {} } = settings;
  const {
    query: { columns: queryColumns },
    availableQueries,
    snapshot,
    pending,
  } = content;
  const queryNotAvailable =
    queryId && !availableQueries.map(({ id }) => id).includes(Number(queryId));

  const [loading, setLoading] = React.useState(false);
  const [issues, setIssues] = React.useState([]);
  React.useEffect(() => {
    if (pending) {
      (async (): Promise<void> => {
        const response = await renderIssues({ query_id: queryId, limit });
        setIssues(await response.json());
      })();
    } else {
      setIssues(snapshot);
    }
  }, [queryId, limit]);

  const handleSort = React.useCallback(
    async (id, desc) => {
      try {
        setLoading(true);
        const response = await renderIssues({
          query_id: queryId,
          sort_by: id,
          direction: desc ? 'desc' : 'asc',
          limit,
        });
        setIssues(await response.json());
        /* eslint-enable */
      } finally {
        setLoading(false);
      }
    },
    [queryId, limit]
  );

  const columns = React.useMemo(
    () =>
      queryColumns.map((column) => {
        const nowrap =
          NOWRAP_COLUMN_NAMES.includes(column.name) ||
          NOWRAP_CUSTOM_FIELD_COLUMN_FORMATS.includes(column.format);
        const accessor = column.name.replace('.', '__');
        const width =
          columnWidths[accessor] || (nowrap ? 0 : DEFAULT_COLUMN_WIDTH);
        const renderCell: React.FC<any> = ({ row }: any) => {
          const {
            original: { contents },
          } = row;
          const content = contents[column.name]?.replace(
            /<a/,
            '<a target="_blank"'
          );
          return <span dangerouslySetInnerHTML={{ __html: content }} />;
        };

        return {
          Header: column.caption,
          // eslint-disable-next-line react/display-name
          Cell: renderCell,
          nosort: pending ? !column.sortable : true,
          accessor,
          nowrap,
          width,
          data: {
            'data-name': column.name,
            'data-format': column.format,
          },
        };
      }),
    [queryColumns, columnWidths]
  );

  const handleResize = React.useCallback(
    (name, width) => {
      if (columnWidths[name] !== width) {
        const values = {
          ...settings,
          columnWidths: {
            ...columnWidths,
            [name]: width,
          },
        };
        setSettings(values);
        if (editLayoutMode) {
          updatePanel(panel.id, { settings: values });
        }
      }
    },
    [columnWidths, panel.id, settings, editLayoutMode]
  );

  const { urls, projectIdentifier } = PageVariables;
  const issuesLinkPath = queryId
    ? `${urls.root}/projects/${projectIdentifier}/issues?query_id=${queryId}`
    : `${urls.root}/projects/${projectIdentifier}/issues?set_filter=1&sort=`;

  return (
    <PanelRootComponent
      title={panelTitle}
      data-test="issue-list-panel" // For feature spec
      titlebarButtons={renderEditButton()}
      renderPanelSettings={renderPanelSettings(settings, content)}
    >
      <IssueListWrapper>
        {queryNotAvailable && pending && (
          <ErrorMessage>
            {t('issue_list_panel.message.query_not_available')}
          </ErrorMessage>
        )}
        {!(queryNotAvailable && pending) && (
          <>
            <IssueListPanelTableWrapper>
              <IssueListPanelTable
                columns={columns}
                data={issues}
                disableResizing={loading}
                onResizeColumn={handleResize}
                onSortBy={handleSort}
              />
              {loading && <LoadSpinner />}
            </IssueListPanelTableWrapper>
            <Link target="_blank" href={issuesLinkPath}>
              {t('label_issues_link')}
            </Link>
          </>
        )}
        <StyledCommentFrame visible={isCommentVisible}>
          {renderComment()}
        </StyledCommentFrame>
      </IssueListWrapper>
    </PanelRootComponent>
  );
};

export default IssueListPanel;
