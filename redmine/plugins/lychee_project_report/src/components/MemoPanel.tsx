import * as React from 'react';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { PanelProps } from '../interfaces/interface';
import { useCommentField } from './CommentField';

const InputField = styled.input`
  width: 180px;
`;

const Comment = styled.div`
  overflow-y: auto;
  height: 100%;
`;

interface FormProps {
  onSubmit: (setting: MemoSettings) => void;
  onClose: () => void;
  settings: MemoSettings;
}

const PanelSettingForm: React.FC<FormProps> = ({
  onSubmit,
  onClose,
  settings,
}: FormProps) => {
  const { t } = useTranslation();

  const [formValues, setFormValues] = React.useState<MemoSettings>(settings);

  const handleChange = (name) => (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    const { value } = event.target;

    setFormValues({
      ...formValues,
      [name]: value,
    });
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();
    onSubmit(formValues);
    toast(t('save_setting_alert'));
    onClose();
  };

  const { panelTitle } = formValues;

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <InputField
          value={panelTitle || ''}
          placeholder={t('input_panel_title')}
          onChange={handleChange('panelTitle')}
        />
      </div>
      <div>
        <button type="submit">{t('button_save')}</button>
        <button onClick={onClose}>{t('button_cancel')}</button>
      </div>
    </form>
  );
};

// eslint-disable-next-line react/display-name
const renderPanelSettings = (settings: MemoSettings) => ({
  updateSettings,
  closeSettings,
}): React.ReactElement => (
  <PanelSettingForm
    settings={settings}
    onSubmit={updateSettings}
    onClose={closeSettings}
  />
);

interface MemoSettings {
  panelTitle: string;
}

const MemoPanel: React.FC<PanelProps<{}, MemoSettings>> = ({
  panel,
  PanelRootComponent,
}: PanelProps<{}, MemoSettings>) => {
  const { settings } = panel;
  const { panelTitle } = settings;

  const [isCommentVisible, setCommentVisible] = React.useState(true);
  const { renderEditButton, renderComment } = useCommentField(
    panel.comment,
    setCommentVisible
  );
  React.useEffect(() => {
    setCommentVisible(!!panel.comment.text);
  }, []);

  return (
    <PanelRootComponent
      title={panelTitle}
      data-test="memo-panel" // For feature spec
      titlebarButtons={renderEditButton()}
      renderPanelSettings={renderPanelSettings(settings)}
    >
      <Comment style={isCommentVisible ? {} : { display: 'none' }}>
        {renderComment()}
      </Comment>
    </PanelRootComponent>
  );
};

export default MemoPanel;
