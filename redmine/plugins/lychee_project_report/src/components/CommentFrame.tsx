import * as React from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import { MoreButton } from './CommentField';

const Frame = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  padding: 10px 0 10px 10px;

  ${MoreButton} {
    right: 0px;
  }
  box-sizing: border-box;
`;

const Label = styled.span`
  font-weight: bold;
  margin-bottom: 1em;
`;

interface CommentFrameProps {
  className?: string;
  children: React.ReactNode;
  visible: boolean;
}

const CommentFrame: React.FC<CommentFrameProps> = ({
  className,
  children,
  visible,
}: CommentFrameProps) => {
  const { t } = useTranslation();
  return (
    <div className={className} style={visible ? {} : { display: 'none' }}>
      <Frame>
        <Label>{t('label_comment')}</Label>
        {children}
      </Frame>
    </div>
  );
};

export default CommentFrame;
