import * as React from "react";

// position: absolute でページの真ん中ではなく、親要素の真ん中になります
const LoadSpinnerPopup: React.FC<{}> = (): React.ReactElement => (
  <div id="ajax-indicator" style={{ position: "absolute" }}>
    <span>Loading</span>
  </div>
);

export default LoadSpinnerPopup;
