import * as React from "react";
import styled from "styled-components";
import Checkbox from "./Checkbox";

const SingleCheckbox = styled(Checkbox)`
  padding-right: 8px;
  margin-right: 10px;
`;

const Group = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  -ms-grid-columns: repeat(3, 1fr);
  height: 100%;
  padding-top: 5px;
  padding-bottom: 10px;
`;

const toggleId = (
  idList: Array<number | string>,
  id: number | string,
  on: boolean
): Array<number | string> => {
  if (on) {
    return [...idList, id];
  } else {
    return idList.filter(i => i !== id);
  }
};

interface CheckboxGroupProps {
  values: Array<{ id: number | string; name: string }>;
  checked: Array<number | string>;
  onChange: (value: Array<number | string>) => void;
}

const CheckboxGroup: React.FC<CheckboxGroupProps> = ({
  values,
  checked,
  onChange
}: CheckboxGroupProps): React.ReactElement => {
  return (
    <Group>
      {values.map(v => (
        <SingleCheckbox
          key={`${v.id}`}
          checked={checked.includes(v.id)}
          onChange={(on): void => onChange(toggleId(checked, v.id, on))}
        >
          {v.name}
        </SingleCheckbox>
      ))}
    </Group>
  );
};

export default CheckboxGroup;
