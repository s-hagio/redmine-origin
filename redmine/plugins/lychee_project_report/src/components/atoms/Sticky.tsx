import * as React from "react";
import styled from "styled-components";
import Stickyfill from "stickyfilljs";

interface StickyContentProps {
  className?: string;
  children: React.ReactNode;
}

const Wrapper = styled.div`
  position: sticky;
  &::before,
  &::after {
    content: "";
    display: table;
  }

  @media print {
    position: static;
  }
`;

const Sticky: React.FC<StickyContentProps> = ({
  children,
  className
}: StickyContentProps) => {
  const wrapperEl = React.useRef(null);
  React.useEffect(() => {
    Stickyfill.add(wrapperEl.current);
  }, []);
  return (
    <Wrapper className={className} ref={wrapperEl}>
      {children}
    </Wrapper>
  );
};

export default Sticky;
