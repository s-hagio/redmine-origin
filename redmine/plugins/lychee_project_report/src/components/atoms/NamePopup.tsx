import * as React from "react"
import { useTranslation } from "react-i18next";
import Popup from "reactjs-popup";
import styled from "styled-components";

const NameInput = styled.input`
  min-width: 200px;
`;

interface NameFormProps {
  placeholder: string;
  onSubmit: (name: string) => void;
}

const NameForm: React.FC<NameFormProps> = ({
  placeholder,
  onSubmit
}: NameFormProps) => {
  const { t } = useTranslation();
  const [name, setName] = React.useState<string>("");

  return (
    <div>
      <NameInput
        value={name}
        placeholder={placeholder}
        onChange={(ev): void => setName(ev.target.value)}
      />
      <button disabled={name === ""} onClick={(): void => onSubmit(name)}>
        {t("button_save")}
      </button>
    </div>
  );
};

interface NamePopupProps {
  inputPlaceholder: string;
  trigger: React.ReactElement;
  onSubmit: (name: string) => void;
}

const NamePopup: React.FC<NamePopupProps> = ({
  inputPlaceholder,
  trigger,
  onSubmit
}: NamePopupProps) => (
  <Popup
    trigger={trigger}
    position="bottom center"
    contentStyle={{ width: "auto" }}
  >
    <NameForm onSubmit={onSubmit} placeholder={inputPlaceholder} />
  </Popup>
);

export default NamePopup;
