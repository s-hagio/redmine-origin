import styled from "styled-components";

const Spacer = styled.span`
  flex-grow: 1;
`;

export default Spacer;
