import * as React from "react";
import classNames from "classnames";
import styled from "styled-components";

const LinkLabel = styled.a`
  color: #169;
  cursor: pointer;
  margin-left: 5px;
`;

type ClickEvent = React.MouseEvent<HTMLAnchorElement>;

interface IconButtonProps {
  className?: string;
  icon: string;
  children: React.ReactNode;
  onClick?: (ev: ClickEvent) => void;
}

export type Ref = HTMLAnchorElement;

const IconButton = React.forwardRef<Ref, IconButtonProps>(
  ({ className, icon, children, onClick }: IconButtonProps, ref) => {
    const handleClick = React.useCallback(
      (ev: ClickEvent): void => {
        if (onClick) {
          onClick(ev);
        }
      },
      [onClick]
    );

    return (
      <LinkLabel
        ref={ref}
        className={classNames(["icon", `icon-${icon}`, className])}
        onClick={handleClick}
      >
        {children}
      </LinkLabel>
    );
  }
);

IconButton.displayName = "IconButton";

export default IconButton;
