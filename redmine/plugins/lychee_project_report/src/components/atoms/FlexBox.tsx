import styled from "styled-components";

interface FlexBoxProps {
  align?: string;
  justify?: string;
  ["flex-wrap"]?: string;
  ["flex-grow"]?: string;
  ["flex-basis"]?: string;
}

export default styled.div<FlexBoxProps>`
  display: flex;
  ${(props): string => props.align && `align-items: ${props.align};`}
  ${(props): string => props.justify && `justify-content: ${props.justify};`}
  ${(props): string =>
    props["flex-wrap"] && `flex-wrap: ${props["flex-wrap"]};`}
  ${(props): string =>
    props["flex-grow"] && `flex-grow: ${props["flex-grow"]};`}
  ${(props): string =>
    props["flex-basis"] && `flex-basis: ${props["flex-basis"]};`}
`;
