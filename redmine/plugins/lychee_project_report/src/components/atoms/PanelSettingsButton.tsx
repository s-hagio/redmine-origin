import * as React from "react";
import { useTranslation } from "react-i18next";
import Popup from "reactjs-popup";
import styled from "styled-components";
import PageVariables from "../../pageVariables";
import { TitlebarButton } from "./PanelLayout";

const GearIcon = styled(TitlebarButton)`
  background: url(${PageVariables.urls.root}/images/changeset.png) no-repeat;
`;

interface PanelSettingsButtonProps {
  renderForm: (close) => React.ReactElement;
}

const PanelSettingsButton: React.FC<PanelSettingsButtonProps> = ({
  renderForm
}: PanelSettingsButtonProps): React.ReactElement => {
  const { t } = useTranslation();

  return (
    <Popup
      trigger={<GearIcon title={t("label_options")} />}
      position={["right center", "left center"]}
      contentStyle={{ width: "auto", lineHeight: "normal", zIndex: 10 }}
      overlayStyle={{ zIndex: 9 }}
    >
      {renderForm}
    </Popup>
  );
};

export default PanelSettingsButton;
