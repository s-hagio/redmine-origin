import * as React from "react";
import Popup from "reactjs-popup";
import styled from "styled-components";

const Root = styled.div`
  height: auto;
  width: auto;
  display: block;
  z-index: 101;
  position: relative;
  padding: 0;
`;

const Content = styled.div`
  width: auto;
  min-height: 95px;
  max-height: 500px;
  height: auto;
  overflow-y: scroll;
`;

const IS_IE =
  window.navigator.userAgent.toLowerCase().indexOf("trident") !== -1;
const PopModal = IS_IE
  ? styled(Popup)`
      &-content {
        max-height: 0px;
      } //IEのときだけ
    `
  : Popup;

interface ModalProps {
  children: React.ReactNode;
  title?: string;
  trigger: JSX.Element | ((isOpen: boolean) => JSX.Element);
}

// jQueryのモーダルを真似するコンポーネント
const Modal: React.FC<ModalProps> = ({
  children,
  title,
  trigger
}: ModalProps) => (
  <PopModal
    trigger={trigger}
    modal
    closeOnDocumentClick={false}
    contentStyle={{ padding: 0, borderRadius: "5px", border: "1px solid #ddd" }}
  >
    {(close): React.ReactElement => (
      <Root className="ui-dialog ui-widget ui-corner-all ui-front modal">
        <div className="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle">
          {
            <>
              {title && <span className="ui-dialog-title">{title}</span>}
              <button
                className="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close"
                title="Close"
                onClick={close}
              >
                <span className="ui-button-icon-primary ui-icon ui-icon-closethick" />
              </button>
            </>
          }
        </div>
        <Content className="ui-dialog-content ui-widget-content">
          {children}
        </Content>
      </Root>
    )}
  </PopModal>
);

export default Modal;
