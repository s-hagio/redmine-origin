import * as React from "react";
import styled from "styled-components";
import FieldLabel from "./FieldLabel";

const Root = styled.div`
  display: flex;
  align-items: center;
  height: 18px;
`;

interface CheckboxProps {
  children: React.ReactNode;
  className?: string;
  checked: boolean;
  onChange: (value: boolean) => void;
}

const Checkbox: React.FC<CheckboxProps> = ({
  children,
  checked,
  onChange,
  className
}: CheckboxProps): React.ReactElement => {
  const handleChange = React.useCallback(() => {
    if (onChange) {
      onChange(!checked);
    }
  }, [checked, onChange]);

  return (
    <Root className={className}>
      <input type="checkbox" checked={checked} onChange={handleChange} />
      <FieldLabel onClick={handleChange}>{children}</FieldLabel>
    </Root>
  );
};

export default Checkbox;
