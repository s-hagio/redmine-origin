import * as React from 'react';
import { parseISO } from 'date-fns';

interface DateFieldProps {
  value: string;
  onChange?: (value: string) => void;
}

const DateField: React.FC<DateFieldProps> = ({
  value,
  onChange,
}: DateFieldProps): React.ReactElement => {
  const handleChange = React.useCallback(
    (value: string): void => {
      if (!onChange) {
        return;
      }
      const isInvalidValue = !parseISO(value).valueOf();
      if (isInvalidValue) {
        onChange('');
        return;
      }

      onChange(value);
    },
    [onChange]
  );

  const dateField = React.useRef<HTMLInputElement>(null);

  React.useEffect(() => {
    if (dateField.current) {
      // Redmine本体のjQueryを使う。
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      $(dateField.current).datepickerFallback({
        dateFormat: 'yy-mm-dd',
        onClose: function (dateText: string) {
          handleChange(dateText);
        },
      });
    }
  }, [dateField, handleChange, onChange]);

  return (
    <input
      value={value}
      type="date"
      className="date"
      onChange={(ev: React.ChangeEvent<HTMLInputElement>): void =>
        handleChange(ev.target.value)
      }
      ref={dateField}
    />
  );
};

export default DateField;
