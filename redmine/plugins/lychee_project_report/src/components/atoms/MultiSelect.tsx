import * as React from 'react';
import Select, {
  OptionsType,
  OptionTypeBase,
  MultiValueProps,
  StylesConfig,
} from 'react-select';

export type SelectStyles = StylesConfig;

const reactSelectStyles: StylesConfig = {
  multiValue: (styles) => ({ ...styles, justifyContent: 'space-between' }),
  multiValueRemove: (styles) => ({ ...styles, flexShrink: 0 }),
};

const ReactSelectCustomMultiValueLabel: React.FC<
  MultiValueProps<OptionTypeBase>
> = ({ children, data, innerProps }: MultiValueProps<OptionTypeBase>) => (
  <div {...innerProps} title={data.label}>
    {children}
  </div>
);

interface MultiSelectProps {
  options: OptionsType<any>;
  values: Array<any>;
  onChange: (values: Array<any>) => void;
  selectStyles?: SelectStyles;
}

const MultiSelect: React.FC<MultiSelectProps> = ({
  options,
  values,
  onChange,
  selectStyles,
}: MultiSelectProps) => (
  <Select
    className="select-versions"
    options={options}
    isMulti
    value={values}
    onChange={onChange}
    styles={{ ...reactSelectStyles, ...(selectStyles || {}) }}
    components={{ MultiValueLabel: ReactSelectCustomMultiValueLabel }}
  />
);

export default MultiSelect;
