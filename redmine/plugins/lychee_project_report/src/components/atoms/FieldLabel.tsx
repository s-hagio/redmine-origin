import * as React from "react";
import styled from "styled-components";

const RedAsterisk = styled.span.attrs({ children: "*" })`
  color: red;
`;

interface FieldLabelProps {
  children: React.ReactNode;
  required?: boolean;
  onClick?: (event: React.MouseEvent<HTMLLabelElement, MouseEvent>) => void;
}

const FieldLabel: React.FC<FieldLabelProps> = ({
  children,
  required,
  onClick
}: FieldLabelProps): React.ReactElement => {
  return (
    <label onClick={onClick}>
      {children}
      {required && <RedAsterisk />}
    </label>
  );
};

export default FieldLabel;
