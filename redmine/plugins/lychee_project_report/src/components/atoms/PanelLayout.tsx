import * as React from 'react';
import styled from 'styled-components';
import Spacer from './Spacer';

const PanelRoot = styled.div`
  color: #000000;
  height: 100%;
`;

const panelPaddingXpx = 10;
const panelPaddingYpx = 5;
const panelPadding = `${panelPaddingYpx}px ${panelPaddingXpx}px`;

const Titlebar = styled.div`
  display: flex;

  background-color: #f0f3fa;
  padding: ${panelPadding};
  white-space: nowrap;
  margin: 0;
  border-radius: 5px;
`;

const Title = styled.h2`
  margin: 0;

  text-overflow: ellipsis;
  overflow: hidden;
  font-size: 20px;
  padding: 2px 10px 1px 0;
  line-height: 1.5em;
`;

const TitlebarButtons = styled.span`
  line-height: 33px;
  & > * {
    padding: 0px 3px;
  }
`;

const titleBarHeight = 43; // 単位はpx. タイトルの高さ+タイトルのpadding
const Content = styled.div<{ isTitleDisplay: boolean }>`
  white-space: nowrap;
  overflow: auto;
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  ${(props) =>
    props.isTitleDisplay &&
    `padding: ${panelPadding};
    height: calc(100% - ${
      titleBarHeight + panelPaddingYpx + panelPaddingYpx
    }px);`}
`;

export const TitlebarButton = styled.span`
  cursor: pointer;
  width: 16px;
  height: 16px;
  display: inline-block;
`;

interface PanelLayoutProps {
  children: React.ReactNode;
  isTitleDisplay?: boolean;
  title: string;
  titlebarButtons?: React.ReactNode;
}

const PanelLayout: React.FC<PanelLayoutProps> = ({
  children,
  isTitleDisplay,
  title,
  titlebarButtons,
  ...otherAttributes // Forwarding data-test for feature spec
}: PanelLayoutProps) => {
  return (
    <PanelRoot {...otherAttributes}>
      {isTitleDisplay && (
        <Titlebar className="panel-drag-handle">
          <Title
            data-test="title" // For feature spec
            title={title}
          >
            {title}
          </Title>
          <Spacer />
          <TitlebarButtons>{titlebarButtons}</TitlebarButtons>
        </Titlebar>
      )}
      <Content isTitleDisplay={isTitleDisplay}>{children}</Content>
    </PanelRoot>
  );
};

PanelLayout.defaultProps = {
  isTitleDisplay: true,
};

export default PanelLayout;
