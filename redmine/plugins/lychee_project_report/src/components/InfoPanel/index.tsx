import * as React from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import { PanelProps } from '../../interfaces/interface';
import SettingsForm, { InfoPanelSettings } from './SettingsForm';

const InfoTable = styled.table`
  width: 100%;
  table-layout: fixed;
  margin-top: 10px;

  th {
    font-weight: bold;
    width: 35%;
  }

  td {
    width: 65%;
  }

  th,
  td {
    border-bottom: 2px solid;
    border-color: #e4e4e4;
    text-align: left;
    white-space: pre-wrap;
    word-wrap: break-word;
    overflow-wrap: break-word;
  }

  tr:nth-child(2n + 2) {
    background-color: #fcfcfc;
  }

  span.project-description p {
    overflow-wrap: break-word;
    word-wrap: break-word;
    padding-top: 1em !important;
    padding-bottom: 1em !important;
  }

  span.project-description ul,
  span.project-description ol {
    padding-left: 40px;
    margin-bottom: 1em;
    white-space: normal;
  }

  span.project-description ul {
    list-style: disc;
  }

  span.project-description ul ul {
    list-style: circle;
  }

  span.project-description ul ul ul {
    list-style: square;
  }

  span.project-description ol {
    list-style: decimal;
  }

  span.project-description table,
  span.project-description td,
  span.project-description th {
    border: 1px solid #bbb;
    width: auto;
  }

  span.project-description td,
  span.project-description th {
    padding: 4px;
  }

  span.project-description th {
    text-align: center;
  }

  span.project-description table {
    border-collapse: collapse;
    margin-bottom: 1em;
  }
`;

type InfoContentProps = Partial<{
  projectName: string;
  projectStartDate: string;
  projectEndDate: string;
  displayedFields: Array<{
    id: number;
    name: string;
    value: string;
    format: string;
  }>;
  availableCustomFields: Array<{
    id: number;
    name: string;
  }>;
  projectDescription: string;
}>;

const InfoPanel: React.FC<PanelProps<InfoContentProps, InfoPanelSettings>> = ({
  panel,
  PanelRootComponent,
}: PanelProps<InfoContentProps, InfoPanelSettings>) => {
  const { t } = useTranslation();
  const {
    projectName,
    projectStartDate,
    projectEndDate,
    displayedFields,
    availableCustomFields,
    projectDescription,
  } = panel.content;
  const { visibleBaseFieldsIds } = panel.settings;

  return (
    <PanelRootComponent
      title={t('panel_names.LycheeProjectReport::Panels::Info')}
      renderPanelSettings={({
        updateSettings,
        closeSettings,
      }): React.ReactElement => (
        <SettingsForm
          settings={panel.settings}
          updateSettings={updateSettings}
          closeSettings={closeSettings}
          availableCustomFields={availableCustomFields}
        ></SettingsForm>
      )}
      data-test="info-panel" // For feature spec
    >
      <InfoTable>
        <tbody>
          {visibleBaseFieldsIds.includes('projectName') && (
            <tr>
              <th>{t('info_panel.label.project_name')}</th>
              <td>{projectName}</td>
            </tr>
          )}
          {visibleBaseFieldsIds.includes('projectStartDate') && (
            <tr>
              <th>{t('info_panel.label.project_start_date')}</th>
              <td>{projectStartDate}</td>
            </tr>
          )}
          {visibleBaseFieldsIds.includes('projectEndDate') && (
            <tr>
              <th>{t('info_panel.label.project_end_date')}</th>
              <td>{projectEndDate}</td>
            </tr>
          )}
          {displayedFields &&
            displayedFields.map((v) =>
              v.format === 'text' ? (
                <React.Fragment key={`${v.id}`}>
                  <tr>
                    <th>{v.name}</th>
                    <td></td>
                  </tr>
                  <tr>
                    <td colSpan={2}>{v.value}</td>
                  </tr>
                </React.Fragment>
              ) : (
                <tr key={`${v.id}`}>
                  <th>{v.name}</th>
                  <td>
                    {v.format === 'int' || v.format === 'float' ? (
                      Number(v.value).toLocaleString()
                    ) : v.format === 'link' ? (
                      v.html_value ? (
                        <span
                          dangerouslySetInnerHTML={{ __html: v.html_value }}
                        ></span>
                      ) : (
                        <a
                          href={
                            v.value.startsWith('http')
                              ? v.value
                              : 'http://' + v.value
                          }
                        >
                          {v.value}
                        </a>
                      )
                    ) : (
                      v.value
                    )}
                  </td>
                </tr>
              )
            )}
          {visibleBaseFieldsIds.includes('projectDescription') && (
            <>
              <tr>
                <th>{t('info_panel.label.project_description')}</th>
                <td></td>
              </tr>
              <tr>
                <td colSpan={2}>
                  <span
                    className="project-description"
                    dangerouslySetInnerHTML={{ __html: projectDescription }}
                  ></span>
                </td>
              </tr>
            </>
          )}
        </tbody>
      </InfoTable>
    </PanelRootComponent>
  );
};

export default InfoPanel;
