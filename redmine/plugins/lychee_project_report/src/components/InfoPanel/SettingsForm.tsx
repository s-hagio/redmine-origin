import * as React from 'react';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import { useTranslationWithPrefix } from '../../i18n';
import CheckboxGroup from '../atoms/CheckboxGroup';

export interface InfoPanelSettings {
  visibleCustomFieldIds: Array<number>;
  visibleBaseFieldsIds: Array<string>;
}

interface SettingsFormProps {
  settings: InfoPanelSettings;
  availableCustomFields: Array<{
    id: number;
    name: string;
  }>;
  closeSettings: () => void;
  updateSettings: (settings: InfoPanelSettings) => void;
}

const SettingsForm: React.FC<SettingsFormProps> = ({
  closeSettings,
  updateSettings,
  settings,
  availableCustomFields,
}: SettingsFormProps) => {
  const { t } = useTranslationWithPrefix('info_panel.settings');

  const [formValues, setFormValues] = React.useState(settings);

  const availableFields = React.useMemo(
    () => [
      {
        id: 'projectName',
        name: t('label_project_name'),
      },
      {
        id: 'projectStartDate',
        name: t('label_project_start_date'),
      },
      {
        id: 'projectEndDate',
        name: t('label_project_end_date'),
      },
      ...availableCustomFields,
      {
        id: 'projectDescription',
        name: t('label_project_description'),
      },
    ],
    [availableCustomFields, t]
  );

  const { visibleCustomFieldIds, visibleBaseFieldsIds } = formValues;

  const visibleFields = React.useMemo(
    () => [...visibleBaseFieldsIds, ...visibleCustomFieldIds],
    [visibleBaseFieldsIds, visibleCustomFieldIds]
  );
  const message = useTranslation().t('save_setting_alert');
  const submit = (): void => {
    updateSettings(formValues);
    toast(message);
    closeSettings();
  };

  return (
    <div>
      <label>{t('label_displayedFields')}:</label>
      <CheckboxGroup
        values={availableFields}
        checked={visibleFields}
        onChange={(newIds): void =>
          setFormValues({
            visibleCustomFieldIds: newIds.filter(
              (id) => typeof id === 'number'
            ) as number[],
            visibleBaseFieldsIds: newIds.filter(
              (id) => typeof id === 'string'
            ) as string[],
          })
        }
      />
      <div>
        <button onClick={submit}>{useTranslation().t('button_save')}</button>
        <button onClick={closeSettings}>
          {useTranslation().t('button_cancel')}
        </button>
      </div>
    </div>
  );
};

export default SettingsForm;
