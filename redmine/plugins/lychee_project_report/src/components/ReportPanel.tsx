import * as React from 'react';
import { PanelProps } from '../interfaces/interface';
import { useCommentField } from './CommentField';
import CommentFrame from './CommentFrame';
import ReportPanelSettingsForm, {
  ReportPanelSettings,
} from './ReportPanelSettingsForm';
import IndiceFrame from '../components/PeriodicReportPanel/IndiceFrame';
import styled from 'styled-components';

export const PanelContent = styled.div`
  flex-grow: 1;
  height: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
`;

const CommentArea = styled(CommentFrame)`
  background-color: #fff;
  border-top: 2px solid #ededed;
  margin-top: 4px;
  display: flex;
  min-height: 0; // オーバーフローを防ぐため
  height: 100%; // IE対応
  bottom: 0;
  position: relative;
  width: 100%;
`;

const ErrorMessage = styled.div`
  white-space: pre-wrap;
  overflow-wrap: break-word;
  word-wrap: break-word; // IE11 対策。 overflow-wrap が使えないから
  overflow: auto;
  height: 100%;
`;

interface ReportProps {
  indiceSettingName: string | null;
  isPersisted: boolean;
  decision: string;
  isResultPresent: boolean;
  reportResultValue: string;
  isResultPrevPresent: boolean;
  indiceSetting: {
    unitString: string;
    biggerBetter: boolean;
    signalDisabled: boolean;
    upperThreshold: number;
    middleThreshold: number;
  };
  arrowStyle: string;
  prevReportResultValue: string;
  availableIndexes: Array<{ id: number; name: string }>;
  errorMessage?: string;
}

const ReportPanel: React.FC<PanelProps<ReportProps, ReportPanelSettings>> = ({
  panel,
  PanelRootComponent,
}: PanelProps<ReportProps, ReportPanelSettings>) => {
  const {
    indiceSettingName,
    isPersisted,
    decision,
    isResultPresent,
    reportResultValue,
    isResultPrevPresent,
    indiceSetting,
    arrowStyle,
    prevReportResultValue,
    availableIndexes,
    errorMessage,
  } = panel.content;

  const [isCommentVisible, setCommentVisible] = React.useState(true);
  const { renderEditButton, renderComment } = useCommentField(
    panel.comment,
    setCommentVisible
  );
  React.useEffect(() => {
    setCommentVisible(!!panel.comment.text);
  }, []);

  return (
    <PanelRootComponent
      title={indiceSettingName}
      titlebarButtons={renderEditButton()}
      renderPanelSettings={({
        updateSettings,
        closeSettings,
      }): React.ReactElement => (
        <ReportPanelSettingsForm
          availableIndexes={availableIndexes}
          settings={panel.settings}
          updateSettings={updateSettings}
          closeSettings={closeSettings}
        ></ReportPanelSettingsForm>
      )}
      data-test="report-panel" // For feature spec
    >
      {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
      {isPersisted && (
        <PanelContent>
          <IndiceFrame
            decision={decision}
            isResultPresent={isResultPresent}
            reportResultValue={reportResultValue}
            isResultPrevPresent={isResultPrevPresent}
            prevReportResultValue={prevReportResultValue}
            indiceSetting={indiceSetting}
            arrowStyle={arrowStyle}
          />
          <CommentArea visible={isCommentVisible}>
            {renderComment()}
          </CommentArea>
        </PanelContent>
      )}
    </PanelRootComponent>
  );
};

export default ReportPanel;
