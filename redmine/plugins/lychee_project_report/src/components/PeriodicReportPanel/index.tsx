import * as React from 'react';
import { PanelProps } from '../../interfaces/interface';
import { useCommentField } from '../CommentField';
import CommentFrame from '../CommentFrame';
import SettingsForm, { Settings } from './SettingsForm';
import IndiceFrame from './IndiceFrame';
import HistoryGraph, { HistoryGraphData } from './HistoryGraph';
import styled from 'styled-components';

const PeriodicReportPanelContainer = styled.div`
  display: flex;
  height: 100%;
`;

const PeriodicReportPanelWrapper = styled.div`
  flex-grow: 1;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const PeriodicReportPanelLeft = styled.span`
  overflow: hidden;
  border-right: 2px solid #ededed;
  flex-grow: 1;
  box-sizing: border-box;

  .highcharts-container {
    display: flex;
    flex-direction: column;
  }
`;

const PeriodicReportPanelRight = styled.span`
  width: 30%;
  box-sizing: border-box;
`;

const PanelRightContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const CommentArea = styled(CommentFrame)`
  border-top: 1px solid #d5d5d5;
  background-color: #fff;
  margin-top: 4px;
  display: flex;
  min-height: 8.5rem;
  bottom: 0;
  position: relative;
`;

const ErrorMessage = styled.div`
  white-space: pre-wrap;
  overflow-wrap: break-word;
  word-wrap: break-word; // IE11 対策。 overflow-wrap が使えないから
  overflow: auto;
  height: 100%;
`;

interface PeriodicReportProps {
  indiceSettingName: string | null;
  isPersisted: boolean;
  decision: string;
  isResultPresent: boolean;
  reportResultValue: string;
  isResultPrevPresent: boolean;
  arrowStyle: string;
  prevReportResultValue: string;
  availableIndexes: Array<{ id: number; name: string }>;
  errorMessage?: {
    key: string;
    options: Record<string, string>;
  };
  indiceSetting: {
    unitString: string;
    biggerBetter: boolean;
    signalDisabled: boolean;
    upperThreshold: number;
    middleThreshold: number;
  };
  graphData: HistoryGraphData | undefined;
}

const PeriodicReportPanel: React.FC<
  PanelProps<PeriodicReportProps, Settings>
> = ({
  panel,
  PanelRootComponent,
}: PanelProps<PeriodicReportProps, Settings>) => {
  const {
    indiceSettingName,
    isPersisted,
    decision,
    isResultPresent,
    reportResultValue,
    isResultPrevPresent,
    arrowStyle,
    prevReportResultValue,
    availableIndexes,
    errorMessage,
    indiceSetting,
    graphData,
  } = panel.content;
  const settings = panel.settings;

  const [isCommentVisible, setCommentVisible] = React.useState(true);
  const { renderEditButton, renderComment } = useCommentField(
    panel.comment,
    setCommentVisible
  );
  React.useEffect(() => {
    setCommentVisible(!!panel.comment.text);
  }, []);

  const graphParent = React.useRef(null);
  const [graphHeight, setGraphHeight] = React.useState(0);
  React.useEffect(() => {
    if (graphParent.current) {
      setGraphHeight(graphParent.current.clientHeight);
    }
  }, [panel]);

  return (
    <PanelRootComponent
      title={indiceSettingName}
      titlebarButtons={renderEditButton()}
      isTitleDisplay={!isPersisted}
      renderPanelSettings={({
        updateSettings,
        closeSettings,
      }): React.ReactElement => (
        <SettingsForm
          availableIndexes={availableIndexes}
          settings={settings}
          updateSettings={updateSettings}
          closeSettings={closeSettings}
        ></SettingsForm>
      )}
      data-test="periodic-report-panel" // For feature spec
    >
      <PeriodicReportPanelWrapper>
        {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
        {isPersisted && (
          <PeriodicReportPanelContainer>
            {graphData && (
              <PeriodicReportPanelLeft ref={graphParent}>
                <HistoryGraph graphData={graphData} height={graphHeight} />
              </PeriodicReportPanelLeft>
            )}
            <PeriodicReportPanelRight>
              <PanelRootComponent
                title={indiceSettingName}
                titlebarButtons={renderEditButton()}
                renderPanelSettings={({
                  updateSettings,
                  closeSettings,
                }): React.ReactElement => (
                  <SettingsForm
                    availableIndexes={availableIndexes}
                    settings={settings}
                    updateSettings={updateSettings}
                    closeSettings={closeSettings}
                  ></SettingsForm>
                )}
              >
                <PanelRightContentContainer>
                  <IndiceFrame
                    decision={decision}
                    isResultPresent={isResultPresent}
                    reportResultValue={reportResultValue}
                    isResultPrevPresent={isResultPrevPresent}
                    prevReportResultValue={prevReportResultValue}
                    indiceSetting={indiceSetting}
                    arrowStyle={arrowStyle}
                  />
                  <CommentArea visible={isCommentVisible}>
                    {renderComment()}
                  </CommentArea>
                </PanelRightContentContainer>
              </PanelRootComponent>
            </PeriodicReportPanelRight>
          </PeriodicReportPanelContainer>
        )}
      </PeriodicReportPanelWrapper>
    </PanelRootComponent>
  );
};

export default PeriodicReportPanel;
