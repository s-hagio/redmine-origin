import * as React from 'react';
import Highcharts, { SeriesOptionsType } from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { useTranslation } from 'react-i18next';
import ReactDOMServer from 'react-dom/server';

export type HistoryGraphData = {
  xTicks: Array<string>;
  nextReportDay: string;
  series: {
    [key: string]: Array<[string, number | null]>;
  };
};

interface HistoryGraphProps {
  graphData: HistoryGraphData;
  height: number;
}

const assignedColors = {
  nextColorIndex: 0,
};

const getColor = (name: string): string => {
  if (assignedColors[name]) {
    return assignedColors[name];
  }

  const colorsets = [
    'rgb(255, 99, 132)', // red
    'rgb(54, 162, 235)', // blue
    'rgb(75, 192, 192)', // green
    'rgb(255, 159, 64)', // orange
    'rgb(255, 205, 86)', // yellow
    'rgb(153, 102, 255)', // purple
    'rgb(201, 203, 207)', // grey
  ];
  const color =
    colorsets[assignedColors.nextColorIndex] ||
    '#' + Math.floor(Math.random() * 16777215).toString(16);
  assignedColors[name] = color;
  assignedColors.nextColorIndex += 1;
  return color;
};

const HistoryGraph: React.FC<HistoryGraphProps> = ({
  graphData,
  height,
}: HistoryGraphProps) => {
  const seriesData = graphData.series.map((index) => {
    const { name, description, data } = index;
    const convertedData = data.map((n) => {
      const d = new Date(n[0]).getTime();
      let fixDigitNum: number;
      if (n[1] === null) {
        fixDigitNum = n[1];
      } else {
        fixDigitNum = Math.round(n[1] * 100) / 100;
      }
      return [d, fixDigitNum];
    });

    const nameWithTitle = description ? (
      <span title={description}>{name}</span>
    ) : (
      <span>{name}</span>
    );

    return {
      name: ReactDOMServer.renderToString(nameWithTitle),
      data: convertedData,
      color: getColor(name),
    };
  }) as SeriesOptionsType[];

  const tickDates = graphData.xTicks.map((date) => Date.parse(date));

  const { t } = useTranslation();

  const tooltipFormatter = function (): string {
    const time: Highcharts.Time = new Highcharts.Time({});
    const dateFormat: string = t('periodic_report_panel.tooltip_xdate_format');
    const header = time.dateFormat(dateFormat, this.x);
    const symbolTable = {
      circle: '●',
      diamond: '♦',
      square: '■',
      triangle: '▲',
      'triangle-down': '▼',
    };

    const values = this.points
      .map(({ point }) => {
        const symbol = symbolTable[point.series.symbol];
        return `<span style="color:${point.color}; font-family: ui-monospace"> ${symbol}</span> ${point.series.name}: <b>${point.y}</b>`;
      })
      .join('<br/>');
    return header + '<br/>' + values;
  };

  const chartOptions: Highcharts.Options = {
    chart: { type: 'spline', height, marginRight: 30 },
    title: { text: undefined },
    legend: { verticalAlign: 'top', useHTML: true },
    credits: { enabled: false },
    yAxis: {
      title: { text: t('periodic_report_panel.field_history_graph_y') },
    },
    tooltip: {
      formatter: tooltipFormatter,
      shared: true,
      outside: true,
    },
    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: {
        day: t('periodic_report_panel.label_history_graph_xaxis'),
        week: t('periodic_report_panel.label_history_graph_xaxis'),
        month: '%Y-%m',
        year: '%Y',
      },
      crosshair: true,
      title: { text: t('periodic_report_panel.field_history_graph_x') },
      startOfWeek: new Date(tickDates[0]).getDay(),
      gridLineWidth: 1,
      min: tickDates[0],
      max: tickDates[tickDates.length - 1],
    },
    series: seriesData,
  };
  return <HighchartsReact highcharts={Highcharts} options={chartOptions} />;
};

export default HistoryGraph;
