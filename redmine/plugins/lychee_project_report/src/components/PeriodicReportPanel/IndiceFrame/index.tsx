import * as React from "react";
import PageVariables from "../../../pageVariables";
import IndexDecisionIndicator from "../../IndexDecisionIndicator";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import Thresholds from "./Thresholds";

const IndiceFrameArea = styled.div`
  text-align: center;

  .curled-box {
    display: inline-block;
    margin-right: 2em;
    width: 20px;
    height: 20px;
    vertical-align: bottom;
  }

  .curled-box:nth-child(n + 2) {
    margin-left: 1em;
  }
`;

const Result = styled.div`
  height: 24px;
`;

const PreviousResult = styled.div`
  height: 24px;

  div {
    width: 20px;
    height: 20px;
    display: inline-block;
    margin-right: 2em;
    background-repeat: no-repeat;
    background-position: 30%;
  }
`;

const ResultValue = styled.span`
  font-size: large;
`;

const Description = styled.div`
  font-size: larger;
  font-weight: bold;
  margin-bottom: 0.5em;
  padding: 0 5px;
`;

interface IndiceFrameProps {
  decision: string;
  isResultPresent: boolean;
  reportResultValue: string;
  isResultPrevPresent: boolean;
  prevReportResultValue: string;
  indiceSetting: {
    unitString: string;
    biggerBetter: boolean;
    signalDisabled: boolean;
    upperThreshold: number;
    middleThreshold: number;
  };
  arrowStyle: string;
}

const IndiceFrame: React.FC<IndiceFrameProps> = ({
  decision,
  isResultPresent,
  reportResultValue,
  isResultPrevPresent,
  prevReportResultValue,
  indiceSetting,
  arrowStyle
}: IndiceFrameProps) => {
  const { t } = useTranslation();
  return (
    <IndiceFrameArea>
      <Description>
        {t("periodic_report_panel.label_current_and_diff_value")}
      </Description>

      <Result>
        <IndexDecisionIndicator decision={decision} />
        {isResultPresent && (
          <ResultValue className="input">{reportResultValue}</ResultValue>
        )}
      </Result>

      <PreviousResult>
        {isResultPrevPresent && (
          <>
            <div className={`projectReportValueChange ${arrowStyle}`} />
            <ResultValue className="before">
              {prevReportResultValue}
            </ResultValue>
          </>
        )}
      </PreviousResult>

      {indiceSetting.signalDisabled === false && (
        <Thresholds
          isBiggerBetter={indiceSetting.biggerBetter}
          upperValue={indiceSetting.upperThreshold}
          middleValue={indiceSetting.middleThreshold}
          unit={indiceSetting.unitString}
        />
      )}
    </IndiceFrameArea>
  );
};

export default IndiceFrame;
