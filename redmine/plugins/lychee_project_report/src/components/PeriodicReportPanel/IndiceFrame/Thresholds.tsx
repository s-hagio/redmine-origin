import * as React from 'react';
import styled from 'styled-components';

const ThresholdsContainer = styled.div`
  margin: 0.5em;
  white-space: pre-line;
  text-align: center;
`;

const Colors = {
  green: '#8ec778',
  yellow: '#ffe4a2',
  red: '#e4959a',
};

const CurledBox = styled.span<{ color: string }>`
  display: inline-block;
  border-radius: 50%;
  height: 12px;
  width: 12px;
  vertical-align: middle;

  background-color: ${(props): string => Colors[props.color]};
  margin-right: 2px;
  margin-left: 12px;
`;

interface ThresholdsProps {
  isBiggerBetter: boolean;
  upperValue: number;
  middleValue: number;
  unit: string;
}

const Thresholds: React.FC<ThresholdsProps> = ({
  isBiggerBetter,
  upperValue,
  middleValue,
  unit,
}: ThresholdsProps) => {
  if (isBiggerBetter) {
    return (
      <ThresholdsContainer className="thresholds">
        <span data-test="threshold">
          <CurledBox color={'green'} />
          {`〜${upperValue.toLocaleString()}${unit}`}
        </span>
        <span data-test="threshold">
          <CurledBox color={'yellow'} />
          {`${middleValue.toLocaleString()}${unit}〜${upperValue.toLocaleString()}${unit}`}
        </span>
        <span data-test="threshold">
          <CurledBox color={'red'} />
          {`${middleValue.toLocaleString()}${unit}〜`}
        </span>
      </ThresholdsContainer>
    );
  } else {
    return (
      <ThresholdsContainer className="thresholds">
        <span data-test="threshold">
          <CurledBox color={'green'} />
          {`〜${middleValue.toLocaleString()}${unit}`}
        </span>
        <span data-test="threshold">
          <CurledBox color={'yellow'} />
          {`${middleValue.toLocaleString()}${unit}〜${upperValue.toLocaleString()}${unit}`}
        </span>
        <span data-test="threshold">
          <CurledBox color={'red'} />
          {`${upperValue.toLocaleString()}${unit}〜`}
        </span>
      </ThresholdsContainer>
    );
  }
};

export default Thresholds;
