import * as React from 'react';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import CheckboxGroup from '../atoms/CheckboxGroup';

const Select = styled.select`
  width: auto;
  max-width: 300px;
  text-overflow: ellipsis;
`;

const Input = styled.input`
  width: 60px;
`;

export interface Settings {
  indexId: number | null;
  visibleSeriesIds: Array<number>;
  months: number;
}

interface SettingsFormProps {
  settings: Settings;
  closeSettings: () => void;
  updateSettings: (settings: Settings) => void;
  availableIndexes: Array<{ id: number; name: string }>;
}

const SettingsForm: React.FC<SettingsFormProps> = ({
  closeSettings,
  updateSettings,
  settings,
  availableIndexes,
}: SettingsFormProps) => {
  const { t } = useTranslation();
  const [formValues, setFormValues] = React.useState(settings);

  const options = [
    <option key="-1" value="">
      {t('select_indice')}
    </option>,
  ].concat(
    availableIndexes.map((obj, i) => (
      <option key={`${i}`} value={obj.id}>
        {obj.name}
      </option>
    ))
  );

  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const { value } = event.target;
    if (value !== '') {
      setFormValues({
        ...formValues,
        indexId: parseInt(value, 10),
      });
    }
  };

  const submit = (): void => {
    updateSettings(formValues);
    toast(t('save_setting_alert'));
    closeSettings();
  };

  return (
    <form onSubmit={submit}>
      <label>{t('periodic_report_panel.label_panel_indice')}:</label>
      <div>
        <Select value={formValues.indexId || ''} onChange={handleChange}>
          {options}
        </Select>
      </div>
      <div>
        <label>
          {t('periodic_report_panel.label_displayInGraph_indices_setting')}:
        </label>
        <CheckboxGroup
          values={availableIndexes}
          checked={formValues.visibleSeriesIds}
          onChange={(newIds): void =>
            setFormValues({ ...formValues, visibleSeriesIds: newIds })
          }
        />
      </div>
      <div>
        <label>{t('periodic_report_panel.label_last')}</label>
        <Input
          type="number"
          min="1"
          max="12"
          value={formValues.months || 1}
          required
          onChange={(n): void =>
            setFormValues({ ...formValues, months: parseInt(n.target.value) })
          }
        />
        <label>{t('periodic_report_panel.label_months')}</label>
      </div>
      <div>
        {/* disabled な時の見た目がわかりにくい */}
        <button disabled={!formValues.indexId}>{t('button_save')}</button>
        <button onClick={closeSettings}>{t('button_cancel')}</button>
      </div>
    </form>
  );
};

export default SettingsForm;
