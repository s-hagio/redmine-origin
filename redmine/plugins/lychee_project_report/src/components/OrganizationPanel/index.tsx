import * as React from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import { PanelProps } from "../../interfaces/interface";
import SettingsForm, { OrganizationPanelSettings } from "./SettingsForm";

const OrganizationTable = styled.table`
  width: 100%;
  table-layout: fixed;
  margin-top: 5px;
  td {
    border-bottom: 2px solid;
    border-color: #e4e4e4;
    text-align: left;
    white-space: pre-wrap;
    overflow: hidden;
    padding-left: 15px;
    align-items: center;
    word-wrap: break-word;
    overflow-wrap: break-word;
  }
  tr:first-child > td {
    border-color: #bcbcbc;
  }
  tr:nth-child(2n + 2) {
    background-color: #fcfcfc;
  }
`;

type OrganizationContentProps = Partial<{
  availableProjectCustomFields: Array<{
    id: number;
    name: string;
  }>;
  availableUserCustomFields: Array<{
    id: number;
    name: string;
  }>;
  displayedUserCustomFields: Array<{
    id: number;
    name: string;
  }>;
  tableData: Array<[string]>;
}>;

const OrganizationPanel: React.FC<PanelProps<
  OrganizationContentProps,
  OrganizationPanelSettings
>> = ({
  panel,
  PanelRootComponent
}: PanelProps<OrganizationContentProps, OrganizationPanelSettings>) => {
  const { t } = useTranslation();
  const {
    availableProjectCustomFields,
    availableUserCustomFields,
    displayedUserCustomFields,
    tableData
  } = panel.content;
  const { title } = panel.settings;

  return (
    <PanelRootComponent
      title={title}
      renderPanelSettings={({
        updateSettings,
        closeSettings
      }): React.ReactElement => (
        <SettingsForm
          settings={panel.settings}
          availableProjectCustomFields={availableProjectCustomFields}
          availableUserCustomFields={availableUserCustomFields}
          updateSettings={updateSettings}
          closeSettings={closeSettings}
        ></SettingsForm>
      )}
      data-test="organization-panel" // For feature spec
    >
      <OrganizationTable>
        <tbody>
          <tr>
            <td>{t("organization_panel.label.role")}</td>
            {displayedUserCustomFields &&
              displayedUserCustomFields.map(i => (
                <React.Fragment key={`${i.id}`}>
                  <td>{i.name}</td>
                </React.Fragment>
              ))}
            <td>{t("organization_panel.label.name")}</td>
          </tr>
          {tableData.map(t => (
            <React.Fragment key={`${t}`}>
              <tr>
                {t.map(m => (
                  <React.Fragment key={`${m}`}>
                    <td>{m}</td>
                  </React.Fragment>
                ))}
              </tr>
            </React.Fragment>
          ))}
        </tbody>
      </OrganizationTable>
    </PanelRootComponent>
  );
};

export default OrganizationPanel;
