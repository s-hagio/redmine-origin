import * as React from "react";
import { toast } from "react-toastify";
import { useTranslation } from "react-i18next";
import { useTranslationWithPrefix } from "../../i18n";
import CheckboxGroup from "../atoms/CheckboxGroup";

export interface OrganizationPanelSettings {
  title: string;
  visibleCustomFieldIds: Array<number>;
}

interface SettingsFormProps {
  settings: OrganizationPanelSettings;
  availableProjectCustomFields: Array<{
    id: number;
    name: string;
  }>;
  availableUserCustomFields: Array<{
    id: number;
    name: string;
  }>;
  closeSettings: () => void;
  updateSettings: (settings: OrganizationPanelSettings) => void;
}

const SettingsForm: React.FC<SettingsFormProps> = ({
  closeSettings,
  updateSettings,
  availableProjectCustomFields,
  availableUserCustomFields,
  settings
}: SettingsFormProps) => {
  const { t } = useTranslationWithPrefix("organization_panel.settings");

  const [formValues, setFormValues] = React.useState(settings);

  const { visibleCustomFieldIds } = formValues;

  const message = useTranslation().t("save_setting_alert");
  const submit = (): void => {
    updateSettings(formValues);
    toast(message);
    closeSettings();
  };

  return (
    <div>
      <label>{t("label_project_customField")}:</label>
      <CheckboxGroup
        values={availableProjectCustomFields}
        checked={visibleCustomFieldIds}
        onChange={(newIds): void =>
          setFormValues({
            ...formValues,
            visibleCustomFieldIds: newIds
          })
        }
      />
      <label>{t("label_user_customField")}:</label>
      <CheckboxGroup
        values={availableUserCustomFields}
        checked={visibleCustomFieldIds}
        onChange={(newIds): void =>
          setFormValues({
            ...formValues,
            visibleCustomFieldIds: newIds
          })
        }
      />
      <div>
        <button onClick={submit}>{useTranslation().t("button_save")}</button>
        <button onClick={closeSettings}>{useTranslation().t("button_cancel")}</button>
      </div>
    </div>
  );
};

export default SettingsForm;
