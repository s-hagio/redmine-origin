import * as React from 'react';
import styled from 'styled-components';
import { TinyColor } from '@ctrl/tinycolor';

interface ColorProps {
  color: string;
}

const Root = styled.span`
  display: inline-flex;
  span {
    display: flex;
    align-items: center;
    box-sizing: border-box;
  }
`;

const Label = styled.span`
  color: white;
  width: 60%;
  background-color: ${(props: ColorProps): string => props.color};
  padding: 0.1em 5px;
  overflow-x: hidden;
  text-overflow: ellipsis;
  font-weight: bold;
  font-size: 1.1em;
`;

const Value = styled.span`
  width: 40%;
  color: #000;
  background-color: ${(props: ColorProps): string => props.color};
  text-align: right;
  padding: 0.5em 5px;
  font-size: 1.2em;
  font-weight: bold;
`;

const EllipsisLabel = styled.div`
  width: 100%;
  overflow-x: hidden;
  text-overflow: ellipsis;
`;

const weakenColor = (color: string): string => {
  const wrappedColor = new TinyColor(color);

  // Brightness 180 == White so bright max to 170
  const brightenBy = Math.min(50, 170 - wrappedColor.getBrightness());

  return wrappedColor
    .brighten(brightenBy)
    .desaturate(30)
    .setAlpha(0.4)
    .toString() as string;
};

interface SingleValueProps {
  className?: string;
  color: string;
  label: string;
  children: React.ReactNode;
}

const SingleValue: React.FC<SingleValueProps> = ({
  className,
  color,
  label,
  children,
}: SingleValueProps) => {
  return (
    <Root className={className}>
      <Label color={color} title={label}>
        <EllipsisLabel>{label}</EllipsisLabel>
      </Label>
      <Value color={weakenColor(color)}>
        <EllipsisLabel>{children}</EllipsisLabel>
      </Value>
    </Root>
  );
};

export default SingleValue;
