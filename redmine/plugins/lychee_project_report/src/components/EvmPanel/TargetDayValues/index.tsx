import * as React from 'react';
import styled from 'styled-components';
import { usingLycheeCost } from '..';
import { GraphData, GRAPH_COLUMNS } from '../Graph';
import { YAxisUnit, convertValueForYAxis } from '../SettingsForm';
import SingleValue from './SingleValue';
import { TFunction } from 'i18next';

const Root = styled.div`
  display: flex;
  flex-wrap: nowrap;
  width: 100%;
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const EvmValue = styled(SingleValue)`
  flex: auto;
  width: 98%;
  margin: 1.2px;
  height: 25px;
`;

const COLUMNS = [
  'plannedValue',
  'budgetAtCompletion',
  'earnedValue',
  'estimatedAtCompletion',
  'actualCost',
  'spi',
  'cpi',
];

const color = (column: string): string => {
  if (
    column === 'spi' ||
    column === 'cpi' ||
    column === 'sv' ||
    column === 'cv'
  ) {
    return '#404040';
  }

  return GRAPH_COLUMNS[column].color;
};

interface TargetDayValuesProps {
  graphData: GraphData;
  yAxisUnit: YAxisUnit;
  selectedDate: string;
  t: TFunction;
}

const valueAtDate = (
  graphData: GraphData,
  column: string,
  date: string
): number | null => {
  const dataColumn = column === 'estimatedAtCompletion' ? 'allEacs' : column;
  const pointAtDate = graphData.data[dataColumn].data.find(
    (point) => point[0] == date
  );

  return pointAtDate ? pointAtDate[1] : null;
};

const formatValue = (value: number): string =>
  value.toLocaleString(undefined, {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });

const valueFor = (
  graphData: GraphData,
  column: string,
  yAxisUnit: YAxisUnit,
  selectedDate: string
): string => {
  const value = valueAtDate(graphData, column, selectedDate);
  // TODO: EVMで直す
  if (
    value === null ||
    (column == 'estimatedAtCompletion' &&
      selectedDate > graphData.dates.targetDate)
  ) {
    return '-';
  }

  if (['spi', 'cpi'].includes(column)) {
    return formatValue(value);
  }

  return usingLycheeCost(graphData)
    ? formatValue(value / 1000)
    : formatValue(convertValueForYAxis(value, yAxisUnit));
};

const floatValue = (value: string): number =>
  parseFloat(value.replace(/,/g, ''));

const subtractionValue = (
  graphData: GraphData,
  fromColumn: string,
  subtractColumn: string,
  yAxisUnit: YAxisUnit,
  selectedDate: string
): string => {
  const fromValue = floatValue(
    valueFor(graphData, fromColumn, yAxisUnit, selectedDate)
  );
  const subtractValue = floatValue(
    valueFor(graphData, subtractColumn, yAxisUnit, selectedDate)
  );

  if (isNaN(fromValue) || isNaN(subtractValue)) {
    return '-';
  }

  return formatValue(fromValue - subtractValue);
};

const TargetDayValues: React.FC<TargetDayValuesProps> = ({
  graphData,
  yAxisUnit,
  selectedDate,
  t,
}: TargetDayValuesProps) => {
  const availableColumns = React.useMemo(
    () => COLUMNS.filter((column) => graphData.data[column]),
    [graphData]
  );

  const withLycheeCost = usingLycheeCost(graphData);
  const columnStyle = { width: withLycheeCost ? '25%' : '33.3%' };

  return (
    <Root>
      {withLycheeCost && (
        <Column style={columnStyle}>
          {['budget', 'budgetSurplus']
            .filter((column) => graphData.data[column])
            .map((column) => (
              <EvmValue
                key={column}
                color={color(column)}
                label={graphData.data[column].label}
              >
                {valueFor(graphData, column, yAxisUnit, selectedDate)}
              </EvmValue>
            ))}
        </Column>
      )}
      {[
        ['budgetAtCompletion', 'estimatedAtCompletion'],
        ['plannedValue', 'earnedValue', 'actualCost'],
        ['spi', 'cpi'],
      ].map((columns) => (
        <Column key={columns.join(',')} style={columnStyle}>
          {availableColumns
            .filter((column) => columns.includes(column))
            .map((column) => (
              <>
                <EvmValue
                  key={column}
                  color={color(column)}
                  label={graphData.data[column].label}
                >
                  {valueFor(graphData, column, yAxisUnit, selectedDate)}
                </EvmValue>
                {column === 'spi' && (
                  <EvmValue
                    key="sv"
                    color={color('sv')}
                    label={t('evm_panel.target_day_values.label_sv')}
                  >
                    {subtractionValue(
                      graphData,
                      'earnedValue',
                      'plannedValue',
                      yAxisUnit,
                      selectedDate
                    )}
                  </EvmValue>
                )}
                {column === 'cpi' && (
                  <EvmValue
                    key="cv"
                    color={color('cv')}
                    label={t('evm_panel.target_day_values.label_cv')}
                  >
                    {subtractionValue(
                      graphData,
                      'earnedValue',
                      'actualCost',
                      yAxisUnit,
                      selectedDate
                    )}
                  </EvmValue>
                )}
              </>
            ))}
        </Column>
      ))}
    </Root>
  );
};

export default TargetDayValues;
