import * as React from "react";
import * as Highcharts from "highcharts";
import { useTranslationWithPrefix } from "../../i18n";
import Graph, { GraphProps } from "./Graph";

const round = (num: number): string => {
  if (num < 500) {
    return "0";
  }
  if (num < 1000) {
    return "1";
  }

  return Highcharts.numberFormat(Math.round(num / 1000), -1, undefined, ",");
};

const merge = (originalObject: object, addition: object): object => ({
  ...(originalObject || {}),
  ...addition
});

const LycheeCostGraph: React.FC<GraphProps> = ({
  data,
  height,
  setSelectedDate
}: GraphProps) => {
  const { t } = useTranslationWithPrefix("evm_panel.graph");

  const tooltipFormatter = function(): string {
    const formatDate = (date: string): string =>
      new Date(date).toLocaleDateString();
    const header = `<span style="font-size:10px;">${formatDate(this.x)}</span>`;
    const costUnit = `<span style="font-size:10px; position: absolute; right: 0;">${t(
      "unit.lychee_cost"
    )}</span>`;
    const values = this.points
      .map(({ color, series, point }) => {
        const bullet = `<strong style="color: ${color}"> &bull; </strong>`;
        const label = `<span>${series.name}: </span>`;
        const value = `<strong>${round(point.y)}</strong>`;

        return "<br />" + bullet + label + value;
      })
      .join("");
    return header + costUnit + values;
  };

  const modifyOptions = (options: Highcharts.Options): void => {
    options.chart = merge(options.chart, { marginLeft: 90 }); // Y軸のタイトルとTickラベル(例: 1500百万)が表示できる領域を確保
    options.tooltip = merge(options.tooltip, { formatter: tooltipFormatter });
    const yAxis = options.yAxis as Highcharts.YAxisOptions;
    yAxis.title = merge(yAxis.title, {
      text: t("unit.lychee_cost")
    });
    yAxis.labels = merge(yAxis.labels, {
      formatter: function(): string {
        return round(this.value);
      }
    });
  };

  return (
    <Graph
      data={data}
      height={height}
      modifyOptions={modifyOptions}
      setSelectedDate={setSelectedDate}
    />
  );
};

export default LycheeCostGraph;
