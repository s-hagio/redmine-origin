import { TFunction } from 'i18next';
import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import styled from 'styled-components';
import find from 'lodash/find';

import { getResource } from '../../api';
import { PanelProps } from '../../interfaces/interface';
import PageVariables from '../../pageVariables';
import LoadSpinnerPopup from '../atoms/LoadSpinnerPopup';
import Spacer from '../atoms/Spacer';
import { useCommentField } from '../CommentField';
import CommentFrame from '../CommentFrame';
import DateDisplay from './DateDisplay';
import Filter, { FilterValues } from './Filter';
import Graph, { GraphData } from './Graph';
import LycheeCostGraph from './LycheeCostGraph';
import SettingsForm, {
  EvmPanelSettings,
  VersionData,
  MilestoneData,
} from './SettingsForm';
import TargetDayValues from './TargetDayValues';
import { getEvmFilter } from '../../localstorage';

const borderStyle = '1px solid #d5d5d5';
const VALUES_HEIGHT = 128;

const EvmWrapper = styled.div`
  flex-grow: 1;
  height: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
`;

const LeftColumn = styled.div`
  display: inline-flex;
  flex-direction: column;
  justify-items: stretch;
  box-sizing: border-box;
  width: 75%;
  border-right: ${borderStyle};
  height: 100%;
`;

const RightColumn = styled.div`
  width: 25%;
  height: 100%;
  box-sizing: border-box;
  display: inline-flex;
  flex-direction: column;

  & > * {
    min-height: 0;
  }
`;

const HeaderRow = styled.div`
  display: flex;
  width: 100%;
  line-height: 2em;
  padding: 5px 0px;
  border-bottom: ${borderStyle};
  box-sizing: border-box;
`;

const LabelArea = styled.div`
  width: 100%;
  margin: 0 3px;
`;

const UnitArea = styled.span`
  margin-left: 10px;
`;

const Content = styled.div`
  display: flex;
  width: 100%;
  height: calc(100% - 2em - 10px - 1px);
  box-sizing: border-box;
`;

const Values = styled.div`
  height: ${VALUES_HEIGHT}px;
  border-top: ${borderStyle};
  padding: 5px 10px 5px 0px;
  box-sizing: border-box;
`;

const FilterArea = styled(Filter)`
  flex: 1 0 auto;
`;

const CommentArea = styled(CommentFrame)`
  background-color: #fff;
  border-top: 2px solid #ededed;
  margin-top: 4px;
  display: flex;
  min-height: 0; // オーバーフローを防ぐため
  height: 100%; // IE対応
  bottom: 0;
  position: relative;
  width: 100%;
`;

type EvmApiParams = {
  'evm_targets[]'?: Array<string>;
  'evm_milestones[]'?: Array<string>;
  evm_date1?: string;
  evm_unit: string;
  eac_method: string;
  evm_date2: string;
};

// EVM側のリクエストパラメータがsnake case
const getEvmData = (
  projectId: number,
  targetDate: string,
  { type, xAxisUnit, eacMethod, targetDateFormer, valueUnit }: EvmPanelSettings,
  filterValues: FilterValues
): Promise<GraphData> => {
  const params: EvmApiParams = {
    evm_date2: targetDate,
    evm_unit: xAxisUnit,
    eac_method: eacMethod,
  };
  if (type === 'comparison') {
    params.evm_date1 = targetDateFormer;
  }
  if (filterValues.targetVersions) {
    params['evm_targets[]'] = filterValues.targetVersions
      .filter((o) => o)
      .map((o) => o.value);
  }
  if (filterValues.targetMilestones) {
    params['evm_milestones[]'] = filterValues.targetMilestones.map(
      (o) => o.value
    );
  }
  if (filterValues.valueUnit || valueUnit) {
    params['value_unit'] = filterValues.valueUnit || valueUnit;
  }
  return getResource<GraphData>(
    `projects/${projectId}/${type}_chart.json`,
    params
  );
};

const getEvmVersions = async (projectId: number): Promise<VersionData> =>
  getResource<VersionData>(`projects/${projectId}/evm_targets.json`);

const getEvmMilestones = (
  projectId: number,
  targetDate: string,
  { xAxisUnit }: EvmPanelSettings
): Promise<MilestoneData> => {
  const params: {
    evm_unit: string;
    evm_date2: string;
  } = {
    evm_date2: targetDate,
    evm_unit: xAxisUnit,
  };
  return getResource<MilestoneData>(
    `projects/${projectId}/milestones.json`,
    params
  );
};

const fixDatesIfNecessary = (
  settings: EvmPanelSettings,
  graphData: GraphData,
  t: TFunction,
  updateSettings: (settings: EvmPanelSettings) => void
): void => {
  if (!settings.targetDateFormer) {
    return;
  }

  if (
    PageVariables.permissions.editLayout &&
    graphData.dates.targetDateFormer &&
    settings.targetDateFormer !== graphData.dates.targetDateFormer
  ) {
    updateSettings({
      ...settings,
      targetDateFormer: graphData.dates.targetDateFormer,
    });
    toast.info(
      t('evm_panel.former_date_corrected', { date: settings.targetDateFormer })
    );
  }
};

export const usingLycheeCost = (graphData: GraphData): boolean =>
  graphData && !!graphData.data.budget;

type EvmContentProps = Partial<{
  targetDate: string;
  projectId: number;
  plannedCompletionDate: string;
  isLgcAvailable: boolean;
  isLycheeCostAvailable: boolean;
}>;

const EvmPanel: React.FC<PanelProps<EvmContentProps, EvmPanelSettings>> = ({
  panel,
  PanelRootComponent,
  updateSettings,
}: PanelProps<EvmContentProps, EvmPanelSettings>) => {
  const { t } = useTranslation();
  const {
    targetDate,
    projectId,
    plannedCompletionDate,
    isLgcAvailable,
    isLycheeCostAvailable,
  } = panel.content;
  const { title, yAxisUnit, targetMilestones, targetVersions } = panel.settings;

  const [loading, setLoading] = React.useState(true);
  const [graphData, setGraphData] = React.useState<GraphData>(null);
  const [selectedDate, setSelectedDate] = React.useState<string>(null);
  const [loadError, setLoadError] = React.useState<string>(null);
  const [available, setAvailable] = React.useState<{
    versions: VersionData;
    milestones: MilestoneData;
  }>(null);
  const [filterValues, setFilterValues] = React.useState<FilterValues>(null);
  const [
    actualFilterValues,
    setActualFilterValues,
  ] = React.useState<FilterValues>({});

  const handleFilterUpdate = React.useCallback((values: FilterValues) => {
    setFilterValues(values);
    setActualFilterValues(values);
  }, []);

  React.useEffect(() => {
    if (available) {
      return;
    }
    const loadData = async (): Promise<void> => {
      setLoading(true);
      const loadedData = { versions: [], milestones: [] };
      const loadVersions = async (): Promise<void> => {
        loadedData.versions = await getEvmVersions(projectId);
      };
      const loadMilestones = async (): Promise<void> => {
        if (isLgcAvailable) {
          loadedData.milestones = await getEvmMilestones(
            projectId,
            targetDate,
            panel.settings
          );
        }
      };
      const formValues = getEvmFilter(projectId);
      await Promise.all([loadVersions(), loadMilestones()]);
      setAvailable(loadedData);
      setFilterValues({
        projectId: projectId,
        targetVersions: formValues ? formValues.targetVersions : targetVersions,
        targetMilestones:
          isLgcAvailable &&
          (formValues
            ? formValues.targetMilestones
            : PageVariables.currentLayoutShared
            ? [loadedData.milestones[0]]
            : targetMilestones),
        valueUnit: formValues?.valueUnit,
      });
      setLoading(false);
    };

    loadData();
  }, [
    available,
    filterValues,
    isLgcAvailable,
    panel.settings,
    projectId,
    targetDate,
    targetMilestones,
    targetVersions,
  ]);

  React.useEffect(() => {
    if (!filterValues) {
      return;
    }

    const loadEvm = async (): Promise<void> => {
      setLoading(true);
      const evmData = await getEvmData(
        projectId,
        targetDate,
        panel.settings,
        filterValues
      );
      const { targetIds, error, message } = evmData;
      if (targetIds) {
        if (message !== '') {
          toast.info(message);
        }
        setLoadError(null);
        setGraphData(evmData);
        setActualFilterValues({
          ...filterValues,
          targetVersions: targetIds.map((value) =>
            find(available.versions, { value })
          ),
          valueUnit: filterValues.valueUnit || panel.settings?.valueUnit,
        });
        fixDatesIfNecessary(panel.settings, evmData, t, updateSettings);
      } else {
        if (!loadError) {
          toast.info(error);
          setLoadError(error);
        }
      }
      setLoading(false);
    };

    loadEvm();
  }, [
    available,
    loadError,
    filterValues,
    isLgcAvailable,
    panel.settings,
    projectId,
    t,
    targetDate,
    updateSettings,
  ]);

  const leftColumn = React.useRef(null);
  const [graphHeight, setGraphHeight] = React.useState<number>(0);

  React.useEffect(() => {
    if (!loadError && leftColumn.current) {
      setGraphHeight(leftColumn.current.clientHeight - VALUES_HEIGHT);
      setSelectedDate(graphData.dates.targetDate);
    }
  }, [loadError, graphData]);

  const [isCommentVisible, setCommentVisible] = React.useState(true);
  const { renderEditButton, renderComment } = useCommentField(
    panel.comment,
    setCommentVisible
  );
  React.useEffect(() => {
    setCommentVisible(!!panel.comment.text);
  }, [panel.comment.text]);

  const GraphComponent = usingLycheeCost(graphData) ? LycheeCostGraph : Graph;

  const renderPanelSettings = ({
    updateSettings,
    closeSettings,
  }): React.ReactElement => (
    <SettingsForm
      settings={panel.settings}
      updateSettings={(values): void => {
        updateSettings(values);
        if (!PageVariables.currentLayoutShared) {
          handleFilterUpdate({
            projectId: projectId,
            targetVersions: values.targetVersions,
            targetMilestones: values.targetMilestones,
            valueUnit: values.valueUnit,
          });
        }
      }}
      closeSettings={closeSettings}
      versions={available.versions}
      isLgcAvailable={isLgcAvailable}
      isLycheeCostAvailable={isLycheeCostAvailable}
      milestones={available.milestones}
    ></SettingsForm>
  );

  const renderRightColumn = (): React.ReactElement => (
    <RightColumn>
      <FilterArea
        values={actualFilterValues}
        onChange={handleFilterUpdate}
        versions={available.versions}
        milestones={available.milestones}
        isLgcAvailable={isLgcAvailable}
        isLycheeCostAvailable={isLycheeCostAvailable}
      />
      <CommentArea visible={isCommentVisible}>{renderComment()}</CommentArea>
    </RightColumn>
  );

  if (loadError) {
    return (
      <PanelRootComponent
        title={title || 'EVM'}
        titlebarButtons={renderEditButton()}
        renderPanelSettings={renderPanelSettings}
        data-test="evm-panel"
      >
        <Content>
          <LeftColumn ref={leftColumn}></LeftColumn>
          {renderRightColumn()}
        </Content>
      </PanelRootComponent>
    );
  }

  return (
    <PanelRootComponent
      title={title || 'EVM'}
      titlebarButtons={renderEditButton()}
      renderPanelSettings={renderPanelSettings}
      data-test="evm-panel" // For feature spec
    >
      <EvmWrapper>
        {graphData && (
          <>
            <HeaderRow>
              <DateDisplay
                color="#dbe4f5"
                label={t('evm_panel.header.target_date')}
              >
                {graphData.dates.targetDate}
              </DateDisplay>
              <Spacer />
              <DateDisplay
                color="#e2fadd"
                label={t('evm_panel.header.planned_completion_date')}
              >
                {plannedCompletionDate}
              </DateDisplay>
              <DateDisplay
                color="#fdd9b7"
                label={t('evm_panel.header.estimated_completion_date')}
              >
                {graphData.dates.estimatedCompletionDate}
              </DateDisplay>
              <RightColumn />
            </HeaderRow>
            <Content>
              <LeftColumn ref={leftColumn}>
                <GraphComponent
                  data={graphData}
                  height={graphHeight}
                  yAxisUnit={yAxisUnit}
                  setSelectedDate={setSelectedDate}
                />
                <Values>
                  <LabelArea>
                    {panel.settings.type === 'comparison' ? (
                      <span>
                        {selectedDate}(
                        {t('evm_panel.header.label_base_date_current')})
                      </span>
                    ) : (
                      <span>{selectedDate}</span>
                    )}
                    {usingLycheeCost(graphData) && (
                      <UnitArea>
                        {t('evm_panel.header.label_cost_unit')}
                      </UnitArea>
                    )}
                  </LabelArea>
                  {
                    <TargetDayValues
                      graphData={graphData}
                      yAxisUnit={yAxisUnit}
                      selectedDate={selectedDate}
                      t={t}
                    />
                  }
                </Values>
              </LeftColumn>
              {renderRightColumn()}
            </Content>
          </>
        )}
        {loading && <LoadSpinnerPopup />}
      </EvmWrapper>
    </PanelRootComponent>
  );
};

export default EvmPanel;
