import * as React from "react";
import styled from "styled-components";

const Root = styled.span`
  margin: 0 15px;
`;

interface ColoredLabelProps {
  color: string;
}

const ColoredLabel = styled.span`
  background-color: ${(props: ColoredLabelProps): string => props.color};
  border: 1px solid ${(props: ColoredLabelProps): string => props.color};
  border-radius: 10px;
  padding: 0.25em 10px;
  margin-right: 0.5em;
`;

interface DateDisplayProps {
  color: string;
  label: string;
  children: React.ReactNode;
}

const DateDisplay: React.FC<DateDisplayProps> = ({
  color,
  label,
  children
}: DateDisplayProps) => (
  <Root>
    <ColoredLabel color={color}>{label}</ColoredLabel>
    {children}
  </Root>
);

export default DateDisplay;
