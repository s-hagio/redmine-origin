import * as React from 'react';
import { toast } from 'react-toastify';
import DateField from '../atoms/DateField';
import FieldLabel from '../atoms/FieldLabel';
import MultiSelect, { SelectStyles } from '../atoms/MultiSelect';
import { useTranslation } from 'react-i18next';
import { useTranslationWithPrefix } from '../../i18n';
import PageVariables from '../../pageVariables';

const GRAPH_TYPE = ['line', 'comparison'] as const;
type GraphType = typeof GRAPH_TYPE[number];

const X_AXIS_UNIT = ['daily', 'weekly', 'monthly'] as const;
type XAxisUnit = typeof X_AXIS_UNIT[number];

const Y_AXIS_UNIT = ['hours', 'days', 'months'] as const;
export type YAxisUnit = typeof Y_AXIS_UNIT[number];

export const VALUE_UNIT = ['costs', 'hours'] as const;
export type ValueUnit = typeof VALUE_UNIT[number];

export const convertValueForYAxis = (
  value: number,
  yAxisUnit: YAxisUnit
): number => {
  if (!value) {
    return value;
  }

  switch (yAxisUnit) {
    case 'days':
      return value / 8.0;
    case 'months':
      return value / 160.0;
    default:
      return value;
  }
};

const EAC_METHOD = ['optimistic', 'intermediate', 'pessimistic'] as const;
type EacMethod = typeof EAC_METHOD[number];

export interface EvmPanelSettings {
  title: string;
  type: GraphType;
  targetVersions: VersionData;
  targetMilestones: MilestoneData;
  targetDateFormer?: string;
  xAxisUnit: XAxisUnit;
  yAxisUnit: YAxisUnit;
  eacMethod: EacMethod;
  valueUnit: ValueUnit;
}

export type VersionData = Array<{ value: string; label: string }>;
export type MilestoneData = Array<{ value: string; label: string }>;

export const buildSelect = ({
  options,
  selected,
  onChange,
  getLabel,
}: {
  options: readonly string[];
  selected: string;
  onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void;
  getLabel: (value: string) => string;
}): React.ReactElement => (
  <select value={selected} onChange={onChange}>
    {options.map((v: string) => (
      <option key={v} value={v}>
        {getLabel(v)}
      </option>
    ))}
  </select>
);

const multiSelectStyles: SelectStyles = {
  container: (styles) => ({ ...styles, minWidth: '300px' }),
};

interface SettingsFormProps {
  settings: EvmPanelSettings;
  closeSettings: () => void;
  updateSettings: (settings: EvmPanelSettings) => void;
  versions: VersionData;
  isLgcAvailable: boolean;
  isLycheeCostAvailable: boolean;
  milestones: MilestoneData;
}

const SettingsForm: React.FC<SettingsFormProps> = ({
  closeSettings,
  updateSettings,
  settings,
  versions,
  isLgcAvailable,
  isLycheeCostAvailable,
  milestones,
}: SettingsFormProps) => {
  const { t } = useTranslationWithPrefix('evm_panel');

  const [formValues, setFormValues] = React.useState(settings);

  const message = useTranslation().t('save_setting_alert');
  const submit = (): void => {
    updateSettings(formValues);
    toast(message);
    closeSettings();
  };

  return (
    <div>
      <div>
        <FieldLabel>{t('settings.label_title')}</FieldLabel>
        <input
          value={formValues.title}
          onChange={(ev): void =>
            setFormValues({ ...formValues, title: ev.target.value })
          }
        />
      </div>
      <div>
        <FieldLabel>{t('settings.label_type')}</FieldLabel>
        {buildSelect({
          options: GRAPH_TYPE,
          selected: formValues.type,
          onChange: (ev): void =>
            setFormValues({
              ...formValues,
              type: ev.target.value as GraphType,
            }),
          getLabel: (value): string => t(`settings.type.${value}`),
        })}
      </div>
      {!PageVariables.currentLayoutShared && (
        <>
          <div>
            <FieldLabel>{t('settings.label_version')}</FieldLabel>
            <MultiSelect
              options={versions}
              values={formValues.targetVersions}
              onChange={(selectedOptions): void =>
                setFormValues({
                  ...formValues,
                  targetVersions: selectedOptions as VersionData,
                })
              }
              selectStyles={multiSelectStyles}
            />
          </div>
          {isLgcAvailable && (
            <div>
              <FieldLabel>{t('settings.label_milestone')}</FieldLabel>
              <MultiSelect
                options={milestones}
                values={formValues.targetMilestones}
                onChange={(selectedOptions): void =>
                  setFormValues({
                    ...formValues,
                    targetMilestones: selectedOptions as MilestoneData,
                  })
                }
                selectStyles={multiSelectStyles}
              />
            </div>
          )}
        </>
      )}
      <div>
        <FieldLabel>{t('settings.label_x_axis_unit')}</FieldLabel>
        {buildSelect({
          options: X_AXIS_UNIT,
          selected: formValues.xAxisUnit,
          onChange: (ev): void =>
            setFormValues({
              ...formValues,
              xAxisUnit: ev.target.value as XAxisUnit,
            }),
          getLabel: (value): string => t(`settings.x_axis_unit.${value}`),
        })}
      </div>
      <div>
        <FieldLabel>{t('settings.label_y_axis_unit')}</FieldLabel>
        {buildSelect({
          options: Y_AXIS_UNIT,
          selected: formValues.yAxisUnit,
          onChange: (ev): void =>
            setFormValues({
              ...formValues,
              yAxisUnit: ev.target.value as YAxisUnit,
            }),
          getLabel: (value): string => t(`graph.unit.${value}`),
        })}
      </div>
      <div>
        <FieldLabel>{t('settings.label_eac_method')}</FieldLabel>
        {buildSelect({
          options: EAC_METHOD,
          selected: formValues.eacMethod,
          onChange: (ev): void =>
            setFormValues({
              ...formValues,
              eacMethod: ev.target.value as EacMethod,
            }),
          getLabel: (value): string => t(`settings.eac_method.${value}`),
        })}
      </div>
      {isLycheeCostAvailable && (
        <div>
          <FieldLabel>{t('settings.label_value_unit')}</FieldLabel>
          {buildSelect({
            options: VALUE_UNIT,
            selected: formValues.valueUnit,
            onChange: (ev): void =>
              setFormValues({
                ...formValues,
                valueUnit: ev.target.value as ValueUnit,
              }),
            getLabel: (value): string => t(`settings.value_unit.${value}`),
          })}
        </div>
      )}
      {formValues.type === 'comparison' && (
        <div>
          <FieldLabel required>
            {t('settings.label_target_date_former')}
          </FieldLabel>
          <DateField
            value={formValues.targetDateFormer}
            onChange={(targetDateFormer): void =>
              setFormValues({
                ...formValues,
                targetDateFormer,
              })
            }
          />
        </div>
      )}
      <div>
        <button
          onClick={submit}
          disabled={
            formValues.type === 'comparison' && !formValues.targetDateFormer
          }
        >
          {useTranslation().t('button_save')}
        </button>
        <button onClick={closeSettings}>
          {useTranslation().t('button_cancel')}
        </button>
      </div>
    </div>
  );
};

export default SettingsForm;
