import * as React from 'react';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { TFunction } from 'i18next';
import { format } from 'date-fns';
import styled from 'styled-components';
import { useTranslationWithPrefix } from '../../i18n';
import { YAxisUnit, convertValueForYAxis } from './SettingsForm';

const StyledGraph = styled(({ className, ...props }) => (
  <HighchartsReact {...props} containerProps={{ className }} />
))`
  .highcharts-container {
    display: flex;
    flex-direction: column;
  }
`;

type GraphDates = {
  targetDate: string;
  estimatedCompletionDate: string;
  targetDateFormer?: string;
  estimatedCompletionDateFormer?: string;
  lgcMilestones?: Array<{ name: string; color: string; date: string }>;
};

type GraphSeriesData = {
  [type: string]: { label: string; data: Array<[string, number]> };
};

export type GraphData = {
  title: string;
  dates: GraphDates;
  data: GraphSeriesData;
  targetIds: string[];
  message: string;
  error: string;
};

// Order of series decided here
export const GRAPH_COLUMNS = {
  budget: { color: 'orange' },
  budgetSurplus: { color: 'indianred' },
  plannedValueFormer: { color: 'skyblue' },
  plannedValue: { color: 'blue' },
  actualCost: { color: 'red' },
  earnedValue: { color: 'green' },
  budgetAtCompletionFormer: { color: 'royalblue', dashStyle: 'ShortDot' },
  budgetAtCompletion: { color: 'darkblue', dashStyle: 'ShortDot' },
  estimatedAtCompletionFormer: { color: 'hotpink', dashStyle: 'ShortDot' },
  estimatedAtCompletion: { color: 'red', dashStyle: 'ShortDot' },
  estimatedEarnedValueFormer: { color: 'lightgreen', dashStyle: 'ShortDot' },
  estimatedEarnedValue: { color: 'green', dashStyle: 'ShortDot' },
};

const convertDate = (date: string): number => new Date(date).getTime();

const plotLine = (
  label: string,
  date: string,
  options?: Highcharts.XAxisPlotLinesOptions
): Highcharts.XAxisPlotLinesOptions => {
  const { label: labelOptions, ...otherOptions } = options || {};
  return {
    label: { text: label, ...(labelOptions || {}) },
    value: convertDate(date),
    color: '#666666',
    width: 2,
    dashStyle: 'Solid',
    zIndex: 2,
    ...otherOptions,
  };
};

const tooltipFormatter = function (): string {
  const formatDate = (dateString: string): string =>
    new Date(dateString).toLocaleDateString();
  const round = (num: number): number => Math.round(num * 100) / 100;
  const formatNumber = (num: number): string =>
    Number(round(num)).toLocaleString();

  const header = `<span style="font-size:10px;">${formatDate(this.x)}</span>`;

  const values = this.points
    .map(({ color, series, point }) => {
      const bullet = `<strong style="color: ${color}"> &bull; </strong>`;
      const label = `<span>${series.name}: </span>`;
      const value = `<strong>${formatNumber(point.y)}</strong>`;

      return '<br />' + bullet + label + value;
    })
    .join('');

  return header + values;
};

const plotLinesFromDates = (
  dates: GraphDates,
  t: TFunction
): Highcharts.XAxisPlotLinesOptions[] => {
  const bottomRightLabel: Highcharts.XAxisPlotLinesLabelOptions = {
    verticalAlign: 'bottom',
    textAlign: 'right',
    y: -10,
  };

  const result = [
    plotLine(dates.targetDate, dates.targetDate),
    plotLine(t('estimated_completion_date'), dates.estimatedCompletionDate, {
      dashStyle: 'ShortDot',
      label: bottomRightLabel,
    }),
  ];
  if (dates.targetDateFormer && dates.targetDateFormer !== dates.targetDate) {
    result[1].label.text = t('estimated_completion_date_current');
    result.push(
      plotLine(dates.targetDateFormer, dates.targetDateFormer),
      plotLine(
        t('estimated_completion_date_former'),
        dates.estimatedCompletionDateFormer,
        {
          dashStyle: 'ShortDot',
          label: bottomRightLabel,
        }
      )
    );
  }

  if (dates.lgcMilestones) {
    dates.lgcMilestones.forEach(({ name, date, color }) => {
      result.push(plotLine(date, date, { color }));
      result.push(
        plotLine(name, date, {
          color,
          label: bottomRightLabel,
        })
      );
    });
  }
  return result;
};

const seriesFromData = (
  seriesData: GraphSeriesData,
  yAxisUnit: YAxisUnit
): Highcharts.SeriesLineOptions[] => {
  const presentColumns = Object.keys(GRAPH_COLUMNS).filter(
    (type) => seriesData[type]
  );

  return presentColumns.map((column) => {
    const { label, data } = seriesData[column];

    return {
      name: label,
      data: data.map(([date, value]) => [
        convertDate(date),
        convertValueForYAxis(value, yAxisUnit),
      ]),
      ...GRAPH_COLUMNS[column],
    };
  });
};

const generateOptions = (
  data: GraphData,
  t: TFunction,
  yAxisUnit: YAxisUnit
): Highcharts.Options => {
  if (!data.title) {
    return {};
  }

  return {
    chart: {
      zoomType: 'x',
      resetZoomButton: {
        position: {
          y: 0,
        },
      },
      marginLeft: 70,
      marginRight: 35,
    },
    title: {
      text: '', //originally : data.title, (removed to improve graph visibility on 27/5/2021)
      align: 'left',
      x: 40,
      style: {
        textOverflow: 'ellipsis',
      },
    },
    xAxis: {
      crosshair: true,
      type: 'datetime',
      dateTimeLabelFormats: {
        day: '%Y<br/>%m-%d',
        week: '%Y<br/>%m-%d',
        month: '%Y-%m',
        year: '%Y',
      },
      plotLines: plotLinesFromDates(data.dates, t),
      minRange: 18 * 24 * 3600 * 1000,
    },
    yAxis: {
      title: {
        text: t(`unit.${yAxisUnit}`),
      },
      min: 0,
    },
    tooltip: {
      shared: true,
      useHTML: true,
      formatter: tooltipFormatter,
    },
    plotOptions: {
      series: {
        marker: { enabled: false },
        connectNulls: true,
        dataGrouping: {
          dateTimeLabelFormats: {
            day: ['%A, %b %e, %Y'],
          },
        },
      },
    },
    series: seriesFromData(data.data, yAxisUnit),
    legend: {
      enabled: true,
    },
    credits: {
      enabled: false,
    },
  };
};

export interface GraphProps {
  data: GraphData;
  yAxisUnit?: YAxisUnit;
  height: number;
  modifyOptions?: (options: Highcharts.Options) => void;
  setSelectedDate: (date: string) => void;
}

const Graph: React.FC<GraphProps> = ({
  data,
  height,
  modifyOptions,
  yAxisUnit,
  setSelectedDate,
}: GraphProps) => {
  const { t } = useTranslationWithPrefix('evm_panel.graph');

  const dateFormat = (xAxis: number): string => {
    return format(new Date(xAxis), 'yyyy-MM-dd');
  };

  const options: Highcharts.Options = React.useMemo(() => {
    const result = generateOptions(data, t, yAxisUnit || 'hours');
    result.chart.events = {
      click: function (e): void {
        const xValue = e.xAxis[0].value;
        const dataValueArr = this.series[0].xData;
        const closestValue = dataValueArr.reduce(
          (currentMinimum: number, anotherValue: number) => {
            return Math.abs(anotherValue - xValue) <
              Math.abs(currentMinimum - xValue)
              ? anotherValue
              : currentMinimum;
          }
        );
        const date = dateFormat(closestValue);
        setSelectedDate(date);
      },
    };

    result.plotOptions.series.events = {
      click: function (e): void {
        const date = dateFormat(e.point.x);
        setSelectedDate(date);
      },
    };
    result.chart.height = height;
    if (modifyOptions) {
      modifyOptions(result);
    }
    return result;
  }, [data, height, modifyOptions, setSelectedDate, t, yAxisUnit]);

  return data.title ? (
    <StyledGraph highcharts={Highcharts} options={options} />
  ) : null;
};

export default Graph;
