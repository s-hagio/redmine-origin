import * as React from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import FieldLabel from '../atoms/FieldLabel';
import IconButton from '../atoms/IconButton';
import MultiSelect from '../atoms/MultiSelect';
import {
  VersionData,
  MilestoneData,
  ValueUnit,
  VALUE_UNIT,
  buildSelect,
} from './SettingsForm';
import { setEvmFilter, getEvmFilter } from '../../localstorage';

const Root = styled.div`
  padding: 10px;
`;

export interface FilterValues {
  targetVersions: VersionData;
  targetMilestones: MilestoneData;
  projectId: number;
  valueUnit: ValueUnit;
}

interface FilterProps {
  className?: string;
  values: FilterValues;
  onChange: (value: FilterValues) => void;
  versions: VersionData;
  isLgcAvailable: boolean;
  isLycheeCostAvailable: boolean;
  milestones: MilestoneData;
}

const Filter: React.FC<FilterProps> = ({
  className,
  values,
  onChange,
  versions,
  isLgcAvailable,
  isLycheeCostAvailable,
  milestones,
}: FilterProps) => {
  const { t } = useTranslation();
  if (getEvmFilter(values.projectId) === undefined) {
    setEvmFilter(values.projectId, values);
  }

  const [formValues, setFormValues] = React.useState(
    getEvmFilter(values.projectId)
  );
  React.useEffect(() => setFormValues(values), [values]);
  const handleSubmit = React.useCallback(() => {
    setEvmFilter(formValues.projectId, formValues);
    onChange(formValues);
    setFormValues(formValues);
  }, [onChange, formValues]);

  return (
    <Root className={className}>
      <div>
        <FieldLabel>{t('evm_panel.settings.label_version')}</FieldLabel>
        <MultiSelect
          options={versions}
          values={formValues.targetVersions}
          onChange={(selectedOptions): void =>
            setFormValues({
              ...formValues,
              targetVersions: selectedOptions as VersionData,
            })
          }
        />
      </div>
      {isLgcAvailable && (
        <div>
          <FieldLabel>{t('evm_panel.settings.label_milestone')}</FieldLabel>
          <MultiSelect
            options={milestones}
            values={formValues.targetMilestones}
            onChange={(selectedOptions): void =>
              setFormValues({
                ...formValues,
                targetMilestones: selectedOptions as MilestoneData,
              })
            }
          />
        </div>
      )}
      {isLycheeCostAvailable && (
        <div>
          <FieldLabel>{t('evm_panel.settings.label_value_unit')}</FieldLabel>
          {buildSelect({
            options: VALUE_UNIT,
            selected: formValues.valueUnit,
            onChange: (ev): void =>
              setFormValues({
                ...formValues,
                valueUnit: ev.target.value as ValueUnit,
              }),
            getLabel: (value): string =>
              t(`evm_panel.settings.value_unit.${value}`),
          })}
        </div>
      )}
      <div>
        <IconButton onClick={handleSubmit} icon="checked">
          {t('button_apply')}
        </IconButton>
      </div>
    </Root>
  );
};

export default Filter;
