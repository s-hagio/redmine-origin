import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { PanelProps, Comment } from '../interfaces/interface';
import { useCommentField } from './CommentField';
import styled from 'styled-components';

const Comment = styled.div`
  overflow-y: auto;
  height: 100%;
`;

interface CommentProps {
  reportComment: Comment;
}

const CommentPanel: React.FC<PanelProps<CommentProps>> = ({
  panel,
  PanelRootComponent,
}: PanelProps<CommentProps>) => {
  const { reportComment } = panel.content;
  const { t } = useTranslation();
  const [isCommentVisible, setCommentVisible] = React.useState(true);
  const { renderEditButton, renderComment } = useCommentField(
    reportComment,
    setCommentVisible
  );
  React.useEffect(() => {
    setCommentVisible(!!reportComment.text);
  }, []);

  return (
    <PanelRootComponent
      title={t('label_project_report_comment')}
      titlebarButtons={renderEditButton()}
      data-test="comment-panel" // For feature spec
    >
      <Comment style={isCommentVisible ? {} : { display: 'none' }}>
        {renderComment()}
      </Comment>
    </PanelRootComponent>
  );
};

export default CommentPanel;
