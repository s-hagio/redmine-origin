import * as React from "react";

interface IndexDecisionIndicatorProps {
  decision: string;
}

const IndexDecisionIndicator: React.FC<IndexDecisionIndicatorProps> = ({
  decision
}: IndexDecisionIndicatorProps) => {
  switch (decision) {
    case "green":
      return <div className="curled-box bggr"></div>;
    case "yellow":
      return <div className="curled-box bgye"></div>;
    case "red":
      return <div className="curled-box bgre"></div>;
    case "not_available":
      return <div className="not-available">―</div>;
    case "not_use":
      return <div className="curled-box not-use"></div>;
    default:
      return <div className="curled-box bggr"></div>;
  }
};

export default IndexDecisionIndicator;
