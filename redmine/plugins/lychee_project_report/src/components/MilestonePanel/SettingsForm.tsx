import * as React from "react";
import { toast } from "react-toastify";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import { useTranslationWithPrefix } from "../../i18n";
import CheckboxGroup from "../atoms/CheckboxGroup";
import { Trans } from "react-i18next";

const Input = styled.input`
  width: 45px;
`;

export interface MilestonePanelSettings {
  title: string;
  pastMonths: number;
  months: number;
  visibleSubProjectsIds: Array<number>;
}

interface SettingsFormProps {
  settings: MilestonePanelSettings;
  closeSettings: () => void;
  updateSettings: (settings: MilestonePanelSettings) => void;
  availableSubProjects: Array<{ id: number; name: string }>;
}

const SettingsForm: React.FC<SettingsFormProps> = ({
  closeSettings,
  updateSettings,
  settings,
  availableSubProjects
}: SettingsFormProps) => {
  const { t } = useTranslationWithPrefix("milestone_panel.settings");

  const [formValues, setFormValues] = React.useState(settings);

  const { visibleSubProjectsIds } = formValues;

  const message = useTranslation().t("save_setting_alert");
  const submit = (): void => {
    updateSettings(formValues);
    toast(message);
    closeSettings();
  };

  return (
    <div>
      <div>
        <label>{t("label_title")}:</label>
        <input
          value={formValues.title}
          onChange={(ev): void =>
            setFormValues({ ...formValues, title: ev.target.value })
          }
        />
      </div>
      <div>
        <label>{t("label_term")}:</label>
        <Trans i18nKey="display_term">
          <Input
            className="milestone-months"
            type="number"
            min="0"
            max="99"
            value={formValues.pastMonths}
            onChange={(ev): void =>
              setFormValues({
                ...formValues,
                pastMonths: parseInt(ev.target.value)
              })
            }
          />
          ヶ月前から
          <Input
            className="milestone-months"
            type="number"
            min="0"
            max="99"
            value={formValues.months}
            onChange={(ev): void =>
              setFormValues({
                ...formValues,
                months: parseInt(ev.target.value)
              })
            }
          />
          ヶ月分
        </Trans>
      </div>
      <div>
        <label>{t("label_displayedSubProjects")}:</label>
        <CheckboxGroup
          values={availableSubProjects}
          checked={visibleSubProjectsIds}
          onChange={(newIds): void =>
            setFormValues({
              ...formValues,
              visibleSubProjectsIds: newIds
            })
          }
        />
      </div>
      <div>
        <button onClick={submit}>{useTranslation().t("button_save")}</button>
        <button onClick={closeSettings}>{useTranslation().t("button_cancel")}</button>
      </div>
    </div>
  );
};

export default SettingsForm;
