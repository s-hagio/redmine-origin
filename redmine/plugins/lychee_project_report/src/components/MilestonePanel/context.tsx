import * as React from "react";

interface GraphHeightProps {
  children: React.ReactChildren;
}

interface ContextValue {
  chartHeight: number;
  subChartHeight: Array<{
    id: number;
    graphHeight: number;
  }>;
  setChartHeight: (chartHeight) => void;
  setSubChartHeight: (subChartHeight) => void;
}

export const GraphHeightContext = React.createContext({} as ContextValue);

const GraphHeightProvider: React.ProviderProps<GraphHeightProps> = ({
  children
}: GraphHeightProps) => {
  const [chartHeight, setChartHeight] = React.useState();
  const [subChartHeight, setSubChartHeight] = React.useState([]);

  return (
    <GraphHeightContext.Provider
      value={{
        chartHeight,
        setChartHeight,
        subChartHeight,
        setSubChartHeight
      }}
    >
      {children}
    </GraphHeightContext.Provider>
  );
};

export default GraphHeightProvider;
