import * as React from "react";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import { SubProjects } from "./index";
import { GraphHeightContext } from "./context";

const borderStyle = "1.5px solid #e9e9e9";

const ProjectsNameLabel = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 44px;
  background-color: #f3f3f3;
  border: ${borderStyle};
  overflow-wrap: break-word;
  white-space: normal;
  word-break: break-all;
  overflow-y: auto;
`;

const ProjectName = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-start;
  margin-top: 1px;
  background-color: #f3f3f3;
  border: ${borderStyle};
  overflow-wrap: break-word;
  white-space: normal;
  word-break: break-all;
  box-sizing: border-box;
  padding: 0.5rem 0;
  overflow-y: auto;
`;

interface ProjectsTableProps {
  projectName: string;
  displayedSubProjects: SubProjects;
}

const ProjectsTable: React.FC<ProjectsTableProps> = ({
  projectName,
  displayedSubProjects
}: ProjectsTableProps) => {
  const { t } = useTranslation();
  const chartContext = React.useContext(GraphHeightContext);
  const { chartHeight, subChartHeight } = chartContext;

  const projectHeight = (project): number => {
    const findProject = subChartHeight.find(v => v.id === project.id);
    return findProject.graphHeight;
  };

  return (
    <>
      <ProjectsNameLabel>
        {t("milestone_panel.label.project_name")}
      </ProjectsNameLabel>
      <ProjectName style={{ height: `${chartHeight - 50}px` }}>
        {projectName}
      </ProjectName>
      {subChartHeight.length > 0 &&
        displayedSubProjects.map(project => (
          <ProjectName
            key={project.id}
            style={{
              height: `${projectHeight(project)}px`
            }}
          >
            {project.name}
          </ProjectName>
        ))}
    </>
  );
};

export default ProjectsTable;
