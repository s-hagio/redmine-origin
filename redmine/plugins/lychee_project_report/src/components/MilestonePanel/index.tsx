import * as React from 'react';
import styled from 'styled-components';
import CommentFrame from '../CommentFrame';
import { useCommentField } from '../CommentField';
import { PanelProps } from '../../interfaces/interface';
import SettingsForm, { MilestonePanelSettings } from './SettingsForm';
import Chart from './Chart';
import ProjectsTable from './ProjectsTable';
import GraphHeightProvider from './context';

const borderStyle = '1px solid #d5d5d5';

const MilestoneWrapper = styled.div`
  flex-grow: 1;
  height: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
`

const ChartColumn = styled.div`
  display: flex;
`;

const LeftColumn = styled.div`
  padding-top: 10px;
  overflow: hidden;
  box-sizing: border-box;
  width: 15%;
  height: 100%;
  color: #828282;
  font-size: 15px;
  font-weight: bold;
`;

const RightColumn = styled.div`
  padding-top: 10px;
  padding-left: 5px;
  width: 85%;
  height: 100%;
  overflow: hidden;
  box-sizing: border-box;
`;

const CommentArea = styled(CommentFrame)`
  border-top: ${borderStyle};
  background-color: #fff;
  margin-top: 4px;
  display: flex;
  min-height: 8.5rem;
  bottom: 0;
  position: relative;
  width: 100%;
`;

const ChartComponent = styled(Chart)`
  height: 100%;
`;

const Error = styled.div`
  font-size: 18px;
  text-align: center;
  width: 100%;
`;

export type SubProjects = Array<{
  id: number;
  name: string;
  term: {
    projectStart: string;
    projectEnd: string;
  };
  milestones: Array<{ name: string; date: string; color: string }>;
  versions: Array<{
    name: string;
    start_date: string;
    end_date: string;
  }>;
}>;

export type MilestonePanelContent = Partial<{
  projectName: string;
  error: string;
  comment: string;
  commentId: number;
  renderedComment: string;
  projectTerm: {
    projectStart: string;
    projectEnd: string;
  };
  milestones: Array<{ name: string; date: string; color: string }>;
  versions: Array<{
    name: string;
    start_date: string;
    end_date: string;
  }>;
  displayRange: {
    start_date: string;
    end_date: string;
  };
  displayedSubProjects: SubProjects;
  availableSubProjects: Array<{
    id: number;
    name: string;
  }>;
}>;

const MilestonePanel: React.FC<
  PanelProps<MilestonePanelContent, MilestonePanelSettings>
> = ({
  panel,
  PanelRootComponent,
}: PanelProps<MilestonePanelContent, MilestonePanelSettings>) => {
  const {
    projectName,
    projectTerm,
    milestones,
    versions,
    displayRange,
    error,
    displayedSubProjects,
    availableSubProjects,
  } = panel.content;
  const { title } = panel.settings;
  const [isCommentVisible, setCommentVisible] = React.useState(true);
  const { renderEditButton, renderComment } = useCommentField(
    panel.comment,
    setCommentVisible
  );
  React.useEffect(() => {
    setCommentVisible(!!panel.comment.text);
  }, []);
  if (error) {
    return (
      <PanelRootComponent title={title} data-test="milestone-panel">
        <Error>{error}</Error>
      </PanelRootComponent>
    );
  }
  return (
    <PanelRootComponent
      title={title}
      titlebarButtons={!error && renderEditButton()}
      renderPanelSettings={({
        updateSettings,
        closeSettings,
      }): React.ReactElement => (
        <SettingsForm
          settings={panel.settings}
          availableSubProjects={availableSubProjects}
          updateSettings={updateSettings}
          closeSettings={closeSettings}
        ></SettingsForm>
      )}
      data-test="milestone-panel"
    >
      <MilestoneWrapper>
        <GraphHeightProvider>
          <ChartColumn>
            <LeftColumn>
              <ProjectsTable
                projectName={projectName}
                displayedSubProjects={displayedSubProjects}
              />
            </LeftColumn>
            <RightColumn>
              <ChartComponent
                projectTerm={projectTerm}
                milestones={milestones}
                versions={versions}
                displayRange={displayRange}
              />
              {displayedSubProjects &&
                displayedSubProjects.map((subprojectData, index) => (
                  <ChartComponent
                    key={index}
                    id={subprojectData.id}
                    projectTerm={subprojectData.term}
                    milestones={subprojectData.milestones}
                    versions={subprojectData.versions}
                    displayRange={displayRange}
                    forSubProject
                  />
                ))}
            </RightColumn>
          </ChartColumn>
        </GraphHeightProvider>
        <CommentArea visible={isCommentVisible}>{renderComment()}</CommentArea>
      </MilestoneWrapper>
    </PanelRootComponent>
  );
};

export default MilestonePanel;
