import * as React from 'react';
import ReactDOMServer from 'react-dom/server';
import { useTranslation } from 'react-i18next';
import * as Highcharts from 'highcharts';
import styled from 'styled-components';
import HighchartsReact from 'highcharts-react-official';
import maxBy from 'lodash/maxBy';
import minBy from 'lodash/minBy';

import xrange from 'highcharts/modules/xrange';
import { GraphHeightContext } from './context';
xrange(Highcharts);

const ChartBox = styled.div`
  border-bottom: 1px dashed #bbbbbb;

  .highcharts {
    &-xaxis {
      &-labels span {
        overflow: visible !important;
      }

      &-label {
        font-size: 13px;
        font-weight: 800;
        line-height: 1;
        white-space: nowrap;
        word-break: keep-all;

        &__year {
          width: 1px;
        }
      }
    }

    &-container {
      display: flex;
      flex-direction: column;
    }
  }
`;

const COLORS = {
  xAxisBgColor: '#f6f6f6',
  termColor: '#0894d9',
  expiredColor: 'gray',
  textColor: '#778CBA',
  expiredBgColor: '#f3f3f3',
  expiredTextColor: '#a3a3a3',
  dashedColor: 'red',
  plotBgColor: 'white',
  versionBgColor: '#DAE3F5',
};

const HEIGHTS = {
  projectTerm: -1.3,
  milestones: -1.3,
  milestonesLabel: 0.7,
  milestonesHeight: 25,
  versions: -2.8,
  versionHeight: 30,
};

type ProjectTerm = {
  projectStart: string;
  projectEnd: string;
};

type Version = {
  id: number;
  name: string;
  status: string;
  start_date: string;
  end_date: string;
};

type DisplayRange = {
  start_date: string;
  end_date: string;
};

interface ChartProps {
  id: number;
  projectTerm: ProjectTerm;
  milestones: Array<{ name: string; date: string; color: string }>;
  versions: Array<Version>;
  displayRange: DisplayRange;
  forSubProject?: boolean;
}

const dateData = (date: string): number => {
  return new Date(date).getTime();
};

const convertPeriod = (
  startDate: string,
  endDate: string
): { start: number; end: number } | null => {
  if (!startDate && !endDate) {
    return null;
  }

  return {
    start: startDate ? dateData(startDate) : -Infinity,
    end: endDate ? dateData(endDate) : Infinity,
  };
};

const projectTermLine = (
  projectTerm: ProjectTerm,
  yValue: number
): Highcharts.SeriesOptionsType => {
  const { projectStart, projectEnd } = projectTerm;
  const actualProjectTerm = convertPeriod(projectStart, projectEnd);

  return {
    type: 'line',
    lineWidth: 3,
    color: COLORS.termColor,
    data: actualProjectTerm && [
      {
        x: actualProjectTerm.start,
        y: HEIGHTS.projectTerm - yValue,
        marker: { fillColor: COLORS.termColor },
      },
      {
        x: actualProjectTerm.end,
        y: HEIGHTS.projectTerm - yValue,
        marker: { fillColor: COLORS.termColor },
      },
    ],
  };
};

type Rect = { x: number; y: number; width: number; height: number };

const isOverlap = (rect: Rect, nextRect: Rect): boolean => {
  const rectL = rect.x;
  const rectR = rect.x + rect.width;
  const nextRectL = nextRect.x;
  const nextRectR = nextRect.x + nextRect.width;

  const rectB = rect.y;
  const rectT = rect.y + rect.height;
  const nextRectB = nextRect.y;
  const nextRectT = nextRect.y + nextRect.height;
  if (nextRectL > rectR || nextRectR < rectL) {
    return false;
  } // overlap not possible
  if (nextRectB > rectT || nextRectT < rectB) {
    return false;
  } // overlap not possible

  if (nextRectL >= rectL && nextRectL <= rectR) {
    return true;
  }
  if (nextRectR >= rectL && nextRectR <= rectR) {
    return true;
  }

  if (rectL >= nextRectL && rectL <= nextRectR) {
    return true;
  }
  if (rectR >= nextRectL && rectR <= nextRectR) {
    return true;
  }
  if (nextRectT >= rectB && nextRectT <= rectT) {
    return true;
  }
  if (nextRectB >= rectB && nextRectB <= rectT) {
    return true;
  }

  return false;
};

const removeCollidingElement = (
  points: Array<any>,
  getDimensions: (point: any) => Rect
): any | undefined => {
  let result = null;
  let resultIndex = null;
  for (let i = 0; i < points.length; i++) {
    for (let j = i + 1; j < points.length; j++) {
      if (getDimensions(points[i]) && getDimensions(points[j])) {
        if (isOverlap(getDimensions(points[i]), getDimensions(points[j]))) {
          result = points[j];
          resultIndex = j;
          break;
        }
      }
    }
    if (result !== null) {
      break;
    }
  }

  if (result) {
    points.splice(resultIndex, 1);
    return result;
  }
};

const calculateCollisionYLevels = (
  points: Array<any>,
  getDimension: (point: any) => Rect
): Array<Array<any>> => {
  const yLevels = [Array.from(points)];
  let currentYLevel = 0;
  let nextYLevel = [];

  for (;;) {
    const collidingElement = removeCollidingElement(
      yLevels[currentYLevel],
      getDimension
    );

    if (collidingElement) {
      nextYLevel.push(collidingElement);
    } else {
      if (nextYLevel.length === 0) {
        break;
      }
      yLevels.push(nextYLevel);
      currentYLevel++;
      nextYLevel = [];
    }
  }
  return yLevels;
};

const versionBars = (
  versions: Array<Version>
): Array<Highcharts.XrangePointOptionsObject> => {
  const compareVersion = (a, b): number => {
    const aDate = dateData(a.start_date);
    const bDate = dateData(b.start_date);
    return aDate - bDate;
  };
  return versions.sort(compareVersion).map((v) => {
    const actualVersionPeriod = convertPeriod(v.start_date, v.end_date);
    return {
      id: v.id.toString(),
      x: actualVersionPeriod.start,
      x2: actualVersionPeriod.end,
      y: HEIGHTS.versions,
      color:
        v.status === 'open' ? COLORS.versionBgColor : COLORS.expiredBgColor,
      dataLabels: {
        enabled: true,
        align: 'left',
        x: 4,
        allowOverlap: true,
        format: v.name,
        style: {
          textOutline: '0px',
          color:
            v.status === 'open' ? COLORS.textColor : COLORS.expiredTextColor,
          fontSize: '13px',
        },
      },
    };
  });
};

const Chart: React.FC<ChartProps> = ({
  id,
  projectTerm,
  milestones,
  displayRange,
  versions,
  forSubProject,
}: ChartProps): React.ReactElement => {
  const { t } = useTranslation();
  const isExpired = (date: string): boolean => {
    return Date.now() > dateData(date);
  };

  const [yValue, setYvalue] = React.useState(0);
  const [graphHeight, setGraphHeight] = React.useState(210);
  const chartContext = React.useContext(GraphHeightContext);
  const { setChartHeight, setSubChartHeight } = chartContext;

  React.useEffect(() => {
    if (!forSubProject) {
      return setChartHeight(graphHeight + 40);
    } else {
      return setSubChartHeight((subChartHeight) => [
        { id, graphHeight },
        ...subChartHeight,
      ]);
    }
  }, [forSubProject, graphHeight, setChartHeight, setSubChartHeight, id]);

  const dateFormatChoice = (dateValue: number, firstMonth: number): string => {
    const month = Highcharts.dateFormat('%b', dateValue);
    if (new Date(dateValue).getMonth() === 0 || dateValue === firstMonth) {
      const year = Highcharts.dateFormat(
        t('milestone_panel.label.year_format'),
        dateValue
      );
      const label = (
        <div className="highcharts-xaxis-label">
          <div className="highcharts-xaxis-label__year">{year}</div>
          <div className="highcharts-xaxis-label__month">{month}</div>
        </div>
      );
      return ReactDOMServer.renderToString(label);
    } else {
      const label = (
        <div className="highcharts-xaxis-label">
          <div className="highcharts-xaxis-label__month">{month}</div>
        </div>
      );
      return ReactDOMServer.renderToString(label);
    }
  };

  const milestoneData = {
    data: milestones.map((m) => ({
      x: dateData(m.date),
      y: HEIGHTS.milestones - yValue,
      dataLabels: {
        enabled: true,
        format: m.name,
        style: {
          color: isExpired(m.date) ? COLORS.expiredColor : COLORS.textColor,
        },
        x: -12,
        y: -2,
      },
      marker: { fillColor: isExpired(m.date) ? COLORS.expiredColor : m.color },
    })),
  };

  const finalVersionData = React.useMemo(
    () => ({
      type: 'xrange',
      styledMode: true,
      pointWidth: 22,
      borderRadius: 0,
      data: versionBars(versions).map((p) => ({ ...p, y: p.y - yValue })),
    }),
    [versions, yValue]
  );

  interface PointDataLabel {
    translate: (x: number, y: number) => void;
    translateX: number;
    translateY: number;
    height: number;
    width: number;
    padding: number;
  }
  type HighchartsPoint = Highcharts.Point & { dataLabel: PointDataLabel };
  const antiCollision = (points: Array<HighchartsPoint>): number => {
    const yLevels = calculateCollisionYLevels(points, (point) => ({
      x: point.dataLabel.x - point.dataLabel.padding,
      y: point.dataLabel.y,
      width: point.dataLabel.width,
      height: point.dataLabel.height,
    }));

    let heightSoFar = 0;
    for (let i = 1; i < yLevels.length; i++) {
      const tallestLabel = maxBy(yLevels[i - 1], (p) => p.dataLabel.height)
        .dataLabel;
      heightSoFar += tallestLabel.height - tallestLabel.padding * 1.8;

      yLevels[i].forEach((p) => {
        p.dataLabel.translate(
          p.dataLabel.translateX,
          p.dataLabel.translateY - heightSoFar
        );
      });
    }
    setYvalue(HEIGHTS.milestonesLabel * (yLevels.length - 1));
    return yLevels.length;
  };

  const versionAntiCollision = (points: Array<HighchartsPoint>): number => {
    if (points.length === 0) {
      return 0;
    }

    const lowestPositionY = minBy(points, (point) => point.shapeArgs.y)
      .shapeArgs.y;
    points.forEach((point) => {
      point.dataLabel.translate(point.dataLabel.translateX, 0);
      point.graphic.translate(0, -point.shapeArgs.y);
    });
    const yLevels = calculateCollisionYLevels(points, (point) => ({
      x: Math.min(point.shapeArgs.x, point.dataLabel.translateX),
      y: point.shapeArgs.y,
      width: Math.max(
        point.dataLabel.padding * 2 + point.dataLabel.width,
        point.shapeArgs.width
      ),
      height: point.shapeArgs.height,
    })) as Array<Array<HighchartsPoint>>;

    for (let i = 0; i < yLevels.length; i++) {
      yLevels[i].forEach((p) => {
        const posy = lowestPositionY + HEIGHTS.versionHeight * i;
        p.dataLabel.translate(p.dataLabel.translateX, posy);
        p.graphic.translate(0, p.graphic.translateY + posy);
      });
    }
    return yLevels.length;
  };

  const options: Highcharts.Options = {
    chart: {
      type: 'line',
      height: forSubProject ? graphHeight : graphHeight + 40,
      backgroundColor: forSubProject ? '' : COLORS.xAxisBgColor,
      plotBackgroundColor: COLORS.plotBgColor,
      events: {
        load: function (): void {
          const milestoneLayers = antiCollision(
            this.series[1].points as Array<HighchartsPoint>
          );
          const versionLayers = versionAntiCollision(
            this.series[2].points as Array<HighchartsPoint>
          );
          setGraphHeight(
            milestoneLayers * HEIGHTS.milestonesHeight +
              versionLayers * HEIGHTS.versionHeight +
              (this.xAxis[0].autoRotation ? 45 : 20)
          );
        },
        redraw: function (): void {
          const milestoneLayers = antiCollision(
            this.series[1].points as Array<HighchartsPoint>
          );
          const versionLayers = versionAntiCollision(
            this.series[2].points as Array<HighchartsPoint>
          );
          setGraphHeight(
            milestoneLayers * HEIGHTS.milestonesHeight +
              versionLayers * HEIGHTS.versionHeight +
              (this.xAxis[0].autoRotation ? 45 : 20)
          );
        },
      },
      marginLeft: 0,
      marginRight: 0,
      marginBottom: 0,
    },
    credits: {
      enabled: false,
    },
    legend: {
      enabled: false,
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      },
    },
    title: {
      text: undefined,
    },
    xAxis: [
      {
        tickWidth: 0,
        opposite: true,
        lineWidth: 0,
        type: 'datetime',
        tickInterval: 30 * 24 * 3600 * 1000,
        labels: forSubProject
          ? { enabled: false }
          : {
              formatter: function (): string {
                const dateValue = this.value;
                const firstMonth = this.axis.tickPositions[0];
                return dateFormatChoice(dateValue, firstMonth);
              },
              useHTML: true,
              autoRotation: false,
            },
        min: dateData(displayRange.start_date),
        max: dateData(displayRange.end_date),
        plotLines: [
          {
            color: COLORS.dashedColor,
            width: 2,
            value: Date.now(),
            dashStyle: 'ShortDot',
            zIndex: 4,
          },
        ],
      },
    ],
    plotOptions: {
      line: {
        lineWidth: 0,
        marker: {
          radius: 7,
          enabledThreshold: 0,
          symbol: 'diamond',
        },
        dataLabels: {
          enabled: false,
          align: 'left',
          allowOverlap: true,
          x: -12,
          y: -6,
          verticalAlign: 'bottom',
          style: {
            fontSize: '13px',
          },
        },
      },
      series: {
        states: {
          inactive: {
            opacity: 1,
          },
          hover: {
            enabled: false,
          },
        },
      },
    },
    yAxis: [
      {
        visible: false,
        min: -(graphHeight / 30) * 1.3 - 0.5,
        max: 0,
      },
    ],
    series: [
      projectTermLine(projectTerm, yValue),
      milestoneData,
      finalVersionData,
    ],
    tooltip: {
      enabled: false,
    },
  };

  return (
    <>
      <ChartBox>
        <HighchartsReact highcharts={Highcharts} options={options} />
      </ChartBox>
    </>
  );
};

export default Chart;
