import { Layout } from './interfaces/interface';

interface Permissions {
  createLayout: boolean;
  editLayout: boolean;
  createSharedLayout: boolean;
}

interface PageVariablesType {
  projectIdentifier: string;
  urls: {
    root: string;
    layout: string;
    newLayout: string;
    panels: string;
    createSharedLayout: string;
  };
  permissions: Permissions;
  allowedPanels: Array<string>;
  language: string;
  currentLayoutId: number;
  currentLayoutShared: boolean;
  availableLayouts: Array<Layout>;
  reportedOn: string;
  startInEditMode: boolean;
  nextReportDate: string;
}

const dataset = document.getElementById('project-report').dataset;
const convertPermissions = (permissions: Array<string>): Permissions => ({
  createLayout: permissions.includes('createLayout'),
  editLayout: permissions.includes('editLayout'),
  createSharedLayout: permissions.includes('createSharedLayout'),
});

const parsedUrls = JSON.parse(dataset.urls);
parsedUrls.root = `${location.origin}${parsedUrls.root}`;
if (parsedUrls.root.endsWith('/')) {
  parsedUrls.root = parsedUrls.root.slice(0, -1);
}

export default {
  projectIdentifier: dataset.projectIdentifier,
  urls: parsedUrls,
  allowedPanels: JSON.parse(dataset.allowedPanels),
  language: dataset.language,
  permissions: convertPermissions(JSON.parse(dataset.permissions)),
  currentLayoutId: parseInt(dataset.currentLayoutId),
  currentLayoutShared: dataset.currentLayoutShared === 'true',
  availableLayouts: JSON.parse(dataset.availableSharedLayouts).concat(
    JSON.parse(dataset.availableLayouts)
  ),
  reportedOn: dataset.reportedOn,
  startInEditMode: dataset.startInEditMode === 'true',
  nextReportDate: dataset.nextReportDate,
} as PageVariablesType;
