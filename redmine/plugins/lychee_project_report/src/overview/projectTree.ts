import { LprStore } from './LprStore';

export class ProjectTree {
  lprStore: LprStore;
  onToggle: () => void;

  constructor(args: { onToggle: () => void }) {
    this.lprStore = new LprStore();
    this.onToggle = args.onToggle;
  }

  open(projectId: number): void {
    const openedProjects = this.lprStore.get('openedProjects');
    if (openedProjects.indexOf(projectId) < 0) {
      openedProjects.push(projectId);
    }
    this.lprStore.set('openedProjects', openedProjects);
    this.onToggle();
  }

  close(projectId: number): void {
    const openedProjects = this.lprStore.get('openedProjects');
    const index = openedProjects.indexOf(projectId);
    if (index >= 0) {
      openedProjects.splice(index, 1);
    }
    this.lprStore.set('openedProjects', openedProjects);
    this.onToggle();
  }
}
