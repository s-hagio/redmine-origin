export type Data = {
  source: RowData[];
  total_count: number;
};
type RowData = {
  project: {
    id: number;
    name: string;
    link: string;
    visible: boolean;
    version_ids: number[];
    lft: number;
    rgt: number;
    self_and_ancestor_lfts: number[];
    depth: number;
    ancestor_ids: number[];
    descendant_ids: number[];
  };
  term: {
    start_date: string;
    end_date: string;
  };
  version:
    | {
        id: number;
        name: string;
        effective_date: string;
        link: string;
        visible: boolean;
      }
    | {
        id: null;
        name: null;
        effective_date: null;
        link: string;
        visible: boolean;
      };
  comment: string | null;
  milestone: {
    project: {
      name: string;
      term: string | null;
      start_progress: Progress;
      end_progress: Progress;
    };
    lgc_milestones: {
      name: string;
      progress: Progress;
    }[];
    versions: {
      name: string;
      start_progress: Progress;
      end_progress: Progress;
      behind_progress: Progress;
      actual_width: number;
      title_position: number;
    }[];
    today_position: number | null;
  };
} & {
  [indice: string]: {
    decision: Decision;
    result: number | null;
    unit: string | null;
    budget?: number | null;
    eac?: number | null;
  };
} & {
  [user_custom_field: string]: {
    name: string | null;
    value: string | null;
  };
};
type Progress = number | null;
type Decision = 'not_available' | 'not_use' | 'green' | 'red' | 'yellow';
