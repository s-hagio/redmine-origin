import axios from 'axios';
import Tabulator from 'tabulator-tables';
import { LprStore } from './LprStore';
import { ColumnDefinitions } from './columnDefinitions';
import { ProjectTree } from './projectTree';
import { Data } from './api';
import { Dataset } from './dataset';

type Row = Data['source'][number];

const isVisible = (
  project: { ancestor_ids: number[] },
  openedProjects: number[]
): boolean => {
  if (project.ancestor_ids.length === 0) {
    return true;
  }
  return project.ancestor_ids.every((id) => openedProjects.includes(id));
};

const setProjectTermTooltips = (
  params: Dataset['options']['tooltip']
): void => {
  $('.project-term').tooltip({
    items: '.project-term-bar',
    content: function () {
      const element = $(this);
      return `
          <div>
            <dl class="project-term-bar-tooltip">
              <dt>${params.name}: </dt>
              <dd>${element.data('project-name')}</dd>
              <dt>${params.period}: </dt>
              <dd>${element.data('project-term')}</dd>
            </dl>
          </div>
        `;
    },
  });
};

$(async function () {
  let allData: Row[] = [];
  const dataStore: Record<number, Data['source'][number] | undefined> = {};
  const lprStore = new LprStore();

  const dataset = $('#project-report-table').data() as Dataset;

  const getVisibleData = (rows: Row[]): Row[] => {
    const openedProjects = lprStore.get('openedProjects');
    return rows.filter((row) => isVisible(row.project, openedProjects));
  };

  const columnDefinitions = new ColumnDefinitions(
    lprStore.get('columnWidths'),
    () => lprStore.get('openedProjects'),
    dataStore
  );
  const columns = columnDefinitions.definitions(
    dataset.columns,
    dataset.options
  );

  const calculateHeight = () => {
    const targetTable = $('#project-report-table');
    const headerHeight = targetTable.offset().top;
    const newHeight = window.innerHeight - headerHeight - 36; // footer height
    return newHeight;
  };

  const resizeReportTable = (height = undefined) => {
    const targetTable = $('#project-report-table');
    const newHeight = height || calculateHeight();
    targetTable.css('height', newHeight);
    targetTable.css('min-height', newHeight);
    targetTable.css('max-height', newHeight);
  };

  const table = new Tabulator('#project-report-table', {
    layout: 'fitColumns',
    selectable: false,
    data: getVisibleData(allData),
    columns,
    height: calculateHeight(),
    initialSort: dataset.sorts.map((sort) => ({
      column: sort[0],
      dir: sort[1],
    })),
    tooltipGenerationMode: 'hover',
    columnResized: (column): void => {
      const columnWitdh: { [key: string]: number } = {};
      column
        .getTable()
        .getColumnLayout()
        .forEach((layout) => {
          if (layout.field && layout.width) {
            columnWitdh[layout.field] = Number(layout.width);
          }
        });
      lprStore.set('columnWidths', columnWitdh);
      // 同一threadでredrawするとlinkのclickが効かなくなったりするっぽいので…
      // TODO: need redrawing?
      setTimeout(() => {
        table.redraw(true);
      }, 0);
    },
    dataSorting: (sorters): void => {
      const sortQuery = sorters
        .filter(({ field }) => field !== '')
        .map(({ field, dir }) => `${field}:${dir}`)
        .join(',');
      // to update sort in session
      if (sortQuery) {
        $.get(location.href, { sort: sortQuery });
      }
    },
    renderComplete: (): void => {
      setProjectTermTooltips(dataset.options.tooltip);
    },
    printAsHtml: true,
    printStyled: true,
    printRowRange: 'active',
    tooltipsHeader: function (column) {
      const descriptions = $('#indice_setting_descriptions').data(
        'descriptions'
      );
      const field = column.getDefinition().field;
      return descriptions[field] || '';
    },
    placeholder: $('#placeholder').data('message'),
  });

  const replaceData = (): void => {
    table.replaceData(getVisibleData(allData));
  };

  const ajaxGet = <T>(url: string) => {
    return new Promise<{ data: T }>((resolve, reject) => {
      $.get(url).then(
        (result) => resolve({ data: result }),
        () => reject()
      );
    });
  };

  const buildUrlWithOffset = (offset: number, limit: number): string => {
    const sameUrlAsJson =
      location.origin + location.pathname + '.json' + location.search;

    return (
      sameUrlAsJson +
      (sameUrlAsJson.indexOf('?') !== -1 ? '&' : '?') +
      `offset=${offset.toString()}&limit=${limit.toString()}`
    );
  };

  const setData = (source: Data['source']): void => {
    allData = allData.concat(source);
    source.forEach((data) => {
      if (data.version.id === null) {
        dataStore[data.project.id] = data;
      }
    });
  };

  const getData = async (
    offset: number,
    limit: number
  ): Promise<Data['source']> => {
    const urlWithOffset = buildUrlWithOffset(offset, limit);
    const { data } = await axios.get<Data>(urlWithOffset);
    return data.source;
  };

  const FIRST_LIMIT = 25 as const;
  const LIMIT = 25 as const;
  (async () => {
    const urlWithOffset = buildUrlWithOffset(0, FIRST_LIMIT);
    const { data } = await ajaxGet<Data>(urlWithOffset);
    setData(data.source);
    replaceData();

    const totalCount = data.total_count;
    const offsets: number[] = [];
    for (let i = 0; FIRST_LIMIT + i * LIMIT < totalCount; i++) {
      offsets.push(FIRST_LIMIT + i * LIMIT);
    }
    const CHUNK = 5 as const;
    for (let i = 0, j = offsets.length; i < j; i += CHUNK) {
      const sources = await Promise.all(
        offsets.slice(i, i + CHUNK).map((offset) => {
          return getData(offset, LIMIT);
        })
      );
      sources.forEach((source) => setData(source));
      replaceData();
    }
  })();

  $(document).on('click', '.tree-handle', function () {
    $(this).toggleClass('tree-open');

    const projectTree = new ProjectTree({ onToggle: replaceData });
    const projectId: number = $($(this).parents('.tabulator-row')[0]).data(
      'projectId'
    );
    if ($(this).hasClass('tree-open')) {
      projectTree.open(projectId);
    } else {
      projectTree.close(projectId);
    }
  });

  $(document).on('click', '#toggle-all-rows', function () {
    $(this).toggleClass('tree-open');
    const openAll = $(this).hasClass('tree-open');
    const projectTree = new ProjectTree({ onToggle: replaceData });
    const openedProjects = [];

    if (openAll) {
      allData
        .map((d) => d.project)
        .forEach(function (d) {
          if (openedProjects.indexOf(d.id) < 0) {
            openedProjects.push(d.id);
          }
        });
    }

    $($('#project-report-table').find('.tree-handle')).each(function (_) {
      if (openAll) {
        if (!$(this).hasClass('tree-open')) {
          $(this).addClass('tree-open');
        }
      } else {
        if ($(this).hasClass('tree-open')) {
          $(this).removeClass('tree-open');
        }
      }
    });

    projectTree.lprStore.set('openedProjects', openedProjects);
    replaceData();
  });

  // TODO:
  // 高速スクロールしたときにうまく表示されないことがあるため、秒間のスクロール量が大きいときにredrawしている
  let scrollState: { scrollTop: number; timestamp: number };
  let timer: number | null = 0;
  $('#project-report-table')
    .find('.tabulator-tableHolder')
    .on('scroll', (e) => {
      if (timer) {
        clearTimeout(timer);
      } else {
        scrollState = {
          scrollTop: e.target.scrollTop,
          timestamp: new Date().valueOf(),
        };
      }
      timer = setTimeout(() => {
        const deltaScroll = Math.abs(
          e.target.scrollTop - scrollState.scrollTop
        );
        const deltaTime = (new Date().valueOf() - scrollState.timestamp) / 1000;
        if (deltaScroll / deltaTime > 800) {
          table.redraw(false);
        }
        timer = null;
      }, 100);
    });

  jQuery(() => {
    const newHeight = calculateHeight();
    resizeReportTable(newHeight);
    table.setHeight(newHeight);
    table.redraw(false);
  });

  $(window).on('resize', () => {
    const newHeight = calculateHeight();
    resizeReportTable(newHeight);
    table.setHeight(newHeight);
    table.redraw(false);
  });
});
