import { RowComponent, ColumnComponent, SortDirection } from 'tabulator-tables';
import { Data as ApiData } from './api';

interface Row<T> extends RowComponent {
  getData: () => T;
}

type Data = {
  project: {
    id: number;
    lft: number;
    rgt: number;
    ancestor_ids: number[];
    self_and_ancestor_lfts: number[];
  };
  version:
    | {
        id: number;
        effective_date: string;
      }
    | {
        id: null;
        effective_date: null;
      };
};

export const sorterWithTree = <T>(
  dataStore: Record<number, ApiData['source'][number] | undefined>
) => (
  a: T,
  b: T,
  aRow: Row<Data>,
  bRow: Row<Data>,
  column: ColumnComponent,
  dir: SortDirection
): number => {
  const { project: aProject, version: aVersion } = aRow.getData();
  const { project: bProject, version: bVersion } = bRow.getData();

  // aがbのサブプロジェクトの場合、aを上にする
  if (bProject.lft < aProject.lft && aProject.rgt < bProject.rgt) {
    return dir === 'asc' ? 1 : -1;
  }

  // bがaのサブプロジェクトの場合、bを上にする
  if (aProject.lft < bProject.lft && bProject.rgt < aProject.rgt) {
    return dir === 'asc' ? -1 : 1;
  }

  const aAncestorIds = [...aProject.ancestor_ids, aProject.id];
  const bAncestorIds = [...bProject.ancestor_ids, bProject.id];
  const sortValuesMin = Math.min(aAncestorIds.length, bAncestorIds.length);

  // 同一の親を持つ階層を探す
  let depth: number;
  for (depth = 0; depth < sortValuesMin - 1; depth++) {
    if (aAncestorIds[depth] !== bAncestorIds[depth]) {
      break;
    }
  }
  // 同一の親を持つ階層同士で比較する
  const aValue = getValue(dataStore[aAncestorIds[depth]], column);
  const bValue = getValue(dataStore[bAncestorIds[depth]], column);

  if (typeof aValue === 'string' && typeof bValue === 'string') {
    const compareValue = aValue.localeCompare(bValue);
    if (compareValue !== 0) {
      return compareValue;
    }
  } else if (typeof aValue === 'number' && typeof bValue === 'number') {
    const compareValue = aValue - bValue;
    if (compareValue !== 0) {
      return compareValue;
    }
  } else if (typeof aValue === 'number' || typeof bValue === 'number') {
    const compareValue = (parseFloat(aValue) || 0) - (parseFloat(bValue) || 0);
    if (compareValue !== 0) {
      return compareValue;
    }
  } else {
    throw new Error(`compared values are invalid: ${aValue}, ${bValue}`);
  }

  // 第2sort以降の場合、sortを終了する
  const sorters = column.getTable().getSorters();
  if (sorters.length > 1 && sorters[0].field !== column.getField()) {
    return 0;
  }

  // プロジェクトの階層でソート
  const aProjectLfts = aProject.self_and_ancestor_lfts;
  const bProjectLfts = bProject.self_and_ancestor_lfts;
  for (let i = 0; i < sortValuesMin; i++) {
    const diff = aProjectLfts[i] - bProjectLfts[i];
    if (diff !== 0) {
      return diff;
    }
  }

  if (aAncestorIds.length > bAncestorIds.length) {
    return dir === 'asc' ? 1 : -1;
  }

  if (bAncestorIds.length > aAncestorIds.length) {
    return dir === 'asc' ? -1 : 1;
  }

  const lftCompare =
    dir == 'asc' ? aProject.lft - bProject.lft : bProject.lft - aProject.lft;
  if (lftCompare !== 0) {
    return lftCompare;
  }
  if (!aVersion.id && bVersion.id) {
    return dir == 'asc' ? -1 : 1;
  }
  if (aVersion.id && !bVersion.id) {
    return dir == 'asc' ? 1 : -1;
  }
  if (aVersion.id && bVersion.id) {
    // バージョンは常にeffective_date asc
    const compareDate = aVersion.effective_date.localeCompare(
      bVersion.effective_date
    );
    return dir == 'asc' ? compareDate : -compareDate;
  }

  return 0;
};

const getValue = (
  data: ApiData['source'][number] | undefined,
  column: ColumnComponent
) => {
  const field = column.getField();
  if (field.match(/^cf_/)) {
    const { format } = data[field];
    let value = data ? data[field].name ?? '' : '';
    if (format === 'int' || format === 'float') {
      value = parseFloat(value) || 0;
    }
    return value;
  }
  switch (field) {
    case 'project':
    case 'project_manager':
    case 'project_sub_manager':
      return data ? data[field].name ?? '' : '';
    case 'term':
      return data ? data[field].start_date ?? '' : '';
    default:
      return data
        ? +(data[field].result ?? Number.NEGATIVE_INFINITY)
        : Number.NEGATIVE_INFINITY;
  }
};
