import { ColumnDefinition, CellComponent } from 'tabulator-tables';
import { htmlGenerator } from './htmlGenerator';
import { sorterWithTree } from './sorterWithTree';
import { Data as ApiData } from './api';
import { Dataset } from './dataset';

interface Cell<T> extends CellComponent {
  getValue: () => T;
  getData: () => Data;
}

type Column = {
  field: string;
  title: string;
  sortable: boolean;
};

type Data = ApiData['source'][number];

type ColumnWidths = {
  [key: string]: number;
};

const addNoUnderlineClass = <T>(cell: Cell<T>): void => {
  const {
    project: { version_ids: versionIds },
    version: { id: versionId },
  } = cell.getData();
  if (
    cell.getRow().getCell('version') &&
    versionIds.length > 0 &&
    versionId !== versionIds[versionIds.length - 1]
  ) {
    cell.getElement().classList.add('no-underline');
  }
};

export class ColumnDefinitions {
  private columnWidths: ColumnWidths;
  private getOpenedProjects: () => number[];
  private dataStore: Record<number, Data | undefined>;

  constructor(
    columnWidths: ColumnWidths,
    getOpenedProjects: () => number[],
    dataStore: Record<number, Data | undefined>
  ) {
    this.columnWidths = columnWidths;
    this.getOpenedProjects = getOpenedProjects;
    this.dataStore = dataStore;
  }

  definitions(
    columns: Column[],
    options: Dataset['options']
  ): ColumnDefinition[] {
    let result = columns.map((column) => {
      switch (column.field) {
        case 'project':
          return this.project(column);
        case 'term':
          return this.term(column);
        case 'comment':
          return this.comment(column);
        case 'version':
          return this.version(column);
        case 'milestone':
          return this.milestone(column, options.milestone);
      }
      if (column.field.match(/^indice_setting_\d+$/)) {
        return this.result(column);
      }
      if (column.field.match(/^cf_\d+$/)) {
        return this.userCustomField(column);
      }
      throw new Error(`Unexpected field: ${column.field}`);
    });
    if (options.milestone.days <= 0) {
      result = result.filter((column) => column.field !== 'milestone');
    }
    return result;
  }

  private isExpanded(id: number): boolean {
    return this.getOpenedProjects().includes(id);
  }

  private project<T extends Data['project']>(column: Column): ColumnDefinition {
    return {
      field: column.field,
      title: column.title,
      width: this.columnWidths.project ?? 200,
      frozen: true,
      variableHeight: true,
      tooltip: (cell: Cell<T>): string => cell.getValue().name,
      formatter: (cell: Cell<T>, _, defer): string => {
        defer(() => {
          addNoUnderlineClass(cell);
          const {
            project: { id: projectId },
          } = cell.getData();
          const rowElement = cell.getRow().getElement();
          rowElement.classList.add(`project-${projectId}`); // for testing
          rowElement.dataset['projectId'] = projectId.toString(); // for tree-handle click
        });

        const { project, version } = cell.getData();
        if (version.name) {
          return '';
        }
        return `
          <div style="padding-left: ${project.depth * 10}px;">
            ${
              project.descendant_ids.length > 0
                ? `<span class="tree-handle ${
                    this.isExpanded(project.id) ? 'tree-open' : ''
                  }"></span>`
                : ''
            }
            <span class="icon icon-projects" />
            ${
              project.visible
                ? `<a href="${project.link}">${project.name}</a>`
                : project.name
            }
          <div>
        `;
      },
      sorter: sorterWithTree(this.dataStore),
    };
  }

  private term<T extends Data['term']>(column: Column): ColumnDefinition {
    return {
      field: column.field,
      title: column.title,
      width: 120,
      formatter: (cell: Cell<T>): string => {
        const { start_date, end_date } = cell.getValue();
        return `${start_date || ''} ${start_date || end_date ? '~' : ''} ${
          end_date || ''
        }`;
      },
      headerSort: true,
      sorter: sorterWithTree(this.dataStore),
    };
  }

  private userCustomField<T extends Data[string]>(
    column: Column
  ): ColumnDefinition {
    return {
      field: column.field,
      title: column.title,
      width: this.columnWidths[column.field] ?? 150,
      formatter: (cell: Cell<T>): string => {
        return cell.getValue().name;
      },
      headerSort: true,
      sorter: sorterWithTree(this.dataStore),
    };
  }

  private comment<T extends Data['comment']>(column: Column): ColumnDefinition {
    const formatText = (text) => {
      return text
        .replace(/<img[^>]*>/g, '')
        .replace(/<a\s+href=/gi, '<a target="_blank" href=');
    };

    return {
      field: column.field,
      title: column.title,
      formatter: (cell: Cell<T>): string => {
        return `
          <div class="comment" style='font-size: 12px'>
            <p>${formatText(cell.getValue())}</p>
          </div>
        `;
      },
      width: this.columnWidths.comment ?? 150,
      headerSort: false,
    };
  }

  private milestone<
    T extends Data['milestone'],
    P extends Dataset['options']['milestone']
  >(column: Column, params: P): ColumnDefinition {
    const width = params.days * 3 + 9;
    return {
      field: column.field,
      title: column.title,
      width: width,
      minWidth: width,
      headerSort: false,
      titleFormatterParams: (): P => params,
      titleFormatter: (_, params: P): string =>
        `<div>
           <div class="milestone">${params.title}</div>
           <div class="milestone-months">
             ${params.months
               .map((month) => {
                 const width = month.days * 3 - 1; // 1 day = 3px (minus 1px for border)
                 return `<div class="milestone-month" style="width: ${width}px">${month.month}${params.ext}</div>`;
               })
               .join('')}
            </div>
          </div>
        `,
      formatter: (cell: Cell<T>): string => {
        return htmlGenerator.milestone(cell.getValue());
      },
      print: false,
    };
  }

  private version<T extends Data['version']>(column: Column): ColumnDefinition {
    return {
      field: column.field,
      title: column.title,
      width: this.columnWidths.version ?? 150,
      variableHeight: true,
      tooltip: (cell: Cell<T>): string => cell.getValue().name ?? '',
      headerSort: false,
      formatter: (cell: Cell<T>): string => {
        const version = cell.getValue();
        if (!version.id || !version.name) {
          return '';
        }
        return `
          <div>
            <span class="icon icon-package" />
            ${
              version.visible
                ? `<a href="${version.link}">${version.name}</a>`
                : version.name
            }
          </div>
        `;
      },
    };
  }

  private result<T extends Data[string]>(column: Column): ColumnDefinition {
    return {
      field: column.field,
      title: column.title,
      width: this.columnWidths[column.field] ?? 100,
      hozAlign: 'center',
      formatter: (cell: Cell<T>): string =>
        htmlGenerator.result(cell.getValue()),
      headerSort: column.sortable,
      sorter: sorterWithTree(this.dataStore),
    };
  }
}
