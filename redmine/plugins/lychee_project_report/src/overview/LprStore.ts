type Data = {
  columnWidths: { [key: string]: number };
  openedProjects: number[];
};

export class LprStore {
  private __current: Data;

  constructor() {
    this.__current = { columnWidths: {}, openedProjects: [] };
  }

  get<K extends keyof Data>(key: K): Data[K] {
    this.load();
    return this.__current[key];
  }

  set<K extends keyof Data>(key: K, value: Data[K]): void {
    this.__current[key] = value;
    this.store();
  }

  private store(): void {
    $.cookie('lpr-store', JSON.stringify(this.__current));
  }

  private load(): Data {
    this.__current = JSON.parse(
      $.cookie('lpr-store') || '{ "columnWidths":{}, "openedProjects": [] }'
    );
    return this.__current;
  }
}
