import { Data } from './api';
import Big from 'big.js';

type MilestoneData = Data['source'][number]['milestone'];
type ResultData = Data['source'][number][string];
type Decision = ResultData['decision'];

const milestone = (data: MilestoneData): string => {
  const $doc = $('<div>');
  if (
    data.project.start_progress !== null &&
    data.project.start_progress >= 0 &&
    data.project.end_progress
  ) {
    // project bar
    const {
      term,
      name,
      start_progress: start,
      end_progress: end,
    } = data.project;
    const html = `
        <div class="project-term">
          <div class="project-term-bar" data-project-term="${term}" data-project-name="${name}" style="left: ${start}%; right: ${
      100 - end
    }%;">
            <div class="project-term-inner-bar" />
          </div>
        </div>
      `;
    $doc.append(html);
  }
  // lgc_milestones
  data.lgc_milestones.forEach((milestone) => {
    const html = `
        <div class="milestone-area">
          <span style="left: ${milestone.progress}%;">${milestone.name}</span>
        </div>
      `;
    $doc.append(html);
  });
  // versions
  data.versions.forEach((version) => {
    const {
      name,
      start_progress: start,
      end_progress: end,
      behind_progress: behind,
      actual_width: width,
      title_position: titlePosition,
    } = version;
    const html = `
        <div class="wrap-version" title="${name}">
          <div class="version-plan" style="left: ${start}%; right: ${
      100 - (end ?? 0)
    }%;"></div>
          ${
            behind !== null
              ? `<div class="version-plan re" style="left: ${start}%; right: ${
                  100 - behind
                }%;"></div>`
              : ''
          }
          <div class="version-actual" style="left: ${start}%; width: ${width}%;"></div>
          <div class="version-plan-arrow" style="left: ${end}%;"></div>
          <div class="version-title" style="left: ${titlePosition}%;">${name}</div>
        </div>
      `;
    $doc.append(html);
  });
  // today line
  const line = `
    <div class="progress-line" style="left: ${data.today_position}%;"></div>
  `;
  $doc.append(line);
  return '<div class="milestone">' + $doc.html() + '</div>';
};

const result = (data: ResultData): string => {
  if (data.eac !== undefined && data.budget !== undefined) {
    return eacComponent({
      eac: data.eac ?? 0,
      budget: data.budget ?? 0,
      decision: data.decision,
    });
  }
  let decision = '';
  switch (data.decision) {
    case 'green':
      decision = '<div class="curled-box bggr" />';
      break;
    case 'yellow':
      decision = '<div class="curled-box bgye" />';
      break;
    case 'red':
      decision = '<div class="curled-box bgre" />';
      break;
    case 'not_available':
      decision = '<div class="not-available">―</div>';
      break;
  }
  let result = data.result;
  if (result !== null) {
    if (data.unit === '%') {
      result = Big(result).times(100);
    }
    result = parseFloat(Big(result).round(2, Big.roundDown)); // 小数点2位まで
  }
  return `
    <div>${decision}</div>
    <div>${
      result !== null ? `${result.toLocaleString()}${data.unit ?? ''}` : ''
    }</div>
  `;
};

const eacComponent = (data: {
  eac: number;
  budget: number;
  decision: Decision;
}): string => {
  let decision = '';
  const budget = Math.round(data.budget); // 小数点以下表示しない
  if (budget === 0) {
    decision = '<div class="not-available">―</div>';
  } else {
    decision = budget.toLocaleString();
  }
  const eac = Math.round(data.eac); // 小数点以下表示しない
  if (eac <= 0) {
    return `
      <div>${decision}</div>
      <hr/>
      <div><div class="not-available">―</div></div>
    `;
  }

  // budget > 0 and EAC > 0 = decision
  let background = '';
  switch (data.decision) {
    case 'green':
      background = 'bggr';
      break;
    case 'yellow':
      background = 'bgye';
      break;
    case 'red':
      background = 'bgre';
      break;
  }

  return `
    <div class="${background}">
      <div>${decision}</div>
      <hr/>
      <div>${eac.toLocaleString()}</div>
    </div>
  `;
};

export const htmlGenerator = { milestone, result };
