export type Dataset = {
  columns: {
    field: string;
    title: string;
    sortable: boolean;
  }[];

  sorts: [string, 'asc' | 'desc'][];

  options: {
    tooltip: {
      name: string;
      period: string;
    };
    milestone: {
      months: { month: number; days: number }[];
      days: number;
      title: string;
      ext: string;
    };
  };
};
