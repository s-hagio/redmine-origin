import { Panel, PanelPosition, PanelType, Json } from "./interfaces/interface";
import PageVariables from "./pageVariables";

const csrfToken = document.getElementsByName(
  "csrf-token"
)[0] as HTMLMetaElement;

// E2EテストならCSRFトークンがない
const csrfHeader = csrfToken ? { "X-CSRF-TOKEN": csrfToken.content } : {};

const contentTypeJsonHeader = {
  "Content-Type": "application/json; charset=utf-8"
};

const panelsRootUrl = `${PageVariables.urls.panels}`;

export const fetchPanels = (): Promise<Response> => fetch(panelsRootUrl);

export const createPanel = (
  type: PanelType,
  position: PanelPosition
): Promise<Response> =>
  fetch(panelsRootUrl, {
    method: "POST",
    headers: { ...csrfHeader, ...contentTypeJsonHeader },
    body: JSON.stringify({
      lycheeProjectReportPanel: {
        type,
        position
      }
    })
  });

export const getPanel = (id: number, options = {}): Promise<Response> => {
  const qs = new URLSearchParams();
  Object.entries(options).forEach(([key, value]) =>
    qs.set(`options[${key}]`, value)
  );
  return fetch(`${panelsRootUrl}/${id}?${qs}`, {
    method: "GET",
    headers: { ...csrfHeader }
  });
};

export const updatePanel = (
  id: number,
  body: {
    settings?: Json;
    position?: PanelPosition;
  }
): Promise<Response> =>
  fetch(`${panelsRootUrl}/${id}`, {
    method: "PATCH",
    headers: { ...csrfHeader, ...contentTypeJsonHeader },
    body: JSON.stringify({
      lycheeProjectReportPanel: body
    })
  });

export const deletePanel = (panel: Panel): Promise<Response> =>
  fetch(`${panelsRootUrl}/${panel.id}`, {
    method: "DELETE",
    headers: csrfHeader
  });

export const createLayout = (): Promise<Response> =>
  fetch(PageVariables.urls.newLayout, {
    method: "POST",
    headers: csrfHeader
  });

export const copyLayout = (copyFrom: Json): Promise<Response> =>
  fetch(PageVariables.urls.newLayout, {
    method: "POST",
    headers: { ...csrfHeader, ...contentTypeJsonHeader },
    body: JSON.stringify({
      projectReportLayout: { copyFrom }
    })
  });

export const updateLayout = (body: Json): Promise<Response> =>
  fetch(PageVariables.urls.layout, {
    method: "PATCH",
    headers: { ...csrfHeader, ...contentTypeJsonHeader },
    body: JSON.stringify({
      projectReportLayout: body
    })
  });

export const deleteLayout = (): Promise<Response> =>
  fetch(PageVariables.urls.layout, {
    method: "DELETE",
    headers: csrfHeader
  });

export const createSharedLayout = (): Promise<Response> =>
  fetch(PageVariables.urls.createSharedLayout, {
    method: "POST",
    headers: csrfHeader
  });

export const sendRequest = (
  method: string,
  path: string,
  body: Json
): Promise<Response> => {
  const url = `${PageVariables.urls.root}/${path}`;
  return fetch(url, {
    method,
    headers: { ...csrfHeader, ...contentTypeJsonHeader },
    body: JSON.stringify(body)
  });
};

export const getResource = async <T>(
  path: string,
  searchParams?: object
): Promise<T> => {
  const url = new URL(`${PageVariables.urls.root}/${path}`);
  if (searchParams) {
    Object.entries(searchParams).forEach(([key, value]) => {
      if (Array.isArray(value)) {
        value.forEach(v => url.searchParams.append(key, v));
      } else {
        url.searchParams.append(key, value);
      }
    });
  }
  const response = await fetch(url.toString(), {
    method: "GET",
    headers: { ...csrfHeader }
  });

  return (await response.json()) as T;
};

export const renderIssues = (options = {}): Promise<Response> => {
  const qs = new URLSearchParams();
  Object.entries(options).forEach(([key, value]) => qs.set(key, value));
  return fetch(`${PageVariables.urls.renderIssues}?${qs}`, {
    method: "GET",
    headers: { ...csrfHeader }
  });
};
