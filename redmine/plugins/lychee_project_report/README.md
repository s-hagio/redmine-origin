# Lychee Project Report

## 本番環境の設定

### 日毎レポート作成

Redmine のルートディレクトリで下記を実行すると cron 設定ファイルに書き込まれる。

```console
$ bundle exec whenever -f plugins/lychee_project_report/config/schedule.rb --update-crontab
```

## 開発

### React のフロントエンドを起動する

プラグインのディレクトリで

```sh
yarn dev
```

### レポート作成

Redmine のルートディレクトリで

```sh
bundle exec rake redmine:plugins:lychee_project_report:daily_reporting
```
