import React from "react";
import ReactDOM from "react-dom";
import { RecoilRoot } from "recoil";
import { DndProvider } from "react-dnd";
import MouseBackend from "react-dnd-mouse-backend";

import { setClientConfig } from "@/api/client";
import { baseURLState } from "@/recoil/baseURLState";
import { manageableState } from "@/recoil/manageableState";
import { projectIdState } from "@/recoil/projectIdState";
import { importableProjectsState } from "@/recoil/importableProjectsState";
import { App } from "@/components/App";
import { DebugObserver } from "@/components/DebugObserver";
import i18n from "@/i18n";
import "./index.css";

const LycheeProjectDashboard = {
  start: (
    container: Element,
    {
      baseURL,
      manageable,
      projectId,
      importableProjects,
      locales,
    }: {
      baseURL: string;
      manageable: boolean;
      projectId: string;
      importableProjects: {
        identifier: string;
        name: string;
      }[];
      locales: Record<string, string>;
    }
  ) => {
    setClientConfig({ baseURL });
    i18n.addResourceBundle(i18n.language, "translation", locales);

    ReactDOM.render(
      <React.StrictMode>
        <RecoilRoot
          initializeState={({ set }) => {
            set(baseURLState, baseURL);
            set(manageableState, manageable);
            set(projectIdState, projectId);
            set(importableProjectsState, importableProjects);
          }}
        >
          {process.env.NODE_ENV === "development" && <DebugObserver />}
          <DndProvider backend={MouseBackend}>
            <App title={locales.dashboard} />
          </DndProvider>
        </RecoilRoot>
      </React.StrictMode>,
      container
    );
  },
};

declare global {
  interface Window {
    LycheeProjectDashboard: typeof LycheeProjectDashboard;
  }
}

window.LycheeProjectDashboard = LycheeProjectDashboard;
