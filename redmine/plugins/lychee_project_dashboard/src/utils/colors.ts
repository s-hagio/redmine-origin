const colors = [
  "#7bd148", // Green
  "#21428b", // Indigo
  "#5484ed", // Bold blue
  "#a4bdfc", // Blue
  "#46d6db", // Turquoise
  "#7ae7bf", // Light green
  "#51b749", // Bold green
  "#D9CA00", // Lemon Yellow
  "#fbd75b", // Yellow
  "#ffb878", // Orange
  "#ff887c", // Red
  "#dc2127", // Bold red
  "#ff0099", // Pink
  "#dbadff", // Purple
  "#e1e1e1", // Gray
] as const;

export const getColor = (index: number): typeof colors[number] =>
  colors[index % colors.length];
