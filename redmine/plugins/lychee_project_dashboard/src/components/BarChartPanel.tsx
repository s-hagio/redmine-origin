import React from "react";
import { useRecoilValue } from "recoil";
import { projectIdState } from "@/recoil/projectIdState";
import useAspidaSWR from "@/hooks/useAspidaSWR";
import { createClient } from "@/api/client";
import { EmptyConfig } from "@/components/EmptyConfig";
import { Warning } from "@/components/Warning";
import { BarChartPanelEditor } from "@/components/BarChartPanelEditor";
import { BarChart } from "@/components/react-google-charts/BarChart";
import { LinkToIssues } from "@/components/LinkToIssues";
import type { BarChartPanelType } from "@/types";

type Props = {
  edit: boolean;
  endEdit: () => void;
  panel: BarChartPanelType;
};

export const BarChartPanel: React.VFC<Props> = ({
  edit,
  endEdit,
  panel: { id, title, config },
}) => {
  const projectId = useRecoilValue(projectIdState);
  const client = createClient({ caseConvert: false });
  const { data: resData, mutate } = useAspidaSWR(
    client.lychee_project_dashboard.projects
      ._projectId(projectId)
      .bar_chart_panels._id(id),
    {
      suspense: true,
    }
  );
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const data = resData!;

  if (edit) {
    return (
      <BarChartPanelEditor
        id={id}
        title={title}
        config={config}
        onUpdate={mutate}
        onClose={endEdit}
      />
    );
  }

  if (!data.success) {
    return <Warning>{data.errors}</Warning>;
  }

  const {
    row_target_ids: rowTargetIds,
    row_targets: rowTargets,
    column_target_ids: columnTargetIds,
    column_targets: columnTargets,
    issues,
  } = data;
  const labels = columnTargetIds.map((id) => columnTargets[id]);
  const chartData = rowTargetIds.map((rowTargetId) => [
    rowTargets[rowTargetId],
    ...columnTargetIds.map(
      (columnTargetId) => (issues[rowTargetId] ?? {})[columnTargetId] ?? 0
    ),
  ]) as [string, ...number[]][];

  if (!config) {
    return <EmptyConfig />;
  }

  return (
    <>
      <BarChart labels={labels} data={chartData} />
      <div className="flex justify-end">
        <LinkToIssues queryId={config.queryId} noFilter={true} />
      </div>
    </>
  );
};
