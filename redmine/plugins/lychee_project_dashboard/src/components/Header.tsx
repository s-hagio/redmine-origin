import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { useTranslation } from "react-i18next";
import { focusPanelIdState } from "@/recoil/focusPanelIdState";
import { createClient } from "@/api/client";
import { manageableState } from "@/recoil/manageableState";
import { projectIdState } from "@/recoil/projectIdState";

type Props = {
  title: string;
};

export const Header: React.VFC<Props> = ({ title }) => {
  const manageable = useRecoilValue(manageableState);
  const projectId = useRecoilValue(projectIdState);
  const ref = React.useRef<HTMLUListElement>(null);
  const setFocusPanelId = useSetRecoilState(focusPanelIdState);
  const { t } = useTranslation();
  const client = createClient();

  const panels = [
    { value: "PieChartPanel", label: t("pieChartPanel") },
    { value: "BarChartPanel", label: t("barChartPanel") },
    // { value: "IssueDurationPanel", label: t("issueDurationPanel") },
    { value: "IssueListPanel", label: t("issueListPanel") },
  ] as const;

  const handleClick =
    (value: typeof panels[number]["value"]) => async (): Promise<void> => {
      ref.current?.blur();
      const data = await client.lychee_project_dashboard.projects
        ._projectId(projectId)
        .panels.$post({
          body: {
            type: value,
          },
        });

      if (data.success) {
        setFocusPanelId(data.id);
      }
    };

  return (
    <div className="flex">
      <div className="pt-0.5 pr-2.5 pb-[0.0625rem] text-[1.25rem] font-bold text-[#555]">
        <div>{title}</div>
      </div>
      {manageable && (
        <div className="dropdown">
          <div className="flex h-full items-center">
            <label tabIndex={0} className="cursor-pointer">
              <span className="icon icon-add" />
              {t("newPanel")}
            </label>
          </div>
          <ul
            ref={ref}
            tabIndex={0}
            className="dropdown-content menu z-10 w-52 rounded-sm bg-white p-2 shadow"
          >
            {panels.map(({ value, label }) => (
              <li key={value} className="hover:bg-gray-50">
                <a onClick={handleClick(value)}>{label}</a>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};
