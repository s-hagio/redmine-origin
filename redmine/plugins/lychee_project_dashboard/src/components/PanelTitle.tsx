import { useRecoilValue } from "recoil";
import type { ConnectDragSource } from "react-dnd";
import { manageableState } from "@/recoil/manageableState";
import { panelState } from "@/recoil/panelState";
import { useRemovePanel } from "@/hooks/useRemovePanel";

type Props = {
  id: number;
  onEditClick: () => void;
  connectDragSource?: ConnectDragSource;
};

export const PanelTitle: React.VFC<Props> = ({
  id,
  onEditClick,
  connectDragSource,
}) => {
  const manageable = useRecoilValue(manageableState);
  const panel = useRecoilValue(panelState(id));
  const { remove } = useRemovePanel();

  if (!panel) {
    return null;
  }

  const { title } = panel;

  return (
    <div className="flex justify-between">
      <h2
        ref={connectDragSource}
        className={`flex-1 text-xl ${manageable ? "cursor-grab" : ""}`}
      >
        {title}
      </h2>
      {manageable && (
        <div className="flex gap-2.5">
          <a
            className="icon icon-settings cursor-pointer"
            onClick={onEditClick}
          />
          <a
            className="icon icon-close cursor-pointer"
            onClick={remove(id)}
          />
        </div>
      )}
    </div>
  );
};
