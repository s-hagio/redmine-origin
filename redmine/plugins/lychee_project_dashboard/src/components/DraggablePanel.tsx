import React from "react";
import { usePanelFocus } from "@/hooks/usePanelFocus";
import { usePanelDrag } from "@/hooks/usePanelDrag";
import { usePanelDrop } from "@/hooks/usePanelDrop";
import { CustomPanel } from "@/components/CustomPanel";

type Props = {
  id: number;
  title: string;
  aligned: "left" | "right";
  position: number;
};

export const DraggablePanel: React.VFC<Props> = (props) => {
  const { id } = props;
  const ref = React.useRef<HTMLDivElement>(null);
  usePanelFocus(id, ref);

  const { drop, handlerId } = usePanelDrop(ref, props);
  const { drag, opacity } = usePanelDrag(props);

  drop(ref);

  return (
    <div ref={ref} style={{ opacity }} data-handler-id={handlerId}>
      <CustomPanel id={id} connectDragSource={drag} />
    </div>
  );
};
