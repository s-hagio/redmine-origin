import React from "react";

type Props = {
  fallback: React.ReactNode;
};

export class ErrorBoundary extends React.Component<Props> {
  state = { hasError: false, error: null };

  static getDerivedStateFromError<T>(error: T): {
    hasError: boolean;
    error: T;
  } {
    return {
      hasError: true,
      error,
    };
  }

  render(): React.ReactNode {
    if (this.state.hasError) {
      return this.props.fallback;
    }
    return this.props.children;
  }
}
