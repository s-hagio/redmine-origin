import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { useTranslation } from "react-i18next";
import { createClient } from "@/api/client";
import { panelState } from "@/recoil/panelState";
import { useIssueQueries } from "@/hooks/useIssueQueries";
import { useTargetAttributes } from "@/hooks/useTargetAttributes";
import { BarChartPanelType } from "@/types";
import { projectIdState } from "@/recoil/projectIdState";

type Props = {
  id: BarChartPanelType["id"];
  title: BarChartPanelType["title"];
  config: BarChartPanelType["config"];
  onUpdate: () => void;
  onClose: () => void;
};

export const BarChartPanelEditor: React.VFC<Props> = ({
  id,
  title: defaultTitle,
  config,
  onUpdate,
  onClose,
}) => {
  const { t } = useTranslation();
  const targetAttributes = useTargetAttributes();
  const projectId = useRecoilValue(projectIdState);
  const [title, setTitle] = React.useState(defaultTitle);
  const [queryId, setQueryId] = React.useState(config?.queryId ?? null);
  const [rowTarget, setRowTarget] = React.useState<string>(
    config && targetAttributes.map((a) => a.value).includes(config.rowTarget)
      ? config.rowTarget
      : targetAttributes[0].value
  );
  const [columnTarget, setColumnTarget] = React.useState<string>(
    config && targetAttributes.map((a) => a.value).includes(config.columnTarget)
      ? config.columnTarget
      : targetAttributes[1].value
  );
  const setPanel = useSetRecoilState(panelState(id));
  const { queries } = useIssueQueries();

  const handleChangeTitle = (
    e: React.SyntheticEvent<HTMLInputElement>
  ): void => {
    setTitle(e.currentTarget.value);
  };

  const handleChangeQueryId = (
    e: React.SyntheticEvent<HTMLSelectElement>
  ): void => {
    const { value } = e.currentTarget;
    setQueryId(value === "" ? null : Number(value));
  };

  const handleChangeRowTarget = (
    e: React.SyntheticEvent<HTMLSelectElement>
  ): void => {
    const { value } = e.currentTarget;
    setRowTarget(value);
  };

  const handleChangeColumnTarget = (
    e: React.SyntheticEvent<HTMLSelectElement>
  ): void => {
    const { value } = e.currentTarget;
    setColumnTarget(value);
  };

  const client = createClient();
  const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>): void => {
    e.preventDefault();
    setPanel((currVal) =>
      currVal && currVal.type === "BarChartPanel"
        ? {
            ...currVal,
            title,
            config: { queryId, rowTarget, columnTarget },
          }
        : currVal
    );
    (async () => {
      await client.lychee_project_dashboard.projects
        ._projectId(projectId)
        .bar_chart_panels._id(id)
        .$patch({
          body: {
            panel: {
              title,
              configuration_attributes: {
                queryId,
                rowTarget,
                columnTarget,
              },
            },
          },
        });
      onUpdate();
    })();
    onClose();
  };

  return (
    <form className="space-y-2" onSubmit={handleSubmit}>
      <div className="space-y-1">
        <label className="block">{t("title")}</label>
        <input
          className="w-96 border px-2 py-4"
          value={title}
          onChange={handleChangeTitle}
          maxLength={30}
        />
      </div>
      <div className="space-y-1">
        <label className="block">{t("customQuery")}</label>
        <select
          className="h-full border py-2 pl-2 pr-6"
          value={queryId ?? undefined}
          onChange={handleChangeQueryId}
        >
          <option />
          {queries.map(({ id, name }) => (
            <option key={id} value={id}>
              {name}
            </option>
          ))}
        </select>
      </div>
      <div className="space-y-1">
        <label className="block">{t("yAsix")}</label>
        <select
          className="h-full border py-2 pl-2 pr-6"
          value={rowTarget}
          onChange={handleChangeRowTarget}
        >
          {targetAttributes.map((target) => (
            <option key={target.value} value={target.value}>
              {target.label}
            </option>
          ))}
        </select>
      </div>
      <div className="space-y-1">
        <label className="block">{t("series")}</label>
        <select
          className="h-full border py-2 pl-2 pr-6"
          value={columnTarget}
          onChange={handleChangeColumnTarget}
        >
          {targetAttributes.map((target) => (
            <option key={target.value} value={target.value}>
              {target.label}
            </option>
          ))}
        </select>
      </div>
      <div>
        <button className="h-full border px-2 py-1 text-sm" type="submit">
          {t("buttonSave")}
        </button>
        <button className="h-full border px-2 py-1 text-sm" onClick={onClose}>
          {t("buttonCancel")}
        </button>
      </div>
    </form>
  );
};
