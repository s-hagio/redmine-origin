import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { useTranslation } from "react-i18next";
import { createClient } from "@/api/client";
import { projectIdState } from "@/recoil/projectIdState";
import { panelState } from "@/recoil/panelState";
import { useIssueQueries } from "@/hooks/useIssueQueries";
import { IssueListPanelType } from "@/types";

type Props = {
  id: IssueListPanelType["id"];
  title: IssueListPanelType["title"];
  config: IssueListPanelType["config"];
  onUpdate: () => void;
  onClose: () => void;
};

const DEFAULT_LIMIT = 50;

export const IssueListPanelEditor: React.VFC<Props> = ({
  id,
  title: defaultTitle,
  config,
  onUpdate,
  onClose,
}) => {
  const { t } = useTranslation();
  const projectId = useRecoilValue(projectIdState);
  const [title, setTitle] = React.useState(defaultTitle);
  const [queryId, setQueryId] = React.useState(config?.queryId ?? null);
  const [limit, setLimit] = React.useState<number | null>(
    config?.limit ?? DEFAULT_LIMIT
  );
  const setPanel = useSetRecoilState(panelState(id));
  const { queries } = useIssueQueries();

  const handleChangeTitle = (
    e: React.SyntheticEvent<HTMLInputElement>
  ): void => {
    setTitle(e.currentTarget.value);
  };

  const handleChangeQueryId = (
    e: React.SyntheticEvent<HTMLSelectElement>
  ): void => {
    const { value } = e.currentTarget;
    setQueryId(value === "" ? null : Number(value));
  };

  const handleChangeLimit = (
    e: React.SyntheticEvent<HTMLInputElement>
  ): void => {
    const { value } = e.currentTarget;
    setLimit(value === "" ? null : Number(value));
  };

  const client = createClient();
  const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>): void => {
    e.preventDefault();
    setPanel((currVal) =>
      currVal && currVal.type === "IssueListPanel"
        ? {
            ...currVal,
            title,
            config: { queryId, limit: limit ?? DEFAULT_LIMIT },
          }
        : currVal
    );
    (async () => {
      await client.lychee_project_dashboard.projects
        ._projectId(projectId)
        .issue_list_panels._id(id)
        .$patch({
          body: {
            panel: {
              title,
              configuration_attributes: {
                queryId,
                limit: limit ?? DEFAULT_LIMIT,
              },
            },
          },
        });
      onUpdate();
    })();
    onClose();
  };

  return (
    <form className="space-y-2" onSubmit={handleSubmit}>
      <div className="space-y-1">
        <label className="block">{t("title")}</label>
        <input
          className="w-96 border px-2 py-4"
          value={title}
          onChange={handleChangeTitle}
          maxLength={30}
        />
      </div>
      <div className="space-y-1">
        <label className="block">{t("customQuery")}</label>
        <select
          className="h-full border py-2 pl-2 pr-6"
          value={queryId ?? undefined}
          onChange={handleChangeQueryId}
        >
          <option />
          {queries.map(({ id, name }) => (
            <option key={id} value={id}>
              {name}
            </option>
          ))}
        </select>
      </div>
      <div className="space-y-1">
        <label className="block">{t("maxRows")}</label>
        <input
          type="number"
          className="w-12 border px-2 py-4 [appearance:none]"
          value={limit ?? ""}
          onChange={handleChangeLimit}
          required
          min={1}
          max={DEFAULT_LIMIT}
        />
      </div>
      <div>
        <button className="h-full border px-2 py-1 text-sm" type="submit">
          {t("buttonSave")}
        </button>
        <button className="h-full border px-2 py-1 text-sm" onClick={onClose}>
          {t("buttonCancel")}
        </button>
      </div>
    </form>
  );
};
