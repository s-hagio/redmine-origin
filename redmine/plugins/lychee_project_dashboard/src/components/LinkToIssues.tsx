import { useRecoilValue } from "recoil";
import { useTranslation } from "react-i18next";
import { baseURLState } from "@/recoil/baseURLState";
import { projectIdState } from "@/recoil/projectIdState";

type Props = {
  queryId: number | null;
  noFilter?: boolean; // フィルターをかけない（＝完了チケットもすべて表示）
};

export const LinkToIssues: React.FC<Props> = ({ queryId, noFilter }) => {
  const { t } = useTranslation();
  const baseURL = useRecoilValue(baseURLState);
  const projectId = useRecoilValue(projectIdState);
  const url = `${baseURL}/projects/${projectId}/issues?${
    queryId
      ? `query_id=${queryId}`
      : `set_filter=1${
          noFilter
            ? "&sort=id:desc&f[]=&c[]=tracker&c[]=status&c[]=priority&c[]=subject&c[]=assigned_to&c[]=updated_on&group_by=&t[]="
            : ""
        }`
  }`;

  return (
    <a href={url} target="_blank" rel="noreferrer">
      {t("issuesLink")}
    </a>
  );
};
