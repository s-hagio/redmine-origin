import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { useTranslation } from "react-i18next";
import { createClient } from "@/api/client";
import { panelState } from "@/recoil/panelState";
import { projectIdState } from "@/recoil/projectIdState";
import { useIssueQueries } from "@/hooks/useIssueQueries";
import { useTargetAttributes } from "@/hooks/useTargetAttributes";
import { PieChartPanelType } from "@/types";

type Props = {
  id: PieChartPanelType["id"];
  title: PieChartPanelType["title"];
  config: PieChartPanelType["config"];
  onUpdate: () => void;
  onClose: () => void;
};

export const PieChartPanelEditor: React.VFC<Props> = ({
  id,
  title: defaultTitle,
  config,
  onUpdate,
  onClose,
}) => {
  const { t } = useTranslation();
  const targetAttributes = useTargetAttributes();
  const [title, setTitle] = React.useState(defaultTitle);
  const [queryId, setQueryId] = React.useState(config?.queryId ?? null);
  const [target, setTarget] = React.useState<string>(
    config && targetAttributes.map((a) => a.value).includes(config.target)
      ? config.target
      : targetAttributes[0].value
  );
  const setPanel = useSetRecoilState(panelState(id));
  const projectId = useRecoilValue(projectIdState);
  const { queries } = useIssueQueries();

  const handleChangeTitle = (
    e: React.SyntheticEvent<HTMLInputElement>
  ): void => {
    setTitle(e.currentTarget.value);
  };

  const handleChangeQueryId = (
    e: React.SyntheticEvent<HTMLSelectElement>
  ): void => {
    const { value } = e.currentTarget;
    setQueryId(value === "" ? null : Number(value));
  };

  const handleChangeTarget = (
    e: React.SyntheticEvent<HTMLSelectElement>
  ): void => {
    const { value } = e.currentTarget;
    setTarget(value);
  };

  const client = createClient();
  const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>): void => {
    e.preventDefault();
    setPanel((currVal) =>
      currVal && currVal.type === "PieChartPanel"
        ? {
            ...currVal,
            title,
            config: { queryId, target },
          }
        : currVal
    );
    (async () => {
      await client.lychee_project_dashboard.projects
        ._projectId(projectId)
        .pie_chart_panels._id(id)
        .$patch({
          body: {
            panel: {
              title,
              configuration_attributes: {
                queryId,
                target,
              },
            },
          },
        });
      onUpdate();
    })();
    onClose();
  };

  return (
    <form className="space-y-2" onSubmit={handleSubmit}>
      <div className="space-y-1">
        <label className="block">{t("title")}</label>
        <input
          className="w-96 border px-2 py-4"
          value={title}
          onChange={handleChangeTitle}
          maxLength={30}
        />
      </div>
      <div className="space-y-1">
        <label className="block">{t("customQuery")}</label>
        <select
          className="h-full border py-2 pl-2 pr-6"
          value={queryId ?? undefined}
          onChange={handleChangeQueryId}
        >
          <option />
          {queries.map(({ id, name }) => (
            <option key={id} value={id}>
              {name}
            </option>
          ))}
        </select>
      </div>
      <div className="space-y-1">
        <label className="block">{t("aggregateTarget")}</label>
        <select
          className="h-full border py-2 pl-2 pr-6"
          value={target}
          onChange={handleChangeTarget}
        >
          {targetAttributes.map((target) => (
            <option key={target.value} value={target.value}>
              {target.label}
            </option>
          ))}
        </select>
      </div>
      <div>
        <button className="h-full border px-2 py-1 text-sm" type="submit">
          {t("buttonSave")}
        </button>
        <button className="h-full border px-2 py-1 text-sm" onClick={onClose}>
          {t("buttonCancel")}
        </button>
      </div>
    </form>
  );
};
