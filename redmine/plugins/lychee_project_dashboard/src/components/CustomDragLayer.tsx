import { useDragLayer, XYCoord } from "react-dnd";
import { ItemTypes, DragItem } from "@/dnd";

const getItemStyles = (
  initialOffset: XYCoord | null,
  currentOffset: XYCoord | null
): React.CSSProperties => {
  if (!initialOffset || !currentOffset) {
    return {
      display: "none",
    };
  }

  const { x, y } = currentOffset;

  const transform = `translate(${x}px, ${y}px)`;
  return {
    transform,
    WebkitTransform: transform,
  };
};

export const CustomDragLayer: React.VFC = () => {
  const { itemType, isDragging, item, initialOffset, currentOffset } =
    useDragLayer((monitor) => ({
      item: monitor.getItem() as DragItem,
      itemType: monitor.getItemType(),
      initialOffset: monitor.getInitialSourceClientOffset(),
      currentOffset: monitor.getSourceClientOffset(),
      isDragging: monitor.isDragging(),
    }));

  const renderItem = (): React.ReactNode => {
    switch (itemType) {
      case ItemTypes.PANEL:
        return (
          <div className="h-full rounded-[5px] border border-[gray] bg-white p-2.5 shadow-[1px_1px_2px_darkgray]">
            <h2 className="px-2 text-xl">{item.title}</h2>
          </div>
        );
      default:
        return null;
    }
  };

  if (!isDragging) {
    return null;
  }

  return (
    <div className="pointer-events-none fixed left-0 top-0 z-10 h-20 w-[calc((100%-24px)/2)]">
      <div
        className="h-full"
        style={getItemStyles(initialOffset, currentOffset)}
      >
        {renderItem()}
      </div>
    </div>
  );
};
