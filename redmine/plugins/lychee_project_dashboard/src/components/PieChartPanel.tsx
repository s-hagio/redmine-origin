import React from "react";
import { useRecoilValue } from "recoil";
import { projectIdState } from "@/recoil/projectIdState";
import useAspidaSWR from "@/hooks/useAspidaSWR";
import { createClient } from "@/api/client";
import { PieChartPanelEditor } from "@/components/PieChartPanelEditor";
import { PieChart } from "@/components/react-google-charts/PieChart";
import { LinkToIssues } from "@/components/LinkToIssues";
import { EmptyConfig } from "@/components/EmptyConfig";
import { Warning } from "@/components/Warning";
import type { PieChartPanelType } from "@/types";

type Props = {
  edit: boolean;
  endEdit: () => void;
  panel: PieChartPanelType;
};

export const PieChartPanel: React.VFC<Props> = ({
  edit,
  endEdit,
  panel: { id, title, config },
}) => {
  const projectId = useRecoilValue(projectIdState);
  const client = createClient();
  const { data: resData, mutate } = useAspidaSWR(
    client.lychee_project_dashboard.projects
      ._projectId(projectId)
      .pie_chart_panels._id(id),
    {
      suspense: true,
    }
  );
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const data = resData!;

  if (edit) {
    return (
      <PieChartPanelEditor
        id={id}
        title={title}
        config={config}
        onUpdate={mutate}
        onClose={endEdit}
      />
    );
  }

  if (!data.success) {
    return <Warning>{data.errors}</Warning>;
  }

  if (!config) {
    return <EmptyConfig />;
  }

  return (
    <>
      <PieChart data={data.result} />
      <div className="flex justify-end">
        <LinkToIssues queryId={config.queryId} noFilter={true} />
      </div>
    </>
  );
};
