import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import { PieChart } from "./PieChart";

export default {
  title: "chartjs/PieChart",
  component: PieChart,
  decorators: [
    (Story) => (
      <div style={{ height: "90vh" }}>
        <Story />
      </div>
    ),
  ],
} as ComponentMeta<typeof PieChart>;

const labels = ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"];
const data = [12, 19, 3, 5, 2, 3];

export const Default: ComponentStoryObj<typeof PieChart> = {
  args: {
    data: labels.map((l, i) => [l, data[i]]),
  },
};
