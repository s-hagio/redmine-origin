import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import { BarChart } from "./BarChart";

export default {
  title: "chartjs/BarChart",
  component: BarChart,
  decorators: [
    (Story) => (
      <div style={{ height: "90vh" }}>
        <Story />
      </div>
    ),
  ],
} as ComponentMeta<typeof BarChart>;

const data = {
  labels: ["佐藤", "鈴木", "本田"],
  datasets: [
    {
      label: "TODO",
      data: [1, 0, 2],
      backgroundColor: "rgb(255, 99, 132)",
    },
    {
      label: "DOING",
      data: [2, 3, 4],
      backgroundColor: "rgb(75, 192, 192)",
    },
    {
      label: "DONE",
      data: [2, 0, 0],
      backgroundColor: "rgb(53, 162, 235)",
    },
  ],
};

export const Default: ComponentStoryObj<typeof BarChart> = {
  args: {
    data,
  },
};
