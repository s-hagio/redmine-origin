import {
  Chart as ChartJS,
  ArcElement,
  Tooltip,
  Legend,
  ChartDataset,
} from "chart.js";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { ChartProps, Doughnut } from "react-chartjs-2";

ChartJS.register(ArcElement, Tooltip, Legend);

const options: ChartProps<"doughnut">["options"] = {
  plugins: {
    legend: {
      position: "right",
    },
    datalabels: {
      anchor: "end",
      align: "end",
      offset: 10,
      formatter: (_, context) => {
        const labels = context.chart.data.labels;
        const label = labels ? labels[context.dataIndex] : "";
        const dataset = context.dataset as ChartDataset<"doughnut">;
        const sum = dataset.data.reduce((p, c) => p + c);
        const ratio = (dataset.data[context.dataIndex] * 100) / sum;
        return `${label}\n${ratio.toFixed(2)}%`;
      },
    },
  },
  layout: {
    padding: 50,
  },
  maintainAspectRatio: false,
};

type Props = {
  data: [string, number][];
};

export const PieChart: React.VFC<Props> = ({ data }) => {
  return (
    <Doughnut
      plugins={[ChartDataLabels]}
      options={options}
      height={300}
      data={{
        labels: data.map((d) => d[0]),
        datasets: [
          {
            backgroundColor: colors,
            borderWidth: 1,
            data: data.map((d) => d[1]),
          },
        ],
      }}
    />
  );
};

const colors = [
  "#7bd148", // Green
  "#21428b", // Indigo
  "#5484ed", // Bold blue
  "#a4bdfc", // Blue
  "#46d6db", // Turquoise
  "#7ae7bf", // Light green
  "#51b749", // Bold green
  "#D9CA00", // Lemon Yellow
  "#fbd75b", // Yellow
  "#ffb878", // Orange
  "#ff887c", // Red
  "#dc2127", // Bold red
  "#ff0099", // Pink
  "#dbadff", // Purple
  "#e1e1e1", // Gray
] as const;
