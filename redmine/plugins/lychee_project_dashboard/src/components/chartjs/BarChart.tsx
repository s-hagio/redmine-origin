import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar, ChartProps } from "react-chartjs-2";

ChartJS.register(CategoryScale, LinearScale, BarElement, Tooltip, Legend);

const options: ChartProps<"bar">["options"] = {
  plugins: {
    legend: { position: "right" },
  },
  indexAxis: "y",
  responsive: true,
  interaction: {
    mode: "nearest",
    axis: "y",
    intersect: false,
  },
  scales: {
    x: {
      stacked: true,
    },
    y: {
      stacked: true,
    },
  },
};

type Props = {
  data: ChartProps<"bar">["data"];
  height?: number;
};

export const BarChart: React.VFC<Props> = ({ data, height }) => {
  return <Bar options={options} height={height} data={data} />;
};
