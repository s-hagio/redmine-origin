import {
  Chart as ChartJS,
  LinearScale,
  TimeSeriesScale,
  TimeScale,
  PointElement,
  LineElement,
  Tooltip,
  Legend,
} from "chart.js";
import { ChartProps, Line } from "react-chartjs-2";
import { format } from "date-fns";
import "chartjs-adapter-date-fns";

ChartJS.register(
  LinearScale,
  TimeSeriesScale,
  TimeScale,
  PointElement,
  LineElement,
  Tooltip,
  Legend
);

const FORMAT = "yyyy-MM-dd" as const;

const options: ChartProps<"line">["options"] = {
  responsive: true,
  plugins: {
    legend: {
      position: "top",
    },
    tooltip: {
      callbacks: {
        title: (tooltipItems) => format(tooltipItems[0].parsed.x, FORMAT),
      },
    },
  },
  interaction: {
    mode: "nearest",
    axis: "x",
    intersect: false,
  },
  scales: {
    x: {
      type: "time",
      time: {
        unit: "day",
        displayFormats: {
          day: FORMAT,
        },
      },
    },
  },
};

type Props = {
  data: ChartProps<"line">["data"];
};

export const LineChart: React.VFC<Props> = ({ data }) => {
  return <Line options={options} data={data} />;
};
