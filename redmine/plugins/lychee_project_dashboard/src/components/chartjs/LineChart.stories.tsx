import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import { LineChart } from "./LineChart";

export default {
  title: "chartjs/LineChart",
  component: LineChart,
  decorators: [
    (Story) => (
      <div style={{ height: "90vh" }}>
        <Story />
      </div>
    ),
  ],
} as ComponentMeta<typeof LineChart>;

const data = {
  labels: ["2022-01-01", "2022-01-02", "2022-01-04"],
  datasets: [
    {
      label: "予定",
      data: [10, 5, 0],
      borderColor: "rgb(53, 162, 235)",
      backgroundColor: "rgba(53, 162, 235, 0.5)",
    },
    {
      label: "実績",
      data: [10, 8, 4],
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 99, 132, 0.5)",
    },
  ],
};

export const Default: ComponentStoryObj<typeof LineChart> = {
  args: { data },
};
