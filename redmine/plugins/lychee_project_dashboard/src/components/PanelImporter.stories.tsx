import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import { Component } from "./PanelImporter";

export default {
  title: "PanelImporter",
  component: Component,
  decorators: [
    (Story) => (
      <div style={{ height: "100vh" }}>
        <Story />
      </div>
    ),
  ],
} as ComponentMeta<typeof Component>;

export const Basic: ComponentStoryObj<typeof Component> = {
  args: {
    projects: [
      {
        identifier: "project-1",
        name: "Project 1",
      },
    ],
    selected: "project-1",
    onChange: () => {
      return;
    },
    onSubmit: () => {
      return;
    },
  },
};
