import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import { BarChart } from "./BarChart";

export default {
  title: "google/BarChart",
  component: BarChart,
  decorators: [
    (Story) => (
      <div style={{ height: "100vh" }}>
        <Story />
      </div>
    ),
  ],
} as ComponentMeta<typeof BarChart>;

export const Basic: ComponentStoryObj<typeof BarChart> = {
  args: {
    labels: ["2000", "2010"],
    data: [
      ["New York City, NY", 8175000, 8008000],
      ["Los Angeles, CA", 3792000, 3694000],
      ["Chicago, IL", 2695000, 2896000],
      ["Houston, TX", 2099000, 1953000],
      ["Philadelphia, PA", 1526000, 1517000],
    ],
  },
};
