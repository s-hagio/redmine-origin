import { Chart } from "react-google-charts";
import { ChartWrapperOptions } from "react-google-charts/dist/types";
import { getColor } from "@/utils/colors";
import { NoData } from "@/components/NoData";

const options: ChartWrapperOptions["options"] = {
  fontSize: 12,
  annotations: {
    alwaysOutside: true,
    textStyle: {
      color: "#000000",
    },
  },
  chartArea: {
    top: 50,
    left: 125,
    right: 50,
    bottom: 20,
    width: "75%",
    height: "90%",
  },
  focusTarget: "category",
  hAxis: {
    minValue: 0,
  },
  isStacked: true,
  legend: {
    position: "top",
    maxLines: 3,
  },
};

type Props = {
  id?: number;
  labels: string[];
  data: [string, ...number[]][];
};

export const BarChart: React.VFC<Props> = ({ id, labels, data }) => {
  if (labels.length === 0 || data.length === 0) {
    return <NoData />;
  }

  return (
    <Chart
      key={`${id}-${data.length}`}
      className="overflow-hidden"
      chartType="BarChart"
      height={Math.max(200, data.length * 50)}
      data={[
        ["", ...labels, { role: "annotation" }],
        ...data.map((datum) => {
          const [name, ...values] = datum;
          return [name, ...values, values.reduce((p, c) => p + c)];
        }),
      ]}
      options={{
        ...options,
        series: labels.map((_, index) => ({ color: getColor(index) })),
      }}
    />
  );
};
