import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import { PieChart } from "./PieChart";

export default {
  title: "google/PieChart",
  component: PieChart,
  decorators: [
    (Story) => (
      <div style={{ height: "100vh" }}>
        <Story />
      </div>
    ),
  ],
} as ComponentMeta<typeof PieChart>;

const data = [
  ["Work", 11],
  ["Eat", 2],
  ["Commute", 2],
  ["Watch TV", 2],
  ["Sleep", 7],
] as [string, number][];

export const Basic: ComponentStoryObj<typeof PieChart> = {
  args: {
    data,
  },
};
