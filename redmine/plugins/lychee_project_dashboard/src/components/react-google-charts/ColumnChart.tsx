import { Chart } from "react-google-charts";
import type { ChartWrapperOptions } from "react-google-charts/dist/types";
import { NoData } from "@/components/NoData";

const NORMAL_COLOR = "#adc946"; // Green
const OVER_COLOR = "#d66565"; // Red

const options: ChartWrapperOptions["options"] = {
  fontSize: 12,
  annotations: {
    alwaysOutside: true,
    textStyle: {
      color: "#000000",
    },
  },
  chartArea: {
    top: 50,
    left: 125,
    right: 50,
    bottom: 20,
    width: "75%",
    height: "90%",
  },
  legend: "none",
};

type Props = {
  id?: number;
  data: {
    name: string;
    value: number;
    tooltipText: string;
  }[];
  thresholdIndex: number;
  onSelect: (selection: any) => void;
};

export const ColumnChart: React.VFC<Props> = ({ id, data, thresholdIndex, onSelect }) => {
  if (data.length === 0) {
    return <NoData />;
  }

  return (
    <Chart
      key={`${id}-${data.length}`}
      className="overflow-hidden"
      chartType="ColumnChart"
      chartEvents={[
        {
          eventName: "select",
          callback: ({ chartWrapper }) => {
            onSelect(chartWrapper.getChart().getSelection()[0]);
          },
        },
      ]}
      height={200}
      data={[
        ["", "", { role: "style" }, { role: "tooltip", type: "string" }],
        ...data.map(({ name, value, tooltipText }, i) => [name, value, i < thresholdIndex ? NORMAL_COLOR : OVER_COLOR, `${tooltipText}: ${value}`]),
      ]}
      options={options}
    />
  );
};
