import { Chart } from "react-google-charts";
import { ChartWrapperOptions } from "react-google-charts/dist/types";
import { getColor } from "@/utils/colors";
import { NoData } from "@/components/NoData";

const options: ChartWrapperOptions["options"] = {
  chartArea: {
    height: "90%",
  },
  legend: {
    position: "labeled",
  },
  pieHole: 0.4,
  pieSliceText: "none",
  theme: "maximized",
};

type Props = {
  data: [string, number][];
};

export const PieChart: React.VFC<Props> = ({ data }) => {
  if (data.length === 0 || data.every((v) => v[1] === 0)) {
    return <NoData />;
  }

  return (
    <Chart
      className="overflow-hidden"
      chartType="PieChart"
      width="100%"
      height={250}
      data={[["", ""], ...data]}
      options={{
        ...options,
        slices: data.map((_, index) => ({
          color: getColor(index),
        })),
      }}
    />
  );
};
