import { useTranslation } from "react-i18next";
import { Warning } from "@/components/Warning";

export const NoData: React.VFC = () => {
  const { t } = useTranslation();

  return <Warning>{t("noData")}</Warning>;
};
