import React from "react";
import { useDrop } from "react-dnd";
import { ItemTypes } from "@/dnd";

type Props = {
  position: "top" | "bottom";
};

export const ScrollHandler: React.VFC<Props> = ({ position }) => {
  const offset = (position === "top" ? -1 : 1) * 200;
  const positionClass = position === "top" ? "top-0" : "bottom-0";
  const className = "fixed pointer-events-none h-12 w-full";

  const [{ isOver }, drop] = useDrop({
    accept: ItemTypes.PANEL,
    collect: (monitor) => ({ isOver: monitor.isOver() }),
  });

  const scroll = (): void => {
    window.scroll({ top: window.scrollY + offset, behavior: "smooth" });
  };

  React.useEffect(() => {
    let timeoutId: NodeJS.Timeout | null = null;
    const tick = (): void => {
      scroll();
      timeoutId = setTimeout(tick, 200);
    };
    if (isOver) {
      tick();
    }
    if (timeoutId && !isOver) {
      clearTimeout(timeoutId);
    }

    return () => {
      timeoutId && clearTimeout(timeoutId);
    };
  }, [isOver]);

  return <div ref={drop} className={`${className} ${positionClass}`} />;
};
