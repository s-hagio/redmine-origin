import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { useTranslation } from "react-i18next";
import { createClient } from "@/api/client";
import { panelState } from "@/recoil/panelState";
import { projectIdState } from "@/recoil/projectIdState";
import { useIssueQueries } from "@/hooks/useIssueQueries";
import { NumberArrayInput } from "@/components/NumberArrayInput";
import { IssueDurationPanelType } from "@/types";

const DEFAULT_WORKING_DURATION_GROUPS = [1, 3, 6, 11];
const DEFAULT_THRESHOLD_INDEX = DEFAULT_WORKING_DURATION_GROUPS.indexOf(6);

type Props = {
  id: IssueDurationPanelType["id"];
  title: IssueDurationPanelType["title"];
  config: IssueDurationPanelType["config"];
  onUpdate: () => void;
  onClose: () => void;
};

export const IssueDurationPanelEditor: React.VFC<Props> = ({
  id,
  title: defaultTitle,
  config,
  onUpdate,
  onClose,
}) => {
  const { t } = useTranslation();
  const [title, setTitle] = React.useState(defaultTitle);
  const [queryId, setQueryId] = React.useState(config?.queryId ?? null);
  const [targetValues, setTargetValues] = React.useState<number[]>(config?.targetValues.split(",").map(Number) ?? DEFAULT_WORKING_DURATION_GROUPS);
  const [thresholdIndex, setThresholdIndex] = React.useState<number>(config?.thresholdIndex ?? DEFAULT_THRESHOLD_INDEX);
  const [threshold, setThreshold] = React.useState<number>(targetValues[thresholdIndex]);
  const setPanel = useSetRecoilState(panelState(id));
  const projectId = useRecoilValue(projectIdState);
  const { queries } = useIssueQueries();

  const handleChangeTitle = (
    e: React.SyntheticEvent<HTMLInputElement>
  ): void => {
    setTitle(e.currentTarget.value);
  };

  const handleChangeQueryId = (
    e: React.SyntheticEvent<HTMLSelectElement>
  ): void => {
    const { value } = e.currentTarget;
    setQueryId(value === "" ? null : Number(value));
  };

  const handleChangeTargetValues = (
    numberAry: (number | null)[],
  ): void => {
    if (numberAry.some((numberOrNull) => numberOrNull === null)) {
      return;
    }

    const nextTargetValues = numberAry as number[];
    setTargetValues(nextTargetValues);
    const index = nextTargetValues.indexOf(threshold);
    console.log({threshold, index, nextTargetValues, targetValues});
    if (index < 0) {
      setThreshold(nextTargetValues[thresholdIndex]);
      return;
    }
    if (thresholdIndex !== index) {
      setThresholdIndex(index);
    }
  };

  const handleChangeThresholdIndex = (
    e: React.SyntheticEvent<HTMLSelectElement>
  ): void => {
    const { value } = e.currentTarget;
    setThresholdIndex(value === "" ? 0 : Number(value));
  };

  const client = createClient();
  const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>): void => {
    e.preventDefault();
    setPanel((currVal) =>
      currVal && currVal.type === "IssueDurationPanel"
        ? {
            ...currVal,
            title,
            config: { queryId, targetValues: targetValues.join(","), thresholdIndex },
          }
        : currVal
    );
    (async () => {
      await client.lychee_project_dashboard.projects
        ._projectId(projectId)
        .issue_duration_panels._id(id)
        .$patch({
          body: {
            panel: {
              title,
              configuration_attributes: {
                query_id: queryId,
                target_values: targetValues.join(","),
                threshold_index: thresholdIndex,
              },
            },
          },
        });
      onUpdate();
    })();
    onClose();
  };

  return (
    <form className="space-y-2" onSubmit={handleSubmit}>
      <div className="space-y-1">
        <label className="block">{t("title")}</label>
        <input
          className="w-96 border px-2 py-4"
          value={title}
          onChange={handleChangeTitle}
          maxLength={30}
        />
      </div>
      <div className="space-y-1">
        <label className="block">{t("customQuery")}</label>
        <select
          className="h-full border py-2 pl-2 pr-6"
          value={queryId ?? undefined}
          onChange={handleChangeQueryId}
        >
          <option />
          {queries.map(({ id, name }) => (
            <option key={id} value={id}>
              {name}
            </option>
          ))}
        </select>
      </div>
      <div className="space-y-1">
        <label className="block">{t("distribution")}</label>
        <NumberArrayInput
          className="w-96 border px-2 py-4"
          value={targetValues}
          required
          onChange={handleChangeTargetValues}
        />
      </div>
      <div className="space-y-1">
        <label className="block">{t("threshold")}</label>
        {t("warnAfterDaysBefore")}
        <select
          className="h-full border py-2 pl-2 pr-6"
          value={thresholdIndex}
          onChange={handleChangeThresholdIndex}
        >
          {targetValues.map((value, i) => (
            <option key={i} value={i}>
              {value}
            </option>
          ))}
        </select>
        {t("warnAfterDaysAfter")}
      </div>
      <div>
        <button className="h-full border px-2 py-1 text-sm" type="submit">
          {t("buttonSave")}
        </button>
        <button className="h-full border px-2 py-1 text-sm" onClick={onClose}>
          {t("buttonCancel")}
        </button>
      </div>
    </form>
  );
};
