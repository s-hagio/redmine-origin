import React from "react";

const normalizeRawInputValue = (rawValue: string) => {
  return [...rawValue.matchAll(/[0-9,]+/g)].join("");
};

const parseRawValue = (rawValue: string) => {
  return rawValue.split(",").map((item) => item === '' ? null : Number(item));
};

type Props = {
  value: number[];
  className?: string;
  required?: boolean;
  onChange: (value: (number | null)[]) => void;
};

export const NumberArrayInput: React.VFC<Props> = ({
  value,
  className,
  required,
  onChange,
}) => {
  const [rawValue, setRawValue] = React.useState<string>(value.join(","));

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ): void => {
    e.target.setAttribute("type", "text");

    const currentRawValue = normalizeRawInputValue(e.target.value);
    if (rawValue === currentRawValue) {
      return;
    }

    setRawValue(currentRawValue);
    onChange(parseRawValue(currentRawValue));
  };

  const handleFocus = (
    e: React.FocusEvent<HTMLInputElement>
  ): void => {
    e.target.setAttribute("type", "url");
  };

  const normalizeInputValueAndFireOnChange = (
    target: HTMLInputElement,
  ): void => {
    target.setAttribute("type", "text");
    const value = parseRawValue(rawValue);
    const normalizedValue = [...new Set(value.filter((s) => s))].sort((a, b) => (a as number) - (b as number));
    const normalizedRawValue = normalizedValue.join(",");
    if (normalizedRawValue === rawValue) {
      return;
    }

    setRawValue(normalizedRawValue);
    onChange(normalizedValue);
  };

  const handleBlur = (
    e: React.FocusEvent<HTMLInputElement>
  ): void => {
    normalizeInputValueAndFireOnChange(e.target);
  };

  const handleKeyDown = (
    e: React.KeyboardEvent<HTMLInputElement>
  ): void => {
    if (e.key !== "Enter") {
      return;
    }

    normalizeInputValueAndFireOnChange(e.target as HTMLInputElement);
  };

  return (
    <input
      className={className}
      title="" // ブラウザのデフォルト表示を消すため
      value={rawValue}
      required={required}
      onChange={handleChange}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onKeyDown={handleKeyDown}
    />
  );
};
