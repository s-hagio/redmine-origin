import React from "react";
import { useRecoilValue, useRecoilState } from "recoil";
import { useTranslation } from "react-i18next";
import { createClient } from "@/api/client";
import useAspidaSWR from "@/hooks/useAspidaSWR";
import { useQueryRegistration } from "@/hooks/useQueryRegistration";
import { useCustomFieldResistration } from "@/hooks/useCustomFieldRegistration";
import { useWindowWidth } from "@/hooks/useWindowWidth";
import { usePanelFocus } from "@/hooks/usePanelFocus";
import { Context } from "@/hooks/usePanelsContext";
import { manageableState } from "@/recoil/manageableState";
import { projectIdState } from "@/recoil/projectIdState";
import { panelsState } from "@/recoil/panelsState";
import { focusPanelIdState } from "@/recoil/focusPanelIdState";
import { importableProjectsState } from "@/recoil/importableProjectsState";

import { ErrorBoundary } from "@/components/ErrorBoundary";
import { DraggablePanel } from "@/components/DraggablePanel";
import { CustomPanel } from "@/components/CustomPanel";
import { CustomDragLayer } from "@/components/CustomDragLayer";
import { Warning } from "@/components/Warning";
import { DropZone } from "@/components/DropZone";
import { ScrollHandler } from "@/components/ScrollHandler";
import { PanelImporter } from "@/components/PanelImporter";

import { Panel } from "@/types";

export const Panels: React.VFC = () => {
  const manageable = useRecoilValue(manageableState);
  const projectId = useRecoilValue(projectIdState);
  const focusPanelId = useRecoilValue(focusPanelIdState);
  const importableProjects = useRecoilValue(importableProjectsState);
  const [panels, setPanels] = useRecoilState(panelsState);
  const { t } = useTranslation();
  const client = createClient();

  const {
    data: responseData,
    isValidating,
    mutate,
  } = useAspidaSWR(
    client.lychee_project_dashboard.projects._projectId(projectId).panels,
    {
      suspense: true,
    }
  );
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const data = responseData!;

  React.useEffect(() => {
    if (!isValidating) {
      setPanels(data.panels);
    }
  }, [isValidating]);

  React.useEffect(() => {
    if (focusPanelId) {
      mutate();
    }
  }, [focusPanelId]);

  useQueryRegistration();
  useCustomFieldResistration();

  return (
    <Context.Provider value={{ refetchPanels: mutate }}>
      {data.panels.length === 0 && (
        <Warning>
          <div>{manageable ? t("noPanelsForManager") : t("noPanels")}</div>
          {manageable && (
            <div className="mt-3">
              <PanelImporter projects={importableProjects} />
            </div>
          )}
        </Warning>
      )}
      <ScrollHandler position="top" />
      <div className="flex min-h-[calc(600px-43px)] space-x-1">
        <div className="flex-1">
          <PanelColumn panels={panels} aligned="left" />
        </div>
        <div className="flex-1">
          <PanelColumn panels={panels} aligned="right" />
        </div>
        <CustomDragLayer />
      </div>
      <ScrollHandler position="bottom" />
    </Context.Provider>
  );
};

const PanelColumn: React.VFC<{
  panels: Panel[];
  aligned: "left" | "right";
}> = ({ panels, aligned }) => {
  const width = useWindowWidth();
  const alignedPanels = panels.filter((p) => p.aligned === aligned);

  return (
    <div className="flex h-full flex-col">
      {alignedPanels.map((panel) => (
        <SinglePanel key={`${panel.id}-${width}`} {...panel} />
      ))}
      <div className="flex-1">
        <DropZone aligned={aligned} />
      </div>
    </div>
  );
};

const SinglePanel: React.VFC<Panel> = ({ id, title, aligned, position }) => {
  const ref = React.useRef<HTMLDivElement>(null);
  const manageable = useRecoilValue(manageableState);
  usePanelFocus(id, ref);

  return (
    <div ref={ref} className="min-h-[4.75rem]">
      <ErrorBoundary fallback={<div>ERROR</div>}>
        <React.Suspense fallback={null}>
          {manageable ? (
            <DraggablePanel
              id={id}
              title={title}
              aligned={aligned}
              position={position}
            />
          ) : (
            <CustomPanel id={id} />
          )}
        </React.Suspense>
      </ErrorBoundary>
    </div>
  );
};
