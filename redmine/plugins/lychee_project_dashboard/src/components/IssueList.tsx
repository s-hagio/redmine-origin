import { NoData } from "@/components/NoData";
import type { Issue } from "@/api/lychee_project_dashboard/projects/_projectId@string/issue_list_panels/_id@number";

type Props = {
  issues: Issue[];
};

export const IssueList: React.VFC<Props> = ({ issues }) => {
  if (issues.length === 0) {
    return <NoData />;
  }

  return (
    <table className="list">
      <thead>
        <tr>
          {issues[0].columns.map(({ name, caption }) => (
            <th key={name}>{caption}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {issues.map((issue) => (
          <tr key={issue.id} className="issue">
            {issue.columns.map(({ name, content }) => {
              return (
                <td key={name} className={name.replace(".", "-")}>
                  <div dangerouslySetInnerHTML={{ __html: content }} />
                </td>
              );
            })}
          </tr>
        ))}
      </tbody>
    </table>
  );
};
