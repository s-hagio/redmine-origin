import React from "react";
import { useRecoilValue } from "recoil";
import { baseURLState } from "@/recoil/baseURLState";
import { projectIdState } from "@/recoil/projectIdState";
import useAspidaSWR from "@/hooks/useAspidaSWR";
import { createClient } from "@/api/client";
import { IssueDurationPanelEditor } from "@/components/IssueDurationPanelEditor";
import { ColumnChart } from "@/components/react-google-charts/ColumnChart";
import { LinkToIssues } from "@/components/LinkToIssues";
import { EmptyConfig } from "@/components/EmptyConfig";
import { Warning } from "@/components/Warning";
import type { IssueDurationPanelType } from "@/types";

const ISSUES_URL_TEMPLATE =
  "%{baseURL}/projects/%{projectId}/issues?set_filter=1&sort=id&f%5B%5D=issue_id&op%5Bissue_id%5D=%3D&v%5Bissue_id%5D%5B%5D=%{issueIds}";

type Props = {
  edit: boolean;
  endEdit: () => void;
  panel: IssueDurationPanelType;
};

export const IssueDurationPanel: React.VFC<Props> = ({
  edit,
  endEdit,
  panel: { id, title, config },
}) => {
  const baseURL = useRecoilValue(baseURLState);
  const projectId = useRecoilValue(projectIdState);
  const client = createClient({ caseConvert: false });
  const { data: resData, mutate } = useAspidaSWR(
    client.lychee_project_dashboard.projects
      ._projectId(projectId)
      .issue_duration_panels._id(id),
    {
      suspense: true,
    }
  );
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const data = resData!;

  if (edit) {
    return (
      <IssueDurationPanelEditor
        id={id}
        title={title}
        config={config}
        onUpdate={mutate}
        onClose={endEdit}
      />
    );
  }

  if (!data.success) {
    return <Warning>{data.errors}</Warning>;
  }

  if (!config) {
    return <EmptyConfig />;
  }

  const buildIssuesUrl = (issueIds: number[]) => {
    const joinedIssueIds = issueIds.sort().join("%2C");
    return ISSUES_URL_TEMPLATE
      .replace(/%{baseURL}/g, baseURL)
      .replace(/%{projectId}/g, projectId)
      .replace(/%{issueIds}/g, joinedIssueIds);
  };

  const {
    threshold_index: thresholdIndex,
    targets,
    issue_ids_by_targets: issueIdsByTargets,
  } = data;
  const chartData = targets.map(({ id, name, tooltip_text: tooltipText }) => {
    const issueIds = issueIdsByTargets[id];
    const value = issueIds?.length ?? 0;
    return { name, tooltipText, value };
  });

  const onSelect = (selection: { row: number }) => {
    const { row } = selection;
    const target = targets[row];
    const issueIds = issueIdsByTargets[target.id];
    if (!issueIds) { // 0件クリック時
      return;
    }
    const issuesUrl = buildIssuesUrl(issueIds);
    window.open(issuesUrl);
  };

  return (
    <>
      <ColumnChart
        data={chartData}
        thresholdIndex={thresholdIndex}
        onSelect={onSelect}
      />
      <div className="flex justify-end">
        <LinkToIssues queryId={config.queryId} />
      </div>
    </>
  );
};
