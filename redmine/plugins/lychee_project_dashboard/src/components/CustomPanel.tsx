import React from "react";
import { useRecoilValue } from "recoil";
import type { ConnectDragSource } from "react-dnd";
import { panelState } from "@/recoil/panelState";
import { dragPanelIdState } from "@/recoil/dragPanelIdState";
import { PieChartPanel } from "@/components/PieChartPanel";
import { BarChartPanel } from "@/components/BarChartPanel";
import { IssueDurationPanel } from "@/components/IssueDurationPanel";
import { IssueListPanel } from "@/components/IssueListPanel";
import { PanelTitle } from "@/components/PanelTitle";
import { ErrorBoundary } from "@/components/ErrorBoundary";

type Props = {
  id: number;
  connectDragSource?: ConnectDragSource;
};

export const CustomPanel: React.VFC<Props> = ({ id, connectDragSource }) => {
  const [edit, setEdit] = React.useState(false);
  const panel = useRecoilValue(panelState(id));
  const dragPanelId = useRecoilValue(dragPanelIdState);

  if (!panel) {
    return null;
  }

  if (id === dragPanelId) {
    return (
      <>
        <div className="h-20 w-full rounded-[5px] bg-gray-300" />
        <MarginForDrop />
      </>
    );
  }

  const renderPanel = (): React.ReactNode => {
    switch (panel.type) {
      case "PieChartPanel":
        return (
          <PieChartPanel
            edit={edit}
            endEdit={() => {
              setEdit(false);
            }}
            panel={panel}
          />
        );
      case "BarChartPanel":
        return (
          <BarChartPanel
            edit={edit}
            endEdit={() => {
              setEdit(false);
            }}
            panel={panel}
          />
        );
      case "IssueDurationPanel":
        return (
          <IssueDurationPanel
            edit={edit}
            endEdit={() => {
              setEdit(false);
            }}
            panel={panel}
          />
        );
      case "IssueListPanel":
        return (
          <IssueListPanel
            edit={edit}
            endEdit={() => {
              setEdit(false);
            }}
            panel={panel}
          />
        );
    }
  };

  return (
    <>
      <div className="flex flex-col space-y-1 rounded-[5px] border border-[gray] px-2 py-2.5 shadow-[1px_1px_2px_darkgray]">
        {!edit && (
          <PanelTitle
            id={panel.id}
            onEditClick={() => {
              setEdit(true);
            }}
            connectDragSource={connectDragSource}
          />
        )}
        <div className="flex justify-center">
          <div className="max-w-[calc(50vw-50px)] flex-1 overflow-x-scroll">
            <ErrorBoundary fallback={<div>ERROR</div>}>
              <React.Suspense fallback={null}>{renderPanel()}</React.Suspense>
            </ErrorBoundary>
          </div>
        </div>
      </div>
      <MarginForDrop />
    </>
  );
};

const MarginForDrop: React.VFC = () => <div className="h-1" />;
