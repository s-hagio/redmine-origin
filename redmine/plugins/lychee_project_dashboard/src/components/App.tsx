import React from "react";
import { ErrorBoundary } from "@/components/ErrorBoundary";
import { Header } from "@/components/Header";
import { Panels } from "@/components/Panels";

type Props = {
  title: string;
};
export const App: React.VFC<Props> = ({ title }) => (
  <div className="relative">
    <div className="sticky top-0 z-10 h-12 bg-white">
      <Header title={title} />
    </div>
    <ErrorBoundary fallback={<div>ERROR</div>}>
      <React.Suspense fallback={null}>
        <Panels />
      </React.Suspense>
    </ErrorBoundary>
  </div>
);
