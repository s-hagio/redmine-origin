import React from "react";
import { useRecoilValue } from "recoil";
import { projectIdState } from "@/recoil/projectIdState";
import useAspidaSWR from "@/hooks/useAspidaSWR";
import { createClient } from "@/api/client";
import { EmptyConfig } from "@/components/EmptyConfig";
import { Warning } from "@/components/Warning";
import { IssueListPanelEditor } from "@/components/IssueListPanelEditor";
import { IssueList } from "@/components/IssueList";
import { LinkToIssues } from "@/components/LinkToIssues";
import type { IssueListPanelType } from "@/types";

type Props = {
  edit: boolean;
  endEdit: () => void;
  panel: IssueListPanelType;
};

export const IssueListPanel: React.VFC<Props> = ({
  edit,
  endEdit,
  panel: { id, title, config },
}) => {
  const projectId = useRecoilValue(projectIdState);
  const client = createClient();
  const { data: resData, mutate } = useAspidaSWR(
    client.lychee_project_dashboard.projects
      ._projectId(projectId)
      .issue_list_panels._id(id),
    {
      suspense: true,
    }
  );
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const data = resData!;

  if (edit) {
    return (
      <IssueListPanelEditor
        id={id}
        title={title}
        config={config}
        onUpdate={mutate}
        onClose={endEdit}
      />
    );
  }

  if (!data.success) {
    return <Warning>{data.errors}</Warning>;
  }

  if (!config) {
    return <EmptyConfig />;
  }

  return (
    <>
      <IssueList issues={data.issues} />
      <div className="flex justify-end">
        <LinkToIssues queryId={config.queryId} />
      </div>
    </>
  );
};
