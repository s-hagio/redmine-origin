import { Trans } from "react-i18next";
import { Warning } from "@/components/Warning";

export const EmptyConfig: React.VFC = () => {
  return (
    <Warning>
      {/* prettier-ignore */}
      <Trans i18nKey="pleaseConfigure">
        Please configure from <span className="icon icon-settings ml-0.5 mr-0.5" />
      </Trans>
    </Warning>
  );
};
