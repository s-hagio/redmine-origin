import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import { IssueList } from "./IssueList";

export default {
  title: "IssueList",
  component: IssueList,
} as ComponentMeta<typeof IssueList>;

const headers = {
  id: "#",
  tracker: "トラッカー",
  subject: "題名",
  assignedTo: "担当者",
  dueDate: "期日",
};

const issues = [
  {
    id: 1,
    columns: [
      { name: "id", caption: "#", content: "1" },
      { name: "subject", caption: "題名", content: "issue-1" },
    ],
  },
  {
    id: 2,
    columns: [
      { name: "id", caption: "#", content: "2" },
      { name: "subject", caption: "題名", content: "issue-2" },
    ],
  },
];

export const Basic: ComponentStoryObj<typeof IssueList> = {
  args: {
    issues,
  },
};

export const Empty = {
  args: {
    headers,
    issues: [],
  },
};
