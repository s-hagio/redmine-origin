type Props = {
  children: React.ReactNode;
};

export const Warning: React.VFC<Props> = ({ children }) => (
  <div className="mb-3 whitespace-pre-wrap rounded-[3px] border border-[#eadbbc] bg-[#f3edd1] py-1.5 pl-[1.875rem] pr-1 text-center text-[1.1em] text-[#a6750c]">
    {children}
  </div>
);
