import { useRecoilValue } from "recoil";
import { useDrop } from "react-dnd";
import { panelsState } from "@/recoil/panelsState";
import { ItemTypes, DragItem } from "@/dnd";
import { useMovePanel } from "@/hooks/useMovePanel";

type Props = {
  aligned: "left" | "right";
};

export const DropZone: React.VFC<Props> = ({ aligned }) => {
  const panels = useRecoilValue(panelsState);
  const move = useMovePanel(aligned, panels.length);
  const position = Math.max(
    1,
    panels.filter((p) => p.aligned === aligned).length
  );

  const [, drop] = useDrop<DragItem>({
    accept: ItemTypes.PANEL,
    hover: (item) => {
      if (item.aligned !== aligned || item.position !== position) {
        item.aligned = aligned;
        item.position = position;
        move(item.id);
      }
    },
  });

  return <div ref={drop} className="h-full" />;
};
