import React from "react";
import { useRecoilValue } from "recoil";
import { useTranslation } from "react-i18next";
import { createClient } from "@/api/client";
import { projectIdState } from "@/recoil/projectIdState";
import { usePanelsContext } from "@/hooks/usePanelsContext";

type Props = {
  projects: {
    identifier: string;
    name: string;
  }[];
};

export const PanelImporter: React.VFC<Props> = ({ projects }) => {
  const projectId = useRecoilValue(projectIdState);
  const [selected, setSelected] = React.useState("");
  const { refetchPanels } = usePanelsContext();
  const client = createClient();

  const handleSubmit = async (): Promise<void> => {
    await client.lychee_project_dashboard.projects
      ._projectId(projectId)
      .panels.import.$post({
        body: { source: selected },
      });
    refetchPanels();
  };

  return (
    <Component
      projects={projects}
      selected={selected}
      onChange={(e) => {
        setSelected(e.currentTarget.value);
      }}
      onSubmit={handleSubmit}
    />
  );
};

type ComponentProps = {
  projects: {
    identifier: string;
    name: string;
  }[];
  selected: string;
  onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void;
  onSubmit: () => void;
};

export const Component: React.VFC<ComponentProps> = ({
  projects,
  selected,
  onChange,
  onSubmit,
}) => {
  const { t } = useTranslation();
  const [clicked, setClicked] = React.useState(false);

  const handleClick = (): void => {
    setClicked(true);
    onSubmit();
  };

  return (
    <div className="flex items-center justify-center space-x-1">
      <div>{t("copyFrom")}:</div>
      <select
        className="h-full border py-1.5 pl-2 pr-6"
        value={selected}
        onChange={onChange}
      >
        <option value="" />
        {projects.map(({ identifier, name }) => (
          <option key={identifier} value={identifier}>
            {name}
          </option>
        ))}
      </select>
      <button
        className="h-full border bg-white px-2 py-1 text-sm"
        type="submit"
        disabled={selected === "" || clicked || projects.length === 0}
        onClick={handleClick}
      >
        {t("copy")}
      </button>
    </div>
  );
};
