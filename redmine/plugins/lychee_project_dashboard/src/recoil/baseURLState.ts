import { atom } from "recoil";

export const baseURLState = atom({
  key: "baseURLState",
  default: "",
});
