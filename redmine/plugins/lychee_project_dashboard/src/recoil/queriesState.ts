import { atom } from "recoil";

type Query = {
  id: number;
  name: string;
};

export const queriesState = atom<Query[]>({
  key: "queriesState",
  default: [],
});
