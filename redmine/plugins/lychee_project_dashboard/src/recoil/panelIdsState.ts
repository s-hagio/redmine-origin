import { atom } from "recoil";

export const panelIdsState = atom<number[]>({
  key: "panelIdsState",
  default: [],
});
