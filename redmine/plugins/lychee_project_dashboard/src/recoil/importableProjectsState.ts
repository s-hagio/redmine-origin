import { atom } from "recoil";

type Project = {
  identifier: string;
  name: string;
};

export const importableProjectsState = atom<Project[]>({
  key: "importableProjectsState",
  default: [],
});
