import { atom } from "recoil";

export const dragPanelIdState = atom<number | null>({
  key: "dragPanelIdState",
  default: null,
});
