import { atom } from "recoil";

export const manageableState = atom<boolean>({
  key: "manageableState",
  default: false,
});
