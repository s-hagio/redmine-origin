import { atom } from "recoil";

type Attribute = {
  label: string;
  value: string;
};

export const customFieldsState = atom<Attribute[]>({
  key: "customFieldsState",
  default: [],
});
