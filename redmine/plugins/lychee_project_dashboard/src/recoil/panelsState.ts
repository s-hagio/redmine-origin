import { selector, DefaultValue } from "recoil";
import { panelState } from "@/recoil/panelState";
import { panelIdsState } from "@/recoil/panelIdsState";
import { Panel } from "@/types";

export const panelsState = selector<Panel[]>({
  key: "panelsState",
  get: ({ get }) =>
    get(panelIdsState)
      .map((id) => get(panelState(id)))
      .filter((panel): panel is Panel => panel !== null)
      .sort((a, b) => a.position - b.position),
  set: ({ set }, panels) => {
    if (panels instanceof DefaultValue) {
      set(panelIdsState, []);
      return;
    }
    panels.forEach((panel) => {
      set(panelState(panel.id), panel);
    });
    set(panelIdsState, Array.from(new Set(panels.map((panel) => panel.id))));
  },
});
