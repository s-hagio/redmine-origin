import { atomFamily } from "recoil";
import { Panel } from "@/types";

export const panelState = atomFamily<Panel | null, number>({
  key: "panelState",
  default: null,
});
