import { atom } from "recoil";

export const focusPanelIdState = atom<number | null>({
  key: "focusPanelIdState",
  default: null,
});
