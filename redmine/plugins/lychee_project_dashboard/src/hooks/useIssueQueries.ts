import { useRecoilValue } from "recoil";
import { queriesState } from "@/recoil/queriesState";

type Query = {
  id: number;
  name: string;
};

export const useIssueQueries = (): { queries: Query[] } => {
  const queries = useRecoilValue(queriesState);

  return { queries };
};
