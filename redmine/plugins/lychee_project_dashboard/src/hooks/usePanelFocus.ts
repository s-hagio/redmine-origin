import React from "react";
import { useRecoilState } from "recoil";
import { focusPanelIdState } from "@/recoil/focusPanelIdState";

export const usePanelFocus = (
  id: number,
  ref: React.RefObject<HTMLDivElement>
): void => {
  const [focusPanelId, setFocusPanelId] = useRecoilState(focusPanelIdState);

  React.useEffect(() => {
    if (id === focusPanelId && ref.current) {
      ref.current.scrollIntoView({ behavior: "smooth" });
      setFocusPanelId(null);
    }
  }, [focusPanelId]);
};
