import produce from "immer";
import { useRecoilState, useRecoilValue } from "recoil";
import { useTranslation } from "react-i18next";
import { projectIdState } from "@/recoil/projectIdState";
import { panelIdsState } from "@/recoil/panelIdsState";
import { usePanelsContext } from "@/hooks/usePanelsContext";
import { createClient } from "@/api/client";

export const useRemovePanel = (): {
  remove: (id: number) => () => void;
} => {
  const { t } = useTranslation();
  const projectId = useRecoilValue(projectIdState);
  const [panelIds, setPanelIds] = useRecoilState(panelIdsState);
  const { refetchPanels } = usePanelsContext();
  const client = createClient();

  const remove = (id: number) => async () => {
    if (!confirm(t("areYouSureToRemovePanel"))) {
      return;
    }
    const newPanelIds = produce(panelIds, (draft) => {
      const index = draft.indexOf(id);
      draft.splice(index, 1);
    });
    setPanelIds(newPanelIds);
    await client.lychee_project_dashboard.projects
      ._projectId(projectId)
      .panels._id(id)
      .$delete();
    refetchPanels();
  };

  return { remove };
};
