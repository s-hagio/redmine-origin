import { useRecoilValue } from "recoil";
import { useTranslation } from "react-i18next";
import { customFieldsState } from "@/recoil/customFieldsState";

type Attribute = {
  label: string;
  value: string;
};

export const useTargetAttributes = (): Attribute[] => {
  const { t } = useTranslation();
  const customFields = useRecoilValue(customFieldsState);

  return [
    {
      label: t("fieldTracker"),
      value: "tracker_id",
    },
    {
      label: t("fieldStatus"),
      value: "status_id",
    },
    {
      label: t("fieldPriority"),
      value: "priority_id",
    },
    {
      label: t("fieldAssignedTo"),
      value: "assigned_to_id",
    },
    {
      label: t("fieldCategory"),
      value: "category_id",
    },
    ...customFields,
  ];
};
