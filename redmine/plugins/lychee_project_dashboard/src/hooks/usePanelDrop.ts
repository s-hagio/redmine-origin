import { useRecoilValue } from "recoil";
import { useDrop, ConnectDropTarget } from "react-dnd";
import type { XYCoord, Identifier } from "dnd-core";
import { panelsState } from "@/recoil/panelsState";
import { useMovePanel } from "@/hooks/useMovePanel";
import { ItemTypes, DragItem } from "@/dnd";

export const usePanelDrop = (
  ref: React.RefObject<HTMLDivElement>,
  panel: DragItem
): { drop: ConnectDropTarget; handlerId: Identifier | null } => {
  const panels = useRecoilValue(panelsState);
  const move = useMovePanel(
    panel.aligned,
    panels.findIndex((p) => p.id === panel.id)
  );

  const [{ handlerId }, drop] = useDrop<
    DragItem,
    null,
    { handlerId: Identifier | null }
  >({
    accept: ItemTypes.PANEL,
    collect: (monitor) => ({
      handlerId: monitor.getHandlerId(),
    }),
    hover: (item, monitor) => {
      if (!ref.current) {
        return;
      }
      const { id: dragId, aligned: dragAligned, position: dragPosition } = item;
      const {
        id: hoverId,
        aligned: hoverAligned,
        position: hoverPosition,
      } = panel;

      // Don't replace items with themselves
      if (dragId === hoverId) {
        return;
      }

      // Determine rectangle on screen
      const hoverBoundingRect = ref.current.getBoundingClientRect();

      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

      // Determine mouse position
      const clientOffset = monitor.getClientOffset();

      // Get pixels to the top
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%

      if (dragAligned === hoverAligned) {
        // Dragging downwards
        if (dragPosition < hoverPosition && hoverClientY < hoverMiddleY) {
          return;
        }

        // Dragging upwards
        if (dragPosition > hoverPosition && hoverClientY > hoverMiddleY) {
          return;
        }
      }

      // Time to actually perform the action
      move(dragId);

      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.aligned = hoverAligned;
      item.position = hoverPosition;
    },
  });

  return { drop, handlerId };
};
