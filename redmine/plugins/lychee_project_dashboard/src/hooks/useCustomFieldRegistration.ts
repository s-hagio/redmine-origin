import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import useAspidaSWR from "@/hooks/useAspidaSWR";
import { projectIdState } from "@/recoil/projectIdState";
import { customFieldsState } from "@/recoil/customFieldsState";
import { createClient } from "@/api/client";

export const useCustomFieldResistration = (): void => {
  const projectId = useRecoilValue(projectIdState);
  const setCustomFields = useSetRecoilState(customFieldsState);
  const client = createClient();
  const { data, isValidating } = useAspidaSWR(
    client.lychee_project_dashboard.projects._projectId(projectId)
      .custom_fields,
    {
      suspense: true,
    }
  );

  React.useEffect(() => {
    if (!isValidating) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      setCustomFields(data!.customFields);
    }
  }, [isValidating]);
};
