import produce from "immer";
import { useRecoilState } from "recoil";
import { panelsState } from "@/recoil/panelsState";

export const useMovePanel = (
  aligned: "left" | "right",
  insertIndex: number
): ((dragId: number) => void) => {
  const [panels, setPanels] = useRecoilState(panelsState);

  const move = (dragId: number): void => {
    const newPanels = produce(panels, (draft) => {
      const dragIndex = draft.findIndex((p) => p.id === dragId);
      const [dragged] = draft.splice(dragIndex, 1);
      dragged.aligned = aligned;
      draft.splice(insertIndex, 0, dragged);
      draft
        .filter((p) => p.aligned === "left")
        .forEach((p, index) => (p.position = index + 1));
      draft
        .filter((p) => p.aligned === "right")
        .forEach((p, index) => (p.position = index + 1));
    });

    setPanels(newPanels);
  };

  return move;
};
