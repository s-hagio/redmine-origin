import React from "react";

export const Context = React.createContext({
  refetchPanels: () => {
    return;
  },
});

export const usePanelsContext = (): { refetchPanels: () => void } => {
  return React.useContext(Context);
};
