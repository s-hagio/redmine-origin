import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import useAspidaSWR from "@/hooks/useAspidaSWR";
import { projectIdState } from "@/recoil/projectIdState";
import { queriesState } from "@/recoil/queriesState";
import { createClient } from "@/api/client";

export const useQueryRegistration = (): void => {
  const projectId = useRecoilValue(projectIdState);
  const setQueries = useSetRecoilState(queriesState);
  const client = createClient();
  const { data, isValidating } = useAspidaSWR(
    client.lychee_project_dashboard.projects._projectId(projectId).queries,
    {
      suspense: true,
    }
  );

  React.useEffect(() => {
    if (!isValidating) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      setQueries(data!.queries);
    }
  }, [isValidating]);
};
