import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { ConnectDragSource, useDrag } from "react-dnd";
import { projectIdState } from "@/recoil/projectIdState";
import { dragPanelIdState } from "@/recoil/dragPanelIdState";
import { createClient } from "@/api/client";
import { ItemTypes, DragItem } from "@/dnd";

export const usePanelDrag = (
  panel: DragItem
): { drag: ConnectDragSource; opacity: number } => {
  const projectId = useRecoilValue(projectIdState);
  const setDragPanelId = useSetRecoilState(dragPanelIdState);
  const client = createClient();
  const update = async (
    dragId: number,
    aligned: "left" | "right",
    position: number
  ): Promise<void> => {
    client.lychee_project_dashboard.projects
      ._projectId(projectId)
      .panels._id(dragId)
      .$patch({
        body: {
          panel: {
            aligned,
            position,
          },
        },
      });
  };
  const [{ isDragging, opacity }, drag] = useDrag<
    DragItem,
    null,
    { isDragging: boolean; opacity: number }
  >(() => ({
    type: ItemTypes.PANEL,
    item: (): DragItem => ({ ...panel }),
    end: (item) => {
      update(item.id, item.aligned, item.position);
      setDragPanelId(null);
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
      opacity: monitor.isDragging() ? 0.4 : 1,
    }),
  }));

  React.useEffect(() => {
    if (isDragging) {
      setDragPanelId(panel.id);
    }
  }, [isDragging]);

  return { drag, opacity };
};
