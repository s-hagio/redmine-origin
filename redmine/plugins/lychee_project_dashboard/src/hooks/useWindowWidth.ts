import React from "react";

export const useWindowWidth = (): number => {
  const [width, setWidth] = React.useState(window.innerWidth);

  const handleResize = (): void => {
    setWidth(window.innerWidth);
  };

  React.useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return width;
};
