export const ItemTypes = {
  PANEL: "PANEL",
};

export type DragItem = {
  id: number;
  title: string;
  aligned: "left" | "right";
  position: number;
};
