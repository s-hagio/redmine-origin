export type Panel =
  PieChartPanelType |
  BarChartPanelType |
  IssueDurationPanelType |
  IssueListPanelType;

type PanelBase = {
  id: number;
  title: string;
  aligned: "left" | "right";
  position: number;
};

export type PieChartPanelType = PanelBase & {
  type: "PieChartPanel";
  config: {
    queryId: number | null;
    target: string;
  } | null;
};

export type BarChartPanelType = PanelBase & {
  type: "BarChartPanel";
  config: {
    queryId: number | null;
    rowTarget: string;
    columnTarget: string;
  } | null;
};

export type IssueDurationPanelType = PanelBase & {
  type: "IssueDurationPanel";
  config: {
    queryId: number | null;
    thresholdIndex: number;
    targetValues: string;
  } | null;
};

export type IssueListPanelType = PanelBase & {
  type: "IssueListPanel";
  config: {
    queryId: number | null;
    limit?: number;
  } | null;
};
