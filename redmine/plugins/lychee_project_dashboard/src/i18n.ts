import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";

const resources = {
  ja: {
    translation: {
      newPanel: "パネル追加",
      noPanels: "パネルがありません。",
      noPanelsForManager:
        "「パネル追加」からパネルを追加してください。\n他のプロジェクトからコピーすることも出来ます。",
      copyFrom: "コピー元プロジェクト",
      copy: "コピー",
      pieChartPanel: "円グラフ",
      barChartPanel: "棒グラフ",
      issueDurationPanel: "チケットの期間",
      issueListPanel: "チケットリスト",
      title: "タイトル",
      customQuery: "カスタムクエリ",
      aggregateTarget: "集計対象",
      yAsix: "Y軸",
      series: "系列",
      maxRows: "件数",
      pleaseConfigure: "<1/>から設定してください",
      distribution: "分布（カンマ区切りで指定）",
      threshold: "閾値",
      warnAfterDaysBefore: "",
      warnAfterDaysAfter: "日以上で警告",
      areYouSureToRemovePanel: "このパネルを削除しますか？",
      issuesLink: "一覧で見る",
    },
  },
  en: {
    translation: {
      newPanel: "New Panel",
      noPanels: "There is no panel.",
      noPanelsForManager:
        'Panel can be added from "New Panel".\nPanel can be also copied from another projects.',
      copyFrom: "Copy from",
      copy: "Copy",
      pieChartPanel: "Pie Chart",
      barChartPanel: "Bar Chart",
      issueDurationPanel: "Issue Duration",
      issueListPanel: "Issue List",
      title: "Title",
      customQuery: "Custom query",
      aggregateTarget: "Data",
      yAsix: "Y-axis",
      series: "Series",
      maxRows: "Max value",
      pleaseConfigure: "Please configure from <1/>",
      distribution: "Distribution (comma separated)",
      threshold: "Threshold",
      warnAfterDaysBefore: "warn after",
      warnAfterDaysAfter: "days",
      areYouSureToRemovePanel: "Are you sure you want to remove this panel?",
      issuesLink: "View issues",
    },
  },
} as const;

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources,
    fallbackLng: "en",
    interpolation: {
      escapeValue: false,
    },
    detection: {
      order: ["htmlTag"],
    },
  });

export default i18n;
