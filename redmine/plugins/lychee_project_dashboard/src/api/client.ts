import axios, { AxiosRequestConfig } from "axios";
import qs from "qs";
import aspida from "@aspida/axios";
import applyCaseMiddleware from "axios-case-converter";

import api, { ApiInstance } from "@/api/$api";

const axiosInstance = axios.create({
  withCredentials: true,
  paramsSerializer: (params) =>
    qs.stringify(params, { arrayFormat: "brackets" }),
});

export const setClientConfig = ({
  baseURL,
}: Required<Pick<AxiosRequestConfig, "baseURL">>): void => {
  axiosInstance.defaults.baseURL = baseURL;
};

const methods = ["post", "patch", "delete"] as const;

export const createClient = (options = { caseConvert: true }): ApiInstance => {
  const { withCredentials, paramsSerializer, baseURL } = axiosInstance.defaults;
  let client = axios.create({
    withCredentials,
    paramsSerializer,
    baseURL,
  });
  methods.forEach((method) => {
    client.defaults.headers[method]["X-CSRF-Token"] = csrfToken() ?? "";
  });
  if (options.caseConvert) {
    client = applyCaseMiddleware(client);
  }
  return api(aspida(client)) as ApiInstance;
};

const csrfToken = (): string | null => {
  const meta = document.querySelector<HTMLMetaElement>("meta[name=csrf-token]");
  return meta && meta.content;
};
