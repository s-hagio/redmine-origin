export type Issue = {
  id: number;
  columns: {
    name: string;
    caption: string;
    content: string;
  }[];
};

type Panel = {
  id: number;
};

export type Methods = {
  get: {
    resBody:
      | {
          success: true;
          issues: Issue[];
        }
      | {
          success: false;
          errors: string[];
        };
  };
  patch: {
    reqFormat: string;
    reqBody: {
      panel: {
        title: string;
        configuration_attributes: {
          queryId: number | null;
          limit: number;
        };
      };
    };
    resBody: {
      success: boolean;
      panel: Panel;
    };
  };
};
