export type Methods = {
  post: {
    reqFormat: string;
    reqBody: {
      source: string;
    };
  };
};
