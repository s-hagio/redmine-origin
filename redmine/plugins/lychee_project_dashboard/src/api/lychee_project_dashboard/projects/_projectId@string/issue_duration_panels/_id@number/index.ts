type Panel = {
  id: number;
};

type Body<T extends string> = {
  threshold_index: number;
  targets: {
    id: T;
    name: string;
    tooltip_text: string;
  }[],
  issue_ids_by_targets: Record<T, number[]>;
};

export type Methods = {
  get: {
    resBody:
      | ({
          success: true;
        } & Body<string>)
      | {
          success: false;
          errors: string[];
        };
  };
  patch: {
    reqFormat: string;
    reqBody: {
      panel: {
        title: string;
        configuration_attributes: {
          query_id: number | null;
          target_values: string;
          threshold_index: number;
        };
      };
    };
    resBody: {
      success: boolean;
      panel: Panel;
    };
  };
};
