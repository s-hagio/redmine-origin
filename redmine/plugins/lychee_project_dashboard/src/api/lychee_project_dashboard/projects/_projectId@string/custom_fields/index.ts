type CustomField = {
  label: string;
  value: string;
};

export type Methods = {
  get: {
    resBody: {
      customFields: CustomField[];
    };
  };
};
