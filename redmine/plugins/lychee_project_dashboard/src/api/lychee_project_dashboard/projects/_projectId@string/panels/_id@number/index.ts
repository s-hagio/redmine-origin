export type Methods = {
  patch: {
    reqFormat: string;
    reqBody: {
      panel: {
        aligned: "left" | "right";
        position: number;
      };
    };
  };
  delete: {
    // nothing
  };
};
