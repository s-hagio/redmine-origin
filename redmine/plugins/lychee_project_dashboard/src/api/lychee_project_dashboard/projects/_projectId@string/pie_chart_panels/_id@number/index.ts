type Panel = {
  id: number;
};

export type Methods = {
  get: {
    resBody:
      | {
          success: true;
          result: [string, number][];
        }
      | {
          success: false;
          errors: string[];
        };
  };
  patch: {
    reqFormat: string;
    reqBody: {
      panel: {
        title: string;
        configuration_attributes: {
          queryId: number | null;
          target: string;
        };
      };
    };
    resBody: {
      success: boolean;
      panel: Panel | null;
    };
  };
};
