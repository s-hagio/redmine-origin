import { Panel } from "@/types";

export type Methods = {
  get: {
    resBody: {
      panels: Panel[];
    };
  };

  post: {
    reqFormat: string;
    reqBody: {
      type: "PieChartPanel" | "BarChartPanel" | "IssueDurationPanel" | "IssueListPanel";
    };
    resBody:
      | {
          success: true;
          id: number;
        }
      | {
          success: false;
          errors: string[];
        };
  };
};
