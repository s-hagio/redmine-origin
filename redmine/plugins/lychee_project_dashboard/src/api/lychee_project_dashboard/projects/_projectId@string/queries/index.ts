type Query = {
  id: number;
  name: string;
};

export type Methods = {
  get: {
    resBody: {
      queries: Query[];
    };
  };
};
