type Panel = {
  id: number;
};

type Body<T extends string, U extends string> = {
  row_target_ids: T[];
  row_targets: Record<T, string>;
  column_target_ids: U[];
  column_targets: Record<U, string>;
  issues: Record<T, Record<U, number> | undefined>;
};

export type Methods = {
  get: {
    resBody:
      | ({
          success: true;
        } & Body<string, string>)
      | {
          success: false;
          errors: string[];
        };
  };
  patch: {
    reqFormat: string;
    reqBody: {
      panel: {
        title: string;
        configuration_attributes: {
          queryId: number | null;
          rowTarget: string;
          columnTarget: string;
        };
      };
    };
    resBody: {
      success: boolean;
      panel: Panel;
    };
  };
};
