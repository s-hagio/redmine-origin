# lychee_project_dashboard

## 開発

### 準備

必要なバージョンのNodeJSとnpmパッケージをインストール

```console
$ nodenv install --skip-existing $(cat .node-version) && npx -y yarn install
```

### Viteサーバーの起動

```console
$ npx yarn dev
```
