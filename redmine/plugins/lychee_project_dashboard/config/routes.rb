# frozen_string_literal: true

resources :projects, only: [] do
  resource :dashboard, controller: :lychee_project_dashboards, only: :show, as: 'lychee_dashboard'
end

namespace :lychee_project_dashboard, defaults: { format: :json } do
  resources :projects, only: [] do
    resources :queries, only: :index
    resources :custom_fields, only: :index
    resources :panels, only: %i[index create update destroy] do
      post :import, on: :collection, controller: :imports, action: :create
    end
    resources :pie_chart_panels, only: %i[show update]
    resources :bar_chart_panels, only: %i[show update]
    resources :issue_duration_panels, only: %i[show update]
    resources :issue_list_panels, only: %i[show update]
  end
end
