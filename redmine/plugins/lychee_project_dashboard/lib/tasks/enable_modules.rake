# frozen_string_literal: true

namespace :redmine do
  namespace :plugins do
    namespace :lychee_project_dashboard do
      desc 'Enable modules for active projects'
      task create_enabled_modules_for_active_projects: :environment do
        Project.active.find_each do |project|
          project.enable_module!(:lychee_project_dashboard)
        end
      end
    end
  end
end
