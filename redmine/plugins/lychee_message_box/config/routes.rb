# frozen_string_literal: true

resource :lychee_message_box_information, only: %i[show] do
  resources :messages, only: %i[index show], module: :lychee_message_box_information

  collection do
    post :mark_as_read
  end
end

resource :lychee_message_box_system_error, only: %i[show] do
  resources :messages, only: %i[index show], module: :lychee_message_box_system_error

  collection do
    post :mark_as_read
  end
end

resource :lychee_message_box_my_box, only: %i[show] do
  resources :messages, only: %i[index show], module: :lychee_message_box_my_box

  collection do
    post :mark_as_read
  end

  # Wikiページへのハイパーリンク対応
  #
  # https://github.com/redmine/redmine/blob/5.0.2/app/helpers/application_helper.rb#L1018-L1021
  resources :wiki, only: %i[show], module: :lychee_message_box_my_box
end
