$(function(){
  var loadingFlag = false;
  var hasMoreMessages = $('.lychee-message-box-root .message-box').data('has-more-messages');
  const scrollEventThresholdValue = 45;

  function getMessageIdFromQuery() {
    var url = new URL(location);
    return Number(url.searchParams.get('message_id'));
  }

  function pushMessageIdToHistory(messageId) {
    var nextUrl = new URL(location);
    nextUrl.hash = '';
    nextUrl.searchParams.set('message_id', messageId);
    window.history.pushState({}, '', nextUrl);
  }

  function buildResolvedJQueryPromise() {
    var deferred = $.Deferred();
    deferred.resolve();
    return deferred.promise();
  }

  function fetchAndShowMoreMessageSummaries() {
    if (loadingFlag || !hasMoreMessages) {
      return buildResolvedJQueryPromise();
    }

    hideErrorMessage();

    var url = $('.message-box').data('path');
    var data = {
      last_id: $('.message-box .message:last').data('id'),
      last_notified_at: $('.message-box .message:last').data('notified-at')
    };
    loadingFlag = true;
    return $.ajax({
      url: url,
      dataType: 'json',
      type: 'GET',
      data: data
    }).then(function(res) {
      $('.message-box').append(res.messages);
      hasMoreMessages = res.has_more_messages;
    }, function(err) {
      var errorMessage = $('.message-box').data('error-message');
      showErrorMessage(errorMessage);
    }).always(function() {
      loadingFlag = false;
    });
  }

  function scrollToMessageSummary(messageId) {
    var summaryNode = document.querySelector('div[data-id="' + messageId + '"]');
    var scrollNode = summaryNode.parentNode;
    var nextScrollTop = summaryNode.offsetTop - scrollNode.offsetTop;
    summaryNode.scrollIntoView();
  }

  function fetchAndShowMessageDetail(messageId) {
    hideErrorMessage();

    var $summaryNode = $('div[data-id=' + messageId + ']');
    $.ajax({
      url: $summaryNode.data('path'),
      dataType: 'json',
      type: 'GET',
    }).done(function(res) {
      if (!res.have_unread) {
        $('.lychee-message-box-icon').removeClass('unread');
      }
      $('.lychee-message-box-types .my-box .unread-badge').text(res.unread_my_box_message_count_badge);
      $('.lychee-message-box-types .information .unread-badge').text(res.unread_information_message_count_badge);
      $('.lychee-message-box-types .system_error .unread-badge').text(res.unread_system_error_message_count_badge);
      $('.lychee-message-box-root').children('.details').remove();
      $('.lychee-message-box-root').append(res.content);
      $summaryNode.removeClass('unread');
      fixDetailsContentHeight();
      addTargetAttributeToLinksOnDetail();
      scrollDetail();
    }).fail(function(err){
      var errorMessage = $('.message-box').data('error-message');
      showErrorMessage(errorMessage);
    });
  };

  function hideErrorMessage() {
    $('#errorExplanation').remove();
  }

  function showErrorMessage(message) {
    var elemId = 'errorExplanation';
    var selector = '#' + elemId;
    if ($(selector).length === 0) {
      var errorMessageElem = $('<p>', { id: elemId });
      $('.lychee-message-box-root').before(errorMessageElem);
    }
    $(selector).text(message).show();
  }

  function fixDetailsContentHeight() {
    if ($('.lychee-message-box-root .details.not-selected').length > 0) {
      return;
    }
    var titleHeightPx = $('.lychee-message-box-root .details-title')[0].offsetHeight;
    var bottomHeightPx = $('.lychee-message-box-root .details-bottom')[0].offsetHeight;
    var height = 'calc(100% - ' + (titleHeightPx + bottomHeightPx) + 'px)';
    $('.lychee-message-box-root .details-content').css({height: height});
  }

  function addTargetAttributeToLinksOnDetail() {
    Array.from(document.querySelectorAll('.lychee-message-box-root .details-content a[href]:not([href^="#"])')).forEach(function(node) {
      node.setAttribute('target', '_blank');
    });
  }

  function haveScrollBarOnSummaries() {
    var node = document.querySelector('.message-box');
    return node.scrollHeight > node.clientHeight;
  }

  function scrollDetail() {
    if (location.hash) {
      var node = document.querySelector(location.hash);
      if (node) {
        node.scrollIntoView();
        return;
      }
    }

    $('.lychee-message-box-root .scroll-last').each(function(_i, node) {
      node.scrollTop = node.scrollHeight;
    });
  }

  var promise = buildResolvedJQueryPromise();
  var messageId = getMessageIdFromQuery();
  if (messageId) {
    promise = promise.then(function() {
      return scrollToMessageSummary(messageId);
    }).then(function() {
      return fetchAndShowMessageDetail(messageId);
    });
  }
  if (!haveScrollBarOnSummaries()) {
    promise = promise.then(fetchAndShowMoreMessageSummaries);
  }
  promise.always(function() {
    fixDetailsContentHeight();

    $('.message-box').scroll(function() {
      var scrollPosition = this.scrollHeight - (this.scrollTop + this.clientHeight);
      if (scrollPosition <= scrollEventThresholdValue) {
        fetchAndShowMoreMessageSummaries();
      }
    });

    $('.message-box').on('click', '.message', function() {
      var $this = $(this);
      $('.lychee-message-box-root .message.current').removeClass('current');
      $this.addClass('current');

      var messageId = Number($this.data('id'));
      pushMessageIdToHistory(messageId);
      fetchAndShowMessageDetail(messageId);
    });
  });

  $('.lychee-message-box-root').on('input', '.details #issue_notes', function(){
    var $this = $(this);
    var $content = $('.lychee-message-box-root .details-content');
    var beforeHeight = $this.height();
    $this.removeAttr('style');
    var minHeight = $this.outerHeight();
    var afterHeight = $this.prop('scrollHeight');

    if(beforeHeight < afterHeight && $content.height() < Math.min(afterHeight, $content.find(".notes-wrap").height())) { // for maxHeight
      $this.height(beforeHeight);
      return;
    }
    if(beforeHeight > afterHeight && afterHeight <= minHeight) { // for minHeight
      fixDetailsContentHeight();
      return;
    }
    $this.height(afterHeight);
    fixDetailsContentHeight();
  });
});
