version: 2.1
orbs:
  redmine-plugin: agileware-jp/redmine-plugin@3.6.0
  rubocop:
    executors:
      rubocop-executor:
        parameters:
          ruby_version:
            description: version of Ruby
            type: string
            default: $RUBY_MAX_VERSION
        docker:
          - image: cimg/ruby:<<parameters.ruby_version>>
    commands:
      install-rubocop:
        parameters:
          rubocop_version:
            description: version of Rubocop
            type: string
            default: ''
        steps:
          - run:
              name: install rubocop << parameters.rubocop_version >>
              command: |
                set -e
                if [ <<parameters.rubocop_version>> ]; then
                  gem install rubocop -v '<< parameters.rubocop_version >>'
                else
                  gem install rubocop
                fi
      install-cop-from-github:
        parameters:
          url:
            description: URL of Git repository of custom cop
            type: string
        steps:
          - run: git clone << parameters.url >> /tmp/cop-repo && cd /tmp/cop-repo && rake install && cd / && rm -rf /tmp/cop-repo
      install-cop-from-gem:
        parameters:
          gem-name:
            description: name of gem
            type: string
          gem-version:
            description: version of gem
            type: string
            default: ''
        steps:
          - run:
              name: install cop from gem << parameters.gem-version >>
              command: |
                set -e
                if [ << parameters.gem-version >> ]
                then
                  gem uninstall --force << parameters.gem-name >>
                  gem install << parameters.gem-name >> -v '<<parameters.gem-version>>'
                else
                  gem install << parameters.gem-name >>
                fi
      run-cops:
        parameters:
          rubocop_option:
            description: CLI option for rubocop
            type: string
            default: '--format simple'
        steps:
          - run: rubocop << parameters.rubocop_option >>
jobs:
  rubocop:
    parameters:
      rubocop_config_path:
        description: path to rubocop config yaml
        type: string
      rubocop_files:
        description: files to check by rubocop
        type: string
        default: '~/project'
    executor: rubocop/rubocop-executor
    steps:
      - checkout
      - rubocop/install-rubocop:
          rubocop_version: '~> 1.46.0'
      - rubocop/install-cop-from-github:
          url: https://github.com/agileware-jp/rubocop-lychee.git
      - rubocop/install-cop-from-github:
          url: https://github.com/agileware-jp/rubocop-no_keyword_args.git
      - rubocop/install-cop-from-gem:
          gem-name: rubocop-rails
          gem-version: '2.20.2'
      - rubocop/install-cop-from-gem:
          gem-name: rubocop-rspec
      - rubocop/install-cop-from-gem:
          gem-name: rubocop-performance
      - rubocop/run-cops:
          rubocop_option: -f s -c << parameters.rubocop_config_path >> << parameters.rubocop_files >>
  brakeman:
    docker:
      - image: presidentbeef/brakeman
    steps:
      - checkout
      - run: /usr/src/app/bin/brakeman --color
  rspec:
    parameters:
      redmine_version:
        type: string
      ruby_version:
        type: string
      db:
        type: enum
        enum: ['mysql', 'pg']
      db_version:
        type: string
    executor:
      name: redmine-plugin/ruby-<< parameters.db >>
      ruby_version: << parameters.ruby_version >>
      db_version: << parameters.db_version >>
    steps:
      - run:
          name: Uninstall pre-installed Chrome
          command: sudo dpkg --purge google-chrome-stable
      - checkout
      - redmine-plugin/download-redmine:
          version: << parameters.redmine_version >>
      - redmine-plugin/install-self
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/alm.git
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/lychee_notification.git
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/lychee_profile_icon.git
      - redmine-plugin/generate-database_yml
      - redmine-plugin/bundle-install:
          update_packages: 'lychee-dev'
      - redmine-plugin/rspec
      - store_artifacts:
          path: redmine/tmp/capybara
          destination: screenshots
  migration:
    parameters:
      redmine_version:
        type: string
      ruby_version:
        type: string
      db:
        type: enum
        enum: ['mysql', 'pg']
      db_version:
        type: string
    executor:
      name: redmine-plugin/ruby-<< parameters.db >>
      ruby_version: << parameters.ruby_version >>
      db_version: << parameters.db_version >>
    steps:
      - checkout
      - redmine-plugin/download-redmine:
          version: << parameters.redmine_version >>
      - redmine-plugin/install-self
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/alm.git
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/lychee_notification.git
      - redmine-plugin/install-plugin:
          repository: git@github.com:agileware-jp/lychee_profile_icon.git
      - redmine-plugin/generate-database_yml
      - run:
          name: Add production environment to database.yml
          working_directory: redmine/config
          command: |
            set -eux -o pipefail
            if [ $DATABASE_ADAPTER = "mysql2" ]; then
              cat \<<-'EOF' >> database.yml
              production:
                adapter: mysql2
                database: circle_production
                host: 127.0.0.1
                username: root
                password:
                encoding: utf8mb4
            EOF
            fi
            if [ $DATABASE_ADAPTER = "postgresql" ]; then
              cat \<<-'EOF' >> database.yml
              production:
                adapter: postgresql
                database: circle_production
                host: 127.0.0.1
                username: postgres
                password: postgres
            EOF
            fi
            if [ $DATABASE_ADAPTER = "sqlite3" ]; then
              cat \<<-'EOF' >> database.yml
              production:
                adapter: sqlite3
                database: db/production.sqlite3
            EOF
            fi
      - run:
          name: Test migration on production environment
          working_directory: redmine
          environment:
            RAILS_ENV: production
          command: |
            set -eux -o pipefail
            bundle install --path vendor/bundle
            test -f config/initializers/secret_token.rb ||
              bundle exec rake generate_secret_token
            bundle exec rake db:create db:migrate
            # https://github.com/agileware-jp/testcafe/blob/9f7a42c3d7ae20400288a840257fe6d9ee213919/redmine/my-entrypoint.current.sh#L176
            bundle exec rails runner 'puts("Before redmine:plugins task, `rails runner` is runnable")'
            bundle exec rake redmine:plugins
            bundle exec rake redmine:plugins:migrate NAME=$(cat .tested_plugin) VERSION=0
ignore_trial: &ignore_trial
  filters:
    branches:
      ignore:
        - trial
default_context: &default_context
  context:
    - lychee-ci-environment
workflows:
  version: 2
  test:
    jobs:
      - rubocop:
          !!merge <<: *default_context
          !!merge <<: *ignore_trial
          rubocop_config_path: ~/project/.rubocop.yml
          !!merge <<: *ignore_trial
      - brakeman:
          !!merge <<: *default_context
          !!merge <<: *ignore_trial
      - rspec:
          !!merge <<: *default_context
          !!merge <<: *ignore_trial
          name: RSpec on supported maximum versions with PostgreSQL
          redmine_version: $REDMINE_MAX_VERSION
          ruby_version: $RUBY_MAX_VERSION
          db: pg
          db_version: $POSTGRES_VERSION
      - rspec:
          !!merge <<: *default_context
          !!merge <<: *ignore_trial
          name: RSpec on supported minimum versions with MySQL
          redmine_version: $REDMINE_MAX_VERSION
          ruby_version: $RUBY_MAX_VERSION
          db: mysql
          db_version: $MYSQL_VERSION
      - migration:
          !!merge <<: *default_context
          !!merge <<: *ignore_trial
          name: Migration on supported minimum versions with PostgreSQL
          redmine_version: $REDMINE_MAX_VERSION
          ruby_version: $RUBY_MAX_VERSION
          db: pg
          db_version: $POSTGRES_VERSION
      - migration:
          !!merge <<: *default_context
          !!merge <<: *ignore_trial
          name: Migration on supported minimum versions with MySQL
          redmine_version: $REDMINE_MAX_VERSION
          ruby_version: $RUBY_MAX_VERSION
          db: mysql
          db_version: $MYSQL_VERSION
