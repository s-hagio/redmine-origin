# lychee_message_box

- サーバ起動時あるいは `bin/rails runner` 実行時の更新情報取得はproduction環境でのみ動作する．
- 環境変数 `DISABLE_COMPARE_PLUGIN_VERSION` がセットされているとプラグイン情報を更新しない(オンプレなど外部と通信できない環境向け)

## 動作確認用

### 保存しているバージョンを全て"0"にする

```console
$ bin/rails runner '
  p([:count, LycheeMessageBox::PluginVersion.count])
  pp([:before, LycheeMessageBox::PluginVersion.pluck(:id, :version, :plugin_id)])
  LycheeMessageBox::PluginVersion.update_all(version: "0")
  pp([:after, LycheeMessageBox::PluginVersion.pluck(:id, :version, :plugin_id)])
'
```
