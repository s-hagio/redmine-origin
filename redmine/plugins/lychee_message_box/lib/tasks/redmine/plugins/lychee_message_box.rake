# frozen_string_literal: true

namespace :redmine do
  namespace :plugins do
    namespace :lychee_message_box do
      desc 'Import LycheeMessageBox notices'
      task import_notices: :environment do
        LycheeMessageBox::NoticeImporter.call
      end
    end
  end
end
