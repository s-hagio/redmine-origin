$(function() {
  var $assigned_to = $('select#issue_assigned_to_id');
  var $group = $('select#issue_group_id');

  var updateAssignedToOptions = function(group_id) {
    $.ajax({
      type: 'GET',
      url: $('#assigned_to_options_by_selected_group_path').val(),
      data: {
        selected_group_id: group_id,
        assigned_to_id: $assigned_to.val()
      }
    }).done(function(data) {
      $assigned_to.html(data);
      createAssignedToSelect2();
    });
  };

  var createAssignedToSelect2 = function() {
    $assigned_to.select2({
      width: '60%',
      allowClear: true,
      placeholder: ''
    });
  };

  $assigned_to.on('change', function() {
    if ($(this).val() === '') {
      // アサインがクリアされたときグループフィルタをクリアする
      $group.val(null);
      $group.trigger('change');
    }
  });

  $group.on('change', function() {
    updateAssignedToOptions($(this).val());
  });

  $group.trigger('change');
  createAssignedToSelect2();

  // https://github.com/redmica/redmica/blob/master/app/views/issues/_form.html.erb#L63-L68
  // Redmicaから「担当者」のセレクトボックスを変更する時`change`を読んでいません。
  $(".assign-to-me-link").on('click', function(){
    $assigned_to.val($(this).data('id')).change();
  });
}());
