#!/bin/bash

# Redmineプロジェクトのルートディレクトリに移動
cd /usr/src/redmine

# プラグインのディレクトリ一覧
plugins=(
  lychee_saml_auth
  lychee_remaining_estimate
  lychee_manufacturing
  lychee_issues_evm
  lychee_issue_set
  lychee_issue_datetime
  lychee_groups
  lychee_custom_field
  lychee_cost
  lychee_ccpm
  lpt
  lac
  lychee_version_start_date
  lychee_status_color
  lychee_project_term
  lychee_profile_icon
  lychee_notification
  lychee_issue_spread_sheet
  lychee_issue_form
  lychee_help
  lychee_easy_assigned_user
  lrm
  lgc_pro
  redmine_github_hook
  redmine_single_mail
  lychee_message_box
  alm
  ※以降のプラグインはエラーの度に個別にマイグレーションしている
  lychee_project_report
  lychee_kanban
  lychee_workdays
  lychee_project_dashboard
  lychee_project_view
  lychee_time_management
  lychee_work_plan
)

# 各プラグインに対してマイグレーションを実行
for plugin in "${plugins[@]}"; do
  RAILS_ENV=production bundle exec rake redmine:plugins:migrate NAME="$plugin"
done
