#FROM redmine:5.0.5 ※コンテナ起動後に依存関係インストールで失敗する（Zeitwerk::nameError）
FROM redmine:5.0.6

# 必要なパッケージをインストール（無い場合ビルドが失敗する）
RUN apt-get update && \
    apt-get install -y build-essential libjpeg62 libfontconfig-dev vim && \
    rm -rf /var/lib/apt/lists/*

# Redmineのカスタム設定ファイルをコピー  ※database.ymlが無いと起動エラー
COPY ./redmine/config/database.yml /usr/src/redmine/config/database.yml
COPY ./redmine/config/configuration.yml /usr/src/redmine/config/configuration.yml
COPY ./redmine/config/application.rb /usr/src/redmine/config/application.rb
COPY ./redmine/config/migrate.sh /usr/src/redmine/config/migrate.sh

# secret_key_base の生成と設定  ※無いとエラー
# RUN echo "production:\n  secret_key_base: $(bundle exec rake secret)" > /usr/src/redmine/config/secrets.yml

# その他のカスタム設定や作業があればここに追加
# 必要なライブラリのインストール
WORKDIR /usr/src/redmine
# RUN bundle install --without development test

# LycheeRedmineプラグイン用のテーブル作成　※コンテナ起動後にテーブル作成するため不要
#RUN RAILS_ENV=production bundle exec rake redmine:plugins:migrate

# Redmineインスタンスの再起動　※CMDにて実施するため不要
#RUN bundle exec rails server -e production

# CMD や ENTRYPOINT などの必要なコマンドもここに追加
# CMD ["rails", "server", "-b", "0.0.0.0", "-e", "production"]

